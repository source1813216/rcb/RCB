<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-05-26 10:26:14
$dictionary["AOS_Quotes"]["fields"]["leads_aos_quotes_1"] = array (
  'name' => 'leads_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'leads_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_AOS_QUOTES_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_aos_quotes_1leads_ida',
);
$dictionary["AOS_Quotes"]["fields"]["leads_aos_quotes_1_name"] = array (
  'name' => 'leads_aos_quotes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_AOS_QUOTES_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_aos_quotes_1leads_ida',
  'link' => 'leads_aos_quotes_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["AOS_Quotes"]["fields"]["leads_aos_quotes_1leads_ida"] = array (
  'name' => 'leads_aos_quotes_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_aos_quotes_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


 // created: 2021-05-26 18:46:44
$dictionary['AOS_Quotes']['fields']['additional_revenue_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['additional_revenue_c']['labelValue']='Additional Revenue';

 

 // created: 2021-05-27 08:03:53
$dictionary['AOS_Quotes']['fields']['approval_end_date_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['approval_end_date_c']['labelValue']='Approval End Date';

 

 // created: 2021-05-29 20:05:00
$dictionary['AOS_Quotes']['fields']['approval_routine_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['approval_routine_c']['labelValue']='Approval Routine';

 

 // created: 2021-05-30 20:17:11
$dictionary['AOS_Quotes']['fields']['approval_stamp_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['approval_stamp_c']['labelValue']='Approval Stamp';

 

 // created: 2021-05-27 08:03:36
$dictionary['AOS_Quotes']['fields']['approval_start_date_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['approval_start_date_c']['labelValue']='Approval Start Date';

 

 // created: 2021-06-01 13:37:53
$dictionary['AOS_Quotes']['fields']['approval_status']['inline_edit']=true;
$dictionary['AOS_Quotes']['fields']['approval_status']['merge_filter']='disabled';

 

 // created: 2021-05-29 18:57:26
$dictionary['AOS_Quotes']['fields']['approval_steps_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['approval_steps_c']['labelValue']='Approval Steps';

 

 // created: 2021-05-30 07:32:46
$dictionary['AOS_Quotes']['fields']['apv_rej_notified_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['apv_rej_notified_c']['labelValue']='Apv Rej Notified';

 

 // created: 2021-05-30 10:18:18
$dictionary['AOS_Quotes']['fields']['budget_total_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['budget_total_c']['labelValue']='Budget Total';

 

 // created: 2021-05-27 08:18:40
$dictionary['AOS_Quotes']['fields']['ceo_approval_by_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ceo_approval_by_c']['labelValue']='CEO Office Approval By';

 

 // created: 2021-05-29 19:18:32
$dictionary['AOS_Quotes']['fields']['ceo_apv_title_c']['inline_edit']='';
$dictionary['AOS_Quotes']['fields']['ceo_apv_title_c']['labelValue']='3';

 

 // created: 2021-05-27 08:16:56
$dictionary['AOS_Quotes']['fields']['ceo_date_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ceo_date_c']['labelValue']='CEO Office Response Date';

 

 // created: 2021-06-02 13:42:48
$dictionary['AOS_Quotes']['fields']['ceo_flg_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ceo_flg_c']['labelValue']='CEO Flg';

 

 // created: 2021-05-29 19:31:22
$dictionary['AOS_Quotes']['fields']['ceo_office_approval_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ceo_office_approval_c']['labelValue']='CEO Office Approve/Reject';

 

 // created: 2021-06-01 17:14:55
$dictionary['AOS_Quotes']['fields']['ceo_remarks_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ceo_remarks_c']['labelValue']='CEO Remarks';

 

 // created: 2021-05-30 20:03:04
$dictionary['AOS_Quotes']['fields']['ceo_stamp_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ceo_stamp_c']['labelValue']='CEO Stamp';

 

 // created: 2021-06-01 11:06:41
$dictionary['AOS_Quotes']['fields']['clarification_done_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['clarification_done_c']['labelValue']='Clarification Done';

 

 // created: 2021-05-26 18:22:47
$dictionary['AOS_Quotes']['fields']['day_spending_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['day_spending_c']['labelValue']='Day Spending';

 

 // created: 2021-05-27 08:17:45
$dictionary['AOS_Quotes']['fields']['ddm_approval_by_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ddm_approval_by_c']['labelValue']='DDM Approval By';

 

 // created: 2021-06-01 11:08:15
$dictionary['AOS_Quotes']['fields']['ddm_approval_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ddm_approval_c']['labelValue']='DDM Approve/Reject';

 

 // created: 2021-05-29 19:15:15
$dictionary['AOS_Quotes']['fields']['ddm_approval_title_c']['inline_edit']='';
$dictionary['AOS_Quotes']['fields']['ddm_approval_title_c']['labelValue']='DDM Approval Title';

 

 // created: 2021-05-27 08:15:18
$dictionary['AOS_Quotes']['fields']['ddm_date_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ddm_date_c']['labelValue']='DDM Response  Date';

 

 // created: 2021-06-02 13:32:04
$dictionary['AOS_Quotes']['fields']['ddm_flg_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ddm_flg_c']['labelValue']='DDM Flg';

 

 // created: 2021-06-01 17:15:14
$dictionary['AOS_Quotes']['fields']['ddm_remarks_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ddm_remarks_c']['labelValue']='DDM Remarks';

 

 // created: 2021-05-30 20:02:27
$dictionary['AOS_Quotes']['fields']['ddm_stamp_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['ddm_stamp_c']['labelValue']='DDM Stamp';

 

 // created: 2021-05-26 18:51:15
$dictionary['AOS_Quotes']['fields']['event_brief_c']['labelValue']='Event Brief';

 

 // created: 2021-05-30 09:44:58
$dictionary['AOS_Quotes']['fields']['event_date_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['event_date_c']['options']='date_range_search_dom';
$dictionary['AOS_Quotes']['fields']['event_date_c']['labelValue']='Event Date';
$dictionary['AOS_Quotes']['fields']['event_date_c']['enable_range_search']='1';

 

 // created: 2021-05-29 18:46:16
$dictionary['AOS_Quotes']['fields']['event_days_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['event_days_c']['labelValue']='Event Days';

 

 // created: 2021-05-30 09:43:15
$dictionary['AOS_Quotes']['fields']['event_month_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['event_month_c']['labelValue']='Event Month';

 

 // created: 2021-05-30 09:43:30
$dictionary['AOS_Quotes']['fields']['event_year_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['event_year_c']['labelValue']='Event Year';

 

 // created: 2021-05-29 18:47:00
$dictionary['AOS_Quotes']['fields']['expected_revenue_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['expected_revenue_c']['labelValue']='Expected Revenue';

 

 // created: 2021-05-30 19:28:52
$dictionary['AOS_Quotes']['fields']['final_approval_text_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['final_approval_text_c']['labelValue']='Final Approval Text';

 

 // created: 2021-05-30 19:33:18
$dictionary['AOS_Quotes']['fields']['final_approver_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['final_approver_c']['labelValue']='Final Approver';

 

 // created: 2021-05-29 18:47:17
$dictionary['AOS_Quotes']['fields']['gor_commitment_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['gor_commitment_c']['labelValue']='GoR Commitment';

 

 // created: 2021-05-29 19:01:42
$dictionary['AOS_Quotes']['fields']['gor_commitment_limit_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['gor_commitment_limit_c']['labelValue']='GoR Commitment Limit';

 

 // created: 2021-05-29 18:47:34
$dictionary['AOS_Quotes']['fields']['gor_revenue_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['gor_revenue_c']['labelValue']='GoR Revenue (Via Taxes)';

 

 // created: 2021-05-27 06:20:15
$dictionary['AOS_Quotes']['fields']['help_notifications_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['help_notifications_c']['labelValue']='Help Notifications';

 

 // created: 2021-05-27 08:18:08
$dictionary['AOS_Quotes']['fields']['management_approval_by_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['management_approval_by_c']['labelValue']='Management Approval By';

 

 // created: 2021-05-29 19:31:38
$dictionary['AOS_Quotes']['fields']['management_approval_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['management_approval_c']['labelValue']='Mgmt. Approve/Reject';

 

 // created: 2021-05-27 08:15:35
$dictionary['AOS_Quotes']['fields']['management_date_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['management_date_c']['labelValue']='Management Response Date';

 

 // created: 2021-06-01 17:15:31
$dictionary['AOS_Quotes']['fields']['management_remarks_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['management_remarks_c']['labelValue']='Management Remarks';

 

 // created: 2021-05-29 18:39:33
$dictionary['AOS_Quotes']['fields']['manually_calculate_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['manually_calculate_c']['labelValue']='Manually Calculate';

 

 // created: 2021-05-29 19:39:08
$dictionary['AOS_Quotes']['fields']['member_1_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['member_1_c']['labelValue']='Member 1';

 

 // created: 2021-05-29 19:39:45
$dictionary['AOS_Quotes']['fields']['member_2_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['member_2_c']['labelValue']='Member 2';

 

 // created: 2021-05-29 19:40:00
$dictionary['AOS_Quotes']['fields']['member_3_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['member_3_c']['labelValue']='Member 3';

 

 // created: 2021-05-29 19:40:13
$dictionary['AOS_Quotes']['fields']['member_4_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['member_4_c']['labelValue']='Member 4';

 

 // created: 2021-05-29 19:40:28
$dictionary['AOS_Quotes']['fields']['member_5_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['member_5_c']['labelValue']='Member 5';

 

 // created: 2021-05-29 19:40:39
$dictionary['AOS_Quotes']['fields']['member_6_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['member_6_c']['labelValue']='Member 6';

 

 // created: 2021-05-30 20:03:28
$dictionary['AOS_Quotes']['fields']['memstamp1_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['memstamp1_c']['labelValue']='MemStamp1';

 

 // created: 2021-05-30 20:03:39
$dictionary['AOS_Quotes']['fields']['memstamp2_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['memstamp2_c']['labelValue']='MemStamp2';

 

 // created: 2021-05-30 20:03:51
$dictionary['AOS_Quotes']['fields']['memstamp3_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['memstamp3_c']['labelValue']='MemStamp3';

 

 // created: 2021-05-30 20:04:07
$dictionary['AOS_Quotes']['fields']['memstamp4_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['memstamp4_c']['labelValue']='MemStamp4';

 

 // created: 2021-05-30 20:04:20
$dictionary['AOS_Quotes']['fields']['memstamp5_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['memstamp5_c']['labelValue']='MemStamp5';

 

 // created: 2021-05-30 20:04:31
$dictionary['AOS_Quotes']['fields']['memstamp6_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['memstamp6_c']['labelValue']='MemStamp6';

 

 // created: 2021-05-29 19:18:00
$dictionary['AOS_Quotes']['fields']['mgmt_apv_titile_c']['inline_edit']='';
$dictionary['AOS_Quotes']['fields']['mgmt_apv_titile_c']['labelValue']='2';

 

 // created: 2021-05-30 20:02:50
$dictionary['AOS_Quotes']['fields']['mgmt_stamp_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['mgmt_stamp_c']['labelValue']='Mgmt Stamp';

 

 // created: 2021-06-02 13:42:32
$dictionary['AOS_Quotes']['fields']['mmgt_flg_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['mmgt_flg_c']['labelValue']='Mmgt Flg';

 

 // created: 2021-05-27 06:23:15
$dictionary['AOS_Quotes']['fields']['name']['inline_edit']=true;
$dictionary['AOS_Quotes']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Quotes']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Quotes']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Quotes']['fields']['name']['unified_search']=false;

 

 // created: 2021-05-29 18:46:35
$dictionary['AOS_Quotes']['fields']['no_of_delegates_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['no_of_delegates_c']['labelValue']='No of Delegates';

 

 // created: 2021-05-29 19:43:43
$dictionary['AOS_Quotes']['fields']['prepared_by_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['prepared_by_c']['labelValue']='Prepared By';

 

 // created: 2021-05-26 18:58:30
$dictionary['AOS_Quotes']['fields']['recommendation_c']['labelValue']='Recommendation';

 

 // created: 2021-06-01 12:43:38
$dictionary['AOS_Quotes']['fields']['remarks_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['remarks_c']['labelValue']='Remarks';

 

 // created: 2021-05-26 18:48:29
$dictionary['AOS_Quotes']['fields']['revenue_description_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['revenue_description_c']['labelValue']='Revenue Description';

 

 // created: 2021-05-30 10:54:48
$dictionary['AOS_Quotes']['fields']['revenue_total_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['revenue_total_c']['labelValue']='Revenue Total';

 

 // created: 2021-05-27 08:03:15
$dictionary['AOS_Quotes']['fields']['send_for_approval_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['send_for_approval_c']['labelValue']='Send for Approval';

 

 // created: 2021-05-29 20:05:00
$dictionary['AOS_Quotes']['fields']['srp_stdapproval_routine_id_c']['inline_edit']=1;

 

 // created: 2021-06-03 11:12:16
$dictionary['AOS_Quotes']['fields']['title1_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['title1_c']['labelValue']='*';

 

 // created: 2021-06-03 11:12:34
$dictionary['AOS_Quotes']['fields']['title2_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['title2_c']['labelValue']='**';

 

 // created: 2021-05-30 19:33:18
$dictionary['AOS_Quotes']['fields']['user_id10_c']['inline_edit']=1;

 

 // created: 2021-05-30 20:02:27
$dictionary['AOS_Quotes']['fields']['user_id11_c']['inline_edit']=1;

 

 // created: 2021-05-30 20:02:50
$dictionary['AOS_Quotes']['fields']['user_id12_c']['inline_edit']=1;

 

 // created: 2021-05-30 20:03:04
$dictionary['AOS_Quotes']['fields']['user_id13_c']['inline_edit']=1;

 

 // created: 2021-05-30 20:03:28
$dictionary['AOS_Quotes']['fields']['user_id14_c']['inline_edit']=1;

 

 // created: 2021-05-30 20:03:39
$dictionary['AOS_Quotes']['fields']['user_id15_c']['inline_edit']=1;

 

 // created: 2021-05-30 20:03:51
$dictionary['AOS_Quotes']['fields']['user_id16_c']['inline_edit']=1;

 

 // created: 2021-05-30 20:04:07
$dictionary['AOS_Quotes']['fields']['user_id17_c']['inline_edit']=1;

 

 // created: 2021-05-30 20:04:20
$dictionary['AOS_Quotes']['fields']['user_id18_c']['inline_edit']=1;

 

 // created: 2021-05-30 20:04:30
$dictionary['AOS_Quotes']['fields']['user_id19_c']['inline_edit']=1;

 

 // created: 2021-05-27 08:18:08
$dictionary['AOS_Quotes']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2021-05-27 08:18:40
$dictionary['AOS_Quotes']['fields']['user_id2_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:39:08
$dictionary['AOS_Quotes']['fields']['user_id3_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:39:45
$dictionary['AOS_Quotes']['fields']['user_id4_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:40:00
$dictionary['AOS_Quotes']['fields']['user_id5_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:40:13
$dictionary['AOS_Quotes']['fields']['user_id6_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:40:28
$dictionary['AOS_Quotes']['fields']['user_id7_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:40:39
$dictionary['AOS_Quotes']['fields']['user_id8_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:43:43
$dictionary['AOS_Quotes']['fields']['user_id9_c']['inline_edit']=1;

 

 // created: 2021-05-27 08:17:45
$dictionary['AOS_Quotes']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2021-05-26 18:56:26
$dictionary['AOS_Quotes']['fields']['venue_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['venue_c']['labelValue']='Venue';

 

 // created: 2021-05-26 18:56:26
$dictionary['AOS_Quotes']['fields']['vnp_venues_id_c']['inline_edit']=1;

 
?>