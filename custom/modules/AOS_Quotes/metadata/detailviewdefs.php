<?php
$module_name = 'AOS_Quotes';
$_object_name = 'aos_quotes';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DELETE',
          2 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'pdf\');" value="{$MOD.LBL_PRINT_AS_PDF}">',
          ),
          3 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'emailpdf\');" value="{$MOD.LBL_EMAIL_PDF}">',
          ),
        ),
      ),
      'maxColumns' => '2',
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/AOS_Quotes/js/techinc-detail.js',
        ),
      ),
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_OVERVIEW' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_LINE_ITEMS' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'approval_status',
            'label' => 'LBL_APPROVAL_STATUS',
          ),
          1 => 
          array (
            'name' => 'prepared_by_c',
            'studio' => 'visible',
            'label' => 'LBL_PREPARED_BY',
          ),
        ),
        1 => 
        array (
          0 => '',
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'title1_c',
            'label' => 'LBL_TITLE1',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'ddm_approval_by_c',
            'studio' => 'visible',
            'label' => 'LBL_DDM_APPROVAL_BY',
          ),
          1 => 
          array (
            'name' => 'ddm_date_c',
            'label' => 'LBL_DDM_DATE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'management_approval_by_c',
            'studio' => 'visible',
            'label' => 'LBL_MANAGEMENT_APPROVAL_BY',
          ),
          1 => 
          array (
            'name' => 'management_date_c',
            'label' => 'LBL_MANAGEMENT_DATE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'ceo_approval_by_c',
            'studio' => 'visible',
            'label' => 'LBL_CEO_APPROVAL_BY',
          ),
          1 => 
          array (
            'name' => 'ceo_date_c',
            'label' => 'LBL_CEO_DATE',
          ),
        ),
        6 => 
        array (
          0 => '',
          1 => '',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'title2_c',
            'label' => 'LBL_TITLE2',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'member_1_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_1',
          ),
          1 => 
          array (
            'name' => 'member_2_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_2',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'member_3_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_3',
          ),
          1 => 
          array (
            'name' => 'member_4_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_4',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'member_5_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_5',
          ),
          1 => 
          array (
            'name' => 'member_6_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_6',
          ),
        ),
      ),
      'LBL_PANEL_OVERVIEW' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'billing_account',
            'label' => 'LBL_BILLING_ACCOUNT',
          ),
          1 => 
          array (
            'name' => 'billing_contact',
            'label' => 'LBL_BILLING_CONTACT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'leads_aos_quotes_1_name',
            'label' => 'LBL_LEADS_AOS_QUOTES_1_FROM_LEADS_TITLE',
          ),
          1 => 
          array (
            'name' => 'event_date_c',
            'label' => 'LBL_EVENT_DATE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'venue_c',
            'studio' => 'visible',
            'label' => 'LBL_VENUE',
          ),
          1 => 
          array (
            'name' => 'manually_calculate_c',
            'label' => 'LBL_MANUALLY_CALCULATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'no_of_delegates_c',
            'label' => 'LBL_NO_OF_DELEGATES',
          ),
          1 => 
          array (
            'name' => 'event_days_c',
            'label' => 'LBL_EVENT_DAYS',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'day_spending_c',
            'label' => 'LBL_DAY_SPENDING',
          ),
          1 => 
          array (
            'name' => 'expected_revenue_c',
            'label' => 'LBL_EXPECTED_REVENUE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'gor_revenue_c',
            'label' => 'LBL_GOR_REVENUE',
          ),
          1 => 
          array (
            'name' => 'gor_commitment_c',
            'label' => 'LBL_GOR_COMMITMENT',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'additional_revenue_c',
            'label' => 'LBL_ADDITIONAL_REVENUE ',
          ),
          1 => 
          array (
            'name' => 'revenue_description_c',
            'studio' => 'visible',
            'label' => 'LBL_REVENUE_DESCRIPTION',
          ),
        ),
      ),
      'lbl_detailview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'event_brief_c',
            'label' => 'LBL_EVENT_BRIEF',
          ),
        ),
      ),
      'lbl_line_items' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'currency_id',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCY',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'line_items',
            'label' => 'LBL_LINE_ITEMS',
          ),
        ),
        2 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'total_amount',
            'label' => 'LBL_GRAND_TOTAL',
          ),
        ),
      ),
      'lbl_detailview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'recommendation_c',
            'label' => 'LBL_RECOMMENDATION',
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
          1 => 
          array (
            'name' => 'approval_routine_c',
            'studio' => 'visible',
            'label' => 'LBL_APPROVAL_ROUTINE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'label' => 'LBL_DATE_MODIFIED',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'approval_steps_c',
            'label' => 'LBL_APPROVAL_STEPS',
          ),
          1 => 
          array (
            'name' => 'gor_commitment_limit_c',
            'label' => 'LBL_GOR_COMMITMENT_LIMIT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'help_notifications_c',
            'label' => 'LBL_HELP_NOTIFICATIONS',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
