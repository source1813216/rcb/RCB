<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-06-18 12:29:42
$dictionary['Case']['fields']['account_id_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:27:13
$dictionary['Case']['fields']['approval_end_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['approval_end_date_c']['labelValue']='Approval End Date';

 

 // created: 2021-06-18 12:25:54
$dictionary['Case']['fields']['approval_start_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['approval_start_date_c']['labelValue']='Approval Start Date';

 

 // created: 2021-06-18 12:27:45
$dictionary['Case']['fields']['approval_status_c']['inline_edit']='1';
$dictionary['Case']['fields']['approval_status_c']['labelValue']='Approval Status';

 

 // created: 2021-06-18 12:29:42
$dictionary['Case']['fields']['client_c']['inline_edit']='1';
$dictionary['Case']['fields']['client_c']['labelValue']='Client';

 

 // created: 2021-06-18 12:29:52
$dictionary['Case']['fields']['contact_c']['inline_edit']='1';
$dictionary['Case']['fields']['contact_c']['labelValue']='Contact';

 

 // created: 2021-06-18 12:29:52
$dictionary['Case']['fields']['contact_id_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:39:34
$dictionary['Case']['fields']['ddm_approval_by_c']['inline_edit']='1';
$dictionary['Case']['fields']['ddm_approval_by_c']['labelValue']='DDM Approval By';

 

 // created: 2021-06-18 12:44:20
$dictionary['Case']['fields']['ddm_approval_c']['inline_edit']='1';
$dictionary['Case']['fields']['ddm_approval_c']['labelValue']='DDM Approv/Reject';

 

 // created: 2021-06-18 12:25:16
$dictionary['Case']['fields']['ddm_approval_title_c']['inline_edit']='';
$dictionary['Case']['fields']['ddm_approval_title_c']['labelValue']='1';

 

 // created: 2021-06-18 12:45:26
$dictionary['Case']['fields']['ddm_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['ddm_date_c']['labelValue']='DDM Response Date';

 

 // created: 2021-06-18 12:44:31
$dictionary['Case']['fields']['ddm_flag_c']['inline_edit']='1';
$dictionary['Case']['fields']['ddm_flag_c']['labelValue']='DDM Flag';

 

 // created: 2021-06-18 12:45:07
$dictionary['Case']['fields']['ddm_remarks_c']['inline_edit']='1';
$dictionary['Case']['fields']['ddm_remarks_c']['labelValue']='DDM Remarks';

 

 // created: 2021-06-18 12:45:46
$dictionary['Case']['fields']['ddm_stamp_c']['inline_edit']='1';
$dictionary['Case']['fields']['ddm_stamp_c']['labelValue']='DDM Stamp';

 

 // created: 2021-06-18 12:46:19
$dictionary['Case']['fields']['event_brief_c']['labelValue']='Event Brief';

 

 // created: 2021-06-18 12:46:40
$dictionary['Case']['fields']['event_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['event_date_c']['labelValue']='Event Date';

 

 // created: 2021-05-05 11:41:20
$dictionary['Case']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:19
$dictionary['Case']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:19
$dictionary['Case']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:19
$dictionary['Case']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:49:25
$dictionary['Case']['fields']['member_1_c']['inline_edit']='1';
$dictionary['Case']['fields']['member_1_c']['labelValue']='Member 1';

 

 // created: 2021-06-18 12:49:36
$dictionary['Case']['fields']['member_2_c']['inline_edit']='1';
$dictionary['Case']['fields']['member_2_c']['labelValue']='Member 2';

 

 // created: 2021-06-18 12:49:46
$dictionary['Case']['fields']['member_3_c']['inline_edit']='1';
$dictionary['Case']['fields']['member_3_c']['labelValue']='Member 3';

 

 // created: 2021-06-18 12:49:58
$dictionary['Case']['fields']['member_4_c']['inline_edit']='1';
$dictionary['Case']['fields']['member_4_c']['labelValue']='Member 4';

 

 // created: 2021-06-18 12:50:12
$dictionary['Case']['fields']['member_5_c']['inline_edit']='1';
$dictionary['Case']['fields']['member_5_c']['labelValue']='Member 5';

 

 // created: 2021-06-18 12:50:29
$dictionary['Case']['fields']['member_6_c']['inline_edit']='1';
$dictionary['Case']['fields']['member_6_c']['labelValue']='Member 6';

 

 // created: 2021-06-18 12:50:49
$dictionary['Case']['fields']['memstamp1_c']['inline_edit']='1';
$dictionary['Case']['fields']['memstamp1_c']['labelValue']='MemStamp1';

 

 // created: 2021-06-18 12:51:03
$dictionary['Case']['fields']['memstamp2_c']['inline_edit']='1';
$dictionary['Case']['fields']['memstamp2_c']['labelValue']='MemStamp2';

 

 // created: 2021-06-18 12:51:15
$dictionary['Case']['fields']['memstamp3_c']['inline_edit']='1';
$dictionary['Case']['fields']['memstamp3_c']['labelValue']='MemStamp3';

 

 // created: 2021-06-18 12:51:47
$dictionary['Case']['fields']['memstamp4_c']['inline_edit']='1';
$dictionary['Case']['fields']['memstamp4_c']['labelValue']='MemStamp4';

 

 // created: 2021-06-18 12:51:59
$dictionary['Case']['fields']['memstamp5_c']['inline_edit']='1';
$dictionary['Case']['fields']['memstamp5_c']['labelValue']='MemStamp5';

 

 // created: 2021-06-18 12:52:10
$dictionary['Case']['fields']['memstamp6_c']['inline_edit']='1';
$dictionary['Case']['fields']['memstamp6_c']['labelValue']='MemStamp6';

 

 // created: 2021-06-18 13:20:28
$dictionary['Case']['fields']['mgmt_approval_c']['inline_edit']='1';
$dictionary['Case']['fields']['mgmt_approval_c']['labelValue']='Management Approve/Reject';

 

 // created: 2021-06-18 12:25:28
$dictionary['Case']['fields']['mgmt_approval_title_c']['inline_edit']='';
$dictionary['Case']['fields']['mgmt_approval_title_c']['labelValue']='2';

 

 // created: 2021-06-18 12:47:41
$dictionary['Case']['fields']['mgmt_apv_by_c']['inline_edit']='1';
$dictionary['Case']['fields']['mgmt_apv_by_c']['labelValue']='Management Approval By';

 

 // created: 2021-06-18 12:48:24
$dictionary['Case']['fields']['mgmt_date_c']['inline_edit']='1';
$dictionary['Case']['fields']['mgmt_date_c']['labelValue']='Management Response Date';

 

 // created: 2021-06-18 13:21:41
$dictionary['Case']['fields']['mgmt_flag_c']['inline_edit']='1';
$dictionary['Case']['fields']['mgmt_flag_c']['labelValue']='Management Flag';

 

 // created: 2021-06-18 12:48:04
$dictionary['Case']['fields']['mgmt_remarks_c']['inline_edit']='1';
$dictionary['Case']['fields']['mgmt_remarks_c']['labelValue']='Management Remarks';

 

 // created: 2021-06-18 13:23:40
$dictionary['Case']['fields']['prepared_by_c']['inline_edit']='1';
$dictionary['Case']['fields']['prepared_by_c']['labelValue']='Prepared By';

 

 // created: 2021-06-18 14:03:07
$dictionary['Case']['fields']['send_for_approval_c']['inline_edit']='1';
$dictionary['Case']['fields']['send_for_approval_c']['labelValue']='Send For Approval';

 

 // created: 2021-06-18 12:51:03
$dictionary['Case']['fields']['user_id10_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:51:15
$dictionary['Case']['fields']['user_id11_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:51:47
$dictionary['Case']['fields']['user_id12_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:51:59
$dictionary['Case']['fields']['user_id13_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:52:10
$dictionary['Case']['fields']['user_id14_c']['inline_edit']=1;

 

 // created: 2021-06-18 13:23:40
$dictionary['Case']['fields']['user_id15_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:45:46
$dictionary['Case']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:47:41
$dictionary['Case']['fields']['user_id2_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:49:25
$dictionary['Case']['fields']['user_id3_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:49:36
$dictionary['Case']['fields']['user_id4_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:49:46
$dictionary['Case']['fields']['user_id5_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:49:58
$dictionary['Case']['fields']['user_id6_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:50:12
$dictionary['Case']['fields']['user_id7_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:50:29
$dictionary['Case']['fields']['user_id8_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:50:49
$dictionary['Case']['fields']['user_id9_c']['inline_edit']=1;

 

 // created: 2021-06-18 12:39:34
$dictionary['Case']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2021-06-18 13:23:55
$dictionary['Case']['fields']['venue_c']['inline_edit']='1';
$dictionary['Case']['fields']['venue_c']['labelValue']='Venue';

 

 // created: 2021-06-18 13:23:55
$dictionary['Case']['fields']['vnp_venues_id_c']['inline_edit']=1;

 
?>