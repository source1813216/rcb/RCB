<?php
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
function checkPluginLicenceFAC($licenceKey) 
{
    require_once('modules/Administration/Administration.php');
    global $sugar_config;
    $administrationObj = new Administration();
    $administrationObj->retrieveSettings('FieldAccessControlPlugin');
    $domain = 'certislanka.tech';
    $pluginSku = $administrationObj->settings['FieldAccessControlPlugin_FieldACPluginProductSKU'];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, sprintf('https://www.appjetty.com/extension/licence.php'));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'key=' . urlencode($licenceKey) . '&domains=' . urlencode($domain) . '&sec=' . $pluginSku);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $content = curl_exec($ch);
    $result = json_decode($content);
    $errmsg = curl_error($ch);
    $errNo = curl_errno($ch);
    $currDate = date("Y/m/d");
    
    if ($result->suc == 1) {
        $administrationObj->saveSetting("FieldAccessControlPlugin", "LicenceKey", $licenceKey);
        $administrationObj->saveSetting("FieldAccessControlPlugin", "LastValidation", 1);
        $administrationObj->saveSetting("FieldAccessControlPlugin", "LastValidationDate", $currDate);
        $administrationObj->saveSetting("FieldAccessControlPlugin", "LastValidationMsg", "This module is disabled, to enable it go to admin section.");
        $administrationObj->saveSetting("FieldAccessControlPlugin", "ModuleEnabled", 0);
    } else {
        $administrationObj->saveSetting("FieldAccessControlPlugin", "LicenceKey", $licenceKey);
        $administrationObj->saveSetting("FieldAccessControlPlugin", "LastValidation", 0);
        $administrationObj->saveSetting("FieldAccessControlPlugin", "LastValidationDate", $currDate);
        if ($errNo > 0) {
            $administrationObj->saveSetting("FieldAccessControlPlugin", "LastValidationMsg", 'There seems some error while validating your license for field level access control module. Please try again later.<br /><b>Error : ' . $errmsg . '</b>');
        } else {
            $administrationObj->saveSetting("FieldAccessControlPlugin", "LastValidationMsg", $result->msg);
        }
        $administrationObj->saveSetting("FieldAccessControlPlugin", "ModuleEnabled", 0);
    }
    return $result;
}
function checkPluginStatus_FAC(){
    $disabled_plugin_loginslider = false;
    $checkLoginSubscription = validateFieldACLSubscription();
    if (!$checkLoginSubscription['success']) {
        $disabled_plugin_loginslider = true;
    }
    return $disabled_plugin_loginslider;
}
function validateFieldACLSubscription() 
{
    global $db;
    require_once('modules/Administration/Administration.php');
    $administrationObj = new Administration();
    $administrationObj->retrieveSettings('FieldAccessControlPlugin');
    $LastValidation = $administrationObj->settings['FieldAccessControlPlugin_LastValidation'];
    $LastValidationDate = isset($administrationObj->settings['FieldAccessControlPlugin_LastValidationDate']) ? $administrationObj->settings['FieldAccessControlPlugin_LastValidationDate'] : "";
    $LastValidationMsg = $administrationObj->settings['FieldAccessControlPlugin_LastValidationMsg'];
    $ModuleEnabled = $administrationObj->settings['FieldAccessControlPlugin_ModuleEnabled'];
    $LastValidationDate = strtotime($LastValidationDate);
    $CurrentDate = strtotime(date("Y/m/d"));
    $response = array();
    if (($LastValidation == 1) && ($ModuleEnabled == 1)) {
        $response['success'] = true;
        $response['message'] = html_entity_decode($LastValidationMsg);
    } else {
        $response['success'] = false;
        $response['message'] = html_entity_decode($LastValidationMsg);
    }
    $qry = "SELECT * FROM upgrade_history WHERE id_name = 'SuiteCRM_Field_Access_Control_Plugin' ";
    $result = $db->query($qry);
    while ($row = $db->fetchByAssoc($result)) {
        if ($row['enabled'] == 0) {
            $response['success'] = false;
            $response['message'] = 'The plugin is disabled. Please contact your administrator.';
        }
    }
    return $response;
}
