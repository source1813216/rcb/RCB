{*
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
 *}
<html>
    <div class="moduleTitle">
        <h2 class="module-title-text">{$MOD.LBL_AUTOPOPULATE_FIELDS}</h2>
        <div class="clear"></div>
    </div>
    
    <!-- SuiteCRM Help Box Design Start -->
    {$HELP_BOX_CONTENT}
    <!-- SuiteCRM Help Box Design End -->

    <div style="float: right;">
        <a href="index.php?module=VIAutoPopulateFieldsLicenseAddon&action=license"><button class="button">{$MOD.LBL_UPDATE_LICENSE}</button></a>
    </div><br><br>

    <link rel="stylesheet" type="text/css" href="custom/modules/Administration/css/VIAutoPopulateFieldsListview.css">

    {if $AUTO_POPULATE_FIELDS_CONFIG|@count gt 0}
        <div>
            <table class="addNewTable">
                <tr>
                    <td>
                        <input type="button" name="addNew" value="{$MOD.LBL_ADD_NEW}" class="button" onclick="location.href = '{$EDITVIEW_URL}';">
                    </td>
                    <td class="bulkActionRow">
                        <div>
                            <button class="bulkAction" disabled="true">
                                <label class="selected-actions-label hidden-desktop">
                                    {$MOD.LBL_BULK_ACTION}
                                    <span class="suitepicon suitepicon-action-caret actionIcon"></span>
                                </label>
                            </button>

                            <ul id="actionLinkTop" class="clickMenu selectActions fancymenu SugarActionMenu" style="display:none;" name="selectActions">
                                <li class="sugar_action_button actionButton">
                                    <label class="selected-actions-label hidden-desktop actionLabel">{$MOD.LBL_BULK_ACTION}
                                        <span class="suitepicon suitepicon-action-caret actions actionIcon"></span>
                                    </label>
                                    <ul class="subnav ddopen" id="actionMenu" style="display:none;">
                                        <li>
                                            <a class="actionStatus" onclick="updateActionStatus(1)">{$MOD.LBL_ACTIVE}</a>
                                        </li>
                                        <li>
                                            <a class="actionStatus" onclick="updateActionStatus(0)">{$MOD.LBL_INACTIVE}</a>
                                        </li>
                                        <li>
                                            <a class="btnDelete actionStatus"onclick="actionDelete();">{$MOD.LBL_DELETE}</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul> 
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="list-view-rounded-corners">
            <table cellpadding="0" cellspacing="0" border="0" class="list view table-responsive">
                <thead>
                    <tr height="20">
                        <th class><input type="checkbox" id="selectAll"></th>
                        <th class="td_alt quick_view_links">&nbsp;</th>
                        <th>
                            <div>
                                <a class="listViewThLinkS1" href="#">{$MOD.LBL_PRIMARY_MODULE}</a>
                            </div>
                        </th>
                        <th>
                            <div>
                                <a class="listViewThLinkS1" href="#">{$MOD.LBL_FIELD_MAPPING}</a>
                            </div>
                        </th>
                        <th>
                            <div>
                                <a class="listViewThLinkS1" href="#">{$MOD.LBL_CALCULATE_FIELD}</a>
                            </div>
                        </th>
                        <th>
                            <div>
                                <a class="listViewThLinkS1" href="#">{$MOD.LBL_STATUS}</a>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$AUTO_POPULATE_FIELDS_CONFIG key=k item=data}
                        <tr height="20" data-id="{$k}">
                            <td>
                                <input class="listviewCheckbox" name="mass[]" id="mass[]" value="{$k}" type="checkbox">
                            </td>
                            {if $THEME eq 'SuiteP'}
                                <td>
                                    <a class="edit-link" title="{$MOD.LBL_EDIT}" id="{$k}" href="{$EDITVIEW_URL}&records={$k}">
                                    <img src="themes/{$THEME}/images/edit_inline.png"></a>
                                </td>
                            {else}
                                <td>
                                    <a class="edit-link" title="{$MOD.LBL_EDIT}" id="{$k}" href="{$EDITVIEW_URL}&records={$k}"><img src="themes/{$THEME}/images/edit.gif"></a>
                                </td>
                            {/if}
                            <td field="module_name" valign="top" align="left">
                                <b>{$data.moduleName}</b>
                            </td>
                            <td field="field_mapping_related_field" valign="top" align="left">
                                <b>
                                    {if $data.fieldMappingRelateFieldList|@count gt 0}
                                        {foreach from=$data.fieldMappingRelateFieldList key=rkey item=relatedField}
                                            {if rkey neq '0'}
                                                <br>
                                            {/if}
                                            {$relatedField}
                                        {/foreach}
                                    {else}
                                        -
                                    {/if}
                                </b>
                            </td>
                            <td field="calculate_related_field" valign="top" align="left">
                                <b>
                                    {if $data.calculateFieldRelateFieldList|@count gt 0}
                                        {foreach from=$data.calculateFieldRelateFieldList key=rkey item=calculateRelatedField}
                                            {if rkey neq '0'}
                                                <br>
                                            {/if}
                                            {$calculateRelatedField}
                                        {/foreach}
                                    {else}
                                        -
                                    {/if}
                                </b>
                            </td>
                            <td field="status" valign="top" align="left">
                                <label class="switch">
                                    <input type="checkbox" class="enableAutoPopulateConfig" name="enableAutoPopulateConfig" {if $data.status eq 1}checked="checked"{/if} value="0">
                                    <span class="slider round" id='slider_round'></span>
                                </label>
                            </td>
                        </tr>
                    {/foreach}        
                </tbody>
            </table>
        </div>
    {else}
        <br>
        <div class="list view listViewEmpty">
            <br><p class="msg">{$MOD.LBL_CREATE_MESSAGE}
                    <a href="{$EDITVIEW_URL}">{$MOD.LBL_CREATE}</a>{$MOD.LBL_CREATE_MESSAGE_ONE}
                </p>
        </div>
    {/if}
</html>

{literal}
<script type="text/javascript">
    var mod = {/literal}{$MOD|@json_encode}{literal};
    var script   = document.createElement("script");
    script.type  = "text/javascript";
    script.src   = "custom/modules/Administration/js/VIAutoPopulateFieldsListView.js?v="+Math.random();
    document.body.appendChild(script);
</script>
{/literal}