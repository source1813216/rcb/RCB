{**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 *}
<form method='POST' name='EditView' id='Field_ACL_Setting_Form'>
<input type='hidden' name='module' value='Administration'>
<input type='hidden' name='action' value='fieldAccessControlHandler'>
<input type='hidden' name='method' value='save_field_list_by_module'>
<input type='hidden' name='role_id' value='{$ROLE.id}'>
<div class="saveClearButtonSection">
<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button flaGlobalButton" onclick="this.form.method.value='save_field_list_by_module';field_access_control_view.fieldACLSaveView('Field_ACL_Setting_Form');return false;" type="button" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" > &nbsp;
<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" class='button flaGlobalButton' accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" type='button' name='save' value="{$APP.LBL_CANCEL_BUTTON_LABEL}" class='button' onclick='field_access_control_view.fieldACLDisplay("None");'>
<input title="{$RESET_ACCESS}" class='button flaGlobalButton resetAccessSugar' type='button' name='save' value="{$RESET_ACCESS}" class='button' onclick='this.form.method.value="reset_field_accesses";field_access_control_view.resetAccess("Field_ACL_Setting_Form");'>
<span id='successMsg'></span></div>
{$COLOR_PALETTE}