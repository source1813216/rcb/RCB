{*
/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
*}
{$listviewURL}
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="custom/modules/Administration/css/VIAutoPopulateFieldsEditView.css">
	</head>
	<body>
		<div id="pagecontent" class=".pagecontent">
			<div class="moduleTitle">
			<h2>{$mod.LBL_AUTOPOPULATE_FIELDS}</h2>
		</div>
		<div class="clear"></div>
		<form name="EditView" id="EditView">
			<div class="progression-container">
				<ul class="progression">
					<li id="navStep1" class="nav-steps selected" data-nav-step="1"><div id="selectModule">{$mod.LBL_SELECT_MODULE}</div></li>
					<li id="navStep2" class="nav-steps" data-nav-step="2"><div id="fieldMapping">{$mod.LBL_FIELD_MAPPING}</div></li>
					<li id="navStep3" class="nav-steps" data-nav-step="3"><div id="calculateField">{$mod.LBL_CALCULATE_FIELD}</div></li>
				</ul>
			</div>
			<p></p>
			<div id="buttons">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr> 
							<td align="left" width="30%">
								<table border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td>
												<div id="backButtonDiv"><button type="button" class="button" name="backButton" id="backButton" style="display: none;">{$mod.LBL_BACK_BUTTON}</button></div>
											</td>
											<td>
												<div id="cancelButtonDiv"><button type="button" class="button" name="cancelButton" id="cancelButton" onclick="cancel();">{$mod.LBL_CANCEL_BUTTON}</button></div>
											</td>
											<td>
												<div id="clearButtonDiv"><button type="button" class="button" name="clearButton" id="clearButton" onclick="clearall();" style="margin-left: 20px;">{$mod.LBL_CLEAR_BUTTON}</button></div>
											</td>
											<td>
											<div id="nextButtonDiv"><button type="button" class="button" name="nextButton" id="nextButton" style="margin-left: 20px;" >{$mod.LBL_NEXT_BUTTON}</button></div>
											</td>
											<td>
											<div id="saveButtonDiv"><button type="button" class="button" name="saveButton" id="saveButton" onclick="saveAutoPopulateFieldsRecord();" style="margin-left: 20px;display: none;">{$mod.LBL_SAVE_BUTTON}</button></div>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<p></p>
			<table cellspacing="1" style="">
				<tbody>
					<tr>
						<td class="edit view" rowspan="2" width="100%">
							<div id="wiz_message"></div>
							<div id="wizard" class="wizard-unique-elem" style="width:1000px;">
								<div id="step1">
									<div class="template-panel">
										<div class="template-panel-container panel">
											<div class="template-container-full">
												<table width="100%" border="0" cellspacing="10" cellpadding="0">
													<tbody>
														<tr>
															<th colspan="4">
															<h4 class="header-4"><b>{$mod.LBL_SELECT_MODULE}</b></h4></th>
														</tr>
														<tr>
															<td nowrap width="20%" scope="row">
																<h2>{$mod.LBL_PRIMARY_MODULE}<span class="required">*</span></h2>
															</td>
															<td width="25%" style="padding-top:25px;">
																<select name="module" id="module" onchange="getRelateFieldModuleName('change')">
																	<option value="">{$mod.LBL_SELECT_MODULE}</option>
																	{foreach from=$MODULE_LIST key=k  item=module}
																	<option {if $SOURCE_MODULE eq $k} selected = "selected" {/if} value="{$k}">{$module}</option>
																	{/foreach}
																</select>
															</td>
														</tr>
														<tr rowspan="4">
															<td nowrap width="20%" scope="row">
																<b><h2>{$mod.LBL_STATUS}<span class="required">*</span></h2></b>
															</td>
															<td class="setvisibilityclass">
																<select name="status" id="status" title="">
																	<option label="{$mod.LBL_ACTIVE}" value="Active" selected="selected">{$mod.LBL_ACTIVE}</option>
																	<option label="{$mod.LBL_INACTIVE}" value="Inactive" {if $STATUS eq 'Inactive'} selected="selected" {/if}>{$mod.LBL_INACTIVE}</option>
																</select>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div id="step2" style="display:none;">
									<div class="template-panel">
										<div class="template-panel-container panel">
											<div class="template-container-full">
												<table width="100%" border="0" cellspacing="10" cellpadding="0">
													<tbody>
														<tr>
															<th colspan="4">
															<h4 class="header-4"><b>{$mod.LBL_FIELD_MAPPING}</b></h4></th>
														</tr>
														<tr>
															<td colspan="2">{$mod.LBL_SELECT_REALTE_FIELD}
																<select name="relatedFields" id="relatedFields" style="margin-left: 20px;"></select>
																<button type="button" class="button" name='addFieldMappingBlock' id='addFieldMappingBlock' style="margin-left: 20px;">{$mod.LBL_ADD_FIELD_MAPPING_BLOCK}</button>
															</td>
														</tr>
													</tbody>
												</table>
												<span id="conditionLinesSpanId">
													<script type="text/javascript" src="custom/modules/Administration/js/VIAutoPopulateFieldMapping.js?v={$RANDOM_NUMBER}"></script>
												</span>
												{$FIELD_MAPPING_CONTENT}
											</div>
										</div>
									</div>
								</div>
								<div id="step3" style="display:none;">
									<div class="template-panel">
										<div class="template-panel-container panel">
											<div class="template-container-full">
												<table width="100%" border="0" cellspacing="10" cellpadding="0">
													<tbody>
														<tr>
															<th colspan="4">
															<h4 class="header-4">{$mod.LBL_CALCULATE_FIELD}</h4></th>
														</tr>
														<tr>
															<td colspan="2">{$mod.LBL_SELECT_REALTE_FIELD}
																<select name="calculateRelatedFields" id="calculateRelatedFields" style="margin-left: 20px;"></select>
																<button type="button" class="button" name='addCalculateFieldBlock' id='addCalculateFieldBlock' style="margin-left: 20px;">{$mod.LBL_ADD_CALCULATE_FIELD_BLOCK}</button>
															</td>
														</tr>
													</tbody>
												</table>
												<span id="calculateConditionLinesSpan">
													<script type="text/javascript" src="custom/modules/Administration/js/VIAutoPopulateCalculateField.js?v={$RANDOM_NUMBER}"></script>
												</span>
												{$CALCULATE_FIELD_CONTENT}
											</div>
										</div>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
	</body>
</html>

{literal}
<script type="text/javascript">
	var recordId = "{/literal}{$RECORDID}{literal}";
	var listviewURL = "{/literal}{$LISTVIEW_URL}{literal}";
	var mod = {/literal}{$mod|@json_encode}{literal};
	var script   = document.createElement("script");
    script.type  = "text/javascript";
    script.src   = "custom/modules/Administration/js/VIAutoPopulateFieldsEditView.js?v="+Math.random();
    document.body.appendChild(script);
</script>
{/literal}