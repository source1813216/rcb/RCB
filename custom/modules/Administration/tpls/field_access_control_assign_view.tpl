{**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 *}
{if !$DISABLED_PLUGIN_LOGINSLIDER }
<script src="{sugar_getjspath file='modules/ACLRoles/ACLRoles.js'}"></script>
<div class="selectModuleHelp">
    <strong>{$MOD.LBL_FIELD_ACCESS_CONTROL_ACCESS}</strong>
</div>
<div class="fclSubDesc">Select role and module to see respective fields list</div>
<table width='100%'>
    <tr>
        <td colspan="2">
            <table width='100%' class='detail view' border='0' cellpadding=0 cellspacing = 1 id="CustomViewTable" >
                <tr>
                    <td class="selectModuleSection">
                        {$ROLE_DROPDOWN} {$no_users} {$no_roles}
                    </td>
                </tr>
            </table>
            
        </td>
    </tr>
    <tr>
       <td colspan="2">
          {if $ROLE_ID != ''} 
          
           <table width='100%' class='detail view' border='0' cellpadding=0 cellspacing = 1 id="CustomViewTable" >
               <tr>
                   <td class="selectModuleSection"><label class="selectModuleSectionLabel">Module: </label>
                       <span>
                           <select id="field_list_dd" class="flaGlobalDropdown" name="name" onchange="showAllFieldListByModule(this)" class="selectModuleDropdown">
                               <option value="" disabled selected>Select</option>
                               {foreach from=$CATEGORIES2 item="TYPES" key="CATEGORY_NAME"}
                                   {if $APP_LIST.moduleList[$CATEGORY_NAME] !='Users'  && $APP_LIST.moduleList[$CATEGORY_NAME] != ''}
                                                       <option value="{$ROLE.id}/{$CATEGORY_NAME}">{$APP_LIST.moduleList[$CATEGORY_NAME]}</option>

                                   {/if}
                               {/foreach}
                        </select>
                           <span id="field_data" class="noModuleSelected"></span>
                       </span>
                   </td>
               </tr>
           </table>
           {/if}
       </td>
   </tr>
</table>
           <div id='myDiv'> </div>
{else}
    <div style="color: #F11147;text-align: center;background: #FAD7EC;padding: 10px;margin: 3% auto;width: 70%;top: 50%;left: 0;right: 0;border: 1px solid #F8B3CC;font-size : 14px;">{$DISABLED_PLUGIN_LOGINSLIDER_MSG}</div>
{/if}
{literal}
    <script type="text/javascript">
        function getAclModuleList(el){
           var roleID = el.value;
           $.ajax({
                url: "index.php",
                type: "GET",
                data: {module: 'Administration', 
                    action: 'field_access_control_view',
                    roleID: roleID},
                success: function (html) {
                   $('body').html(html);
                }
            });
        }
        function showAllFieldListByModule(el) {
            if (el.value == '') {
                field_access_control_view.fieldACLDisplay('None');
            } else {
                var selectedVal = el.value;
                var splitVal = selectedVal.split('/');
                var role_id = splitVal[0];
                var categoryName = splitVal[1];
                field_access_control_view.showFieldACLView(role_id, categoryName);
            }
        }
        function displayMoreDetails(id) {
            if (this.document.getElementById(id).style.display == 'none') {
                this.document.getElementById(id).style.display = '';
                if (this.document.getElementById(id + "link") != undefined) {
                    this.document.getElementById(id + "link").style.display = 'none';
                }
                if (this.document.getElementById(id + "_anchor") != undefined)
                    this.document.getElementById(id + "_anchor").innerHTML = '<button type="button" class="btn buttonFieldNameInfo"><span class="glyphicon glyphicon-minus"></span></button>';
            } else {
                this.document.getElementById(id).style.display = 'none';
                if (this.document.getElementById(id + "link") != undefined) {
                    this.document.getElementById(id + "link").style.display = '';
                }
                if (this.document.getElementById(id + "_anchor") != undefined)
                    this.document.getElementById(id + "_anchor").innerHTML = '<button type="button" class="btn buttonFieldNameInfo"><span class="glyphicon glyphicon-plus"></span></button>';
            }
        }
        function displayMoreDetailsSugarCE(id) {
            if (this.document.getElementById(id).style.display == 'none') {
                this.document.getElementById(id).style.display = '';
                if (this.document.getElementById(id + "link") != undefined) {
                    this.document.getElementById(id + "link").style.display = 'none';
                }
                if (this.document.getElementById(id + "_anchor") != undefined)
                    this.document.getElementById(id + "_anchor").innerHTML = '<button type="button" class="__web-inspector-hide-shortcut__"><img class="fieldAccessPlusMinusButton" src="index.php?entryPoint=getImage&amp;themeName=Sugar5&amp;imageName=id-ff-remove-nobg.png"></button>';
            } else {
                this.document.getElementById(id).style.display = 'none';
                if (this.document.getElementById(id + "link") != undefined) {
                    this.document.getElementById(id + "link").style.display = '';
                }
                if (this.document.getElementById(id + "_anchor") != undefined)
                    this.document.getElementById(id + "_anchor").innerHTML = '<button type="button" class="__web-inspector-hide-shortcut__"><img class="fieldAccessPlusMinusButton" src="themes/default/images/id-ff-add.png?v=FkOi8fcLbiYeHIGQHkliXA"></button>';
            }
        }
        var field_access_control_view = function () {
            var saveMsg = '<span style="color:red;">Failed to save!</span>';
            return{showFieldACLView: function (role_id, role_module,successMsg = '') {
                   $.ajax({
                        url: "index.php",
                        type: "GET",
                        data: {module: 'Administration', 
                            action: 'fieldAccessControlHandler',
                            role_id: role_id, role_module: role_module, method: 'show_field_list_by_module'},
                        beforeSend: function () {
                            $('#successMsg').html("<span style='color:green;'><b>Saving..</b><img style=\'color:red;padding-left: 10px;vertical-align: middle;\' id=\'login_loader\' src= " + SUGAR.themes.loading_image + "></span>");
                        },
                        success: function (html) {
                           var decodedHtml = JSON.parse(html);
                           $('#myDiv').html(decodedHtml);
                           $('#successMsg').html(successMsg);
                           $("#successMsg").delay(3000).fadeOut();
                        }
                    });
                }, fieldACLSaveView: function (form_name) {
                   $.ajax({
                        url: "index.php",
                        type: "GET",
                        data: $('#'+form_name).serialize(),
                        success: function (html) {
                        var obj = $.parseJSON(html);
                        if(obj.success){
                            saveMsg = "<span style='color:green;'>Field's accesses has been saved successfully.</span>";
                        }
                        field_access_control_view.showFieldACLView(obj.role_id, obj.role_module,saveMsg);
                        }
                    });
                   
                },
                resetAccess: function (form_name) {
                   if(confirm("Are you sure want to reset all the field level accesses for this module?")){
                        $.ajax({
                             url: "index.php",
                             type: "GET",
                             data: $('#'+form_name).serialize(),
                             beforeSend: function () {
                                $('#successMsg').html("<span style='color:green;'><b>Saving..</b><img style=\'color:red;padding-left: 10px;vertical-align: middle;\' id=\'login_loader\' src= " + SUGAR.themes.loading_image + "></span>");
                             },
                             success: function (html) {
                             var obj = $.parseJSON(html);
                             saveMsg = "<span style='color:green;'>Field's accesses has been reset successfully.</span>";
                             field_access_control_view.showFieldACLView(obj.role_id, obj.role_module,saveMsg);
                             }
                         });
                    }
                },
                fieldACLAfterSave: function (o) {
                    eval(o.responseText);
                    field_access_control_view.showFieldACLView(result['role_id'], result['module']);
                }, fieldACLDisplay: function (o) {
                    if (typeof o !== 'undefined' && o == 'None') {
                        $('#myDiv').html('');
                        $('#field_list_dd').val('');
                    } else {
                        field_access_control_view.finalValue = '';
                        ajaxStatus.flashStatus('Done');
                        document.getElementById('field_data').setAttribute("style", '');
                        document.getElementById('field_data').innerHTML = o.responseText;
                    }
                }, fieldACLFailed: function () {
                    ajax.flashStatus('Could Not Connect');
                }, fieldACLToggleDisplay: function (id) {
                    if (field_access_control_view.finalValue != '' && typeof (field_access_control_view.finalValue) != 'undefined') {
                        field_access_control_view.showFalse(field_access_control_view.finalValue);
                    }
                    if (field_access_control_view.finalValue != id) {
                        field_access_control_view.showTrue(id);
                        field_access_control_view.finalValue = id;
                    } else {
                        field_access_control_view.finalValue = '';
                    }
                }, showFalse: function (id) {
                    document.getElementById(id).style.display = 'none';
                    document.getElementById(id + 'link').style.display = '';
                }, showTrue: function (id) {
                    document.getElementById(id).style.display = '';
                    document.getElementById(id + 'link').style.display = 'none';
                }};
        }();
    </script>    
{/literal}
<!--    Change By BC -->
