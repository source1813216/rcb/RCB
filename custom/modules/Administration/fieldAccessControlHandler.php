<?php

/**
 * The file used to manage actions for Field Level Access views 
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
require_once 'custom/biz_FLA/classes/Field_access_controller.php';
$oField_access_controller = new Field_access_controller();
if (!empty($_REQUEST['method'])) {
    $method = $_REQUEST['method'];
    switch ($method) {
        case 'validateLicence_FAC':
            $result = $oField_access_controller->validateLicence_FAC();
            break;
        case 'get_email_address_html':
            $result = $oField_access_controller->get_email_address_html();
            break;
        case 'check_if_access':
            $result = $oField_access_controller->check_if_access();
            break;
        case 'manageVisiblityFieldACL':
            $result = $oField_access_controller->manageVisiblityFieldACL();
            break;
        case 'show_field_list_by_module':
            $result = $oField_access_controller->show_field_list_by_module();
            break;
        case 'save_field_list_by_module':
            $result = $oField_access_controller->save_field_list_by_module();
            break;
        case 'reset_field_accesses':
            $result = $oField_access_controller->reset_field_accesses();
            break;
        default:
            break;
    }
} else {
    echo 'not valid action';
}
ob_clean();
echo json_encode($result);
die();