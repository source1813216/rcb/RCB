<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('include/MVC/View/SugarView.php');
include("custom/VIDynamicPanels/VIDynamicPanelsFunctions.php");
class Viewvi_dynamicpanelseditview extends SugarView {
    public function __construct() {
        parent::SugarView();
    }//end of function
    public function display() {
        global $mod_strings;

        if(isset($_REQUEST['records']) && !empty($_REQUEST['records'])){
            $recordId = $_REQUEST['records'];
        }else{
            $recordId = '';
        }//end of else
        
        $html = '';
        $fieldLine = '';
        $fieldColorLine = '';
        $smarty = new Sugar_Smarty();
        if($recordId != ''){
            $dynamicPanelsData = array();
            $selData = "SELECT * FROM vi_dynamic_panels WHERE dynamic_panels_id = '$recordId'";
            $selResultRow = $GLOBALS['db']->fetchOne($selData);
            
            //data
            $defaultPanel = explode(',',$selResultRow['default_panel_hide']);
            $defaultField = explode(',',$selResultRow['default_field_hide']);
            $panelHide = explode(',',$selResultRow['panel_hide']);
            $panelShow = explode(',',$selResultRow['panel_show']);
            $fieldHide = explode(',',$selResultRow['field_hide']);
            $fieldShow = explode(',',$selResultRow['field_show']);
            $fieldReadonly = explode(',',$selResultRow['field_readonly']);
            $fieldMandatory = explode(',',$selResultRow['field_mandatory']);
            $userRoles = explode(',',$selResultRow['role_id']);
            
            $dynamicPanelsData = array("NAME" => $selResultRow['name'],
                                      "PRIMARY_MODULE" => $selResultRow['primary_module'],
                                      "DEFAULTPANEL" => $defaultPanel,
                                      "DEFAULTFIELD" => $defaultField,
                                      "PANELHIDE" => $panelHide,
                                      "PANELSHOW" => $panelShow,
                                      "FIELDHIDE" => $fieldHide,
                                      "FIELDSHOW" => $fieldShow,
                                      "FIELDREADONLY" => $fieldReadonly,
                                      "FIELDMANDATORY" => $fieldMandatory,
                                      "USERTYPE" => $selResultRow['user_type'],
                                      "USERROLES" => $userRoles,
                                      "CONDITIONAL_OPERATOR" => $selResultRow['conditional_operator']);

            $smarty->assign("DYNAMIC_PANELS_DATA",$dynamicPanelsData);

            $html .= '<script src="custom/modules/Administration/js/VIDynamicPanelsConditionLines.js"></script>';
            $html .= "<table border='0' cellspacing='4' width='100%' id='aow_conditionLines'></table>";
            $html .= "<div style='padding-top: 10px; padding-bottom:10px;'>";
            $html .= "<input type=\"button\" tabindex=\"116\" class=\"button\" value=\"Add Condition\" id=\"btn_ConditionLine\" onclick=\"insertConditionLine()\" disabled/>";
            $html .= "</div>";

            if(isset($selResultRow['primary_module']) && !empty($selResultRow['primary_module'])){
                //condition line
                require_once("modules/AOW_WorkFlow/aow_utils.php");
                $html .= "<script>";
                $html .= "flow_rel_modules = \"".trim(preg_replace('/\s+/', ' ', getModuleRelationships($selResultRow['primary_module'])))."\";";
                $html .= "flow_module = \"".$selResultRow['primary_module']."\";";
                $html .= "document.getElementById('btn_ConditionLine').disabled = '';";
                
                $selCondition = "SELECT * FROM vi_dynamic_panels_condition 
                                          WHERE deleted = 0 
                                          AND dynamic_panels_id = '".$recordId."'";
                $selConditionResult = $GLOBALS['db']->query($selCondition);
                $fields = '';
                $fields .= "flow_fields = \"".trim(preg_replace('/\s+/', ' ',getFieldsData($selResultRow['primary_module'])))."\";";
                while ($selConditionRow = $GLOBALS['db']->fetchByAssoc($selConditionResult)) {
                    $html .= $fields;
                    $condition_item = json_encode($selConditionRow);
                    if($selConditionRow['value_type'] == 'Date'){
                        $conditionVal = json_encode(unserialize(base64_decode($selConditionRow['value'])));    
                    }else{
                        $conditionVal = json_encode($selConditionRow['value']);
                    }
                    $html .= "loadConditionLine(".$condition_item.",".$conditionVal.");";
                }//end of while
                
                $html .= $fields;
                $html .= "</script>";

                //fieldline
                //$fieldLine .= '<script src="custom/modules/Administration/VIDynamicPanelsConditionLines.js"></script>';
                $fieldLine .= "<table border='0' cellspacing='4' width='100%' id='fieldLines'></table>";
                $fieldLine .= "<div style='padding-top: 10px; padding-bottom:10px;'>";
                $fieldLine .= "<input type=\"button\" tabindex=\"116\" class=\"button\" value=\"Add Field\" id=\"btn_fieldLine\" onclick=\"insertFieldLine()\"/>";
                $fieldLine .= "</div>";
                $fieldLine .= "<script>";
                $fieldLine .= "flow_rel_modules = \"".trim(preg_replace('/\s+/', ' ', getModuleRelationships($selResultRow['primary_module'])))."\";";
                $fieldLine .= "flow_module = \"".$selResultRow['primary_module']."\";";

                $selFields = "SELECT * FROM vi_dynamic_panels_fields 
                                       WHERE deleted = 0 
                                       AND dynamic_panels_id = '".$recordId."'";
                $selFieldResult = $GLOBALS['db']->query($selFields);
                while ($selFieldRow = $GLOBALS['db']->fetchByAssoc($selFieldResult)) {
                    $fieldLine .= $fields;
                    $field_item = json_encode($selFieldRow);
                    $fieldLine .= "loadFieldLine(".$field_item.");";
                }//end of while
                
                $fieldLine .= $fields;
                $fieldLine .= "</script>";

                //fieldcolor line
                //$fieldColorLine .= '<script src="custom/modules/Administration/VIDynamicPanelsConditionLines.js"></script>';
                $fieldColorLine .= '<script src="custom/modules/Administration/js/VIDynamicPanelsJscolor.js"></script>';
                $fieldColorLine .= "<table border='0' cellspacing='4' width='100%' id='fieldColor'></table>";
                $fieldColorLine .= "<div style='padding-top: 10px; padding-bottom:10px;'>";
                $fieldColorLine .= "<input type=\"button\" tabindex=\"116\" class=\"button\" value=\"Add Field\" id=\"btn_fieldColor\" onclick=\"insertFieldColor()\"/>";
                $fieldColorLine .= "</div>";
                $fieldColorLine .= "<script>";
                $fieldColorLine .= "flow_rel_modules = \"".trim(preg_replace('/\s+/', ' ', getModuleRelationships($selResultRow['primary_module'])))."\";";
                
                $fieldColorLine .= "flow_module = \"".$selResultRow['primary_module']."\";";

                $selFieldsColor = "SELECT * FROM vi_dynamic_panels_fields_color 
                                            WHERE deleted = 0 
                                            AND dynamic_panels_id = '".$recordId."'";
                $selFieldsColorResult = $GLOBALS['db']->query($selFieldsColor);
                while ($selFieldColorRow = $GLOBALS['db']->fetchByAssoc($selFieldsColorResult)) {
                    $fieldColorLine .= $fields;
                    $field_color_item = json_encode($selFieldColorRow);
                    $fieldColorLine .= "loadFieldColorLine(".$field_color_item.");";
                }//end of while
                
                $fieldColorLine .= $fields;
                $fieldColorLine .= "</script>";
            }//end of if
            $smarty->assign('HTML',$html);
            $smarty->assign('FIELD_LINE',$fieldLine); 
            $smarty->assign("FIELD_COLOR_LINE",$fieldColorLine); 
        }
        //get module list
        require_once("modules/MySettings/TabController.php");
        $controller = new TabController();
        $tabs = $controller->get_tabs_system();
        foreach ($tabs[0] as $key=>$value){
            if($key != 'Home' && $key != 'Calendar' && $key != 'Campaigns' && $key != 'EmailTemplates' && $key != 'AOR_Reports' && $key != 'Emails' && $key != 'Calls'){
                $enabled[$key] = translate($key);
            }//end of if
        }//end of foreach
        
        $enabled['ProjectTask'] = translate('ProjectTask');
        
        asort($enabled);

        //data
        $smarty->assign('MODULELIST',$enabled);
        $smarty->assign('RECORDID',$recordId);
        $smarty->assign('MOD',$mod_strings);
        $smarty->display('custom/modules/Administration/tpl/vi_dynamicpanelseditview.tpl');
        parent::display();
    }
}