<?php
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class ViewFieldAccessControlConfiguration extends SugarView {

    function display() {
        global $current_user,$db;
        $readOnly ='';
        $hidden = '';
        // Check whether plugin is Enabled or not
        $is_enabled_plugin = true;
        $qry = "SELECT * FROM upgrade_history WHERE id_name = 'SuiteCRM_Field_Access_Control_Plugin' ";
        $result = $db->query($qry);
        while ($row = $db->fetchByAssoc($result)) {
            if ($row['enabled'] == 0) {
                $is_enabled_plugin = false;
            }
        }
        if($is_enabled_plugin){
        echo '<link rel="stylesheet" href="custom/include/css/fieldAccessControl.css" type="text/css">';
        if ($current_user->getPreference('suitecrm_version') != '') {
            $is_sugar = $current_user->getPreference('suitecrm_version');
        } else {
            $is_sugar = '';
        }
        require_once('modules/Administration/Administration.php');
        $administrationObj = new Administration();
        $administrationObj->retrieveSettings('FieldAccessControlPlugin');
        $LastValidation = $administrationObj->settings['FieldAccessControlPlugin_LastValidation'];
        $ModuleEnabled = $administrationObj->settings['FieldAccessControlPlugin_ModuleEnabled'];
        $licenseKey = (!empty($administrationObj->settings['FieldAccessControlPlugin_LicenceKey'])) ? $administrationObj->settings['FieldAccessControlPlugin_LicenceKey'] : "";
        if($LastValidation){
           $readOnly = 'readonly';
           $hidden = 'hidden';
        }
        $html = '<table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody><tr><td colspan="100"><h2><div class="moduleTitle">
                <h2>License Configuration </h2>
                <div class="clear"></div></div>
                </h2></td></tr>
                <tr><td colspan="100">
	            <div class="add_table" style="margin-bottom:5px">
		        <table id="ConfigureLogin" class="themeSettings edit view" style="margin-bottom:0px;" border="0" cellpadding="0" cellspacing="0">
			    <tbody>
			    <tr><th align="left" colspan="4" scope="row"><h4>Validate License</h4></th></tr>
			    <tr>
			    <td scope="row" nowrap="nowrap" style="width: 7%;"><label for="name_basic" class="alignLicenceLabels"> License Key: </label></td>
            	<td nowrap="nowrap" style="width: 15%;"><input name="licence_key" id="licence_key" class="licence_key" size="30" maxlength="255" value="' . $licenseKey . '" title="" accesskey="9" type="text" '.$readOnly.'></td>
            	<td nowrap="nowrap" style="width: 20%;"><input style="margin-left:20px;" title="Validate" id="Validate" class="button primary flaGlobalButton" onclick="validateLicence_FAC(this);" name="validate" value="Validate" type="button">&nbsp;<input title="Clear" id="clearkey" class="button primary flaGlobalButton" onclick="clearKey();" name="clear" value="Clear" type="button" '.$hidden.'></td>
            	<td nowrap="nowrap" style="width: 55%;">&nbsp;</td>
            	</tr></tbody></table></div>';
        $display_enable = "display:none;";
        $display_validate = "display:block;";
        if ($LastValidation == 1) {
            $display_enable = "display:block;";
            $display_validate = "display:none;";
        }
        $html .= '<table class="actionsContainer" style="' . $display_validate . '" border="0" cellpadding="1" cellspacing="1">
		        <tbody><tr><td>
				<input title="Back" accesskey="l" class="button flaGlobalButton" onclick="redirectToindex();" name="button" value="Back" type="button">
                                <input title="Back To Module Loader" accesskey="l" class="button flaGlobalButton" onclick="redirectToModuleLoader();" name="button" value="Back To Module Loader" type="button">
			    </td></tr>
            	</tbody></table>
                </td></tr></tbody></table>';
        $html .= '<div class="add_table" id="enableDiv" style="margin-bottom:5px;' . $display_enable . '">
		        <table id="ConfigureLogin" class="themeSettings edit view" style="margin-bottom:0px;" border="0" cellpadding="0" cellspacing="0">
			    <tbody>
			    <tr><th align="left" colspan="4" scope="row"><h4>Enable/Disable Module</h4></th></tr>
                <tr>
			    <td scope="row" nowrap="nowrap" style="width: 7%;"><label for="name_basic" class="alignLicenceLabels"> Enable/Disable: </label></td>
            	<td nowrap="nowrap" style="width: 15%;">
            	<select name="enable" id="enable" class="flaGlobalDropdown">';
        if ($ModuleEnabled == "1") {
            $html .= '<option value="1" selected="">Enable</option>
            	     <option value="0">Disable</option>';
        } else {
            $html .= '<option value="1">Enable</option>
            	     <option value="0" selected="">Disable</option>';
        }
        $html .= '</select>
            	</td>
            	<td nowrap="nowrap" style="width: 20%;">&nbsp;</td>
            	<td nowrap="nowrap" style="width: 55%;">&nbsp;</td>
            	</tr>
                </tbody></table>
	            </div>
	            <table class="actionsContainerEnableDiv" style="' . $display_enable . '" border="0" cellpadding="1" cellspacing="1">
		        <tbody><tr><td>
				<input title="Save" accesskey="a" class="button primary flaGlobalButton" onclick="enableFieldAccessControlPlugin();" name="button" value="Save" type="submit">
				<input title="Cancel" accesskey="l" class="button flaGlobalButton" onclick="redirectToindex();" name="button" value="Cancel" type="button">
			    </td></tr>
            	</tbody></table>
                </td></tr></tbody></table>';
        $html .= '<script type="text/javascript">
                    $("document").ready(function(){
                    $("#error_span").remove();
                    })
                    function clearKey(){
                        $("#licence_key").val("");
                        $("#error_span").html("");
                    }
                    function redirectToindex(){
                        location.href = "index.php?module=Administration&action=index";
                    }
                    function redirectToModuleLoader(){
                        location.href = "index.php?module=Administration&action=UpgradeWizard&view=module";
                    }
                   function validateLicence_FAC(element){
                    $("#error_span").remove();
                    var key = $("#licence_key");
                        if(key.val().trim() == ""){
                            $("#clearkey").after("<span style=\'color:red;padding-left: 10px;\' id=\'error_span\'>Please enter license key.</span>")
                            key.focus();
                            return false;
                        }else{
                             var trimmedKey = $("#licence_key").val().trim();
                             $.ajax({
                                url:"index.php?module=Administration&action=fieldAccessControlHandler&method=validateLicence_FAC",
                                type:"POST",
                                data:{"k": trimmedKey},
                                beforeSend : function(){
                                    $("#clearkey").after("<img style=\'color:red;padding-left: 10px;vertical-align: middle;\' id=\'login_loader\' src= "+SUGAR.themes.loading_image+">");
                                    $(element).attr("disabled","disabled");
                                },
                                complete : function(){
                                    $("#login_loader").remove();
                                    $(element).removeAttr("disabled");
                                },
                                success:function(result){
                                result  = JSON.parse(result);
                                $("#licence_key").val(trimmedKey);
                                $("#login_loader").remove();
                                $("#enable option[value=0]").prop("selected", true);
                                        if(result["suc"] == 1){
                                            $("#clearkey").css("display","none");
                                            $("#licence_key").attr("readonly","readonly");
                                            $("#enableDiv").show();
                                            $(".actionsContainerEnableDiv").show();
                                            $(".actionsContainer").hide();
                                            $("#clearkey").after("<span style=\'color:green;padding-left: 10px;\' id=\'error_span\'>License validated successfully.</span>");
                                        }else{
                                            $("#clearkey").css("display","inline");
                                            $("#licence_key").removeAttr("readonly");
                                            if(!result["msg"])
                                            {
                                                result["msg"] = "There seems some error while validating your license!";
                                            }
                                            $("#clearkey").after("<span style=\'color:red;padding-left: 10px;\' id=\'error_span\'>"+result["msg"]+"</span>");
                                            $("#enableDiv").hide();
                                            $(".actionsContainerEnableDiv").hide();
                                            $(".actionsContainer").show();
                                        }
                                    }
                                });
                            }
                        }
                    function enableFieldAccessControlPlugin(){
                            var enabled = $("#enable").val();
                             $.ajax({
                                url:"index.php?module=Administration&action=fieldAccessControlHandler&method=manageVisiblityFieldACL",
                                data :{"enabled" : enabled},
                                type:"POST",
                                success:function(result){
                                    if(enabled == "1"){
                                    alert("Module enabled successfully.");
                                    }else{
                                    alert("Module disabled successfully.");
                                    }
                                    location.href = "index.php?module=Administration&action=index";
                                  }
                                });
                        }
                  </script>';
        }else{
            $html = '<div style="color: #F11147;text-align: center;background: #FAD7EC;padding: 10px;margin: 3% auto;width: 70%;top: 50%;left: 0;right: 0;border: 1px solid #F8B3CC;font-size : 14px;">The Plugin Is Disabled. Please Contact Your Administrator.</div>';
        }
        parent::display();
        echo $html;
    }
}
?>

