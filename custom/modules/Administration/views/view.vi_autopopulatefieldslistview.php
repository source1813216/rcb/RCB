<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php");
class Viewvi_autopopulatefieldslistview extends SugarView {
	public function __construct() {
		parent::SugarView();
	}//end of function

	public function display() {
		global $mod_strings, $theme;

		//get autopopulate fields configuration
		$autoPopulateConfigData = getAllModuleAutoPopulateFieldsConfigData();

		$url = "https://suitehelp.varianceinfotech.com";
		$helpBoxContent = getAutoPopulateFieldsHelpBoxHtml($url);

		$editviewUrl = "index.php?module=Administration&action=vi_autopopulatefieldseditview";

		$smarty = new Sugar_Smarty();
  		$smarty->assign("MOD",$mod_strings);
  		$smarty->assign("THEME",$theme);
  		$smarty->assign('HELP_BOX_CONTENT',$helpBoxContent);
  		$smarty->assign("AUTO_POPULATE_FIELDS_CONFIG",$autoPopulateConfigData);
  		$smarty->assign("EDITVIEW_URL",$editviewUrl);
  		$smarty->display('custom/modules/Administration/tpls/vi_autopopulatefieldslistview.tpl');
	}//end of function
}//end of class
?>