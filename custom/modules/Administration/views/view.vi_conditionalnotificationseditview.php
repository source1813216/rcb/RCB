<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('include/MVC/View/SugarView.php');
class Viewvi_conditionalnotificationseditview extends SugarView {

    public function getFields($module) {
        require_once('include/utils.php');
        $bean = BeanFactory::newBean($module);
        $field = $bean->getFieldDefinitions();

        $addressFieldData = array();
        foreach($field as $value){
            if($value['type'] == 'varchar'){
                if(isset($value['group'])){
                    $fieldName = $value['name'];
                    $fieldLabel = translate($value['vname'],$module);
                    if(strpos($fieldLabel, ':')){
                        $fieldLabel = substr_replace($fieldLabel, "", -1);
                    }//end of if
                $addressFieldData[$fieldName] = $fieldLabel;
                }//end of if
            }//end of if
        }//end of foreach

        unset($addressFieldData['email1']);
        unset($addressFieldData['email2']);

        $editViewFieldData = $this->getEditDetailViewFields('editview',$module);
        $detailViewFieldData = $this->getEditDetailViewFields('detailview',$module);

        $value = '';
        $arrayMerge = array_merge($editViewFieldData,$addressFieldData,$detailViewFieldData); //merge array
        asort($arrayMerge);
        return get_select_options_with_id($arrayMerge,$value);
    }//end of function
    
    public function display() {
        global $mod_strings;
        if(isset($_REQUEST['records'])){
            $recordId = $_REQUEST['records'];
        }else{
            $recordId = '';
        }
        $html = '';
        $smarty = new Sugar_Smarty();
        if($recordId != ''){
            $selData = "SELECT * FROM vi_conditional_notifications WHERE conditional_notifications_id = '$recordId'";
            $selResultRow = $GLOBALS['db']->fetchOne($selData);

            $conditionNotificationsData = array('MODULENAME' => $selResultRow['module_name'],
                                                'DESCRIPTION' => $selResultRow['description'],
                                                'ACTIONTITLE' => $selResultRow['action_title'],
                                                'NOTIFICATIONONEDIT' => $selResultRow['notification_on_edit'],
                                                'NOTIFICATIONONVIEW' => $selResultRow['notification_on_view'],
                                                'NOTIFICATIONONSAVE' => $selResultRow['notification_on_save'],
                                                'NOTIFICATIONNOTALLOWSAVE' => $selResultRow['notification_not_allow_save'],
                                                'NOTIFICATIONCONFIRMATIONDIALOG' => $selResultRow['notification_confirmation_dialog'],
                                                'NOTIFICATIONONDUPLICATE' => $selResultRow['notification_on_duplicate'],
                                                'BODYHTML' => $selResultRow['body_html'],
                                                'CONDITIONALOPERATOR' => $selResultRow['conditional_operator']);

            //condition lines
            $html .= '<script src="custom/modules/Administration/js/VIConditionalNotificationsConditionLines.js"></script>';
            $html .= "<table border='0' cellspacing='4' width='100%' id='aow_conditionLines'></table>";
            $html .= "<div style='padding-top: 10px; padding-bottom:10px;'>";
            $html .= "<input type=\"button\" tabindex=\"116\" class=\"button\" value=\"Add Condition\" id=\"btn_ConditionLine\" onclick=\"insertConditionLine()\" disabled/>";
            $html .= "</div>";

            if(isset($selResultRow['module_name']) && $selResultRow['module_name']!= ''){
                require_once("modules/AOW_WorkFlow/aow_utils.php");
                $html .= "<script>";
                $html .= "flow_rel_modules = \"".trim(preg_replace('/\s+/', ' ', getModuleRelationships($selResultRow['module_name'])))."\";";
                $html .= "flow_module = \"".$selResultRow['module_name']."\";";
                $html .= "document.getElementById('btn_ConditionLine').disabled = '';";
                $selCondition = "SELECT * FROM vi_conditional_notifications_conditions 
                                          WHERE deleted = 0 
                                          AND conditional_notifications_id = '".$recordId."'";
                $selConditionResult = $GLOBALS['db']->query($selCondition);
                $fields = '';
                $fields .= "flow_fields = \"".trim(preg_replace('/\s+/', ' ',$this->getFields($selResultRow['module_name'])))."\";";
                while ($selConditionRow = $GLOBALS['db']->fetchByAssoc($selConditionResult)) {
                    $html .= $fields;
                    $condition_item = json_encode($selConditionRow);
                    if($selConditionRow['value_type'] == 'Date'){
                        $conditionVal = json_encode(unserialize(base64_decode($selConditionRow['value'])));    
                    }else{
                        $conditionVal = json_encode($selConditionRow['value']);
                    }
                    $html .= "loadConditionLine(".$condition_item.",".$conditionVal.");";
                }
                $html .= $fields;
                $html .= "</script>";
            }

            $smarty->assign("CONDITION_NOTIFICATIONS_DATA",$conditionNotificationsData);
        }
        //get module list
        require_once("modules/MySettings/TabController.php");
        $controller = new TabController();
        $tabs = $controller->get_tabs_system();
        foreach ($tabs[0] as $key=>$value){
            $enabled[$key] = translate($key);
        }

        //unset module 
        $unsetModuleArray = array("Home","Calendar","AOR_Reports","Emails","Campaigns","Calls","Documents","EmailTemplates","Meetings","AOS_PDF_Templates","AM_ProjectTemplates","Spots","AOW_WorkFlow");
        foreach ($unsetModuleArray as $key => $value) {
            unset($enabled[$value]);
        }//end of forech
        asort($enabled);

        $smarty->assign('MODULELIST',$enabled);
        $smarty->assign('HTML',$html);
        $smarty->assign('RECORDID',$recordId);
        $smarty->assign('MOD',$mod_strings);
        $smarty->display('custom/modules/Administration/tpl/vi_conditionalnotificationseditview.tpl');
        parent::display();
        $this->displayTMCE();
    }

    public function getEditDetailViewFields($view,$module){
        require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
        $view_array = ParserFactory::getParser($view,$module);
        $panelArray = $view_array->_viewdefs['panels'];//editview panels
        $bean = BeanFactory::newBean($module);
        $field = $bean->getFieldDefinitions();

        //editview fields
        $editViewFieldArray = array();
        foreach ($panelArray as $key => $value) {
            foreach ($value as $keys => $values) {
                $editViewFieldArray[] = $values;
            }//end of foreach
        }//end of foreach
    
        $data = array('' => '--None--');
        foreach($editViewFieldArray as $key => $value) {
            foreach($value as $k => $v) {
                if(array_key_exists($v, $field)) {
                    require_once('include/utils.php');
                    $fieldLabel = translate($field[$v]['vname'], $module);
                    if(strpos($fieldLabel, ':')){
                        $fieldLabel = substr_replace($fieldLabel, "", -1);
                    }//end of if
                    $fieldName = $v;
                    $data[$fieldName] = $fieldLabel;
                }//end of if 
            }//end of foreach
        }//end of foreach

        //unset fields
        $unsetData = array('sample','insert_fields','update_text','case_update_form','aop_case_updates_threaded','internal','survey_questions_display','line_items','email1','email2','suggestion_box','filename','product_image','configurationGUI','invite_templates','action_lines','condition_lines','survey_url_display','reminders','pdffooter','pdffooter');
        foreach ($unsetData as $key => $value) {
            unset($data[$value]);
        }//end of foreach

        if($module == 'jjwg_Maps' || $module == 'Meetings' || $module == 'Notes' || $module == 'Tasks' || $module == 'Calls'){
            unset($data['parent_name']);
        }//end of if

        if($module == 'AOS_PDF_Templates' || $module == 'AOK_KnowledgeBase' || $module == 'Cases'){
            unset($data['description']);
        }
        if($module == 'AOS_Invoices'){
            unset($data['number']);
        }
        return $data;
    }//end of function

    function displayTMCE(){
        require_once("include/SugarTinyMCE.php");
        global $locale;

        $tiny = new SugarTinyMCE();
        $tinyMCE = $tiny->getConfig();

        $js =<<<JS
        <script language="javascript" type="text/javascript">
        $tinyMCE
        var df = '{$locale->getPrecedentPreference('default_date_format')}';

        tinyMCE.init({
            theme : "advanced",
            theme_advanced_toolbar_align : "left",
            mode: "exact",
            elements : "body_html",
            theme_advanced_toolbar_location : "top",
            theme_advanced_buttons1: "code,help,separator,bold,italic,underline,strikethrough,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,forecolor,backcolor,separator,styleprops,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,selectall,separator,search,replace,separator,bullist,numlist,separator,outdent,indent,separator,ltr,rtl,separator,undo,redo,separator, link,unlink,anchor,separator,sub,sup,separator,charmap,visualaid",
            theme_advanced_buttons3: "tablecontrols,separator,advhr,hr,removeformat,separator,insertdate,pagebreak",
            theme_advanced_fonts:"Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Helvetica Neu=helveticaneue,sans-serif;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats",
            plugins : "advhr,insertdatetime,table,paste,searchreplace,directionality,style,pagebreak",
            height:"auto",
            width: "100%",
            inline_styles : true,
            directionality : "ltr",
            remove_redundant_brs : true,
            entity_encoding: 'raw',
            cleanup_on_startup : true,
            strict_loading_mode : true,
            convert_urls : false,
            plugin_insertdate_dateFormat : '{DATE '+df+'}',
            pagebreak_separator : "<pagebreak />",
            extended_valid_elements : "textblock",
            custom_elements: "textblock",
        });
        </script>
JS;
        echo $js;
    }
    
}
?>