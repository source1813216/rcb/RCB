<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('include/MVC/View/SugarView.php');

class Viewvi_dynamicpanelslistview extends SugarView {
    public function __construct() {
        parent::SugarView();
    }//end of function
    public function display() {
        global $theme,$mod_strings,$current_user;

        $smarty = new Sugar_Smarty();
    	
        $editviewUrl="index.php?module=Administration&action=vi_dynamicpanelseditview";
    	
        $selDynamicPanels = "SELECT * FROM vi_dynamic_panels WHERE deleted = 0 ORDER BY date_created ASC";
        $selDynamicPanelsResult = $GLOBALS['db']->query($selDynamicPanels);
        $numRow = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selDynamicPanels));
        $dynamicPanelsData = array();
        if(!empty($numRow)){
            while($selDynamicPanelsRow=$GLOBALS['db']->fetchByAssoc($selDynamicPanelsResult)){
                $defaultPanelHide = array();
                $explodDefaultPanel = explode(',',$selDynamicPanelsRow['default_panel_hide']);
                if(!empty($explodDefaultPanel)){
                    foreach($explodDefaultPanel as $value){
                        if($value != ''){
                            $defaultPanelHide[] = translate($value,$selDynamicPanelsRow['primary_module']);
                        }//end of if
                    }//end of foreach
                }//end of if

                $explodDefaultField = explode(',',$selDynamicPanelsRow['default_field_hide']);
                $bean = BeanFactory::newBean($selDynamicPanelsRow['primary_module']);

                $defaultField = array();
                
                if(!empty($explodDefaultField)){
                    foreach ($explodDefaultField as $key => $value) {
                        if($value != ''){
                            $fieldData = $bean->field_defs[$value];
                            if($fieldData['name'] == 'flow_run_on'){
                                $fieldLabel = translate('LBL_FLOW_RUN_ON',$selDynamicPanelsRow['primary_module']);
                            }else{
                                $fieldLabel = translate($fieldData['vname'],$selDynamicPanelsRow['primary_module']);
                            }//end of else
                            
                            $lastChar = substr($fieldLabel, -1);
                            if($lastChar == ':'){
                                $fieldLabel = substr_replace($fieldLabel, "", -1);
                            }
                            
                            $defaultField[] = $fieldLabel;     
                        }//end of if
                    }//end of foreach
                }//end of if

                if($selDynamicPanelsRow['user_type'] == 'AllUsers'){
                    $userType = 'All Users';
                }else if($selDynamicPanelsRow['user_type'] == 'RegularUser'){
                    $userType =  'Regular User';
                }else if($selDynamicPanelsRow['user_type'] == 'Administrator'){
                    $userType = 'System Administrator User';
                }//end of else if

                $userRole = array();
                $explodUserRoles = explode(',',$selDynamicPanelsRow['role_id']);
                foreach($explodUserRoles as $value) { 
                    $aclRoleBean = BeanFactory::getBean("ACLRoles",$value);
                    $userRole[] = $aclRoleBean->name;
                }//end of foreach

                $dateFormat = $current_user->getPreference('datef');
                $dateEntered = date($dateFormat.' H:i',strtotime($selDynamicPanelsRow['date_created']));

                $dynamicPanelsData[] = array('dynamicPanelsId' => $selDynamicPanelsRow['dynamic_panels_id'],
                                            'name' => $selDynamicPanelsRow['name'],
                                            'primaryModule' => translate($selDynamicPanelsRow['primary_module']),
                                            'defaultPanelHide' => $defaultPanelHide,
                                            'defaultField' => $defaultField,
                                            'userType' => $userType,
                                            'userRole' => $userRole,
                                            'dateEntered' => $dateEntered);  
            }//end of while
        }//end of if
        $smarty->assign("EDITVIEWURL",$editviewUrl);
        $smarty->assign("THEME",$theme);
        $smarty->assign("MOD",$mod_strings);
        $smarty->assign("DYNAMIC_PANELS_DATA",$dynamicPanelsData);
        
        parent::display();
        $smarty->display('custom/modules/Administration/tpl/vi_dynamicpanelslistview.tpl');  
    }//end of display
}//end of class
?>