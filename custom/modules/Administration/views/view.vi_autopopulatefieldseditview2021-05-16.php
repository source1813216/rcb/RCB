<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php");
class Viewvi_autopopulatefieldseditview extends SugarView {
    public function __construct() {
        parent::SugarView();
    }//end of function

    public function display() {
        global $mod_strings;
        $smarty = new Sugar_Smarty();

        $allModuleList = getModuleList(); //get all modules

        $randomNumber = rand();

        $listviewURL = "index.php?module=Administration&action=vi_autopopulatefieldslistview";

        if(isset($_REQUEST['records'])){
            $recordId = $_REQUEST['records'];
            //get data
            $where = array('autopopulatefields_id' => "'".$recordId."'");
            $selAutoPopulateData = getAutoPopulateFieldsRecord('vi_autopopulatefields',$fieldNames = array(),$where);
            $selAutoPopulateDataRow = $GLOBALS['db']->fetchOne($selAutoPopulateData);
            
            $sourceModule = $selAutoPopulateDataRow['module'];
            $status = $selAutoPopulateDataRow['status'];

            $fieldMappingContent = getAutoPopulateFieldsFieldMappingContent($recordId,$sourceModule);

            $calculateFieldContent = getAutoPopulateFieldsCalculateFieldContent($recordId,$sourceModule);

            $smarty->assign('SOURCE_MODULE',$sourceModule);
            $smarty->assign('STATUS',$status);
            $smarty->assign('RECORDID',$recordId);
            $smarty->assign('FIELD_MAPPING_CONTENT',$fieldMappingContent);
            $smarty->assign('CALCULATE_FIELD_CONTENT',$calculateFieldContent);
        }//end of if

        $smarty->assign('mod',$mod_strings);
        $smarty->assign('MODULE_LIST',$allModuleList);
        $smarty->assign("RANDOM_NUMBER",$randomNumber);
        $smarty->assign('LISTVIEW_URL',$listviewURL);
        $smarty->display('custom/modules/Administration/tpls/vi_autopopulatefieldseditview.tpl');
    }//end of function
}//end of class
?>