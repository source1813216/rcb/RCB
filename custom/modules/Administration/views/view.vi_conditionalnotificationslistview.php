<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("custom/VIConditionalNotifications/VIConditionalNotificationsFunctions.php");
require_once('include/MVC/View/SugarView.php');
class Viewvi_conditionalnotificationslistview extends SugarView {
    public function __construct() {
        parent::SugarView();
    }
    public function display() {
        global $theme, $mod_strings;
        //get module list
        require_once("modules/MySettings/TabController.php");
        $controller = new TabController();
        $tabs = $controller->get_tabs_system();
        foreach ($tabs[0] as $key=>$value) {
            $enabled[$key] = translate($key);
        }//end of foreach

        //unset module 
        $unsetModuleArray = array("Home","Calendar","AOR_Reports","Emails","Campaigns","Calls","Documents","EmailTemplates","Meetings","AOS_PDF_Templates","AM_ProjectTemplates","Spots","AOW_WorkFlow");
        foreach ($unsetModuleArray as $key => $value) {
            unset($enabled[$value]);
        }//end of forech
        asort($enabled);

        //get conditional notifications data
        $selCondtionalNotifications = "SELECT * FROM vi_conditional_notifications WHERE deleted = 0";
        $selCondtionalNotificationsResult = $GLOBALS['db']->query($selCondtionalNotifications);
        $selCondtionalNotificationsResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selCondtionalNotifications));
        $recordCount = 0;
        $conditionalNotificationsData = array();
        if(!empty($selCondtionalNotificationsResultData)){
            while($selDataRow = $GLOBALS['db']->fetchByAssoc($selCondtionalNotificationsResult)){
                $conditionalNotificationsData[] = array(
                                                        'conditionalNotificationsId' => $selDataRow['conditional_notifications_id'],
                                                        'moduleLabel' => translate($selDataRow['module_name']),
                                                        'description' => $selDataRow['description']);

                $recordCount++;
            }//end of while
        }//end of if

        $url = "https://suitehelp.varianceinfotech.com";

        $helpBoxContent = getConditionalNotificationsHelpBoxHtml($url);

        $smarty = new Sugar_Smarty();
        $editviewUrl = "index.php?module=Administration&action=vi_conditionalnotificationseditview";
        $smarty->assign("EDITVIEW_URL",$editviewUrl);
        $smarty->assign('MODULELIST',$enabled);
        $smarty->assign("THEME",$theme);
        $smarty->assign("MOD",$mod_strings);
        $smarty->assign("RECORDCOUNT",$recordCount);
        $smarty->assign("CONDITIONAL_NOTIFICATIONS_DATA",$conditionalNotificationsData);
        $smarty->assign("HELP_BOX_CONTENT",$helpBoxContent);
        parent::display();
        $smarty->display('custom/modules/Administration/tpl/vi_conditionalnotificationslistview.tpl');
        
    }//end of display
}//end of class
?>