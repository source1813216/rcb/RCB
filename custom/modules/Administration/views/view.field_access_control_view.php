<?php

/**
 * Send All submission Data To report.tpl file
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Original Author Biztech Co.
 */
require_once('include/MVC/View/SugarView.php');
require_once("include/Sugar_Smarty.php");
require_once('custom/modules/Administration/field_access_control_assign_view.php');
//require_once("include/SugarSmarty/Sugar_Smarty.php");

/**
 * generate report data and pass to report.tpl file
 *
 * @author     Original Author Biztech Co
 */
class Viewfield_access_control_view extends SugarView {

    function __construct() {
        parent::SugarView();
    }

    function display() {
        global $db,$app_list_strings;
        $noUserExists = '';
        $noRolesCreated = '';
        $roleID = '';
        if (isset($_REQUEST['roleID'])) {
            $roleID = $_REQUEST['roleID'];
        }
        echo '<link rel="stylesheet" href="custom/include/css/fieldAccessControl.css" type="text/css">';
        $getRolesList = $db->query("Select id,name from acl_roles where deleted = 0");
        $roleDropdown = "<label>Role: </label>";
        $roleDropdown .= "<select class='fieldAccessControlDropdown flaGlobalDropdown' onchange = 'getAclModuleList(this)'><option value='' selected disabled>Select</option>";
        if ($getRolesList->num_rows > 0) {
            while ($rolesList = $db->fetchByAssoc($getRolesList)) {
                $selected = ($rolesList['id'] == $roleID) ? 'selected' : '';
                $roleDropdown .= "<option value='{$rolesList['id']}' {$selected}>{$rolesList['name']}</option>";
            }
        } else {
            $noRolesCreated = "No roles found! <a href='index.php?module=ACLRoles&action=EditView' title='No roles found. Please create to move forward' target='_blank' class='noModuleSelected'>Click here</a> to create.";
        }
        $roleDropdown .= "</select>";
        if (isset($roleID) && !empty($roleID)) {
            $getUsersRolesList = $db->query("Select user_id from acl_roles_users where deleted = 0 and role_id = '{$roleID}' ");
            $getUsersSecurityGroupList = $db->query("Select securitygroup_id from securitygroups_acl_roles where deleted = 0 and role_id = '{$roleID}' ");
            if ($getUsersRolesList->num_rows == 0 && $getUsersSecurityGroupList->num_rows == 0) {
                $noUserExists = "No users assigned to this role. <a href='index.php?module=ACLRoles&action=DetailView&record={$roleID}' title='No assigned user found to this role' target='_blank' class='noModuleSelected'>Click here</a> to assign.";
            }
            $role_obj = new ACLRole();
            $role_obj->retrieve($roleID);
            $acl_categories = $role_obj->getRoleActions($roleID);
            /* Show Only Enabled Modules */
            $disabled = array();
            foreach ($acl_categories as $cat_name => $category) {
                foreach ($category as $type_name => $type) {
                    foreach ($type as $act_name => $action) {
                        if ($type_name == 'module') {
                            if ($act_name != 'aclaccess' && $acl_categories[$cat_name]['module']['access']['aclaccess'] == ACL_ALLOW_DISABLED) {
                                $disabled[] = $cat_name;
                            }
                        }
                    }
                }
            }
            foreach ($disabled as $cat_name) {
                unset($acl_categories[$cat_name]);
            }
            /* Show Only Enabled Modules */
            $acl_action_obj = new ACLAction();
            $acl_names = $acl_action_obj->setupCategoriesMatrix($acl_categories);
            $acl_categories_other = array();
            $acl_categories_other = $acl_categories;
            $acl_disabled_categories = array(
                "KBDocuments", "Campaigns", "Forecasts", "ForecastSchedule",
                "Emails", "EmailTemplates", "EmailMarketing", "AOR_Reports","AOD_Index"
                ,"AOD_IndexEvent","EAPM","Alerts","OutboundEmailAccounts","AOW_Processed",
                "Calls_Reschedule","AOP_Case_Events","AOP_Case_Updates","TemplateSectionLine",
                "SurveyQuestionOptions","SurveyQuestionResponses","SurveyQuestions","SurveyResponses","Users");
            foreach ($acl_disabled_categories as $v) {
                if (isset($acl_categories_other[$v])) {
                    unset($acl_categories_other[$v]);
                }
            }
            $this->ss->assign('ROLE', $role_obj->toArray());
            $this->ss->assign('CATEGORIES2', $acl_categories_other);
            $this->ss->assign('APP_LIST', $app_list_strings);
            $this->ss->assign('no_users', $noUserExists);
        }
        $disabled_plugin_loginslider = false;
        $checkLoginSubscription = validateFieldACLSubscription();
        if (!$checkLoginSubscription['success']) {
            $disabled_plugin_loginslider = true;
        }
        $this->ss->assign('ROLE_DROPDOWN', $roleDropdown);
        $this->ss->assign('ROLE_ID', $roleID);
        $this->ss->assign('DISABLED_PLUGIN_LOGINSLIDER', $disabled_plugin_loginslider);
        $this->ss->assign('DISABLED_PLUGIN_LOGINSLIDER_MSG', $checkLoginSubscription['message']);
        $this->ss->assign('no_roles', $noRolesCreated);
        $this->ss->display('custom/modules/Administration/tpls/field_access_control_assign_view.tpl');
        parent::display();
    }

}
