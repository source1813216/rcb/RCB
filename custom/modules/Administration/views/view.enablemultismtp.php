<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('include/MVC/View/SugarView.php');
class Viewenablemultismtp extends SugarView {
    public function __construct() {
        parent::SugarView();
    } //end of construct
    public function preDisplay() {
    }//end of preDisplay
    public function display() {
      global $mod_strings;
	    $smarty = new Sugar_Smarty();
	    $selSettings = "SELECT * FROM vi_multiple_smtp_settings";
      $result = $GLOBALS['db']->query($selSettings);
      $resultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selSettings));
      if(!empty($resultData)) {
    		$sel_data = "SELECT enable FROM vi_multiple_smtp_settings";
    		$resultData = $GLOBALS['db']->query($sel_data);
    		$row = $GLOBALS['db']->fetchByAssoc($resultData);
    		$enable = $row['enable'];
	      $smarty->assign("enable",$enable);
	    }//end of if
      $smarty->assign("MOD",$mod_strings);
      parent::display();
      $smarty->display('custom/modules/Administration/tpl/enablemultismtp.tpl');
    }//end of display
}//end of class
?>