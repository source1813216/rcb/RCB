/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$('#back_button').hide();
$('#btn_save').hide();

$('#btn_next').on('click',function(){
    var wizardCurrentStep = $('.nav-steps.selected').attr('data-nav-step');
    if(wizardCurrentStep == 1){
        var moduleName = $('#flow_module').val();
        var name = $('#dynamic_panels_name').val();
        var userType = $('#user_type').val();
        var userRoles = $('#user_roles').val();
        if(moduleName == '' || name == '' || userType == '' || userRoles == null){
            label = SUGAR.language.get('Administration','LBL_REQUIRED_VALIDATION');
            alert(label);
        }else{
            $('#step1').css('display','none');
            $('#step2').css('display','block');
            $('#step3').css('display','none');
            $('#step4').css('display','none');
            $('#back_button').show();
            $('#btn_save').hide();
            $('#btn_next').show();
            $('#nav_step1').removeClass('selected');
            $('#nav_step2').addClass('selected');
            $('#nav_step3').removeClass('selected');
            $('#nav_step4').removeClass('selected');  
        }//end of else
    }else if(wizardCurrentStep == 2){
        var rowc = $("#aow_conditionLines > tbody > tr").length;
        var checkConditionValue;
        var conditionValue = '';
        for(var i=0;i<rowc;i++){
            if($('#product_line'+i).is(':visible')){
                var operator = document.getElementById('aow_conditions_operator['+i+']').value;
                var field = document.getElementById('aow_conditions_field'+i).value;
                var valueType = document.getElementById('aow_conditions_value_type['+i+']').value;
                if(valueType == 'Date'){
                    param0 = document.getElementById('aow_conditions_value['+i+'][0]').value;
                    param1 = document.getElementById('aow_conditions_value['+i+'][1]').value;
                    param2 = document.getElementById('aow_conditions_value['+i+'][2]').value;
                    param3 = document.getElementById('aow_conditions_value['+i+'][3]').value;
                    if(param1 == 'now'){
                        param2 = '0';
                        param3 = '0';
                    }
                    if(param0 == '' || param1 == '' || param2 == '' || param3 == ''){
                        conditionValue = '';
                    }else{
                        conditionValue = '0';
                    }
                }else{
                    var myTDs = $('td #aow_conditions_fieldInput'+i).closest('tr').find("td input.datetimecombo_date").length;
                    if(field == 'currency_id'){
                        conditionValue = document.getElementById('aow_conditions_value['+i+']_select').value;
                    }else if(myTDs > 0){
                        conditionValue = $('td #aow_conditions_fieldInput'+i).closest('tr').find("td input.datetimecombo_date").val();
                    }else{
                        conditionValue = document.getElementById('aow_conditions_value['+i+']').value;    
                    }
                }
                if(operator != 'is_null' && operator != 'is_not_null'){ 
                    if(conditionValue == ''){
                        checkConditionValue = 0;
                    }//end of if
                }//end of if
            }//end of if
        }//end of for

        if(checkConditionValue == 0){
            label = SUGAR.language.get('Administration','LBL_CONDITIONS_VALIDATION');
            alert(label);
        }else{
            $('#step1').css('display','none');
            $('#step2').css('display','none');
            $('#step3').css('display','block');
            $('#step4').css('display','none');
            $('#back_button').show();
            $('#btn_save').hide();
            $('#btn_next').show();
            $('#nav_step1').removeClass('selected');
            $('#nav_step2').removeClass('selected');
            $('#nav_step3').addClass('selected');
            $('#nav_step4').removeClass('selected');  
        }//end of else
    }else if(wizardCurrentStep == 3){
        $('#step1').css('display','none');
        $('#step2').css('display','none');
        $('#step3').css('display','none');
        $('#step4').css('display','block');
        $('#back_button').show();
        $('#btn_save').show();
        $('#btn_next').hide();
        $('#nav_step1').removeClass('selected');
        $('#nav_step2').removeClass('selected');
        $('#nav_step3').removeClass('selected');
        $('#nav_step4').addClass('selected'); 
    }//end of else if    
});//end of function

$('#back_button').on('click',function(){
    var wizardCurrentStep = $('.nav-steps.selected').attr('data-nav-step');
    if(wizardCurrentStep == 2){
        $('#step1').css('display','block');
        $('#step2').css('display','none');
        $('#step3').css('display','none');
        $('#step4').css('display','none');
        $('#back_button').hide();
        $('#btn_save').hide();
        $('#btn_next').show();
        $('#nav_step1').addClass('selected');
        $('#nav_step2').removeClass('selected');
        $('#nav_step3').removeClass('selected');
        $('#nav_step4').removeClass('selected'); 
    }else if(wizardCurrentStep == 3){
        $('#step1').css('display','none');
        $('#step2').css('display','block');
        $('#step3').css('display','none');
        $('#step4').css('display','none');
        $('#back_button').show();
        $('#btn_save').hide();
        $('#btn_next').show();
        $('#nav_step1').removeClass('selected');
        $('#nav_step2').addClass('selected');
        $('#nav_step3').removeClass('selected');
        $('#nav_step4').removeClass('selected');
    }else if(wizardCurrentStep == 4){
        $('#step1').css('display','none');
        $('#step2').css('display','none');
        $('#step3').css('display','block');
        $('#step4').css('display','none');
        $('#back_button').show();
        $('#btn_save').hide();
        $('#btn_next').show();
        $('#nav_step1').removeClass('selected');
        $('#nav_step2').removeClass('selected');
        $('#nav_step3').addClass('selected');
        $('#nav_step4').removeClass('selected'); 
    }//end of else if
});//end of function

$('#flow_module').on('change',function(){
    var moduleName = $('#flow_module').val();
    //panel
    var callback = {
        success: function(result) {
            panelName = result.responseText;
            var obj = jQuery.parseJSON(panelName);
            $.each(obj, function (index, value) {
                $('#defaultpanel').append("<option value='"+value.panelLabel+"'>"+value.panelName+"</option>");
                $('#panel_hide').append("<option value='"+value.panelLabel+"'>"+value.panelName+"</option>");
                $('#panel_show').append("<option value='"+value.panelLabel+"'>"+value.panelName+"</option>");
            });//end of each
        }//end of success
    }//end of callback

    //field
    var callback2 = {
        success: function(result) {  
            fieldName = result.responseText;
            var obj = jQuery.parseJSON(fieldName);
            $.each(obj, function (index, value) {
                $('#defaultfield').append("<option value='"+value.fieldname+"'>"+value.label+"</option>");
                $('#field_hide').append("<option value='"+value.fieldname+"'>"+value.label+"</option>");
                $('#field_show').append("<option value='"+value.fieldname+"'>"+value.label+"</option>");
                $('#field_readonly').append("<option value='"+value.fieldname+"'>"+value.label+"</option>");
                if(value.type != 'bool'){
                    $('#field_mandatory').append("<option value='"+value.fieldname+"'>"+value.label+"</option>");    
                }
            });//end of each     
        }//end of success
    }//end of callback2

    YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsAllModulePanels&flow_module="+moduleName,callback);
    YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsAllModuleFields&flow_module="+moduleName,callback2);
    
    $('#defaultpanel').empty();
    $('#panel_hide').empty();
    $('#panel_show').empty();

    $('#defaultfield').empty();
    $('#field_hide').empty();
    $('#field_show').empty();
    $('#field_readonly').empty();
    $('#field_mandatory').empty();
});//end of function

$("#panel_show").change(function () {
    var panelHideValue = $('#panel_hide').val();
    var panelShowValue = $('#panel_show').val();
    var convertStringPanelShowValue = JSON.stringify(panelShowValue);
    var panelShowValueObject = JSON.parse(convertStringPanelShowValue);
    for(var i=0;i<panelShowValueObject.length;i++){
        if(jQuery.inArray(panelShowValueObject[i],panelHideValue) != '-1'){
            label = SUGAR.language.get('Administration','LBL_PANEL_SHOW_VALIDATION');
            alert(label);
            $("select[id='panel_show']").find("option[value='"+panelShowValueObject[i]+"']").attr("selected",false); 
        }//end of if
    }//end of for
});//end of function

$("#panel_hide").change(function () {
    var panelHideValue = $('#panel_hide').val();
    var panelShowValue = $('#panel_show').val();
    var convertStringPanelHideValue = JSON.stringify(panelHideValue);
    var panelHideValueObject = JSON.parse(convertStringPanelHideValue);
    for(var i=0;i<panelHideValueObject.length;i++){
        if(jQuery.inArray(panelHideValueObject[i],panelShowValue) != '-1'){
            label = SUGAR.language.get('Administration','LBL_PANEL_SHOW_VALIDATION');
            alert(label);
            $("select[id='panel_hide']").find("option[value='"+panelHideValueObject[i]+"']").attr("selected",false); 
        }//end of if
    }//end of for
});//end of function

$("#field_show").change(function () {
    var fieldHideValue = $('#field_hide').val();
    var fieldShowValue = $('#field_show').val();
    var convertStringFieldShowValue = JSON.stringify(fieldShowValue);
    var fieldShowValueObject = JSON.parse(convertStringFieldShowValue);
    for(var i=0;i<fieldShowValueObject.length;i++){
        if(jQuery.inArray(fieldShowValueObject[i],fieldHideValue) != '-1'){
            label = SUGAR.language.get('Administration','LBL_PANEL_SHOW_VALIDATION');
            alert(label);
            $("select[id='field_show']").find("option[value='"+fieldShowValueObject[i]+"']").attr("selected",false); 
        }//end of if
    }//end of for
});//end of function

$("#field_hide").change(function () {
    var fieldHideValue = $('#field_hide').val();
    var fieldShowValue = $('#field_show').val();
    var convertStringFieldHideValue = JSON.stringify(fieldHideValue);
    var fieldHideValueObject = JSON.parse(convertStringFieldHideValue);
    for(var i=0;i<fieldHideValueObject.length;i++){
        if(jQuery.inArray(fieldHideValueObject[i],fieldShowValue) != '-1'){
            label = SUGAR.language.get('Administration','LBL_PANEL_SHOW_VALIDATION');
            alert(label);
            $("select[id='field_hide']").find("option[value='"+fieldHideValueObject[i]+"']").attr("selected",false); 
        }//end of if
    }//end of for
});//end of function

function cancel(){
    window.location.href = "index.php?module=Administration&action=vi_dynamicpanelslistview";
}//end of function

function clearall(){
    var wizardCurrentStep = $('.nav-steps.selected').attr('data-nav-step');
    if(wizardCurrentStep == 1){
        $('#dynamic_panels_name').val('');
        $('#flow_module').val('');
        $('#defaultpanel').empty();
        $('#defaultfield').empty();
        $('#user_type').val('');
        $('#user_roles').val('');
    }else if(wizardCurrentStep == 2){
        $("#aow_conditionLines tr").remove(); 
    }else if(wizardCurrentStep == 3){
        $('#panel_hide').val('');
        $('#field_hide').val('');
        $('#panel_show').val('');
        $('#field_show').val('');
        $('#field_readonly').val('');
        $('#field_mandatory').val('');
    }else if(wizardCurrentStep == 4){
        $('#fieldLines tr').remove();
        $('#fieldColor tr').remove();
    }//end of else if
}//end of function
