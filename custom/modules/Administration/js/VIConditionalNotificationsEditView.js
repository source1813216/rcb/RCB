/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$('#back_button').hide();
$('#btn_save').hide();

//notification_on_edit
if($('#notification_on_edit').is(':checked')){
    $('#notification_on_edits').val('1');
}else{
   $('#notification_on_edits').val('0');
}

//notification_on_view
if($('#notification_on_view').is(':checked')){
    $('#notification_on_views').val('1');
}else{
   $('#notification_on_views').val('0');
}

//notification_on_save
if($('#notification_on_save').is(':checked')){
    $('#notification_on_saves').val('1');
}else{
   $('#notification_on_saves').val('0');
}

//notification_not_allow_save
if($('#notification_not_allow_save').is(':checked')){
    $('#notification_not_allow_saves').val('1');
}else{
   $('#notification_not_allow_saves').val('0');
}

//notification_confirmation_dialog
if($('#notification_confirmation_dialog').is(':checked')){
    $('#notification_confirmation_dialogs').val('1');
}else{
   $('#notification_confirmation_dialogs').val('0');
}

//notification_on_duplicate
if($('#notification_on_duplicate').is(':checked')){
    $('#notification_on_duplicates').val('1');
}else{
   $('#notification_on_duplicates').val('0');
}

//back_button
$('#back_button').on('click',function(){
    var wizardCurrentStep = $('.nav-steps.selected').attr('data-nav-step');
    if(wizardCurrentStep == 2){
        $('#btn_save').hide();
        $('#btn_next').show();
        $('#back_button').hide();
        $('#step1').css('display','block');
        $('#step2').css('display','none');
        $('#step3').css('display','none');
        $('#nav_step1').addClass('selected');
        $('#nav_step2').removeClass('selected');
        $('#nav_step3').removeClass('selected');
    }else if(wizardCurrentStep == 3){
        $('#btn_save').hide();
        $('#btn_next').show();
        $('#back_button').show();
        $('#step1').css('display','none');
        $('#step2').css('display','inline-table');
        $('#step3').css('display','none');
        $('#nav_step1').removeClass('selected');
        $('#nav_step2').addClass('selected');
        $('#nav_step3').removeClass('selected');
    }
});

//btn_next
$('#btn_next').on('click',function(){
    var wizardCurrentStep = $('.nav-steps.selected').attr('data-nav-step');
    if(wizardCurrentStep == 1){
        var module = $('#flow_module').val();
        var description = $('#description').val();
        if(description == '' || module == ''){
            alert(SUGAR.language.get('Administration','LBL_VALIDATION_MESSAGE'));
        }else{
            $('#back_button').show();
            $('#btn_save').hide();
            $('#btn_next').show();
            $('#step1').css('display','none');
            $('#step2').css('display','inline-table');
            $('#step3').css('display','none');
            $('#nav_step1').removeClass('selected');
            $('#nav_step2').addClass('selected');
            $('#nav_step3').removeClass('selected');
        }
    }else if(wizardCurrentStep == 2){
        var rowc = $("#aow_conditionLines > tbody > tr").length;
        var checkConditionValue;
        var conditionValue = '';
        for(var i=0;i<rowc;i++){
            if($('#product_line'+i).is(':visible')){
                operator = document.getElementById('aow_conditions_operator['+i+']').value;
                var field = document.getElementById('aow_conditions_field'+i).value;
                var valueType = document.getElementById('aow_conditions_value_type['+i+']').value;
                if(valueType == 'Date'){
                    param0 = document.getElementById('aow_conditions_value['+i+'][0]').value;
                    param1 = document.getElementById('aow_conditions_value['+i+'][1]').value;
                    param2 = document.getElementById('aow_conditions_value['+i+'][2]').value;
                    param3 = document.getElementById('aow_conditions_value['+i+'][3]').value;
                    if(param1 == 'now'){
                        param2 = '0';
                        param3 = '0';
                    }
                    if(param0 == '' || param1 == '' || param2 == '' || param3 == ''){
                        conditionValue = '';
                    }else{
                        conditionValue = '0';
                    }
                }else{
                    var myTDs = $('td #aow_conditions_fieldInput'+i).closest('tr').find("td input.datetimecombo_date").length;
                    if(field == 'currency_id'){
                        conditionValue = document.getElementById('aow_conditions_value['+i+']_select').value;
                    }else if(myTDs > 0){
                        if(operator == 'regular_expression'){
                            conditionValue = document.getElementById('aow_conditions_regular_expression['+i+']').value; 
                        }else{
                            conditionValue = $('td #aow_conditions_fieldInput'+i).closest('tr').find("td input.datetimecombo_date").val();
                        }
                    }else{
                        if(operator == 'regular_expression'){
                            conditionValue = document.getElementById('aow_conditions_regular_expression['+i+']').value;    
                        }else{
                            conditionValue = document.getElementById('aow_conditions_value['+i+']').value;    
                        }
                    }
                }
                if(operator != 'is_null'){ 
                    if(conditionValue == ''){
                        checkConditionValue = 0;
                    }
                }
            }
        }
        if(checkConditionValue == 0){
            alert(SUGAR.language.get('Administration','LBL_PLZ_ENTER_VALUE'));
        }else{
            $('#back_button').show();
            $('#btn_save').show();
            $('#btn_next').hide();
            $('#step1').css('display','none');
            $('#step2').css('display','none');
            $('#step3').css('display','block');
            $('#nav_step1').removeClass('selected');
            $('#nav_step2').removeClass('selected');
            $('#nav_step3').addClass('selected');
        }   
    }
});

//notification_on_edits
$(document).on('change', '#notification_on_edit', function() {
    if($(this).is(':checked')){
        $('#notification_on_edits').val('1');
    }else{
       $('#notification_on_edits').val('0');
    }
});

//notification_on_views
$(document).on('change', '#notification_on_view', function() {
    if($(this).is(':checked')){
        $('#notification_on_views').val('1');
    }else{
       $('#notification_on_views').val('0');
    }
});

//notification_on_save
$(document).on('change', '#notification_on_save', function() {
    if($(this).is(':checked')){
        $('#notification_on_saves').val('1');
    }else{
       $('#notification_on_saves').val('0');
    }
});

//notification_not_allow_save
$(document).on('change', '#notification_not_allow_save', function() {
    if($(this).is(':checked')){
        $('#notification_not_allow_saves').val('1');
        $('#notification_confirmation_dialogs').val('0');
        $('#notification_confirmation_dialog').removeAttr('checked');
    }else{
       $('#notification_not_allow_saves').val('0');
    }
});

//notification_confirmation_dialog
$(document).on('change', '#notification_confirmation_dialog', function() {
    if($(this).is(':checked')){
        $('#notification_confirmation_dialogs').val('1');
        $('#notification_not_allow_saves').val('0');
        $('#notification_not_allow_save').removeAttr('checked');
    }else{
       $('#notification_confirmation_dialogs').val('0');
    }
});

//notification_on_duplicate
$(document).on('change', '#notification_on_duplicate', function() {
    if($(this).is(':checked')){
        $('#notification_on_duplicates').val('1');
    }else{
       $('#notification_on_duplicates').val('0');
    }
});

//cancel function
function cancel(){
    window.location.href = "index.php?module=Administration&action=vi_conditionalnotificationslistview";
}

//clear function
function clearall(){
    var wizardCurrentStep = $('.nav-steps.selected').attr('data-nav-step');
    if(wizardCurrentStep == 1){
        $('#description').val('');
        $('#flow_module').val('');
    }else if(wizardCurrentStep == 2){
        $("#aow_conditionLines tr").remove(); 
    }
}
