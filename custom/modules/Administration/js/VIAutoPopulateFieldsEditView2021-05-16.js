/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
if(recordId != ''){
	getRelateFieldModuleName('load');
	$('#module').attr('disabled',true);
}//end of if

$('#nextButton').on("click",function(){
	var wizardCurrentStep = $('.nav-steps.selected').attr('data-nav-step');

	if(wizardCurrentStep == 1){
		var sourceModule = $('#module').val();
		if(sourceModule == ''){
			alert(mod.LBL_REQUIRED_FIELD_VALIDATION);
		}else{
			$('#step1,#step3').css('display','none');
			$('#step2').css('display','block');
			$('#saveButton').css('display','none');
			$('#cancelButton').css('margin-left','20px');
			$('#backButton').css('display','block');
			$('#nextButton').attr('style', 'display: block;margin-left: 20px;');
			$('#navStep1').removeClass('selected');
			$('#navStep2').addClass('selected');
			$('#navStep3').removeClass('selected');
		}//end of else
	}else if(wizardCurrentStep == 2){
		conditionVal = 0;
		$("#relatedFields option").each(function(){
    		var field = $(this).val();
    		if(field != ''){
    			spanId = field+'_lines_span';
    			if($('#'+spanId).length > 0){
    				var rowc = $("#"+field+"_conditionLines > tbody > tr").length;
    				for(var i=0;i<rowc;i++){
	    				if($('#'+field+'_product_line'+i).is(':visible')){
	    					var targetModuleField = $('#'+field+'_target_module_fields'+i).val();
	    					var sourceModuleField = $('#'+field+'_source_module_fields'+i).val();

	    					if(targetModuleField == '' || sourceModuleField == ''){
	    						conditionVal = 1;
	    					}//end of if
	    				}//end of if
	    			}//end of for
    			}//end of if
    		}//end of if	
		});//end of function

		if(conditionVal == 1){
			alert(mod.LBL_SELECT_MAPPING_FIELD);
		}else{
			$('#step1,#step2').css('display','none');
			$('#step3').css('display','block');
			$('#saveButton').css('display','block');
			$('#cancelButton').css('margin-left','20px');
			$('#backButton').css('display','block');
			$('#nextButton').attr('style', 'display: none;margin-left: 0px;');
			$('#navStep1').removeClass('selected');
			$('#navStep2').removeClass('selected');
			$('#navStep3').addClass('selected');
		}
	}//end of else if
});//end of function

$('#backButton').on("click",function(){
	var wizardCurrentStep = $('.nav-steps.selected').attr('data-nav-step');
	if(wizardCurrentStep == 2){
		$('#backButton').css('display','none');
		$('#cancelButton').css('margin-left','0px');
		$('#saveButton').css('display','none');
		$('#nextButton').attr('style', 'display: block; margin-left: 20px;');
		$('#step1').css('display','block'); 
		$('#step2').css('display','none'); 
		$('#step3').css('display','none'); 
		$('#navStep1').addClass('selected');
		$('#navStep2').removeClass('selected');
		$('#navStep3').removeClass('selected');
	}else if(wizardCurrentStep == 3){
		$('#backButton').css('display','block');
		$('#cancelButton').css('margin-left','20px');
		$('#saveButton').css('display','none');
		$('#nextButton').attr('style', 'display: block; margin-left: 20px;'); 
		$('#step1').css('display','none'); 
		$('#step2').css('display','block'); 
		$('#step3').css('display','none'); 
		$('#navStep1').removeClass('selected');
		$('#navStep2').addClass('selected');
		$('#navStep3').removeClass('selected');
	}//end of else if
});//end of function

function cancel(){
	window.location.href = 'index.php?module=Administration&action=vi_autopopulatefieldslistview';
}//end of function

function clearall(){
	var wizardCurrentStep = $('.nav-steps.selected').attr('data-nav-step');
	if(wizardCurrentStep == 1){
		$('#module').val('');
		$('#status').val('Active');
	}else if(wizardCurrentStep == 2){
		$('#relatedFields').val('');
		$('.tblFieldMapping').closest('div').remove();
		$('.conditionLinespan').remove();
		$('span#conditionLinesSpanId').closest('div').find('br').remove();
	}else if(wizardCurrentStep == 3){
		$('#calculateRelatedFields').val('');
		$('.tblCalculateField').closest('div').remove();
		$('.calculateConditionLinespan').remove();
		$('span#calculateConditionLinesSpan').closest('div').find('br').remove();
	}//end of else if
}//end of function

function getRelateFieldModuleName(type){
	if(recordId == ''){
		$('.tblFieldMapping').remove();
		$('.conditionLinespan').remove();
		$('span#conditionLinesSpanId').closest('div').find('br').remove();
		$('.tblCalculateField').remove();
		$('.calculateConditionLinespan').remove();
		$('span#calculateConditionLinesSpan').closest('div').find('br').remove();
	}//end of if
	var sourceModule = $('#module').val();
	var sourceModuleLabel = $('#module option:selected').text();
	SUGAR.ajaxUI.showLoadingPanel(); // show loader
	if(type == 'load'){
		$.ajax({    
			type: 'POST',
			url: 'index.php?entryPoint=VIAutoPopulteFieldsFetchRelateModule',
			data: {sourceModule: sourceModule},  
			success: function(response){
				SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
				$('#relatedFields').empty();
				$('#relatedFields').append(response);
				$('#calculateRelatedFields').empty();
				$('#calculateRelatedFields').append(response);
			}//end of success 
		});//end of ajax
	}else{
		$.ajax({    
			type: 'POST',
			url: 'index.php?entryPoint=VIAutoPopulateFieldsCheckDuplicateConfiguration',
			data: {sourceModule: sourceModule},  
			success: function(response){
				if(response == 1){
					SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
					alert('"'+sourceModuleLabel+'" '+mod.LBL_DUPLICATE_RECORD_MESSAGE);
					$('#module').val('');
				}else{
					$.ajax({    
						type: 'POST',
						url: 'index.php?entryPoint=VIAutoPopulteFieldsFetchRelateModule',
						data: {sourceModule: sourceModule},  
						success: function(response){
							SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
							$('#relatedFields').empty();
							$('#relatedFields').append(response);
							$('#calculateRelatedFields').empty();
							$('#calculateRelatedFields').append(response);
						}//end of success 
					});//end of ajax
				}//end of else
			}//end of success 
		});//end of ajax
	}//end of else	
}//end of function

$('#addFieldMappingBlock').on('click',function(){
	SUGAR.ajaxUI.showLoadingPanel(); // show loader
	var sourceModule = $('#module').val();
	var relatedFieldName = $('#relatedFields').val();
	var relatedFieldLabel = $('#relatedFields option:selected').text();

	spanId = relatedFieldName+'_lines_span';

	if(relatedFieldName != ''){
		if(($('#'+spanId).length) == 0){
			$.ajax({    
				type: 'POST',
				url: 'index.php?entryPoint=VIAutoPopulteFieldsFieldMappingBlock',
				data: {
					blockType: "fieldMapping",
					sourceModule: sourceModule,
					relatedFieldName : relatedFieldName,
					relatedFieldLabel : relatedFieldLabel
				},  
				success: function(response){
					$('#relatedFields').val('');
					SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
					var relateField = '';
					$('.conditionLinespan').each(function(){
						var spanId = $(this).attr('id');
						var res = spanId.split("_lines_span");
						relateField = res[0];
    				});//end of each
    				if(relateField != ''){
    					$('#'+relateField+'_lines_span').after(response);
    				}else{
    					$('#conditionLinesSpanId').after(response);
    				}//end of else
				}//end of success 
			});//end of ajax
		}else{
			SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
			$('#relatedFields').val('');
			duplicateRelatedFieldMsg = mod.LBL_RELATED_FIELD_DUPLICATE_MSG_1+' "'+relatedFieldLabel+'"'+mod.LBL_RELATED_FIELD_DUPLICATE_MSG_2+' "'+relatedFieldLabel+'" '+mod.LBL_RELATED_FIELD_DUPLICATE_MSG_3;
	        alert(duplicateRelatedFieldMsg);
		}//end of else	
	}else{
		SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
		alert(mod.LBL_SELECT_REALTE_FIELD_VALIDATION);
	}//end of else
});//end of function

$('#addCalculateFieldBlock').on('click',function(){
	SUGAR.ajaxUI.showLoadingPanel(); // show loader
	var sourceModule = $('#module').val();
	var calculateRelatedFieldName = $('#calculateRelatedFields').val();
	var calculateRelatedFieldLabel = $('#calculateRelatedFields option:selected').text();

	spanId = calculateRelatedFieldName+'_calculate_lines_span';
	if(calculateRelatedFieldName != ''){
		if(($('#'+spanId).length) == 0){
			$.ajax({    
				type: 'POST',
				url: 'index.php?entryPoint=VIAutoPopulteFieldsFieldMappingBlock',
				data: {
					blockType: "calculateField",
					sourceModule: sourceModule,
					calculateRelatedFieldName : calculateRelatedFieldName,
					calculateRelatedFieldLabel : calculateRelatedFieldLabel
				},  
				success: function(response){
					$('#calculateRelatedFields').val('');
					SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
					var calculateRelateField = '';
					$('.calculateConditionLinespan').each(function(){
						var spanId = $(this).attr('id');
						var res = spanId.split("_calculate_lines_span");
						calculateRelateField = res[0];
    				});//end of each
    				if(calculateRelateField != ''){
    					$('#'+calculateRelateField+'_calculate_lines_span').after(response);
    				}else{
    					$('#calculateConditionLinesSpan').after(response);
    				}//end of else
				}//end of success 
			});//end of ajax
		}else{
			SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
			$('#calculateRelatedFields').val('');
			duplicateRelatedFieldMsg = mod.LBL_RELATED_FIELD_DUPLICATE_MSG_1+' "'+calculateRelatedFieldLabel+'"'+mod.LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2+' "'+calculateRelatedFieldLabel+'" '+mod.LBL_RELATED_FIELD_DUPLICATE_MSG_3;
	        alert(duplicateRelatedFieldMsg);
		}//end of else
	}else{
		SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
		alert(mod.LBL_SELECT_REALTE_FIELD_VALIDATION);
	}//end of else
});//end of function

var clickCount = 0;
function saveAutoPopulateFieldsRecord(){
	SUGAR.ajaxUI.showLoadingPanel(); // show loader
	var conditionVal = 0;
	$("#calculateRelatedFields option").each(function(){
		var field = $(this).val();
		if(field != ''){
			spanId = field+'_calculate_lines_span';
			if($('#'+spanId).length > 0){
				var rowc = $("#"+field+"_calculate_conditionLines > tbody > tr").length;
				for(var i=0;i<rowc;i++){
    				if($('#'+field+'_calculate_product_line'+i).is(':visible')){
    					var functionName = $('#'+field+'_function_name'+i).val();
    					var inputFormula = $('#'+field+'_input_formula'+i).val();
    					var sourceModuleField = $('#'+field+'_calculate_source_module_fields'+i).val();
    					if(functionName == '' || inputFormula == '' || sourceModuleField == ''){
    						conditionVal = 1;
    					}//end of if
    				}//end of if
    			}//end of for
			}//end of if
		}//end of if	
	});//end of function
	if(conditionVal == 0){
		var numericFunctionArray1 = ["Add","Subtract","Multiply","Average","Minimum"];
	    var numericFunctionArray2 = ["Divide","Power","Percentage","Mod"];
	    var logFloorCeilFunctionArray = ["Ln","Absolute","Negate","Floor","Ceil"];
	    var dateDiffFunctionArray = ["Date Difference","Diff Days","Diff Hour","Diff Minute","Diff Month","Week Diff","Diff Year"];

	    var formulaValidate = false;
	    $("#calculateRelatedFields option").each(function(){
			var field = $(this).val();
			if(field != ''){
				spanId = field+'_calculate_lines_span';
				if($('#'+spanId).length > 0){
					var rowc = $("#"+field+"_calculate_conditionLines > tbody > tr").length;
					for(var i=0;i<rowc;i++){
	    				if($('#'+field+'_calculate_product_line'+i).is(':visible')){
	    					var functionName = $('#'+field+'_function_name'+i).val();
    						var inputFormula = $('#'+field+'_input_formula'+i).val();
    						var functionNameText = $('#'+field+'_function_name'+i+ ' option:selected').text();
    						var formulaValArray = inputFormula.match(/\((.*)\)/);
			                var splitFormula = formulaValArray[1].split(',');
			                var count = 1;

			                if(jQuery.inArray(functionName,numericFunctionArray1) != '-1'){
			                    $.each(splitFormula, function(key,val){
			                        if((val == 'number_field'+count) || (val == '...')){
			                            alert(mod.LBL_NUMBER_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                            formulaValidate = true;
			                            return false;
			                        }//end of if
			                        count++;
			                    });//end of each
			                }else if(jQuery.inArray(functionName,numericFunctionArray2) != '-1'){
			                    if(splitFormula[0] == 'number_field1' || splitFormula[1] == 'number_field2'){
			                        alert(mod.LBL_NUMBER_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }//end of if
			                }else if(functionName == 'String Length'){
			                    if(splitFormula[0] == 'string_field'){
			                        alert(mod.LBL_STRING_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }//end of if
			                }else if(functionName == 'Concatenation'){
			                    $.each(splitFormula, function(key,val){
			                        if((val == 'string_field'+count) || (val == '...')){
			                            alert(mod.LBL_STRING_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                            formulaValidate = true;
			                            return false;
			                        }//end of if
			                        count++;
			                    });//end of each
			                }else if(functionName == 'Logarithm'){
			                    if(splitFormula[0] == 'number_field'){
			                        alert(mod.LBL_NUMBER_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }//end of if

			                    if(splitFormula[1] == 'base' && typeof(splitFormula[1]) == 'string'){
			                        alert(mod.LBL_BASE_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }else if(splitFormula[1] == undefined){
			                    	alert(mod.LBL_BASE_PARAMETER_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION+' '+LBL_INPUT_FORMULA_ERROR);
			                        formulaValidate = true;
			                    }//end of else if
			                }else if(jQuery.inArray(functionName,logFloorCeilFunctionArray) != '-1'){
			                    if(splitFormula[0] == 'number_field'){
			                        alert(mod.LBL_NUMBER_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }//end of if
			                }else if(jQuery.inArray(functionName,dateDiffFunctionArray) != '-1'){
			                    if(splitFormula[0] == 'date_field1' || splitFormula[1] == 'date_field1'){
			                        alert(mod.LBL_DATE_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }//end of if
			                }else if(functionName == 'Add Hours' || functionName == 'Sub Hours'){
			                    if(splitFormula[0] == 'date_field'){
			                        alert(mod.LBL_DATE_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }//end of if

			                    if(splitFormula[1] == 'hours' && typeof(splitFormula[1]) == 'string'){
			                        alert(mod.LBL_HOURS_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }else if(splitFormula[1] == undefined){
			                    	alert(mod.LBL_HOURS_PARAMETER_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION+' '+mod.LBL_INPUT_FORMULA_ERROR);
			                        formulaValidate = true;
			                    }//end of else if
			                }else if(functionName == 'Add Day' || functionName == 'Sub Days'){
			                    if(splitFormula[0] == 'date_field'){
			                        alert(mod.LBL_DATE_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }//end of if

			                    if(splitFormula[1] == 'days' && typeof(splitFormula[1]) == 'string'){
			                        alert(mod.LBL_DAYS_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }else if(splitFormula[1] == undefined){
			                    	alert(mod.LBL_DAYS_PARAMETER_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION+' '+LBL_INPUT_FORMULA_ERROR);
			                        formulaValidate = true;
			                    }//end of else if
			                }else if(functionName == 'Add Week' || functionName == 'Sub Week'){
			                    if(splitFormula[0] == 'date_field'){
			                        alert(mod.LBL_DATE_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }//end of if
			                    if(splitFormula[1] == 'week' && typeof(splitFormula[1]) == 'string'){
			                        alert(mod.LBL_WEEK_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }else if(splitFormula[1] == undefined){
			                    	alert(mod.LBL_WEEK_PARAMETER_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION+' '+LBL_INPUT_FORMULA_ERROR);
			                        formulaValidate = true;
			                    }//end of else if
			                }else if(functionName == 'Add Month' || functionName == 'Sub Month'){
			                    if(splitFormula[0] == 'date_field'){
			                        alert(mod.LBL_DATE_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }//end of if

			                    if(splitFormula[1] == 'month' && typeof(splitFormula[1]) == 'string'){
			                        alert(mod.LBL_MONTH_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }else if(splitFormula[1] == undefined){
			                    	alert(mod.LBL_MONTH_PARAMETER_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION+' '+LBL_INPUT_FORMULA_ERROR);
			                        formulaValidate = true;
			                    }//end of else if
			                }else if(functionName == 'Add Year' || functionName == 'Sub Year'){
			                    if(splitFormula[0] == 'date_field'){
			                        alert(mod.LBL_DATE_FIELD_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }//end of if
			                    
			                    if(splitFormula[1] == 'year' && typeof(splitFormula[1]) == 'string'){
			                        alert(mod.LBL_YEAR_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION);
			                        formulaValidate = true;
			                    }else if(splitFormula[1] == undefined){
			                    	alert(mod.LBL_YEAR_PARAMETER_ERROR+' '+functionNameText+' '+mod.LBL_FUNCTION+' '+LBL_INPUT_FORMULA_ERROR);
			                        formulaValidate = true;
			                    }//end of else if
			                }//end of else if
	    				}//end of if
	    			}//end of for
				}//end of if
			}//end of if	
		});//end of function

		if(clickCount == 0 && formulaValidate == false){
			var formData = $('form');
		    var disabled = formData.find(':disabled').removeAttr('disabled');
		    var formData = formData.serialize();
			$.ajax({
				url: 'index.php?entryPoint=VIAutoPopulateFieldsSaveConfiguration',
				type: "post",
				data: {formData : formData,
					   recordId : recordId},
				success: function (response) {
					window.location.href = listviewURL;
				}//end of success
			});//end of ajax
			clickCount = clickCount + 1;
		}else{
			SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
		}//end of else
	}else{
		SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
		alert(mod.LBL_PLEASE_FILL_ALL_FIELDS);
	}//end of else
}//end of function