/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
//select all checkbox
$('#selectAll').click(function(event) {
    $('#actionMenu').css('display','none');   
    if(this.checked) {
        $('.bulkAction').css('display','none');
        $('#actionLinkTop').css('display','block');
        
        // Iterate each checkbox
        $('.listviewCheckbox').each(function() {
            this.checked = true;                        
        });//end of each
    } else {
        $('.bulkAction').css('display','inline-block');
        $('#actionLinkTop').css('display','none');
        
        // Iterate each checkbox
        $('.listviewCheckbox').each(function() {
            this.checked = false;                       
        });//end of each
    }//end of else
});//end of function

$('.listviewCheckbox').on('click',function(){
    if($(".listviewCheckbox").length == $(".listviewCheckbox:checked").length) {
        $("#selectAll").prop("checked", true);
    }else{
        $("#selectAll").prop("checked", false);
    }//end of else

    $('#actionMenu').css('display','none');

    if(this.checked){
        $('.bulkAction').css('display','none');
        $('#actionLinkTop').css('display','block');
    }else{
        $('.bulkAction').css('display','inline-block');
        $('#actionLinkTop').css('display','none');
    }//end of else
});//end of function

$('.enableAutoPopulateConfig').on('change',function(){
    SUGAR.ajaxUI.showLoadingPanel(); //show loader

    var title = $(this).closest('tr').find('td:nth-child(3)').text();
    var checkValue;
    if($(this).is(':checked')){
        $(this).val('1');
        checkValue = 1;
    }else{
        $(this).val('0');
        checkValue = 0;
    }//end of else

    var id = $(this).closest('tr').attr('data-id');
    
    $.ajax({
        url: "index.php?entryPoint=VIAutoPopulateFieldsSaveConfiguration",
        type: "post",
        data: {enableAutoPopulateConfig : checkValue,
                id : id},
        success: function (response) {
            SUGAR.ajaxUI.hideLoadingPanel(); //hide loader
            if(response == 1){
                if(checkValue == 1){
                    alert(trim(title)+' '+mod.LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED);
                }else if(checkValue == 0){
                    alert(trim(title)+' '+mod.LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED);
                }//end of else if
            }//end of if
        }//end of success
    });//end of ajax
});//end of function

$('.actionButton').on('click',function(){
    if($('#actionMenu').css('display') == "none"){
        $('#actionMenu').css('display','inline-block');        
        $('#actionMenu').css('margin-top','10px');
    }else{
        $('#actionMenu').css('display','none');  
    }//end of else
});//end of function

function updateActionStatus(checkValue){
    SUGAR.ajaxUI.showLoadingPanel(); //show loader
    var ids = [];
    $(".listviewCheckbox:checked").each(function() {
        ids.push($(this).val());
    });//end of each
   
    var selectedValues = ids.join(",");
    $.ajax({
        url: "index.php?entryPoint=VIAutoPopulateFieldsSaveConfiguration",
        type: "post",
        data: {enableAutoPopulateConfig : checkValue,
                recordId : selectedValues},
        success: function (response) {
            SUGAR.ajaxUI.hideLoadingPanel(); //hide loader
            window.location.reload();
        }//end of success
    });//end of ajax
}//end of function

function actionDelete(){
    var ids = [];
    $(".listviewCheckbox:checked").each(function() {
        ids.push($(this).val());
    });//end of function
    
    var msg = mod.LBL_DELETE_MESSAGE_1+" "+(ids.length>1?mod.LBL_DELETE_MESSAGE_2:mod.LBL_DELETE_MESSAGE_3)+" "+mod.LBL_DELETE_MESSAGE_4;
    var checked = confirm(msg);
    if(checked == true){
        var selectedValues = ids.join(",");
        $.ajax({
            type: "POST",
            url: "index.php?entryPoint=VIAutoPopulateFieldsDeleteRecord",
            data: 'delId='+selectedValues,
            success: function(response) {
                window.location.reload();
            }//end of success
        });//end of ajax
    }//end of if
}//end of function