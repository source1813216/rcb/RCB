/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
var calculateFieldCondln = 0;
var calculateTargetModuleFields = Array();
var calculateSourceModuleFields = Array();
var functionsList = Array();

function loadCalculateFieldLine(condition){
	var ln = 0;
	ln = insertCalculateFieldBlock(condition['source_module'],condition['sourceModuleLabel'],condition['calculate_related_field_name'],condition['calculateRelatedFieldLabel'],condition['calculate_related_field_module'],condition['calculateRelatedModuleNameLabel'],condition['calculate_source_module_fields'],condition['function_name'],condition['input_formula']);
}//end of function

function insertCalculateFieldHeader(sourceModuleLable,calculateRelateFieldName,calculateRelateFieldLabel,calculateRelatedFieldModuleLable){
    tablehead = document.createElement("thead");
    tablehead.id = calculateRelateFieldName+"_calculate_conditionLines_head";
    tablehead.className = 'calculate_condition_lines_head';
    document.getElementById(calculateRelateFieldName+'_calculate_conditionLines').appendChild(tablehead);

    var x=tablehead.insertRow(-1);
    x.id= calculateRelateFieldName+'_calculate_conditionLines_head';

    var a=x.insertCell(0);

    var b=x.insertCell(1);
    b.style.color="rgb(0,0,0)";
    b.innerHTML= SUGAR.language.get('Administration','LBL_FUNCTIONS');

    var c=x.insertCell(2);
    c.style.color="rgb(0,0,0)";
    targetModuleFieldsLabel = SUGAR.language.get('Administration', 'LBL_TARGET_MODULE')+' ('+calculateRelatedFieldModuleLable+' - '+calculateRelateFieldLabel+' )'+' '+SUGAR.language.get('Administration', 'LBL_FIELDS');
    c.innerHTML= targetModuleFieldsLabel;

    var d=x.insertCell(3);
    d.style.color="rgb(0,0,0)";
    d.innerHTML= SUGAR.language.get('Administration', 'LBL_INPUT_FORMULA');

    var e=x.insertCell(4);
    e.style.color="rgb(0,0,0)";
    e.innerHTML= SUGAR.language.get('Administration', 'LBL_SOURCE_MODULE')+' ('+sourceModuleLable+') '+' '+SUGAR.language.get('Administration', 'LBL_FIELDS');
}//end of function

function insertCalculateFieldBlock(sourceModuleName, sourceModuleLable, calculateRelateFieldName, calculateRelateFieldLabel, calculateRelatedFieldModuleName, calculateRelatedFieldModuleLable,sourceModuleFieldVal,functionNameVal,inputFormulaVal){
	SUGAR.ajaxUI.showLoadingPanel(); // show loader
	$.ajax({
		url:"index.php?entryPoint=VIAutoPopulateFieldsSourceTargetModuleFields",
		dataType:"JSON",
		data:{sourceModule : sourceModuleName,
		      targetModule : calculateRelatedFieldModuleName},
		success:function(response){
			calculateSourceModuleFields = response['sourceModuleFieldsList'];
			calculateTargetModuleFields = response['targetModuleFieldsList'];
			functionsList = response['functionsList'];
			SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
			
			insertCalculateFieldLine(sourceModuleName, sourceModuleLable, calculateRelateFieldName, calculateRelateFieldLabel, calculateRelatedFieldModuleName, calculateRelatedFieldModuleLable);
			
			cnt = calculateFieldCondln - 1;
			$('#'+calculateRelateFieldName+'_function_name'+cnt).val(functionNameVal);
			$('#'+calculateRelateFieldName+'_input_formula'+cnt).val(inputFormulaVal);
			$('#'+calculateRelateFieldName+'_calculate_source_module_fields'+cnt).val(sourceModuleFieldVal);
		}//end of success
	});//end of ajax
}//end of function

function insertCalculateFieldLine(sourceModuleName, sourceModuleLable, calculateRelateFieldName, calculateRelateFieldLabel, calculateRelatedFieldModuleName, calculateRelatedFieldModuleLable){
	if (document.getElementById(calculateRelateFieldName+'_calculate_conditionLines_head') == null) {
    	insertCalculateFieldHeader(sourceModuleLable,calculateRelateFieldName,calculateRelateFieldLabel,calculateRelatedFieldModuleLable);
    } else {
        document.getElementById(calculateRelateFieldName+'_calculate_conditionLines_head').style.display = '';
    }//end of else

    var rowCount  = $("#"+calculateRelateFieldName+"_calculate_conditionLines > tbody > tr").length;
    calculateFieldCondln = rowCount;
    
    tablebody = document.createElement("tbody");
    tablebody.id = calculateRelateFieldName+"_calculate_conditions_body" + calculateFieldCondln;
    document.getElementById(calculateRelateFieldName+'_calculate_conditionLines').appendChild(tablebody);

    var x = tablebody.insertRow(-1);
    x.id = calculateRelateFieldName+'_calculate_product_line' + calculateFieldCondln;
    var calculateRelateFieldNameVal = '"'+calculateRelateFieldName+'"';
    var a = x.insertCell(0);
    a.innerHTML = "<button type='button' id='"+calculateRelateFieldName+"_calculate_conditions_delete_line" + calculateFieldCondln + "' class='button' value='' tabindex='116' onclick='markCalculateFieldLineDeleted(" + calculateFieldCondln + ","+calculateRelateFieldNameVal+")'><span class='suitepicon suitepicon-action-minus'></span></button><br>";
    a.innerHTML += "<input type='hidden' name='"+calculateRelateFieldName+"_calculate_conditions_deleted[" + calculateFieldCondln + "]' id='"+calculateRelateFieldName+"_calculate_conditions_deleted" + calculateFieldCondln + "' value='0'><input type='hidden' name='"+calculateRelateFieldName+"_calculate_conditions_id[" + calculateFieldCondln + "]' id='"+calculateRelateFieldName+"_calculate_conditions_id" + calculateFieldCondln + "' value=''>";
    
    targetLbl = '"target"';
   	sourceLbl = '"source"';

   	sourceModuleNameVal = '"'+sourceModuleName+'"';
   	calculateRelatedFieldModuleNameVal = '"'+calculateRelatedFieldModuleName+'"';
   	
   	var b = x.insertCell(1);
    b.innerHTML = "<select name='"+calculateRelateFieldName+"_function_name["+ calculateFieldCondln +"]' id='"+calculateRelateFieldName+"_function_name" + calculateFieldCondln + "' value='' tabindex='116' onchange='addFunctionFormula("+calculateFieldCondln+","+calculateRelateFieldNameVal+");'>" + functionsList + "</select>";
    b.innerHTML += "<span id='"+calculateRelateFieldName+"_function_name_label" + calculateFieldCondln + "' ></span>";

    var c = x.insertCell(2);
    c.innerHTML = "<select  style='width:60%;' name='"+calculateRelateFieldName+"_calculate_target_module_fields["+ calculateFieldCondln +"]' id='"+calculateRelateFieldName+"_calculate_target_module_fields" + calculateFieldCondln + "' value='' tabindex='116' onchange='addFieldFormula(" + calculateFieldCondln +","+calculateRelateFieldNameVal+","+calculateRelatedFieldModuleNameVal+")'>" + calculateTargetModuleFields + "</select>";
    c.innerHTML += "<span id='"+calculateRelateFieldName+"_calculate_target_module_fields_label" + calculateFieldCondln + "' ></span>";

    var d = x.insertCell(3);
    d.innerHTML = "<textarea style='width: 197px; height: 67px;' name='"+calculateRelateFieldName+"_input_formula["+calculateFieldCondln+"]' id='"+calculateRelateFieldName+"_input_formula"+calculateFieldCondln+"'></textarea>";
    
    var e = x.insertCell(4);
    e.innerHTML = "<select style='width:70%;' name='"+calculateRelateFieldName+"_calculate_source_module_fields["+ calculateFieldCondln +"]' id='"+calculateRelateFieldName+"_calculate_source_module_fields" + calculateFieldCondln + "' class='"+calculateRelateFieldName+"_calculateSourceModuleFields' value='' tabindex='116' onchange='checkDuplicateField(" + calculateFieldCondln +","+calculateRelateFieldNameVal+");'>" + calculateSourceModuleFields + "</select>";
    e.innerHTML += "<span id='"+calculateRelateFieldName+"_calculate_source_module_fields_label" + calculateFieldCondln + "' ></span>";

    calculateFieldCondln++;

    $('.edit-view-field #'+calculateRelateFieldName+'_calculate_conditionLines').find('tbody').last().find('select').change(function () {
        $(this).find('td').last().removeAttr("style");
        $(this).find('td').height($(this).find('td').last().height() + 8);
    });

    return calculateFieldCondln -1;
}//end of function

function markCalculateFieldLineDeleted(ln,calculateRelatedFieldName){
	// collapse line; update deleted value
    document.getElementById(calculateRelatedFieldName+'_calculate_conditions_body' + ln).style.display = 'none';
    document.getElementById(calculateRelatedFieldName+'_calculate_conditions_deleted' + ln).value = '1';
    document.getElementById(calculateRelatedFieldName+'_calculate_conditions_delete_line' + ln).onclick = '';
}//end of function


function addFunctionFormula(ln,calculateRelatedField){
    var functionValue = $('#'+calculateRelatedField+'_function_name'+ln).val();
    $.ajax({
        type: 'POST',
        url: 'index.php?entryPoint=VIAutoPopulateFieldsDisplayCalculateFieldsFormula',
        data: {functionValue: functionValue},  
        success: function(response){
            $('#'+calculateRelatedField+'_input_formula'+ln).val(response);
        }//end of success
    });//end of ajax
}//end of function

function addFieldFormula(ln,calculateRelatedField,calculateRelatedFieldModule){
    var targetModuleField = $('#'+calculateRelatedField+'_calculate_target_module_fields'+ln).val();
    var functionName = $('#'+calculateRelatedField+'_function_name'+ln).val();
    if(targetModuleField != ''){
        $.ajax({
            type: 'POST',
            url: 'index.php?entryPoint=VIAutoPopulateFieldsCheckCalculateFieldType',
            data: {targetModule: calculateRelatedFieldModule,
            	  targetModuleField : targetModuleField,
            	  functionName : functionName},  
            success: function(response){
                if(response == 1){
                    $('#'+calculateRelatedField+'_input_formula'+ln).val(($('#'+calculateRelatedField+'_input_formula'+ln).val()) + targetModuleField); 
                }else{
                    if(response != ''){
                        alert(response);
                        $('#'+calculateRelatedField+'_calculate_target_module_fields'+ln).val('');
                    }//end of if
                }//end of else   
            }//end of success
        });//end of ajax   
    }//end of if
}//end of function

function checkDuplicateField(calculateFieldCnt, calculateRelatedField){
    var sourceModuleField = $('#'+calculateRelatedField+'_calculate_source_module_fields'+calculateFieldCnt).val();
    var sourceModuleFieldText = $('#'+calculateRelatedField+'_calculate_source_module_fields'+calculateFieldCnt+' option:selected').text();
    $('.'+calculateRelatedField+'_calculateSourceModuleFields').each(function(){
        var fieldVal = $(this).val();
        fieldId = $(this).attr('id');
        if($(this).closest('tbody').css('display') != 'none'){
            calculateFieldId = calculateRelatedField+'_calculate_source_module_fields'+calculateFieldCnt;
            if(calculateFieldId != fieldId){
                if(sourceModuleField != '' && fieldVal != ''){
                    if(sourceModuleField == fieldVal){
                        $('#'+calculateFieldId).val('');
                        alert(sourceModuleFieldText+SUGAR.language.get('Administration','LBL_CALCULATE_FIELD_MAPPING_VALIDATION'));
                    }//end of if
                }//end of if
            }//end of if
        }//end of if
    });//end of each

}//end of function