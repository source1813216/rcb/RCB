/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/

var condln = 0;
var condln_count = 0;
var flow_fields =  new Array();
var flow_module ='';
var fieldln = 0;
var fieldColorln = 0;
var field_count = 0;
var field_color_count = 0;

$('#flow_module').on('change',function(){
    flow_module = $('#flow_module option:selected').text();
    showModuleFields();
    clearFieldColorLines();
});//end of function

function loadConditionLine(condition,conditionVal){

    var prefix = 'aow_conditions_';
    var ln = 0;
    ln = insertConditionLine();

    for(var a in condition){
        if(document.getElementById(prefix + a + ln) != null){
            document.getElementById(prefix + a + ln).value = condition[a];
        }//end of if
    }//end of for

    var select_field = document.getElementById('aow_conditions_field'+ln);
    document.getElementById('aow_conditions_field_label'+ln).innerHTML = select_field.options[select_field.selectedIndex].text;

    document.getElementById('aow_conditions_module_path'+ln).disabled = true;
    var select_field2 = document.getElementById('aow_conditions_module_path'+ln);
    document.getElementById('aow_conditions_module_path_label'+ln).innerHTML = select_field2.options[select_field2.selectedIndex].text;

    if (conditionVal instanceof Array) {
        conditionVal = JSON.stringify(conditionVal)
    }//end of if

    showModuleField(ln, condition['operator'], condition['value_type'], conditionVal);
}//end of function

function loadFieldLine(condition){
    var ln = 0;
    ln = insertFieldLine();
    for(var a in condition){
        if(document.getElementById(a + ln) != null){
            document.getElementById(a + ln).value = condition[a];
        }//end of if
    }//end of for

    var select_field = document.getElementById('field'+ln);
    document.getElementById('field_label'+ln).innerHTML = select_field.options[select_field.selectedIndex].text;
    
    if (condition['field_value'] instanceof Array) {
        condition['field_value'] = JSON.stringify(condition['field_value'])
    }//end of if

    fieldValueDisplay(ln,condition['field_value']);
}//end of function

function loadFieldColorLine(condition){
    var ln = 0;
    ln = insertFieldColor();
    for(var a in condition){
        if(document.getElementById(a + ln) != null){
            document.getElementById(a + ln).value = condition[a];
        }//end of if
    }//end of for
    var select_field = document.getElementById('field_color_name'+ln);
    document.getElementById('field_color_name_label'+ln).innerHTML = select_field.options[select_field.selectedIndex].text;
    fieldColorValueDisplay(ln,condition['field_color']);
}//end of function


function showModuleFields(){
    clearConditionLines();
    flow_module = $('#flow_module').val();
    if(flow_module != ''){
        var callback = {
            success: function(result) {
                flow_rel_modules = result.responseText;
            }//end of success
        }//end of callback
        var callback2 = {
            success: function(result) {
                flow_fields = result.responseText;
                document.getElementById('btn_ConditionLine').disabled = '';
            }//end of success
        }//end of callback2

        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsModuleRelationships&aow_module="+flow_module,callback);
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsModuleFields&view=EditView&aow_module="+flow_module,callback2);
    }//end of if 
}//end of function


function showModuleField(ln, operator_value, type_value, field_value){
    if (typeof operator_value === 'undefined') { operator_value = ''; }
    if (typeof type_value === 'undefined') { type_value = ''; }
    if (typeof field_value === 'undefined') { field_value = ''; }

    var rel_field = document.getElementById('aow_conditions_module_path'+ln).value;
    var aow_field = document.getElementById('aow_conditions_field'+ln).value;
    var conditionalOperator = $('#conditional_operator').val();
    if(conditionalOperator == 'AND'){
        $('.fieldclass').each(function(){
            var fieldVal = $(this).val();
            fieldId = $(this).attr('id');
            aowFieldId = 'aow_conditions_field'+ln;
            if(aowFieldId != fieldId){
                if(aow_field == fieldVal){
                    $('#'+aowFieldId).val('');
                    label = SUGAR.language.get('Administration','LBL_CONDITION_FIELD_LABEL');
                    alert(label);
                }//end of if
            }//end of if
        });//end of each
    }//end of if
    var aow_field = document.getElementById('aow_conditions_field'+ln).value;
    if(aow_field != ''){
        var callback = {
            success: function(result) {
                document.getElementById('aow_conditions_operatorInput'+ln).innerHTML = result.responseText;
                SUGAR.util.evalScript(result.responseText);
                document.getElementById('aow_conditions_operatorInput'+ln).onchange = function(){changeOperator(ln);};

            },//end of success
            failure: function(result) {
                document.getElementById('aow_conditions_operatorInput'+ln).innerHTML = '';
            }//end of failure
        }//end of callback

        var callback2 = {
            success: function(result) {
                document.getElementById('aow_conditions_fieldTypeInput'+ln).innerHTML = result.responseText;
                SUGAR.util.evalScript(result.responseText);
                document.getElementById('aow_conditions_fieldTypeInput'+ln).onchange = function(){showModuleFieldType(ln);};
            },//end of success
            failure: function(result) {
                document.getElementById('aow_conditions_fieldTypeInput'+ln).innerHTML = '';
            }//end of failure
        }//end of callback2

        var callback3 = {
            success: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = result.responseText;
                SUGAR.util.evalScript(result.responseText);
                enableQS(true);
            },//end of success
            failure: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = '';
            }//end of failure
        }//end of callback3

        var aow_operator_name = "aow_conditions_operator["+ln+"]";
        var aow_field_type_name = "aow_conditions_value_type["+ln+"]";
        var aow_field_name = "aow_conditions_value["+ln+"]";

        if(type_value == 'Field'){
            var callback4 = {
                success: function(result) {
                    document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = "<select type='text'  name='"+aow_field_name+"' id='"+aow_field_name+"' title='' tabindex='116' onchange='checkFieldType("+ln+");'>"+result.responseText+"</select>";
                    $('select[name="'+aow_field_name+'"]').val(field_value);
                    SUGAR.util.evalScript(result.responseText);
                    enableQS(true);
                },
                failure: function(result) {
                    document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = "<select type='text'  name='"+aow_field_name+"' id='"+aow_field_name+"' title='' tabindex='116'></select>";
                }
            }
            YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsModuleFields&view=EditView&aow_module="+flow_module+"&type=1&fieldName="+aow_field,callback4);
        }else{
            YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsModuleFieldType&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+aow_field+"&aow_newfieldname="+aow_field_name+"&aow_value="+field_value+"&aow_type="+type_value+"&rel_field="+rel_field,callback3);
        }

        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsModuleOperatorField&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+aow_field+"&aow_newfieldname="+aow_operator_name+"&aow_value="+operator_value+"&rel_field="+rel_field,callback);
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsFieldTypeOptions&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+aow_field+"&aow_newfieldname="+aow_field_type_name+"&aow_value="+type_value+"&rel_field="+rel_field,callback2);
        //YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsModuleFieldType&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+aow_field+"&aow_newfieldname="+aow_field_name+"&aow_value="+field_value+"&aow_type="+type_value+"&rel_field="+rel_field,callback3);
    } else {
        document.getElementById('aow_conditions_operatorInput'+ln).innerHTML = ''
        document.getElementById('aow_conditions_fieldTypeInput'+ln).innerHTML = '';
        document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = '';
    }//end of else

    if(operator_value == 'is_null' || operator_value == 'is_not_null'){
        hideElem('aow_conditions_fieldTypeInput' + ln);
        hideElem('aow_conditions_fieldInput' + ln);
    } else {
        showElem('aow_conditions_fieldTypeInput' + ln);
        showElem('aow_conditions_fieldInput' + ln);
    }//end of else
}//end of function

function showModuleFieldType(ln, value){
    if (typeof value === 'undefined') { value = ''; }
    var rel_field = document.getElementById('aow_conditions_module_path'+ln).value;
    var aow_field = document.getElementById('aow_conditions_field'+ln).value;
    var type_value = document.getElementById("aow_conditions_value_type["+ln+"]").value;
    var aow_field_name = "aow_conditions_value["+ln+"]";

    if(type_value == 'Field'){
        var callback4 = {
            success: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = "<select type='text'  name='"+aow_field_name+"' id='"+aow_field_name+"' onchange='checkFieldType("+ln+");' title='' tabindex='116'>"+result.responseText+"</select>";
                SUGAR.util.evalScript(result.responseText);
                enableQS(true);
            },
            failure: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = "<select type='text'  name='"+aow_field_name+"' id='"+aow_field_name+"'  title='' tabindex='116'></select>";
            }
        }
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsModuleFields&view=EditView&aow_module="+flow_module+"&type=1&fieldName="+aow_field,callback4);
    }else{
        var callback = {
            success: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = result.responseText;
                SUGAR.util.evalScript(result.responseText);
                enableQS(false);
            },
            failure: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = '';
            }
        }
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsModuleFieldType&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+aow_field+"&aow_newfieldname="+aow_field_name+"&aow_value="+value+"&aow_type="+type_value+"&rel_field="+rel_field,callback);
    }
}//end of function
 
function insertConditionHeader(){
    tablehead = document.createElement("thead");
    tablehead.id = "conditionLines_head";
    document.getElementById('aow_conditionLines').appendChild(tablehead);

    var x=tablehead.insertRow(-1);
    x.id='conditionLines_head';

    var a=x.insertCell(0);
    
    var b=x.insertCell(1);
    b.style.color="rgb(0,0,0)";
    b.innerHTML= SUGAR.language.get('Administration', 'LBL_MODULE_PATH');

    var c=x.insertCell(2);
    c.style.color="rgb(0,0,0)";
    c.innerHTML= SUGAR.language.get('Administration', 'LBL_FIELD');

    var d=x.insertCell(3);
    d.style.color="rgb(0,0,0)";
    d.innerHTML= SUGAR.language.get('Administration', 'LBL_OPERATOR');

    var e=x.insertCell(4);
    e.style.color="rgb(0,0,0)";
    e.innerHTML= SUGAR.language.get('Administration', 'LBL_VALUE_TYPE');

    var f=x.insertCell(5);
    f.style.color="rgb(0,0,0)";
    f.innerHTML= SUGAR.language.get('Administration', 'LBL_VALUE');
}//end of function

function insertFieldHeader(){
    tablehead = document.createElement("thead");
    tablehead.id = "fieldLines_head";
    document.getElementById('fieldLines').appendChild(tablehead);

    var x=tablehead.insertRow(-1);
    x.id='fieldLines_head';
    var a=x.insertCell(0);

    var b=x.insertCell(1);
    b.style.color="rgb(0,0,0)";
    b.innerHTML= SUGAR.language.get('Administration', 'LBL_FIELD');

    var c=x.insertCell(2);
    c.style.color="rgb(0,0,0)";
    c.innerHTML = SUGAR.language.get('Administration', 'LBL_VALUE');
}//end of function

function insertFieldColorHeader(){
    tablehead = document.createElement("thead");
    tablehead.id = "fieldColor_head";
    document.getElementById('fieldColor').appendChild(tablehead);

    var x=tablehead.insertRow(-1);
    x.id='fieldColor_head';
    var a=x.insertCell(0);

    var b=x.insertCell(1);
    b.style.color="rgb(0,0,0)";
    b.innerHTML= SUGAR.language.get('Administration', 'LBL_FIELD');

    var c=x.insertCell(2);
    c.style.color="rgb(0,0,0)";
    c.innerHTML = SUGAR.language.get('Administration', 'LBL_VALUE');
}//end of function

function insertConditionLine(){
    if (document.getElementById('conditionLines_head') == null) {
        insertConditionHeader();
    } else {
        document.getElementById('conditionLines_head').style.display = '';
    }//end of else
    var rowc  = $("#aow_conditionLines > tbody > tr").length;
    condln = rowc;
    tablebody = document.createElement("tbody");
    tablebody.id = "aow_conditions_body" + condln;
    document.getElementById('aow_conditionLines').appendChild(tablebody);


    var x = tablebody.insertRow(-1);
    x.id = 'product_line' + condln;

    var a = x.insertCell(0);
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){
        a.innerHTML = "<button type='button' id='aow_conditions_delete_line" + condln + "' class='button' value='' tabindex='116' onclick='markConditionLineDeleted(" + condln + ")'><span class='suitepicon suitepicon-action-minus'></span></button><br>";
        a.innerHTML += "<input type='hidden' name='aow_conditions_deleted[" + condln + "]' id='aow_conditions_deleted" + condln + "' value='0'><input type='hidden' name='aow_conditions_id[" + condln + "]' id='aow_conditions_id" + condln + "' value=''>";
    } else{
        a.innerHTML = condln +1;
    }//end of else

    var b = x.insertCell(1);
    var viewStyle = 'display:none';
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){viewStyle = '';}
    b.innerHTML = "<select style='"+viewStyle+"' name='aow_conditions_module_path["+ condln +"][0]' id='aow_conditions_module_path" + condln + "' value='' title='' tabindex='116' disabled = 'disabled'>" + flow_rel_modules + "</select>";
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){viewStyle = 'display:none';}else{viewStyle = '';}
    b.innerHTML += "<span style='"+viewStyle+"' id='aow_conditions_module_path_label" + condln + "' ></span>";

    var c = x.insertCell(2);
    viewStyle = 'display:none';
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){viewStyle = '';}
    c.innerHTML = "<select style='"+viewStyle+"' name='aow_conditions_field["+ condln +"]' id='aow_conditions_field" + condln + "' value='' title='' tabindex='116' onchange='showModuleField(" + condln + ");' class='fieldclass'>" + flow_fields + "</select>";
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){viewStyle = 'display:none';}else{viewStyle = '';}
    c.innerHTML += "<span style='"+viewStyle+"' id='aow_conditions_field_label" + condln + "' ></span>";


    var d = x.insertCell(3);
    d.id='aow_conditions_operatorInput'+condln;

    var e = x.insertCell(4);
    e.id='aow_conditions_fieldTypeInput'+condln;

    var f = x.insertCell(5);
    f.id='aow_conditions_fieldInput'+condln;

    condln++;
    condln_count++;

    $('.edit-view-field #aow_conditionLines').find('tbody').last().find('select').change(function () {
        $(this).find('td').last().removeAttr("style");
        $(this).find('td').height($(this).find('td').last().height() + 8);
    });

    return condln -1;
}//end of function

function insertFieldLine(){
    if (document.getElementById('fieldLines_head') == null) {
        insertFieldHeader();
    } else {
        document.getElementById('fieldLines_head').style.display = '';
    }//end of else
    
    var rowc  = $("#fieldLines > tbody > tr").length;
    fieldln = rowc;

    tablebody = document.createElement("tbody");
    tablebody.id = "field_body" + fieldln;
    document.getElementById('fieldLines').appendChild(tablebody);

    var x = tablebody.insertRow(-1);
    x.id = 'field_line' + fieldln;
    var a = x.insertCell(0);
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){
        a.innerHTML = "<button type='button' id='field_delete_line" + fieldln + "' class='button' value='' tabindex='116' onclick='markFieldLineDeleted(" + fieldln + ")'><span class='suitepicon suitepicon-action-minus'></span></button><br>";
        a.innerHTML += "<input type='hidden' name='field_deleted[" + fieldln + "]' id='field_deleted" + fieldln + "' value='0'><input type='hidden' name='field_id[" + fieldln + "]' id='field_id" + fieldln + "' value=''>";
    } else{
        a.innerHTML = fieldln +1;
    }//end of else

    var b = x.insertCell(1);
    viewStyle = 'display:none';
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){viewStyle = '';}
    b.innerHTML = "<select style='"+viewStyle+"' name='field["+ fieldln +"]' id='field" + fieldln + "' value='' title='' tabindex='116' onchange='fieldValue(" + fieldln + ");'>" + flow_fields + "</select>";
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){viewStyle = 'display:none';}else{viewStyle = '';}
    b.innerHTML += "<span style='"+viewStyle+"' id='field_label" + fieldln + "' ></span>";

    var c = x.insertCell(2);
    c.id='field_value'+fieldln;

    fieldln++;
    field_count++;
    $('#fieldLines').find('tbody').last().find('select').change(function () {
        $(this).find('td').last().removeAttr("style");
        $(this).find('td').height($(this).find('td').last().height() + 8);
    });

    return fieldln -1;
}//end of function

function insertFieldColor(){
    if (document.getElementById('fieldColor_head') == null) {
        insertFieldColorHeader();
    } else {
        document.getElementById('fieldColor_head').style.display = '';
    }//end of else
    var rowc  = $("#fieldColor > tbody > tr").length;
    fieldColorln = rowc;

    tablebody = document.createElement("tbody");
    tablebody.id = "fieldColor_body" + fieldColorln;
    document.getElementById('fieldColor').appendChild(tablebody);

    var x = tablebody.insertRow(-1);
    x.id = 'fieldcolor_line' + fieldColorln;
    var a = x.insertCell(0);
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){
        a.innerHTML = "<button type='button' id='field_color_delete_line" + fieldColorln + "' class='button' value='' tabindex='116' onclick='markFieldColorLineDeleted(" + fieldColorln + ")'><span class='suitepicon suitepicon-action-minus'></span></button><br>";
        a.innerHTML += "<input type='hidden' name='field_color_deleted[" + fieldColorln + "]' id='field_color_deleted" + fieldColorln + "' value='0'><input type='hidden' name='field_color_id[" + fieldColorln + "]' id='field_color_id" + fieldColorln + "' value=''>";
    } else{
        a.innerHTML = fieldColorln +1;
    }//end of else

    var b = x.insertCell(1);
    viewStyle = 'display:none';
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){viewStyle = '';}
    b.innerHTML = "<select style='"+viewStyle+"' name='field_color_name["+ fieldColorln +"]' id='field_color_name" + fieldColorln + "' value='' title='' tabindex='116' onchange='fieldColorValue(" + fieldColorln + ");'>" + flow_fields + "</select>";
    if(action_sugar_grp1 == 'vi_dynamicpanelseditview'){viewStyle = 'display:none';}else{viewStyle = '';}
    b.innerHTML += "<span style='"+viewStyle+"' id='field_color_name_label" + fieldColorln + "' ></span>";

    var c = x.insertCell(2); 
    c.id='field_color'+fieldColorln;
    fieldColorln++;
    field_color_count++;

    $('#fieldColor').find('tbody').last().find('select').change(function () {
        $(this).find('td').last().removeAttr("style");
        $(this).find('td').height($(this).find('td').last().height() + 8);
    });//end of function

    return fieldColorln -1;
}//end of function

function fieldValue(ln,field_value){
    field_value = '';
    var field = document.getElementById('field'+ln).value;
    if(field != ''){
        var callback3 = {
            success: function(result) {
                document.getElementById('field_value'+ln).innerHTML = result.responseText;
                SUGAR.util.evalScript(result.responseText);
                enableQS(true);
            },//end of success
            failure: function(result) {
                document.getElementById('field_value'+ln).innerHTML = '';
            }//end of failure
        }//end of callback
        var fieldvalue = "field_value["+ln+"]";
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsModuleFieldValue&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+field+"&aow_newfieldname="+fieldvalue+"&aow_value="+field_value+"&aow_type=value&rel_field="+flow_module,callback3);
        
    } else {
        document.getElementById('field_value'+ln).innerHTML = '';
    }//end of else
}//end of function

function fieldValueDisplay(ln,field_value){
    var field = document.getElementById('field'+ln).value;
    if(field != ''){
        var callback3 = {
            success: function(result) {
                document.getElementById('field_value'+ln).innerHTML = result.responseText;
                SUGAR.util.evalScript(result.responseText);
                enableQS(true);
            },//end of success
            failure: function(result) {
                document.getElementById('field_value'+ln).innerHTML = '';
            }//end of failure
        }//end of callback3
        var fieldvalue = "field_value["+ln+"]";
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsModuleFieldValue&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+field+"&aow_newfieldname="+fieldvalue+"&aow_value="+field_value+"&aow_type=value&rel_field="+flow_module,callback3);
    } else {
        document.getElementById('field_value'+ln).innerHTML = '';
    }//end of else
}//end of function

function fieldColorValue(ln,field_value) {
    field_value = '';
    var field = document.getElementById('field_color_name'+ln).value;
    if(field != ''){
        document.getElementById('field_color'+ln).innerHTML = '<input type="text" name="field_color['+ ln +']" id="field_color'+ ln +'" size="30" maxlength="150" value="" title="" class="color">';
    } else {
        document.getElementById('field_color'+ln).innerHTML = '';
    }//end of else
}//end of function

function fieldColorValueDisplay(ln,field_value) {

    var field = document.getElementById('field_color_name'+ln).value;
    if(field != ''){
        document.getElementById('field_color'+ln).innerHTML = '<input type="text" name="field_color['+ ln +']" id="field_color'+ ln +'" size="30" maxlength="150" value="'+field_value+'" title="" class="color">';
    } else {
        document.getElementById('field_color'+ln).innerHTML = '';
    }//end of else
}//end of function

function changeOperator(ln){
    var aow_operator = document.getElementById("aow_conditions_operator["+ln+"]").value;

    if(aow_operator == 'is_null' || aow_operator == 'is_not_null'){
        showModuleField(ln,aow_operator);
    } else {
        showElem('aow_conditions_fieldTypeInput' + ln);
        showElem('aow_conditions_fieldInput' + ln);
    }//end of else
}//end of function

function markConditionLineDeleted(ln){
    // collapse line; update deleted value
    document.getElementById('aow_conditions_body' + ln).style.display = 'none';
    document.getElementById('aow_conditions_deleted' + ln).value = '1';
    document.getElementById('aow_conditions_delete_line' + ln).onclick = '';

    condln_count--;
    if(condln_count == 0){
        document.getElementById('conditionLines_head').style.display = "none";
    }//end of if
}//end of function

function clearConditionLines(){
    if(document.getElementById('aow_conditionLines') != null){
        var cond_rows = document.getElementById('aow_conditionLines').getElementsByTagName('tr');
        var cond_row_length = cond_rows.length;
        var i;
        for (i=0; i < cond_row_length; i++) {
            if(document.getElementById('aow_conditions_delete_line'+i) != null){
                document.getElementById('aow_conditions_delete_line'+i).click();
            }//end of if
        }//end of for
    }//end of if
}//end of function

function clearFieldColorLines(){
    if(document.getElementById('fieldColor') != null){
        var cond_rows = document.getElementById('fieldColor').getElementsByTagName('tr');
        var cond_row_length = cond_rows.length;
        var i;
        for (i=0; i < cond_row_length; i++) {
            if(document.getElementById('field_color_delete_line'+i) != null){
                document.getElementById('field_color_delete_line'+i).click();
            }//end of if
        }//end of for
    }//end of if
}//end of function

function hideElem(id){
    if(document.getElementById(id)){
        document.getElementById(id).style.display = "none";
        document.getElementById(id).value = "";
    }//end of if
}//end of function

function showElem(id){
    if(document.getElementById(id)){
        document.getElementById(id).style.display = "";
    }//end of if
}//end of function

function markFieldLineDeleted(ln){
    // collapse line; update deleted value
    document.getElementById('field_body' + ln).style.display = 'none';
    document.getElementById('field_deleted' + ln).value = '1';
    document.getElementById('field_delete_line' + ln).onclick = '';

    field_count--;
    if(field_count == 0){
        document.getElementById('fieldLines_head').style.display = "none";
    }//end of if
}//end of function

function markFieldColorLineDeleted(ln){
    // collapse line; update deleted value
    document.getElementById('fieldColor_body' + ln).style.display = 'none';
    document.getElementById('field_color_deleted' + ln).value = '1';
    document.getElementById('field_color_delete_line' + ln).onclick = '';
    $('#fieldColor_body'+ln).remove();
    field_color_count--;
    if(field_color_count == 0){
        document.getElementById('fieldColor_head').stringifyyle.display = "none";
    }//end of if
}//end of function

function date_field_change(field){
    if(document.getElementById(field + '[1]').value == 'now'){
        hideElem(field + '[2]');
        hideElem(field + '[3]');
    } else {
        showElem(field + '[2]');
        showElem(field + '[3]');
    }
}

function checkFieldType(line){
    var moduleName = $('#flow_module').val();
    var fieldName = $('#aow_conditions_field'+line).val();
    var fieldVal = $('select[id="aow_conditions_value['+line+']"').val();
    
    $.ajax({
        url:"index.php?entryPoint=VIDynamicPanelsCheckValidFieldType",
        type:"POST",
        data:{moduleName: moduleName,
              fieldName : fieldName,
              fieldVal : fieldVal},
        success:function(result){
            if(result == '0'){
                alert(SUGAR.language.get('Administration','LBL_CHECK_VALID_FIELD_TYPE'));
                $('select[id="aow_conditions_value['+line+']"').val('');
            }
        }
    });
}


