/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
var fieldCondln = 0;
var targetModuleFields = Array();
var sourceModuleFields = Array();

function loadFieldMappingLine(condition){
	var ln = 0;
    ln = insertFieldMappingBlock(condition['source_module'],condition['sourceModuleLabel'],condition['related_field_name'],condition['relatedFieldLabel'],condition['related_field_module'],condition['relatedModuleNameLabel'],condition['source_module_fields'],condition['target_module_fields']);
}//end of function

function insertFieldMappingHeader(sourceModuleLable,relateFieldName,relateFieldLabel,relatedFieldModuleLable){
    tablehead = document.createElement("thead");
    tablehead.id = relateFieldName+"_conditionLines_head";
    tablehead.className = 'condition_lines_head';
    document.getElementById(relateFieldName+'_conditionLines').appendChild(tablehead);

    var x=tablehead.insertRow(-1);
    x.id= relateFieldName+'_conditionLines_head';

    var a=x.insertCell(0);

    var b=x.insertCell(1);
    b.style.color="rgb(0,0,0)";
    targetModuleFieldsLabel = SUGAR.language.get('Administration', 'LBL_TARGET_MODULE')+' ('+relatedFieldModuleLable+' - '+relateFieldLabel+' )'+' '+SUGAR.language.get('Administration', 'LBL_FIELDS');
    b.innerHTML= targetModuleFieldsLabel;

    var c=x.insertCell(2);
    c.style.color="rgb(0,0,0)";
    c.innerHTML= SUGAR.language.get('Administration', 'LBL_SOURCE_MODULE')+' ('+sourceModuleLable+') '+' '+SUGAR.language.get('Administration', 'LBL_FIELDS');
}//end of function

function  insertFieldMappingBlock(sourceModuleName, sourceModuleLable, relateFieldName, relateFieldLabel, relatedFieldModuleName, relatedFieldModuleLable,sourceModuleFieldVal,targetModuleFieldVal) {
	SUGAR.ajaxUI.showLoadingPanel(); // show loader
	$.ajax({
		url:"index.php?entryPoint=VIAutoPopulateFieldsSourceTargetModuleFields",
		dataType:"JSON",
		data:{sourceModule : sourceModuleName,
		      targetModule : relatedFieldModuleName},
		success:function(response){
			sourceModuleFields = response['sourceModuleFieldsList'];
			targetModuleFields = response['targetModuleFieldsList'];
			SUGAR.ajaxUI.hideLoadingPanel(); // hide loader
			insertFieldMappingLine(sourceModuleName, sourceModuleLable, relateFieldName, relateFieldLabel, relatedFieldModuleName, relatedFieldModuleLable);
			cnt = fieldCondln - 1;
			$('#'+relateFieldName+'_source_module_fields'+cnt).val(sourceModuleFieldVal);
    		$('#'+relateFieldName+'_target_module_fields'+cnt).val(targetModuleFieldVal);
		}//end of success
	});//end of ajax
}//end of function

function checkMappingFieldType(fieldCnt, typeLbl, sourceModuleName, relateFieldModule,relateFieldName) {
    var targetModuleField = $('#'+relateFieldName+'_target_module_fields'+fieldCnt).val();
    var targetModuleFieldText = $('#'+relateFieldName+'_target_module_fields'+fieldCnt+' option:selected').text();
    var sourceModuleField = $('#'+relateFieldName+'_source_module_fields'+fieldCnt).val();
    var checkDuplicateField = 0;
    $('.'+relateFieldName+'_targetFieldsClass').each(function(){
        var fieldVal = $(this).val();
        fieldId = $(this).attr('id');
        if($(this).closest('tbody').css('display') != 'none'){
            fieldMappingId = relateFieldName+'_target_module_fields'+fieldCnt;
            if(fieldMappingId != fieldId){
                if(targetModuleField != '' && fieldVal != ''){
                    if(targetModuleField == fieldVal){
                        $('#'+fieldMappingId).val('');
                        checkDuplicateField = 1;
                    }//end of if
                }//end of if
            }//end of if
        }//end of if
    });//end of each

    if(checkDuplicateField == 0){
        if(sourceModuleField != '' && targetModuleField != ''){
            $.ajax({
                url: 'index.php?entryPoint=VIAutoPopulateFieldsMappingFieldType',
                type: 'POST',
                data: {sourceModuleField : sourceModuleField,
                       targetModuleField : targetModuleField,
                       sourceModuleName : sourceModuleName,
                       relateFieldModule : relateFieldModule},  
                success: function(response){
                    if(response == '0'){
                        alert(SUGAR.language.get('Administration','LBL_FIELD_TYPE_MATCH_VALIDATION'));
                    
                        if(typeLbl == 'target'){
                            $('#'+relateFieldName+'_target_module_fields'+fieldCnt).val('');
                        }else{
                            $('#'+relateFieldName+'_source_module_fields'+fieldCnt).val('');
                        }//end of else
                    }//end of if
                }//end of success
            });//end of ajax
        }//end of if
    }else{
        alert(targetModuleFieldText+SUGAR.language.get('Administration','LBL_FIELD_MAPPING_VALIDATION'));
    }//end of else
}//end of function

function insertFieldMappingLine(sourceModuleName, sourceModuleLable, relateFieldName, relateFieldLabel, relatedFieldModuleName, relatedFieldModuleLable){
	if (document.getElementById(relateFieldName+'_conditionLines_head') == null) {
    	insertFieldMappingHeader(sourceModuleLable,relateFieldName,relateFieldLabel,relatedFieldModuleLable);
    } else {
        document.getElementById(relateFieldName+'_conditionLines_head').style.display = '';
    }//end of else

    var rowCount  = $("#"+relateFieldName+"_conditionLines > tbody > tr").length;
    fieldCondln = rowCount;
    
    tablebody = document.createElement("tbody");
    tablebody.id = relateFieldName+"_conditions_body" + fieldCondln;
    document.getElementById(relateFieldName+'_conditionLines').appendChild(tablebody);

    var x = tablebody.insertRow(-1);
    x.id = relateFieldName+'_product_line' + fieldCondln;
    var relateFieldNameVal = '"'+relateFieldName+'"';
    var a = x.insertCell(0);
    a.innerHTML = "<button type='button' id='"+relateFieldName+"_conditions_delete_line" + fieldCondln + "' class='button' value='' tabindex='116' onclick='markFieldMappingLineDeleted(" + fieldCondln + ","+relateFieldNameVal+")'><span class='suitepicon suitepicon-action-minus'></span></button><br>";
    a.innerHTML += "<input type='hidden' name='"+relateFieldName+"_conditions_deleted[" + fieldCondln + "]' id='"+relateFieldName+"_conditions_deleted" + fieldCondln + "' value='0'><input type='hidden' name='"+relateFieldName+"_conditions_id[" + fieldCondln + "]' id='"+relateFieldName+"_conditions_id" + fieldCondln + "' value=''>";
    
    targetLbl = '"target"';
   	sourceLbl = '"source"';

   	sourceModuleNameVal = '"'+sourceModuleName+'"';
   	relatedFieldModuleNameVal = '"'+relatedFieldModuleName+'"';
   	relateFieldNameVal = '"'+relateFieldName+'"';

    var b = x.insertCell(1);
    b.innerHTML = "<select name='"+relateFieldName+"_target_module_fields["+ fieldCondln +"]' id='"+relateFieldName+"_target_module_fields" + fieldCondln + "' class='"+relateFieldName+"_targetFieldsClass' value='' tabindex='116' onchange='checkMappingFieldType("+fieldCondln+","+targetLbl+","+sourceModuleNameVal+","+relatedFieldModuleNameVal+","+relateFieldNameVal+");'>" + targetModuleFields + "</select>";
    b.innerHTML += "<span id='"+relateFieldName+"_target_module_fields_label" + fieldCondln + "' ></span>";

    var c = x.insertCell(2);
    c.innerHTML = "<select name='"+relateFieldName+"_source_module_fields["+ fieldCondln +"]' id='"+relateFieldName+"_source_module_fields" + fieldCondln + "' value='' tabindex='116' onchange='checkMappingFieldType("+fieldCondln+","+sourceLbl+","+sourceModuleNameVal+","+relatedFieldModuleNameVal+","+relateFieldNameVal+");'>" + sourceModuleFields + "</select>";
    c.innerHTML += "<span id='"+relateFieldName+"_source_module_fields_label" + fieldCondln + "' ></span>";

    fieldCondln++;

    $('.edit-view-field #'+relateFieldName+'_conditionLines').find('tbody').last().find('select').change(function () {
        $(this).find('td').last().removeAttr("style");
        $(this).find('td').height($(this).find('td').last().height() + 8);
    });

    return fieldCondln -1;
}//end of function

function markFieldMappingLineDeleted(ln,relatedFieldName){
	// collapse line; update deleted value
    document.getElementById(relatedFieldName+'_conditions_body' + ln).style.display = 'none';
    document.getElementById(relatedFieldName+'_conditions_deleted' + ln).value = '1';
    document.getElementById(relatedFieldName+'_conditions_delete_line' + ln).onclick = '';
}//end of function