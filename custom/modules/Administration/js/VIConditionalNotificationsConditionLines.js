/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
var condln = 0;
var condln_count = 0;
var flow_fields =  new Array();
var flow_module ='';
var fieldln = 0;
var field_count = 0;

$('#flow_module').on('change',function(){
    flow_module = $('#flow_module option:selected').text();
    showModuleFields();
});

function loadConditionLine(condition,conditionVal){
    var prefix = 'aow_conditions_';
    var ln = 0;
    ln = insertConditionLine();

    for(var a in condition){
        if(document.getElementById(prefix + a + ln) != null){
            document.getElementById(prefix + a + ln).value = condition[a];
        }
    }

    var select_field = document.getElementById('aow_conditions_field'+ln);
    document.getElementById('aow_conditions_field_label'+ln).innerHTML = select_field.options[select_field.selectedIndex].text;

    document.getElementById('aow_conditions_module_path'+ln).disabled = true;
    var select_field2 = document.getElementById('aow_conditions_module_path'+ln);
    document.getElementById('aow_conditions_module_path_label'+ln).innerHTML = select_field2.options[select_field2.selectedIndex].text;

    if (conditionVal instanceof Array) {
        conditionVal = JSON.stringify(conditionVal)
    }
    
    document.getElementById('aow_conditions_regular_expression['+ln+']').value = condition['regular_expression'];
    showModuleField(ln, condition['operator'], condition['value_type'], conditionVal)
}

function showModuleFields(){
    clearConditionLines();
    flow_module = $('#flow_module').val();
    if(flow_module != ''){
        var callback = {
            success: function(result) {
                flow_rel_modules = result.responseText;
            }
        }
        var callback2 = {
            success: function(result) {
                flow_fields = result.responseText;
                document.getElementById('btn_ConditionLine').disabled = '';
            }
        }
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIConditionalNotificationsModuleRelationships&aow_module="+flow_module,callback);
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIConditionalNotificationModuleFields&view=EditView&aow_module="+flow_module,callback2);
    }
}

function showConditionCurrentModuleFields(ln, value){
    if (typeof value === 'undefined') { value = ''; }
    flow_module = document.getElementById('flow_module').value;
    var rel_field = document.getElementById('aow_conditions_module_path' + ln).value;

    if(flow_module != '' && rel_field != ''){
        var callback = {
            success: function(result) {
                var fields = JSON.parse(result.responseText);

                document.getElementById('aow_conditions_field'+ln).innerHTML = '';

                var selector = document.getElementById('aow_conditions_field'+ln);
                for (var i in fields) {
                    selector.options[selector.options.length] = new Option(fields[i], i);
                }
                if(fields[value] != null ){
                    document.getElementById('aow_conditions_field'+ln).value = value;
                }
                if(value == '') showModuleField(ln);
            }
        }

        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIConditionalNotificationModuleFields&aow_module="+flow_module+"&view=JSON&rel_field="+rel_field+"&aow_value="+value,callback);
    }
}

function showModuleField(ln, operator_value, type_value, field_value){
    if (typeof operator_value === 'undefined') { operator_value = ''; }
    if (typeof type_value === 'undefined') { type_value = ''; }
    if (typeof field_value === 'undefined') { field_value = ''; }

    var rel_field = document.getElementById('aow_conditions_module_path'+ln).value;
    var aow_field = document.getElementById('aow_conditions_field'+ln).value;
    if(aow_field != ''){

        var callback = {
            success: function(result) {
                document.getElementById('aow_conditions_operatorInput'+ln).innerHTML = result.responseText;
                SUGAR.util.evalScript(result.responseText);
                document.getElementById('aow_conditions_operatorInput'+ln).onchange = function(){changeOperator(ln);};

            },
            failure: function(result) {
                document.getElementById('aow_conditions_operatorInput'+ln).innerHTML = '';
            }
        }
        var callback2 = {
            success: function(result) {
                document.getElementById('aow_conditions_fieldTypeInput'+ln).innerHTML = result.responseText;
                SUGAR.util.evalScript(result.responseText);
                document.getElementById('aow_conditions_fieldTypeInput'+ln).onchange = function(){showModuleFieldType(ln);};
            },
            failure: function(result) {
                document.getElementById('aow_conditions_fieldTypeInput'+ln).innerHTML = '';
            }
        }
        var callback3 = {
            success: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = result.responseText;
                SUGAR.util.evalScript(result.responseText);
                enableQS(true);
            },
            failure: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = '';
            }
        }

        var aow_operator_name = "aow_conditions_operator["+ln+"]";
        var aow_field_type_name = "aow_conditions_value_type["+ln+"]";
        var aow_field_name = "aow_conditions_value["+ln+"]";
        if(type_value == 'Field'){
            var callback4 = {
                success: function(result) {
                    document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = "<select type='text'  name='"+aow_field_name+"' id='"+aow_field_name+"' title='' tabindex='116'>"+result.responseText+"</select>";
                    $('select[name="'+aow_field_name+'"]').val(field_value);
                    SUGAR.util.evalScript(result.responseText);
                    enableQS(true);
                },
                failure: function(result) {
                    document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = "<select type='text'  name='"+aow_field_name+"' id='"+aow_field_name+"' title='' tabindex='116'></select>";
                }
            }
            YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIConditionalNotificationModuleFields&view=EditView&aow_module="+flow_module+"&type=1&fieldName="+aow_field,callback4);
        }else{
            YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIConditionalNotificationsModuleFieldType&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+aow_field+"&aow_newfieldname="+aow_field_name+"&aow_value="+field_value+"&aow_type="+type_value+"&rel_field="+rel_field,callback3);
        }
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIConditionalNotificationsModuleOperatorField&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+aow_field+"&aow_newfieldname="+aow_operator_name+"&aow_value="+operator_value+"&rel_field="+rel_field,callback);
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIConditionalNotificationsFieldTypeOptions&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+aow_field+"&aow_newfieldname="+aow_field_type_name+"&aow_value="+type_value+"&rel_field="+rel_field+"&operator_value="+operator_value,callback2);
    } else {
        document.getElementById('aow_conditions_operatorInput'+ln).innerHTML = ''
        document.getElementById('aow_conditions_fieldTypeInput'+ln).innerHTML = '';
        document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = '';
    }

    if(operator_value == 'is_null'){
        hideElem('aow_conditions_fieldTypeInput' + ln);
        hideElem('aow_conditions_fieldInput' + ln);
    } else if(operator_value == 'regular_expression'){
        hideElem('aow_conditions_fieldInput' + ln);
        showElem('aow_conditions_fieldTypeInput' + ln);
        showElem('aow_conditions_regular_expression['+ln+']');
    } else {
        hideElem('aow_conditions_regular_expression['+ln+']');
        showElem('aow_conditions_fieldTypeInput' + ln);
        showElem('aow_conditions_fieldInput' + ln);
    }
}

function showModuleFieldType(ln, value){
    if (typeof value === 'undefined') { value = ''; }
    var rel_field = document.getElementById('aow_conditions_module_path'+ln).value;
    var aow_field = document.getElementById('aow_conditions_field'+ln).value;
    var type_value = document.getElementById("aow_conditions_value_type["+ln+"]").value;
    var aow_field_name = "aow_conditions_value["+ln+"]";

    if(type_value == 'Field'){
        var callback4 = {
            success: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = "<select type='text'  name='"+aow_field_name+"' id='"+aow_field_name+"' title='' tabindex='116'>"+result.responseText+"</select>";
                SUGAR.util.evalScript(result.responseText);
                enableQS(true);
            },
            failure: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = "<select type='text'  name='"+aow_field_name+"' id='"+aow_field_name+"' title='' tabindex='116'></select>";
            }
        }
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIConditionalNotificationModuleFields&view=EditView&aow_module="+flow_module+"&type=1&fieldName="+aow_field,callback4);
    }else{
        var callback = {
            success: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = result.responseText;
                SUGAR.util.evalScript(result.responseText);
                enableQS(false);
            },
            failure: function(result) {
                document.getElementById('aow_conditions_fieldInput'+ln).innerHTML = '';
            }
        }
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIConditionalNotificationsModuleFieldType&view="+action_sugar_grp1+"&aow_module="+flow_module+"&aow_fieldname="+aow_field+"&aow_newfieldname="+aow_field_name+"&aow_value="+value+"&aow_type="+type_value+"&rel_field="+rel_field,callback);
    }
}
 
function insertConditionHeader(){
    tablehead = document.createElement("thead");
    tablehead.id = "conditionLines_head";
    document.getElementById('aow_conditionLines').appendChild(tablehead);

    var x=tablehead.insertRow(-1);
    x.id='conditionLines_head';

    var a=x.insertCell(0);
    
    var b=x.insertCell(1);
    b.style.color="rgb(0,0,0)";
    b.innerHTML= SUGAR.language.get('Administration', 'LBL_MODULE_PATH');

    var c=x.insertCell(2);
    c.style.color="rgb(0,0,0)";
    c.innerHTML= SUGAR.language.get('Administration', 'LBL_FIELD');

    var d=x.insertCell(3);
    d.style.color="rgb(0,0,0)";
    d.innerHTML= SUGAR.language.get('Administration', 'LBL_OPERATOR');

    var e=x.insertCell(4);
    e.style.color="rgb(0,0,0)";
    e.innerHTML= SUGAR.language.get('Administration', 'LBL_VALUE_TYPE');

    var f=x.insertCell(5);
    f.style.color="rgb(0,0,0)";
    f.innerHTML= SUGAR.language.get('Administration', 'LBL_VALUE');

    var g=x.insertCell(6);
    g.style.color="rgb(0,0,0)";

}

function insertConditionLine(){
    if (document.getElementById('conditionLines_head') == null) {
        insertConditionHeader();
    } else {
        document.getElementById('conditionLines_head').style.display = '';
    }

    var rowc  = $("#aow_conditionLines > tbody > tr").length;
    condln = rowc;
    tablebody = document.createElement("tbody");
    tablebody.id = "aow_conditions_body" + condln;
    document.getElementById('aow_conditionLines').appendChild(tablebody);

    var x = tablebody.insertRow(-1);
    x.id = 'product_line' + condln;

    var a = x.insertCell(0);
    if(action_sugar_grp1 == 'vi_conditionalnotificationseditview'){
        a.innerHTML = "<button type='button' id='aow_conditions_delete_line" + condln + "' class='button' value='' tabindex='116' onclick='markConditionLineDeleted(" + condln + ")'><span class='suitepicon suitepicon-action-minus'></span></button><br>";
        a.innerHTML += "<input type='hidden' name='aow_conditions_deleted[" + condln + "]' id='aow_conditions_deleted" + condln + "' value='0'><input type='hidden' name='aow_conditions_id[" + condln + "]' id='aow_conditions_id" + condln + "' value=''>";
    } else{
        a.innerHTML = condln +1;
    }

    var b = x.insertCell(1);
    var viewStyle = 'display:none';
    if(action_sugar_grp1 == 'vi_conditionalnotificationseditview'){viewStyle = '';}
    b.innerHTML = "<select style='"+viewStyle+"' name='aow_conditions_module_path["+ condln +"][0]' id='aow_conditions_module_path" + condln + "' value='' title='' tabindex='116' onchange='showConditionCurrentModuleFields(" + condln + ");' disabled = 'disabled'>" + flow_rel_modules + "</select>";
    if(action_sugar_grp1 == 'vi_conditionalnotificationseditview'){viewStyle = 'display:none';}else{viewStyle = '';}
    b.innerHTML += "<span style='"+viewStyle+"' id='aow_conditions_module_path_label" + condln + "' ></span>";

    var c = x.insertCell(2);
    viewStyle = 'display:none';
    if(action_sugar_grp1 == 'vi_conditionalnotificationseditview'){viewStyle = '';}
    c.innerHTML = "<select style='"+viewStyle+"' name='aow_conditions_field["+ condln +"]' id='aow_conditions_field" + condln + "' value='' title='' tabindex='116' onchange='showModuleField(" + condln + ");'>" + flow_fields + "</select>";
    if(action_sugar_grp1 == 'vi_conditionalnotificationseditview'){viewStyle = 'display:none';}else{viewStyle = '';}
    c.innerHTML += "<span style='"+viewStyle+"' id='aow_conditions_field_label" + condln + "' ></span>";

    var d = x.insertCell(3);
    d.id='aow_conditions_operatorInput'+condln;

    var e = x.insertCell(4);
    e.id='aow_conditions_fieldTypeInput'+condln;

    var f = x.insertCell(5);
    f.id='aow_conditions_fieldInput'+condln;

    var g = x.insertCell(6);
    var op = $("#aow_conditions_operator["+ condln +"]").val();
    if(op != "regular_expression"){
        viewStyle = 'display:none';
    }else{
        viewStyle = '';
    }
    g.innerHTML = "<input type='text' style='"+viewStyle+"' name='aow_conditions_regular_expression["+ condln +"]' id='aow_conditions_regular_expression[" + condln + "]' value='' title='' tabindex='116' >";
    condln++;
    condln_count++;

    $('.edit-view-field #aow_conditionLines').find('tbody').last().find('select').change(function () {
        $(this).find('td').last().removeAttr("style");
        $(this).find('td').height($(this).find('td').last().height() + 8);
    });

    return condln -1;
}

function changeOperator(ln){
    var aow_operator = document.getElementById("aow_conditions_operator["+ln+"]").value;
    if(aow_operator == 'is_null'){
        hideElem('aow_conditions_regular_expression[' + ln + ']');
        showModuleField(ln,aow_operator);
        hideElem('aow_conditions_regular_expression' + ln);
    } else if(aow_operator == 'regular_expression'){
        hideElem('aow_conditions_fieldInput' + ln);
        $("select[name='aow_conditions_value_type["+ln+"]'] option[value='Field']").remove();
        $("select[name='aow_conditions_value_type["+ln+"]'] option[value='Date']").remove();
        showElem('aow_conditions_fieldTypeInput' + ln);
        showModuleFieldType(ln,'');
        showElem('aow_conditions_regular_expression[' + ln + ']');
    }else{
        hideElem('aow_conditions_regular_expression[' + ln + ']');
        showElem('aow_conditions_fieldTypeInput' + ln);
        showModuleField(ln,aow_operator);
        showElem('aow_conditions_fieldInput' + ln);
    }
}

function markConditionLineDeleted(ln) {
    document.getElementById('aow_conditions_body' + ln).style.display = 'none';
    document.getElementById('aow_conditions_deleted' + ln).value = '1';
    document.getElementById('aow_conditions_delete_line' + ln).onclick = '';

    condln_count--;
    if(condln_count == 0){
        document.getElementById('conditionLines_head').style.display = "none";
    }
}

function clearConditionLines(){
    if(document.getElementById('aow_conditionLines') != null){
        var cond_rows = document.getElementById('aow_conditionLines').getElementsByTagName('tr');
        var cond_row_length = cond_rows.length;
        var i;
        for (i=0; i < cond_row_length; i++) {
            if(document.getElementById('aow_conditions_delete_line'+i) != null){
                document.getElementById('aow_conditions_delete_line'+i).click();
            }
        }
    }
}

function hideElem(id){
    if(document.getElementById(id)){
        document.getElementById(id).style.display = "none";
        document.getElementById(id).value = "";
    }
}

function showElem(id){
    if(document.getElementById(id)){
        document.getElementById(id).style.display = "";
    }
}

function date_field_change(field){
    if(document.getElementById(field + '[1]').value == 'now'){
        hideElem(field + '[2]');
        hideElem(field + '[3]');
    } else {
        showElem(field + '[2]');
        showElem(field + '[3]');
    }
}
