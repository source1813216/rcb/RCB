{*
/*********************************************************************************
* This file is part of package Conditional Notifications.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Conditional Notifications is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
*}
<div class="moduleTitle">
    <h2 class="module-title-text">{$MOD.LBL_CONDITIONAL_NOTIFICATIONS}</h2>
    <div class="clear"></div>
</div>
{$HELP_BOX_CONTENT}
<div style="float: right;">
    <a href="index.php?module=VIConditionalNotificationsLicenseAddon&action=license"><button class="button">{$MOD.LBL_UPDATE_LICENSE}</button></a>
</div><br><br>
{if $RECORDCOUNT neq 0}
    <div>
        <table style = "margin-top:20px;width:100%;">
            <tr>
                <td>
                    <input type="button" name="add_new" value="{$MOD.LBL_ADD_NEW}" class="button" onclick="location.href = '{$EDITVIEW_URL}';" ></td>
                <td style="float: right;">
                    <select name="module_filter" id="module_filter" title="Module Filter">
                        <option label="All" value="All" selected="selected">{$MOD.LBL_MODULE_FILTER_ALL}</option>
                        {foreach from=$MODULELIST key=moduleValue item=moduleName}
                            <option label="{$moduleName}" value="{$moduleName}">{$moduleName}</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
        </table>
    </div>
    <div class="list-view-rounded-corners">
        <table class="list view table-responsive">
            <thead>
                <th class="td_alt"><input type="checkbox" id="select_all"></th>
                <th class="td_alt quick_view_links"></th>
                <th class="" scope="col" data-toggle="true">
                    <div>
                        <a class="listViewThLinkS1" href="#">{$MOD.LBL_SELECTED_MODULE}</a>
                    </div>
                </th>
                <th class="" scope="col" data-toggle="true">
                    <div>
                        <a class="listViewThLinkS1" href="#">{$MOD.LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION}</a>
                    </div>
                </th>
            </thead>
            {foreach from=$CONDITIONAL_NOTIFICATIONS_DATA key=k item=data}
                <tr class="oddListRowS1" height="20" data-id="{$data.conditionalNotificationsId}" value="{$data.moduleLabel}">
                    <td>
                        <input class="listview-checkbox" name="mass[]" id="mass[]" value="{$data.conditionalNotificationsId}" type="checkbox">
                    </td>
                    {if $THEME eq 'SuiteP'}
                        <td>
                            <a class="edit-link" title="Edit" id="{$data.conditionalNotificationsId}" href="index.php?module=Administration&action=vi_conditionalnotificationseditview&records={$data.conditionalNotificationsId}">
                            <img src="themes/{$THEME}/images/edit_inline.png"></a>
                        </td>
                    {else}
                        <td>
                            <a class="edit-link" title="Edit" id="{$data.conditionalNotificationsId}" href="index.php?module=Administration&action=vi_conditionalnotificationseditview&records={$data.conditionalNotificationsId}"><img src="themes/{$THEME}/images/edit.gif"></a>
                        </td>
                    {/if}
                    <td scope="row" type="name" field="module_name" class=" inlineEdit" valign="top" align="left">
                        <b>{$data.moduleLabel}</b>
                    </td>
                    <td scope="row" type="name" field="description" class=" inlineEdit" valign="top" align="left">
                        <b>{$data.description}</b>
                    </td>
                </tr>
            {/foreach}
        </table>
    </div>
    <div>
        <table style = "margin-top:20px;">
            <tr>
                <td>
                    <input type="button" name="btn_delete" value="{$MOD.LBL_DELETE}" id="btn_delete" class="button">
                </td>
            </tr>
        </table>
    </div>
{else}
    <br>
    <div class="list view listViewEmpty">
        <br>
            <p class="msg">{$MOD.LBL_CREATE_MESSAGE}
            <a href="index.php?module=Administration&action=vi_conditionalnotificationseditview">{$MOD.LBL_CREATE}</a>{$MOD.LBL_CREATE_MESSAGE_ONE}</p>
    </div>
{/if}

{literal}
<script type="text/javascript">
    //delete 
    $('#btn_delete').on('click', function(e) {
        var id = [];
        $(".listview-checkbox:checked").each(function() {
            id.push($(this).val());
        });
        if(id.length <=0) { 
            alert(SUGAR.language.get('Administration','LBL_PLZ_SELECT_RECORDS')); 
        }else {
            var msg = SUGAR.language.get('Administration','LBL_DELETE_MESSAGE')+" "+(id.length>1?SUGAR.language.get('Administration','LBL_DELETE_THESE'):SUGAR.language.get('Administration','LBL_DELETE_THIS'))+" "+SUGAR.language.get('Administration','LBL_DELETE_ROW');
            var checked = confirm(msg);
            if(checked == true){
                var selected_values = id.join(",");
                $.ajax({
                    type: "POST",
                    url: "index.php?entryPoint=VIDeleteConditionalNotifications",
                    data: 'del_id='+selected_values,
                    success: function(response) {
                        window.location.href = 'index.php?module=Administration&action=vi_conditionalnotificationslistview';
                    }
                });
            }
        }
    });
    $('#module_filter').on('change',function(e){
        var moduleFilterName = $('#module_filter').val();
        var tableRows = $(".list view table-responsive tr");
        if(moduleFilterName != 'All'){
            $(".oddListRowS1").each(function() {
                var trValue = $(this).attr('value');
                if(moduleFilterName != trValue){
                    $(this).hide();
                }else{
                $(this).show();
                }
            });
        }else{
            $(".oddListRowS1").each(function() {
                $(this).show();
            });
        }
    });

    //select all checkbox
    $('#select_all').click(function(event) {   
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;                        
            });
        }else {
            $(':checkbox').each(function() {
                this.checked = false;                       
            });
        }
    });
</script>
{/literal}
