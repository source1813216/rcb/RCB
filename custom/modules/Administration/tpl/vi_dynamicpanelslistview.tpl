{*
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
*}
<div class="moduleTitle">
    <h2 class="module-title-text">{$MOD.LBL_DYNAMIC_PANELS}</h2>
    <div class="clear"></div>
</div>
<div style="float: right;">
    <a href="index.php?module=VIDynamicPanelsLicenseAddon&action=license"><button class="button">{$MOD.LBL_UPDATE_LICENSE}</button></a>
</div><br><br>
{if $DYNAMIC_PANELS_DATA|@count gt 0}
    <div>
        <table style = "margin-top:20px;">
            <tr>
                <td>
                    <input type="button" name="add_new" value="{$MOD.LBL_ADD_NEW}" class="button" onclick="location.href = '{$EDITVIEWURL}';">
                </td>
            </tr>
        </table>
    </div>
    <div class="list-view-rounded-corners">
        <table class="list view table-responsive">
            <thead>
                <th class="td_alt"><input type="checkbox" id="select_all"></th>
                <th class="td_alt quick_view_links"></th>
                <th class="" scope="col" data-toggle="true">
                    <div>
                        <a class="listViewThLinkS1" href="#">{$MOD.LBL_DYNAMIC_PANELS_NAME}</a>
                    </div>
                </th>
                <th class="" scope="col" data-toggle="true">
                    <div>
                        <a class="listViewThLinkS1" href="#">{$MOD.LBL_PRIMARY_MODULE}</a>
                    </div>
                </th>
                <th class="" scope="col" data-toggle="true">
                    <div>
                        <a class="listViewThLinkS1" href="#">{$MOD.LBL_DEFAULT_HIDDEN_PANELS}</a>
                    </div>
                </th>
                <th class="" scope="col" data-toggle="true">
                    <div>
                        <a class="listViewThLinkS1" href="#">{$MOD.LBL_DEFAULT_HIDDEN_FIELDS}</a>
                    </div>
                </th>
                <th class="" scope="col" data-toggle="true">
                    <div>
                        <a class="listViewThLinkS1" href="#">{$MOD.LBL_USER_TYPE}</a>
                    </div>
                </th>
                <th class="" scope="col" data-toggle="true">
                    <div>
                        <a class="listViewThLinkS1" href="#">{$MOD.LBL_USER_ROLES}</a>
                    </div>
                </th>
                <th class="" scope="col" data-toggle="true">
                    <div>
                        <a class="listViewThLinkS1" href="#">{$MOD.LBL_DATE_ENTERED}</a>
                    </div>
                </th>
            </thead>
            {foreach from=$DYNAMIC_PANELS_DATA key=k item=data}
                <tr class="oddListRowS1" height="20" data-id="{$data.dynamicPanelsId}">
                    <td>
                        <input class="listview-checkbox" name="mass[]" id="mass[]" value="{$data.dynamicPanelsId}" type="checkbox">
                    </td>
                    {if $THEME eq 'SuiteP'}
                        <td>
                            <a class="edit-link" title="Edit" id="{$data.dynamicPanelsId}" href="index.php?module=Administration&action=vi_dynamicpanelseditview&records={$data.dynamicPanelsId}">
                            <img src="themes/{$THEME}/images/edit_inline.png"></a>
                        </td>
                    {else}
                        <td>
                            <a class="edit-link" title="Edit" id="{$data.dynamicPanelsId}" href="index.php?module=Administration&action=vi_dynamicpanelseditview&records={$data.dynamicPanelsId}"><img src="themes/{$THEME}/images/edit.gif"></a>
                        </td>
                    {/if}
                    <td scope="row" type="name" field="name" class="inlineEdit" valign="top" align="left">
                        <b>{$data.name}</b>
                    </td>
                    <td scope="row" type="name" field="flow_module" class="inlineEdit" valign="top" align="left">
                        <b>{$data.primaryModule}</b>
                    </td>
                    <td scope="row" type="name" field="defaultpanel" class=" inlineEdit" valign="top" align="left">
                        {foreach from=$data.defaultPanelHide item=val}
                            <b>{$val}</b><br>
                        {/foreach}
                    </td>
                    <td scope="row" type="name" field="defaultfield" class=" inlineEdit" valign="top" align="left">
                        {foreach from=$data.defaultField item=val}
                            <b>{$val}</b><br>
                        {/foreach}
                    </td>
                    <td scope="row" type="name" field="user_type" class=" inlineEdit" valign="top" align="left">
                        <b>{$data.userType}</b>
                    </td>
                    <td scope="row" type="name" field="user_roles" class=" inlineEdit" valign="top" align="left">
                        {foreach from=$data.userRole item=val}
                            <b>{$val}</b><br>
                        {/foreach}
                    </td>
                    <td scope="row" type="name" field="date_created" class=" inlineEdit" valign="top" align="left">
                        <b>{$data.dateEntered}</b>
                    </td>
                </tr>
            {/foreach}
        </table>
    </div>
    <div>
        <table style = "margin-top:20px;">
            <tr>
                <td><input type="button" name="btn_delete" value="{$MOD.LBL_DELETE}" id="btn_delete" class="button"></td>
            </tr>
        </table>
    </div>
{else}
    <br>
    <div class="list view listViewEmpty">
        <br><p class="msg">{$MOD.LBL_CREATE_MESSAGE}
                <a href="index.php?module=Administration&action=vi_dynamicpanelseditview">{$MOD.LBL_CREATE}</a>{$MOD.LBL_CREATE_MESSAGE_ONE}
            </p>
    </div>
{/if}

<script type="text/javascript">
{literal}
  //delete 
    $('#btn_delete').on('click', function(e) {
        var id = [];
        $(".listview-checkbox:checked").each(function() {
            id.push($(this).val());
        });//end of function
        if(id.length <=0) { 
            label = SUGAR.language.get('Administration','LBL_PLEASE_SELECT_RECORDS');
            alert(label); 
        } else {
            var label1 = SUGAR.language.get('Administration','LBL_DELETE_MESSAGE_1');
            var label2 = SUGAR.language.get('Administration','LBL_DELETE_MESSAGE_2');
            var label3 = SUGAR.language.get('Administration','LBL_DELETE_MESSAGE_3');
            var label4= SUGAR.language.get('Administration','LBL_DELETE_MESSAGE_4');

            var msg = label1+" "+(id.length>1?label2:label3)+" "+label4;
            var checked = confirm(msg);
            if(checked == true){
                var selected_values = id.join(",");
                $.ajax({
                    type: "POST",
                    url: "index.php?entryPoint=VIDeleteDynamicPanels",
                    data: 'del_id='+selected_values,
                    success: function(response) {
                        window.location.href = 'index.php?module=Administration&action=vi_dynamicpanelslistview';
                    }//end of success
                });//end of ajax
            }//end of if
        }//end of else
    });//end of function

    //select all checkbox
    $('#select_all').click(function(event) {   
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;                        
            });//end of each
        } else {
            $(':checkbox').each(function() {
                this.checked = false;                       
            });//end of each
        }//end of else
    });//end of function

    $('.listview-checkbox').on('click',function(){
        if($(".listview-checkbox").length == $(".listview-checkbox:checked").length) {
            $("#select_all").prop("checked", true);
        }else{
            $("#select_all").prop("checked", false);
        }//end of else
    });//end of function
{/literal}
</script>