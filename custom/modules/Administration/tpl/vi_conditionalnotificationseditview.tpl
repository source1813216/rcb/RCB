{*
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
 *}
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="custom/modules/Administration/css/VIConditionalNotificationsCss.css">
	</head>
	<body>
		<div class="moduleTitle">
			<h2>{$MOD.LBL_CONDITIONAL_NOTIFICATIONS}</h2>
		</div>
		<div class="clear"></div>
		<form name="EditView">
			<div class="progression-container">
	    		<ul class="progression">
	        		<li id="nav_step1" class="nav-steps selected" data-nav-step="1" data-nav-url=""><div>{$MOD.LBL_SELECT_MODULE}</div></li>
	        		<li id="nav_step2" class="nav-steps" data-nav-step="2" data-nav-url=""><div>{$MOD.LBL_ADD_CONDITIONS}</div></li>
	        		<li id="nav_step3" class="nav-steps" data-nav-step="3" data-nav-url=""><div>{$MOD.LBL_ADD_TASKS}</div></li>
	 			</ul>
			</div>
			<p>
				<div id ='buttons'>
	            	<table width="100%" border="0" cellspacing="0" cellpadding="0" >
	                	<tr> 
	                    	<td align="left" width='30%'>
	                        	<table border="0" cellspacing="0" cellpadding="0" >
	                        		<tr>
	                            		<td>
	                            			<div id="back_button_div">
	                            				<input id="back_button" type='button' title="{$MOD.LBL_BACK}" class="button" name="back" value="{$MOD.LBL_BACK}">
	                            			</div>
	                            		</td>
	                            		<td>
	                            			<div id="cancel_button_div">
	                            				<button type="button" class="button" name= "btn_cancel" id= "btn_cancel" onclick = "cancel();">{$MOD.LBL_CANCEL}</button>
	                            			</div>
	                            		</td>
	                            		<td>
	                            			<div id="clear_button_div">
	                            				<button type="button" class="button" name= "btn_clear" id= "btn_clear" onclick = "clearall();" style="margin-left: 20px;">{$MOD.LBL_CLEAR}</button>
	                            			</div>
	                            		</td>
	                            		<td>
	                            			<div id="next_button_div">
	                            				<button type="button" class="button" name= "btn_next" id= "btn_next" style="margin-left: 20px;">{$MOD.LBL_NEXT}</button>
	                            			</div>
	                            		</td>
	                            		<td>
	                            			<div id="save_button_div">
	                            				<button type="button" class="button" name= "btn_save" id= "btn_save" onclick = "actionsave()" style="margin-left: 20px;">{$MOD.LBL_SAVE}</button>
	                            			</div>
	                            		</td>
	                            	</tr>
	                        	</table>
	                    	</td>
	                	</tr>
	            	</table>
	    		</div>
			</p>
			<table cellspacing="1" style="">
	    		<tr>
	        		<td class='edit view' rowspan='2' width='100%'>
	            		<div id="wiz_message"></div>
	            		<div id=wizard class="wizard-unique-elem" style="width:1000px;">
	                		<div id="step1">
	                    		<div class="template-panel">
	                        		<div class="template-panel-container panel">
	                            		<div class="template-container-full">
	                                		<table width="100%" border="0" cellspacing="10" cellpadding="0">
	                                    		<tbody>
	                                    			<tr>
	                                    				<th colspan="4"><h4 class="header-4">{$MOD.LBL_SELECT_MODULE}</h4></th>
	                                    			</tr>
	                                    			<tr rowspan = "4">
	                                            		<td>
	                                            			<b>{$MOD.LBL_SELECT_MODULE}<span class="required">*</span></b>
	                                            		</td>
	                                            		<td class="setvisibilityclass">
	                                            			<select name="flow_module" id="flow_module">
	                                            				<option value="" selected="selected">{$MOD.LBL_SELECT_AN_OPTION}</option>
	                                            				{foreach from=$MODULELIST key=moduleValue item=moduleName}
	                                            					{if $CONDITION_NOTIFICATIONS_DATA.MODULENAME eq $moduleValue}
	                                            						<option label="{$moduleName}" value="{$moduleValue}" selected="selected">{$moduleName}</option>
	                                            					{else}
                                                 						<option label="{$moduleName}" value="{$moduleValue}">{$moduleName}</option>
                                                					{/if}
	                                            				{/foreach}
	                                            			</select>
	                                            		</td>
	                                        		</tr>
	                                        		<tr>
	                                            		<td>
	                                            			<b>{$MOD.LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION}<span class="required">*</span></b>
	                                            		</td>
	                                            		<td class="setvisibilityclass">
	                                            			<input type="text" name="description" id="description" size="30" maxlength="150" value="{$CONDITION_NOTIFICATIONS_DATA.DESCRIPTION}">
	                                            		</td>
	                                        		</tr>
	                                    		</tbody>
	                               			</table>
	                               			<b>{$MOD.LBL_NOTE_LABEL}</b> {$MOD.LBL_SELECT_MODULE_NOTES}
	                            		</div>
	                        		</div>
	                    		</div>
	                		</div>
	                		<div id="step2" style="display:none;">
	                    		<div class="template-panel">
	                        		<div class="template-panel-container panel">
	                            		<div class="template-container-full">
	                                		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                    		<tbody>
	                                        		<tr>
	                                        			<th colspan="4"><h4 class="header-4">{$MOD.LBL_ADD_CONDITIONS}</h4></th>
	                                        		</tr>
	                                        		<tr>
	             										<td>
	             											<b>{$MOD.LBL_NOTE_LABEL}</b>{$MOD.LBL_NOTE}<br/>
			             									<span style="margin-left: 45px;">{$MOD.LBL_ONLY_ALPHA_VALUES}</span>
			             									<br/>
			             									<span style="margin-left: 45px;">{$MOD.LBL_ONLY_NUMERIC_VALUES}</span>
			             									<br/>
	             											<span style="margin-left: 45px;">{$MOD.LBL_ALPHANUMERIC_VALUES}</span>
	             											<br/>
	             											<span style="margin-left: 45px;">{$MOD.LBL_DATE_VALUES}</span>
	             											<br/>
	             											<span style="margin-left: 45px;">{$MOD.LBL_DATE_TIME_VALUES}</span>
	             											<br/>
	             										</td>
	             									</tr>
	             									<tr>
		             									<td>
		             										<span></span><br>
		             									</td>
	             									</tr>
	                                        		<tr>
                                              			<td>
                                                  			<label>{$MOD.LBL_CONDITIONAL_OPERATOR}</label>
                                                   			<select name="conditional_operator" id="conditional_operator" style="margin-left: 30px;">
			                                                    <option label="AND" value="AND"{if $CONDITION_NOTIFICATIONS_DATA.CONDITIONALOPERATOR eq 'AND'} selected{/if}>{$MOD.LBL_AND_OPERATOR}</option>
			                                                    <option label="OR" value="OR" {if $CONDITION_NOTIFICATIONS_DATA.CONDITIONALOPERATOR eq 'OR'} selected{/if}>{$MOD.LBL_OR_OPERATOR}</option>
                                                			</select>
                                              			</td>
                                          			</tr>
	                                    		</tbody>
	                               			</table>
	                                		<span id="condition_lines_span">
	                                 			{if $HTML eq ''}
	                                    			<script type="text/javascript" src="custom/modules/Administration/js/VIConditionalNotificationsConditionLines.js"></script>
	                                    			<table id="aow_conditionLines" width="100%" cellspacing="4" border="0"></table>
	                                    			<div style="padding-top: 10px; padding-bottom:10px;">
	                                    				<input tabindex="116" class="button" value="{$MOD.LBL_ADD_CONDITIONS}" id="btn_ConditionLine" onclick="insertConditionLine()" type="button">
	                                    			</div>
	                                    		{else} {$HTML} {/if}
	                                		</span> 
	                            		</div>
	                       	 		</div>
	                    		</div>
	                     		<b>{$MOD.LBL_NOTE_LABEL}</b> {$MOD.LBL_ADD_CONDITIONS_NOTES}
	                		</div>
	                		<div id="step3" style="display:none;">
	                			<div class="template-panel">
	                        		<div class="template-panel-container panel">
	                            		<div class="template-container-full">
	                                		<table width="100%" border="0" cellspacing="10" cellpadding="0">
	                                    		<tbody>
	                                    			<tr>
	                                    				<th colspan="4"><h4 class="header-4">{$MOD.LBL_ADD_TASKS_NOTES}</h4></th>
	                                    			</tr>
	                                    			<tr>
	                                            		<td>
	                                            			<b>{$MOD.LBL_ACTION_TITLE}<span class="required">*</span></b>
	                                            		</td>
	                                            		<td class="setvisibilityclass">
	                                            			<input type="text" name="action_title" id="action_title" value="{$CONDITION_NOTIFICATIONS_DATA.ACTIONTITLE}">
	                                            		</td>
	                                        		</tr>
	                                        		<tr>
	                                            		<td><b>{$MOD.LBL_NOTIFICATION_EDITING}</b></td>
	                                            		<td class="setvisibilityclass">
	                                            			<label class="switch">
	                                            				<input type="checkbox" name="notification_on_edit" id="notification_on_edit" value="" {if $CONDITION_NOTIFICATIONS_DATA.NOTIFICATIONONEDIT eq 1} checked {/if}>
					                            				<span class="slider round"></span>
					                            			</label>
					                            			<input type="hidden" value="0" id="notification_on_edits" name="notification_on_edits">
					                            		</td>
	                                        		</tr>
	                                        		<tr>
	                                            		<td><b>{$MOD.LBL_NOTIFICATION_ON_VIEW}</b></td>
	                                            		<td class="setvisibilityclass">
	                                            			<label class="switch">
	                                            				<input type="checkbox" name="notification_on_view" id="notification_on_view" value="" {if $CONDITION_NOTIFICATIONS_DATA.NOTIFICATIONONVIEW eq 1} checked {/if}>
	                                            				<span class="slider round"></span>
	                                            			</label>
	                                            			<input type="hidden" value="0" id="notification_on_views" name="notification_on_views">
	                                            		</td>
	                                        		</tr>
	                                        		<tr>
	                                            		<td><b>{$MOD.LBL_NOTIFICATION_ON_SAVE}</b></td>
	                                            		<td class="setvisibilityclass">
	                                            			<label class="switch">
					                                            <input type="checkbox" name="notification_on_save" id="notification_on_save" value="" {if $CONDITION_NOTIFICATIONS_DATA.NOTIFICATIONONSAVE eq 1} checked {/if}>
					                                            <span class="slider round"></span>
					                                        </label>
					                                        <input type="hidden" value="0" id="notification_on_saves" name="notification_on_saves">
					                                    </td>
	                                        		</tr>
	                                        		<tr>
	                                            		<td><b>{$MOD.LBL_DO_NOT_ALLOW_SAVE}</b></td>
	                                            		<td class="setvisibilityclass">
	                                            			<label class="switch">
	                                            				<input type="checkbox" name="notification_not_allow_save" id="notification_not_allow_save" value="" {if $CONDITION_NOTIFICATIONS_DATA.NOTIFICATIONNOTALLOWSAVE eq 1} checked {/if}>
	                                            				<span class="slider round"></span>
	                                            			</label>
	                                            			<input type="hidden" value="0" id="notification_not_allow_saves" name="notification_not_allow_saves">
	                                            		</td>
	                                        		</tr>
	                                        		<tr>
	                                            		<td><b>{$MOD.LBL_CONFIRMATION_DIALOG_LABEL}</b></td>
	                                            		<td class="setvisibilityclass">
	                                            			<label class="switch">
	                                            				<input type="checkbox" name="notification_confirmation_dialog" id="notification_confirmation_dialog" value="" {if $CONDITION_NOTIFICATIONS_DATA.NOTIFICATIONCONFIRMATIONDIALOG eq 1} checked {/if}>
	                                            				<span class="slider round"></span>
	                                            			</label>
	                                            			<input type="hidden" value="0" id="notification_confirmation_dialogs" name="notification_confirmation_dialogs">
	                                            		</td>
	                                        		</tr>
	                                        		<tr>
	                                            		<td><b>{$MOD.LBL_NOTIFICATION_ON_DUPLICATE}</b></td>
	                                            		<td class="setvisibilityclass">
	                                            			<label class="switch">
					                                            <input type="checkbox" name="notification_on_duplicate" id="notification_on_duplicate" value="" {if $CONDITION_NOTIFICATIONS_DATA.NOTIFICATIONONDUPLICATE eq 1} checked {/if}>
					                            				<span class="slider round"></span>
					                            			</label>
					                            			<input type="hidden" value="0" id="notification_on_duplicates" name="notification_on_duplicates">
					                            		</td>
	                                        		</tr>
	                                        		<tr>
	                                        			<td colspan="2">
	                                        				<textarea id="body_html" name="body_html" rows="4" cols="50"></textarea>
	                                        				{literal}
				                                        		<script>
				                                        			var body_html = "{/literal}{$CONDITION_NOTIFICATIONS_DATA.BODYHTML}{literal}";
				                                        			$('#body_html').html(body_html);
				                                        		</script>
	                                        				{/literal}
	                                        			</td>
	                                        		</tr>
	                                    		</tbody>
	                                		</table>
	                            		</div>
	                        		</div>
	                			</div>
	                		</div>
	            		</div>
	        		</td>
	    		</tr>
			</table>
		</form>
</html>
{literal}
<script type="text/javascript" src="custom/modules/Administration/js/VIConditionalNotificationsEditView.js"></script>
<script type="text/javascript" src="include/javascript/tiny_mce/tiny_mce.js"></script> 
<script>
   	function actionsave(){
    	var actionTitle = $('#action_title').val();
        if(actionTitle != ""){
        	var id = "{/literal}{$RECORDID}{literal}";
	    	var formData = $('form');
	    	var body_html = tinymce.get('body_html').getContent();
	    	var disabled = formData.find(':disabled').removeAttr('disabled');
	        var formData = formData.serialize();
	        $.ajax({
	            url: "index.php?entryPoint=VIAddConditionalNotifications",
	            type: "post",
	            data: {val : formData,
	            	   body_html : body_html,
	            	   id : id},
	            success: function (response) {
	                window.location.href = "index.php?module=Administration&action=vi_conditionalnotificationslistview";
	            }
	        });
        }else{
        	alert(SUGAR.language.get('Administration','LBL_VALIDATION_MESSAGE'));
        }      
    }
</script>
{/literal}
