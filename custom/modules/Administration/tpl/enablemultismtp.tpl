{*
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
*}
<!-- <link rel="stylesheet" href="themes/SuiteP/css/Dawn/style.css"> -->
<div class="moduleTitle">
<h2 class="module-title-text">{$MOD.LBL_LINK_NAME}</h2>
<div class="clear"></div>
</div>
<br>
<div style="float: right;">
    <a href="index.php?module=VIMultiSMTPLicenseAddon&action=license"><button class="button">{$MOD.LBL_UPDATE_LICENSE}</button></a>
</div>
<div>
<table style = "margin-top:20px;">
<tr>
<td><b>{$MOD.LBL_ENABLED_MULTI_SMTP}</b></td> 
<td><input type="checkbox" name="chk" id="chk" style = "margin-left:20px;" {if $enable eq 1}checked='checked'{/if}></td>
</tr>
</table>
</div>
<script type="text/javascript">
{literal}
$(document).on('change', '#chk', function() {
	var check_value;
	if($(this).is(':checked')){
		  check_value = 1;
	}else{
	  		 check_value = 0;
	}
	var mb = messageBox();
	mb.hideHeader();
	$.ajax({
      	url : 'index.php?entryPoint=VIMultiplesmtp_Status',
      	type: 'POST',
      	data : {val : check_value},
      	success : function(data) {
		    console.log('Success');
		    if(check_value == '1') {	
			       	mb.setBody("Multiple SMTP Enabled");
			       	mb.show();
					mb.on('ok', function () {
			        	"use strict";
			        	 mb.remove();
	      	   		});
	      	  		mb.on('cancel', function () {
			        	"use strict";
			        	mb.remove();
	      	  		});
		    } else 	{
			      	mb.setBody("Multiple SMTP Disabled");
			      	mb.show();
					mb.on('ok', function () {
				    	"use strict";
				    	mb.remove();
	      	  		});
	      	  		mb.on('cancel', function () {
	        			"use strict";
	        			mb.remove();
	      	  		});
		      }
     	}
  	});
});
{/literal}
</script>