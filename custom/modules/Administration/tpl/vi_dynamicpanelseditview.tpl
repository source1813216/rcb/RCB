{*
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
*}
<html>
<head>
    <link rel="stylesheet" type="text/css" href="custom/modules/Administration/css/VIDynamicPanelsCss.css">
</head>
<body>
    <div class="moduleTitle">
        <h2>{$MOD.LBL_DYNAMIC_PANELS}</h2>
    </div>
    <div class="clear"></div>
    <form name="EditView">
    <div class="progression-container">
        <ul class="progression">
            <li id="nav_step1" class="nav-steps selected" data-nav-step="1" data-nav-url=""><div>{$MOD.LBL_SELECT_MODULE}</div></li>
            <li id="nav_step2" class="nav-steps" data-nav-step="2" data-nav-url=""><div>{$MOD.LBL_APPLY_CONDITION}</div></li>
            <li id="nav_step3" class="nav-steps" data-nav-step="3" data-nav-url=""><div>{$MOD.LBL_SET_VISIBILITY}</div></li><li id="nav_step4" class="nav-steps" data-nav-step="4" data-nav-url=""><div>{$MOD.LBL_ASSIGN_VALUE}</div></li>
        </ul>
    </div>
    <p>
        <div id ='buttons'>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                <tr> 
                    <td align="left" width='30%'>
                        <table border="0" cellspacing="0" cellpadding="0" >
                            <tr>
                                <td>
                                    <div id="back_button_div">
                                        <input id="back_button" type='button' title="{$MOD.LBL_BACK}" class="button" name="back" value="{$MOD.LBL_BACK}">
                                    </div>
                                </td>
                                <td>
                                    <div id="cancel_button_div">
                                        <button type="button" class="button" name= "btn_cancel" id= "btn_cancel" onclick = "cancel();">{$MOD.LBL_CANCEL}</button>
                                    </div>
                                </td>
                                <td>
                                    <div id="clear_button_div">
                                        <button type="button" class="button" name= "btn_clear" id= "btn_clear" onclick = "clearall();" style="margin-left: 20px;">{$MOD.LBL_CLEAR}</button>
                                    </div>
                                </td>
                                <td>
                                    <div id="next_button_div">
                                        <button type="button" class="button" name= "btn_next" id= "btn_next" style="margin-left: 20px;">{$MOD.LBL_NEXT}</button>
                                    </div>
                                </td>
                                <td>
                                    <div id="save_button_div">
                                        <button type="button" class="button" name= "btn_save" id= "btn_save" onclick = "save()" style="margin-left: 20px;">{$MOD.LBL_SAVE}</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </p>
    <table cellspacing="1" style="">
        <tr>
            <td class='edit view' rowspan='2' width='100%'>
                <div id="wiz_message"></div>
                <div id=wizard class="wizard-unique-elem" style="width:1000px;">
                    <div id="step1">
                        <div class="template-panel">
                            <div class="template-panel-container panel">
                                <div class="template-container-full">
                                    <table width="100%" border="0" cellspacing="10" cellpadding="0">
                                        <tbody>
                                            <tr><th colspan="4"><h4 class="header-4">{$MOD.LBL_SELECT_MODULE}</h4></th></tr> 
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_DYNAMIC_PANELS_NAME}<span class="required">*</span></b>
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <input type="text" name="dynamic_panels_name" id="dynamic_panels_name" value="{$DYNAMIC_PANELS_DATA.NAME}">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_USER_TYPE}<span class="required">*</span></b>
                                                    <img border="0" onclick="return SUGAR.util.showHelpTips(this,'{$MOD.LBL_USER_TYPE_TOOLTIP}' );" src="themes/default/images/helpInline.gif?v=wmRJeTRpwzmvnjrzcfY9qw" alt="Information" class="inlineHelpTip">
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select id="user_type" name="user_type">
                                                        <option value="">{$MOD.LBL_SELECT_AN_OPTION}</option>
                                                        <option value="AllUsers"{if $DYNAMIC_PANELS_DATA.USERTYPE eq 'AllUsers'} selected{/if}>{$MOD.LBL_USER_TYPE_ALL_USERS}</option>
                                                        <option value="RegularUser"{if $DYNAMIC_PANELS_DATA.USERTYPE eq 'RegularUser'} selected{/if}>{$MOD.LBL_USER_TYPE_REGULAR_USER}</option>
                                                        <option value="Administrator"{if $DYNAMIC_PANELS_DATA.USERTYPE eq 'Administrator'} selected{/if}>{$MOD.LBL_USER_TYPE_ADMIN_USER}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_USER_ROLES}<span class="required">*</span></b>
                                                    <img border="0" onclick="return SUGAR.util.showHelpTips(this,'{$MOD.LBL_USER_ROLE_TOOLTIP}' );" src="themes/default/images/helpInline.gif?v=wmRJeTRpwzmvnjrzcfY9qw" alt="Information" class="inlineHelpTip">
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select id="user_roles" name="user_roles[]" multiple="true"></select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="setvisibilityclass">
                                                    <span style="color:red">{$MOD.LBL_DYNAMIC_PANELS_NOTE}</span><br>
                                                    <a href="https://prnt.sc/mmi2yx" target="_blank">Click here</a> 
                                                    <span style="color:red">{$MOD.LBL_DYNAMIC_PANELS_NOTE1}</span>
                                                </td>
                                            </tr>
                                            <tr rowspan = "4">
                                                <td>
                                                    <b>{$MOD.LBL_SELECT_PRIMARY_MODULE}<span class="required">*</span></b>
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select name="flow_module" id="flow_module" title="">
                                                        <option value="">{$MOD.LBL_SELECT_AN_OPTION}</option>
                                                        {foreach from=$MODULELIST key=moduleValue item=moduleName}
                                                            {if $DYNAMIC_PANELS_DATA.PRIMARY_MODULE eq $moduleValue}
                                                                <option value="{$moduleValue}" selected="selected">{$moduleName}</option>
                                                            {else}
                                                                <option value="{$moduleValue}">{$moduleName}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_SELECT_DEFAULT_PANELS_TO_HIDE}</b>
                                                    <img border="0" onclick="return SUGAR.util.showHelpTips(this,'{$MOD.LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP}');" src="themes/default/images/helpInline.gif?v=wmRJeTRpwzmvnjrzcfY9qw" alt="Information" class="inlineHelpTip">
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select id="defaultpanel" name="defaultpanel[]" multiple="true">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_SELECT_DEFAULT_FIELDS_TO_HIDE}</b>
                                                    <img border="0" onclick="return SUGAR.util.showHelpTips(this,'{$MOD.LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP}' );" src="themes/default/images/helpInline.gif?v=wmRJeTRpwzmvnjrzcfY9qw" alt="Information" class="inlineHelpTip">
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select id="defaultfield" name="defaultfield[]" multiple="true">
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <p></p>
                    <p></p>
                    <div id="step2" style="display:none;">
                        <div class="template-panel">
                            <div class="template-panel-container panel">
                                <div class="template-container-full">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr><th colspan="4"><h4 class="header-4">{$MOD.LBL_CONDITIONS}</h4></th></tr>
                                            <tr>
                                                <td>
                                                    <label>{$MOD.LBL_CONDITIONAL_OPERATOR}</label>
                                                    <select name="conditional_operator" id="conditional_operator">
                                                        <option value="AND"{if $DYNAMIC_PANELS_DATA.CONDITIONAL_OPERATOR eq 'AND'} selected{/if}>{$MOD.LBL_AND}</option>
                                                        <option value="OR" {if $DYNAMIC_PANELS_DATA.CONDITIONAL_OPERATOR eq 'OR'} selected{/if}>{$MOD.LBL_OR}</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    {$MOD.LBL_CONDITIONS_MESSAGE}
                                    <span id="condition_lines_span">
                                        {if $HTML eq ''}
                                            <script type="text/javascript" src="custom/modules/Administration/js/VIDynamicPanelsConditionLines.js"></script>
                                            <script type="text/javascript" src="custom/modules/Administration/js/VIDynamicPanelsJscolor.js"></script>
                                            <table id="aow_conditionLines" width="100%" cellspacing="4" border="0"></table><div style="padding-top: 10px; padding-bottom:10px;"><input tabindex="116" class="button" value="{$MOD.LBL_ADD_CONDITIONS}" id="btn_ConditionLine" onclick="insertConditionLine()" type="button"></div>
                                        {else} {$HTML} {/if}
                                    </span>  
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div id="step3" style="display:none;">
                        <div class="template-panel">
                            <div class="template-panel-container panel">
                                <div class="template-container-full">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="setvisibility">
                                        <tbody>
                                            <tr>
                                                <th colspan="4"><h4 class="header-4">{$MOD.LBL_SET_VISIBILITY}</h4></th>
                                            </tr> 
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_PANELS_TO_HIDE}</b>
                                                    <img border="0" onclick="return SUGAR.util.showHelpTips(this,'{$MOD.LBL_PANELS_HIDE_TOOLTIP}');" src="themes/default/images/helpInline.gif?v=wmRJeTRpwzmvnjrzcfY9qw" alt="Information" class="inlineHelpTip">
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select id="panel_hide" name="panel_hide[]" multiple="true"></select>
                                                </td>        
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_FIELDS_TO_HIDE}</b>
                                                    <img border="0" onclick="return SUGAR.util.showHelpTips(this,'{$MOD.LBL_FIELDS_HIDE_TOOLTIP}');" src="themes/default/images/helpInline.gif?v=wmRJeTRpwzmvnjrzcfY9qw" alt="Information" class="inlineHelpTip">
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select id="field_hide" name="field_hide[]" multiple="true" ></select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_PANELS_TO_SHOW}</b>
                                                    <img border="0" onclick="return SUGAR.util.showHelpTips(this,'{$MOD.LBL_PANELS_SHOW_TOOLTIP}');" src="themes/default/images/helpInline.gif?v=wmRJeTRpwzmvnjrzcfY9qw" alt="Information" class="inlineHelpTip">
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select id="panel_show" name="panel_show[]" multiple="true"></select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_FIELDS_TO_SHOW}</b>
                                                    <img border="0" onclick="return SUGAR.util.showHelpTips(this,'{$MOD.LBL_FIELDS_SHOW_TOOLTIP}');" src="themes/default/images/helpInline.gif?v=wmRJeTRpwzmvnjrzcfY9qw" alt="Information" class="inlineHelpTip">
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select id="field_show" name="field_show[]" multiple="true"></select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_FIELDS_TO_READONLY}</b>
                                                    <img border="0" onclick="return SUGAR.util.showHelpTips(this,'{$MOD.LBL_FIELDS_REDONLY_TOOLTIP}');" src="themes/default/images/helpInline.gif?v=wmRJeTRpwzmvnjrzcfY9qw" alt="Information" class="inlineHelpTip">
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select id="field_readonly" name="field_readonly[]" multiple="true"></select>
                                                </td>
                                            </tr>   
                                            <tr>
                                                <td>
                                                    <b>{$MOD.LBL_FIELDS_TO_MANDATORY}</b>
                                                    <img border="0" onclick="return SUGAR.util.showHelpTips(this,'{$MOD.LBL_FIELDS_MANDATORY_TOOLTIP}');" src="themes/default/images/helpInline.gif?v=wmRJeTRpwzmvnjrzcfY9qw" alt="Information" class="inlineHelpTip">
                                                </td>
                                                <td class="setvisibilityclass">
                                                    <select id="field_mandatory" name="field_mandatory[]" multiple="true"></select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <div id="step4" style="display:none;">
                        <div class="template-panel">
                            <div class="template-panel-container panel">
                                <div class="template-container-full">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr><th colspan="4"><h4 class="header-4">{$MOD.LBL_ASSIGN_VALUE}</h4></th></tr>
                                        </tbody>
                                    </table>
                                    {$MOD.LBL_ASSIGN_VALUE_MESSAGE}
                                    {if $FIELD_LINE eq ''}
                                        <table id="fieldLines" width="100%" cellspacing="4" border="0"></table>
                                        <div style="padding-top: 10px; padding-bottom:10px;">
                                            <input tabindex="116" class="button" value="{$MOD.LBL_ADD_FIELD}" id="btn_fieldLine" onclick="insertFieldLine()" type="button">
                                        </div>
                                    {else}{$FIELD_LINE}{/if}
                                    <br>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <th colspan="4"><h4 class="header-4">{$MOD.LBL_ASSIGN_BACKGROUND_COLOR}</h4></th>
                                            </tr> 
                                        </tbody>
                                    </table>
                                    {$MOD.LBL_ASSIGN_BACKGROUND_MESSAGE}
                                    {if $FIELD_COLOR_LINE eq  ''}
                                        <table id="fieldColor" width="100%" cellspacing="4" border="0"></table>
                                        <div style="padding-top: 10px; padding-bottom:10px;">
                                            <input tabindex="116" class="button" value="{$MOD.LBL_ADD_FIELD}" id="btn_fieldColor"  name="btn_fieldColor" onclick="insertFieldColor()" type="button">
                                        </div>
                                    {else}{$FIELD_COLOR_LINE}{/if}
                                </div>
                            </div>
                        </div>    
                    </div>  
                </div>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
{literal}
<script type="text/javascript" src="custom/modules/Administration/js/VIDynamicPanelsEditView.js"></script> 
<script type="text/javascript" src="themes/SuiteP/js/jscolor.js"></script>  
<script type= "text/javascript">
    $(document).ready(function (){
        var defaultPanel = {/literal}{$DYNAMIC_PANELS_DATA.DEFAULTPANEL|@json_encode}{literal};
        var panelHide = {/literal}{$DYNAMIC_PANELS_DATA.PANELHIDE|@json_encode}{literal};
        var panelShow = {/literal}{$DYNAMIC_PANELS_DATA.PANELSHOW|@json_encode}{literal};
        var defaultField = {/literal}{$DYNAMIC_PANELS_DATA.DEFAULTFIELD|@json_encode}{literal};
        var fieldHide = {/literal}{$DYNAMIC_PANELS_DATA.FIELDHIDE|@json_encode}{literal};
        var fieldShow = {/literal}{$DYNAMIC_PANELS_DATA.FIELDSHOW|@json_encode}{literal};
        var fieldReadOnly = {/literal}{$DYNAMIC_PANELS_DATA.FIELDREADONLY|@json_encode}{literal};
        var fieldMandatory = {/literal}{$DYNAMIC_PANELS_DATA.FIELDMANDATORY|@json_encode}{literal};
        var userRoles = {/literal}{$DYNAMIC_PANELS_DATA.USERROLES|@json_encode}{literal};

        var defaultPanelObject = converStringToObject(defaultPanel);
        var panelHideObject = converStringToObject(panelHide);
        var panelShowObject = converStringToObject(panelShow);
        var defaultFieldObject = converStringToObject(defaultField);
        var fieldHideObject = converStringToObject(fieldHide); 
        var fieldShowObject = converStringToObject(fieldShow);
        var fieldReadOnlyObject = converStringToObject(fieldReadOnly);
        var fieldMandatoryObject = converStringToObject(fieldMandatory);
        var userRolesObject = converStringToObject(userRoles);

        var moduleName = $('#flow_module').val();

        //defautl_panel,panel_hide,panel_show
        var callback = {
            success: function(result) {
                var panelName = result.responseText;
                var panelObject = JSON.parse(panelName);
                panelName = jQuery.parseJSON(panelName);
                
                $.each(panelName,function(index,value){   
                    $('#defaultpanel').append("<option value='"+value.panelLabel+"'>"+value.panelName+"</option>");
                    $('#panel_hide').append("<option value='"+value.panelLabel+"'>"+value.panelName+"</option>");
                    $('#panel_show').append("<option value='"+value.panelLabel+"'>"+value.panelName+"</option>");
                });//end of each

                for(var i=0;i<panelObject.length;i++){
                    if(jQuery.inArray(panelObject[i].panelLabel,defaultPanelObject) != '-1'){
                        $("select[id='defaultpanel']").find("option[value='"+panelObject[i].panelLabel+"']").attr("selected",true);    
                    }//end of if
                }//end of for

                for(var i=0;i<panelObject.length;i++){
                    if(jQuery.inArray(panelObject[i].panelLabel,panelHideObject) != '-1'){
                        $("select[id='panel_hide']").find("option[value='"+panelObject[i].panelLabel+"']").attr("selected",true);    
                    }//end of if
                }//end of for

                for(var i=0;i<panelObject.length;i++){
                    if(jQuery.inArray(panelObject[i].panelLabel,panelShowObject) != '-1'){
                        $("select[id='panel_show']").find("option[value='"+panelObject[i].panelLabel+"']").attr("selected",true);    
                    }//end of if
                }//end of for                              
            }//end of success
        }//end of callback

        //defaultfield,field_hide,field_show,field_readonly,field_mandatory
        var callback2 = {
            success: function(result) {                         
                var fieldName = result.responseText;
                var fieldObject = JSON.parse(fieldName);
                var obj = jQuery.parseJSON(fieldName);
                $.each(obj, function (index, value) {
                    $('#defaultfield').append("<option value='"+value.fieldname+"'>"+value.label+"</option>");
                    $('#field_hide').append("<option value='"+value.fieldname+"'>"+value.label+"</option>");
                    $('#field_show').append("<option value='"+value.fieldname+"'>"+value.label+"</option>");
                    $('#field_readonly').append("<option value='"+value.fieldname+"'>"+value.label+"</option>");
                    if(value.type != 'bool'){
                        $('#field_mandatory').append("<option value='"+value.fieldname+"'>"+value.label+"</option>");    
                    }
                });//end of each 

                for(var i=0;i<fieldObject.length;i++){
                    if(jQuery.inArray(fieldObject[i].fieldname,defaultFieldObject) != '-1'){
                        $("select[id='defaultfield']").find("option[value='"+fieldObject[i].fieldname+"']").attr("selected",true);  
                    }//end of if
                }//end of for

                for(var i=0;i<fieldObject.length;i++){
                    if(jQuery.inArray(fieldObject[i].fieldname,fieldHideObject) != '-1'){
                        $("select[id='field_hide']").find("option[value='"+fieldObject[i].fieldname+"']").attr("selected",true);  
                    }//end of if
                }//end of for

                for(var i=0;i<fieldObject.length;i++){
                    if(jQuery.inArray(fieldObject[i].fieldname,fieldShowObject) != '-1'){
                        $("select[id='field_show']").find("option[value='"+fieldObject[i].fieldname+"']").attr("selected",true);  
                    }//end of if
                }//end of for

                for(var i=0;i<fieldObject.length;i++){
                    if(jQuery.inArray(fieldObject[i].fieldname,fieldReadOnlyObject) != '-1'){
                        $("select[id='field_readonly']").find("option[value='"+fieldObject[i].fieldname+"']").attr("selected",true);  
                    }//end of if
                }//end of for

                for(var i=0;i<fieldObject.length;i++){
                    if(jQuery.inArray(fieldObject[i].fieldname,fieldMandatoryObject) != '-1'){
                        $("select[id='field_mandatory']").find("option[value='"+fieldObject[i].fieldname+"']").attr("selected",true);  
                    }//end of if
                }//end of for

                //display selected option first
                $('#defaultfield option:selected').prependTo('#defaultfield'); 
                $('#field_hide option:selected').prependTo('#field_hide');
                $('#field_show option:selected').prependTo('#field_show');
                $('#field_readonly option:selected').prependTo('#field_readonly'); 
                $('#field_mandatory option:selected').prependTo('#field_mandatory');       
            }//end of success
        }//end of callback2

        //user roles
        var callback3 = {
            success: function(result) { 
                var rolesObject = JSON.parse(result.responseText);
                var obj = jQuery.parseJSON(result.responseText);                        
                $.each(obj, function (index, value) { 
                    $('#user_roles').append("<option value='"+value.id+"'>"+value.name+"</option>");
                });//end of each
                for(var i=0;i<obj.length;i++){
                    if(jQuery.inArray(obj[i].id,userRoles) != '-1'){
                        $("select[id='user_roles']").find("option[value='"+obj[i].id+"']").attr("selected",true);  
                    }//end of if
                }//end of for
            }//end of success
        }//end of callback3

        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsAllModulePanels&flow_module="+moduleName,callback);
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsAllModuleFields&flow_module="+moduleName,callback2);
        YAHOO.util.Connect.asyncRequest ("GET", "index.php?entryPoint=VIDynamicPanelsDisplayRoles",callback3);
    });//end of function

    function converStringToObject(data){
        var convertStringData = JSON.stringify(data);
        var dataObject = jQuery.parseJSON(convertStringData);
        return dataObject;
    }//end of function

    var clickCount = 0;
    function save(){
        if(clickCount == 0){
            var id = "{/literal}{$RECORDID}{literal}";
            var formData = $('form');
            var disabled = formData.find(':disabled').removeAttr('disabled');
            var formData = formData.serialize();
            $.ajax({
                url: "index.php?entryPoint=VIAddDynamicPanels",
                type: "post",
                data: {val : formData,
                       id : id},
                success: function (response) {
                    window.location.href = "index.php?module=Administration&action=vi_dynamicpanelslistview";
                }//end of success
            });//end of ajax
        }//end of if
        clickCount = clickCount + 1;  
    }//end of save    
</script>   
{/literal}
