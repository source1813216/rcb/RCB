<?php 
 //WARNING: The contents of this file are auto-generated



/**
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Original Author Biztech Co.
 */
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

//Field ACL Section
$mod_strings['LBL_APPLY_FIELD_ACCESS_CONTROL'] = 'Field Level Access Control Configurations';
$mod_strings['LBL_APPLY_FIELD_ACCESS_CONTROL_DESC'] = 'Set user wise access on fields of different modules.';
//Main Section Of Field ACL
$mod_strings['LBL_APPLY_FIELD_ACCESS_CONTROL_TITLE'] = 'Field Level Access Control';
$mod_strings['LBL_APPLY_FIELD_ACCESS_CONTROL_HEADER_DESC'] = 'Apply user wise access control on different fields of different modules.';
//View Labels
$mod_strings['LBL_FIELD_ACCESS_CONTROL_ACCESS'] = 'Field Level Access Control Configurations';
$mod_strings['LBL_NO_MODULE_SELECTED'] = 'No Module Selected';
$mod_strings['LBL_MODULE_NAME'] = 'Administration';
//Licence Configuration Section
$mod_strings['LBL_FIELD_ACCESS_CONTROL_LICENCE'] = 'License Configuration';
$mod_strings['LBL_FIELD_ACCESS_CONTROL_LINK_DESC'] = 'Validate license and module configuration.';
$mod_strings['LBL_LICENCE_CONFIG_SECTION'] = 'License Configuration';
$mod_strings['LBL_LICENCE_CONFIG_SECTION_DESCRIPTION'] = 'Validate license and module configuration to access field level access';



$mod_strings['LBL_FI_SUPERUSERACCESSLICENSEADDON'] = 'Superuser Access License Add-on';
$mod_strings['LBL_FI_SUPERUSERACCESSLICENSEADDON_LICENSE_TITLE'] = 'Superuser Access License';
$mod_strings['LBL_FI_SUPERUSERACCESSLICENSEADDON_LICENSE'] = 'Manage and configure the license for this add-on';


$mod_strings['LBL_MTS_SUBPANEL_TOGGLE_CONFIG'] = 'MTS Subpanel Toggle Settings';
$mod_strings['LBL_MTS_SUBPANEL_TOGGLE_CONFIG_TITLE'] = 'Config MTS Subpanel Toggle add-on';
$mod_strings['LBL_MTS_SUBPANEL_TOGGLE_CONFIG_DESC'] = 'Change settings for MTS Subpanel Toggle';

$mod_strings['LBL_MTS_SUBPANEL_TOGGLE_CONFIG_LICENSE'] = 'MTS Subpanel Toggle License Configuration';
$mod_strings['LBL_MTS_SUBPANEL_TOGGLE_CONFIG_LICENSE_DESC'] = 'Manage and configure the license for MTS Subpanel Toggle License add-on';

/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Auto Populate Fields';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Manage Auto Fill Up Fields Depending on Relate Fields';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'BACK';
$mod_strings['LBL_NEXT_BUTTON'] = 'NEXT';
$mod_strings['LBL_CLEAR_BUTTON'] = 'CLEAR';
$mod_strings['LBL_SAVE_BUTTON'] = 'SAVE';
$mod_strings['LBL_CANCEL_BUTTON'] = 'CANCEL';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Select an Option';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Source Module(Copy Data To)';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_SELECT_MODULE'] = 'Select Module';
$mod_strings['LBL_ACTIVE'] = 'Active';
$mod_strings['LBL_INACTIVE'] = 'Inactive';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Field Mapping';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Add Field Mapping';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Select Related Field';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Add Field Mapping Block';
$mod_strings['LBL_SOURCE_MODULE'] = 'Source Module';
$mod_strings['LBL_TARGET_MODULE'] = 'Target Module';
$mod_strings['LBL_FIELDS'] = 'Fields';
$mod_strings['LBL_RELATED_FIELD'] = 'Related Field';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Calculate Field';
$mod_strings['LBL_FUNCTIONS'] = 'Functions';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Add Calculate Field';
$mod_strings['LBL_INPUT_FORMULA'] = 'Input(Formula)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Add Calculate Field Block';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'Add';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'Subtract';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Multiply';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Divide';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'String Length';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'Concatenation';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Log(Logarithm)';
$mod_strings['LBL_LN'] = 'Ln(Natural Log)';
$mod_strings['LBL_ABSOLUTE'] = 'Absolute';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Average';
$mod_strings['LBL_FUNCTION_POWER'] = 'Power';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Date Difference';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'Percentage';
$mod_strings['LBL_FUNCTION_MOD'] = 'Mod(Modulo)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'Minimum';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'Negate';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Floor';
$mod_strings['LBL_FUNCTION_CEIL'] = 'Ceil';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Add Hours';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Add Day';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Add Week';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Add Month';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Add Year';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Sub Hours';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Sub Days';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Sub Week';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Sub Month';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Sub Year';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Diff Days';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Diff Hour';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Diff Minute';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Diff Month';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Week Diff';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Diff Year';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = "Use only 'Integer' OR 'Decimal' fields in Numeric operations";
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = "Use only 'String' fields in String operations";
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = "Use only 'Date' OR 'Datetime' fields in Date operations";
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = 'Please Fill All Required Field!!!';
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Please Select Matching Type Field!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = 'Please Enter Value';
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' Module Configuration already Exists. Please Select another Module.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Please Select Related Field before Clicking on the "Add Field Mapping Block" button.';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Please Select Field Mapping Field.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'For ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = ', Field Mapping block already added. If you want to add another field mapping for ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' then use existing block.';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Add Calculate Field Block';
$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = ', Calculate Field block already added. If you want to add another calculate field for ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = 'Please fill All Fields!!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Please Replace "string_field" with Field Name which is available in Target Module Fields for';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Please Replace "number_field" with Field Name which is available in Target Module Fields for';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Please Replace "date_field" with Field Name which is available in Target Module Fields for';
$mod_strings['LBL_BASE_ERROR'] = 'Please Replace "base" Parameter with any base of log like 2, 10 for';
$mod_strings['LBL_DAYS_ERROR'] = 'Please Replace "days" Parameter with any Number for';
$mod_strings['LBL_WEEK_ERROR'] = 'Please Replace "week" Parameter with any Number for';
$mod_strings['LBL_MONTH_ERROR'] = 'Please Replace "month" Parameter with any Number for';
$mod_strings['LBL_YEAR_ERROR'] = 'Please Replace "year" Parameter with any Number for';
$mod_strings['LBL_HOURS_ERROR'] = 'Please Replace "hours" Parameter with any Number for';
$mod_strings['LBL_FUNCTION'] = 'Function';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Select Mathematical or Logical function to perform calculation on field.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Select field on which you need to perform calculation'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Once you select function, it will auto populate in Input(Formula) box and if you select field on which you need to perform calculation then it is also auto populate. You need to copy that field and put in function as parameter.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Select Primary Module Field to auto fill up calculation based on related field selection';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Update License';
$mod_strings['LBL_ADD_NEW'] = '+ Add New';
$mod_strings['LBL_CREATE_MESSAGE'] = 'You currently have no records saved.';
$mod_strings['LBL_CREATE'] = 'CREATE';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' one now.';
$mod_strings['LBL_DELETE'] = "Delete";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Please select records.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Are you sure you want to delete';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'these';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'this';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'row?';
$mod_strings['LBL_EDIT'] = 'Edit';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "Status Activated Successfully!!";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Status Deactivated Successfully!!";

$mod_strings['LBL_BULK_ACTION'] = 'BULK ACTION';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' field is already selected for Field Mapping. Please select another Field.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' field is already selected for Calculate Field. Please select another Field.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Please add "year" Parameter with any Number for';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Please add "month" Parameter with any Number for';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Please add "week" Parameter with any Number for';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Please add "days" Parameter with any Number for';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Please add "base" Parameter with any Number for';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Please add "hours" Parameter with any Number for';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'in Input Formula';

/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Conditional Notifications';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Manage Conditional Notification for modules';
$mod_strings['LBL_ADD_NEW'] = '+Add New';
$mod_strings['LBL_SELECTED_MODULE'] = 'Module';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'Description';
$mod_strings['LBL_DELETE'] = 'Delete';
$mod_strings['LBL_CREATE_MESSAGE'] = 'You currently have no records saved.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'one now.';
$mod_strings['LBL_SELECT_MODULE'] = 'Select Module';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Add Conditions';
$mod_strings['LBL_ADD_TASKS'] = 'Add Tasks';
$mod_strings['LBL_CANCEL'] = 'Cancel';
$mod_strings['LBL_BACK'] = 'Back';
$mod_strings['LBL_NEXT'] = 'Next';
$mod_strings['LBL_SAVE'] = 'SAVE';
$mod_strings['LBL_CLEAR'] = 'Clear';
$mod_strings['LBL_MODULE_PATH'] = 'Selected Module';
$mod_strings['LBL_FIELD'] = 'Field';
$mod_strings['LBL_OPERATOR'] = 'Operator';
$mod_strings['LBL_VALUE_TYPE'] = 'Value Type';
$mod_strings['LBL_VALUE'] = 'Value';
$mod_strings['LBL_ACTION_TITLE'] = 'Action Title';
$mod_strings['LBL_NOTIFICATION_EDITING'] = 'Notification While Editing/Creating';
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = 'Notification When record is Viewed';
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = 'Notification On Save';
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = 'Do not allowed to Save record';
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = 'Notification When record is Duplicate';
$mod_strings['LBL_INSERT'] = 'Insert';
$mod_strings['LBL_INSERT_FIELDS'] = 'Insert Fields';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'In this steps its allow to select module on which we needs to setup conditional notification';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = 'Setup condition on field of module to display notification';
$mod_strings['LBL_ADD_TASKS_NOTES'] = 'Setup various action when condition satisfied on step # 2';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Conditional Operator';
$mod_strings['LBL_NOTE_LABEL'] = 'Note:';
$mod_strings['LBL_NOTE'] = 'Below are the example of Regular Expression when selecting operator as Regular Expression. Do not add slash(/) before and after of any Regular Expression.'; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Only Alpha Values   :=  ([A-z],[a-z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Only Numeric Values   :=  ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'For AlphaNumeric values := (ex.^[a-zA-Z0-9_]*$).';
$mod_strings['LBL_DATE_VALUES'] = 'For Date values := (^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$)';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'For DateTime := (^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])(? [0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$)';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'All';
$mod_strings['LBL_CREATE'] = 'CREATE';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Please select records.";
$mod_strings['LBL_DELETE_MESSAGE'] = "Are you sure you want to delete";
$mod_strings['LBL_DELETE_THESE'] = 'these';
$mod_strings['LBL_DELETE_THIS'] = 'this';
$mod_strings['LBL_DELETE_ROW'] = 'row?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Select an Option';
$mod_strings['LBL_AND_OPERATOR'] = 'AND';
$mod_strings['LBL_OR_OPERATOR'] = 'OR';
$mod_strings['LBL_VALIDATION_MESSAGE'] = 'Please select a required field.';
$mod_strings['LBL_PLZ_ENTER_VALUE'] = 'Please Enter Value!';
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'Notifications';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Notification with Confirmation Dialog';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Update License';






/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Select Primary Module';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Select Default Panels to Hide';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Select Default Fields to Hide';
$mod_strings['LBL_MODULE_PATH'] = 'Selected Module';
$mod_strings['LBL_FIELD'] = 'Field';
$mod_strings['LBL_OPERATOR'] = 'Operator';
$mod_strings['LBL_VALUE_TYPE'] = 'Value Type';
$mod_strings['LBL_VALUE'] = 'Value';
$mod_strings['LBL_SELECT_MODULE'] = 'Select Module';
$mod_strings['LBL_APPLY_CONDITION'] = 'Apply Condition';
$mod_strings['LBL_SET_VISIBILITY'] = 'Set Visibility';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Assign Value';
$mod_strings['LBL_CANCEL'] = 'Cancel';
$mod_strings['LBL_BACK'] = 'Back';
$mod_strings['LBL_NEXT'] = 'Next';
$mod_strings['LBL_SAVE'] = 'SAVE';
$mod_strings['LBL_CLEAR'] = 'Clear';
$mod_strings['LBL_CONDITIONS'] = 'Conditions';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Add Conditions';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Panels to be Hide';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Fields to be Hide';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Panels to be Show';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Fields to be Show';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'Fields to be Readonly';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = 'Fields to be Mandatory';
$mod_strings['LBL_ADD_FIELD'] = 'Add Field';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Dynamic Panels';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'Manage visibility of fields and panels on selection of specific fields.
';
$mod_strings['LBL_ADD_NEW'] = '+Add New';
$mod_strings['LBL_DELETE'] = 'Delete';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = 'Name';
$mod_strings['LBL_PRIMARY_MODULE'] = 'Module';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'Default Hidden Panels';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'Default Hidden Fields';
$mod_strings['LBL_CREATE_MESSAGE'] = 'You currently have no records saved.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'one now.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = 'Specify conditions to hide panels or fields specified in Step 3(Set Visibility)';
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = 'Specify value of any field of the module on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Hide panels when page load first time.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'Hide fields when page load first time.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = 'Selected Panels will be hidden when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = 'Selected Panels will be visible when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = 'Selected Fields will be hidden when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = 'Selected Fields will be visible when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = 'Selected Fields will be readonly when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = 'Selected Fields will be mandatory when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_USER_TYPE'] = 'User Type';
$mod_strings['LBL_USER_ROLES'] = 'Roles';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = 'Select Type of user who should see Hide/Show effect';
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Apply Hide/Show updates to specific roles';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Conditional Operator';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = 'Note: This condition Only applied to User with selected role above.';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = 'for more information.';
$mod_strings['LBL_DATE_ENTERED'] = 'Date Created';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Assign Background Color';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = 'Specify background color of any field of the module on change of the field background color in Step 2(Apply Condition)';
$mod_strings['LBL_CREATE'] = 'CREATE';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Please select records.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Are you sure you want to delete';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'these';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'this';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'row?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Select an Option';
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'All Users';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Regular User';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'System Administrator User';
$mod_strings['LBL_AND'] = 'AND';
$mod_strings['LBL_OR'] = 'OR';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Please Enter Required Field Values';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = 'Please Enter Value!';
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'Selected value is already used.Please select another value.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = "Please select another field for condition because you've selected operation between condition is 'AND'. So you didn't select same field again.";
$mod_strings['LBL_UPDATE_LICENSE'] = 'Update License';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Please select Multienum type field for comparison";




/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_LINK_NAME'] = 'Multiple SMTP';
$mod_strings['LBL_LINK_DESCRIPTION'] = 'Manage Multiple SMTP Configuration';
$mod_strings['LBL_ENABLED_MULTI_SMTP'] = 'Enabled Multiple SMTP';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Update License';

?>