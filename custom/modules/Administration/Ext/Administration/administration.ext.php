<?php 
 //WARNING: The contents of this file are auto-generated



/**
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Original Author Biztech Co.
 */
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
global $sugar_config;
$admin_option_defs = array();
//Licence Configuration Section
$admin_option_defs['Administration']['field_access_control_licence'] = array(
    '',
    //Title Of The Link 
    $mod_strings['LBL_FIELD_ACCESS_CONTROL_LICENCE'],
    //Description For The Link
    $mod_strings['LBL_FIELD_ACCESS_CONTROL_LINK_DESC'],
    //Where To Send The User When The Link Is Clicked
    './index.php?module=Administration&action=field_access_control_licence_config'
);
//Field Level Access Control Setting Section
$admin_option_defs['Administration']['field_access_control_view'] = array(
    '',
    //Title Of The Link 
    'LBL_APPLY_FIELD_ACCESS_CONTROL',
    //Description For The Link
    'LBL_APPLY_FIELD_ACCESS_CONTROL_DESC',
    //Where To Send The User When The Link Is Clicked
    'index.php?module=Administration&action=field_access_control_view'
);
//Main Section Header And Description
$admin_group_header[] = array(
    'LBL_APPLY_FIELD_ACCESS_CONTROL_TITLE',
    '',
    false,
    $admin_option_defs,
    'LBL_APPLY_FIELD_ACCESS_CONTROL_HEADER_DESC'
);

 
$admin_option_defs = array();

$admin_option_defs['FI_SuperuserAccess_info']= array('icon_AdminFI_SuperuserAccess','LBL_FI_SUPERUSERACCESSLICENSEADDON_LICENSE_TITLE','LBL_FI_SUPERUSERACCESSLICENSEADDON','./index.php?module=SuperuserAccess&action=license');
$admin_option_defs['FI_SuperuserAccess_Key'] = array('icon_FI_SuperuserAccess', 'Restrict access admin', 'Restrict access other admin users', './index.php?module=SuperuserAccess&action=fi_restrictaccess');

// Loop through the menus and add to the Users group
$tmp_menu_set = false;
foreach ($admin_group_header as $key => $values)
{
    if ($values[1] == 'FibreCRM Modules')
    {
    	$admin_group_header[$key][3]['Administration']['FI_SuperuserAccess_info'] = $admin_option_defs['FI_SuperuserAccess_info'];
        $admin_group_header[$key][3]['Administration']['FI_SuperuserAccess_Key'] = $admin_option_defs['FI_SuperuserAccess_Key'];
        $tmp_menu_set = true;
    }
}

// Else create new group
if (!$tmp_menu_set)
    $admin_group_header[] = array('<img src="custom/themes/default/images/fi_socialicon_fi_superuseraccess.jpg"> &nbsp; FibreCRM Modules','FibreCRM Modules',false,array('Administration' => $admin_option_defs),'');







$admin_option_defs = array();
$admin_option_defs['Administration']['MTS_SubpanelToggle'] = array('Administration', 'LBL_MTS_SUBPANEL_TOGGLE_CONFIG', 'LBL_MTS_SUBPANEL_TOGGLE_CONFIG_DESC', './index.php?module=MTS_SubpanelToggle&action=index');
$admin_option_defs['Administration']['MTS_SubpanelToggleLicense'] = array('Administration', 'LBL_MTS_SUBPANEL_TOGGLE_CONFIG_LICENSE', 'LBL_MTS_SUBPANEL_TOGGLE_CONFIG_LICENSE_DESC', './index.php?module=MTS_SubpanelToggle&action=license');

$admin_group_header[] = array('LBL_MTS_SUBPANEL_TOGGLE_CONFIG', '', false, $admin_option_defs, 'LBL_MTS_SUBPANEL_TOGGLE_CONFIG_TITLE');

/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php");
require_once('modules/VIAutoPopulateFieldsLicenseAddon/license/VIAutoPopulateFieldsOutfittersLicense.php');
require_once('include/MVC/Controller/SugarController.php');
global $sugar_config, $mod_strings;
$dynamicURL = $sugar_config['site_url'];
$url = $dynamicURL."/index.php?module=VIAutoPopulateFieldsLicenseAddon&action=license";
$where = array('name' => "'lic_auto-populate-fields'");
$sqlLicenseCheck = getAutoPopulateFieldsRecord('config',$fieldNames = array(), $where);
$resultData = $GLOBALS['db']->fetchOne($sqlLicenseCheck);
if(!empty($resultData)){
    $validate_license = VIAutoPopulateFieldsOutfittersLicense::isValid('VIAutoPopulateFieldsLicenseAddon');
    if($validate_license !== true) {
        if(is_admin($current_user)) {
            SugarApplication::appendErrorMessage('VIAutoPopulateFieldsLicenseAddon is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed <a href='.$url.'>Click Here</a>');
        }
            echo '<h2><p class="error">VIAutoPopulateFieldsLicenseAddon is no longer active</p></h2><p class="error">Please renew your subscription or check your license configuration.</p><a href='.$url.'>Click Here</a>';
    }else{
    	foreach ($admin_group_header as $key => $value) {
            $values[] = $value[0];
        }	
        if (in_array("Other", $values)){
                $array['AutoPopulateFields'] = array('AutoPopulateFields',
                									$mod_strings["LBL_AUTOPOPULATE_FIELDS"],
                									$mod_strings["LBL_AUTOPOPULATE_FIELDS_DESCRIPTION"],
                									'./index.php?module=Administration&action=vi_autopopulatefieldslistview',
                									'auto-populate-fields');
                $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
        }else{
        	$admin_option_defs = array();
			$admin_option_defs['Administration']['AutoPopulateFields'] = array(
				//Icon name. Available icons are located in ./themes/default/images
				'AutoPopulateFields',

				//Link name label 
				$mod_strings["LBL_AUTOPOPULATE_FIELDS"],

				//Link description label
				$mod_strings["LBL_AUTOPOPULATE_FIELDS_DESCRIPTION"],

				//Link URL
				'./index.php?module=Administration&action=vi_autopopulatefieldslistview',
				'auto-populate-fields'
			);
			$admin_group_header['Other'] = array(
				//Section header label
				'Other',

				//$other_text parameter for get_form_header()
				'',

				//$show_help parameter for get_form_header()
				false,

				//Section links
				$admin_option_defs,

				//Section description label
				''
			);
        }	
    }
}else{
	foreach ($admin_group_header as $key => $value) {
        $values[] = $value[0];
    }
    if (in_array("Other", $values))
    {
        $array['AutoPopulateFields'] = array('AutoPopulateFields',$mod_strings["LBL_AUTOPOPULATE_FIELDS"],
                										  $mod_strings["LBL_AUTOPOPULATE_FIELDS_DESCRIPTION"],
                										  './index.php?module=VIAutoPopulateFieldsLicenseAddon&action=license',
                										  'auto-populate-fields');
        $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
    }else{
    	$admin_option_defs = array();
		$admin_option_defs['Administration']['AutoPopulateFields'] = array(
			//Icon name. Available icons are located in ./themes/default/images
			'AutoPopulateFields',

			//Link name label 
			$mod_strings["LBL_AUTOPOPULATE_FIELDS"],

			//Link description label
			$mod_strings["LBL_AUTOPOPULATE_FIELDS_DESCRIPTION"],

			//Link URL
			'./index.php?module=VIAutoPopulateFieldsLicenseAddon&action=license',
			'auto-populate-fields'
		);
		$admin_group_header['Other'] = array(
			//Section header label
			'Other',

			//$other_text parameter for get_form_header()
			'',

			//$show_help parameter for get_form_header()
			false,

			//Section links
			$admin_option_defs,

			//Section description label
			''
		);
    }
}


/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('modules/VIConditionalNotificationsLicenseAddon/license/VIConditionalNotificationsOutfittersLicense.php');
require_once('include/MVC/Controller/SugarController.php');
global $sugar_config;
global $mod_strings;
$dynamicURL = $sugar_config['site_url'];
$url = $dynamicURL."/index.php?module=VIConditionalNotificationsLicenseAddon&action=license";
$sqlLicenseCheck = "SELECT * from config where name = 'lic_conditional-notifications'";
$result = $GLOBALS['db']->query($sqlLicenseCheck);
$resultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($sqlLicenseCheck));
if(!empty($resultData)){
    $validate_license = VIConditionalNotificationsOutfittersLicense::isValid('VIConditionalNotificationsLicenseAddon');
    if($validate_license !== true) {
        if(is_admin($current_user)) {
            SugarApplication::appendErrorMessage('VIConditionalNotificationsLicenseAddon is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed <a href='.$url.'>Click Here</a>');
        }
            echo '<h2><p class="error">VIConditionalNotificationsLicenseAddon is no longer active</p></h2><p class="error">Please renew your subscription or check your license configuration.</p><a href='.$url.'>Click Here</a>';
    }else{
    	foreach ($admin_group_header as $key => $value) {
            $values[] = $value[0];
        }
        if (in_array("Other", $values)){
                $array['ConditionalNotifications'] = array('ConditionalNotifications',
                										  $mod_strings["LBL_CONDITIONAL_NOTIFICATIONS"],
                										  $mod_strings["LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION"],
                										  './index.php?module=Administration&action=vi_conditionalnotificationslistview',
                										  'conditional-notifications');
                $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
        }else{
        	$admin_option_defs = array();
			$admin_option_defs['Administration']['ConditionalNotifications'] = array(
				//Icon name. Available icons are located in ./themes/default/images
				'ConditionalNotifications',

				//Link name label 
				$mod_strings["LBL_CONDITIONAL_NOTIFICATIONS"],

				//Link description label
				$mod_strings["LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION"],

				//Link URL
				'./index.php?module=Administration&action=vi_conditionalnotificationslistview',
				'conditional-notifications'
			);
			$admin_group_header['Other'] = array(
				//Section header label
				'Other',

				//$other_text parameter for get_form_header()
				'',

				//$show_help parameter for get_form_header()
				false,

				//Section links
				$admin_option_defs,

				//Section description label
				''
			);
        }	
    }
}else{
	foreach ($admin_group_header as $key => $value) {
        $values[] = $value[0];
    }
    if (in_array("Other", $values)) {
        $array['ConditionalNotifications'] = array('ConditionalNotifications',
                										  $mod_strings["LBL_CONDITIONAL_NOTIFICATIONS"],
                										  $mod_strings["LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION"],
                										  './index.php?module=VIConditionalNotificationsLicenseAddon&action=license',
                										  'conditional-notifications');
        $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
    }else{
		$admin_option_defs['Administration']['ConditionalNotifications'] = array(
			//Icon name. Available icons are located in ./themes/default/images
			'ConditionalNotifications',

			//Link name label 
			$mod_strings["LBL_CONDITIONAL_NOTIFICATIONS"],

			//Link description label
			$mod_strings["LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION"],

			//Link URL
			'./index.php?module=VIConditionalNotificationsLicenseAddon&action=license',
			'conditional-notifications'
		);
		$admin_group_header['Other'] = array(
			//Section header label
			'Other',

			//$other_text parameter for get_form_header()
			'',

			//$show_help parameter for get_form_header()
			false,

			//Section links
			$admin_option_defs,

			//Section description label
			''
		);
    }
}

 
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('modules/VIDynamicPanelsLicenseAddon/license/VIDynamicPanelsOutfittersLicense.php');
require_once('include/MVC/Controller/SugarController.php');
global $sugar_config;
global $mod_strings;
$dynamicURL = $sugar_config['site_url'];
$url = $dynamicURL."/index.php?module=VIDynamicPanelsLicenseAddon&action=license";
$sqlLicenseCheck = "SELECT * from config where name = 'lic_dynamic-panels'";
$result = $GLOBALS['db']->query($sqlLicenseCheck);
$resultData = $GLOBALS['db']->fetchRow($result);
if(!empty($resultData)){
    $validate_license = VIDynamicPanelsOutfittersLicense::isValid('VIDynamicPanelsLicenseAddon');
    if($validate_license !== true) {
        if(is_admin($current_user)) {
            SugarApplication::appendErrorMessage('VIDynamicPanelsLicenseAddon is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed <a href='.$url.'>Click Here</a>');
        }
            echo '<h2><p class="error">VIDynamicPanelsLicenseAddon is no longer active</p></h2><p class="error">Please renew your subscription or check your license configuration.</p><a href='.$url.'>Click Here</a>';
    }else{
        foreach ($admin_group_header as $key => $value) {
            $values[] = $value[0];
        }
        if (in_array("Other", $values))
        {
                $array['dynamic_panels'] = array('DynamicPanels',
                                                          $mod_strings["LBL_DYNAMIC_PANELS"],
                                                          $mod_strings["LBL_DYNAMIC_PANELS_DESCRIPTION"],
                                                          './index.php?module=Administration&action=vi_dynamicpanelslistview',
                                                          'dynamic-panels');
                $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
        }else{
            $admin_option_defs = array();
            $admin_option_defs['Administration']['dynamic_panels'] = array(
            //Icon name. Available icons are located in ./themes/default/images
            'DynamicPanels',

            //Link name label 
            $mod_strings["LBL_DYNAMIC_PANELS"],

            //Link description label
            $mod_strings["LBL_DYNAMIC_PANELS_DESCRIPTION"],

            //Link URL
            './index.php?module=Administration&action=vi_dynamicpanelslistview',
             'dynamic-panels',
            );
            $admin_group_header['Other'] = array(
            //Section header label
            'Other',

            //$other_text parameter for get_form_header()
            '',

            //$show_help parameter for get_form_header()
            false,

            //Section links
            $admin_option_defs,

            //Section description label
            ''
            
            );
        }
    }
}else{
    foreach ($admin_group_header as $key => $value) {
        $values[] = $value[0];
    }
    if (in_array("Other", $values))
    {
        $array['dynamic_panels'] = array('DynamicPanels',
                                                      $mod_strings["LBL_DYNAMIC_PANELS"],
                                                      $mod_strings["LBL_DYNAMIC_PANELS_DESCRIPTION"],
                                                     './index.php?module=VIDynamicPanelsLicenseAddon&action=license',
                                                      'dynamic-panels');
        $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
    }else{
        $admin_option_defs = array();
        $admin_option_defs['Administration']['dynamic_panels'] = array(
        //Icon name. Available icons are located in ./themes/default/images
        'DynamicPanels',

        //Link name label 
        $mod_strings["LBL_DYNAMIC_PANELS"],


        //Link description label
        $mod_strings["LBL_DYNAMIC_PANELS_DESCRIPTION"],

        //Link URL
        './index.php?module=VIDynamicPanelsLicenseAddon&action=license',
        'dynamic-panels',
        );
        $admin_group_header['Other'] = array(
        //Section header label
        'Other',

        //$other_text parameter for get_form_header()
        '',

        //$show_help parameter for get_form_header()
        false,

        //Section links
        $admin_option_defs,

        //Section description label
        ''
        
        );
    }      
}



/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('modules/VIMultiSMTPLicenseAddon/license/VIMultipleSMTPOutfittersLicense.php');
require_once('include/MVC/Controller/SugarController.php');
global $sugar_config;
$dynamicURL = $sugar_config['site_url'];
$url = $dynamicURL."/index.php?module=VIMultiSMTPLicenseAddon&action=license";
$sqlLicenseCheck = "SELECT * from config where name = 'lic_vimultiple-smtp'";
$result = $GLOBALS['db']->query($sqlLicenseCheck);
$resultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($sqlLicenseCheck));
if(!empty($resultData)){
    $validate_license = VIMultipleSMTPOutfittersLicense::isValid('VIMultiSMTPLicenseAddon');
    if($validate_license !== true) {
        if(is_admin($current_user)) {
            SugarApplication::appendErrorMessage('MultiSMTPLicenseAddon is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed <a href='.$url.'>Click Here</a>');
        }
            echo '<h2><p class="error">MultiSMTPLicenseAddon is no longer active. Please renew your subscription or check your license configuration.</p></h2><a href='.$url.'>Click Here</a>';
    }else{
         foreach ($admin_group_header as $key => $value) {
                $values[] = $value[0];
            }
            if (in_array("Other", $values))
            {
                $array['email-settings'] = array('email-settings','Multiple SMTP','Manage Multiple SMTP Configuration','./index.php?module=Administration&action=enablemultismtp','email-settings');
                $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
            }
            else
            {
                $admin_option_defs = array();
                $admin_option_defs['Administration']['email-settings'] = array(
                    //Icon name. Available icons are located in ./themes/default/images
                    'Administration',
                    
                    //Link name label 
                    'Multiple SMTP',
                    
                    //Link description label
                    'Manage Multiple SMTP Configuration',
                    
                    //Link URL
                    './index.php?module=Administration&action=enablemultismtp',
                    'email-settings',
                );

                $admin_group_header['Other'] = array(
                    //Section header label
                    'Other',
                    
                    //$other_text parameter for get_form_header()
                    '',
                    
                    //$show_help parameter for get_form_header()
                    false,
                    
                    //Section links
                    $admin_option_defs, 
                    
                    //Section description label
                    ''
                );
            }
        }
}else{
    foreach ($admin_group_header as $key => $value) {
        $values[] = $value[0];
    }
    if (in_array("Other", $values))
    {
        $array['email-settings'] = array('email-settings','Multiple SMTP','Manage Multiple SMTP Configuration','./index.php?module=VIMultiSMTPLicenseAddon&action=license','email-settings');
        $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
    }
    else
    {
        $admin_option_defs['Administration']['email-settings'] = array(
            //Icon name. Available icons are located in ./themes/default/images
            'Administration',
            
            //Link name label 
            'Multiple SMTP',
            
            //Link description label
            'Manage Multiple SMTP Configuration',
            
            //Link URL
            './index.php?module=VIMultiSMTPLicenseAddon&action=license',
            'email-settings',
        );

        $admin_group_header['Other'] = array(
            //Section header label
            'Other',
            
            //$other_text parameter for get_form_header()
            '',
            
            //$show_help parameter for get_form_header()
            false,
            
            //Section links
            $admin_option_defs, 
            
            //Section description label
            ''
        );
    }
}

?>