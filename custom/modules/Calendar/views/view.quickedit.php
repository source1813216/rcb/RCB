<?php

/**
 * 
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */

require_once('include/MVC/View/views/view.quickedit.php');

class CalendarViewQuickEdit extends ViewQuickEdit {

    var $ev;
    protected $editable;

    public function preDisplay() {
        $this->bean = $this->view_object_map['currentBean'];

        if ($this->bean->ACLAccess('Save')) {
            $this->editable = 1;
        } else {
            $this->editable = 0;
        }
    }

    public function display() {
        require_once("modules/Calendar/CalendarUtils.php");
        require_once 'modules/access_control_fields/acl_checkaccess.php';
        $module = $this->view_object_map['currentModule'];
        global $current_user;
        $_REQUEST['module'] = $module;

        $base = 'modules/' . $module . '/metadata/';
        $source = 'custom/' . $base . 'quickcreatedefs.php';
        if (!file_exists($source)) {
            $source = $base . 'quickcreatedefs.php';
            if (!file_exists($source)) {
                $source = 'custom/' . $base . 'editviewdefs.php';
                if (!file_exists($source)) {
                    $source = $base . 'editviewdefs.php';
                }
            }
        }

        $GLOBALS['mod_strings'] = return_module_language($GLOBALS['current_language'], $module);
        $tpl = $this->getCustomFilePathIfExists('include/EditView/EditView.tpl');
        $acl_obj = $this->bean->object_name;
        $user_id = $current_user->id;
        $acl_module = $this->bean->module_dir;
        if ($this->bean) {
            $is_owner = $this->bean->isOwner($current_user->id);
        }
        limit_access_of_users($acl_module, $acl_obj, $user_id, $refresh = false);

        if (is_array($_SESSION['ACL'][$user_id][$acl_module]['fields']))
            foreach ($this->bean->field_defs as $acl_field_name => $limit) {
                if (!$is_owner && $this->bean->assigned_user_id == '' && $this->bean->created_by == $current_user->id) {
                    $is_owner = true;
                }
                if (array_key_exists($acl_field_name, $_SESSION['ACL'][$user_id][$acl_module]['fields'])) {
                    $this->bean->field_defs[$acl_field_name]['acl'] = checkIfAccess($acl_field_name, $acl_module, $user_id, $is_owner);
                } else {
                    $this->bean->field_defs[$acl_field_name]['acl'] = 4;
                }
            }
        $this->ev = new EditView();
        $this->ev->view = "QuickCreate";
        $this->ev->ss = new Sugar_Smarty();
        $this->ev->formName = "CalendarEditView";
        $this->ev->setup($module, $this->bean, $source, $tpl);
        $this->ev->defs['templateMeta']['form']['headerTpl'] = "modules/Calendar/tpls/editHeader.tpl";
        $this->ev->defs['templateMeta']['form']['footerTpl'] = "modules/Calendar/tpls/empty.tpl";
        $this->ev->process(false, "CalendarEditView");

        if (!empty($this->bean->id)) {
            require_once('include/json_config.php');
            global $json;
            $json = getJSONobj();
            $json_config = new json_config();
            $GRjavascript = $json_config->getFocusData($module, $this->bean->id);
        } else {
            $GRjavascript = "";
        }

        $json_arr = array(
            'access' => 'yes',
            'module_name' => $this->bean->module_dir,
            'record' => $this->bean->id,
            'edit' => $this->editable,
            'html' => $this->ev->display(false, true),
            'gr' => $GRjavascript,
        );

        if ($repeat_arr = CalendarUtils::get_sendback_repeat_data($this->bean)) {
            $json_arr = array_merge($json_arr, array("repeat" => $repeat_arr));
        }

        ob_clean();
        echo json_encode($json_arr);
    }

}
