<?php
// created: 2021-06-18 14:06:19
$mod_strings = array (
  'LBL_ACCOUNT_NAME' => 'Client Name:',
  'LBL_ACCOUNTS' => 'Clients',
  'LBL_ACCOUNT_ID' => 'Client ID',
  'LBL_LIST_ACCOUNT_NAME' => 'Client Name',
  'LBL_AOS_QUOTES' => 'Initial Event Approval',
  'LBL_AOS_CONTRACTS' => 'Site Inspection',
);