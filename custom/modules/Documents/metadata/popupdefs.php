<?php
$popupMeta = array (
    'moduleMain' => 'Documents',
    'varName' => 'DOCUMENTS',
    'orderBy' => 'name',
    'whereClauses' => array (
  'name' => 'documents.name',
  'filename' => 'documents.filename',
  'latest_revision' => 'documents.latest_revision',
  'template_type' => 'documents.template_type',
  'leads_documents_1_name' => 'documents.leads_documents_1_name',
  'assigned_user_name' => 'documents.assigned_user_name',
),
    'searchInputs' => array (
  0 => 'name',
  1 => 'filename',
  2 => 'latest_revision',
  3 => 'template_type',
  4 => 'leads_documents_1_name',
  5 => 'assigned_user_name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NAME',
    'width' => '10%',
    'name' => 'name',
  ),
  'filename' => 
  array (
    'type' => 'file',
    'label' => 'LBL_FILENAME',
    'width' => '10%',
    'name' => 'filename',
  ),
  'latest_revision' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_LATEST_REVISION',
    'width' => '10%',
    'name' => 'latest_revision',
  ),
  'template_type' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_TEMPLATE_TYPE',
    'width' => '10%',
    'name' => 'template_type',
  ),
  'leads_documents_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_LEADS_DOCUMENTS_1_FROM_LEADS_TITLE',
    'id' => 'LEADS_DOCUMENTS_1LEADS_IDA',
    'width' => '10%',
    'name' => 'leads_documents_1_name',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'name' => 'assigned_user_name',
  ),
),
    'listviewdefs' => array (
),
);
