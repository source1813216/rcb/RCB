<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-06-23 10:34:25
$dictionary["Document"]["fields"]["edp_endorsements_documents_1"] = array (
  'name' => 'edp_endorsements_documents_1',
  'type' => 'link',
  'relationship' => 'edp_endorsements_documents_1',
  'source' => 'non-db',
  'module' => 'EDP_Endorsements',
  'bean_name' => 'EDP_Endorsements',
  'vname' => 'LBL_EDP_ENDORSEMENTS_DOCUMENTS_1_FROM_EDP_ENDORSEMENTS_TITLE',
  'id_name' => 'edp_endorsements_documents_1edp_endorsements_ida',
);
$dictionary["Document"]["fields"]["edp_endorsements_documents_1_name"] = array (
  'name' => 'edp_endorsements_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EDP_ENDORSEMENTS_DOCUMENTS_1_FROM_EDP_ENDORSEMENTS_TITLE',
  'save' => true,
  'id_name' => 'edp_endorsements_documents_1edp_endorsements_ida',
  'link' => 'edp_endorsements_documents_1',
  'table' => 'edp_endorsements',
  'module' => 'EDP_Endorsements',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["edp_endorsements_documents_1edp_endorsements_ida"] = array (
  'name' => 'edp_endorsements_documents_1edp_endorsements_ida',
  'type' => 'link',
  'relationship' => 'edp_endorsements_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_EDP_ENDORSEMENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2021-06-14 17:23:42
$dictionary["Document"]["fields"]["edp_event_documents_documents_1"] = array (
  'name' => 'edp_event_documents_documents_1',
  'type' => 'link',
  'relationship' => 'edp_event_documents_documents_1',
  'source' => 'non-db',
  'module' => 'EDP_Event_Documents',
  'bean_name' => 'EDP_Event_Documents',
  'vname' => 'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_EDP_EVENT_DOCUMENTS_TITLE',
  'id_name' => 'edp_event_documents_documents_1edp_event_documents_ida',
);
$dictionary["Document"]["fields"]["edp_event_documents_documents_1_name"] = array (
  'name' => 'edp_event_documents_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_EDP_EVENT_DOCUMENTS_TITLE',
  'save' => true,
  'id_name' => 'edp_event_documents_documents_1edp_event_documents_ida',
  'link' => 'edp_event_documents_documents_1',
  'table' => 'edp_event_documents',
  'module' => 'EDP_Event_Documents',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["edp_event_documents_documents_1edp_event_documents_ida"] = array (
  'name' => 'edp_event_documents_documents_1edp_event_documents_ida',
  'type' => 'link',
  'relationship' => 'edp_event_documents_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2021-05-18 17:58:42
$dictionary["Document"]["fields"]["iep_initial_engagement_documents_1"] = array (
  'name' => 'iep_initial_engagement_documents_1',
  'type' => 'link',
  'relationship' => 'iep_initial_engagement_documents_1',
  'source' => 'non-db',
  'module' => 'IEP_Initial_Engagement',
  'bean_name' => 'IEP_Initial_Engagement',
  'vname' => 'LBL_IEP_INITIAL_ENGAGEMENT_DOCUMENTS_1_FROM_IEP_INITIAL_ENGAGEMENT_TITLE',
  'id_name' => 'iep_initial_engagement_documents_1iep_initial_engagement_ida',
);
$dictionary["Document"]["fields"]["iep_initial_engagement_documents_1_name"] = array (
  'name' => 'iep_initial_engagement_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_IEP_INITIAL_ENGAGEMENT_DOCUMENTS_1_FROM_IEP_INITIAL_ENGAGEMENT_TITLE',
  'save' => true,
  'id_name' => 'iep_initial_engagement_documents_1iep_initial_engagement_ida',
  'link' => 'iep_initial_engagement_documents_1',
  'table' => 'iep_initial_engagement',
  'module' => 'IEP_Initial_Engagement',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["iep_initial_engagement_documents_1iep_initial_engagement_ida"] = array (
  'name' => 'iep_initial_engagement_documents_1iep_initial_engagement_ida',
  'type' => 'link',
  'relationship' => 'iep_initial_engagement_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_IEP_INITIAL_ENGAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2021-05-18 18:18:41
$dictionary["Document"]["fields"]["leads_documents_1"] = array (
  'name' => 'leads_documents_1',
  'type' => 'link',
  'relationship' => 'leads_documents_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_DOCUMENTS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_documents_1leads_ida',
);
$dictionary["Document"]["fields"]["leads_documents_1_name"] = array (
  'name' => 'leads_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_DOCUMENTS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_documents_1leads_ida',
  'link' => 'leads_documents_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["Document"]["fields"]["leads_documents_1leads_ida"] = array (
  'name' => 'leads_documents_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2021-07-09 09:34:34
$dictionary["Document"]["fields"]["mou_mou_documents_1"] = array (
  'name' => 'mou_mou_documents_1',
  'type' => 'link',
  'relationship' => 'mou_mou_documents_1',
  'source' => 'non-db',
  'module' => 'MOU_MOU',
  'bean_name' => 'MOU_MOU',
  'vname' => 'LBL_MOU_MOU_DOCUMENTS_1_FROM_MOU_MOU_TITLE',
  'id_name' => 'mou_mou_documents_1mou_mou_ida',
);
$dictionary["Document"]["fields"]["mou_mou_documents_1_name"] = array (
  'name' => 'mou_mou_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_MOU_MOU_DOCUMENTS_1_FROM_MOU_MOU_TITLE',
  'save' => true,
  'id_name' => 'mou_mou_documents_1mou_mou_ida',
  'link' => 'mou_mou_documents_1',
  'table' => 'mou_mou',
  'module' => 'MOU_MOU',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["mou_mou_documents_1mou_mou_ida"] = array (
  'name' => 'mou_mou_documents_1mou_mou_ida',
  'type' => 'link',
  'relationship' => 'mou_mou_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_MOU_MOU_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2021-08-03 18:31:11
$dictionary["Document"]["fields"]["projecttask_documents_1"] = array (
  'name' => 'projecttask_documents_1',
  'type' => 'link',
  'relationship' => 'projecttask_documents_1',
  'source' => 'non-db',
  'module' => 'ProjectTask',
  'bean_name' => 'ProjectTask',
  'vname' => 'LBL_PROJECTTASK_DOCUMENTS_1_FROM_PROJECTTASK_TITLE',
);


// created: 2021-08-03 18:30:24
$dictionary["Document"]["fields"]["project_documents_1"] = array (
  'name' => 'project_documents_1',
  'type' => 'link',
  'relationship' => 'project_documents_1',
  'source' => 'non-db',
  'module' => 'Project',
  'bean_name' => 'Project',
  'vname' => 'LBL_PROJECT_DOCUMENTS_1_FROM_PROJECT_TITLE',
);


 // created: 2021-06-15 08:50:42
$dictionary['Document']['fields']['budget_total_c']['inline_edit']='1';
$dictionary['Document']['fields']['budget_total_c']['labelValue']='Budget Total';

 

 // created: 2021-06-15 09:33:19
$dictionary['Document']['fields']['edp_event_documents_id_c']['inline_edit']=1;

 

 // created: 2021-08-30 16:31:47
$dictionary['Document']['fields']['template_type']['required']=true;
$dictionary['Document']['fields']['template_type']['inline_edit']=true;
$dictionary['Document']['fields']['template_type']['merge_filter']='disabled';
$dictionary['Document']['fields']['template_type']['reportable']=true;

 

 // created: 2021-06-15 09:33:19
$dictionary['Document']['fields']['temp_event_document_c']['inline_edit']='1';
$dictionary['Document']['fields']['temp_event_document_c']['labelValue']='Temp Event Document';

 
?>