<?php
// created: 2021-09-02 14:48:35
$mod_strings = array (
  'LBL_TEMPLATE_TYPE' => 'Document Type',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Clients',
  'LBL_BUDGET_TOTAL' => 'Budget Total',
  'LBL_TEMP_EVENT_DOCUMENT_EDP_EVENT_DOCUMENTS_ID' => '&#039;Temp Event Document&#039; (related &#039;&#039; ID)',
  'LBL_TEMP_EVENT_DOCUMENT' => 'Temp Event Document',
  'LBL_CASES_SUBPANEL_TITLE' => 'Case',
  'LBL_AOS_CONTRACTS' => 'Site Inspection',
  'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_EDP_EVENT_DOCUMENTS_TITLE' => 'Bid Submission',
);