<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-05-29 19:57:43
$dictionary['SRP_StdApproval_Routine']['fields']['ceo_office_apv_by_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['ceo_office_apv_by_c']['labelValue']='CEO Office Approval By';

 

 // created: 2021-06-06 18:23:34
$dictionary['SRP_StdApproval_Routine']['fields']['dceo_apv_by_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['dceo_apv_by_c']['labelValue']='Deputy CEO Approval By';

 

 // created: 2021-05-29 19:56:30
$dictionary['SRP_StdApproval_Routine']['fields']['ddm_approval_by_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['ddm_approval_by_c']['labelValue']='DDM Approval By';

 

 // created: 2021-07-08 17:30:43
$dictionary['SRP_StdApproval_Routine']['fields']['dec_approval_by_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['dec_approval_by_c']['labelValue']='DEC Approval By';

 

 // created: 2021-07-08 17:31:15
$dictionary['SRP_StdApproval_Routine']['fields']['legal_approval_by_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['legal_approval_by_c']['labelValue']='Legal Approval By';

 

 // created: 2021-05-29 19:58:05
$dictionary['SRP_StdApproval_Routine']['fields']['member_1_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['member_1_c']['labelValue']='Member 1';

 

 // created: 2021-05-29 19:58:19
$dictionary['SRP_StdApproval_Routine']['fields']['member_2_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['member_2_c']['labelValue']='Member 2';

 

 // created: 2021-05-29 19:58:34
$dictionary['SRP_StdApproval_Routine']['fields']['member_3_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['member_3_c']['labelValue']='Member 3';

 

 // created: 2021-05-29 19:58:47
$dictionary['SRP_StdApproval_Routine']['fields']['member_4_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['member_4_c']['labelValue']='Member 4';

 

 // created: 2021-05-29 19:58:59
$dictionary['SRP_StdApproval_Routine']['fields']['member_5_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['member_5_c']['labelValue']='Member 5';

 

 // created: 2021-05-29 19:59:11
$dictionary['SRP_StdApproval_Routine']['fields']['member_6_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['member_6_c']['labelValue']='Member 6';

 

 // created: 2021-05-29 19:57:13
$dictionary['SRP_StdApproval_Routine']['fields']['mgmt_apv_by_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['mgmt_apv_by_c']['labelValue']='Mgmt Approval By';

 

 // created: 2021-07-08 17:29:55
$dictionary['SRP_StdApproval_Routine']['fields']['ssd_approver_c']['inline_edit']='1';
$dictionary['SRP_StdApproval_Routine']['fields']['ssd_approver_c']['labelValue']='SSD Approval By';

 

 // created: 2021-07-08 17:29:26
$dictionary['SRP_StdApproval_Routine']['fields']['user_id10_c']['inline_edit']=1;

 

 // created: 2021-07-08 17:30:43
$dictionary['SRP_StdApproval_Routine']['fields']['user_id11_c']['inline_edit']=1;

 

 // created: 2021-07-08 17:31:15
$dictionary['SRP_StdApproval_Routine']['fields']['user_id12_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:57:13
$dictionary['SRP_StdApproval_Routine']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:57:43
$dictionary['SRP_StdApproval_Routine']['fields']['user_id2_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:58:05
$dictionary['SRP_StdApproval_Routine']['fields']['user_id3_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:58:19
$dictionary['SRP_StdApproval_Routine']['fields']['user_id4_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:58:34
$dictionary['SRP_StdApproval_Routine']['fields']['user_id5_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:58:47
$dictionary['SRP_StdApproval_Routine']['fields']['user_id6_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:58:59
$dictionary['SRP_StdApproval_Routine']['fields']['user_id7_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:59:11
$dictionary['SRP_StdApproval_Routine']['fields']['user_id8_c']['inline_edit']=1;

 

 // created: 2021-06-06 18:23:34
$dictionary['SRP_StdApproval_Routine']['fields']['user_id9_c']['inline_edit']=1;

 

 // created: 2021-05-29 19:56:30
$dictionary['SRP_StdApproval_Routine']['fields']['user_id_c']['inline_edit']=1;

 
?>