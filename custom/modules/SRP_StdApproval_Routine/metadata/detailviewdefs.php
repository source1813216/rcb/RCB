<?php
$module_name = 'SRP_StdApproval_Routine';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'assigned_user_name',
        ),
        1 => 
        array (
          0 => 'description',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'ddm_approval_by_c',
            'studio' => 'visible',
            'label' => 'LBL_DDM_APPROVAL_BY',
          ),
          1 => 
          array (
            'name' => 'legal_approval_by_c',
            'studio' => 'visible',
            'label' => 'LBL_LEGAL_APPROVAL_BY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'mgmt_apv_by_c',
            'studio' => 'visible',
            'label' => 'LBL_MGMT_APV_BY',
          ),
          1 => 
          array (
            'name' => 'ssd_approver_c',
            'studio' => 'visible',
            'label' => 'LBL_SSD_APPROVER',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'ceo_office_apv_by_c',
            'studio' => 'visible',
            'label' => 'LBL_CEO_OFFICE_APV_BY',
          ),
          1 => 
          array (
            'name' => 'dec_approval_by_c',
            'studio' => 'visible',
            'label' => 'LBL_DEC_APPROVAL_BY',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'dceo_apv_by_c',
            'studio' => 'visible',
            'label' => 'LBL_DCEO_APV_BY',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'member_1_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_1',
          ),
          1 => 
          array (
            'name' => 'member_2_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_2',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'member_3_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_3',
          ),
          1 => 
          array (
            'name' => 'member_4_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_4',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'member_5_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_5',
          ),
          1 => 
          array (
            'name' => 'member_6_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_6',
          ),
        ),
      ),
    ),
  ),
);
;
?>
