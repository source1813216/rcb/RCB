<?php
$module_name = 'STP_Sales_Target';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 'name',
      1 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'sales_persona_c' => 
      array (
        'type' => 'relate',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_SALES_PERSONA',
        'id' => 'USER_ID_C',
        'link' => true,
        'width' => '10%',
        'name' => 'sales_persona_c',
      ),
      'sales_target_c' => 
      array (
        'type' => 'currency',
        'default' => true,
        'label' => 'LBL_SALES_TARGET',
        'currency_format' => true,
        'width' => '10%',
        'name' => 'sales_target_c',
      ),
      'target_year_c' => 
      array (
        'type' => 'int',
        'default' => true,
        'label' => 'LBL_TARGET_YEAR',
        'width' => '10%',
        'name' => 'target_year_c',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
