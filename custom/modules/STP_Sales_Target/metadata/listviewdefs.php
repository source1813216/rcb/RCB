<?php
$module_name = 'STP_Sales_Target';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'SALES_PERSONA_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_SALES_PERSONA',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => '10%',
  ),
  'TARGET_YEAR_C' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_TARGET_YEAR',
    'width' => '10%',
  ),
  'SALES_TARGET_C' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_SALES_TARGET',
    'currency_format' => true,
    'width' => '10%',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'TARGET_END_DATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_TARGET_END_DATE',
    'width' => '10%',
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => false,
  ),
  'CURRENCY_ID' => 
  array (
    'type' => 'currency_id',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_CURRENCY',
    'width' => '10%',
  ),
);
;
?>
