<?php
// created: 2022-10-05 07:25:57
$mod_strings = array (
  'LBL_SALES_PERSONA_USER_ID' => '&#039;Sales Persona&#039; (related &#039;User&#039; ID)',
  'LBL_SALES_PERSONA' => 'Sales Person',
  'LBL_TARGET_YEAR' => 'Target Year',
  'LBL_SALES_TARGET' => 'Sales Target',
  'LBL_TARGET_END_DATE' => 'Target End Date',
  'LBL_EDITVIEW_PANEL1' => 'Target Details',
  'LBL_DESCRIPTION' => 'Remarks',
  'LBL_DETAILVIEW_PANEL2' => 'Additional Details',
  'LBL_STP_SALES_TARGET_LEADS_1_FROM_LEADS_TITLE' => 'Leads',
  'LBL_NO_OF_EVENTS' => 'No of Events',
  'LBL_NO_OF_SITE_INSPECTIONS' => 'No of Site Inspections',
  'LBL_EDITVIEW_PANEL2' => 'Qty Targets',
  'LBL_DETAILVIEW_PANEL3' => 'Qty Targets',
  'LBL_TARGET_YEAR_TO' => 'Target Year (To)',
);