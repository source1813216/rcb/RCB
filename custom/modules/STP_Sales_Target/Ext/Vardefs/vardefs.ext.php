<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-08-15 18:03:18
$dictionary["STP_Sales_Target"]["fields"]["stp_sales_target_leads_1"] = array (
  'name' => 'stp_sales_target_leads_1',
  'type' => 'link',
  'relationship' => 'stp_sales_target_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'side' => 'right',
  'vname' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_LEADS_TITLE',
);


 // created: 2021-08-15 17:21:06
$dictionary['STP_Sales_Target']['fields']['currency_id']['inline_edit']=1;

 

 // created: 2021-08-17 18:40:17
$dictionary['STP_Sales_Target']['fields']['no_of_events_c']['inline_edit']='1';
$dictionary['STP_Sales_Target']['fields']['no_of_events_c']['labelValue']='No of Events';

 

 // created: 2021-08-17 18:40:51
$dictionary['STP_Sales_Target']['fields']['no_of_site_inspections_c']['inline_edit']='1';
$dictionary['STP_Sales_Target']['fields']['no_of_site_inspections_c']['labelValue']='No of Site Inspections';

 

 // created: 2021-08-15 17:18:16
$dictionary['STP_Sales_Target']['fields']['sales_persona_c']['inline_edit']='1';
$dictionary['STP_Sales_Target']['fields']['sales_persona_c']['labelValue']='Sales Persona';

 

 // created: 2021-08-15 17:21:06
$dictionary['STP_Sales_Target']['fields']['sales_target_c']['inline_edit']='1';
$dictionary['STP_Sales_Target']['fields']['sales_target_c']['labelValue']='Sales Target';

 

 // created: 2021-08-15 17:22:07
$dictionary['STP_Sales_Target']['fields']['target_end_date_c']['inline_edit']='1';
$dictionary['STP_Sales_Target']['fields']['target_end_date_c']['labelValue']='Target End Date';

 

 // created: 2021-08-15 17:18:51
$dictionary['STP_Sales_Target']['fields']['target_year_c']['inline_edit']='1';
$dictionary['STP_Sales_Target']['fields']['target_year_c']['options']='numeric_range_search_dom';
$dictionary['STP_Sales_Target']['fields']['target_year_c']['labelValue']='Target Year';
$dictionary['STP_Sales_Target']['fields']['target_year_c']['enable_range_search']='1';

 

 // created: 2022-10-05 07:25:57
$dictionary['STP_Sales_Target']['fields']['target_year_to_c']['inline_edit']='1';
$dictionary['STP_Sales_Target']['fields']['target_year_to_c']['labelValue']='Target Year (To)';

 

 // created: 2021-08-15 17:18:16
$dictionary['STP_Sales_Target']['fields']['user_id_c']['inline_edit']=1;

 
?>