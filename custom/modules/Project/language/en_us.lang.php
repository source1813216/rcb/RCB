<?php
// created: 2021-08-05 08:52:18
$mod_strings = array (
  'LBL_ACCOUNTS' => 'Clients',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Clients',
  'LBL_AOS_QUOTES_PROJECT' => 'Initial Event Approval: Project',
  'LBL_CASES' => 'Case',
  'LBL_CASES_SUBPANEL_TITLE' => 'Cases',
  'LBL_VENUE_VNP_VENUES_ID' => '&#039;Venue&#039; (related &#039;&#039; ID)',
  'LBL_VENUE' => 'Venue',
  'LBL_PROJECT_TASKS_SUBPANEL_TITLE' => 'Project Tasks',
);