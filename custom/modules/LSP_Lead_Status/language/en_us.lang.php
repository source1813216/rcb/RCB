<?php
// created: 2021-08-02 12:24:24
$mod_strings = array (
  'LBL_DESCRIPTION' => 'Remarks',
  'LBL_STATUS' => 'Status',
  'LBL_VALID_STATUS' => 'Valid Status Record',
  'LBL_UPDATE_STATUS' => 'Update Status',
  'LBL_LEAD_PROGRESS' => 'Lead Progress',
);