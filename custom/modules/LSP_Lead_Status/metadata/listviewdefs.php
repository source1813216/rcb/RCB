<?php
$module_name = 'LSP_Lead_Status';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'LEADS_LSP_LEAD_STATUS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_LEADS_LSP_LEAD_STATUS_1_FROM_LEADS_TITLE',
    'id' => 'LEADS_LSP_LEAD_STATUS_1LEADS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'LEAD_PROGRESS_C' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_LEAD_PROGRESS',
    'width' => '10%',
  ),
  'STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10%',
  ),
  'VALID_STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_VALID_STATUS',
    'width' => '10%',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
);
;
?>
