<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-07-15 11:19:53
$dictionary["LSP_Lead_Status"]["fields"]["leads_lsp_lead_status_1"] = array (
  'name' => 'leads_lsp_lead_status_1',
  'type' => 'link',
  'relationship' => 'leads_lsp_lead_status_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_LSP_LEAD_STATUS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_lsp_lead_status_1leads_ida',
);
$dictionary["LSP_Lead_Status"]["fields"]["leads_lsp_lead_status_1_name"] = array (
  'name' => 'leads_lsp_lead_status_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_LSP_LEAD_STATUS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_lsp_lead_status_1leads_ida',
  'link' => 'leads_lsp_lead_status_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["LSP_Lead_Status"]["fields"]["leads_lsp_lead_status_1leads_ida"] = array (
  'name' => 'leads_lsp_lead_status_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_lsp_lead_status_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_LSP_LEAD_STATUS_1_FROM_LSP_LEAD_STATUS_TITLE',
);


 // created: 2021-08-02 12:24:24
$dictionary['LSP_Lead_Status']['fields']['lead_progress_c']['inline_edit']='1';
$dictionary['LSP_Lead_Status']['fields']['lead_progress_c']['labelValue']='Lead Progress';

 

 // created: 2021-07-15 11:29:43
$dictionary['LSP_Lead_Status']['fields']['status_c']['inline_edit']='1';
$dictionary['LSP_Lead_Status']['fields']['status_c']['labelValue']='Status';

 

 // created: 2021-07-29 20:19:30
$dictionary['LSP_Lead_Status']['fields']['update_status_c']['inline_edit']='1';
$dictionary['LSP_Lead_Status']['fields']['update_status_c']['labelValue']='Update Status';

 

 // created: 2021-07-29 15:25:09
$dictionary['LSP_Lead_Status']['fields']['valid_status_c']['inline_edit']='1';
$dictionary['LSP_Lead_Status']['fields']['valid_status_c']['labelValue']='Valid Status Record';

 
?>