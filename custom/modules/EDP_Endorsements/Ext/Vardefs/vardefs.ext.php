<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-06-23 10:34:25
$dictionary["EDP_Endorsements"]["fields"]["edp_endorsements_documents_1"] = array (
  'name' => 'edp_endorsements_documents_1',
  'type' => 'link',
  'relationship' => 'edp_endorsements_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_EDP_ENDORSEMENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2021-06-15 16:46:24
$dictionary["EDP_Endorsements"]["fields"]["leads_edp_endorsements_1"] = array (
  'name' => 'leads_edp_endorsements_1',
  'type' => 'link',
  'relationship' => 'leads_edp_endorsements_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_edp_endorsements_1leads_ida',
);
$dictionary["EDP_Endorsements"]["fields"]["leads_edp_endorsements_1_name"] = array (
  'name' => 'leads_edp_endorsements_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_edp_endorsements_1leads_ida',
  'link' => 'leads_edp_endorsements_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["EDP_Endorsements"]["fields"]["leads_edp_endorsements_1leads_ida"] = array (
  'name' => 'leads_edp_endorsements_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_edp_endorsements_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_EDP_ENDORSEMENTS_TITLE',
);


 // created: 2021-06-14 16:09:11
$dictionary['EDP_Endorsements']['fields']['account_id_c']['inline_edit']=1;

 

 // created: 2021-06-14 16:04:15
$dictionary['EDP_Endorsements']['fields']['approval_routine_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['approval_routine_c']['labelValue']='Approval Routine';

 

 // created: 2021-06-16 08:34:21
$dictionary['EDP_Endorsements']['fields']['approval_stamp_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['approval_stamp_c']['labelValue']='Approval Stamp';

 

 // created: 2021-06-14 16:05:11
$dictionary['EDP_Endorsements']['fields']['approval_status_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['approval_status_c']['labelValue']='Approval Status';

 

 // created: 2021-06-14 17:55:01
$dictionary['EDP_Endorsements']['fields']['ceo_approval_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ceo_approval_c']['labelValue']='CEO Approve/Reject';

 

 // created: 2021-06-14 16:05:47
$dictionary['EDP_Endorsements']['fields']['ceo_apv_by_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ceo_apv_by_c']['labelValue']='CEO apv by';

 

 // created: 2021-06-14 16:07:13
$dictionary['EDP_Endorsements']['fields']['ceo_flag_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ceo_flag_c']['labelValue']='CEO Flag';

 

 // created: 2021-06-16 05:13:14
$dictionary['EDP_Endorsements']['fields']['ceo_label_c']['inline_edit']='';
$dictionary['EDP_Endorsements']['fields']['ceo_label_c']['labelValue']='3';

 

 // created: 2021-06-14 16:07:26
$dictionary['EDP_Endorsements']['fields']['ceo_remarks_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ceo_remarks_c']['labelValue']='CEO Remarks';

 

 // created: 2021-06-14 16:07:51
$dictionary['EDP_Endorsements']['fields']['ceo_response_date_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ceo_response_date_c']['labelValue']='CEO Response Date';

 

 // created: 2021-06-14 16:08:09
$dictionary['EDP_Endorsements']['fields']['ceo_stamp_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ceo_stamp_c']['labelValue']='CEO Stamp';

 

 // created: 2021-06-14 16:08:26
$dictionary['EDP_Endorsements']['fields']['clarification_done_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['clarification_done_c']['labelValue']='Clarification Done';

 

 // created: 2021-06-14 16:09:11
$dictionary['EDP_Endorsements']['fields']['client_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['client_c']['labelValue']='Client';

 

 // created: 2021-06-14 16:09:25
$dictionary['EDP_Endorsements']['fields']['contact_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['contact_c']['labelValue']='Contact';

 

 // created: 2021-06-14 16:09:25
$dictionary['EDP_Endorsements']['fields']['contact_id_c']['inline_edit']=1;

 

 // created: 2021-06-14 16:14:35
$dictionary['EDP_Endorsements']['fields']['dceo_approval_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['dceo_approval_c']['labelValue']='Deputy CEO Approve/Reject';

 

 // created: 2021-06-14 16:14:03
$dictionary['EDP_Endorsements']['fields']['dceo_apv_by_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['dceo_apv_by_c']['labelValue']='Deputy CEO  Approval By';

 

 // created: 2021-06-14 16:09:51
$dictionary['EDP_Endorsements']['fields']['dceo_flag_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['dceo_flag_c']['labelValue']='DCEO Flag';

 

 // created: 2021-06-16 05:12:32
$dictionary['EDP_Endorsements']['fields']['dceo_label_c']['inline_edit']='';
$dictionary['EDP_Endorsements']['fields']['dceo_label_c']['labelValue']='2';

 

 // created: 2021-06-14 16:15:01
$dictionary['EDP_Endorsements']['fields']['dceo_remarks_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['dceo_remarks_c']['labelValue']='Deputy CEO Remarks';

 

 // created: 2021-06-14 16:16:06
$dictionary['EDP_Endorsements']['fields']['dceo_response_date_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['dceo_response_date_c']['labelValue']='Deputy CEO Response Date';

 

 // created: 2021-06-14 16:10:14
$dictionary['EDP_Endorsements']['fields']['dceo_stamp_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['dceo_stamp_c']['labelValue']='DCEO Stamp';

 

 // created: 2021-06-14 17:56:30
$dictionary['EDP_Endorsements']['fields']['ddm_approval_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ddm_approval_c']['labelValue']='DDM Approve/Reject';

 

 // created: 2021-06-14 16:11:44
$dictionary['EDP_Endorsements']['fields']['ddm_approved_by_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ddm_approved_by_c']['labelValue']='DDM Approved By';

 

 // created: 2021-06-14 16:12:09
$dictionary['EDP_Endorsements']['fields']['ddm_flag_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ddm_flag_c']['labelValue']='DDM Flag';

 

 // created: 2021-06-16 05:12:50
$dictionary['EDP_Endorsements']['fields']['ddm_label_c']['inline_edit']='';
$dictionary['EDP_Endorsements']['fields']['ddm_label_c']['labelValue']='1';

 

 // created: 2021-06-14 16:12:32
$dictionary['EDP_Endorsements']['fields']['ddm_remarks_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ddm_remarks_c']['labelValue']='DDM Remarks';

 

 // created: 2021-06-14 16:12:54
$dictionary['EDP_Endorsements']['fields']['ddm_response_date_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ddm_response_date_c']['labelValue']='DDM Response Date';

 

 // created: 2021-06-14 16:13:10
$dictionary['EDP_Endorsements']['fields']['ddm_stamp_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['ddm_stamp_c']['labelValue']='DDM Stamp';

 

 // created: 2021-06-15 19:05:32
$dictionary['EDP_Endorsements']['fields']['endorsement_end_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['endorsement_end_c']['labelValue']='Endorsement End Date';

 

 // created: 2021-06-15 19:05:13
$dictionary['EDP_Endorsements']['fields']['endorsement_start_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['endorsement_start_c']['labelValue']='Endorsement Start Date';

 

 // created: 2021-06-14 16:21:51
$dictionary['EDP_Endorsements']['fields']['event_brief_c']['labelValue']='Event Brief';

 

 // created: 2021-06-14 16:22:06
$dictionary['EDP_Endorsements']['fields']['event_title_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['event_title_c']['labelValue']='Event Title';

 

 // created: 2021-06-14 16:22:32
$dictionary['EDP_Endorsements']['fields']['line_institute_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['line_institute_c']['labelValue']='Line Institute';

 

 // created: 2021-06-14 16:22:32
$dictionary['EDP_Endorsements']['fields']['lip_line_institutes_id_c']['inline_edit']=1;

 

 // created: 2021-06-14 16:25:41
$dictionary['EDP_Endorsements']['fields']['prepared_by_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['prepared_by_c']['labelValue']='Prepared By';

 

 // created: 2021-06-14 16:25:59
$dictionary['EDP_Endorsements']['fields']['reference_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['reference_c']['labelValue']='Reference';

 

 // created: 2021-06-14 16:26:11
$dictionary['EDP_Endorsements']['fields']['response_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['response_c']['labelValue']='Response';

 

 // created: 2021-06-14 16:26:34
$dictionary['EDP_Endorsements']['fields']['send_for_approval_c']['inline_edit']='1';
$dictionary['EDP_Endorsements']['fields']['send_for_approval_c']['labelValue']='Send For Approval';

 

 // created: 2021-06-14 16:04:15
$dictionary['EDP_Endorsements']['fields']['srp_stdapproval_routine_id_c']['inline_edit']=1;

 

 // created: 2021-06-14 16:08:08
$dictionary['EDP_Endorsements']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2021-06-14 16:10:14
$dictionary['EDP_Endorsements']['fields']['user_id2_c']['inline_edit']=1;

 

 // created: 2021-06-14 16:11:44
$dictionary['EDP_Endorsements']['fields']['user_id3_c']['inline_edit']=1;

 

 // created: 2021-06-14 16:13:10
$dictionary['EDP_Endorsements']['fields']['user_id4_c']['inline_edit']=1;

 

 // created: 2021-06-14 16:14:03
$dictionary['EDP_Endorsements']['fields']['user_id5_c']['inline_edit']=1;

 

 // created: 2021-06-14 16:25:41
$dictionary['EDP_Endorsements']['fields']['user_id6_c']['inline_edit']=1;

 

 // created: 2021-06-14 16:05:47
$dictionary['EDP_Endorsements']['fields']['user_id_c']['inline_edit']=1;

 
?>