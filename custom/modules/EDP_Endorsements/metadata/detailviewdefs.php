<?php
$module_name = 'EDP_Endorsements';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
         // 1 => 'DUPLICATE',
          2 => 'DELETE',
          //3 => 'FIND_DUPLICATES',
		  4 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'pdf\');" value="{$MOD.LBL_PRINT_AS_PDF}">',
          ),
          5 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'emailpdf\');" value="{$MOD.LBL_EMAIL_PDF}">',
          ),
        ),
      ),
      'maxColumns' => '2',
	  'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/EDP_Endorsements/js/techinc-detail.js',
        ),
      ),
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_DETAILVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_detailview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'approval_status_c',
            'studio' => 'visible',
            'label' => 'LBL_APPROVAL_STATUS',
          ),
          1 => 
          array (
            'name' => 'prepared_by_c',
            'studio' => 'visible',
            'label' => 'LBL_PREPARED_BY',
          ),
        ),
        1 => 
        array (
          0 => '',
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'ddm_approved_by_c',
            'studio' => 'visible',
            'label' => 'LBL_DDM_APPROVED_BY',
          ),
          1 => 
          array (
            'name' => 'ddm_response_date_c',
            'label' => 'LBL_DDM_RESPONSE_DATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'dceo_apv_by_c',
            'studio' => 'visible',
            'label' => 'LBL_DCEO_APV_BY',
          ),
          1 => 
          array (
            'name' => 'dceo_response_date_c',
            'label' => 'LBL_DCEO_RESPONSE_DATE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'ceo_apv_by_c',
            'studio' => 'visible',
            'label' => 'LBL_CEO_APV_BY',
          ),
          1 => 
          array (
            'name' => 'ceo_response_date_c',
            'label' => 'LBL_CEO_RESPONSE_DATE',
          ),
        ),
      ),
      'lbl_detailview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'leads_edp_endorsements_1_name',
            'label' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_LEADS_TITLE',
          ),
          1 => 
          array (
            'name' => 'client_c',
            'studio' => 'visible',
            'label' => 'LBL_CLIENT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'contact_c',
            'studio' => 'visible',
            'label' => 'LBL_CONTACT',
          ),
          1 => 
          array (
            'name' => 'line_institute_c',
            'studio' => 'visible',
            'label' => 'LBL_LINE_INSTITUTE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'event_brief_c',
            'label' => 'LBL_EVENT_BRIEF',
          ),
        ),
      ),
      'lbl_detailview_panel3' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
            'label' => 'LBL_DATE_ENTERED',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
      ),
    ),
  ),
);
;
?>
