<?php
// created: 2021-06-16 06:56:40
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'eoi_date_c' => 
  array (
    'type' => 'date',
    'default' => true,
    'vname' => 'LBL_EOI_DATE',
    'width' => '10%',
  ),
  'eoi_title_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_EOI_TITLE',
    'width' => '10%',
  ),
  'approval_status_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_APPROVAL_STATUS',
    'width' => '10%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '45%',
    'default' => true,
  ),
);