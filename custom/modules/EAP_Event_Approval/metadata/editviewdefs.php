<?php
$module_name = 'EAP_Event_Approval';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/EAP_Event_Approval/js/techinc-edit.js',
        ),
      ),
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'line_institute_c',
            'studio' => 'visible',
            'label' => 'LBL_LINE_INSTITUTE',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'event_brief_c',
            'label' => 'LBL_EVENT_BRIEF',
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'eoi_title_c',
            'label' => 'LBL_EOI_TITLE',
          ),
          1 => 
          array (
            'name' => 'eoi_date_c',
            'label' => 'LBL_EOI_DATE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'reference_c',
            'label' => 'LBL_REFERENCE',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'eoi_details_c',
            'label' => 'LBL_EOI_DETAILS',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'send_for_approval_c',
            'label' => 'LBL_SEND_FOR_APPROVAL',
          ),
          1 => 
          array (
            'name' => 'approval_status_c',
            'studio' => 'visible',
            'label' => 'LBL_APPROVAL_STATUS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'clarification_done_c',
            'label' => 'LBL_CLARIFICATION_DONE',
          ),
          1 => 
          array (
            'name' => 'response_c',
            'studio' => 'visible',
            'label' => 'LBL_RESPONSE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'ddm_label_c',
            'studio' => 'visible',
            'label' => 'LBL_DDM_LABEL',
          ),
          1 => 
          array (
            'name' => 'ddm_approval_c',
            'studio' => 'visible',
            'label' => 'LBL_DDM_APPROVAL',
          ),
        ),
        3 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'ddm_remarks_c',
            'studio' => 'visible',
            'label' => 'LBL_DDM_REMARKS',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'dceo_label_c',
            'studio' => 'visible',
            'label' => 'LBL_DCEO_LABEL',
          ),
          1 => 
          array (
            'name' => 'dceo_approval_c',
            'studio' => 'visible',
            'label' => 'LBL_DCEO_APPROVAL',
          ),
        ),
        5 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'dceo_remarks_c',
            'studio' => 'visible',
            'label' => 'LBL_DCEO_REMARKS',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'ceo_label_c',
            'studio' => 'visible',
            'label' => 'LBL_CEO_LABEL',
          ),
          1 => 
          array (
            'name' => 'ceo_approval_c',
            'studio' => 'visible',
            'label' => 'LBL_CEO_APPROVAL',
          ),
        ),
        7 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'ceo_remarks_c',
            'studio' => 'visible',
            'label' => 'LBL_CEO_REMARKS',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
