<?php
// created: 2021-06-10 08:53:52
$mod_strings = array (
  'LBL_PRINT_AS_PDF' => 'Print PDF',
  'LBL_EMAIL_PDF' => 'E-Mail PDF',
  'LBL_PDF_NAME'=>'EOI',
  'LBL_NO_TEMPLATE' => 'ERROR\\nNo templates found. If you have not created a Event Approval template, go to the PDF templates module and create one',
  'LBL_NAME' => 'EOI',
  'LBL_EVENT_BRIEF' => 'Event Brief',
  'LNK_NEW_RECORD' => 'Create Expression of Interest',
  'LNK_LIST' => 'View Expression of Interest',
  'LNK_IMPORT_EAP_EVENT_APPROVAL' => 'Import Expression of Interest',
  'LBL_LIST_FORM_TITLE' => 'Expression of Interest List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Expression of Interest',
  'LBL_HOMEPAGE_TITLE' => 'My Expression of Interest',
  'LBL_EOI_TITLE' => 'EOI Title',
  'LBL_APPROVAL_STATUS' => 'Approval Status',
  'LBL_EVENT_TITLE' => 'Event Title',
  'LBL_EVENT_ABBREVIATION' => 'Event Abbreviation',
  'LBL_DDM_STAMP_USER_ID' => '&#039;DDM Stamp&#039; (related &#039;User&#039; ID)',
  'LBL_DDM_STAMP' => 'DDM Stamp',
  'LBL_DDM_APPROVER_USER_ID' => '&#039;DDM Approved By&#039; (related &#039;User&#039; ID)',
  'LBL_DDM_APPROVER' => 'DDM Approved By',
  'LBL_DDM_RESPONSE' => 'DDM Response Date',
  'LBL_DDM_REMARKS' => 'DDM Remarks',
  'LBL_DCEO_APV_BY_USER_ID' => '&#039;Deputy CEO Approval By&#039; (related &#039;User&#039; ID)',
  'LBL_DCEO_APV_BY' => 'Deputy CEO Approval By',
  'LBL_DCEO_STAMP_USER_ID' => '&#039;DCEO Stamp&#039; (related &#039;User&#039; ID)',
  'LBL_DCEO_STAMP' => 'DCEO Stamp',
  'LBL_DDM_APPROVAL' => 'DDM Approve/Reject',
  'LBL_DCEO_RESPONSE' => 'Deputy CEO Response Date',
  'LBL_DCEO_APPROVAL' => 'Deputy CEO Approve/Reject',
  'LBL_DCEO_REMARKS' => 'Deputy CEO Remarks',
  'LBL_CEO_APV_BY_USER_ID' => '&#039;CEO Approval By&#039; (related &#039;User&#039; ID)',
  'LBL_CEO_APV_BY' => 'CEO Approval By',
  'LBL_CEO_STAMP_USER_ID' => '&#039;CEO Stamp&#039; (related &#039;User&#039; ID)',
  'LBL_CEO_STAMP' => 'CEO Stamp',
  'LBL_CEO_APPROVAL' => 'CEO Approve/Reject',
  'LBL_CEO_RESPONSE' => 'CEO Response Date',
  'LBL_CEO_REMARKS' => 'CEO Remarks',
  'LBL_EOI_APPROVAL_START' => 'EOI Approval Start Date',
  'LBL_EOI_APPROVAL_END' => 'EOI Approval End Date',
  'LBL_EOI_DATE' => 'EOI Date',
  'LBL_REFERENCE' => 'Reference',
  'LBL_EOI_DETAILS' => 'EOI Details',
  'LBL_EDITVIEW_PANEL1' => 'Record Info',
  'LBL_EDITVIEW_PANEL2' => 'Event Brief',
  'LBL_EDITVIEW_PANEL3' => 'Approval',
  'LBL_SEND_FOR_APPROVAL' => 'Send for Approval',
  'LBL_CLARIFICATION_DONE' => 'Clarification Done',
  'LBL_RESPONSE' => 'Response',
  'LBL_DDM_LABEL' => '1',
  'LBL_DCEO_LABEL' => '2',
  'LBL_CEO_LABEL' => '3',
  'LBL_EDITVIEW_PANEL4' => 'EOI Writeup',
  'LBL_CLIENT_ACCOUNT_ID' => '&#039;Client&#039; (related &#039;Client&#039; ID)',
  'LBL_CLIENT' => 'Client',
  'LBL_CONTACT_CONTACT_ID' => '&#039;Contact&#039; (related &#039;Contact&#039; ID)',
  'LBL_CONTACT' => 'Contact',
  'LBL_DETAILVIEW_PANEL1' => 'Record Infor',
  'LBL_DETAILVIEW_PANEL2' => 'Approval Process',
  'LBL_DETAILVIEW_PANEL3' => 'New Panel 3',
  'LBL_DETAILVIEW_PANEL4' => 'EOI Details',
  'LBL_DETAILVIEW_PANEL5' => 'Event Brief',
  'LBL_LINE_INSTITUTE_LIP_LINE_INSTITUTES_ID' => '&#039;Line Institute&#039; (related &#039;&#039; ID)',
  'LBL_LINE_INSTITUTE' => 'Line Institute',
  'LBL_PREPARED_BY_USER_ID' => '&#039;Prepared By&#039; (related &#039;User&#039; ID)',
  'LBL_PREPARED_BY' => 'Prepared By',
  'LBL_APPROVAL_ROUTINE_SRP_STDAPPROVAL_ROUTINE_ID' => '&#039;Approval Routine&#039; (related &#039;&#039; ID)',
  'LBL_APPROVAL_ROUTINE' => 'Approval Routine',
  'LBL_DDM_FLG' => 'DDM Flg',
  'LBL_DCEO_FLG' => 'DCEO Flg',
  'LBL_CEO_FLG' => 'CEO Flg',
  'LBL_APPROVAL_STAMP' => 'Approval Stamp',
);