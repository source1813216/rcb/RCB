<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-06-07 14:48:34
$dictionary["EAP_Event_Approval"]["fields"]["leads_eap_event_approval_1"] = array (
  'name' => 'leads_eap_event_approval_1',
  'type' => 'link',
  'relationship' => 'leads_eap_event_approval_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_EAP_EVENT_APPROVAL_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_eap_event_approval_1leads_ida',
);
$dictionary["EAP_Event_Approval"]["fields"]["leads_eap_event_approval_1_name"] = array (
  'name' => 'leads_eap_event_approval_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_EAP_EVENT_APPROVAL_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_eap_event_approval_1leads_ida',
  'link' => 'leads_eap_event_approval_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["EAP_Event_Approval"]["fields"]["leads_eap_event_approval_1leads_ida"] = array (
  'name' => 'leads_eap_event_approval_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_eap_event_approval_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_EAP_EVENT_APPROVAL_1_FROM_EAP_EVENT_APPROVAL_TITLE',
);


 // created: 2021-06-07 14:05:05
$dictionary['EAP_Event_Approval']['fields']['account_id_c']['inline_edit']=1;

 

 // created: 2021-06-07 19:35:41
$dictionary['EAP_Event_Approval']['fields']['approval_routine_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['approval_routine_c']['labelValue']='Approval Routine';

 

 // created: 2021-06-10 08:53:52
$dictionary['EAP_Event_Approval']['fields']['approval_stamp_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['approval_stamp_c']['labelValue']='Approval Stamp';

 

 // created: 2021-08-23 10:03:16
$dictionary['EAP_Event_Approval']['fields']['approval_status_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['approval_status_c']['labelValue']='Approval Status';

 

 // created: 2021-06-07 12:48:15
$dictionary['EAP_Event_Approval']['fields']['ceo_approval_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ceo_approval_c']['labelValue']='CEO Approve/Reject';

 

 // created: 2021-06-07 12:47:19
$dictionary['EAP_Event_Approval']['fields']['ceo_apv_by_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ceo_apv_by_c']['labelValue']='CEO Approval By';

 

 // created: 2021-06-08 08:23:32
$dictionary['EAP_Event_Approval']['fields']['ceo_flg_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ceo_flg_c']['labelValue']='CEO Flg';

 

 // created: 2021-06-07 13:21:06
$dictionary['EAP_Event_Approval']['fields']['ceo_label_c']['inline_edit']='';
$dictionary['EAP_Event_Approval']['fields']['ceo_label_c']['labelValue']='3';

 

 // created: 2021-06-07 12:49:34
$dictionary['EAP_Event_Approval']['fields']['ceo_remarks_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ceo_remarks_c']['labelValue']='CEO Remarks';

 

 // created: 2021-06-07 12:48:57
$dictionary['EAP_Event_Approval']['fields']['ceo_response_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ceo_response_c']['labelValue']='CEO Response Date';

 

 // created: 2021-06-07 12:47:46
$dictionary['EAP_Event_Approval']['fields']['ceo_stamp_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ceo_stamp_c']['labelValue']='CEO Stamp';

 

 // created: 2021-06-07 13:13:42
$dictionary['EAP_Event_Approval']['fields']['clarification_done_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['clarification_done_c']['labelValue']='Clarification Done';

 

 // created: 2021-06-07 14:05:05
$dictionary['EAP_Event_Approval']['fields']['client_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['client_c']['labelValue']='Client';

 

 // created: 2021-06-07 14:05:32
$dictionary['EAP_Event_Approval']['fields']['contact_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['contact_c']['labelValue']='Contact';

 

 // created: 2021-06-07 14:05:32
$dictionary['EAP_Event_Approval']['fields']['contact_id_c']['inline_edit']=1;

 

 // created: 2021-06-07 12:43:48
$dictionary['EAP_Event_Approval']['fields']['dceo_approval_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['dceo_approval_c']['labelValue']='Deputy CEO Approve/Reject';

 

 // created: 2021-06-07 12:39:35
$dictionary['EAP_Event_Approval']['fields']['dceo_apv_by_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['dceo_apv_by_c']['labelValue']='Deputy CEO Approval By';

 

 // created: 2021-06-08 08:23:19
$dictionary['EAP_Event_Approval']['fields']['dceo_flg_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['dceo_flg_c']['labelValue']='DCEO Flg';

 

 // created: 2021-06-07 13:20:18
$dictionary['EAP_Event_Approval']['fields']['dceo_label_c']['inline_edit']='';
$dictionary['EAP_Event_Approval']['fields']['dceo_label_c']['labelValue']='2';

 

 // created: 2021-06-07 12:44:12
$dictionary['EAP_Event_Approval']['fields']['dceo_remarks_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['dceo_remarks_c']['labelValue']='Deputy CEO Remarks';

 

 // created: 2021-06-07 12:42:51
$dictionary['EAP_Event_Approval']['fields']['dceo_response_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['dceo_response_c']['labelValue']='Deputy CEO Response Date';

 

 // created: 2021-06-07 12:40:45
$dictionary['EAP_Event_Approval']['fields']['dceo_stamp_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['dceo_stamp_c']['labelValue']='DCEO Stamp';

 

 // created: 2021-06-07 12:41:42
$dictionary['EAP_Event_Approval']['fields']['ddm_approval_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ddm_approval_c']['labelValue']='DDM Approve/Reject';

 

 // created: 2021-06-06 21:47:52
$dictionary['EAP_Event_Approval']['fields']['ddm_approver_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ddm_approver_c']['labelValue']='DDM Approved By';

 

 // created: 2021-06-08 08:23:03
$dictionary['EAP_Event_Approval']['fields']['ddm_flg_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ddm_flg_c']['labelValue']='DDM Flg';

 

 // created: 2021-06-07 13:19:35
$dictionary['EAP_Event_Approval']['fields']['ddm_label_c']['inline_edit']='';
$dictionary['EAP_Event_Approval']['fields']['ddm_label_c']['labelValue']='1';

 

 // created: 2021-06-07 12:38:20
$dictionary['EAP_Event_Approval']['fields']['ddm_remarks_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ddm_remarks_c']['labelValue']='DDM Remarks';

 

 // created: 2021-06-06 21:51:56
$dictionary['EAP_Event_Approval']['fields']['ddm_response_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ddm_response_c']['labelValue']='DDM Response Date';

 

 // created: 2021-06-06 21:47:29
$dictionary['EAP_Event_Approval']['fields']['ddm_stamp_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['ddm_stamp_c']['labelValue']='DDM Stamp';

 

 // created: 2021-06-07 12:50:37
$dictionary['EAP_Event_Approval']['fields']['eoi_approval_end_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['eoi_approval_end_c']['labelValue']='EOI Approval End Date';

 

 // created: 2021-06-07 12:50:21
$dictionary['EAP_Event_Approval']['fields']['eoi_approval_start_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['eoi_approval_start_c']['labelValue']='EOI Approval Start Date';

 

 // created: 2021-06-07 13:07:20
$dictionary['EAP_Event_Approval']['fields']['eoi_date_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['eoi_date_c']['labelValue']='EOI Date';

 

 // created: 2021-06-07 13:07:59
$dictionary['EAP_Event_Approval']['fields']['eoi_details_c']['labelValue']='EOI Details';

 

 // created: 2021-06-06 18:30:25
$dictionary['EAP_Event_Approval']['fields']['eoi_title_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['eoi_title_c']['labelValue']='EOI Title';

 

 // created: 2021-06-06 19:09:13
$dictionary['EAP_Event_Approval']['fields']['event_abbreviation_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['event_abbreviation_c']['labelValue']='Event Abbreviation';

 

 // created: 2021-05-26 09:30:09
$dictionary['EAP_Event_Approval']['fields']['event_brief_c']['labelValue']='Event Brief';

 

 // created: 2021-06-06 19:08:33
$dictionary['EAP_Event_Approval']['fields']['event_title_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['event_title_c']['labelValue']='Event Title';

 

 // created: 2021-06-07 14:38:34
$dictionary['EAP_Event_Approval']['fields']['line_institute_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['line_institute_c']['labelValue']='Line Institute';

 

 // created: 2021-06-07 14:38:34
$dictionary['EAP_Event_Approval']['fields']['lip_line_institutes_id_c']['inline_edit']=1;

 

 // created: 2021-06-06 18:30:07
$dictionary['EAP_Event_Approval']['fields']['name']['inline_edit']=true;
$dictionary['EAP_Event_Approval']['fields']['name']['duplicate_merge']='disabled';
$dictionary['EAP_Event_Approval']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['EAP_Event_Approval']['fields']['name']['merge_filter']='disabled';
$dictionary['EAP_Event_Approval']['fields']['name']['unified_search']=false;

 

 // created: 2021-06-07 14:44:07
$dictionary['EAP_Event_Approval']['fields']['prepared_by_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['prepared_by_c']['labelValue']='Prepared By';

 

 // created: 2021-10-09 11:30:20
$dictionary['EAP_Event_Approval']['fields']['reference_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['reference_c']['labelValue']='Reference';

 

 // created: 2021-06-07 13:14:18
$dictionary['EAP_Event_Approval']['fields']['response_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['response_c']['labelValue']='Response';

 

 // created: 2021-06-07 13:13:14
$dictionary['EAP_Event_Approval']['fields']['send_for_approval_c']['inline_edit']='1';
$dictionary['EAP_Event_Approval']['fields']['send_for_approval_c']['labelValue']='Send for Approval';

 

 // created: 2021-06-07 19:35:41
$dictionary['EAP_Event_Approval']['fields']['srp_stdapproval_routine_id_c']['inline_edit']=1;

 

 // created: 2021-06-06 21:47:52
$dictionary['EAP_Event_Approval']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2021-06-07 12:39:35
$dictionary['EAP_Event_Approval']['fields']['user_id2_c']['inline_edit']=1;

 

 // created: 2021-06-07 12:40:45
$dictionary['EAP_Event_Approval']['fields']['user_id3_c']['inline_edit']=1;

 

 // created: 2021-06-07 12:47:19
$dictionary['EAP_Event_Approval']['fields']['user_id4_c']['inline_edit']=1;

 

 // created: 2021-06-07 12:47:46
$dictionary['EAP_Event_Approval']['fields']['user_id5_c']['inline_edit']=1;

 

 // created: 2021-06-07 14:44:07
$dictionary['EAP_Event_Approval']['fields']['user_id6_c']['inline_edit']=1;

 

 // created: 2021-06-06 21:47:29
$dictionary['EAP_Event_Approval']['fields']['user_id_c']['inline_edit']=1;

 
?>