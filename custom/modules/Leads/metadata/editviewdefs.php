<?php
$viewdefs ['Leads'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'hidden' => 
        array (
          0 => '<input type="hidden" name="prospect_id" value="{if isset($smarty.request.prospect_id)}{$smarty.request.prospect_id}{else}{$bean->prospect_id}{/if}">',
          1 => '<input type="hidden" name="account_id" value="{if isset($smarty.request.account_id)}{$smarty.request.account_id}{else}{$bean->account_id}{/if}">',
          2 => '<input type="hidden" name="contact_id" value="{if isset($smarty.request.contact_id)}{$smarty.request.contact_id}{else}{$bean->contact_id}{/if}">',
          3 => '<input type="hidden" name="opportunity_id" value="{if isset($smarty.request.opportunity_id)}{$smarty.request.opportunity_id}{else}{$bean->opportunity_id}{/if}">',
        ),
        'buttons' => 
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/Leads/js/techinc-edit.js',
        ),
      ),
      'javascript' => '<script type="text/javascript" language="Javascript">function copyAddressRight(form)  {ldelim} form.alt_address_street.value = form.primary_address_street.value;form.alt_address_city.value = form.primary_address_city.value;form.alt_address_state.value = form.primary_address_state.value;form.alt_address_postalcode.value = form.primary_address_postalcode.value;form.alt_address_country.value = form.primary_address_country.value;return true; {rdelim} function copyAddressLeft(form)  {ldelim} form.primary_address_street.value =form.alt_address_street.value;form.primary_address_city.value = form.alt_address_city.value;form.primary_address_state.value = form.alt_address_state.value;form.primary_address_postalcode.value =form.alt_address_postalcode.value;form.primary_address_country.value = form.alt_address_country.value;return true; {rdelim} </script>',
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL7' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'account_c',
            'studio' => 'visible',
            'label' => 'LBL_ACCOUNT',
          ),
          1 => 
          array (
            'name' => 'segment_c',
            'studio' => 'visible',
            'label' => 'LBL_SEGMENT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'event_title_c',
            'label' => 'LBL_EVENT_TITLE',
          ),
          1 => 
          array (
            'name' => 'primary_contact_c',
            'studio' => 'visible',
            'label' => 'LBL_PRIMARY_CONTACT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'event_abbreviation_c',
            'label' => 'LBL_EVENT_ABBREVIATION',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'event_date_c',
            'label' => 'LBL_EVENT_DATE',
          ),
          1 => 
          array (
            'name' => 'event_end_date_c',
            'label' => 'LBL_EVENT_END_DATE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'packs_c',
            'label' => 'LBL_PACKS',
          ),
          1 => 
          array (
            'name' => 'event_days_c',
            'label' => 'LBL_EVENT_DAYS',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'auto_calculate_c',
            'label' => 'LBL_AUTO_CALCULATE',
          ),
          1 => 
          array (
            'name' => 'currency_id',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCY',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'expected_revenue_c',
            'label' => 'LBL_EXPECTED_REVENUE',
          ),
          1 => 
          array (
            'name' => 'gor_commitment_c',
            'label' => 'LBL_GOR_COMMITMENT',
          ),
        ),
        7 => 
        array (
          0 => '',
          1 => '',
        ),
        8 => 
        array (
          0 => 'lead_source',
          1 => 
          array (
            'name' => 'source_details_c',
            'studio' => 'visible',
            'label' => 'LBL_SOURCE_DETAILS',
          ),
        ),
        9 => 
        array (
          0 => '',
          1 => '',
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'probability_c',
            'studio' => 'visible',
            'label' => 'LBL_PROBABILITY ',
          ),
          1 => 
          array (
            'name' => 'line_institute_c',
            'studio' => 'visible',
            'label' => 'LBL_LINE_INSTITUTE',
          ),
        ),
        11 => 
        array (
          0 => '',
          1 => '',
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'event_coordinator_c',
            'studio' => 'visible',
            'label' => 'LBL_EVENT_COORDINATOR',
          ),
          1 => 
          array (
            'name' => 'stp_sales_target_leads_1_name',
            'label' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_STP_SALES_TARGET_TITLE',
          ),
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'event_brief_c',
            'label' => 'LBL_EVENT_BRIEF',
          ),
        ),
      ),
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'start_engagement_c',
            'studio' => 'visible',
            'label' => 'LBL_START_ENGAGEMENT',
          ),
          1 => 
          array (
            'name' => 'start_event_approval_c',
            'studio' => 'visible',
            'label' => 'LBL_START_EVENT_APPROVAL',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'start_eoi_c',
            'studio' => 'visible',
            'label' => 'LBL_START_EOI',
          ),
          1 => 
          array (
            'name' => 'start_endorsement_c',
            'studio' => 'visible',
            'label' => 'LBL_START_ENDORSEMENT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'start_event_docs_c',
            'studio' => 'visible',
            'label' => 'LBL_START_EVENT_DOCS',
          ),
          1 => 
          array (
            'name' => 'start_inspection_c',
            'studio' => 'visible',
            'label' => 'LBL_START_INSPECTION',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'start_presentation_c',
            'studio' => 'visible',
            'label' => 'LBL_START_PRESENTATION',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => '',
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'start_mou_c',
            'studio' => 'visible',
            'label' => 'LBL_START_MOU',
          ),
        ),
      ),
      'lbl_editview_panel7' => 
      array (
        0 => 
        array (
          0 => 'description',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'challenges_c',
            'studio' => 'visible',
            'label' => 'LBL_CHALLENGES',
          ),
          1 => 
          array (
            'name' => 'way_forward_c',
            'studio' => 'visible',
            'label' => 'LBL_WAY_FORWARD',
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'step3_created_c',
            'label' => 'LBL_STEP3_CREATED',
          ),
        ),
      ),
    ),
  ),
);
;
?>
