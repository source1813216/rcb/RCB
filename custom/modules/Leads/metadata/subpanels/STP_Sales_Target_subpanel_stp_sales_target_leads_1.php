<?php
// created: 2021-08-17 12:54:09
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_LIST_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'sort_order' => 'asc',
    'sort_by' => 'last_name',
    'module' => 'Leads',
    'width' => '20%',
    'default' => true,
  ),
  'lead_source' => 
  array (
    'vname' => 'LBL_LIST_LEAD_SOURCE',
    'width' => '13%',
    'default' => true,
  ),
  'venue_c' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_VENUE',
    'id' => 'VNP_VENUES_ID_C',
    'link' => true,
    'width' => '10%',
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'VNP_Venues',
    'target_record_key' => 'vnp_venues_id_c',
  ),
  'expected_revenue_c' => 
  array (
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_EXPECTED_REVENUE',
    'currency_format' => true,
    'width' => '10%',
  ),
  'gor_revenue_c' => 
  array (
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_GOR_REVENUE',
    'currency_format' => true,
    'width' => '10%',
  ),
  'gor_commitment_c' => 
  array (
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_GOR_COMMITMENT',
    'currency_format' => true,
    'width' => '10%',
  ),
  'lead_progress_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_LEAD_PROGRESS',
    'width' => '10%',
  ),
  'status' => 
  array (
    'type' => 'enum',
    'vname' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'Leads',
    'width' => '4%',
    'default' => true,
  ),
  'first_name' => 
  array (
    'usage' => 'query_only',
  ),
  'last_name' => 
  array (
    'usage' => 'query_only',
  ),
  'salutation' => 
  array (
    'name' => 'salutation',
    'usage' => 'query_only',
  ),
);