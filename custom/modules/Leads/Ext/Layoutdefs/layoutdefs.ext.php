<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-06-18 14:21:21
$layout_defs["Leads"]["subpanel_setup"]['leads_aos_contracts_1'] = array (
  'order' => 100,
  'module' => 'AOS_Contracts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_AOS_CONTRACTS_1_FROM_AOS_CONTRACTS_TITLE',
  'get_subpanel_data' => 'leads_aos_contracts_1',
  'top_buttons' => 
  array (
   /* 0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),*/
  ),
);


 // created: 2021-06-18 07:43:43
$layout_defs["Leads"]["subpanel_setup"]['leads_aos_invoices_1'] = array (
  'order' => 100,
  'module' => 'AOS_Invoices',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
  'get_subpanel_data' => 'leads_aos_invoices_1',
  'top_buttons' => 
  array (
   /* 0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
	*/
  ),
);


 // created: 2021-05-26 10:26:14
$layout_defs["Leads"]["subpanel_setup"]['leads_aos_quotes_1'] = array (
  'order' => 100,
  'module' => 'AOS_Quotes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
  'get_subpanel_data' => 'leads_aos_quotes_1',
  'top_buttons' => 
  array (
   /* 0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ), */
  ),
);


 // created: 2021-05-18 18:18:41
$layout_defs["Leads"]["subpanel_setup"]['leads_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'leads_documents_1',
  'top_buttons' => 
  array (
   0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
	
  ),
);


 // created: 2021-06-07 14:48:33
$layout_defs["Leads"]["subpanel_setup"]['leads_eap_event_approval_1'] = array (
  'order' => 100,
  'module' => 'EAP_Event_Approval',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_EAP_EVENT_APPROVAL_1_FROM_EAP_EVENT_APPROVAL_TITLE',
  'get_subpanel_data' => 'leads_eap_event_approval_1',
  'top_buttons' => 
  array (
   /* 0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
	*/
  ),
);


 // created: 2021-06-15 16:46:24
$layout_defs["Leads"]["subpanel_setup"]['leads_edp_endorsements_1'] = array (
  'order' => 100,
  'module' => 'EDP_Endorsements',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_EDP_ENDORSEMENTS_TITLE',
  'get_subpanel_data' => 'leads_edp_endorsements_1',
  'top_buttons' => 
  array (
   /* 0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
	*/
  ),
);


 // created: 2021-06-14 16:42:28
$layout_defs["Leads"]["subpanel_setup"]['leads_edp_event_documents_1'] = array (
  'order' => 100,
  'module' => 'EDP_Event_Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_EDP_EVENT_DOCUMENTS_1_FROM_EDP_EVENT_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'leads_edp_event_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
	
  ),
);


 // created: 2021-05-18 07:14:04
$layout_defs["Leads"]["subpanel_setup"]['leads_iep_initial_engagement_1'] = array (
  'order' => 100,
  'module' => 'IEP_Initial_Engagement',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_IEP_INITIAL_ENGAGEMENT_1_FROM_IEP_INITIAL_ENGAGEMENT_TITLE',
  'get_subpanel_data' => 'leads_iep_initial_engagement_1',
  'top_buttons' => 
  array (
    /* 0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ), */
  ),
);


 // created: 2021-07-12 17:37:00
$layout_defs["Leads"]["subpanel_setup"]['leads_jjwg_maps_1'] = array (
  'order' => 100,
  'module' => 'jjwg_Maps',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_JJWG_MAPS_1_FROM_JJWG_MAPS_TITLE',
  'get_subpanel_data' => 'leads_jjwg_maps_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2021-07-15 11:19:53
$layout_defs["Leads"]["subpanel_setup"]['leads_lsp_lead_status_1'] = array (
  'order' => 100,
  'module' => 'LSP_Lead_Status',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_LSP_LEAD_STATUS_1_FROM_LSP_LEAD_STATUS_TITLE',
  'get_subpanel_data' => 'leads_lsp_lead_status_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2021-07-12 17:45:01
$layout_defs["Leads"]["subpanel_setup"]['leads_mou_mou_1'] = array (
  'order' => 100,
  'module' => 'MOU_MOU',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_MOU_MOU_1_FROM_MOU_MOU_TITLE',
  'get_subpanel_data' => 'leads_mou_mou_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2021-06-02 12:42:20
$layout_defs["Leads"]["subpanel_setup"]['leads_notes_1'] = array (
  'order' => 100,
  'module' => 'Notes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_NOTES_1_FROM_NOTES_TITLE',
  'get_subpanel_data' => 'leads_notes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2021-08-03 18:02:01
$layout_defs["Leads"]["subpanel_setup"]['leads_project_1'] = array (
  'order' => 100,
  'module' => 'Project',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_PROJECT_1_FROM_PROJECT_TITLE',
  'get_subpanel_data' => 'leads_project_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2021-08-17 16:27:33
$layout_defs["Leads"]["subpanel_setup"]['leads_usp_update_lead_status_1'] = array (
  'order' => 100,
  'module' => 'USP_Update_Lead_Status',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_USP_UPDATE_LEAD_STATUS_TITLE',
  'get_subpanel_data' => 'leads_usp_update_lead_status_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_aos_contracts_1']['override_subpanel_name'] = 'Lead_subpanel_leads_aos_contracts_1';


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_aos_invoices_1']['override_subpanel_name'] = 'Lead_subpanel_leads_aos_invoices_1';


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_aos_quotes_1']['override_subpanel_name'] = 'Lead_subpanel_leads_aos_quotes_1';


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_documents_1']['override_subpanel_name'] = 'Lead_subpanel_leads_documents_1';


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_eap_event_approval_1']['override_subpanel_name'] = 'Lead_subpanel_leads_eap_event_approval_1';


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_edp_endorsements_1']['override_subpanel_name'] = 'Lead_subpanel_leads_edp_endorsements_1';


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_edp_event_documents_1']['override_subpanel_name'] = 'Lead_subpanel_leads_edp_event_documents_1';


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_iep_initial_engagement_1']['override_subpanel_name'] = 'Lead_subpanel_leads_iep_initial_engagement_1';


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_lsp_lead_status_1']['override_subpanel_name'] = 'Lead_subpanel_leads_lsp_lead_status_1';


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_mou_mou_1']['override_subpanel_name'] = 'Lead_subpanel_leads_mou_mou_1';


//auto-generated file DO NOT EDIT
$layout_defs['Leads']['subpanel_setup']['leads_notes_1']['override_subpanel_name'] = 'Lead_subpanel_leads_notes_1';

?>