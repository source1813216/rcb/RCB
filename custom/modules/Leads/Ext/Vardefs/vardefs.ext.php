<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-06-18 14:21:21
$dictionary["Lead"]["fields"]["leads_aos_contracts_1"] = array (
  'name' => 'leads_aos_contracts_1',
  'type' => 'link',
  'relationship' => 'leads_aos_contracts_1',
  'source' => 'non-db',
  'module' => 'AOS_Contracts',
  'bean_name' => 'AOS_Contracts',
  'side' => 'right',
  'vname' => 'LBL_LEADS_AOS_CONTRACTS_1_FROM_AOS_CONTRACTS_TITLE',
);


// created: 2021-06-18 07:43:43
$dictionary["Lead"]["fields"]["leads_aos_invoices_1"] = array (
  'name' => 'leads_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'leads_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'side' => 'right',
  'vname' => 'LBL_LEADS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


// created: 2021-05-26 10:26:14
$dictionary["Lead"]["fields"]["leads_aos_quotes_1"] = array (
  'name' => 'leads_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'leads_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'side' => 'right',
  'vname' => 'LBL_LEADS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


// created: 2021-05-18 18:18:41
$dictionary["Lead"]["fields"]["leads_documents_1"] = array (
  'name' => 'leads_documents_1',
  'type' => 'link',
  'relationship' => 'leads_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_LEADS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2021-06-07 14:48:34
$dictionary["Lead"]["fields"]["leads_eap_event_approval_1"] = array (
  'name' => 'leads_eap_event_approval_1',
  'type' => 'link',
  'relationship' => 'leads_eap_event_approval_1',
  'source' => 'non-db',
  'module' => 'EAP_Event_Approval',
  'bean_name' => 'EAP_Event_Approval',
  'side' => 'right',
  'vname' => 'LBL_LEADS_EAP_EVENT_APPROVAL_1_FROM_EAP_EVENT_APPROVAL_TITLE',
);


// created: 2021-06-15 16:46:24
$dictionary["Lead"]["fields"]["leads_edp_endorsements_1"] = array (
  'name' => 'leads_edp_endorsements_1',
  'type' => 'link',
  'relationship' => 'leads_edp_endorsements_1',
  'source' => 'non-db',
  'module' => 'EDP_Endorsements',
  'bean_name' => 'EDP_Endorsements',
  'side' => 'right',
  'vname' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_EDP_ENDORSEMENTS_TITLE',
);


// created: 2021-06-14 16:42:28
$dictionary["Lead"]["fields"]["leads_edp_event_documents_1"] = array (
  'name' => 'leads_edp_event_documents_1',
  'type' => 'link',
  'relationship' => 'leads_edp_event_documents_1',
  'source' => 'non-db',
  'module' => 'EDP_Event_Documents',
  'bean_name' => 'EDP_Event_Documents',
  'side' => 'right',
  'vname' => 'LBL_LEADS_EDP_EVENT_DOCUMENTS_1_FROM_EDP_EVENT_DOCUMENTS_TITLE',
);


// created: 2021-05-18 07:14:04
$dictionary["Lead"]["fields"]["leads_iep_initial_engagement_1"] = array (
  'name' => 'leads_iep_initial_engagement_1',
  'type' => 'link',
  'relationship' => 'leads_iep_initial_engagement_1',
  'source' => 'non-db',
  'module' => 'IEP_Initial_Engagement',
  'bean_name' => 'IEP_Initial_Engagement',
  'side' => 'right',
  'vname' => 'LBL_LEADS_IEP_INITIAL_ENGAGEMENT_1_FROM_IEP_INITIAL_ENGAGEMENT_TITLE',
);


// created: 2021-07-12 17:37:00
$dictionary["Lead"]["fields"]["leads_jjwg_maps_1"] = array (
  'name' => 'leads_jjwg_maps_1',
  'type' => 'link',
  'relationship' => 'leads_jjwg_maps_1',
  'source' => 'non-db',
  'module' => 'jjwg_Maps',
  'bean_name' => 'jjwg_Maps',
  'side' => 'right',
  'vname' => 'LBL_LEADS_JJWG_MAPS_1_FROM_JJWG_MAPS_TITLE',
);


// created: 2021-07-15 11:19:53
$dictionary["Lead"]["fields"]["leads_lsp_lead_status_1"] = array (
  'name' => 'leads_lsp_lead_status_1',
  'type' => 'link',
  'relationship' => 'leads_lsp_lead_status_1',
  'source' => 'non-db',
  'module' => 'LSP_Lead_Status',
  'bean_name' => 'LSP_Lead_Status',
  'side' => 'right',
  'vname' => 'LBL_LEADS_LSP_LEAD_STATUS_1_FROM_LSP_LEAD_STATUS_TITLE',
);


// created: 2021-07-12 17:45:01
$dictionary["Lead"]["fields"]["leads_mou_mou_1"] = array (
  'name' => 'leads_mou_mou_1',
  'type' => 'link',
  'relationship' => 'leads_mou_mou_1',
  'source' => 'non-db',
  'module' => 'MOU_MOU',
  'bean_name' => 'MOU_MOU',
  'side' => 'right',
  'vname' => 'LBL_LEADS_MOU_MOU_1_FROM_MOU_MOU_TITLE',
);


// created: 2021-06-02 12:42:20
$dictionary["Lead"]["fields"]["leads_notes_1"] = array (
  'name' => 'leads_notes_1',
  'type' => 'link',
  'relationship' => 'leads_notes_1',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'side' => 'right',
  'vname' => 'LBL_LEADS_NOTES_1_FROM_NOTES_TITLE',
);


// created: 2021-08-03 18:02:01
$dictionary["Lead"]["fields"]["leads_project_1"] = array (
  'name' => 'leads_project_1',
  'type' => 'link',
  'relationship' => 'leads_project_1',
  'source' => 'non-db',
  'module' => 'Project',
  'bean_name' => 'Project',
  'side' => 'right',
  'vname' => 'LBL_LEADS_PROJECT_1_FROM_PROJECT_TITLE',
);


// created: 2021-08-17 16:27:33
$dictionary["Lead"]["fields"]["leads_usp_update_lead_status_1"] = array (
  'name' => 'leads_usp_update_lead_status_1',
  'type' => 'link',
  'relationship' => 'leads_usp_update_lead_status_1',
  'source' => 'non-db',
  'module' => 'USP_Update_Lead_Status',
  'bean_name' => 'USP_Update_Lead_Status',
  'side' => 'right',
  'vname' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_USP_UPDATE_LEAD_STATUS_TITLE',
);


// created: 2021-08-15 18:03:18
$dictionary["Lead"]["fields"]["stp_sales_target_leads_1"] = array (
  'name' => 'stp_sales_target_leads_1',
  'type' => 'link',
  'relationship' => 'stp_sales_target_leads_1',
  'source' => 'non-db',
  'module' => 'STP_Sales_Target',
  'bean_name' => 'STP_Sales_Target',
  'vname' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_STP_SALES_TARGET_TITLE',
  'id_name' => 'stp_sales_target_leads_1stp_sales_target_ida',
);
$dictionary["Lead"]["fields"]["stp_sales_target_leads_1_name"] = array (
  'name' => 'stp_sales_target_leads_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_STP_SALES_TARGET_TITLE',
  'save' => true,
  'id_name' => 'stp_sales_target_leads_1stp_sales_target_ida',
  'link' => 'stp_sales_target_leads_1',
  'table' => 'stp_sales_target',
  'module' => 'STP_Sales_Target',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["stp_sales_target_leads_1stp_sales_target_ida"] = array (
  'name' => 'stp_sales_target_leads_1stp_sales_target_ida',
  'type' => 'link',
  'relationship' => 'stp_sales_target_leads_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_LEADS_TITLE',
);


 // created: 2021-05-16 22:47:41
$dictionary['Lead']['fields']['account_c']['inline_edit']='1';
$dictionary['Lead']['fields']['account_c']['labelValue']='Account';

 

 // created: 2021-05-16 22:47:41
$dictionary['Lead']['fields']['account_id_c']['inline_edit']=1;

 

 // created: 2021-07-26 12:56:21
$dictionary['Lead']['fields']['am_projecttemplates_id_c']['inline_edit']=1;

 

 // created: 2021-05-22 08:20:47
$dictionary['Lead']['fields']['auto_calculate_c']['inline_edit']='1';
$dictionary['Lead']['fields']['auto_calculate_c']['labelValue']='Manually Calculate';

 

 // created: 2021-07-31 19:21:05
$dictionary['Lead']['fields']['bid_presentation_c']['inline_edit']='1';
$dictionary['Lead']['fields']['bid_presentation_c']['labelValue']='Bid Presentation';

 

 // created: 2022-12-14 18:39:49
$dictionary['Lead']['fields']['brief_c']['inline_edit']='1';
$dictionary['Lead']['fields']['brief_c']['labelValue']='Brief';

 

 // created: 2022-12-13 17:58:35
$dictionary['Lead']['fields']['challenges_c']['inline_edit']='1';
$dictionary['Lead']['fields']['challenges_c']['labelValue']='Challenges';

 

 // created: 2021-05-22 20:10:45
$dictionary['Lead']['fields']['client_reponse_details_c']['inline_edit']='1';
$dictionary['Lead']['fields']['client_reponse_details_c']['labelValue']='Client Reponse Details';

 

 // created: 2021-05-16 22:48:33
$dictionary['Lead']['fields']['contact_id_c']['inline_edit']=1;

 

 // created: 2021-05-16 22:49:57
$dictionary['Lead']['fields']['currency_id']['inline_edit']=1;

 

 // created: 2021-05-20 07:39:52
$dictionary['Lead']['fields']['day_spending_c']['inline_edit']='1';
$dictionary['Lead']['fields']['day_spending_c']['labelValue']='Day Spending';

 

 // created: 2022-12-13 17:59:21
$dictionary['Lead']['fields']['description']['inline_edit']=true;
$dictionary['Lead']['fields']['description']['comments']='Full text of the note';
$dictionary['Lead']['fields']['description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['description']['audited']=true;

 

 // created: 2021-06-14 19:15:07
$dictionary['Lead']['fields']['documentation_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['documentation_date_c']['labelValue']='Documentation Date';

 

 // created: 2021-09-02 14:51:45
$dictionary['Lead']['fields']['document_process_c']['inline_edit']='1';
$dictionary['Lead']['fields']['document_process_c']['labelValue']='Bid Submission';

 

 // created: 2021-05-16 23:07:22
$dictionary['Lead']['fields']['document_requirement_c']['inline_edit']='1';
$dictionary['Lead']['fields']['document_requirement_c']['labelValue']='Document Requirement';

 

 // created: 2021-06-14 19:30:22
$dictionary['Lead']['fields']['doc_complete_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['doc_complete_date_c']['labelValue']='Complete Date';

 

 // created: 2021-05-18 08:32:24
$dictionary['Lead']['fields']['email1']['required']=true;
$dictionary['Lead']['fields']['email1']['inline_edit']=true;
$dictionary['Lead']['fields']['email1']['merge_filter']='disabled';

 

 // created: 2021-07-31 19:17:41
$dictionary['Lead']['fields']['endorsement_c']['inline_edit']='1';
$dictionary['Lead']['fields']['endorsement_c']['labelValue']='Endorsement';

 

 // created: 2021-06-14 19:14:37
$dictionary['Lead']['fields']['endorsement_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['endorsement_date_c']['labelValue']='Endorsement Start Date';

 

 // created: 2021-06-14 19:24:54
$dictionary['Lead']['fields']['endorsement_end_c']['inline_edit']='1';
$dictionary['Lead']['fields']['endorsement_end_c']['labelValue']='Endorsement Approval End';

 

 // created: 2021-06-14 19:24:13
$dictionary['Lead']['fields']['endorsement_start_c']['inline_edit']='1';
$dictionary['Lead']['fields']['endorsement_start_c']['labelValue']='Endorsement Approval Start';

 

 // created: 2021-05-17 19:35:20
$dictionary['Lead']['fields']['engaged_by_c']['inline_edit']='1';
$dictionary['Lead']['fields']['engaged_by_c']['labelValue']='Engaged By';

 

 // created: 2021-06-07 13:47:50
$dictionary['Lead']['fields']['eoi_approval_end_c']['inline_edit']='1';
$dictionary['Lead']['fields']['eoi_approval_end_c']['labelValue']='EOI Approval End Date';

 

 // created: 2021-06-07 13:47:33
$dictionary['Lead']['fields']['eoi_approval_start_c']['inline_edit']='1';
$dictionary['Lead']['fields']['eoi_approval_start_c']['labelValue']='EOI Approval Start Date';

 

 // created: 2021-07-31 19:17:16
$dictionary['Lead']['fields']['eoi_c']['inline_edit']='1';
$dictionary['Lead']['fields']['eoi_c']['labelValue']='Expression of Interest';

 

 // created: 2021-06-07 13:29:45
$dictionary['Lead']['fields']['event_abbreviation_c']['inline_edit']='1';
$dictionary['Lead']['fields']['event_abbreviation_c']['labelValue']='Event Abbreviation';

 

 // created: 2021-05-16 22:42:23
$dictionary['Lead']['fields']['event_brief_c']['labelValue']='Event Brief';
// $dictionary['Lead']['fields']['event_brief_c'][ 'required']= true;

 

 // created: 2021-06-15 18:10:34
$dictionary['Lead']['fields']['event_coordinator_c']['inline_edit']='1';
$dictionary['Lead']['fields']['event_coordinator_c']['labelValue']='Event Coordinator';

 

 // created: 2022-08-01 13:08:01
$dictionary['Lead']['fields']['event_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['event_date_c']['options']='date_range_search_dom';
$dictionary['Lead']['fields']['event_date_c']['labelValue']='Event Start Date';
$dictionary['Lead']['fields']['event_date_c']['enable_range_search']='1';

 

 // created: 2021-05-20 07:38:12
$dictionary['Lead']['fields']['event_days_c']['inline_edit']='1';
$dictionary['Lead']['fields']['event_days_c']['labelValue']='Event Days';

 

 // created: 2022-08-01 13:08:45
$dictionary['Lead']['fields']['event_end_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['event_end_date_c']['options']='date_range_search_dom';
$dictionary['Lead']['fields']['event_end_date_c']['labelValue']='Event End Date';
$dictionary['Lead']['fields']['event_end_date_c']['enable_range_search']='1';

 

 // created: 2021-05-16 22:41:52
$dictionary['Lead']['fields']['event_title_c']['inline_edit']='1';
$dictionary['Lead']['fields']['event_title_c']['labelValue']='Event Title';

 

 // created: 2021-05-22 13:48:28
$dictionary['Lead']['fields']['expected_revenue_c']['inline_edit']='1';
$dictionary['Lead']['fields']['expected_revenue_c']['options']='numeric_range_search_dom';
$dictionary['Lead']['fields']['expected_revenue_c']['labelValue']='Expected Revenue';
$dictionary['Lead']['fields']['expected_revenue_c']['enable_range_search']='1';

 

 // created: 2021-05-16 22:50:42
$dictionary['Lead']['fields']['gor_commitment_c']['inline_edit']='1';
$dictionary['Lead']['fields']['gor_commitment_c']['labelValue']='GoR Commitment';

 

 // created: 2021-05-27 06:10:15
$dictionary['Lead']['fields']['gor_revenue_c']['inline_edit']='1';
$dictionary['Lead']['fields']['gor_revenue_c']['labelValue']='GoR Revenue';

 

 // created: 2021-05-17 17:28:46
$dictionary['Lead']['fields']['help_notifications_c']['inline_edit']='1';
$dictionary['Lead']['fields']['help_notifications_c']['labelValue']='Help Notifications';

 

 // created: 2021-05-27 08:23:13
$dictionary['Lead']['fields']['iea_end_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['iea_end_date_c']['labelValue']='Initial Event Approval End Date';

 

 // created: 2021-05-27 08:22:48
$dictionary['Lead']['fields']['iea_start_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['iea_start_date_c']['labelValue']='Event Approval  Start Date';

 

 // created: 2021-05-17 18:58:30
$dictionary['Lead']['fields']['initial_email_c']['inline_edit']='1';
$dictionary['Lead']['fields']['initial_email_c']['labelValue']='Initial Email';

 

 // created: 2021-05-27 08:22:03
$dictionary['Lead']['fields']['initial_event_approval_c']['inline_edit']='1';
$dictionary['Lead']['fields']['initial_event_approval_c']['labelValue']='Initial Event Approval';

 

 // created: 2021-07-01 11:22:28
$dictionary['Lead']['fields']['inspection_end_c']['inline_edit']='1';
$dictionary['Lead']['fields']['inspection_end_c']['labelValue']='Inspection End Date';

 

 // created: 2021-07-01 11:22:12
$dictionary['Lead']['fields']['inspection_start_c']['inline_edit']='1';
$dictionary['Lead']['fields']['inspection_start_c']['labelValue']='Inspection Start Date';

 

 // created: 2021-05-05 11:41:20
$dictionary['Lead']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:20
$dictionary['Lead']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:20
$dictionary['Lead']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:20
$dictionary['Lead']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2021-07-30 09:42:28
$dictionary['Lead']['fields']['lead_progress_c']['inline_edit']='1';
$dictionary['Lead']['fields']['lead_progress_c']['labelValue']='Lead Progress';

 

 // created: 2021-05-16 23:30:33
$dictionary['Lead']['fields']['lead_source']['inline_edit']=true;
$dictionary['Lead']['fields']['lead_source']['comments']='Lead source (ex: Web, print)';
$dictionary['Lead']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Lead']['fields']['lead_source']['required']=true;

 

 // created: 2021-05-22 14:47:25
$dictionary['Lead']['fields']['line_institute_c']['inline_edit']='1';
$dictionary['Lead']['fields']['line_institute_c']['labelValue']='Line Institute';

 

 // created: 2021-05-22 14:47:25
$dictionary['Lead']['fields']['lip_line_institutes_id_c']['inline_edit']=1;

 

 // created: 2021-07-31 19:21:46
$dictionary['Lead']['fields']['mou_c']['inline_edit']='1';
$dictionary['Lead']['fields']['mou_c']['labelValue']='MOU';

 

 // created: 2021-07-12 17:39:35
$dictionary['Lead']['fields']['mou_end_c']['inline_edit']='1';
$dictionary['Lead']['fields']['mou_end_c']['labelValue']='MOU End Date';

 

 // created: 2021-07-12 17:39:17
$dictionary['Lead']['fields']['mou_start_c']['inline_edit']='1';
$dictionary['Lead']['fields']['mou_start_c']['labelValue']='MOU Start Date';

 

 // created: 2021-08-17 18:38:36
$dictionary['Lead']['fields']['no_of_events_c']['inline_edit']='1';
$dictionary['Lead']['fields']['no_of_events_c']['labelValue']='No of Events';

 

 // created: 2021-08-17 18:39:09
$dictionary['Lead']['fields']['no_of_site_inspections_c']['inline_edit']='1';
$dictionary['Lead']['fields']['no_of_site_inspections_c']['labelValue']='No of Site Inspections';

 

 // created: 2021-05-20 07:37:04
$dictionary['Lead']['fields']['packs_c']['inline_edit']='1';
$dictionary['Lead']['fields']['packs_c']['labelValue']='No of Delegates';

 

 // created: 2021-07-01 11:22:51
$dictionary['Lead']['fields']['presentation_end_c']['inline_edit']='1';
$dictionary['Lead']['fields']['presentation_end_c']['labelValue']='Presentation End Date';

 

 // created: 2021-07-01 11:22:36
$dictionary['Lead']['fields']['presentation_start_c']['inline_edit']='1';
$dictionary['Lead']['fields']['presentation_start_c']['labelValue']='Presentation Start Date';

 

 // created: 2021-05-16 22:48:33
$dictionary['Lead']['fields']['primary_contact_c']['inline_edit']='1';
$dictionary['Lead']['fields']['primary_contact_c']['labelValue']='Primary Lead Contact';

 

 // created: 2021-05-20 08:25:08
$dictionary['Lead']['fields']['probability_c']['inline_edit']='1';
$dictionary['Lead']['fields']['probability_c']['labelValue']='Probability';

 

 // created: 2021-08-03 11:24:27
$dictionary['Lead']['fields']['project_created_c']['inline_edit']='1';
$dictionary['Lead']['fields']['project_created_c']['labelValue']='Project Created';

 

 // created: 2021-07-26 12:56:21
$dictionary['Lead']['fields']['project_type_c']['inline_edit']='1';
$dictionary['Lead']['fields']['project_type_c']['labelValue']='Project Type Template';

 

 // created: 2021-05-17 19:06:41
$dictionary['Lead']['fields']['qualfication_c']['inline_edit']='1';
$dictionary['Lead']['fields']['qualfication_c']['labelValue']='Qualification Status';

 

 // created: 2021-05-17 19:07:07
$dictionary['Lead']['fields']['qualified_by_c']['inline_edit']='1';
$dictionary['Lead']['fields']['qualified_by_c']['labelValue']='Qualified By';

 

 // created: 2021-05-17 19:06:14
$dictionary['Lead']['fields']['qualified_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['qualified_date_c']['labelValue']='Qualified Date';

 

 // created: 2021-08-17 16:23:01
$dictionary['Lead']['fields']['reason_c']['inline_edit']='1';
$dictionary['Lead']['fields']['reason_c']['labelValue']='Reason';

 

 // created: 2021-05-17 19:37:01
$dictionary['Lead']['fields']['responded_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['responded_date_c']['labelValue']='Responded Date';

 

 // created: 2021-08-15 18:20:04
$dictionary['Lead']['fields']['sales_target_c']['inline_edit']='1';
$dictionary['Lead']['fields']['sales_target_c']['labelValue']='Sales Target Value';

 

 // created: 2021-05-16 22:46:06
$dictionary['Lead']['fields']['segment_c']['inline_edit']='1';
$dictionary['Lead']['fields']['segment_c']['labelValue']='Segment';

 

 // created: 2021-05-17 19:36:06
$dictionary['Lead']['fields']['sent_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['sent_date_c']['labelValue']='Sent Date';

 

 // created: 2021-07-31 19:20:48
$dictionary['Lead']['fields']['site_inspection_c']['inline_edit']='1';
$dictionary['Lead']['fields']['site_inspection_c']['labelValue']='Site Inspection';

 

 // created: 2021-05-20 08:24:39
$dictionary['Lead']['fields']['source_details_c']['inline_edit']='1';
$dictionary['Lead']['fields']['source_details_c']['labelValue']='Source Details';

 

 // created: 2021-06-15 17:18:09
$dictionary['Lead']['fields']['start_endorsement_c']['inline_edit']='1';
$dictionary['Lead']['fields']['start_endorsement_c']['labelValue']='Start Endorsement Process';

 

 // created: 2021-09-01 18:55:14
$dictionary['Lead']['fields']['start_engagement_c']['inline_edit']='1';
$dictionary['Lead']['fields']['start_engagement_c']['labelValue']='Start Engagement Process';

 

 // created: 2021-09-01 18:56:52
$dictionary['Lead']['fields']['start_eoi_c']['inline_edit']='1';
$dictionary['Lead']['fields']['start_eoi_c']['labelValue']='Start EOI Process';

 

 // created: 2021-09-01 19:44:48
$dictionary['Lead']['fields']['start_event_approval_c']['inline_edit']='1';
$dictionary['Lead']['fields']['start_event_approval_c']['labelValue']='Start Event Approval';

 

 // created: 2021-09-02 14:53:12
$dictionary['Lead']['fields']['start_event_docs_c']['inline_edit']='1';
$dictionary['Lead']['fields']['start_event_docs_c']['labelValue']='Start Bid Process';

 

 // created: 2021-06-30 20:08:49
$dictionary['Lead']['fields']['start_inspection_c']['inline_edit']='1';
$dictionary['Lead']['fields']['start_inspection_c']['labelValue']='Start Site Inspection';

 

 // created: 2021-07-09 09:31:51
$dictionary['Lead']['fields']['start_mou_c']['inline_edit']='1';
$dictionary['Lead']['fields']['start_mou_c']['labelValue']='Start MOU Process';

 

 // created: 2021-06-30 20:08:34
$dictionary['Lead']['fields']['start_presentation_c']['inline_edit']='1';
$dictionary['Lead']['fields']['start_presentation_c']['labelValue']='Start Bid Presentation';

 

 // created: 2021-05-17 15:11:48
$dictionary['Lead']['fields']['status']['inline_edit']=true;
$dictionary['Lead']['fields']['status']['massupdate']='1';
$dictionary['Lead']['fields']['status']['comments']='Status of the lead';
$dictionary['Lead']['fields']['status']['merge_filter']='disabled';

 

 // created: 2021-07-30 10:38:58
$dictionary['Lead']['fields']['status_change_on_c']['inline_edit']='1';
$dictionary['Lead']['fields']['status_change_on_c']['labelValue']='Status Change On';

 

 // created: 2021-05-27 06:08:30
$dictionary['Lead']['fields']['step2_created_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step2_created_c']['labelValue']='Step2 Created';

 

 // created: 2021-06-07 13:54:12
$dictionary['Lead']['fields']['step3_created_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step3_created_c']['labelValue']='Step3 Created';

 

 // created: 2021-07-31 16:12:34
$dictionary['Lead']['fields']['step3_eoi_chk_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step3_eoi_chk_c']['labelValue']='Step3 EOI CHK';

 

 // created: 2021-06-14 19:37:36
$dictionary['Lead']['fields']['step4_created_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step4_created_c']['labelValue']='Step4 Created';

 

 // created: 2021-07-31 16:13:26
$dictionary['Lead']['fields']['step4_endo_chk_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step4_endo_chk_c']['labelValue']='Step4 ENDO CHK';

 

 // created: 2021-06-14 19:37:49
$dictionary['Lead']['fields']['step5_created_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step5_created_c']['labelValue']='Step5 Created';

 

 // created: 2021-07-31 16:13:50
$dictionary['Lead']['fields']['step5_doc_chk_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step5_doc_chk_c']['labelValue']='Step5 DOC CHK';

 

 // created: 2021-06-30 17:32:10
$dictionary['Lead']['fields']['step6_created_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step6_created_c']['labelValue']='Step6 Created';

 

 // created: 2021-07-31 16:14:15
$dictionary['Lead']['fields']['step6_site_chk_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step6_site_chk_c']['labelValue']='Step6 SITE CHK';

 

 // created: 2021-07-31 16:14:40
$dictionary['Lead']['fields']['step7_bid_chk_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step7_bid_chk_c']['labelValue']='Step7 BID CHK';

 

 // created: 2021-06-30 17:32:25
$dictionary['Lead']['fields']['step7_created_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step7_created_c']['labelValue']='Step7 Created';

 

 // created: 2021-07-31 16:14:57
$dictionary['Lead']['fields']['step8_bid_chk_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step8_bid_chk_c']['labelValue']='Step8 BID CHK';

 

 // created: 2021-07-09 09:32:39
$dictionary['Lead']['fields']['step8_created_c']['inline_edit']='1';
$dictionary['Lead']['fields']['step8_created_c']['labelValue']='Step8 Created';

 

 // created: 2021-05-17 19:35:20
$dictionary['Lead']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2021-06-15 18:10:34
$dictionary['Lead']['fields']['user_id2_c']['inline_edit']=1;

 

 // created: 2021-05-17 19:07:07
$dictionary['Lead']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2021-08-03 12:34:45
$dictionary['Lead']['fields']['venue_c']['inline_edit']='1';
$dictionary['Lead']['fields']['venue_c']['labelValue']='Venue';

 

 // created: 2021-08-03 12:34:45
$dictionary['Lead']['fields']['vnp_venues_id_c']['inline_edit']=1;

 

 // created: 2022-12-13 17:58:12
$dictionary['Lead']['fields']['way_forward_c']['inline_edit']='1';
$dictionary['Lead']['fields']['way_forward_c']['labelValue']='Way Forward';

 
?>