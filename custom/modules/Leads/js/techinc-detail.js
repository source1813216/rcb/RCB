// 
/* Company TechincGlobal
 Custom fields show hidden
 */
$(document).ready(function () {
    var engaged_by_c_row = $("[field='engaged_by_c']").parent('.detail-view-row-item');
    var sent_date_c_row = $("[field='sent_date_c']").parent('.detail-view-row-item');
    var responded_date_c_row = $("[field='responded_date_c']").parent('.detail-view-row-item');
	
	 var eoi_approval_start_c_row = $("[field='eoi_approval_start_c']").parent('.detail-view-row-item');
	 var eoi_approval_end_c_row = $("[field='eoi_approval_end_c']").parent('.detail-view-row-item');
	 
	 var iea_end_date_c_row = $("[field='iea_end_date_c']").parent('.detail-view-row-item');
	 var iea_start_date_c_row = $("[field='iea_start_date_c']").parent('.detail-view-row-item');
	 
	 var iea_end_date_c_row = $("[field='iea_end_date_c']").parent('.detail-view-row-item');
	 var iea_start_date_c_row = $("[field='iea_start_date_c']").parent('.detail-view-row-item');
	 
	 var endorsement_start_c_row = $("[field='endorsement_start_c']").parent('.detail-view-row-item');
	 var endorsement_end_c_row = $("[field='endorsement_end_c']").parent('.detail-view-row-item');
	 var doc_complete_date_c_row = $("[field='doc_complete_date_c']").parent('.detail-view-row-item');
	 
	 
	 var start_endorsement_c_row = $("[field='start_endorsement_c']").parent('.detail-view-row-item');
	 var start_event_docs_c_row = $("[field='start_event_docs_c']").parent('.detail-view-row-item');
	 
	 var inspection_start_c_row = $("[field='inspection_start_c']").parent('.detail-view-row-item');
	 var inspection_end_c_row = $("[field='inspection_end_c']").parent('.detail-view-row-item');
	 
	  var presentation_start_c_row = $("[field='presentation_start_c']").parent('.detail-view-row-item');
	 var presentation_end_c_row = $("[field='presentation_end_c']").parent('.detail-view-row-item');
	 
	 
	 var iea_start_date_c_val=$('#iea_start_date_c').text();
	 var iea_end_date_c_val=$('#iea_end_date_c').text();
	 
	 var eoi_approval_start_c_val=$('#eoi_approval_start_c').text();
	 var  eoi_approval_end_c_val=$('#eoi_approval_end_c').text();
	 
	 var endorsement_start_c_val=$('#endorsement_start_c').text();
	 var endorsement_end_c_val=$('#endorsement_end_c').text();
	 var doc_complete_date_c_val=$('#doc_complete_date_c').text();
	  var inspection_start_c_val=$('#inspection_start_c').text();
	 var inspection_end_c_val=$('#inspection_end_c').text();
	 
	  var presentation_start_c_val=$('#presentation_start_c').text();
	 var presentation_end_c_val=$('#presentation_end_c').text();

    if ($(initial_email_c).val() == "Pending") {
        engaged_by_c_row.attr("hidden",true);
        sent_date_c_row.attr("hidden",true);
       // responded_dainspection_start_cte_c_row.attr("hidden",true);
    }else{
        if($(initial_email_c).val() != "Response"){
            responded_date_c_row.attr("hidden",true);
        }
    }
	
	if(presentation_start_c_val==""){
		presentation_start_c_row.attr("hidden",true);
	}
	if(presentation_end_c_val==""){
		presentation_end_c_row.attr("hidden",true);
	}
	
	
	if(inspection_end_c_val==""){
		inspection_end_c_row.attr("hidden",true);
	}
	if(inspection_start_c_val==""){
		inspection_start_c_row.attr("hidden",true);
	}
	
	if(iea_end_date_c_val==""){
		iea_end_date_c_row.attr("hidden",true);
	}
	if(iea_start_date_c_val==""){
		iea_start_date_c_row.attr("hidden",true);
	}
	
	if(eoi_approval_start_c_val==""){
		eoi_approval_start_c_row.attr("hidden",true);
	}
	if(eoi_approval_end_c_val==""){
		eoi_approval_end_c_row.attr("hidden",true);
	}
	
	if(endorsement_start_c_val==""){
		endorsement_start_c_row.attr("hidden",true);
	}
	if(endorsement_end_c_val==""){
		endorsement_end_c_row.attr("hidden",true);
	}
	
	if(doc_complete_date_c_val==""){
		doc_complete_date_c_row.attr("hidden",true);
	}
	if($('#mou_end_c').text()==""){
		$("[field='mou_end_c']").parent('.detail-view-row-item').attr("hidden",true);
	}
	
	if($('#step3_created_c').prop("checked") == true){
		//alert('ok');
  }else{
	  start_endorsement_c_row.attr("hidden",true);
	  start_event_docs_c_row.attr("hidden",true);
	   // alert('note');
  }
	

});