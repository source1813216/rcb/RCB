<?php
$module_name = 'EDP_Event_Documents';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'validate_budgetdocs_c',
            'studio' => 'visible',
            'label' => 'LBL_VALIDATE_BUDGETDOCS',
          ),
          1 => 
          array (
            'name' => 'budget_docs_c',
            'label' => 'LBL_BUDGET_DOCS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'validate_endorsements_c',
            'studio' => 'visible',
            'label' => 'LBL_VALIDATE_ENDORSEMENTS',
          ),
          1 => 
          array (
            'name' => 'endorsements_c',
            'label' => 'LBL_ENDORSEMENTS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'validate_biddocs_c',
            'studio' => 'visible',
            'label' => 'LBL_VALIDATE_BIDDOCS',
          ),
          1 => 
          array (
            'name' => 'bid_docs_c',
            'label' => 'LBL_BID_DOCS',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'event_coordinator_c',
            'studio' => 'visible',
            'label' => 'LBL_EVENT_COORDINATOR',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 'description',
        ),
      ),
    ),
  ),
);
;
?>
