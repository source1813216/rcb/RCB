<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-06-14 17:23:42
$dictionary["EDP_Event_Documents"]["fields"]["edp_event_documents_documents_1"] = array (
  'name' => 'edp_event_documents_documents_1',
  'type' => 'link',
  'relationship' => 'edp_event_documents_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2021-06-14 16:42:28
$dictionary["EDP_Event_Documents"]["fields"]["leads_edp_event_documents_1"] = array (
  'name' => 'leads_edp_event_documents_1',
  'type' => 'link',
  'relationship' => 'leads_edp_event_documents_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_EDP_EVENT_DOCUMENTS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_edp_event_documents_1leads_ida',
);
$dictionary["EDP_Event_Documents"]["fields"]["leads_edp_event_documents_1_name"] = array (
  'name' => 'leads_edp_event_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_EDP_EVENT_DOCUMENTS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_edp_event_documents_1leads_ida',
  'link' => 'leads_edp_event_documents_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["EDP_Event_Documents"]["fields"]["leads_edp_event_documents_1leads_ida"] = array (
  'name' => 'leads_edp_event_documents_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_edp_event_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_EDP_EVENT_DOCUMENTS_1_FROM_EDP_EVENT_DOCUMENTS_TITLE',
);


 // created: 2021-06-14 17:10:27
$dictionary['EDP_Event_Documents']['fields']['account_id_c']['inline_edit']=1;

 

 // created: 2021-06-14 17:34:23
$dictionary['EDP_Event_Documents']['fields']['approved_endrosements_c']['inline_edit']='1';
$dictionary['EDP_Event_Documents']['fields']['approved_endrosements_c']['labelValue']='Approved Endrosements';

 

 // created: 2021-06-14 17:34:56
$dictionary['EDP_Event_Documents']['fields']['bid_book_c']['inline_edit']='1';
$dictionary['EDP_Event_Documents']['fields']['bid_book_c']['labelValue']='Bid Book';

 

 // created: 2021-06-15 09:46:27
$dictionary['EDP_Event_Documents']['fields']['bid_docs_c']['inline_edit']='1';
$dictionary['EDP_Event_Documents']['fields']['bid_docs_c']['labelValue']='Bid Docs';

 

 // created: 2021-06-15 09:46:11
$dictionary['EDP_Event_Documents']['fields']['budget_docs_c']['inline_edit']='1';
$dictionary['EDP_Event_Documents']['fields']['budget_docs_c']['labelValue']='Budget Docs';

 

 // created: 2021-06-14 17:10:28
$dictionary['EDP_Event_Documents']['fields']['client_c']['inline_edit']='1';
$dictionary['EDP_Event_Documents']['fields']['client_c']['labelValue']='Client';

 

 // created: 2021-06-14 17:10:45
$dictionary['EDP_Event_Documents']['fields']['contact_id_c']['inline_edit']=1;

 

 // created: 2021-06-15 09:46:49
$dictionary['EDP_Event_Documents']['fields']['endorsements_c']['inline_edit']='1';
$dictionary['EDP_Event_Documents']['fields']['endorsements_c']['labelValue']='Endorsements';

 

 // created: 2021-06-14 17:11:16
$dictionary['EDP_Event_Documents']['fields']['event_brief_c']['labelValue']='Event Brief';

 

 // created: 2021-06-14 17:33:46
$dictionary['EDP_Event_Documents']['fields']['event_budget_c']['inline_edit']='1';
$dictionary['EDP_Event_Documents']['fields']['event_budget_c']['labelValue']='Event Budget';

 

 // created: 2021-06-14 17:49:31
$dictionary['EDP_Event_Documents']['fields']['event_coordinator_c']['inline_edit']='1';
$dictionary['EDP_Event_Documents']['fields']['event_coordinator_c']['labelValue']='Event Coordinator';

 

 // created: 2021-06-22 18:37:37
$dictionary['EDP_Event_Documents']['fields']['name']['full_text_search']=array (
);

 

 // created: 2021-06-14 17:10:45
$dictionary['EDP_Event_Documents']['fields']['primary_contact_c']['inline_edit']='1';
$dictionary['EDP_Event_Documents']['fields']['primary_contact_c']['labelValue']='Primary Contact';

 

 // created: 2021-06-14 17:49:31
$dictionary['EDP_Event_Documents']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2021-06-15 09:50:33
$dictionary['EDP_Event_Documents']['fields']['validate_biddocs_c']['inline_edit']='';
$dictionary['EDP_Event_Documents']['fields']['validate_biddocs_c']['labelValue']='3';

 

 // created: 2021-06-15 09:48:26
$dictionary['EDP_Event_Documents']['fields']['validate_budgetdocs_c']['inline_edit']='';
$dictionary['EDP_Event_Documents']['fields']['validate_budgetdocs_c']['labelValue']='1';

 

 // created: 2021-06-15 09:49:41
$dictionary['EDP_Event_Documents']['fields']['validate_endorsements_c']['inline_edit']='';
$dictionary['EDP_Event_Documents']['fields']['validate_endorsements_c']['labelValue']='2';

 
?>