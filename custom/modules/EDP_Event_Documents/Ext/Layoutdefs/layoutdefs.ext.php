<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-06-14 17:23:42
$layout_defs["EDP_Event_Documents"]["subpanel_setup"]['edp_event_documents_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'edp_event_documents_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['EDP_Event_Documents']['subpanel_setup']['edp_event_documents_documents_1']['override_subpanel_name'] = 'EDP_Event_Documents_subpanel_edp_event_documents_documents_1';

?>