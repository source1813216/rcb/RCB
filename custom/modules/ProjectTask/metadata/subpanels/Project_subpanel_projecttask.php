<?php
// created: 2021-08-05 08:55:00
$subpanel_layout['list_fields'] = array (
  'task_number' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_TASK_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'milestone_flag' => 
  array (
    'type' => 'bool',
    'vname' => 'LBL_MILESTONE_FLAG',
    'width' => '10%',
    'default' => true,
  ),
  'predecessors' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_PREDECESSORS',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_LIST_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '70%',
    'default' => true,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'vname' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'date_start' => 
  array (
    'vname' => 'LBL_DATE_START',
    'width' => '15%',
    'default' => true,
  ),
  'date_finish' => 
  array (
    'vname' => 'LBL_DATE_FINISH',
    'width' => '15%',
    'default' => true,
  ),
  'percent_complete' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_PERCENT_COMPLETE',
    'width' => '10%',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ASSIGNED_USER_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'ProjectTask',
    'width' => '3%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'Project',
    'width' => '3%',
    'default' => true,
  ),
);