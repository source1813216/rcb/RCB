<?php
$module_name = 'USP_Update_Lead_Status';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_DETAILVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'change_status_c',
            'studio' => 'visible',
            'label' => 'LBL_CHANGE_STATUS',
          ),
          1 => 
          array (
            'name' => 'leads_usp_update_lead_status_1_name',
            'label' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_LEADS_TITLE',
          ),
        ),
        1 => 
        array (
          0 => 'description',
        ),
      ),
      'lbl_detailview_panel2' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
          1 => 
          array (
            'name' => 'updated_c',
            'label' => 'LBL_UPDATED',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'changed_by_c',
            'studio' => 'visible',
            'label' => 'LBL_CHANGED_BY',
          ),
          1 => 
          array (
            'name' => 'changed_date_c',
            'label' => 'LBL_CHANGED_DATE',
          ),
        ),
      ),
    ),
  ),
);
;
?>
