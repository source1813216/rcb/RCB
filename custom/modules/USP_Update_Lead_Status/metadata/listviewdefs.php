<?php
$module_name = 'USP_Update_Lead_Status';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'LEADS_USP_UPDATE_LEAD_STATUS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_LEADS_TITLE',
    'id' => 'LEADS_USP_UPDATE_LEAD_STATUS_1LEADS_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'CHANGE_STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_CHANGE_STATUS',
    'width' => '10%',
  ),
  'CHANGED_BY_C' => 
  array (
    'type' => 'relate',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_CHANGED_BY',
    'id' => 'USER_ID_C',
    'link' => true,
    'width' => '10%',
  ),
  'CHANGED_DATE_C' => 
  array (
    'type' => 'datetimecombo',
    'default' => true,
    'label' => 'LBL_CHANGED_DATE',
    'width' => '10%',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
);
;
?>
