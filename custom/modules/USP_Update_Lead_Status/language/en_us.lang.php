<?php
// created: 2021-08-17 16:28:59
$mod_strings = array (
  'LBL_NAME' => 'Update',
  'LBL_CHANGE_STATUS' => 'Change Status to',
  'LBL_CHANGED_DATE' => 'Changed Date',
  'LBL_CHANGED_BY_USER_ID' => '&#039;Changed By&#039; (related &#039;User&#039; ID)',
  'LBL_CHANGED_BY' => 'Changed By',
  'LBL_DESCRIPTION' => 'Reason',
  'LBL_UPDATED' => 'Updated',
  'LBL_EDITVIEW_PANEL1' => 'Lead Status Update',
  'LBL_DETAILVIEW_PANEL2' => 'Status Changed Details',
);