<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-08-17 16:27:33
$dictionary["USP_Update_Lead_Status"]["fields"]["leads_usp_update_lead_status_1"] = array (
  'name' => 'leads_usp_update_lead_status_1',
  'type' => 'link',
  'relationship' => 'leads_usp_update_lead_status_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_usp_update_lead_status_1leads_ida',
);
$dictionary["USP_Update_Lead_Status"]["fields"]["leads_usp_update_lead_status_1_name"] = array (
  'name' => 'leads_usp_update_lead_status_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_usp_update_lead_status_1leads_ida',
  'link' => 'leads_usp_update_lead_status_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["USP_Update_Lead_Status"]["fields"]["leads_usp_update_lead_status_1leads_ida"] = array (
  'name' => 'leads_usp_update_lead_status_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_usp_update_lead_status_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_USP_UPDATE_LEAD_STATUS_TITLE',
);


 // created: 2021-08-17 16:21:12
$dictionary['USP_Update_Lead_Status']['fields']['changed_by_c']['inline_edit']='1';
$dictionary['USP_Update_Lead_Status']['fields']['changed_by_c']['labelValue']='Changed By';

 

 // created: 2021-08-17 16:20:48
$dictionary['USP_Update_Lead_Status']['fields']['changed_date_c']['inline_edit']='1';
$dictionary['USP_Update_Lead_Status']['fields']['changed_date_c']['labelValue']='Changed Date';

 

 // created: 2021-08-17 16:20:22
$dictionary['USP_Update_Lead_Status']['fields']['change_status_c']['inline_edit']='1';
$dictionary['USP_Update_Lead_Status']['fields']['change_status_c']['labelValue']='Change Status to';

 

 // created: 2021-08-17 16:21:28
$dictionary['USP_Update_Lead_Status']['fields']['description']['required']=true;
$dictionary['USP_Update_Lead_Status']['fields']['description']['inline_edit']=true;
$dictionary['USP_Update_Lead_Status']['fields']['description']['comments']='Full text of the note';
$dictionary['USP_Update_Lead_Status']['fields']['description']['merge_filter']='disabled';

 

 // created: 2021-08-17 16:18:57
$dictionary['USP_Update_Lead_Status']['fields']['name']['inline_edit']=true;
$dictionary['USP_Update_Lead_Status']['fields']['name']['duplicate_merge']='disabled';
$dictionary['USP_Update_Lead_Status']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['USP_Update_Lead_Status']['fields']['name']['merge_filter']='disabled';
$dictionary['USP_Update_Lead_Status']['fields']['name']['unified_search']=false;

 

 // created: 2021-08-17 16:23:55
$dictionary['USP_Update_Lead_Status']['fields']['updated_c']['inline_edit']='1';
$dictionary['USP_Update_Lead_Status']['fields']['updated_c']['labelValue']='Updated';

 

 // created: 2021-08-17 16:21:12
$dictionary['USP_Update_Lead_Status']['fields']['user_id_c']['inline_edit']=1;

 
?>