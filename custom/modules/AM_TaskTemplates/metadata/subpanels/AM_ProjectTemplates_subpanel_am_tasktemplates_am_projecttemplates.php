<?php
// created: 2021-08-05 10:47:48
$subpanel_layout['list_fields'] = array (
  'task_number' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_TASK_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'predecessors' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_PREDECESSORS',
    'width' => '10%',
    'default' => true,
  ),
  'milestone_flag' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_MILESTONE_FLAG',
    'width' => '10%',
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'duration' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_DURATION',
    'width' => '10%',
    'default' => true,
  ),
  'priority' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_PRIORITY',
    'width' => '10%',
  ),
  'status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_STATUS',
    'width' => '10%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '45%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'AM_TaskTemplates',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'AM_TaskTemplates',
    'width' => '5%',
    'default' => true,
  ),
);