<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-05-16 20:21:18
$dictionary['Account']['fields']['account_status_c']['inline_edit']='1';
$dictionary['Account']['fields']['account_status_c']['labelValue']='Account Status';

 

 // created: 2021-05-16 20:29:39
$dictionary['Account']['fields']['contact_id_c']['inline_edit']=1;

 

 // created: 2021-05-20 10:44:56
$dictionary['Account']['fields']['economic_sector_c']['inline_edit']='1';
$dictionary['Account']['fields']['economic_sector_c']['labelValue']='Economic Sector';

 

 // created: 2021-05-16 20:27:41
$dictionary['Account']['fields']['email1']['required']=true;
$dictionary['Account']['fields']['email1']['inline_edit']=true;
$dictionary['Account']['fields']['email1']['merge_filter']='disabled';

 

 // created: 2021-05-20 10:37:39
$dictionary['Account']['fields']['industry']['len']=100;
$dictionary['Account']['fields']['industry']['required']=true;
$dictionary['Account']['fields']['industry']['inline_edit']=true;
$dictionary['Account']['fields']['industry']['comments']='The company belongs in this industry';
$dictionary['Account']['fields']['industry']['merge_filter']='disabled';

 

 // created: 2021-05-05 11:41:19
$dictionary['Account']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:19
$dictionary['Account']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:19
$dictionary['Account']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:19
$dictionary['Account']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2021-05-16 20:27:12
$dictionary['Account']['fields']['phone_office']['required']=true;
$dictionary['Account']['fields']['phone_office']['inline_edit']=true;
$dictionary['Account']['fields']['phone_office']['comments']='The office phone number';
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';

 

 // created: 2021-05-16 21:51:01
$dictionary['Account']['fields']['primary_contact_c']['inline_edit']='1';
$dictionary['Account']['fields']['primary_contact_c']['labelValue']='Primary Contact';

 

 // created: 2021-05-16 20:47:23
$dictionary['Account']['fields']['segment_c']['inline_edit']='1';
$dictionary['Account']['fields']['segment_c']['labelValue']='Segment';

 
?>