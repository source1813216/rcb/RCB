<?php
// created: 2021-06-18 14:06:19
$mod_strings = array (
  'LBL_EMAILS_ACCOUNTS_REL' => 'Emails:Clients',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Clients',
  'LBL_EMAILS_CASES_REL' => 'Emails:Case',
  'LBL_CASES_SUBPANEL_TITLE' => 'Cases',
  'LBL_EMAILS_CONTRACTS_REL' => 'Emails:Contract',
);