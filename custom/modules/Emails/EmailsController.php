<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
include_once 'include/Exceptions/SugarControllerException.php';
include('modules/Emails/Email.php');
include_once('modules/Emails/EmailException.php');
require_once('include/SugarPHPMailer.php');
require_once 'include/UploadFile.php';
require_once 'include/UploadMultipleFiles.php';

class EmailsController extends SugarController {
    public $bean;
    const ERR_INVALID_INBOUND_EMAIL_TYPE = 100;
    const ERR_STORED_OUTBOUND_EMAIL_NOT_SET = 101;
    const ERR_STORED_OUTBOUND_EMAIL_ID_IS_INVALID = 102;
    const ERR_STORED_OUTBOUND_EMAIL_NOT_FOUND = 103;
    const ERR_REPLY_TO_ADDR_NOT_FOUND = 110;
    const ERR_REPLY_TO_FROMAT_INVALID_SPLITS = 111;
    const ERR_REPLY_TO_FROMAT_INVALID_NO_NAME = 112;
    const ERR_REPLY_TO_FROMAT_INVALID_NO_ADDR = 113;
    const ERR_REPLY_TO_FROMAT_INVALID_AS_FROM = 114;

    const COMPOSE_BEAN_MODE_UNDEFINED = 0;

    /**
     * @see EmailsController::composeBean()
     */
    const COMPOSE_BEAN_MODE_REPLY_TO = 1;

    /**
     * @see EmailsController::composeBean()
     */
    const COMPOSE_BEAN_MODE_REPLY_TO_ALL = 2;

    /**
     * @see EmailsController::composeBean()
     */
    const COMPOSE_BEAN_MODE_FORWARD = 3;

    /**
     * @see EmailsController::composeBean()
     */
    const COMPOSE_BEAN_WITH_PDF_TEMPLATE = 4;

    protected static $doNotImportFields = array(
        'action',
        'type',
        'send',
        'record',
        'from_addr_name',
        'reply_to_addr',
        'to_addrs_names',
        'cc_addrs_names',
        'bcc_addrs_names',
        'imap_keywords',
        'raw_source',
        'description',
        'description_html',
        'date_sent',
        'message_id',
        'name',
        'status',
        'reply_to_status',
        'mailbox_id',
        'created_by_link',
        'modified_user_link',
        'assigned_user_link',
        'assigned_user_link',
        'uid',
        'msgno',
        'folder',
        'folder_type',
        'inbound_email_record',
        'is_imported',
        'has_attachment',
        'id',
    );
    public function action_index() {
        $this->view = 'list';
    }//end of method
    public function action_DetailDraftView() {
        $this->view = 'detaildraft';
    }//end of method
    
    public function action_ComposeView()
    {
        $this->view = 'compose';
        // For viewing the Compose as modal from other modules we need to load the Emails language strings
        if (isset($_REQUEST['in_popup']) && $_REQUEST['in_popup']) {
            if (!is_file('cache/jsLanguage/Emails/' . $GLOBALS['current_language'] . '.js')) {
                require_once('include/language/jsLanguage.php');
                jsLanguage::createModuleStringsCache('Emails', $GLOBALS['current_language']);
            }
            echo '<script src="cache/jsLanguage/Emails/'. $GLOBALS['current_language'] . '.js"></script>';
        }
        if (isset($_REQUEST['ids']) && isset($_REQUEST['targetModule'])) {
            $toAddressIds = explode(',', rtrim($_REQUEST['ids'], ','));
            foreach ($toAddressIds as $id) {
                $destinataryBean = BeanFactory::getBean($_REQUEST['targetModule'], $id);
                if ($destinataryBean) {
                    $idLine = '<input type="hidden" class="email-compose-view-to-list" ';
                    $idLine .= 'data-record-module="' . $_REQUEST['targetModule'] . '" ';
                    $idLine .= 'data-record-id="' . $id . '" ';
                    $idLine .= 'data-record-name="' . $destinataryBean->name . '" ';
                    $idLine .= 'data-record-email="' . $destinataryBean->email1 . '">';
                    echo $idLine;
                }
            }
        }
        if (isset($_REQUEST['relatedModule']) && isset($_REQUEST['relatedId'])) {
            $relateBean = BeanFactory::getBean($_REQUEST['relatedModule'], $_REQUEST['relatedId']);
            $relateLine = '<input type="hidden" class="email-relate-target" ';
            $relateLine .= 'data-relate-module="' . $_REQUEST['relatedModule'] . '" ';
            $relateLine .= 'data-relate-id="' . $_REQUEST['relatedId'] . '" ';
            $relateLine .= 'data-relate-name="' . $relateBean->name . '">';
            echo $relateLine;
        }
    }

    public function action_QuickCreate(){
        $this->view = 'ajax';
        $originModule = $_REQUEST['module'];
        $targetModule = $_REQUEST['quickCreateModule'];

        $_REQUEST['module'] = $targetModule;

        $controller = ControllerFactory::getController($targetModule);
        $controller->loadBean();
        $controller->pre_save();
        $controller->action_save();
        $bean = $controller->bean;

        $_REQUEST['module'] = $originModule;

        if (!$bean) {
            $result = ['id' => false];
            echo json_encode($result);
            return;
        }

        $result = [
            'id' => $bean->id,
            'module' => $bean->module_name,
        ];
        echo json_encode($result);

        if (empty($_REQUEST['parentEmailRecordId'])) {
            return;
        }
        $emailBean = BeanFactory::getBean('Emails', $_REQUEST['parentEmailRecordId']);
        if (!$emailBean) {
            return;
        }

        $relationship = strtolower($controller->module);
        $emailBean->load_relationship($relationship);
        $emailBean->$relationship->add($bean->id);

        if (!$bean->load_relationship('emails')) {
            return;
        }

        $bean->emails->add($emailBean->id);
    }

    /**
     * @see EmailsViewSendemail
     */
    public function action_send() {
        
        global $current_user;
        global $app_strings;

        $request = $_REQUEST;

        $this->bean = $this->bean->populateBeanFromRequest($this->bean, $request);
        $inboundEmailAccount = new InboundEmail();
        $inboundEmailAccount->retrieve($_REQUEST['inbound_email_id']);

        if ($this->userIsAllowedToSendEmail($current_user, $inboundEmailAccount, $this->bean)) {
            $this->bean->save();

            $this->bean->handleMultipleFileAttachments();

        // parse and replace bean variables
        $this->bean = $this->replaceEmailVariables($this->bean, $request);

        if ($this->send1()) {
            $this->bean->status = 'sent';
            $this->bean->save();
        } else {
            // Don't save status if the email is a draft.
                // We need to ensure that drafts will still show
                // in the list view
                if ($this->bean->status !== 'draft') {
                    $this->bean->save();
                }
                $this->bean->status = 'send_error';
        }

            $this->view = 'sendemail';
        } else {
            $GLOBALS['log']->security(
                'User ' . $current_user->name .
                ' attempted to send an email using incorrect email account settings in' .
                ' which they do not have access to.'
            );

            $this->view = 'ajax';
            $response['errors'] = [
                'type' => get_class($this->bean),
                'id' => $this->bean->id,
                'title' => $app_strings['LBL_EMAIL_ERROR_SENDING']
            ];
            echo json_encode($response);
            // log out the user
            session_destroy();
        }
    }//end of method
    
    protected function replaceEmailVariables(Email $email, $request) {
        // request validation before replace bean variables

        if ($this->isValidRequestForReplaceEmailVariables($request)) {

            $macro_nv = array();

            $focusName = $request['parent_type'];
            $focus = BeanFactory::getBean($focusName, $request['parent_id']);
            if ($email->module_dir == 'Accounts') {
                $focusName = 'Accounts';
            }

            /**
             * @var EmailTemplate $emailTemplate
             */
            $emailTemplate = BeanFactory::getBean(
                'EmailTemplates',
                isset($request['emails_email_templates_idb']) ?
                    $request['emails_email_templates_idb'] :
                    null
            );
            $templateData = $emailTemplate->parse_email_template(
                array(
                    'subject' => $email->name,
                    'body_html' => $email->description_html,
                    'body' => $email->description,
                ),
                $focusName,
                $focus,
                $macro_nv
            );

            $email->name = $templateData['subject'];
            $email->description_html = $templateData['body_html'];
            $email->description = $templateData['body'];
        } else {
            $this->log('Email variables is not replaced because an invalid request.');
        }


        return $email;
    }//end of method

    /**
     * Request validation before replace bean variables,
     * see log to check validation problems
     *
     * @param array $request
     * @return bool
     */
    protected function isValidRequestForReplaceEmailVariables($request) {
        $isValidRequestForReplaceEmailVariables = true;
        if (!is_array($request)) {

            // request should be an array like standard $_REQUEST

            $isValidRequestForReplaceEmailVariables = false;
            $this->log('Incorrect request format');
        }
        if (!isset($request['parent_type']) || !$request['parent_type']) {

            // there is no any selected option in 'Related To' field
            // so impossible to replace variables to selected bean data

            $isValidRequestForReplaceEmailVariables = false;
            $this->log('There isn\'t any selected BEAN-TYPE option in \'Related To\' dropdown');
        }
        if (!isset($request['parent_id']) || !$request['parent_id']) {

            // there is no any selected bean in 'Related To' field
            // so impossible to replace variables to selected bean data

            $isValidRequestForReplaceEmailVariables = false;
            $this->log('There isn\'t any selected BEAN-ELEMENT in \'Related To\' field');
        }
        return $isValidRequestForReplaceEmailVariables;
    }//end of method

    /**
     * Add a message to log
     *
     * @param string $msg
     * @param string $level
     */
    private function log($msg, $level = 'info') {
        $GLOBALS['log']->$level($msg);
    }

    /**
     * @see EmailsViewCompose
     */
    public function action_SaveDraft(){
        $this->bean = $this->bean->populateBeanFromRequest($this->bean, $_REQUEST);
        $this->bean->status = 'draft';
        $this->bean->save();
        $this->bean->handleMultipleFileAttachments();
        $this->view = 'savedraftemail';
    }

    /**
     * @see EmailsViewCompose
     */
    public function action_DeleteDraft() {
        $this->bean->deleted = '1';
        $this->bean->status = 'draft';
        $this->bean->save();
        $this->view = 'deletedraftemail';
    }


    /**
     * @see EmailsViewPopup
     */
    public function action_Popup() {
        $this->view = 'popup';
    }

    /**
     * Gets the values of the "from" field
     * includes the signatures for each account
     */
   public function action_getFromFields() {
        global $current_user;
        $email = new Email();
        $email->email2init();
        $ie = new InboundEmail();
        $ie->email = $email;
        $accounts = $ieAccountsFull = $ie->retrieveAllByGroupIdWithGroupAccounts($current_user->id);
        $accountSignatures = $current_user->getPreference('account_signatures', 'Emails');
        $showFolders = unserialize(base64_decode($current_user->getPreference('showFolders', 'Emails')));
        if ($accountSignatures != null) {
            $emailSignatures = unserialize(base64_decode($accountSignatures));
        } else {
            $GLOBALS['log']->warn('User ' . $current_user->name . ' does not have a signature');
        }

        $defaultEmailSignature = $current_user->getDefaultSignature();
        if (empty($defaultEmailSignature)) {
            $defaultEmailSignature = array(
                'html' => '<br>',
                'plain' => '\r\n',
            );
            $defaultEmailSignature['no_default_available'] = true;
        } else {
            $defaultEmailSignature['no_default_available'] = false;
        }

        $prependSignature = $current_user->getPreference('signature_prepend');

        $data = array();
        foreach ($accounts as $inboundEmailId => $inboundEmail) {
            if(in_array($inboundEmail->id, $showFolders)) {
                $storedOptions = unserialize(base64_decode($inboundEmail->stored_options));
                $isGroupEmailAccount = $inboundEmail->isGroupEmailAccount();
                $isPersonalEmailAccount = $inboundEmail->isPersonalEmailAccount();

                $oe = new OutboundEmail();
                $oe->retrieve($storedOptions['outbound_email']);
                
                $dataAddress = array(
                    'type' => $inboundEmail->module_name,
                    'id' => $inboundEmail->id,
                    'attributes' => array(
                        'reply_to' => $storedOptions['reply_to_addr'],
                        'name' => $storedOptions['from_name'],
                        'from' => $storedOptions['from_addr'],
                    ),
                    'prepend' => $prependSignature,
                    'isPersonalEmailAccount' => $isPersonalEmailAccount,
                    'isGroupEmailAccount' => $isGroupEmailAccount,
                    'outboundEmail' => array(
                        'id' => $oe->id,
                        'name' => $oe->name,
                    ),
                );

                // Include signature
                if (isset($emailSignatures[$inboundEmail->id]) && !empty($emailSignatures[$inboundEmail->id])) {
                    $emailSignatureId = $emailSignatures[$inboundEmail->id];
                } else {
                    $emailSignatureId = '';
                }

                $signature = $current_user->getSignature($emailSignatureId);
                if (!$signature) {

                    if ($defaultEmailSignature['no_default_available'] === true) {
                        $dataAddress['emailSignatures'] = $defaultEmailSignature;
                    } else {
                        $dataAddress['emailSignatures'] = array(
                            'html' => utf8_encode(html_entity_decode($defaultEmailSignature['signature_html'])),
                            'plain' => $defaultEmailSignature['signature'],
                        );
                    }
                } else {
                    $dataAddress['emailSignatures'] = array(
                        'html' => utf8_encode(html_entity_decode($signature['signature_html'])),
                        'plain' => $signature['signature'],
                    );
                }

                $data[] = $dataAddress;
            }
        }
        $user_id  = $current_user->id;
        $query = "SELECT enable FROM vi_multiple_smtp_settings";
        $result = $GLOBALS['db']->query($query);
        $a = $GLOBALS['db']->fetchByAssoc($result);
        $enable = $a['enable'];
        if($enable == 1) {
            $sel = "SELECT * FROM vi_st_multiple_smtp WHERE user_id ='$user_id'";
            $res = $GLOBALS['db']->query($sel);
            $row = $GLOBALS['db']->fetchByAssoc($res);
            if($row == NULL){
            $sel = "SELECT * FROM vi_st_multiple_smtp WHERE user_id ='1'";
            $res = $GLOBALS['db']->query($sel);
            $row = $GLOBALS['db']->fetchByAssoc($res);
            }
        } else {
            $sel = "SELECT * FROM outbound_email WHERE type = 'system'";
            $res = $GLOBALS['db']->query($sel);
            $row = $GLOBALS['db']->fetchByAssoc($res); 
        }
       
        $data[] = array(
                'type' => 'user',
                'id' => $row['id'],
                'attributes' => array(
                    'reply_to' => $current_user->full_name . ' &lt;' . $row['smtp_from_addr']. '&gt;',
                    'from' => $current_user->full_name . ' &lt;' . $row['smtp_from_addr'] . '&gt;',
                    'name' => $row['smtp_from_name'],
                    'oe' => $row['mail_smtpuser'],
                ),
                'prepend' => false,
                'isPersonalEmailAccount' => true,
                'isGroupEmailAccount' => $row['id'],
                'outboundEmail' => array(
                    'id' => $row['id'],
                    'name' => $row['name'],
                ),
                'emailSignatures' => $defaultEmailSignature,
            );
        $dataEncoded = json_encode(array('data' => $data), JSON_UNESCAPED_UNICODE);
        echo utf8_decode($dataEncoded);

        $this->view = 'ajax';
    }

    

    /**
     * Returns attachment data to ajax call
     */
    public function action_GetDraftAttachmentData() {
        $data['attachments'] = array();

        if(!empty($_REQUEST['id'])){
            $bean = BeanFactory::getBean('Emails', $_REQUEST['id']);
            $data['draft'] = $bean->status == 'draft' ? 1 : 0;
            $attachmentBeans = BeanFactory::getBean('Notes')
                ->get_full_list('', "parent_id = '" . $_REQUEST['id'] . "'");
            foreach($attachmentBeans as $attachmentBean) {
                $data['attachments'][] = array(
                    'id' => $attachmentBean->id,
                    'name' => $attachmentBean->name,
                    'file_mime_type' => $attachmentBean->file_mime_type,
                    'filename' => $attachmentBean->filename,
                    'parent_type' => $attachmentBean->parent_type,
                    'parent_id' => $attachmentBean->parent_id,
                    'description' => $attachmentBean->description,
                );
            }
        }

        $dataEncoded = json_encode(array('data' => $data), JSON_UNESCAPED_UNICODE);
        echo utf8_decode($dataEncoded);
        $this->view = 'ajax';
    }

    public function action_CheckEmail() {
        $inboundEmail = new InboundEmail();
        $inboundEmail->syncEmail();

        echo json_encode(array('response' => array()));
        $this->view = 'ajax';
    }

    /**
     * Used to list folders in the list view
     */
    public function action_GetFolders() {
        require_once 'include/SugarFolders/SugarFolders.php';
        global $current_user, $mod_strings;
        $email = new Email();
        $email->email2init();
        $ie = new InboundEmail();
        $ie->email = $email;
        $GLOBALS['log']->debug('********** EMAIL 2.0 - Asynchronous - at: refreshSugarFolders');
        $rootNode = new ExtNode('', '');
        $folderOpenState = $current_user->getPreference('folderOpenState', 'Emails');
        $folderOpenState = empty($folderOpenState) ? '' : $folderOpenState;

        try {
            $ret = $email->et->folder->getUserFolders($rootNode, sugar_unserialize($folderOpenState), $current_user,
                true);
            $out = json_encode(array('response' => $ret));
        } catch (SugarFolderEmptyException $e) {
            $GLOBALS['log']->warn($e->getMessage());
            $out = json_encode(array('errors' => array($mod_strings['LBL_ERROR_NO_FOLDERS'])));
        }

        echo $out;
        $this->view = 'ajax';
    }


    /**
     * @see EmailsViewDetailnonimported
     */
    public function action_DisplayDetailView() {
        global $db;
        $emails = BeanFactory::getBean("Emails");
        
        $inboundEmailRecordIdQuoted = $db->quote($_REQUEST['inbound_email_record']);
        $uidQuoted = $db->quote($_REQUEST['uid']);
        
        $result = $emails->get_full_list('', "mailbox_id = '" . $inboundEmailRecordIdQuoted . "' AND uid = '" . $uidQuoted . "'");

        if (empty($result)) {
            $this->view = 'detailnonimported';
        } else {
            header('location:index.php?module=Emails&action=DetailView&record=' . $result[0]->id);
        }
    }

    /**
     * @see EmailsViewDetailnonimported
     */
    public function action_ImportAndShowDetailView() {
        global $db;
        if (isset($_REQUEST['inbound_email_record']) && !empty($_REQUEST['inbound_email_record'])) {
            $inboundEmail = new InboundEmail();
            $inboundEmail->retrieve($db->quote($_REQUEST['inbound_email_record']), true, true);
            $inboundEmail->connectMailserver();
            $importedEmailId = $inboundEmail->returnImportedEmail($_REQUEST['msgno'], $_REQUEST['uid']);


            // Set the fields which have been posted in the request
            $this->bean = $this->setAfterImport($importedEmailId, $_REQUEST);

            if ($importedEmailId !== false) {
                header('location:index.php?module=Emails&action=DetailView&record=' . $importedEmailId);
            }
        } else {
            // When something fail redirect user to index
            header('location:index.php?module=Emails&action=index');
        }
    }

    /**
     * @see EmailsViewImport
     */
    public function action_ImportView() {
        $this->view = 'import';
    }

    public function action_GetCurrentUserID() {
        global $current_user;
        echo json_encode(array("response" => $current_user->id));
        $this->view = 'ajax';
    }

    public function action_ImportFromListView() {
        global $db;
        if (isset($_REQUEST['inbound_email_record']) && !empty($_REQUEST['inbound_email_record'])) {
            $inboundEmail = BeanFactory::getBean('InboundEmail', $db->quote($_REQUEST['inbound_email_record']));
            if (isset($_REQUEST['folder']) && !empty($_REQUEST['folder'])) {
                $inboundEmail->mailbox = $_REQUEST['folder'];
            }
            $inboundEmail->connectMailserver();

            if (isset($_REQUEST['all']) && $_REQUEST['all'] === 'true') {
                // import all in folder
                $importedEmailsId = $inboundEmail->importAllFromFolder();
                foreach ($importedEmailsId as $importedEmailId) {
                    $this->bean = $this->setAfterImport($importedEmailId, $_REQUEST);
                }
            } else {
                foreach ($_REQUEST['uid'] as $uid) {
                    $importedEmailId = $inboundEmail->returnImportedEmail($_REQUEST['msgno'], $uid);
                    $this->bean = $this->setAfterImport($importedEmailId, $_REQUEST);
                }
            }

        } else {
            $GLOBALS['log']->fatal('EmailsController::action_ImportFromListView() missing inbound_email_record');
        }

        header('location:index.php?module=Emails&action=index');
    }

    public function action_ReplyTo() {
        $this->composeBean($_REQUEST, self::COMPOSE_BEAN_MODE_REPLY_TO);
        $this->view = 'compose';
    }

    public function action_ReplyToAll() {
        $this->composeBean($_REQUEST, self::COMPOSE_BEAN_MODE_REPLY_TO_ALL);
        $this->view = 'compose';
    }

    public function action_Forward() {
        $this->composeBean($_REQUEST, self::COMPOSE_BEAN_MODE_FORWARD);
        $this->view = 'compose';
    }

    /**
     * Fills compose view body with the output from PDF Template
     * @see sendEmail::send_email()
     */
    public function action_ComposeViewWithPdfTemplate() {
        $this->composeBean($_REQUEST, self::COMPOSE_BEAN_WITH_PDF_TEMPLATE);
        $this->view = 'compose';
    }

    public function action_SendDraft() {
        $this->view = 'ajax';
        echo json_encode(array());
    }


    /**
     * @throws SugarControllerException
     */
    public function action_MarkEmails() {
        $this->markEmails($_REQUEST);
        echo json_encode(array('response' => true));
        $this->view = 'ajax';
    }

    /**
     * @param array $request
     * @throws SugarControllerException
     */
    public function markEmails($request) {
        // validate the request

        if (!isset($request['inbound_email_record']) || !$request['inbound_email_record']) {
            throw new SugarControllerException('No Inbound Email record in request');
        }

        if (!isset($request['folder']) || !$request['folder']) {
            throw new SugarControllerException('No Inbound Email folder in request');
        }

        // connect to requested inbound email server
        // and select the folder

        $ie = $this->getInboundEmail($request['inbound_email_record']);
        $ie->mailbox = $request['folder'];
        $ie->connectMailserver();

        // get requested UIDs and flag type

        $UIDs = $this->getRequestedUIDs($request);
        $type = $this->getRequestedFlagType($request);

        // mark emails
        $ie->markEmails($UIDs, $type);
    }

    public function composeBean($request, $mode = self::COMPOSE_BEAN_MODE_UNDEFINED) {

        if ($mode === self::COMPOSE_BEAN_MODE_UNDEFINED) {
            throw new InvalidArgumentException('EmailController::composeBean $mode argument is COMPOSE_BEAN_MODE_UNDEFINED');
        }

        global $db;
        global $mod_strings;

                
        global $current_user;
        $email = new Email();
        $email->email2init();
        $ie = new InboundEmail();
        $ie->email = $email;
        $accounts = $ieAccountsFull = $ie->retrieveAllByGroupIdWithGroupAccounts($current_user->id);
        if(!$accounts) {
            $url = 'index.php?module=Users&action=EditView&record=' . $current_user->id . "&showEmailSettingsPopup=1";
            SugarApplication::appendErrorMessage(
                    "You don't have any valid email account settings yet. <a href=\"$url\">Click here to set your email accounts.</a>");
        }
        
        
        if (isset($request['record']) && !empty($request['record'])) {
            $this->bean->retrieve($request['record']);
        } else {
            $inboundEmail = BeanFactory::getBean('InboundEmail', $db->quote($request['inbound_email_record']));
            $inboundEmail->connectMailserver();
            $importedEmailId = $inboundEmail->returnImportedEmail($request['msgno'], $request['uid']);
            $this->bean->retrieve($importedEmailId);
        }

        $_REQUEST['return_module'] = 'Emails';
        $_REQUEST['return_Action'] = 'index';

        if ($mode === self::COMPOSE_BEAN_MODE_REPLY_TO || $mode === self::COMPOSE_BEAN_MODE_REPLY_TO_ALL) {
            // Move email addresses from the "from" field to the "to" field
            $this->bean->to_addrs = $this->bean->from_addr;
            $this->bean->to_addrs_names = $this->bean->from_addr_name;
        } else {
            if ($mode === self::COMPOSE_BEAN_MODE_FORWARD) {
                $this->bean->to_addrs = '';
                $this->bean->to_addrs_names = '';
            } else {
                if ($mode === self::COMPOSE_BEAN_WITH_PDF_TEMPLATE) {
                    // Get Related To Field
                    // Populate to
                }
            }
        }

        if ($mode !== self::COMPOSE_BEAN_MODE_REPLY_TO_ALL) {
            $this->bean->cc_addrs_arr = array();
            $this->bean->cc_addrs_names = '';
            $this->bean->cc_addrs = '';
            $this->bean->cc_addrs_ids = '';
            $this->bean->cc_addrs_emails = '';
        }

        if ($mode === self::COMPOSE_BEAN_MODE_REPLY_TO || $mode === self::COMPOSE_BEAN_MODE_REPLY_TO_ALL) {
            // Add Re to subject
            $this->bean->name = $mod_strings['LBL_RE'] . $this->bean->name;
        } else {
            if ($mode === self::COMPOSE_BEAN_MODE_FORWARD) {
                // Add FW to subject
                $this->bean->name = $mod_strings['LBL_FW'] . $this->bean->name;
            }
        }

        if (empty($this->bean->name)) {
            $this->bean->name = $mod_strings['LBL_NO_SUBJECT'] . $this->bean->name;
        }

        // Move body into original message
        if (!empty($this->bean->description_html)) {
            $this->bean->description = '<br>' . $mod_strings['LBL_ORIGINAL_MESSAGE_SEPERATOR'] . '<br>' .
                $this->bean->description_html;
        } else {
            if (!empty($this->bean->description)) {
                $this->bean->description = PHP_EOL . $mod_strings['LBL_ORIGINAL_MESSAGE_SEPERATOR'] . PHP_EOL .
                    $this->bean->description;
            }
        }
    }


    /**
     * @param $request
     * @return null|string
     */
    private function getRequestedUIDs($request) {
        $ret = $this->getRequestedArgument($request, 'uid');
        if (is_array($ret)) {
            $ret = implode(',', $ret);
        }

        return $ret;
    }

    /**
     * @param array $request
     * @return null|mixed
     */
    private function getRequestedFlagType($request) {
        $ret = $this->getRequestedArgument($request, 'type');

        return $ret;
    }

    /**
     * @param array $request
     * @param string $key
     * @return null|mixed
     */
    private function getRequestedArgument($request, $key)
    {
        if (!isset($request[$key])) {
            $GLOBALS['log']->error("Requested key is not set: ");

            return null;
        }

        return $request[$key];
    }
    private function getInboundEmail($record)
    {
        $db = DBManagerFactory::getInstance();
        $ie = BeanFactory::getBean('InboundEmail', $db->quote($record));
        if (!$ie) {
            throw new SugarControllerException("BeanFactory can't resolve an InboundEmail record: $record");
        }

        return $ie;
    }
    protected function setAfterImport($importedEmailId, $request)
    {
        $emails = BeanFactory::getBean("Emails", $importedEmailId);
        foreach ($request as $requestKey => $requestValue) {
            if (strpos($requestKey, 'SET_AFTER_IMPORT_') !== false) {
                $field = str_replace('SET_AFTER_IMPORT_', '', $requestKey);
                if (in_array($field, self::$doNotImportFields)) {
                    continue;
                }

                $emails->{$field} = $requestValue;
            }
        }

        $emails->save();

        return $emails;
    }
    protected function userIsAllowedToSendEmail($requestedUser, $requestedInboundEmail, $requestedEmail)
    {
        // Check that user is allowed to use inbound email account
        $hasAccessToInboundEmailAccount = false;
        $usersInboundEmailAccounts = $requestedInboundEmail->retrieveAllByGroupIdWithGroupAccounts($requestedUser->id);
        foreach ($usersInboundEmailAccounts as $inboundEmailId => $userInboundEmail) {
            if ($userInboundEmail->id === $requestedInboundEmail->id) {
                $hasAccessToInboundEmailAccount = true;
                break;
            }
        }

        $inboundEmailStoredOptions = $requestedInboundEmail->getStoredOptions();

        // if group email account, check that user is allowed to use group email account
        if ($requestedInboundEmail->isGroupEmailAccount()) {
            if ($inboundEmailStoredOptions['allow_outbound_group_usage'] === true) {
                $hasAccessToInboundEmailAccount = true;
            } else {
                $hasAccessToInboundEmailAccount = false;
            }
        }

        // Check that the from address is the same as the inbound email account
        $isFromAddressTheSame = false;
        if ($inboundEmailStoredOptions['from_addr'] === $requestedEmail->from_addr) {
            $isFromAddressTheSame = true;
        }
        $outboundEmailAccount = new OutboundEmail();
        if(empty($inboundEmailStoredOptions['outbound_email'])) {
            $outboundEmailAccount->getSystemMailerSettings();
        } else {
            $outboundEmailAccount->retrieve($inboundEmailStoredOptions['outbound_email']);
        }

        $isAllowedToUseOutboundEmail = false;
        if ($outboundEmailAccount->type === 'system') {
            if($outboundEmailAccount->isAllowUserAccessToSystemDefaultOutbound()) {
                $isAllowedToUseOutboundEmail = true;
            }

            // When there are not any authentication details for the system account, allow the user to use the system
            // email account.
            if($outboundEmailAccount->mail_smtpauth_req == 0) {
                $isAllowedToUseOutboundEmail = true;
            }

            $admin = new Administration();
            $admin->retrieveSettings();
            $adminNotifyFromAddress = $admin->settings['notify_fromaddress'];
            if ($adminNotifyFromAddress === $requestedEmail->from_addr) {
                $isFromAddressTheSame = true;
            }
        } else if ($outboundEmailAccount->type === 'user') {
            $isAllowedToUseOutboundEmail = true;
        }

        // The inbound email account is an empty object, we assume the user has access
        if (empty($requestedInboundEmail->id)) {
            $hasAccessToInboundEmailAccount = true;
            $isFromAddressTheSame = true;
        }

        return $hasAccessToInboundEmailAccount === true &&
            $isFromAddressTheSame === true &&
            $isAllowedToUseOutboundEmail === true;
    }
    public function send1()
    {
        global $mod_strings, $app_strings;
        global $current_user;
        global $sugar_config;
        global $locale;
        $OBCharset = $locale->getPrecedentPreference('default_email_charset');
        $mail = new SugarPHPMailer();

        foreach ($this->bean->to_addrs_arr as $addr_arr) {
            if (empty($addr_arr['display'])) {
                $mail->AddAddress($addr_arr['email'], "");
            } else {
                $mail->AddAddress($addr_arr['email'],
                    $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset));
            }
        }
        foreach ($this->bean->cc_addrs_arr as $addr_arr) {
            if (empty($addr_arr['display'])) {
                $mail->AddCC($addr_arr['email'], "");
            } else {
                $mail->AddCC($addr_arr['email'],
                    $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset));
            }
        }

        foreach ($this->bean->bcc_addrs_arr as $addr_arr) {
            if (empty($addr_arr['display'])) {
                $mail->AddBCC($addr_arr['email'], "");
            } else {
                $mail->AddBCC($addr_arr['email'],
                    $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset));
            }
        }

        $ieId = $this->bean->mailbox_id;
        $query = "SELECT enable FROM vi_multiple_smtp_settings";
        $result = $GLOBALS['db']->query($query);
        $a = $GLOBALS['db']->fetchByAssoc($result);
        $enable = $a['enable'];
        if($enable == 1) {
            $mail = $this->custom_setMailer($mail, '', $ieId);
        } else {
            $mail = $this->bean->setMailer($mail, '', $ieId);
        }
       

        // FROM ADDRESS
        if (!empty($this->bean->from_addr)) {
            $mail->From = $this->bean->from_addr;
        } else {
            $mail->From = $current_user->getPreference('mail_fromaddress');
            $this->bean->from_addr = $mail->From;
        }
        // FROM NAME
        if (!empty($this->bean->from_name)) {
            $mail->FromName = $this->bean->from_name;
        } elseif (!empty($this->bean->from_addr_name)) {
            $mail->FromName = $this->bean->from_addr_name;
        } else {
            $mail->FromName = $current_user->getPreference('mail_fromname');
            $this->bean->from_name = $mail->FromName;
        }

        //Reply to information for case create and autoreply.
        if (!empty($this->bean->reply_to_name)) {
            $ReplyToName = $this->bean->reply_to_name;
        } else {
            $ReplyToName = $mail->FromName;
        }
        if (!empty($this->bean->reply_to_addr)) {
            $ReplyToAddr = $this->bean->reply_to_addr;
        } else {
            $ReplyToAddr = $mail->From;
        }
        $mail->Sender = $mail->From; /* set Return-Path field in header to reduce spam score in emails sent via Sugar's Email module */
        $mail->AddReplyTo($ReplyToAddr, $locale->translateCharsetMIME(trim($ReplyToName), 'UTF-8', $OBCharset));

        //$mail->Subject = html_entity_decode($this->name, ENT_QUOTES, 'UTF-8');
        $mail->Subject = $this->bean->name;

        ///////////////////////////////////////////////////////////////////////
        ////    ATTACHMENTS
        foreach ($this->bean->saved_attachments as $note) {
            $mime_type = 'text/plain';
            if ($note->object_name == 'Note') {
                if (!empty($note->file->temp_file_location) && is_file($note->file->temp_file_location)) { // brandy-new file upload/attachment
                    $file_location = "file/" . $note->file->temp_file_location;
                    $filename = $note->file->original_file_name;
                    $mime_type = $note->file->mime_type;
                } else { // attachment coming from template/forward
                    $file_location = "upload/{$note->id}";
                    // cn: bug 9723 - documents from EmailTemplates sent with Doc Name, not file name.
                    $filename = !empty($note->filename) ? $note->filename : $note->name;
                    $mime_type = $note->file_mime_type;
                }
            } elseif ($note->object_name == 'DocumentRevision') { // from Documents
                $filePathName = $note->id;
                // cn: bug 9723 - Emails with documents send GUID instead of Doc name
                $filename = $note->getDocumentRevisionNameForDisplay();
                $file_location = "upload/$note->id";
                $mime_type = $note->file_mime_type;
            }

            // strip out the "Email attachment label if exists
            $filename = str_replace($mod_strings['LBL_EMAIL_ATTACHMENT'] . ': ', '', $filename);
            $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
            //is attachment in our list of bad files extensions?  If so, append .txt to file location
            //check to see if this is a file with extension located in "badext"
            foreach ($sugar_config['upload_badext'] as $badExt) {
                if (strtolower($file_ext) == strtolower($badExt)) {
                    //if found, then append with .txt to filename and break out of lookup
                    //this will make sure that the file goes out with right extension, but is stored
                    //as a text in db.
                    $file_location = $file_location . ".txt";
                    break; // no need to look for more
                }
            }
            $mail->AddAttachment($file_location, $locale->translateCharsetMIME(trim($filename), 'UTF-8', $OBCharset),
                'base64', $mime_type);

            // embedded Images
            if ($note->embed_flag == true) {
                $cid = $filename;
                $mail->AddEmbeddedImage($file_location, $cid, $filename, 'base64', $mime_type);
            }
        }
        ////    END ATTACHMENTS
        ///////////////////////////////////////////////////////////////////////

        $mail = $this->bean->handleBody($mail);

        $GLOBALS['log']->debug('Email sending --------------------- ');

        ///////////////////////////////////////////////////////////////////////
        ////    I18N TRANSLATION
        $mail->prepForOutbound();
        ////    END I18N TRANSLATION
        ///////////////////////////////////////////////////////////////////////

        if ($mail->Send()) {
            ///////////////////////////////////////////////////////////////////
            ////    INBOUND EMAIL HANDLING
            // mark replied
            if (!empty($_REQUEST['inbound_email_id'])) {
                $ieMail = new Email();
                $ieMail->retrieve($_REQUEST['inbound_email_id']);
                $ieMail->status = 'replied';
                $ieMail->save();
            }
            $GLOBALS['log']->debug(' --------------------- buh bye -- sent successful');
            ////    END INBOUND EMAIL HANDLING
            ///////////////////////////////////////////////////////////////////
            return true;
        }
        $GLOBALS['log']->debug($app_strings['LBL_EMAIL_ERROR_PREPEND'] . $mail->ErrorInfo);

        return false;
    }

    public function custom_setMailer($mail, $mailer_id = '', $ieId = '')
    {
        global $current_user;
        $user_id  = $current_user->id;
        $sel = "SELECT * FROM vi_st_multiple_smtp WHERE user_id ='$user_id'";
        $res = $GLOBALS['db']->query($sel);
        $row = $GLOBALS['db']->fetchByAssoc($res);
        if($row == NULL){
            $sel = "SELECT * FROM vi_st_multiple_smtp WHERE user_id ='1'";
            $res = $GLOBALS['db']->query($sel);
            $row = $GLOBALS['db']->fetchByAssoc($res);
        }
        // ssl or tcp - keeping outside isSMTP b/c a default may inadvertantly set ssl://
        $mail->protocol = ($row['mail_smtpssl']) ? "ssl://" : "tcp://";
       if ($row['mail_sendtype'] == "SMTP") {
            //Set mail send type information
            $mail->Mailer = "smtp";
            $mail->Host = $row['mail_smtpserver'];
            $mail->Port = $row['mail_smtpport'];
            if ($row['mail_smtpssl'] == 1) {
                $mail->SMTPSecure = 'ssl';
            } // if
            if ($row['mail_smtpssl'] == 2) {
                $mail->SMTPSecure = 'tls';
            } // if*/

            if ($row['mail_smtpauth_req']) {
                $mail->SMTPAuth = true;
                $mail->Username = $row['mail_smtpuser'];
                $mail->Password = $row['mail_smtppass'];
            }
        }
         else {
            $mail->Mailer = "sendmail";
        }

        return $mail;
    }
}
?>