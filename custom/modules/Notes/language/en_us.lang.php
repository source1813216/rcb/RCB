<?php
// created: 2021-06-18 14:06:19
$mod_strings = array (
  'LBL_ACCOUNTS' => 'Clients',
  'LBL_ACCOUNT_ID' => 'Client ID:',
  'LBL_CASES' => 'Case',
  'LBL_CASE_ID' => 'Case ID:',
  'LBL_CONTRACT' => 'Contract',
);