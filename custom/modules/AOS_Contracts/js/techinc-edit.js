// 
/* Company TechincGlobal
 Custom fields show hidden
 */
 // alert("fallowup_c_val")
 $( document ).ready(function() {
	  var approval_status_val= $("#approval_status_c option").eq($("#approval_status_c").prop("selectedIndex")).val();
	  var send_for_approval_c_row = $("[field='send_for_approval_c']").parent('.edit-view-row-item');
	  var approval_steps_c_row = $("[field='approval_steps_c']").parent('.edit-view-row-item');
	   var approval_steps_c_val=$("#approval_steps_c").val();
	   // ddm_approval
	   var ddm_approval_c_row_col=$("[field='ddm_approval_c']").parent('.edit-view-row-item');
	   var ddm_approval_title_c_row_col=$("[field='ddm_apv_title_c']").parent('.edit-view-row-item');
	   // management_approval
	   var ddm_management_approval_c_row_col=$("[field='mgmt_approval_c']").parent('.edit-view-row-item');
	   var mgmt_apv_titile_c_row_col=$("[field='mgmt_apv_title_c']").parent('.edit-view-row-item');
	   // ceo_office_approval
	   var ceo_office_approval_c_row_col=$("[field='ceo_approval_c']").parent('.edit-view-row-item');
	   var ceo_apv_title_c_row_col=$("[field='ceo_label_c']").parent('.edit-view-row-item');
	  
		var remarks_c_row_col=$("[field='response_c']").parent('.edit-view-row-item');
		var clarification_done_c_row_col=$("[field='clarificaiton_done_c']").parent('.edit-view-row-item');

		var ddm_remarks_c_row_col=$("[field='ddm_remarks_c']").parent('.edit-view-row-item');
		var management_remarks_c_row_col=$("[field='mgmt_remarks_c']").parent('.edit-view-row-item');
		var ceo_remarks_c_row_col=$("[field='ceo_remarks_c']").parent('.edit-view-row-item');

	  
	   approval_steps_c_row.attr("hidden",true);
	   remarks_c_row_col.hide();
	   clarification_done_c_row_col.hide();
	    ddm_remarks_c_row_col.hide();
		  management_remarks_c_row_col.hide();
		  ceo_remarks_c_row_col.hide();
	   var ddm_approval_c_val=$("#ddm_approval_c option:selected").val();
	   var management_approval_c_val=$("#management_approval_c option:selected").val();
	   //var ddm_approval_c_val=$("#approval_status option:selected").val();
	  //$("#approval_status option:selected").val()
	  
	 //alert(approval_status_val);
	 
	 $("[field='approval_status_c']").parent('.edit-view-row-item').attr("hidden",true)
	if(approval_status_val=="Pending" || approval_status_val=="Rejected" || approval_status_val=="DDM_Rejected" || approval_status_val=="Mgmt_Rejected" || approval_status_val=="CEO_Rejected"){
	}else{
		send_for_approval_c_row.attr("hidden",true);
	}
	

		if(approval_status_val=="Pending_DDM" ){
			ddm_management_approval_c_row_col.attr("hidden",true)
			mgmt_apv_titile_c_row_col.attr("hidden",true)
			ceo_office_approval_c_row_col.attr("hidden",true);
			ceo_apv_title_c_row_col.attr("hidden",true);
		}else if(approval_status_val=="Pending_Management"){
			inatalshowremark('ddm_remarks_c')
			inatalshowremark('management_remarks_c')
		ceo_office_approval_c_row_col.attr("hidden",true);
		ceo_apv_title_c_row_col.attr("hidden",true);
	}else if(approval_status_val=="Pending_CEO" || approval_status_val=="Approved" || approval_status_val=="Not Approved"){
		inatalshowremark('ddm_remarks_c')
			inatalshowremark('management_remarks_c')
			inatalshowremark('ceo_remarks_c');
		
	}else if(approval_status_val=="Rejected" ){
		if(ddm_approval_c_val=="Rejected"){
			inatalshowremark('ddm_remarks_c')
			ddm_management_approval_c_row_col.attr("hidden",true)
			mgmt_apv_titile_c_row_col.attr("hidden",true)
			ceo_office_approval_c_row_col.attr("hidden",true);
		ceo_apv_title_c_row_col.attr("hidden",true);
		}else if(management_approval_c_val=="Rejected"){
			inatalshowremark('ddm_remarks_c')
			inatalshowremark('management_remarks_c')
			
			ceo_office_approval_c_row_col.attr("hidden",true);
		ceo_apv_title_c_row_col.attr("hidden",true);
		}
		else{
		}
		
	}
	else if(approval_status_val=="DDM_Clarificaiton"){
		clarification_done_c_row_col.show();
		inatalshowremark('ddm_remarks_c')
		ddm_management_approval_c_row_col.attr("hidden",true)
			mgmt_apv_titile_c_row_col.attr("hidden",true)
			ceo_office_approval_c_row_col.attr("hidden",true);
		ceo_apv_title_c_row_col.attr("hidden",true);
	}
	else if(approval_status_val=="DCEO_Clarification"){
		inatalshowremark('ddm_remarks_c')
			inatalshowremark('management_remarks_c')
		clarification_done_c_row_col.show();
			ceo_office_approval_c_row_col.attr("hidden",true);
		ceo_apv_title_c_row_col.attr("hidden",true);
	}
	else if(approval_status_val=="CEO_Clarification"){
		inatalshowremark('ddm_remarks_c')
			inatalshowremark('management_remarks_c')
			inatalshowremark('ceo_remarks_c');
		clarification_done_c_row_col.show();
	}
		else if(approval_status_val=="DDM_Rejected"){
			inatalshowremark('ddm_remarks_c')
		ddm_management_approval_c_row_col.attr("hidden",true)
			mgmt_apv_titile_c_row_col.attr("hidden",true)
			ceo_office_approval_c_row_col.attr("hidden",true);
		ceo_apv_title_c_row_col.attr("hidden",true);
	}
	else if(approval_status_val=="DCEO_Rejected"){
		inatalshowremark('ddm_remarks_c')
			inatalshowremark('management_remarks_c')
			ceo_office_approval_c_row_col.attr("hidden",true);
		ceo_apv_title_c_row_col.attr("hidden",true);
	}
	else if(approval_status_val=="CEO_Rejected"){
		inatalshowremark('ddm_remarks_c')
			inatalshowremark('management_remarks_c')
			inatalshowremark('ceo_remarks_c');

	}
	else{
			ddm_approval_c_row_col.attr("hidden",true)
			ddm_approval_title_c_row_col.attr("hidden",true)
			ddm_management_approval_c_row_col.attr("hidden",true)
			mgmt_apv_titile_c_row_col.attr("hidden",true)
			ceo_office_approval_c_row_col.attr("hidden",true);
			ceo_apv_title_c_row_col.attr("hidden",true);
	}
		
	
	
	
	$('#clarificaiton_done_c').click(function(){
			var formId ='EditView';
			var value={"field" :'response_c'};
            if($(this).prop("checked") == true){
                //console.log("Checkbox is checked.");
				 remarks_c_row_col.show();
				 addToValidate('EditView','response_c','',true,'Remarks Field is required.');
				 var requiredClass = $("#"+formId+" div[field='"+value.field+"']").prev().find('span').attr('class');
 if(requiredClass != 'required'){
    $("#"+formId+" div[field='"+value.field+"']").prev().append('<span class="required">*</span>');    
}
            }
            else if($(this).prop("checked") == false){
				removeFromValidate('EditView','response_c');
				 remarks_c_row_col.hide();
                //console.log("Checkbox is unchecked.");
            }
        });
		
		$("#ddm_approval_c").change(function() {
			showhideremark('ddm_remarks_c')
		})
		
		$("#mgmt_approval_c").change(function() {
			showhideremark('mgmt_remarks_c')
		})
		$("#ceo_approval_c").change(function() {
			showhideremark('ceo_remarks_c')
		})
		
		//aditianal functions   
			function showhideremark(feild){
			
				var formId ='EditView';
			var value={"field" :feild};
				var status_checker;
				switch(feild) {
  case "ddm_remarks_c":
    // code block
	status_checker="ddm_approval_c";
    break;
  case "mgmt_remarks_c":
    // code block
	status_checker="mgmt_approval_c";
    break;
	 case "ceo_remarks_c":
    // code block
	status_checker="ceo_approval_c";
    break;
  default:
    // code block
}

		var status_checker=$("#"+status_checker+" option:selected").val();
		if(status_checker==""){
			$("[field='"+feild+"']").parent('.edit-view-row-item').hide();
			removeFromValidate('EditView',feild);
			var requiredClass = $("#"+formId+" div[field='"+value.field+"']").prev().find('span').attr('class');
			console.log(requiredClass);
 if(requiredClass =='required'){
    $("#"+formId+" div[field='"+value.field+"']").prev().find('span').remove();    
}
		}else if(status_checker == "Approved"){
			removeFromValidate('EditView',feild);
				$("[field='"+feild+"']").parent('.edit-view-row-item').show();
				 var requiredClass = $("#"+formId+" div[field='"+value.field+"']").prev().find('span').attr('class');
				 console.log(requiredClass);
 if(requiredClass == 'required'){
    $("#"+formId+" div[field='"+value.field+"']").prev().find('span').remove();    
}
					
		}
		else{
			$("[field='"+feild+"']").parent('.edit-view-row-item').show();
			addToValidate(formId,value.field,'',true,'Remarks Field is required.');
				 var requiredClass = $("#"+formId+" div[field='"+value.field+"']").prev().find('span').attr('class');
 if(requiredClass != 'required'){
    $("#"+formId+" div[field='"+value.field+"']").prev().append('<span class="required">*</span>');    
}
		}
			}
			
			function inatalshowremark(feild){
				var formId ='EditView';
			var value={"field" :feild};
				var status_checker;
				switch(feild) {
  case "ddm_remarks_c":
    // code block
	status_checker="ddm_approval_c";
    break;
  case "mgmt_remarks_c":
    // code block
	status_checker="mgmt_approval_c";
    break;
	 case "ceo_remarks_c":
    // code block
	status_checker="ceo_approval_c";
    break;
  default:
    // code block
}

		var status_checker=$("#"+status_checker+" option:selected").val();
		if(status_checker==""){
			$("[field='"+feild+"']").parent('.edit-view-row-item').hide();
			removeFromValidate('EditView',feild);
		}else if(status_checker == "Approved"){
			removeFromValidate('EditView',feild);
			if($("#"+feild).val()==""){
			}else{
				$("[field='"+feild+"']").parent('.edit-view-row-item').show();
			}		
		}
		else{
			$("[field='"+feild+"']").parent('.edit-view-row-item').show();
			addToValidate(formId,value.field,'',true,'Remarks Field is required.');
				 var requiredClass = $("#"+formId+" div[field='"+value.field+"']").prev().find('span').attr('class');
 if(requiredClass != 'required'){
    $("#"+formId+" div[field='"+value.field+"']").prev().append('<span class="required">*</span>');    
}
		}
			//alert(feild);
			}
  
  });