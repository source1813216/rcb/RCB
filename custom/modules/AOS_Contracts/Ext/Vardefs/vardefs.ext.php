<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-06-18 14:21:21
$dictionary["AOS_Contracts"]["fields"]["leads_aos_contracts_1"] = array (
  'name' => 'leads_aos_contracts_1',
  'type' => 'link',
  'relationship' => 'leads_aos_contracts_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_AOS_CONTRACTS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_aos_contracts_1leads_ida',
);
$dictionary["AOS_Contracts"]["fields"]["leads_aos_contracts_1_name"] = array (
  'name' => 'leads_aos_contracts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_AOS_CONTRACTS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_aos_contracts_1leads_ida',
  'link' => 'leads_aos_contracts_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["AOS_Contracts"]["fields"]["leads_aos_contracts_1leads_ida"] = array (
  'name' => 'leads_aos_contracts_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_aos_contracts_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_AOS_CONTRACTS_1_FROM_AOS_CONTRACTS_TITLE',
);


 // created: 2021-06-18 14:10:16
$dictionary['AOS_Contracts']['fields']['approval_end_date_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['approval_end_date_c']['labelValue']='Approval End Date';

 

 // created: 2021-07-01 12:03:52
$dictionary['AOS_Contracts']['fields']['approval_stamp_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['approval_stamp_c']['labelValue']='Approval Stamp';

 

 // created: 2021-06-18 14:10:04
$dictionary['AOS_Contracts']['fields']['approval_start_date_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['approval_start_date_c']['labelValue']='Approval Start Date';

 

 // created: 2021-06-18 14:10:31
$dictionary['AOS_Contracts']['fields']['approval_status_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['approval_status_c']['labelValue']='Approval Status';

 

 // created: 2021-06-23 07:40:03
$dictionary['AOS_Contracts']['fields']['clarificaiton_done_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['clarificaiton_done_c']['labelValue']='Clarificaiton Done';

 

 // created: 2021-06-23 05:55:14
$dictionary['AOS_Contracts']['fields']['contract_account']['inline_edit']=true;
$dictionary['AOS_Contracts']['fields']['contract_account']['merge_filter']='disabled';

 

 // created: 2021-06-18 14:12:21
$dictionary['AOS_Contracts']['fields']['ddm_approval_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['ddm_approval_c']['labelValue']='DDM Approve/Reject';

 

 // created: 2021-06-18 14:11:37
$dictionary['AOS_Contracts']['fields']['ddm_apv_by_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['ddm_apv_by_c']['labelValue']='DDM Approval By';

 

 // created: 2021-07-03 09:03:54
$dictionary['AOS_Contracts']['fields']['ddm_apv_title_c']['inline_edit']='';
$dictionary['AOS_Contracts']['fields']['ddm_apv_title_c']['labelValue']='1';

 

 // created: 2021-06-18 14:12:35
$dictionary['AOS_Contracts']['fields']['ddm_flag_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['ddm_flag_c']['labelValue']='DDM Flag';

 

 // created: 2021-06-18 14:12:56
$dictionary['AOS_Contracts']['fields']['ddm_remarks_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['ddm_remarks_c']['labelValue']='DDM Remarks';

 

 // created: 2021-06-18 14:13:18
$dictionary['AOS_Contracts']['fields']['ddm_response_date_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['ddm_response_date_c']['labelValue']='DDM Response Date';

 

 // created: 2021-06-18 14:13:34
$dictionary['AOS_Contracts']['fields']['ddm_stamp_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['ddm_stamp_c']['labelValue']='DDM Stamp';

 

 // created: 2021-06-23 05:52:29
$dictionary['AOS_Contracts']['fields']['end_date']['required']=true;
$dictionary['AOS_Contracts']['fields']['end_date']['inline_edit']=true;
$dictionary['AOS_Contracts']['fields']['end_date']['merge_filter']='disabled';

 

 // created: 2021-06-18 14:13:53
$dictionary['AOS_Contracts']['fields']['event_brief_c']['labelValue']='Event Brief';

 

 // created: 2021-06-18 14:14:03
$dictionary['AOS_Contracts']['fields']['event_date_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['event_date_c']['labelValue']='Event Date';

 

 // created: 2021-06-23 07:44:09
$dictionary['AOS_Contracts']['fields']['itinerary_c']['labelValue']='Itinerary';

 

 // created: 2021-06-18 14:16:11
$dictionary['AOS_Contracts']['fields']['member_1_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['member_1_c']['labelValue']='Member 1';

 

 // created: 2021-06-18 14:16:22
$dictionary['AOS_Contracts']['fields']['member_2_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['member_2_c']['labelValue']='Member 2';

 

 // created: 2021-06-18 14:16:33
$dictionary['AOS_Contracts']['fields']['member_3_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['member_3_c']['labelValue']='Member 3';

 

 // created: 2021-06-18 14:16:42
$dictionary['AOS_Contracts']['fields']['member_4_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['member_4_c']['labelValue']='Member 4';

 

 // created: 2021-06-18 14:16:53
$dictionary['AOS_Contracts']['fields']['member_5_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['member_5_c']['labelValue']='Member 5';

 

 // created: 2021-06-18 14:17:05
$dictionary['AOS_Contracts']['fields']['member_6_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['member_6_c']['labelValue']='Member 6';

 

 // created: 2021-07-01 11:07:43
$dictionary['AOS_Contracts']['fields']['member_label_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['member_label_c']['labelValue']='**';

 

 // created: 2021-06-18 14:17:20
$dictionary['AOS_Contracts']['fields']['memstamp1_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['memstamp1_c']['labelValue']='MemStamp1';

 

 // created: 2021-06-18 14:17:37
$dictionary['AOS_Contracts']['fields']['memstamp2_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['memstamp2_c']['labelValue']='MemStamp2';

 

 // created: 2021-06-18 14:18:28
$dictionary['AOS_Contracts']['fields']['memstamp3_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['memstamp3_c']['labelValue']='MemStamp3';

 

 // created: 2021-06-18 14:18:39
$dictionary['AOS_Contracts']['fields']['memstamp4_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['memstamp4_c']['labelValue']='MemStamp4';

 

 // created: 2021-06-18 14:18:50
$dictionary['AOS_Contracts']['fields']['memstamp5_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['memstamp5_c']['labelValue']='MemStamp5';

 

 // created: 2021-06-18 14:19:01
$dictionary['AOS_Contracts']['fields']['memstamp6_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['memstamp6_c']['labelValue']='MemStamp6';

 

 // created: 2021-06-18 14:20:02
$dictionary['AOS_Contracts']['fields']['mgmt_approval_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['mgmt_approval_c']['labelValue']='Management Approve/Reject';

 

 // created: 2021-06-18 14:14:44
$dictionary['AOS_Contracts']['fields']['mgmt_apv_by_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['mgmt_apv_by_c']['labelValue']='Management Approval By';

 

 // created: 2021-07-03 09:04:15
$dictionary['AOS_Contracts']['fields']['mgmt_apv_title_c']['inline_edit']='';
$dictionary['AOS_Contracts']['fields']['mgmt_apv_title_c']['labelValue']='2';

 

 // created: 2021-06-18 14:20:13
$dictionary['AOS_Contracts']['fields']['mgmt_flag_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['mgmt_flag_c']['labelValue']='MGMT Flag';

 

 // created: 2021-06-18 14:15:21
$dictionary['AOS_Contracts']['fields']['mgmt_remarks_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['mgmt_remarks_c']['labelValue']='Management Remarks';

 

 // created: 2021-06-18 14:15:44
$dictionary['AOS_Contracts']['fields']['mgmt_response_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['mgmt_response_c']['labelValue']='Management Response Date';

 

 // created: 2021-06-18 14:19:26
$dictionary['AOS_Contracts']['fields']['mgmt_stamp_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['mgmt_stamp_c']['labelValue']='Management Stamp';

 

 // created: 2021-06-18 14:20:30
$dictionary['AOS_Contracts']['fields']['prepared_by_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['prepared_by_c']['labelValue']='Prepared By';

 

 // created: 2021-06-23 07:40:23
$dictionary['AOS_Contracts']['fields']['response_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['response_c']['labelValue']='Response';

 

 // created: 2021-06-30 17:55:47
$dictionary['AOS_Contracts']['fields']['routine_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['routine_c']['labelValue']='Routine';

 

 // created: 2021-07-01 11:07:06
$dictionary['AOS_Contracts']['fields']['routine_label_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['routine_label_c']['labelValue']='*';

 

 // created: 2021-06-23 07:15:43
$dictionary['AOS_Contracts']['fields']['send_for_approval_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['send_for_approval_c']['labelValue']='Send for Approval';

 

 // created: 2021-06-30 17:55:47
$dictionary['AOS_Contracts']['fields']['srp_stdapproval_routine_id_c']['inline_edit']=1;

 

 // created: 2021-06-23 05:52:14
$dictionary['AOS_Contracts']['fields']['start_date']['required']=true;
$dictionary['AOS_Contracts']['fields']['start_date']['inline_edit']=true;
$dictionary['AOS_Contracts']['fields']['start_date']['merge_filter']='disabled';

 

 // created: 2021-06-18 14:17:37
$dictionary['AOS_Contracts']['fields']['user_id10_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:18:28
$dictionary['AOS_Contracts']['fields']['user_id11_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:18:39
$dictionary['AOS_Contracts']['fields']['user_id12_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:18:50
$dictionary['AOS_Contracts']['fields']['user_id13_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:19:01
$dictionary['AOS_Contracts']['fields']['user_id14_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:19:26
$dictionary['AOS_Contracts']['fields']['user_id15_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:20:30
$dictionary['AOS_Contracts']['fields']['user_id16_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:13:34
$dictionary['AOS_Contracts']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:14:44
$dictionary['AOS_Contracts']['fields']['user_id2_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:16:11
$dictionary['AOS_Contracts']['fields']['user_id3_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:16:22
$dictionary['AOS_Contracts']['fields']['user_id4_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:16:33
$dictionary['AOS_Contracts']['fields']['user_id5_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:16:42
$dictionary['AOS_Contracts']['fields']['user_id6_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:16:53
$dictionary['AOS_Contracts']['fields']['user_id7_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:17:05
$dictionary['AOS_Contracts']['fields']['user_id8_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:17:20
$dictionary['AOS_Contracts']['fields']['user_id9_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:11:37
$dictionary['AOS_Contracts']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2021-06-18 14:20:40
$dictionary['AOS_Contracts']['fields']['venue_c']['inline_edit']='1';
$dictionary['AOS_Contracts']['fields']['venue_c']['labelValue']='Venue';

 

 // created: 2021-06-18 14:20:40
$dictionary['AOS_Contracts']['fields']['vnp_venues_id_c']['inline_edit']=1;

 
?>