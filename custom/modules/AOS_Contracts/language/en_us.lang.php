<?php
// created: 2021-07-01 12:03:52
$mod_strings = array (
  'LBL_CONTRACT_ACCOUNT' => 'Client',
  'LBL_ACCOUNTS' => 'Clients',
  'LBL_AOS_QUOTES_AOS_CONTRACTS' => 'Initial Event Approval: Contracts',
  'LNK_NEW_RECORD' => 'Create Contract',
  'LNK_LIST' => 'View Site Inspection',
  'LBL_LIST_FORM_TITLE' => 'Site Inspection List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Site Inspection',
  'LBL_HOMEPAGE_TITLE' => 'My Site Inspection',
  'LBL_DDM_APV_TITLE' => '1',
  'LBL_MGMT_APV_TITLE' => '2',
  'LBL_APPROVAL_START_DATE' => 'Approval Start Date',
  'LBL_APPROVAL_END_DATE' => 'Approval End Date',
  'LBL_APPROVAL_STATUS' => 'Approval Status',
  'LBL_DDM_APV_BY_USER_ID' => '&#039;DDM Approval By&#039; (related &#039;User&#039; ID)',
  'LBL_DDM_APV_BY' => 'DDM Approval By',
  'LBL_DDM_APPROVAL' => 'DDM Approve/Reject',
  'LBL_DDM_FLAG' => 'DDM Flag',
  'LBL_DDM_REMARKS' => 'DDM Remarks',
  'LBL_DDM_RESPONSE_DATE' => 'DDM Response Date',
  'LBL_DDM_STAMP_USER_ID' => '&#039;DDM Stamp&#039; (related &#039;User&#039; ID)',
  'LBL_DDM_STAMP' => 'DDM Stamp',
  'LBL_EVENT_BRIEF' => 'Inspection Brief',
  'LBL_EVENT_DATE' => 'Event Date',
  'LBL_MGMT_APV_BY_USER_ID' => '&#039;Management Approval By&#039; (related &#039;User&#039; ID)',
  'LBL_MGMT_APV_BY' => 'Management Approval By',
  'LBL_MGMT_REMARKS' => 'Management Remarks',
  'LBL_MGMT_RESPONSE' => 'Management Response Date',
  'LBL_MEMBER_1_USER_ID' => '&#039;Member 1&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_1' => 'Member 1',
  'LBL_MEMBER_2_USER_ID' => '&#039;Member 2&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_2' => 'Member 2',
  'LBL_MEMBER_3_USER_ID' => '&#039;Member 3&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_3' => 'Member 3',
  'LBL_MEMBER_4_USER_ID' => '&#039;Member 4&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_4' => 'Member 4',
  'LBL_MEMBER_5_USER_ID' => '&#039;Member 5&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_5' => 'Member 5',
  'LBL_MEMBER_6_USER_ID' => '&#039;Member 6&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_6' => 'Member 6',
  'LBL_MEMSTAMP1_USER_ID' => '&#039;MemStamp1&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP1' => 'MemStamp1',
  'LBL_MEMSTAMP2_USER_ID' => '&#039;MemStamp2&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP2' => 'MemStamp2',
  'LBL_MEMSTAMP3_USER_ID' => '&#039;MemStamp3&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP3' => 'MemStamp3',
  'LBL_MEMSTAMP4_USER_ID' => '&#039;MemStamp4&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP4' => 'MemStamp4',
  'LBL_MEMSTAMP5_USER_ID' => '&#039;MemStamp5&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP5' => 'MemStamp5',
  'LBL_MEMSTAMP6_USER_ID' => '&#039;MemStamp6&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP6' => 'MemStamp6',
  'LBL_MGMT_STAMP_USER_ID' => '&#039;Management Stamp&#039; (related &#039;User&#039; ID)',
  'LBL_MGMT_STAMP' => 'Management Stamp',
  'LBL_MGMT_APPROVAL' => 'Management Approve/Reject',
  'LBL_MGMT_FLAG' => 'MGMT Flag',
  'LBL_PREPARED_BY_USER_ID' => '&#039;Prepared By&#039; (related &#039;User&#039; ID)',
  'LBL_PREPARED_BY' => 'Prepared By',
  'LBL_VENUE_VNP_VENUES_ID' => '&#039;Venue&#039; (related &#039;&#039; ID)',
  'LBL_VENUE' => 'Venue',
  'LBL_EDITVIEW_PANEL1' => 'Inspection Details',
  'LBL_START_DATE' => 'Start Date',
  'LBL_END_DATE' => 'End Date',
  'LBL_CONTRACT_ACCOUNT_ACCOUNT_ID' => '&#039;Client&#039; (related &#039;Client&#039; ID)',
  'LBL_LINE_ITEMS' => 'Cost Breakdown',
  'LBL_EDITVIEW_PANEL2' => 'Itinerary ',
  'LBL_ITINERARY ' => 'Itinerary',
  'LBL_EDITVIEW_PANEL3' => 'Record Info',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_EDITVIEW_PANEL4' => 'Approvals',
  'LBL_SEND_FOR_APPROVAL' => 'Send for Approval',
  'LBL_CLARIFICAITON_DONE' => 'Clarificaiton Done',
  'LBL_RESPONSE' => 'Response',
  'LBL_ADD_GROUP' => 'Add Costs',
  'LBL_DETAILVIEW_PANEL4' => 'Approval Process',
  'LBL_NAME' => 'Inspection',
  'LBL_ROUTINE_SRP_STDAPPROVAL_ROUTINE_ID' => '&#039;Routine&#039; (related &#039;&#039; ID)',
  'LBL_ROUTINE' => 'Routine',
  'LBL_ROUTINE_LABEL' => '*',
  'LBL_MEMBER_LABEL' => '**',
  'LBL_APPROVAL_STAMP' => 'Approval Stamp',
);