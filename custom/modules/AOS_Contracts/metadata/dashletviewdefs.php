<?php
$dashletData['AOS_ContractsDashlet']['searchFields'] = array (
  'name' => 
  array (
    'default' => '',
  ),
  'leads_aos_contracts_1_name' => 
  array (
    'default' => '',
  ),
  'contract_account' => 
  array (
    'default' => '',
  ),
  'date_entered' => 
  array (
    'default' => '',
  ),
  'created_by_name' => 
  array (
    'default' => '',
  ),
);
$dashletData['AOS_ContractsDashlet']['columns'] = array (
  'name' => 
  array (
    'width' => '25%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'contract_account' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_CONTRACT_ACCOUNT',
    'id' => 'CONTRACT_ACCOUNT_ID',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'approval_status_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_APPROVAL_STATUS',
    'width' => '10%',
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => true,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => true,
  ),
);
