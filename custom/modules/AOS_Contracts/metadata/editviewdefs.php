<?php
$module_name = 'AOS_Contracts';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
	  'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/AOS_Contracts/js/techinc-edit.js',
        ),
      ),
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'syncDetailEditViews' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_LINE_ITEMS' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'start_date',
            'label' => 'LBL_START_DATE',
          ),
          1 => 
          array (
            'name' => 'end_date',
            'label' => 'LBL_END_DATE',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'event_brief_c',
            'label' => 'LBL_EVENT_BRIEF',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'itinerary_c',
            'label' => 'LBL_ITINERARY ',
          ),
        ),
      ),
      'lbl_line_items' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'currency_id',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'line_items',
            'label' => 'LBL_LINE_ITEMS',
          ),
        ),
        2 => 
        array (
          0 => '',
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'send_for_approval_c',
            'label' => 'LBL_SEND_FOR_APPROVAL',
          ),
          1 => 
          array (
            'name' => 'approval_status_c',
            'studio' => 'visible',
            'label' => 'LBL_APPROVAL_STATUS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'clarificaiton_done_c',
            'label' => 'LBL_CLARIFICAITON_DONE',
          ),
          1 => 
          array (
            'name' => 'response_c',
            'studio' => 'visible',
            'label' => 'LBL_RESPONSE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'ddm_apv_title_c',
            'studio' => 'visible',
            'label' => 'LBL_DDM_APV_TITLE',
          ),
          1 => 
          array (
            'name' => 'ddm_approval_c',
            'studio' => 'visible',
            'label' => 'LBL_DDM_APPROVAL',
          ),
        ),
        3 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'ddm_remarks_c',
            'studio' => 'visible',
            'label' => 'LBL_DDM_REMARKS',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'mgmt_apv_title_c',
            'studio' => 'visible',
            'label' => 'LBL_MGMT_APV_TITLE',
          ),
          1 => 
          array (
            'name' => 'mgmt_approval_c',
            'studio' => 'visible',
            'label' => 'LBL_MGMT_APPROVAL',
          ),
        ),
        5 => 
        array (
          0 => '',
          1 => 
          array (
            'name' => 'mgmt_remarks_c',
            'studio' => 'visible',
            'label' => 'LBL_MGMT_REMARKS',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
