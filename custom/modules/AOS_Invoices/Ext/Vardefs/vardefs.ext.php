<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-06-18 07:43:43
$dictionary["AOS_Invoices"]["fields"]["leads_aos_invoices_1"] = array (
  'name' => 'leads_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'leads_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_AOS_INVOICES_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_aos_invoices_1leads_ida',
);
$dictionary["AOS_Invoices"]["fields"]["leads_aos_invoices_1_name"] = array (
  'name' => 'leads_aos_invoices_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_AOS_INVOICES_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_aos_invoices_1leads_ida',
  'link' => 'leads_aos_invoices_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["AOS_Invoices"]["fields"]["leads_aos_invoices_1leads_ida"] = array (
  'name' => 'leads_aos_invoices_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_aos_invoices_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


 // created: 2021-06-18 09:28:29
$dictionary['AOS_Invoices']['fields']['approval_end_date_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['approval_end_date_c']['labelValue']='Approval End Date';

 

 // created: 2021-07-01 12:04:31
$dictionary['AOS_Invoices']['fields']['approval_stamp_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['approval_stamp_c']['labelValue']='Approval Stamp';

 

 // created: 2021-06-18 09:28:16
$dictionary['AOS_Invoices']['fields']['approval_start_date_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['approval_start_date_c']['labelValue']='Approval Start Date';

 

 // created: 2021-06-18 09:29:03
$dictionary['AOS_Invoices']['fields']['approval_status_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['approval_status_c']['labelValue']='Approval Status';

 

 // created: 2021-06-18 12:13:05
$dictionary['AOS_Invoices']['fields']['billing_account']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['billing_account']['merge_filter']='disabled';

 

 // created: 2021-06-18 09:27:40
$dictionary['AOS_Invoices']['fields']['ceo_apv_title_c']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['ceo_apv_title_c']['labelValue']='3';

 

 // created: 2021-06-30 17:27:01
$dictionary['AOS_Invoices']['fields']['clarifications_done_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['clarifications_done_c']['labelValue']='Clarifications Done';

 

 // created: 2021-06-18 09:30:43
$dictionary['AOS_Invoices']['fields']['ddm_approval_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['ddm_approval_c']['labelValue']='DDM Approve/Reject';

 

 // created: 2021-06-18 09:27:00
$dictionary['AOS_Invoices']['fields']['ddm_approval_title_c']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['ddm_approval_title_c']['labelValue']='1';

 

 // created: 2021-06-18 09:30:14
$dictionary['AOS_Invoices']['fields']['ddm_approved_by_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['ddm_approved_by_c']['labelValue']='DDM Approved By';

 

 // created: 2021-06-18 09:32:04
$dictionary['AOS_Invoices']['fields']['ddm_date_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['ddm_date_c']['labelValue']='DDM Response Date';

 

 // created: 2021-06-18 09:30:59
$dictionary['AOS_Invoices']['fields']['ddm_flag_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['ddm_flag_c']['labelValue']='DDM Flag';

 

 // created: 2021-06-18 09:31:18
$dictionary['AOS_Invoices']['fields']['ddm_remarks_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['ddm_remarks_c']['labelValue']='DDM Remarks';

 

 // created: 2021-06-18 09:32:31
$dictionary['AOS_Invoices']['fields']['ddm_stamp_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['ddm_stamp_c']['labelValue']='DDM Stamp';

 

 // created: 2021-06-30 18:50:06
$dictionary['AOS_Invoices']['fields']['end_date_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['end_date_c']['labelValue']='End Date';

 

 // created: 2021-06-30 17:16:03
$dictionary['AOS_Invoices']['fields']['event_brief_c']['labelValue']='Event & Travel Brief';

 

 // created: 2021-06-18 09:33:59
$dictionary['AOS_Invoices']['fields']['event_date_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['event_date_c']['labelValue']='Event Date';

 

 // created: 2021-07-01 12:48:06
$dictionary['AOS_Invoices']['fields']['management_approval_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['management_approval_c']['labelValue']='Management Approve/Reject';

 

 // created: 2021-06-18 10:06:30
$dictionary['AOS_Invoices']['fields']['management_apv_by_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['management_apv_by_c']['labelValue']='Management Approval By';

 

 // created: 2021-06-18 10:07:13
$dictionary['AOS_Invoices']['fields']['management_date_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['management_date_c']['labelValue']='Management Response Date';

 

 // created: 2021-06-18 10:06:53
$dictionary['AOS_Invoices']['fields']['management_remarks_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['management_remarks_c']['labelValue']='Management Remarks';

 

 // created: 2021-07-01 12:03:15
$dictionary['AOS_Invoices']['fields']['management_stamp_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['management_stamp_c']['labelValue']='Management Stamp';

 

 // created: 2021-06-18 10:08:27
$dictionary['AOS_Invoices']['fields']['member_1_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['member_1_c']['labelValue']='Member 1';

 

 // created: 2021-06-18 10:08:38
$dictionary['AOS_Invoices']['fields']['member_2_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['member_2_c']['labelValue']='Member 2';

 

 // created: 2021-06-18 10:08:49
$dictionary['AOS_Invoices']['fields']['member_3_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['member_3_c']['labelValue']='Member 3';

 

 // created: 2021-06-18 10:09:00
$dictionary['AOS_Invoices']['fields']['member_4_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['member_4_c']['labelValue']='Member 4';

 

 // created: 2021-06-18 10:09:22
$dictionary['AOS_Invoices']['fields']['member_5_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['member_5_c']['labelValue']='Member 5';

 

 // created: 2021-06-18 10:09:35
$dictionary['AOS_Invoices']['fields']['member_6_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['member_6_c']['labelValue']='Member 6';

 

 // created: 2021-07-01 11:09:46
$dictionary['AOS_Invoices']['fields']['member_label_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['member_label_c']['labelValue']='**';

 

 // created: 2021-06-18 10:11:10
$dictionary['AOS_Invoices']['fields']['memstamp1_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['memstamp1_c']['labelValue']='MemStamp1';

 

 // created: 2021-06-18 10:11:23
$dictionary['AOS_Invoices']['fields']['memstamp2_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['memstamp2_c']['labelValue']='MemStamp2';

 

 // created: 2021-06-18 10:11:38
$dictionary['AOS_Invoices']['fields']['memstamp3_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['memstamp3_c']['labelValue']='MemStamp3';

 

 // created: 2021-06-18 10:11:50
$dictionary['AOS_Invoices']['fields']['memstamp4_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['memstamp4_c']['labelValue']='MemStamp4';

 

 // created: 2021-06-18 10:12:11
$dictionary['AOS_Invoices']['fields']['memstamp5_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['memstamp5_c']['labelValue']='MemStamp5';

 

 // created: 2021-06-18 10:12:25
$dictionary['AOS_Invoices']['fields']['memstamp6_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['memstamp6_c']['labelValue']='MemStamp6';

 

 // created: 2021-06-18 09:27:24
$dictionary['AOS_Invoices']['fields']['mgmt_approval_title_c']['inline_edit']='';
$dictionary['AOS_Invoices']['fields']['mgmt_approval_title_c']['labelValue']='2';

 

 // created: 2021-06-18 10:13:59
$dictionary['AOS_Invoices']['fields']['mgmt_flag_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['mgmt_flag_c']['labelValue']='MGMT Flag';

 

 // created: 2021-06-30 17:53:47
$dictionary['AOS_Invoices']['fields']['name']['inline_edit']=true;
$dictionary['AOS_Invoices']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Invoices']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Invoices']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Invoices']['fields']['name']['unified_search']=false;

 

 // created: 2021-06-18 10:14:15
$dictionary['AOS_Invoices']['fields']['prepared_by_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['prepared_by_c']['labelValue']='Prepared By';

 

 // created: 2021-06-30 17:27:21
$dictionary['AOS_Invoices']['fields']['response_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['response_c']['labelValue']='Response';

 

 // created: 2021-06-30 17:57:31
$dictionary['AOS_Invoices']['fields']['routine_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['routine_c']['labelValue']='Routine';

 

 // created: 2021-07-01 11:09:09
$dictionary['AOS_Invoices']['fields']['routine_label_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['routine_label_c']['labelValue']='*';

 

 // created: 2021-06-18 14:00:22
$dictionary['AOS_Invoices']['fields']['send_for_approval_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['send_for_approval_c']['labelValue']='Send For Approval';

 

 // created: 2021-06-30 17:57:31
$dictionary['AOS_Invoices']['fields']['srp_stdapproval_routine_id_c']['inline_edit']=1;

 

 // created: 2021-06-30 18:50:17
$dictionary['AOS_Invoices']['fields']['start_date_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['start_date_c']['labelValue']='Start Date';

 

 // created: 2021-06-18 10:11:23
$dictionary['AOS_Invoices']['fields']['user_id10_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:11:38
$dictionary['AOS_Invoices']['fields']['user_id11_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:11:50
$dictionary['AOS_Invoices']['fields']['user_id12_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:12:11
$dictionary['AOS_Invoices']['fields']['user_id13_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:12:25
$dictionary['AOS_Invoices']['fields']['user_id14_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:14:15
$dictionary['AOS_Invoices']['fields']['user_id15_c']['inline_edit']=1;

 

 // created: 2021-07-01 12:03:15
$dictionary['AOS_Invoices']['fields']['user_id16_c']['inline_edit']=1;

 

 // created: 2021-06-18 09:32:31
$dictionary['AOS_Invoices']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:06:30
$dictionary['AOS_Invoices']['fields']['user_id2_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:08:27
$dictionary['AOS_Invoices']['fields']['user_id3_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:08:38
$dictionary['AOS_Invoices']['fields']['user_id4_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:08:49
$dictionary['AOS_Invoices']['fields']['user_id5_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:09:00
$dictionary['AOS_Invoices']['fields']['user_id6_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:09:22
$dictionary['AOS_Invoices']['fields']['user_id7_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:09:35
$dictionary['AOS_Invoices']['fields']['user_id8_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:11:10
$dictionary['AOS_Invoices']['fields']['user_id9_c']['inline_edit']=1;

 

 // created: 2021-06-18 09:30:14
$dictionary['AOS_Invoices']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2021-06-18 10:14:32
$dictionary['AOS_Invoices']['fields']['venue_c']['inline_edit']='1';
$dictionary['AOS_Invoices']['fields']['venue_c']['labelValue']='Venue';

 

 // created: 2021-06-18 10:14:31
$dictionary['AOS_Invoices']['fields']['vnp_venues_id_c']['inline_edit']=1;

 
?>