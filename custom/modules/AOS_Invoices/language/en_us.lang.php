<?php
// created: 2021-07-01 13:26:57
$mod_strings = array (
  'LBL_BILLING_ACCOUNT' => 'Client',
  'LBL_ACCOUNTS' => 'Clients',
  'LBL_AOS_QUOTES_AOS_INVOICES' => 'Initial Event Approval: Invoices',
  'LNK_NEW_RECORD' => 'Create Invoice',
  'LNK_LIST' => 'View Bid Presentation',
  'MSG_SHOW_DUPLICATES' => 'Creating this account may potentially create a duplicate account. You may either click on Save to continue creating this new account with the previously entered data or you may click Cancel.',
  'LBL_LIST_FORM_TITLE' => 'Bid Presentation List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Bid Presentation',
  'LBL_HOMEPAGE_TITLE' => 'My Bid Presentation',
  'LBL_DDM_APPROVAL_TITLE' => '1',
  'LBL_MGMT_APPROVAL_TITLE' => '2',
  'LBL_CEO_APV_TITLE' => '3',
  'LBL_APPROVAL_START_DATE' => 'Approval Start Date',
  'LBL_APPROVAL_END_DATE' => 'Approval End Date',
  'LBL_APPROVAL_STATUS' => 'Approval Status',
  'LBL_DDM_APPROVED_BY_USER_ID' => '&#039;DDM Approved By&#039; (related &#039;User&#039; ID)',
  'LBL_DDM_APPROVED_BY' => 'DDM Approved By',
  'LBL_DDM_APPROVAL' => 'DDM Approve/Reject',
  'LBL_DDM_FLAG' => 'DDM Flag',
  'LBL_DDM_REMARKS' => 'DDM Remarks',
  'LBL_DDM_DATE' => 'DDM Response Date',
  'LBL_DDM_STAMP_USER_ID' => '&#039;DDM Stamp&#039; (related &#039;User&#039; ID)',
  'LBL_DDM_STAMP' => 'DDM Stamp',
  'LBL_EVENT_BRIEF' => 'Event & Travel Brief',
  'LBL_EVENT_DATE' => 'Event Date',
  'LBL_MANAGEMENT_APV_BY_USER_ID' => '&#039;Management Approval By&#039; (related &#039;User&#039; ID)',
  'LBL_MANAGEMENT_APV_BY' => 'Management Approval By',
  'LBL_MANAGEMENT_REMARKS' => 'Management Remarks',
  'LBL_MANAGEMENT_DATE' => 'Management Response Date',
  'LBL_MEMBER_1_USER_ID' => '&#039;Member 1&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_1' => 'Member 1',
  'LBL_MEMBER_2_USER_ID' => '&#039;Member 2&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_2' => 'Member 2',
  'LBL_MEMBER_3_USER_ID' => '&#039;Member 3&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_3' => 'Member 3',
  'LBL_MEMBER_4_USER_ID' => '&#039;Member 4&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_4' => 'Member 4',
  'LBL_MEMBER_5_USER_ID' => '&#039;Member 5&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_5' => 'Member 5',
  'LBL_MEMBER_6_USER_ID' => '&#039;Member 6&#039; (related &#039;User&#039; ID)',
  'LBL_MEMBER_6' => 'Member 6',
  'LBL_MEMSTAMP1_USER_ID' => '&#039;MemStamp1&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP1' => 'MemStamp1',
  'LBL_MEMSTAMP2_USER_ID' => '&#039;MemStamp2&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP2' => 'MemStamp2',
  'LBL_MEMSTAMP3_USER_ID' => '&#039;MemStamp3&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP3' => 'MemStamp3',
  'LBL_MEMSTAMP4_USER_ID' => '&#039;MemStamp4&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP4' => 'MemStamp4',
  'LBL_MEMSTAMP5_USER_ID' => '&#039;MemStamp5&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP5' => 'MemStamp5',
  'LBL_MEMSTAMP6_USER_ID' => '&#039;MemStamp6&#039; (related &#039;User&#039; ID)',
  'LBL_MEMSTAMP6' => 'MemStamp6',
  'LBL_MANAGEMENT_APPROVAL' => 'Management Approve/Reject',
  'LBL_MGMT_FLAG' => 'MGMT Flag',
  'LBL_PREPARED_BY_USER_ID' => '&#039;Prepared By&#039; (related &#039;User&#039; ID)',
  'LBL_PREPARED_BY' => 'Prepared By',
  'LBL_VENUE_VNP_VENUES_ID' => '&#039;Venue&#039; (related &#039;&#039; ID)',
  'LBL_VENUE' => 'Venue',
  'LBL_DETAILVIEW_PANEL2' => 'Approvals',
  'LBL_DETAILVIEW_PANEL1' => 'Approval Process',
  'LBL_DETAILVIEW_PANEL3' => 'Brief',
  'LBL_EDITVIEW_PANEL2' => 'Approvals',
  'LBL_EDITVIEW_PANEL3' => 'Record Info',
  'LBL_EDITVIEW_PANEL4' => 'Record Info',
  'LBL_BILLING_ACCOUNT_ACCOUNT_ID' => '&#039;billing account&#039; (related &#039;Client&#039; ID)',
  'LBL_DETAILVIEW_PANEL4' => 'New Panel 4',
  'LBL_PANEL_ASSIGNMENT' => 'Record Information',
  'LBL_DETAILVIEW_PANEL5' => 'New Panel 5',
  'LBL_SEND_FOR_APPROVAL' => 'Send For Approval',
  'LBL_EDITVIEW_PANEL5' => 'New Panel 5',
  'LBL_EDITVIEW_PANEL1' => 'Presentation Details',
  'LBL_START_DATE' => 'Start Date',
  'LBL_END_DATE' => 'End Date',
  'LBL_LINE_ITEMS' => 'Cost Breakdown',
  'LBL_CLARIFICATIONS_DONE' => 'Clarifications Done',
  'LBL_RESPONSE' => 'Response',
  'LBL_INVOICE_TO' => 'Presentation Details',
  'LBL_NAME' => 'Bid Presentation',
  'LBL_ROUTINE_SRP_STDAPPROVAL_ROUTINE_ID' => '&#039;Routine&#039; (related &#039;&#039; ID)',
  'LBL_ROUTINE' => 'Routine',
  'LBL_ROUTINE_LABEL' => '*',
  'LBL_MEMBER_LABEL' => '**',
  'LBL_MANAGEMENT_STAMP_USER_ID' => '&#039;Management Stamp&#039; (related &#039;User&#039; ID)',
  'LBL_MANAGEMENT_STAMP' => 'Management Stamp',
  'LBL_APPROVAL_STAMP' => 'Approval Stamp',
);