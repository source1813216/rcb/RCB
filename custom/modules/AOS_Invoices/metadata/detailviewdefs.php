<?php
$module_name = 'AOS_Invoices';
$_object_name = 'aos_invoices';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
          4 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'pdf\');" value="{$MOD.LBL_PRINT_AS_PDF}">',
          ),
          5 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'emailpdf\');" value="{$MOD.LBL_EMAIL_PDF}">',
          ),
          6 => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup(\'email\');" value="{$MOD.LBL_EMAIL_INVOICE}">',
          ),
        ),
      ),
      'maxColumns' => '2',
	  'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/AOS_Invoices/js/techinc-detail.js',
        ),
      ),
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_DETAILVIEW_PANEL2' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_INVOICE_TO' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_LINE_ITEMS' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_detailview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'approval_status_c',
            'studio' => 'visible',
            'label' => 'LBL_APPROVAL_STATUS',
          ),
          1 => 
          array (
            'name' => 'prepared_by_c',
            'studio' => 'visible',
            'label' => 'LBL_PREPARED_BY',
          ),
        ),
        1 => 
        array (
          0 => '',
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'routine_label_c',
            'label' => 'LBL_ROUTINE_LABEL',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'ddm_approved_by_c',
            'studio' => 'visible',
            'label' => 'LBL_DDM_APPROVED_BY',
          ),
          1 => 
          array (
            'name' => 'ddm_date_c',
            'label' => 'LBL_DDM_DATE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'management_apv_by_c',
            'studio' => 'visible',
            'label' => 'LBL_MANAGEMENT_APV_BY',
          ),
          1 => 
          array (
            'name' => 'management_date_c',
            'label' => 'LBL_MANAGEMENT_DATE',
          ),
        ),
        5 => 
        array (
          0 => '',
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'member_label_c',
            'label' => 'LBL_MEMBER_LABEL',
          ),
          1 => '',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'member_1_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_1',
          ),
          1 => 
          array (
            'name' => 'member_2_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_2',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'member_3_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_3',
          ),
          1 => 
          array (
            'name' => 'member_4_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_4',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'member_5_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_5',
          ),
          1 => 
          array (
            'name' => 'member_6_c',
            'studio' => 'visible',
            'label' => 'LBL_MEMBER_6',
          ),
        ),
      ),
      'LBL_INVOICE_TO' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'billing_account',
            'label' => 'LBL_BILLING_ACCOUNT',
          ),
          1 => 
          array (
            'name' => 'billing_contact',
            'label' => 'LBL_BILLING_CONTACT',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'leads_aos_invoices_1_name',
            'label' => 'LBL_LEADS_AOS_INVOICES_1_FROM_LEADS_TITLE',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'start_date_c',
            'label' => 'LBL_START_DATE',
          ),
          1 => 
          array (
            'name' => 'end_date_c',
            'label' => 'LBL_END_DATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'event_brief_c',
            'label' => 'LBL_EVENT_BRIEF',
          ),
        ),
      ),
      'lbl_line_items' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'currency_id',
            'studio' => 'visible',
            'label' => 'LBL_CURRENCY',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'line_items',
            'label' => 'LBL_LINE_ITEMS',
          ),
        ),
        2 => 
        array (
          0 => '',
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'label' => 'LBL_DATE_MODIFIED',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
;
?>
