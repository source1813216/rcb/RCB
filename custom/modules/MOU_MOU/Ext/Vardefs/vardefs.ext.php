<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-07-26 13:22:42
$dictionary["MOU_MOU"]["fields"]["am_projecttemplates_mou_mou_1"] = array (
  'name' => 'am_projecttemplates_mou_mou_1',
  'type' => 'link',
  'relationship' => 'am_projecttemplates_mou_mou_1',
  'source' => 'non-db',
  'module' => 'AM_ProjectTemplates',
  'bean_name' => 'AM_ProjectTemplates',
  'vname' => 'LBL_AM_PROJECTTEMPLATES_MOU_MOU_1_FROM_AM_PROJECTTEMPLATES_TITLE',
  'id_name' => 'am_projecttemplates_mou_mou_1am_projecttemplates_ida',
);
$dictionary["MOU_MOU"]["fields"]["am_projecttemplates_mou_mou_1_name"] = array (
  'name' => 'am_projecttemplates_mou_mou_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AM_PROJECTTEMPLATES_MOU_MOU_1_FROM_AM_PROJECTTEMPLATES_TITLE',
  'save' => true,
  'id_name' => 'am_projecttemplates_mou_mou_1am_projecttemplates_ida',
  'link' => 'am_projecttemplates_mou_mou_1',
  'table' => 'am_projecttemplates',
  'module' => 'AM_ProjectTemplates',
  'rname' => 'name',
);
$dictionary["MOU_MOU"]["fields"]["am_projecttemplates_mou_mou_1am_projecttemplates_ida"] = array (
  'name' => 'am_projecttemplates_mou_mou_1am_projecttemplates_ida',
  'type' => 'link',
  'relationship' => 'am_projecttemplates_mou_mou_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AM_PROJECTTEMPLATES_MOU_MOU_1_FROM_MOU_MOU_TITLE',
);


// created: 2021-07-12 17:45:01
$dictionary["MOU_MOU"]["fields"]["leads_mou_mou_1"] = array (
  'name' => 'leads_mou_mou_1',
  'type' => 'link',
  'relationship' => 'leads_mou_mou_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_MOU_MOU_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_mou_mou_1leads_ida',
);
$dictionary["MOU_MOU"]["fields"]["leads_mou_mou_1_name"] = array (
  'name' => 'leads_mou_mou_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_MOU_MOU_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_mou_mou_1leads_ida',
  'link' => 'leads_mou_mou_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["MOU_MOU"]["fields"]["leads_mou_mou_1leads_ida"] = array (
  'name' => 'leads_mou_mou_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_mou_mou_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_MOU_MOU_1_FROM_MOU_MOU_TITLE',
);


// created: 2021-07-09 09:34:34
$dictionary["MOU_MOU"]["fields"]["mou_mou_documents_1"] = array (
  'name' => 'mou_mou_documents_1',
  'type' => 'link',
  'relationship' => 'mou_mou_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_MOU_MOU_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2021-07-09 09:34:58
$dictionary["MOU_MOU"]["fields"]["mou_mou_tasks_1"] = array (
  'name' => 'mou_mou_tasks_1',
  'type' => 'link',
  'relationship' => 'mou_mou_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_MOU_MOU_TASKS_1_FROM_TASKS_TITLE',
);


 // created: 2021-07-09 06:46:39
$dictionary['MOU_MOU']['fields']['approval_routine_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['approval_routine_c']['labelValue']='Approval Routine';

 

 // created: 2021-07-15 19:36:48
$dictionary['MOU_MOU']['fields']['approval_stamp_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['approval_stamp_c']['labelValue']='Approval Stamp';

 

 // created: 2021-07-15 06:32:56
$dictionary['MOU_MOU']['fields']['approval_status_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['approval_status_c']['labelValue']='Approval Status';

 

 // created: 2021-07-09 06:49:02
$dictionary['MOU_MOU']['fields']['ceo_approval_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ceo_approval_c']['labelValue']='CEO Approve/Reject';

 

 // created: 2021-07-09 06:48:12
$dictionary['MOU_MOU']['fields']['ceo_apv_by_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ceo_apv_by_c']['labelValue']='CEO Approval By';

 

 // created: 2021-07-09 06:49:32
$dictionary['MOU_MOU']['fields']['ceo_flg_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ceo_flg_c']['labelValue']='CEO Flg';

 

 // created: 2021-07-09 08:28:30
$dictionary['MOU_MOU']['fields']['ceo_label_c']['inline_edit']='';
$dictionary['MOU_MOU']['fields']['ceo_label_c']['labelValue']='6';

 

 // created: 2021-07-09 06:52:27
$dictionary['MOU_MOU']['fields']['ceo_remarks_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ceo_remarks_c']['labelValue']='CEO Remarks';

 

 // created: 2021-07-09 06:52:47
$dictionary['MOU_MOU']['fields']['ceo_response_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ceo_response_c']['labelValue']='CEO Response Date';

 

 // created: 2021-07-09 06:53:12
$dictionary['MOU_MOU']['fields']['ceo_stamp_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ceo_stamp_c']['labelValue']='CEO Stamp';

 

 // created: 2021-07-09 08:33:12
$dictionary['MOU_MOU']['fields']['clarification_done_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['clarification_done_c']['labelValue']='Clarification Done';

 

 // created: 2021-07-09 06:56:21
$dictionary['MOU_MOU']['fields']['dceo_approval_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dceo_approval_c']['labelValue']='Deputy CEO Approve/Reject';

 

 // created: 2021-07-09 06:59:54
$dictionary['MOU_MOU']['fields']['dceo_apv_by_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dceo_apv_by_c']['labelValue']='Deputy CEO Approval By';

 

 // created: 2021-07-09 08:28:09
$dictionary['MOU_MOU']['fields']['dceo_c']['inline_edit']='';
$dictionary['MOU_MOU']['fields']['dceo_c']['labelValue']='5';

 

 // created: 2021-07-09 06:53:50
$dictionary['MOU_MOU']['fields']['dceo_flg_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dceo_flg_c']['labelValue']='DCEO Flg';

 

 // created: 2021-07-09 07:01:06
$dictionary['MOU_MOU']['fields']['dceo_remarks_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dceo_remarks_c']['labelValue']='Deputy CEO Remarks';

 

 // created: 2021-07-09 07:00:30
$dictionary['MOU_MOU']['fields']['dceo_response_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dceo_response_c']['labelValue']='Deputy CEO Response Date';

 

 // created: 2021-07-09 06:54:51
$dictionary['MOU_MOU']['fields']['dceo_stamp_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dceo_stamp_c']['labelValue']='DCEO Stamp';

 

 // created: 2021-07-09 07:04:32
$dictionary['MOU_MOU']['fields']['ddm_approval_by_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ddm_approval_by_c']['labelValue']='DDM Approved By';

 

 // created: 2021-07-09 08:38:10
$dictionary['MOU_MOU']['fields']['ddm_approval_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ddm_approval_c']['labelValue']='DDM Approve/Reject';

 

 // created: 2021-07-09 07:09:27
$dictionary['MOU_MOU']['fields']['ddm_flg_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ddm_flg_c']['labelValue']='DDM Flg';

 

 // created: 2021-07-09 08:27:40
$dictionary['MOU_MOU']['fields']['ddm_label_c']['inline_edit']='';
$dictionary['MOU_MOU']['fields']['ddm_label_c']['labelValue']='4';

 

 // created: 2021-07-09 07:05:43
$dictionary['MOU_MOU']['fields']['ddm_remarks_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ddm_remarks_c']['labelValue']='DDM Remarks';

 

 // created: 2021-07-09 07:08:37
$dictionary['MOU_MOU']['fields']['ddm_response_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ddm_response_c']['labelValue']='DDM Response Date';

 

 // created: 2021-07-09 07:05:23
$dictionary['MOU_MOU']['fields']['ddm_stamp_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ddm_stamp_c']['labelValue']='DDM Stamp';

 

 // created: 2021-07-09 07:58:01
$dictionary['MOU_MOU']['fields']['dec_approval_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dec_approval_c']['labelValue']='DEC Approve/Reject';

 

 // created: 2021-07-09 07:56:23
$dictionary['MOU_MOU']['fields']['dec_apv_by_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dec_apv_by_c']['labelValue']='DEC Approval By';

 

 // created: 2021-07-09 07:58:38
$dictionary['MOU_MOU']['fields']['dec_flg_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dec_flg_c']['labelValue']='DEC Flg';

 

 // created: 2021-07-09 08:27:05
$dictionary['MOU_MOU']['fields']['dec_lavbel_c']['inline_edit']='';
$dictionary['MOU_MOU']['fields']['dec_lavbel_c']['labelValue']='3';

 

 // created: 2021-07-09 08:00:30
$dictionary['MOU_MOU']['fields']['dec_remarks_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dec_remarks_c']['labelValue']='DEC Remarks';

 

 // created: 2021-07-09 07:59:01
$dictionary['MOU_MOU']['fields']['dec_response_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dec_response_c']['labelValue']='DEC Response Date';

 

 // created: 2021-07-09 07:58:24
$dictionary['MOU_MOU']['fields']['dec_stamp_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['dec_stamp_c']['labelValue']='DEC Stamp';

 

 // created: 2021-07-08 17:09:34
$dictionary['MOU_MOU']['fields']['end_date_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['end_date_c']['labelValue']='Approval End Date';

 

 // created: 2021-07-15 19:12:05
$dictionary['MOU_MOU']['fields']['lawyer_notes_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['lawyer_notes_c']['labelValue']='Lawyer\'s Notes';

 

 // created: 2021-07-12 17:36:27

 

 // created: 2021-08-02 18:58:25
$dictionary['MOU_MOU']['fields']['lead_id_string_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['lead_id_string_c']['labelValue']='Lead ID String';

 

 // created: 2021-07-09 07:26:47
$dictionary['MOU_MOU']['fields']['legal_approval_by_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['legal_approval_by_c']['labelValue']='Legal Approval By';

 

 // created: 2021-07-09 07:32:12
$dictionary['MOU_MOU']['fields']['legal_approval_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['legal_approval_c']['labelValue']='Legal Approve/Reject';

 

 // created: 2021-07-09 07:31:09
$dictionary['MOU_MOU']['fields']['legal_flg_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['legal_flg_c']['labelValue']='Legal Flg';

 

 // created: 2021-07-09 08:25:57
$dictionary['MOU_MOU']['fields']['legal_label_c']['inline_edit']='';
$dictionary['MOU_MOU']['fields']['legal_label_c']['labelValue']='1';

 

 // created: 2021-07-09 07:32:57
$dictionary['MOU_MOU']['fields']['legal_remarks_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['legal_remarks_c']['labelValue']='Legal Remarks';

 

 // created: 2021-07-09 07:31:44
$dictionary['MOU_MOU']['fields']['legal_response_date_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['legal_response_date_c']['labelValue']='Legal Response Date';

 

 // created: 2021-07-09 07:34:36
$dictionary['MOU_MOU']['fields']['legal_stamp_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['legal_stamp_c']['labelValue']='Legal Stamp';

 

 // created: 2021-07-12 17:05:20
$dictionary['MOU_MOU']['fields']['mou_title_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['mou_title_c']['labelValue']='MOU Title';

 

 // created: 2021-07-08 17:10:33
$dictionary['MOU_MOU']['fields']['prepared_by_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['prepared_by_c']['labelValue']='Prepared By';

 

 // created: 2021-07-19 19:56:00
$dictionary['MOU_MOU']['fields']['require_amendments_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['require_amendments_c']['labelValue']='Require Amendments';

 

 // created: 2021-07-09 08:33:30
$dictionary['MOU_MOU']['fields']['response_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['response_c']['labelValue']='Response';

 

 // created: 2021-07-15 20:31:08
$dictionary['MOU_MOU']['fields']['revision_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['revision_c']['labelValue']='Revision';

 

 // created: 2021-07-09 08:29:32
$dictionary['MOU_MOU']['fields']['routine_label_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['routine_label_c']['labelValue']='*';

 

 // created: 2021-07-09 08:32:59
$dictionary['MOU_MOU']['fields']['send_for_approval_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['send_for_approval_c']['labelValue']='Send for Approval';

 

 // created: 2021-07-16 05:52:57
$dictionary['MOU_MOU']['fields']['send_to_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['send_to_c']['labelValue']='Send To';

 

 // created: 2021-07-09 06:46:39
$dictionary['MOU_MOU']['fields']['srp_stdapproval_routine_id_c']['inline_edit']=1;

 

 // created: 2021-07-09 07:29:12
$dictionary['MOU_MOU']['fields']['ssd_approval_by_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ssd_approval_by_c']['labelValue']='SSD Approval By';

 

 // created: 2021-07-09 07:37:11
$dictionary['MOU_MOU']['fields']['ssd_approval_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ssd_approval_c']['labelValue']='SSD Approve/Reject';

 

 // created: 2021-07-09 07:35:49
$dictionary['MOU_MOU']['fields']['ssd_flg_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ssd_flg_c']['labelValue']='SSD Flg';

 

 // created: 2021-07-09 08:26:37
$dictionary['MOU_MOU']['fields']['ssd_label_c']['inline_edit']='';
$dictionary['MOU_MOU']['fields']['ssd_label_c']['labelValue']='2';

 

 // created: 2021-07-09 07:36:05
$dictionary['MOU_MOU']['fields']['ssd_remarks_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ssd_remarks_c']['labelValue']='SSD Remarks';

 

 // created: 2021-07-09 07:38:30
$dictionary['MOU_MOU']['fields']['ssd_response_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ssd_response_c']['labelValue']='SSD Response Date';

 

 // created: 2021-07-09 07:29:52
$dictionary['MOU_MOU']['fields']['ssd_stamp_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['ssd_stamp_c']['labelValue']='SSD Stamp';

 

 // created: 2021-07-08 17:08:59
$dictionary['MOU_MOU']['fields']['start_date_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['start_date_c']['labelValue']='Approval Start Date';

 

 // created: 2021-07-16 07:15:01
$dictionary['MOU_MOU']['fields']['task_description_email_c']['inline_edit']='1';
$dictionary['MOU_MOU']['fields']['task_description_email_c']['labelValue']='Task Description Email';

 

 // created: 2021-07-09 07:34:36
$dictionary['MOU_MOU']['fields']['user_id10_c']['inline_edit']=1;

 

 // created: 2021-07-09 07:56:23
$dictionary['MOU_MOU']['fields']['user_id11_c']['inline_edit']=1;

 

 // created: 2021-07-09 07:58:24
$dictionary['MOU_MOU']['fields']['user_id12_c']['inline_edit']=1;

 

 // created: 2021-07-16 05:52:57
$dictionary['MOU_MOU']['fields']['user_id13_c']['inline_edit']=1;

 

 // created: 2021-07-09 06:48:12
$dictionary['MOU_MOU']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2021-07-09 06:53:12
$dictionary['MOU_MOU']['fields']['user_id2_c']['inline_edit']=1;

 

 // created: 2021-07-09 06:54:51
$dictionary['MOU_MOU']['fields']['user_id3_c']['inline_edit']=1;

 

 // created: 2021-07-09 06:59:54
$dictionary['MOU_MOU']['fields']['user_id4_c']['inline_edit']=1;

 

 // created: 2021-07-09 07:04:32
$dictionary['MOU_MOU']['fields']['user_id5_c']['inline_edit']=1;

 

 // created: 2021-07-09 07:05:23
$dictionary['MOU_MOU']['fields']['user_id6_c']['inline_edit']=1;

 

 // created: 2021-07-09 07:26:47
$dictionary['MOU_MOU']['fields']['user_id7_c']['inline_edit']=1;

 

 // created: 2021-07-09 07:29:12
$dictionary['MOU_MOU']['fields']['user_id8_c']['inline_edit']=1;

 

 // created: 2021-07-09 07:29:51
$dictionary['MOU_MOU']['fields']['user_id9_c']['inline_edit']=1;

 

 // created: 2021-07-08 17:10:33
$dictionary['MOU_MOU']['fields']['user_id_c']['inline_edit']=1;

 
?>