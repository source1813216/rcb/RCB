<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/   
    class outgoing_server_settings
    {
        function outgoing_server_settings_display($event, $arguments)
        {
           if($GLOBALS['app']->controller->action == 'DetailView'){

            $recordId = $_REQUEST['record'];
            $sel = "SELECT * FROM vi_multiple_smtp_settings";
            $result = $GLOBALS['db']->query($sel);
            $row = $GLOBALS['db']->fetchByAssoc($result);
            $enable = $row['enable'];
            if($enable == '1')
            {
                $javascript = <<<EOQ
                <script language="javascript">
                    $('document').ready(function(){
                    $("#tab-actions").after("<input type='button' value='Outgoing Server' id = 'outgoing_server' />"); 
                    $('#outgoing_server').click(function(){
                        window.open('index.php?module=Users&action=multismtp&records=$recordId','_self');
                    });
                 });
                </script>
EOQ;
       			 echo $javascript;
            }
       
        }
    }
}
?>