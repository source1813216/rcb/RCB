<?php 
 //WARNING: The contents of this file are auto-generated


/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
    $hook_version = 1;

    if (!isset($hook_array) || !is_array($hook_array)) {
        $hook_array = array();
    }

    if (!isset($hook_array['after_ui_frame']) || !is_array($hook_array['after_ui_frame'])) {
        $hook_array['after_ui_frame'] = array();
    }
    
    $hook_array['after_ui_frame'][] = Array(
        //Processing index. For sorting the array.
        1, 
        
        //Label. A string value to identify the hook.
        'after_ui_frame', 
        
        //The PHP file where your class is located.
        'custom/modules/Users/outgoing_server_settings.php', 
        
        //The class the method is in.
        'outgoing_server_settings', 
        
        //The method to call.
        'outgoing_server_settings_display' 
    );

?>