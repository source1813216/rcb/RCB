<?php 
 //WARNING: The contents of this file are auto-generated


/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_EMAILS_PER_RUN'] = 'Number of emails sent per batch:';
$mod_strings['LBL_LOCATION_ONLY'] = 'Location';
$mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'] = 'Use only integer values to specify the number of emails sent per batch';
$mod_strings['LBL_LIST_FROM_NAME'] = 'From Name';
$mod_strings['LBL_MAIL_SMTPSERVER'] = 'SMTP Mail Server:';
$mod_strings['LBL_MAIL_SMTPPORT'] = 'SMTP Port:';
$mod_strings['LBL_MAIL_SMTPUSER'] = 'Username:';
$mod_strings['LBL_MAIL_SMTPPASS'] = 'Password:';
$mod_strings['LBL_EMAIL_OUTBOUND_CONFIGURATION'] = 'Outgoing Mail Configuration';
$mod_strings['LBL_CONFIGURATION_NOTE'] = 'Configure the outgoing mail server for sending email notifications, including workflow alerts.';
$mod_strings['LBL_FROM_NAME'] = '"From" Name:';
$mod_strings['LBL_FROM_ADDRESS'] = '"From" Address:';
$mod_strings['LBL_REQUIRED_SYMBOL'] = '*';
$mod_strings['LBL_NTC_REQUIRED'] = 'Indicates required field';
$mod_strings['LBL_MAIL_SENDTYPE'] = 'Mail Transfer Agent:';
$mod_strings['LBL_CHOOSE_EMAIL_PROVIDER'] = 'Choose your Email provider';
$mod_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';
$mod_strings['LBL_SMTPTYPE_YAHOO'] = 'Yahoo!Mail';
$mod_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';
$mod_strings['LBL_SMTPTYPE_OTHER'] = 'Other';
$mod_strings['LBL_MAIL_SMTPAUTH_REQ'] =  'Use SMTP Authentication:';
$mod_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'Enable SMTP over SSL or TLS?';
$mod_strings['LBL_CHANGE_PASSWORD'] = 'Change Password';
$mod_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Send Test Email';
$mod_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'Email Address For Test Notification';
$mod_strings['LBL_EMAIL_SEND'] = 'Send';
$mod_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Cancel';










?>