<?php 
 //WARNING: The contents of this file are auto-generated


/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_EMAILS_PER_RUN'] = 'Tételenként elküldött e-mailek száma:';
$mod_strings['LBL_LOCATION_ONLY'] = 'Elhelyezkedés';
$mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'] = 'Csak egy egész értékeket használjon a kötegenként küldött e-mailek számának megadásához';
$mod_strings['LBL_LIST_FROM_NAME'] = 'Névből';
$mod_strings['LBL_MAIL_SMTPSERVER'] = 'SMTP Mail Server:';
$mod_strings['LBL_MAIL_SMTPPORT'] = 'SMTP port:';
$mod_strings['LBL_MAIL_SMTPUSER'] = 'Felhasználónév:';
$mod_strings['LBL_MAIL_SMTPPASS'] = 'Jelszó:';
$mod_strings['LBL_EMAIL_OUTBOUND_CONFIGURATION'] = 'Kimenő levelek konfigurálása';
$mod_strings['LBL_CONFIGURATION_NOTE'] = 'Konfigurálja a kimenő e-mail kiszolgálót e-mail értesítések küldéséhez, beleértve a munkafolyamat-riasztásokat is.';
$mod_strings['LBL_FROM_NAME'] = '"Névből:';
$mod_strings['LBL_FROM_ADDRESS'] = '"From" cím:';
$mod_strings['LBL_REQUIRED_SYMBOL'] = '*';
$mod_strings['LBL_NTC_REQUIRED'] = 'A szükséges mezőt jelzi';
$mod_strings['LBL_MAIL_SENDTYPE'] = 'Mail Transfer Agent:';
$mod_strings['LBL_CHOOSE_EMAIL_PROVIDER'] = 'Válassza ki az e-mail szolgáltatóját';
$mod_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';
$mod_strings['LBL_SMTPTYPE_YAHOO'] = 'Yahoo levelezés';
$mod_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';
$mod_strings['LBL_SMTPTYPE_OTHER'] = 'Más';
$mod_strings['LBL_MAIL_SMTPAUTH_REQ'] =  'SMTP hitelesítés használata:';
$mod_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'SMTP engedélyezése SSL vagy TLS használatával?';
$mod_strings['LBL_CHANGE_PASSWORD'] = 'Jelszó módosítása';
$mod_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Teszt e-mail küldése';
$mod_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'E-mail cím a teszt értesítéshez';
$mod_strings['LBL_EMAIL_SEND'] = 'Elküld';
$mod_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Megszünteti';










?>