<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('include/MVC/View/SugarView.php');
require_once('custom/modules/Users/VIMultiplesmtp_custom_forms.php');

class Viewmultismtp extends SugarView {
    protected function _getModuleTitleParams($browserTitle = false) {
        global $mod_strings;
        return array(
           "<a href='index.php?module=Administration&action=index'>".translate('LBL_MODULE_NAME', 'Administration')."</a>",
           translate('Multiple SMTP', 'Administration'),
           );
    } //end of _getModuleTitleParams
    public function preDisplay() {
    } //end of preDisplay
    
    /**
     * display
     * Override the display method to support customization for MultiSMTP
     */
    public function display() {
        //to fetch record_id of user from request
        $recordId = $_REQUEST['records'];
        global $mod_strings;
        global $app_list_strings;
        global $app_strings;
        global $current_user;
        global $sugar_config;
		echo $this->getModuleTitle(false);
        global $currentModule;

        $focus = new Administration();
        $focus->retrieveSettings(); //retrieve all admin settings.
        $GLOBALS['log']->info("Mass Emailer(EmailMan) ConfigureSettings view");

        $selUserName = "SELECT user_name FROM users WHERE id = '$recordId'";
        $selUserNameResult = $GLOBALS['db']->query($selUserName);
        $selUserNameRow = $GLOBALS['db']->fetchByAssoc($selUserNameResult);
        $userName = $selUserNameRow['user_name'];

        $selSettings = "SELECT * FROM vi_st_multiple_smtp WHERE user_id = '$recordId'";
        $result = $GLOBALS['db']->query($selSettings); 
        $row = $GLOBALS['db']->fetchByAssoc($result);

        $this->ss->assign("MOD", $mod_strings);
        $this->ss->assign("APP", $app_strings);
        $this->ss->assign("RETURN_MODULE", "Administration");
        $this->ss->assign("RETURN_ACTION", "index");
        $this->ss->assign("notify_fromname", $row['smtp_from_name']);
        $this->ss->assign("notify_fromaddress",$row['smtp_from_addr']);
        $this->ss->assign("notify_allow_default_outbound_on", (!empty($row['allow_default_outbound']) && $row['allow_default_outbound']) ? "checked='checked'" : "");
        $this->ss->assign("mail_smtpserver", $row['mail_smtpserver']);
        $this->ss->assign("mail_smtpport", $row['mail_smtpport']);
        $this->ss->assign("mail_smtpuser", $row['mail_smtpuser']);
        $this->ss->assign("mail_smtpauth_req_val",$row['mail_smtpauth_req']);
        $this->ss->assign("mail_smtpauth_req", ($row['mail_smtpauth_req']) ? "checked='checked'" : "");
        $this->ss->assign("mail_haspass", empty($row['mail_smtppass'])?0:1);
        $this->ss->assign("mail_smtppass",$row['mail_smtppass']);
        $this->ss->assign("MAIL_SSL_OPTIONS", get_select_options_with_id($app_list_strings['email_settings_for_ssl'], $row['mail_smtpssl']));

        //Assign the current users email for the test send dialogue.
        $this->ss->assign("CURRENT_USER_EMAIL", $current_user->email1);

        $showSendMail = false;
        $outboundSendTypeCSSClass = "yui-hidden";
        if (isset($sugar_config['allow_sendmail_outbound']) && $sugar_config['allow_sendmail_outbound']) {
            $showSendMail = true;
            $app_list_strings['notifymail_sendtype']['sendmail'] = 'sendmail';
            $outboundSendTypeCSSClass = "";
        }

        $this->ss->assign("OUTBOUND_TYPE_CLASS", $outboundSendTypeCSSClass);
        $this->ss->assign("mail_sendtype_options", get_select_options_with_id($app_list_strings['notifymail_sendtype'], $row['mail_sendtype']));

        
        require_once('modules/Emails/Email.php');
        $email = new Email();
        $this->ss->assign('ROLLOVER', $email->rolloverStyle);
        $this->ss->assign('THEME', $GLOBALS['theme']);
        $this->ss->assign('UserId',$recordId);
        $this->ss->assign('UserName',$userName);
        $this->ss->assign("JAVASCRIPT", get_validate_record_js());
        $this->ss->display('custom/modules/Users/tpl/multismtp.tpl');
    }//end of display
}//end of class
?>