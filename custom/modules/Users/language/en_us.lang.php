<?php
// created: 2021-05-30 16:46:10
$mod_strings = array (
  'LBL_EMAIL_STRING' => 'Email String',
  'LBL_WIZARD_WELCOME_TITLE' => 'Welcome to Techincglobal',
  'LBL_WIZARD_WELCOME_NOSMTP' => 'Click <b>Next</b> to configure a few basic settings for using System.',
  'LBL_WIZARD_LOCALE_DESC' => 'Specify your time zone and how you would like dates, currencies and names to appear in the System.',
  'LBL_WIZARD_FINISH_TITLE' => 'Just You are ready',
  'LBL_SIGNATURE' => 'Signature',
  'LBL_EDITVIEW_PANEL1' => 'Signature Image',
  'LBL_DETAILVIEW_PANEL1' => 'Signature Image',
);