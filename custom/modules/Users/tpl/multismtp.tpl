{*
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
*}
<script type="text/javascript" src="{sugar_getjspath file='modules/Users/User.js'}"></script>
<script type="text/javascript" src="{sugar_getjspath file='cache/include/javascript/sugar_grp_yui_widgets.js'}"></script>
<form name="ConfigureSettings" id="EditView" method="post">
<input type="hidden" name="action">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>
			<input class="button primary" type="button" name="button" id="btn_save" value="Save" onclick="data_save();return verify_data(this);this.form.module.value='{$RETURN_MODULE}'">
			<input type="submit" name="button" value="Cancel" onclick="this.form.action.value='{$RETURN_ACTION}'; this.form.module.value='{$RETURN_MODULE}';">
		</td>
		<td align="right" nowrap>
			<span class="required">{$MOD.LBL_REQUIRED_SYMBOL}</span> {$MOD.LBL_NTC_REQUIRED}
		</td>
	</tr>
</table>
<div id="EditView_tabs">
	<div class="panel-content">
		<div class="panel panel-default">
			<div class="panel-heading ">
				<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
					<div class="col-xs-10 col-sm-11 col-md-11">
						 {$MOD.LBL_EMAIL_OUTBOUND_CONFIGURATION}
					</div>
				</a>
			</div>
			<div class="panel-body">
				<div class="tab-content">
					<table width="100%" border="1" cellspacing="0" cellpadding="0" class="edit view">
						<tr>
							<td align="left" scope="row" colspan="4">
								{$MOD.LBL_CONFIGURATION_NOTE}
								<br />&nbsp;
							</td>
						</tr>
						<tr class="{$OUTBOUND_TYPE_CLASS}">
							<td width="20%" scope="row">{$MOD.LBL_MAIL_SENDTYPE}</td>
							<td width="30%">
								<select id="mail_sendtype" name="mail_sendtype" onChange="notify_setrequired(document.ConfigureSettings); SUGAR.user.showHideGmailDefaultLink(this);" tabindex="1">{$mail_sendtype_options}</select>
							</td>
							<td scope="row">&nbsp;</td>
							<td >&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" scope="row">{$MOD.LBL_FROM_NAME}<span class="required">{$MOD.LBL_REQUIRED_SYMBOL}</span></td>
							<td width="30%" ><input id='notify_fromname' name='notify_fromname' tabindex='1' size='25' maxlength='128' type="text" style ="margin-right: 500px;" value="{$notify_fromname}" ></td>
						</tr>
						<tr>
							<td width="20%" scope="row">{$MOD.LBL_FROM_ADDRESS}<span class="required">{$MOD.LBL_REQUIRED_SYMBOL}</span></td>
							<td width="30%"><input id='notify_fromaddress' name='notify_fromaddress' tabindex='1' size='25' maxlength='128' type="text" value="{$notify_fromaddress}"></td>
						</tr>
						<tr>
							<td align="left" scope="row" colspan="4">{$MOD.LBL_CHOOSE_EMAIL_PROVIDER}</td>
						</tr>
						<tr>
							<td colspan="4">
								<div id="smtpButtonGroup" class="yui-buttongroup">
								<span id="gmail" class="yui-button yui-radio-button{if $mail_smtptype == 'gmail'} yui-button-checked{/if}">
								<span class="first-child">
								<button type="button" name="mail_smtptype" value="gmail" class="btn btn-danger">
								&nbsp;&nbsp;&nbsp;&nbsp;{$MOD.LBL_SMTPTYPE_GMAIL}&nbsp;&nbsp;&nbsp;&nbsp;
								</button>
								</span>
								</span>
								<span id="yahoomail" class="yui-button yui-radio-button{if $mail_smtptype == 'yahoomail'} yui-button-checked{/if}">
								<span class="first-child">
								<button type="button" name="mail_smtptype" value="yahoomail" class="btn btn-danger">
								&nbsp;&nbsp;&nbsp;&nbsp;{$MOD.LBL_SMTPTYPE_YAHOO}&nbsp;&nbsp;&nbsp;&nbsp;
								</button>
								</span>
								</span>
								<span id="exchange" class="yui-button yui-radio-button{if $mail_smtptype == 'exchange'} yui-button-checked{/if}">
								<span class="first-child">
								<button type="button" name="mail_smtptype" value="exchange" class="btn btn-danger">
								&nbsp;&nbsp;&nbsp;&nbsp;{$MOD.LBL_SMTPTYPE_EXCHANGE}&nbsp;&nbsp;&nbsp;&nbsp;
								</button>
								</span>
								</span>
								<span id="other" class="yui-button yui-radio-button{if $mail_smtptype == 'other' || empty($mail_smtptype)} yui-button-checked{/if}">
								<span class="first-child">
								<button type="button" name="mail_smtptype" value="other" class="btn btn-danger">
								&nbsp;&nbsp;&nbsp;&nbsp;{$MOD.LBL_SMTPTYPE_OTHER}&nbsp;&nbsp;&nbsp;&nbsp;
								</button>
								</span>
								</span>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<div id="smtp_settings">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr id="mailsettings1">
											<td width="20%" scope="row"><span id="mail_smtpserver_label">{$MOD.LBL_MAIL_SMTPSERVER}</span> <span class="required">{$MOD.LBL_REQUIRED_SYMBOL}</span></td>
											<td width="30%" ><input type="text" id="mail_smtpserver" name="mail_smtpserver" tabindex="1" size="25" maxlength="255" value="{$mail_smtpserver}"></td>
											<td width="20%" scope="row"><span id="mail_smtpport_label">{$MOD.LBL_MAIL_SMTPPORT}</span> <span class="required">{$MOD.LBL_REQUIRED_SYMBOL}</span></td>
											<td width="30%" ><input type="text" id="mail_smtpport" name="mail_smtpport" tabindex="1" size="5" maxlength="5" value="{$mail_smtpport}"></td>
										</tr>
										<tr id="mailsettings2">
											<td scope="row"><span id='mail_smtpauth_req_label'>{$MOD.LBL_MAIL_SMTPAUTH_REQ}</span></td>
											<td >
												<input id='mail_smtpauth_req' name='mail_smtpauth_req' type="checkbox" class="checkbox" value="{$mail_smtpauth_req_val}" tabindex='1'
													   onclick="notify_setrequired(document.ConfigureSettings);" {$mail_smtpauth_req}>
											</td>
											<td width="15%" scope="row"><span id="mail_smtpssl_label">{$MOD.LBL_EMAIL_SMTP_SSL_OR_TLS}</span></td>
											<td width="35%" >
												<select id="mail_smtpssl" name="mail_smtpssl" tabindex="501" onchange="setDefaultSMTPPort();">{$MAIL_SSL_OPTIONS}</select>
											</td>
										</tr>
										<tr id="smtp_auth1">
											<td width="20%" scope="row"><span id="mail_smtpuser_label">{$MOD.LBL_MAIL_SMTPUSER}</span> <span class="required">{$MOD.LBL_REQUIRED_SYMBOL}</span></td>
											<td width="30%" ><input type="text" id="mail_smtpuser" name="mail_smtpuser" size="25" maxlength="255" value="{$mail_smtpuser}" tabindex='1' ></td>
											<td width="20%">&nbsp;</td>
											<td width="30%">&nbsp;</td>
										</tr>
										<tr id="smtp_auth2">
											<td width="20%" scope="row"><span id="mail_smtppass_label">{$MOD.LBL_MAIL_SMTPPASS}</span> <span class="required">{$MOD.LBL_REQUIRED_SYMBOL}</span></td>
											<td width="30%" >
												<input type="password" id="mail_smtppass" name="mail_smtppass" size="25" maxlength="255" tabindex='1'>
												{if $mail_smtppass neq ''}
										            <input type="hidden" id="mail_smtppassdata" name ="mail_smtppassdata" value="{$mail_smtppass}">
										        {else}
										            <input type="hidden" id="mail_smtppassdata" name ="mail_smtppassdata" value="">
										        {/if}
												<a href="javascript:void(0)" id='mail_smtppass_link' onClick="SUGAR.util.setEmailPasswordEdit('mail_smtppass')" style="display: none">{$MOD.LBL_CHANGE_PASSWORD}</a>
											</td>
											<td width="20%">&nbsp;</td>
											<td width="30%">&nbsp;</td>
										</tr>
										<tr><td colspan="4">&nbsp;</tr>
										<tr>
										<td width="15%"><input type="button" class="btn btn-info" value="{$MOD.LBL_EMAIL_TEST_OUTBOUND_SETTINGS}" onclick="testOutboundSettings();">&nbsp;</td>
										<td width="15%">&nbsp;</td>
										<td width="40%">&nbsp;</td>
										<td width="40%">&nbsp;</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="testOutboundDialog" class="yui-hidden">
    <div id="testOutbound">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="edit view">
			<tr>
				<td scope="row">
					{$MOD.LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR}
					<span class="required">
						{$MOD.LBL_REQUIRED_SYMBOL}
					</span>
				</td>
				<td >
					<input type="text" id="outboundtest_from_address" name="outboundtest_from_address" size="35" maxlength="64" value="{$CURRENT_USER_EMAIL}">
				</td>
			</tr>
			<tr>
				<td scope="row" colspan="2">
					<input type="button" class="button" value="   {$MOD.LBL_EMAIL_SEND}   " onclick="javascript:sendTestEmail();">&nbsp;
					<input type="button" class="button" value="   {$MOD.LBL_CANCEL_BUTTON_LABEL}   " onclick="javascript:EmailMan.testOutboundDialog.hide();">&nbsp;
				</td>
			</tr>
		</table>
	</div>
</div>
<div style="padding-top:2px;">
	<input class="button primary" type="button" name="button" id="btn_save" value="Save" onclick="data_save();return verify_data(this);">
	<input type="submit" name="button" value="Cancel" onclick="this.form.action.value='{$RETURN_ACTION}'; this.form.module.value='{$RETURN_MODULE}';">
</div>
</form>
{$JAVASCRIPT}
{literal}
<script type="text/javascript">
	EmailMan = {};
	function testOutboundSettings() {
		if (document.getElementById('mail_sendtype').value == 'sendmail') {
			testOutboundSettingsDialog();
			return;
		}
		var errorMessage = '';
		var isError = false;
		var fromAddress = document.getElementById("outboundtest_from_address").value;
	    var errorMessage = '';
	    var isError = false;
	    var smtpServer = document.getElementById('mail_smtpserver').value;
	    var smtpPort = document.getElementById('mail_smtpport').value;
	    var smtpssl  = document.getElementById('mail_smtpssl').value;
	    var mailsmtpauthreq = document.getElementById('mail_smtpauth_req');
	    if(trim(smtpServer) == '') {
	        isError = true;
	        errorMessage += "{/literal}{$APP.LBL_EMAIL_ACCOUNTS_SMTPSERVER}{literal}" + "<br/>";
	    }
	    if(trim(smtpPort) == '') {
	        isError = true;
	        errorMessage += "{/literal}{$APP.LBL_EMAIL_ACCOUNTS_SMTPPORT}{literal}" + "<br/>";
	    }
	    if(mailsmtpauthreq.checked) {
	        if(trim(document.getElementById('mail_smtpuser').value) == '') {
	            isError = true;
	            errorMessage += "{/literal}{$APP.LBL_EMAIL_ACCOUNTS_SMTPUSER}{literal}" + "<br/>";
	        }
	    }
	    if(isError) {
	        overlay("{/literal}{$APP.ERR_MISSING_REQUIRED_FIELDS}{literal}", errorMessage, 'alert');
	        return false;
	    }

	    testOutboundSettingsDialog();

	}
	function data_save() {
		var user_id = "{/literal}{$UserId}{literal}";
		var user_name = "{/literal}{$UserName}{literal}";
		var from_name = $('#notify_fromname').val();
		var from_address = $.trim($('#notify_fromaddress').val());
		var mail_smtpserver = $('#mail_smtpserver').val();
		var mail_smtpport = $('#mail_smtpport').val();
		var mail_smtpauth_req = $('#mail_smtpauth_req').val();
		var mail_smtpuser = $.trim($('#mail_smtpuser').val());
		var mail_smtppass = $.trim($('#mail_smtppass').val());
		var mail_smtpssl = $('#mail_smtpssl').val();
		var mail_sendtype = $('#mail_sendtype').val();
		$.ajax({
	    	url: 'index.php?entryPoint=VIOutgoing_Server_Setting',
			type: 'POST',
			data: {id : user_id,
				   user_name : user_name,
		           from_name : from_name,
		           from_address : from_address,
		           mail_smtpserver : mail_smtpserver,
		           mail_smtpport : mail_smtpport,
		           mail_smtpauth_req : mail_smtpauth_req,
		           mail_smtpuser : mail_smtpuser,
		           mail_smtppass : mail_smtppass,
		           mail_smtpssl : mail_smtpssl,
		           mail_sendtype : mail_sendtype },
			success: function(data) {
		    	console.log('success');
		    	
		    	window.location.href='index.php?module=Users&action=DetailView&record='+user_id;
		    },
		    error: function(){
		    	console.log('error');
		    }
		   
	    });
	}
	$('#mail_smtppass').on('change',function(){
		$('#mail_smtppassdata').val('');
	});
	function sendTestEmail() {
	    var toAddress = document.getElementById("outboundtest_from_address").value;
	    var fromAddress = document.getElementById("notify_fromaddress").value;
	    if (trim(toAddress) == "")
	    {
	        overlay("{/literal}{$APP.ERR_MISSING_REQUIRED_FIELDS}{literal}", "{/literal}{$APP.LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR}{literal}", 'alert');
	        return;
	    }
	    else if (!isValidEmail(toAddress)) {
	        overlay("{/literal}{$APP.ERR_INVALID_REQUIRED_FIELDS}{literal}", "{/literal}{$APP.LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR}{literal}", 'alert');
	        return;
	    }
	    if (trim(fromAddress) == "")
	    {
	        overlay("{/literal}{$APP.ERR_MISSING_REQUIRED_FIELDS}{literal}", "{/literal}{$APP.LBL_EMAIL_SETTINGS_FROM_ADDR}{literal}", 'alert');
	        return;
	    }
	    else if (!isValidEmail(fromAddress)) {
	        overlay("{/literal}{$APP.ERR_INVALID_REQUIRED_FIELDS}{literal}", "{/literal}{$APP.LBL_EMAIL_SETTINGS_FROM_ADDR}{literal}", 'alert');
	        return;
	    }
	    //Hide the email address window and show a message notifying the user that the test email is being sent.
	    EmailMan.testOutboundDialog.hide();
	    overlay("{/literal}{$APP.LBL_EMAIL_PERFORMING_TASK}{literal}", "{/literal}{$APP.LBL_EMAIL_ONE_MOMENT}{literal}", 'alert');

	    var callbackOutboundTest = {
	    	success	: function(o) {
	    		hideOverlay();
				var responseObject = YAHOO.lang.JSON.parse(o.responseText);
				if (responseObject.status)
					overlay("{/literal}{$APP.LBL_EMAIL_TEST_OUTBOUND_SETTINGS}{literal}", "{/literal}{$APP.LBL_EMAIL_TEST_NOTIFICATION_SENT}{literal}", 'alert');
				else
					overlay("{/literal}{$APP.LBL_EMAIL_TEST_OUTBOUND_SETTINGS}{literal}", responseObject.errorMessage, 'alert');
			}
	    };
	    var smtpServer = document.getElementById('mail_smtpserver').value;
	    var smtpPort = document.getElementById('mail_smtpport').value;
	    var smtpssl  = document.getElementById('mail_smtpssl').value;
	    var mailsmtpauthreq = document.getElementById('mail_smtpauth_req');
	    var mail_sendtype = document.getElementById('mail_sendtype').value;
	    var from_name = document.getElementById('notify_fromname').value;
	    var existing_smtppass = document.getElementById('mail_smtppassdata').value;
	    var mail_smtppass = '';
	    if(existing_smtppass != ''){
     		mail_smtppass = existing_smtppass;
     	}else{
      		mail_smtppass = document.getElementById('mail_smtppass').value;
     	}
		var postDataString = 'mail_type=system&mail_sendtype=' + mail_sendtype + '&mail_smtpserver=' + smtpServer + "&mail_smtpport=" + smtpPort + "&mail_smtpssl=" + smtpssl +
		                      "&mail_smtpauth_req=" + mailsmtpauthreq.checked + "&mail_smtpuser=" + trim(document.getElementById('mail_smtpuser').value) +
		                      "&mail_smtppass=" + trim(mail_smtppass) + "&outboundtest_to_address=" + encodeURIComponent(toAddress) +
	                          "&outboundtest_from_address=" + fromAddress + "&mail_from_name=" + from_name;

		YAHOO.util.Connect.asyncRequest("POST", "index.php?action=testOutboundEmail&module=EmailMan&to_pdf=true&sugar_body_only=true", callbackOutboundTest, postDataString);
	}
	function testOutboundSettingsDialog() {
	        // lazy load dialogue
	        if(!EmailMan.testOutboundDialog) {
	        	EmailMan.testOutboundDialog = new YAHOO.widget.Dialog("testOutboundDialog", {
	                modal:true,
					visible:true,
	            	fixedcenter:true,
	            	constraintoviewport: true,
	                width	: 600,
	                shadow	: false
	            });
	            EmailMan.testOutboundDialog.setHeader("{/literal}{$APP.LBL_EMAIL_TEST_OUTBOUND_SETTINGS}{literal}");
	            YAHOO.util.Dom.removeClass("testOutboundDialog", "yui-hidden");
	        } // end lazy load

	        EmailMan.testOutboundDialog.render();
	        EmailMan.testOutboundDialog.show();
	} // fn

	function overlay(reqtitle, body, type) {
	    var config = { };
	    config.type = type;
	    config.title = reqtitle;
	    config.msg = body;
	    YAHOO.SUGAR.MessageBox.show(config);
	}

	function hideOverlay() {
		YAHOO.SUGAR.MessageBox.hide();
	}
	function notify_setrequired(f) {

		document.getElementById("smtp_settings").style.display = (f.mail_sendtype.value == "SMTP") ? "inline" : "none";
		document.getElementById("smtp_settings").style.visibility = (f.mail_sendtype.value == "SMTP") ? "visible" : "hidden";
		document.getElementById("smtp_auth1").style.display = (document.getElementById('mail_smtpauth_req').checked) ? "" : "none";
		document.getElementById("smtp_auth1").style.visibility = (document.getElementById('mail_smtpauth_req').checked) ? "visible" : "hidden";
		document.getElementById("smtp_auth2").style.display = (document.getElementById('mail_smtpauth_req').checked) ? "" : "none";
		document.getElementById("smtp_auth2").style.visibility = (document.getElementById('mail_smtpauth_req').checked) ? "visible" : "hidden";
		if( document.getElementById('mail_smtpauth_req').checked)
		   YAHOO.util.Dom.removeClass('mail_allow_user', "yui-hidden");
		else
		   YAHOO.util.Dom.addClass("mail_allow_user", "yui-hidden");

		return true;
	}

	var first_load = true;
	function setDefaultSMTPPort() 
	{
	    if (!first_load)
	    {
	        useSSLPort = !document.getElementById("mail_smtpssl").options[0].selected;

	        if ( useSSLPort && document.getElementById("mail_smtpport").value == '25' ) {
	            document.getElementById("mail_smtpport").value = '465';
	        }
	        if ( !useSSLPort && document.getElementById("mail_smtpport").value == '465' ) {
	            document.getElementById("mail_smtpport").value = '25';
	        }
	    }
	    else
	    {
	        first_load = false;
	    }
	}
	notify_setrequired(document.ConfigureSettings);
	function changeEmailScreenDisplay(smtptype, clear)
	{
	    if(clear) {
		    document.getElementById("mail_smtpserver").value = '';
		    document.getElementById("mail_smtpport").value = '25';
		    document.getElementById("mail_smtpauth_req").checked = true;
		    document.getElementById("mailsettings1").style.display = '';
		    document.getElementById("mailsettings2").style.display = '';
		    document.getElementById("mail_smtppass_label").innerHTML = '{/literal}{$MOD.LBL_MAIL_SMTPPASS}{literal}';
		    document.getElementById("mail_smtpport_label").innerHTML = '{/literal}{$MOD.LBL_MAIL_SMTPPORT}{literal}';
		    document.getElementById("mail_smtpserver_label").innerHTML = '{/literal}SMTP Mail Server:{literal}';
		    document.getElementById("mail_smtpuser_label").innerHTML = '{/literal}{$MOD.LBL_MAIL_SMTPUSER}{literal}';
	    }

	    switch (smtptype) {
	    case "yahoomail":
	        document.getElementById("mail_smtpserver").value = 'smtp.mail.yahoo.com';
	        document.getElementById("mail_smtpport").value = '465';
	        document.getElementById("mail_smtpauth_req").checked = true;
	        var ssl = document.getElementById("mail_smtpssl");
	        for(var j=0;j<ssl.options.length;j++) {
	            if(ssl.options[j].text == 'SSL') {
	                ssl.options[j].selected = true;
	                break;
	            }
	        }
	        document.getElementById("mailsettings1").style.display = 'none';
	        document.getElementById("mailsettings2").style.display = 'none';
	        document.getElementById("mail_smtppass_label").innerHTML =
	        document.getElementById("mail_smtppass_label").innerHTML = '{/literal}{$MOD.LBL_YAHOOMAIL_SMTPPASS}{literal}';
	        document.getElementById("mail_smtpuser_label").innerHTML = '{/literal}{$MOD.LBL_YAHOOMAIL_SMTPUSER}{literal}';
	        break;
	    case "gmail":
	        if(document.getElementById("mail_smtpserver").value == "" || document.getElementById("mail_smtpserver").value == 'smtp.mail.yahoo.com') {
	            document.getElementById("mail_smtpserver").value = 'smtp.gmail.com';
	            document.getElementById("mail_smtpport").value = '587';
	            document.getElementById("mail_smtpauth_req").checked = true;
	            var ssl = document.getElementById("mail_smtpssl");
	            for(var j=0;j<ssl.options.length;j++) {
	                if(ssl.options[j].text == 'TLS') {
	                    ssl.options[j].selected = true;
	                    break;
	                }
	            }
	        }
	        //document.getElementById("mailsettings1").style.display = 'none';
	        //document.getElementById("mailsettings2").style.display = 'none';
	        document.getElementById("mail_smtppass_label").innerHTML = '{/literal}{$MOD.LBL_GMAIL_SMTPPASS}{literal}';
	        document.getElementById("mail_smtpuser_label").innerHTML = '{/literal}{$MOD.LBL_GMAIL_SMTPUSER}{literal}';
	        break;
	    case "exchange":
	        if ( document.getElementById("mail_smtpserver").value == 'smtp.mail.yahoo.com'
	                || document.getElementById("mail_smtpserver").value == 'smtp.gmail.com' ) {
	            document.getElementById("mail_smtpserver").value = '';
	        }
	        //document.getElementById("mail_smtpport").value = '25';
	        //document.getElementById("mail_smtpauth_req").checked = true; bug 40998
	        document.getElementById("mailsettings1").style.display = '';
	        document.getElementById("mailsettings2").style.display = '';
	        document.getElementById("mail_smtppass_label").innerHTML = '{/literal}{$MOD.LBL_EXCHANGE_SMTPPASS}{literal}';
	        document.getElementById("mail_smtpport_label").innerHTML = '{/literal}{$MOD.LBL_EXCHANGE_SMTPPORT}{literal}';
	        document.getElementById("mail_smtpserver_label").innerHTML = '{/literal}SMTP Mail Server:{literal}';
	        document.getElementById("mail_smtpuser_label").innerHTML = '{/literal}{$MOD.LBL_EXCHANGE_SMTPUSER}{literal}';
	        break;
	    }
	    setDefaultSMTPPort();
	    notify_setrequired(document.ConfigureSettings);
	}
	var oButtonGroup = new YAHOO.widget.ButtonGroup("smtpButtonGroup");
	oButtonGroup.subscribe('checkedButtonChange', function(e)
	{
	    changeEmailScreenDisplay(e.newValue.get('value'), true);
	    document.getElementById('smtp_settings').style.display = '';
	    document.getElementById('EditView').mail_smtptype.value = e.newValue.get('value');
	});
	YAHOO.widget.Button.addHiddenFieldsToForm(document.ConfigureSettings);
	if(window.addEventListener){
	    window.addEventListener("load", function() { SUGAR.util.setEmailPasswordDisplay('mail_smtppass', {/literal}{$mail_haspass}{literal}); }, false);
	}else{
	    window.attachEvent("onload", function() { SUGAR.util.setEmailPasswordDisplay('mail_smtppass', {/literal}{$mail_haspass}{literal}); });
	}
	{/literal}{if !empty($mail_smtptype)}{literal}
	changeEmailScreenDisplay("{/literal}{$mail_smtptype}{literal}", false);
	{/literal}{/if}{literal}
	$('#mail_smtpauth_req').on('change',function(){
		if($(this).is(':checked')){
	        $('#mail_smtpauth_req').val('1'); 
	    }else{
	        $('#mail_smtpauth_req').val('0'); 
	    }//end of else
	});
</script>
{/literal}