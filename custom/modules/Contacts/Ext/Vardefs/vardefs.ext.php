<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-05-05 11:41:20
$dictionary['Contact']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:20
$dictionary['Contact']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:20
$dictionary['Contact']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2021-05-05 11:41:20
$dictionary['Contact']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2021-05-16 20:56:35
$dictionary['Contact']['fields']['phone_mobile']['required']=true;
$dictionary['Contact']['fields']['phone_mobile']['inline_edit']=true;
$dictionary['Contact']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Contact']['fields']['phone_mobile']['merge_filter']='disabled';

 

 // created: 2021-09-03 20:00:50
$dictionary['Contact']['fields']['stakeholder_type_c']['inline_edit']='1';
$dictionary['Contact']['fields']['stakeholder_type_c']['labelValue']='Stakeholder Type';

 

 // created: 2021-09-03 20:00:50
$dictionary['Contact']['fields']['stp_stakeholder_type_id_c']['inline_edit']=1;

 

 // created: 2021-05-16 20:56:57
$dictionary['Contact']['fields']['title']['required']=true;
$dictionary['Contact']['fields']['title']['inline_edit']=true;
$dictionary['Contact']['fields']['title']['comments']='The title of the contact';
$dictionary['Contact']['fields']['title']['merge_filter']='disabled';

 
?>