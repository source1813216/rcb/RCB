<?php
// created: 2021-09-03 20:01:46
$mod_strings = array (
  'LBL_PANEL_ASSIGNMENT' => 'Record Info',
  'LBL_MOBILE_PHONE' => 'Mobile:',
  'LBL_TITLE' => 'Title:',
  'LBL_ACCOUNT_NAME' => 'Client Name:',
  'LBL_ACCOUNT_ID' => 'Client ID:',
  'LBL_ACCOUNT' => 'Client',
  'LBL_LIST_ACCOUNT_NAME' => 'Client Name',
  'LBL_AOS_QUOTES' => 'Initial Event Approval',
  'LBL_CASES' => 'Case',
  'LBL_CASES_SUBPANEL_TITLE' => 'Cases',
  'LBL_AOS_INVOICES' => 'Bid Presentation',
  'LBL_AOS_CONTRACTS' => 'Site Inspection',
  'LBL_STAKEHOLDER_TYPE_STP_STAKEHOLDER_TYPE_ID' => '&#039;Stakeholder Type&#039; (related &#039;&#039; ID)',
  'LBL_STAKEHOLDER_TYPE' => 'Stakeholder Type',
  'LBL_EDITVIEW_PANEL1' => 'Stakeholder',
  'LBL_DETAILVIEW_PANEL1' => 'Stakeholder',
);