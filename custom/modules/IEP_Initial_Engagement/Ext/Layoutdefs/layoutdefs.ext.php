<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-05-18 17:58:42
$layout_defs["IEP_Initial_Engagement"]["subpanel_setup"]['iep_initial_engagement_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_IEP_INITIAL_ENGAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'iep_initial_engagement_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    /*1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ), */
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['IEP_Initial_Engagement']['subpanel_setup']['iep_initial_engagement_documents_1']['override_subpanel_name'] = 'IEP_Initial_Engagement_subpanel_iep_initial_engagement_documents_1';

?>