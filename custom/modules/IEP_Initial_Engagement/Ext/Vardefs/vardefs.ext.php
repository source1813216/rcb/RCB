<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-05-18 17:58:42
$dictionary["IEP_Initial_Engagement"]["fields"]["iep_initial_engagement_documents_1"] = array (
  'name' => 'iep_initial_engagement_documents_1',
  'type' => 'link',
  'relationship' => 'iep_initial_engagement_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_IEP_INITIAL_ENGAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2021-05-18 07:14:04
$dictionary["IEP_Initial_Engagement"]["fields"]["leads_iep_initial_engagement_1"] = array (
  'name' => 'leads_iep_initial_engagement_1',
  'type' => 'link',
  'relationship' => 'leads_iep_initial_engagement_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_IEP_INITIAL_ENGAGEMENT_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_iep_initial_engagement_1leads_ida',
);
$dictionary["IEP_Initial_Engagement"]["fields"]["leads_iep_initial_engagement_1_name"] = array (
  'name' => 'leads_iep_initial_engagement_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_IEP_INITIAL_ENGAGEMENT_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_iep_initial_engagement_1leads_ida',
  'link' => 'leads_iep_initial_engagement_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["IEP_Initial_Engagement"]["fields"]["leads_iep_initial_engagement_1leads_ida"] = array (
  'name' => 'leads_iep_initial_engagement_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_iep_initial_engagement_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_IEP_INITIAL_ENGAGEMENT_1_FROM_IEP_INITIAL_ENGAGEMENT_TITLE',
);


 // created: 2021-05-31 18:44:12
$dictionary['IEP_Initial_Engagement']['fields']['client_responded_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['client_responded_c']['labelValue']='Client Responded';

 

 // created: 2021-05-22 20:10:15
$dictionary['IEP_Initial_Engagement']['fields']['client_response_details_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['client_response_details_c']['labelValue']='Client Response Details';

 

 // created: 2021-05-18 07:48:35
$dictionary['IEP_Initial_Engagement']['fields']['contact_first_name_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['contact_first_name_c']['labelValue']='Contact First Name';

 

 // created: 2021-05-18 07:26:30
$dictionary['IEP_Initial_Engagement']['fields']['contact_id_c']['inline_edit']=1;

 

 // created: 2021-05-20 12:23:20
$dictionary['IEP_Initial_Engagement']['fields']['convert_lead_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['convert_lead_c']['labelValue']='Convert Lead';

 

 // created: 2021-05-22 11:46:28
$dictionary['IEP_Initial_Engagement']['fields']['edit_followup_template_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['edit_followup_template_c']['labelValue']='Edit Follow-up Template';

 

 // created: 2021-05-22 19:10:35
$dictionary['IEP_Initial_Engagement']['fields']['email_sent_status_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['email_sent_status_c']['labelValue']='Email Sent Status';

 

 // created: 2021-05-20 13:01:09
$dictionary['IEP_Initial_Engagement']['fields']['followup_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['followup_c']['labelValue']='Is it a Follow-Up?';

 

 // created: 2021-05-21 13:09:02
$dictionary['IEP_Initial_Engagement']['fields']['followup_count_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['followup_count_c']['labelValue']='Number of Follow-Ups';

 

 // created: 2021-05-22 17:51:50
$dictionary['IEP_Initial_Engagement']['fields']['followup_template_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['followup_template_c']['labelValue']='Follow-up Template';

 

 // created: 2021-05-18 07:14:59
$dictionary['IEP_Initial_Engagement']['fields']['name']['inline_edit']=true;
$dictionary['IEP_Initial_Engagement']['fields']['name']['duplicate_merge']='disabled';
$dictionary['IEP_Initial_Engagement']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['IEP_Initial_Engagement']['fields']['name']['merge_filter']='disabled';
$dictionary['IEP_Initial_Engagement']['fields']['name']['unified_search']=false;

 

 // created: 2021-05-19 04:33:39
$dictionary['IEP_Initial_Engagement']['fields']['no_reponse_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['no_reponse_c']['labelValue']='Continue?';

 

 // created: 2021-05-21 12:20:00
$dictionary['IEP_Initial_Engagement']['fields']['resend_email_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['resend_email_c']['labelValue']='Resend Client Email';

 

 // created: 2021-05-20 14:04:32
$dictionary['IEP_Initial_Engagement']['fields']['resend_original_email_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['resend_original_email_c']['labelValue']='Resend Original Email';

 

 // created: 2021-05-18 07:54:31
$dictionary['IEP_Initial_Engagement']['fields']['sender_email_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['sender_email_c']['labelValue']='Sender Email';

 

 // created: 2021-05-18 07:50:06
$dictionary['IEP_Initial_Engagement']['fields']['sender_full_name_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['sender_full_name_c']['labelValue']='Sender Full Name';

 

 // created: 2021-05-18 07:49:27
$dictionary['IEP_Initial_Engagement']['fields']['sender_mobile_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['sender_mobile_c']['labelValue']='Sender Mobile';

 

 // created: 2021-05-18 07:49:46
$dictionary['IEP_Initial_Engagement']['fields']['sender_title_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['sender_title_c']['labelValue']='Sender Title';

 

 // created: 2021-05-18 07:27:52
$dictionary['IEP_Initial_Engagement']['fields']['send_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['send_c']['labelValue']='SEND';

 

 // created: 2021-05-18 07:26:59
$dictionary['IEP_Initial_Engagement']['fields']['sent_from_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['sent_from_c']['labelValue']='Sent From';

 

 // created: 2021-05-18 07:26:30
$dictionary['IEP_Initial_Engagement']['fields']['sent_to_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['sent_to_c']['labelValue']='Sent To';

 

 // created: 2021-05-18 07:27:18
$dictionary['IEP_Initial_Engagement']['fields']['subject_c']['inline_edit']='1';
$dictionary['IEP_Initial_Engagement']['fields']['subject_c']['labelValue']='Subject';

 

 // created: 2021-05-18 07:26:59
$dictionary['IEP_Initial_Engagement']['fields']['user_id_c']['inline_edit']=1;

 
?>