<?php
$module_name = 'IEP_Initial_Engagement';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'sent_to_c',
            'studio' => 'visible',
            'label' => 'LBL_SENT_TO',
          ),
          1 => 
          array (
            'name' => 'sent_from_c',
            'studio' => 'visible',
            'label' => 'LBL_SENT_FROM',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'subject_c',
            'label' => 'LBL_SUBJECT',
          ),
          1 => 
          array (
            'name' => 'send_c',
            'studio' => 'visible',
            'label' => 'LBL_SEND',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
