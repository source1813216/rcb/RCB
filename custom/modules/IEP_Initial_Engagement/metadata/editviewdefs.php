<?php
$module_name = 'IEP_Initial_Engagement';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/IEP_Initial_Engagement/js/techinc-edit.js',
        ),
      ),
    ),
   
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'subject_c',
            'label' => 'LBL_SUBJECT',
          ),
          1 => 
          array (
            'name' => 'sent_to_c',
            'studio' => 'visible',
            'label' => 'LBL_SENT_TO',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'followup_c',
            'studio' => 'visible',
            'label' => 'LBL_FOLLOWUP',
          ),
          1 => 
          array (
            'name' => 'send_c',
            'studio' => 'visible',
            'label' => 'LBL_SEND',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'edit_followup_template_c',
            'label' => 'LBL_EDIT_FOLLOWUP_TEMPLATE',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'followup_template_c',
            'studio' => 'visible',
            'label' => 'LBL_FOLLOWUP_TEMPLATE',
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'resend_email_c',
            'studio' => 'visible',
            'label' => 'LBL_RESEND_EMAIL',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'convert_lead_c',
            'studio' => 'visible',
            'label' => 'LBL_CONVERT_LEAD',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
