{*
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
*}
{php}
global $current_user,$sugar_config;
$this->_tpl_vars['current_user'] = $current_user->id;
$this->_tpl_vars['is_admin_user'] = $current_user->is_admin;
$this->_tpl_vars['current_theme'] = $current_user->getPreference('user_theme');
$this->_tpl_vars['suite_valid_version'] =  true;
if(preg_match('/(7\.7\.[0-9])/', $sugar_config['suitecrm_version']) || $sugar_config['suitecrm_version'] == '7.7'){
    $this->_tpl_vars['suite_valid_version'] = false;
}
{/php}
{if $current_theme == 'SuiteP'}
{{sugar_include type="smarty" file=$headerTpl}}
{sugar_include include=$includes}
<div class="detail-view">
    {if $suite_valid_version}
    <div class="mobile-pagination">{$PAGINATION}</div>
    {/if}
    {*display tabs*}
    {{counter name="tabCount" start=-1 print=false assign="tabCount"}}
    <ul class="nav nav-tabs">
        {{if $useTabs}}
        {{counter name="isection" start=0 print=false assign="isection"}}
        {{foreach name=section from=$sectionPanels key=label item=panel}}
        {{capture name=label_upper assign=label_upper}}{{$label|upper}}{{/capture}}
        {* if tab *}
        {{if (isset($tabDefs[$label_upper].newTab) && $tabDefs[$label_upper].newTab == true)}}
        {*if tab display*}
        {{counter name="tabCount" print=false}}
        {{if $tabCount == '0'}}
        <li role="presentation" class="active">
            <a id="tab{{$tabCount}}" data-toggle="tab" class="hidden-xs">
                {sugar_translate label='{{$label}}' module='{{$module}}'}
            </a>

            {* Count Tabs *}
            {{counter name="tabCountOnlyXS" start=-1 print=false assign="tabCountOnlyXS"}}
            {{foreach name=sectionOnlyXS from=$sectionPanels key=labelOnly item=panelOnlyXS}}
            {{capture name=label_upper_count_only assign=label_upper_count_only}}{{$labelOnly|upper}}{{/capture}}
            {{if (isset($tabDefs[$label_upper_count_only].newTab) && $tabDefs[$label_upper_count_only].newTab == true)}}
            {{counter name="tabCountOnlyXS" print=false}}
            {{/if}}
            {{/foreach}}

            {*
                For the mobile view, only show the first tab has a drop down when:
                * There is more than one tab set
                * When Acton Menu's are enabled
            *}
            <a id="xstab{{$tabCount}}" href="#" class="visible-xs first-tab{{if $tabCountOnlyXS > 0}}-xs{{/if}} dropdown-toggle" data-toggle="dropdown">
                {sugar_translate label='{{$label}}' module='{{$module}}'}
            </a>
            {{if $tabCountOnlyXS > 0}}
            <ul id="first-tab-menu-xs" class="dropdown-menu">
                {{counter name="tabCountXS" start=1 print=false assign="tabCountXS"}}
                {{foreach name=sectionXS from=$sectionPanels key=label item=panelXS}}
                {{capture name=label_upper_xs assign=label_upper_xs}}{{$label|upper}}{{/capture}}
                {{if (isset($tabDefs[$label_upper_xs].newTab) && $tabDefs[$label_upper_xs].newTab == true)}}
                <li role="presentation">
                    <a id="tab{{$tabCountXS}}" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-{{$tabCountXS}}');">
                        {sugar_translate label='{{$label}}' module='{{$module}}'}
                    </a>
                </li>
                {{/if}}
                {{counter name="tabCountXS" print=false}}
                {{/foreach}}
            </ul>
            {{/if}}

        </li>
        {{else}}
        <li role="presentation" class="hidden-xs">
            <a id="tab{{$tabCount}}" data-toggle="tab">
                {sugar_translate label='{{$label}}' module='{{$module}}'}
            </a>
        </li>
        {{/if}}
        {{else}}
        {* if panel skip*}
        {{/if}}
        {{/foreach}}
        {{else}}
        {*
            Since: SuieCRM 7.8
            When action menus are enabled and When there are only panels and there are not any tabs,
            make the first panel a tab so that the action menu looks correct. This is regardless of what the
            meta/studio defines the first panel should always be tab.
        *}
        {if $config.enable_action_menu and $config.enable_action_menu != false}
            {{foreach name=section from=$sectionPanels key=label item=panel}}
            {{capture name=label_upper assign=label_upper}}{{$label|upper}}{{/capture}}
            {{counter name="tabCount" print=false}}
            {{if $tabCount == '0'}}
            <li role="presentation" class="active">
                <a id="tab{{$tabCount}}" data-toggle="tab" class="hidden-xs">
                    {sugar_translate label='{{$label}}' module='{{$module}}'}
                </a>
                <a id="xstab{{$tabCount}}" href="#" class="visible-xs first-tab dropdown-toggle" data-toggle="dropdown">
                    {sugar_translate label='{{$label}}' module='{{$module}}'}
                </a>
            </li>
            {{else}}
            {* if panel skip *}
            {{/if}}
        {{/foreach}}
        {/if}
        {{/if}}
        {if $config.enable_action_menu and $config.enable_action_menu != false}
        <li id="tab-actions" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{$APP.LBL_LINK_ACTIONS}}<span class="suitepicon suitepicon-action-caret"></span></a>
            {{include file="themes/SuiteP/include/DetailView/actions_menu.tpl"}}
        </li>
        {if $suite_valid_version}
        <li class="tab-inline-pagination">
            {{if $panelCount == 0}}
            {{* Render tag for VCR control if SHOW_VCR_CONTROL is true *}}
            {{if $SHOW_VCR_CONTROL}}
            {$PAGINATION}
            {{/if}}
            {{counter name="panelCount" print=false}}
            {{/if}}
        </li>
        {/if}
        {/if}
    </ul>
    {{counter name="tabCount" start=0 print=false assign="tabCount"}}
    <div class="clearfix"></div>
    {{if $useTabs}}
    {*<!-- TAB CONTENT USE TABS -->*}
    <div class="tab-content">
        {{else}}
        {*
           Since: SuieCRM 7.8
           When action menus are enabled and When there are only panels and there are not any tabs,
           make the first panel a tab so that the action menu looks correct. This is regardless of what the
           meta/studio defines the first panel should always be tab.
       *}
        {if $config.enable_action_menu and $config.enable_action_menu != false}
        {{if tabCount == 0}}
        {*<!-- TAB CONTENT USE TABS -->*}
        <div class="tab-content">
            {{else}}
            {*<!-- TAB CONTENT DOESN'T USE TABS -->*}
            <div class="tab-content" style="padding: 0; border: 0;">
                {{/if}}
                {else}
                {*<!-- TAB CONTENT DOESN'T USE TABS -->*}
                <div class="tab-content" style="padding: 0; border: 0;">
                    {/if}
                    {{/if}}
                    {* Loop through all top level panels first *}
                    {{if $useTabs}}
                    {{foreach name=section from=$sectionPanels key=label item=panel}}
                    {{capture name=label_upper assign=label_upper}}{{$label|upper}}{{/capture}}
                    {{if isset($tabDefs[$label_upper].newTab) && $tabDefs[$label_upper].newTab == true}}
                    {{if $tabCount == '0'}}
                    <div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-{{$tabCount}}'>
                        {{include file='custom/themes/tab_panel/tab_panel_content_detail.tpl'}}
                    </div>
                    {{else}}
                    <div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-{{$tabCount}}'>
                        {{include file='custom/themes/tab_panel/tab_panel_content_detail.tpl'}}
                    </div>
                    {{/if}}
                    {{/if}}
                    {{counter name="tabCount" print=false}}
                    {{/foreach}}
                    {{else}}
                    {*
                       Since: SuieCRM 7.8
                       When action menus are enabled and When there are only panels and there are not any tabs,
                       make the first panel a tab so that the action menu looks correct. This is regardless of what the
                       meta/studio defines the first panel should always be tab.
                   *}
                    {if $config.enable_action_menu and $config.enable_action_menu != false}
                        {{foreach name=section from=$sectionPanels key=label item=panel}}
                        {{capture name=label_upper assign=label_upper}}{{$label|upper}}{{/capture}}
                        {{if $tabCount == '0'}}
                        <div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-{{$tabCount}}'>
                            {{include file='custom/themes/tab_panel/tab_panel_content_detail.tpl'}}
                        </div>
                        {{else}}

                        {{/if}}
                    {{counter name="tabCount" print=false}}
                    {{/foreach}}
                    {else}
                    {*<!-- TAB CONTENT DOESN'T USE TABS -->*}
                    <div class="tab-pane-NOBOOTSTRAPTOGGLER panel-collapse"></div>
                    {/if}
                    {{/if}}
                </div>
                {*display panels*}
                <div class="panel-content">
                    <div>&nbsp;</div>
                    {{counter name="panelCount" start=-1 print=false assign="panelCount"}}
                    {{foreach name=section from=$sectionPanels key=label item=panel}}

                    {{capture name=label_upper assign=label_upper}}{{$label|upper}}{{/capture}}
                    {* if tab *}
                    {{if (isset($tabDefs[$label_upper].newTab) && $tabDefs[$label_upper].newTab == true && $useTabs)}}
                    {*if tab skip*}
                    {{else}}
                    {* if panel display*}
                    {*if panel collasped*}
                    {{if (isset($tabDefs[$label_upper].panelDefault) && $tabDefs[$label_upper].panelDefault == "collapsed") }}
                    {*collapse panel*}
                    {{assign var='collapse' value="panel-collapse collapse"}}
                    {{assign var='collapsed' value="collapsed"}}
                    {{assign var='collapseIcon' value="glyphicon glyphicon-plus"}}
                    {{assign var='panelHeadingCollapse' value="panel-heading-collapse"}}
                    {{else}}
                    {*expand panel*}
                    {{assign var='collapse' value="panel-collapse collapse in"}}
                    {{assign var='collapseIcon' value="glyphicon glyphicon-minus"}}
                    {{assign var='panelHeadingCollapse' value=""}}
                    {{/if}}
                    {{if $label != "LBL_AOP_CASE_UPDATES"}}
                    {{assign var='panelId' value="top-panel-$panelCount"}}
                    {{else}}
                    {{assign var='panelId' value="LBL_AOP_CASE_UPDATES"}}
                    {{/if}}

                    {*
                       Since: SuieCRM 7.8
                       When action menus are enabled and When there are only panels and there are not any tabs,
                       make the first panel a tab so that the action menu looks correct. This is regardless of what the
                       meta/studio defines the first panel should always be tab.
                    *}
                    {if $config.enable_action_menu and $config.enable_action_menu != false}
                        {{if $panelCount == -1}}
                        {* skip panel as it has been converted to a tab*}
                        {{else}}
                        {* display panels as they have always been displayed *}
                        <div class="panel panel-default">
                            <div class="panel-heading {{$panelHeadingCollapse}}">
                                <a class="{{$collapsed}}" role="button" data-toggle="collapse" href="#{{$panelId}}" aria-expanded="false">
                                    <div class="col-xs-10 col-sm-11 col-md-11">
                                        {sugar_translate label='{{$label}}' module='{{$module}}'}
                                    </div>
                                </a>
                            </div>
                            <div class="panel-body {{$collapse}} panelContainer" id="{{$panelId}}"  data-id="{{$label_upper}}">
                                <div class="tab-content">
                                    <!-- TAB CONTENT -->
                                    {{include file='custom/themes/tab_panel/tab_panel_content_detail.tpl'}}
                                </div>
                            </div>
                        </div>
                        {{/if}}
                    {else}
                    {* display panels as they have always been displayed *}
                    <div class="panel panel-default">
                        <div class="panel-heading {{$panelHeadingCollapse}}">
                            <a class="{{$collapsed}}" role="button" data-toggle="collapse" href="#{{$panelId}}" aria-expanded="false">
                                <div class="col-xs-10 col-sm-11 col-md-11">
                                    {sugar_translate label='{{$label}}' module='{{$module}}'}
                                </div>
                            </a>

                        </div>
                        <div class="panel-body {{$collapse}} panelContainer" id="{{$panelId}}" data-id="{{$label_upper}}">
                            <div class="tab-content">
                                <!-- TAB CONTENT -->
                                {{include file='custom/themes/tab_panel/tab_panel_content_detail.tpl'}}
                            </div>
                        </div>
                    </div>
                    {/if}


                    {{/if}}
                    {{counter name="panelCount" print=false}}
                    {{/foreach}}
                </div>
            </div>

            {{include file=$footerTpl}}
            <script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
            <script type="text/javascript" src="modules/Favorites/favorites.js"></script>

            {literal}

                <script type="text/javascript">

                    var selectTabDetailView = function(tab) {
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').hide();
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').eq(tab).show().addClass('active').addClass('in');
                    };

                    var selectTabOnError = function(tab) {
                        selectTabDetailView(tab);
                        $('#content ul.nav.nav-tabs > li').removeClass('active');
                        $('#content ul.nav.nav-tabs > li a').css('color', '');

                        $('#content ul.nav.nav-tabs > li').eq(tab).find('a').first().css('color', 'red');
                        $('#content ul.nav.nav-tabs > li').eq(tab).addClass('active');

                    };

                    var selectTabOnErrorInputHandle = function(inputHandle) {
                        var tab = $(inputHandle).closest('.tab-pane-NOBOOTSTRAPTOGGLER').attr('id').match(/^detailpanel_(.*)$/)[1];
                        selectTabOnError(tab);
                    };


                    $(function(){
                        $('#content ul.nav.nav-tabs > li > a[data-toggle="tab"]').click(function(e){
                            if(typeof $(this).parent().find('a').first().attr('id') != 'undefined') {
                                var tab = parseInt($(this).parent().find('a').first().attr('id').match(/^tab(.)*$/)[1]);
                                selectTabDetailView(tab);
                            }
                        });
                    });

                </script>

            {/literal}
{else}
{{include file=$headerTpl}}
{sugar_include include=$includes}
<div id="{{$module}}_detailview_tabs"
{{if $useTabs}}
class="yui-navset detailview_tabs"
{{/if}}
>
    {{if $useTabs}}
    {* Generate the Tab headers *}
    {{counter name="tabCount" start=-1 print=false assign="tabCount"}}
    <ul class="yui-nav">
    {{foreach name=section from=$sectionPanels key=label item=panel}}
        {{capture name=label_upper assign=label_upper}}{{$label|upper}}{{/capture}}
        {* override from tab definitions *}
        {{if (isset($tabDefs[$label_upper].newTab) && $tabDefs[$label_upper].newTab == true)}}
            {{counter name="tabCount" print=false}}
            <li><a id="tab{{$tabCount}}" href="javascript:void(0)"><em>{sugar_translate label='{{$label}}' module='{{$module}}'}</em></a></li>
        {{/if}}
    {{/foreach}}
    </ul>
    {{/if}}
    <div {{if $useTabs}}class="yui-content"{{/if}}>
{{* Loop through all top level panels first *}}
{{counter name="panelCount" print=false start=0 assign="panelCount"}}
{{counter name="tabCount" start=-1 print=false assign="tabCount"}}
{{foreach name=section from=$sectionPanels key=label item=panel}}
{{assign var='panel_id' value=$panelCount}}
{{capture name=label_upper assign=label_upper}}{{$label|upper}}{{/capture}}
  {{if (isset($tabDefs[$label_upper].newTab) && $tabDefs[$label_upper].newTab == true)}}
    {{counter name="tabCount" print=false}}
    {{if $tabCount != 0}}</div>{{/if}}
    <div id='tabcontent{{$tabCount}}'>
  {{/if}}

    {{if ( isset($tabDefs[$label_upper].panelDefault) && $tabDefs[$label_upper].panelDefault == "collapsed" && isset($tabDefs[$label_upper].newTab) && $tabDefs[$label_upper].newTab == false) }}
        {{assign var='panelState' value=$tabDefs[$label_upper].panelDefault}}
    {{else}}
        {{assign var='panelState' value="expanded"}}
    {{/if}}
<div id='detailpanel_{{$smarty.foreach.section.iteration}}' class='detail view  detail508 {{$panelState}}'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
{{* Print out the panel title if one exists*}}

{{* Check to see if the panel variable is an array, if not, we'll attempt an include with type param php *}}
{{* See function.sugar_include.php *}}
{{if !is_array($panel)}}
    {sugar_include type='php' file='{{$panel}}'}
{{else}}

    {{if !empty($label) && !is_int($label) && $label != 'DEFAULT' && (!isset($tabDefs[$label_upper].newTab) || (isset($tabDefs[$label_upper].newTab) && $tabDefs[$label_upper].newTab == false))}}
    <h4>
      <a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel({{$smarty.foreach.section.iteration}});">
      <img border="0" id="detailpanel_{{$smarty.foreach.section.iteration}}_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
      <a href="javascript:void(0)" class="expandLink" onclick="expandPanel({{$smarty.foreach.section.iteration}});">
      <img border="0" id="detailpanel_{{$smarty.foreach.section.iteration}}_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
      {sugar_translate label='{{$label}}' module='{{$module}}'}
    {{if isset($panelState) && $panelState == 'collapsed'}}
    <script>
      document.getElementById('detailpanel_{{$smarty.foreach.section.iteration}}').className += ' collapsed';
    </script>
    {{else}}
    <script>
      document.getElementById('detailpanel_{{$smarty.foreach.section.iteration}}').className += ' expanded';
    </script>
    {{/if}}
    </h4>

    {{/if}}
	{{* Print out the table data *}}
	<!-- PANEL CONTAINER HERE.. -->
  <table id='{{$label}}' class="panelContainer" cellspacing='{$gridline}'>



	{{foreach name=rowIteration from=$panel key=row item=rowData}}
	{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
	{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
	{capture name="tr" assign="tableRow"}
	<tr>
		{{assign var='columnsInRow' value=$rowData|@count}}
		{{assign var='columnsUsed' value=0}}
		{{foreach name=colIteration from=$rowData key=col item=colData}}
	    {{if !empty($colData.field.hideIf)}}
	    	{if !({{$colData.field.hideIf}}) }
	    {{/if}}
               {*Added By Biztech*}
               {{if !empty($colData.field.name) }}
                    {if $fields.{{$colData.field.name}}.acl > 1 || $fields.{{$colData.field.name}}.acl > 0}
                {{/if}}
                {*Added By Biztech*}
			{counter name="fieldsUsed"}
			{{if empty($colData.field.hideLabel)}}
			<td width='{{$def.templateMeta.widths[$smarty.foreach.colIteration.index].label}}%' scope="col">
				{{if !empty($colData.field.name)}}
				    {if !$fields.{{$colData.field.name}}.hidden}
                {{/if}}
				{{if isset($colData.field.customLabel)}}
			       {{$colData.field.customLabel}}
				{{elseif isset($colData.field.label) && strpos($colData.field.label, '$')}}
				   {capture name="label" assign="label"}{{$colData.field.label}}{/capture}
			       {$label|strip_semicolon}:
				{{elseif isset($colData.field.label)}}
				   {capture name="label" assign="label"}{sugar_translate label='{{$colData.field.label}}' module='{{$module}}'}{/capture}
			       {$label|strip_semicolon}:
				{{elseif isset($fields[$colData.field.name])}}
				   {capture name="label" assign="label"}{sugar_translate label='{{$fields[$colData.field.name].vname}}' module='{{$module}}'}{/capture}
			       {$label|strip_semicolon}:
				{{else}}
				   &nbsp;
				{{/if}}
                {{if isset($colData.field.popupHelp) || isset($fields[$colData.field.name]) && isset($fields[$colData.field.name].popupHelp) }}
                   {{if isset($colData.field.popupHelp) }}
                     {capture name="popupText" assign="popupText"}{sugar_translate label="{{$colData.field.popupHelp}}" module='{{$module}}'}{/capture}
                   {{elseif isset($fields[$colData.field.name].popupHelp)}}
                     {capture name="popupText" assign="popupText"}{sugar_translate label="{{$fields[$colData.field.name].popupHelp}}" module='{{$module}}'}{/capture}
                   {{/if}}
                   {sugar_help text=$popupText WIDTH=400}
                {{/if}}
                {*Added By Biztech*}
                {{if !empty($colData.field.name)}}
                            {else}
                                {counter name="fieldsHidden"}
                {*Added By Biztech*}
                {/if}
                {{/if}}
                {{/if}}
			</td>
                        {*Added By Biztech*}
			<td class="{{if $inline_edit && $fields[$colData.field.name].acl > 1 && !empty($colData.field.name) && ($fields[$colData.field.name].inline_edit == 1 || !isset($fields[$colData.field.name].inline_edit))}}inlineEdit{{/if}}" type="{{$fields[$colData.field.name].type}}" field="{{$fields[$colData.field.name].name}}" width='{{$def.templateMeta.widths[$smarty.foreach.colIteration.index].field}}%' {{if $colData.colspan}}colspan='{{$colData.colspan}}'{{/if}} {{if isset($fields[$colData.field.name].type) && $fields[$colData.field.name].type == 'phone'}}class="phone"{{/if}}>
			    {{if !empty($colData.field.name)}}
			    {if !$fields.{{$colData.field.name}}.hidden}
			    {{/if}}
				{{$colData.field.prefix}}
				{{if ($colData.field.customCode && !$colData.field.customCodeRenderField) || $colData.field.assign}}
					{counter name="panelFieldCount"}
					<span id="{{$colData.field.name}}" class="sugar_field">{{sugar_evalcolumn var=$colData.field colData=$colData}}</span>
				{{elseif $fields[$colData.field.name] && !empty($colData.field.fields) }}
				    {{foreach from=$colData.field.fields item=subField}}
				        {{if $fields[$subField]}}
				        	{counter name="panelFieldCount"}
				            {{sugar_field parentFieldArray='fields' tabindex=$tabIndex vardef=$fields[$subField] displayType='DetailView'}}&nbsp;

				        {{else}}
				        	{counter name="panelFieldCount"}
				            {{$subField}}
				        {{/if}}
				    {{/foreach}}
				{{elseif $fields[$colData.field.name]}}
					{counter name="panelFieldCount"}
					{{sugar_field parentFieldArray='fields' vardef=$fields[$colData.field.name] displayType='DetailView' displayParams=$colData.field.displayParams typeOverride=$colData.field.type}}

				{{/if}}
				{{if !empty($colData.field.customCode) && $colData.field.customCodeRenderField}}
				    {counter name="panelFieldCount"}
				    <span id="{{$colData.field.name}}" class="sugar_field">{{sugar_evalcolumn var=$colData.field colData=$colData}}</span>
                {{/if}}
				{{$colData.field.suffix}}
				{{if !empty($colData.field.name)}}
				{/if}
				{{/if}}
                                {*Added By Biztech*}
				{{if $inline_edit && $fields[$colData.field.name].acl > 1 && !empty($colData.field.name) && ($fields[$colData.field.name].inline_edit == 1 || !isset($fields[$colData.field.name].inline_edit))}}<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>{{/if}}
			</td>
                                {*Added By Biztech*}
                                {{if !empty($colData.field.name)}}
                                {else}

                                    <td>&nbsp;</td><td>&nbsp;</td>
                                    {/if}
	{{/if}}
            {*Added By Biztech*}
	    {{if !empty($colData.field.hideIf)}}
			{else}

			<td>&nbsp;</td><td>&nbsp;</td>
			{/if}
	    {{/if}}
		{{/foreach}}
	</tr>
	{/capture}
	{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
	{$tableRow}
	{/if}
	{{/foreach}}
	</table>
    {{if !empty($label) && !is_int($label) && $label != 'DEFAULT' && (!isset($tabDefs[$label_upper].newTab) || (isset($tabDefs[$label_upper].newTab) && $tabDefs[$label_upper].newTab == false))}}
    <script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel({{$smarty.foreach.section.iteration}}, '{{$panelState}}'); {rdelim}); </script>
    {{/if}}
{{/if}}
</div>
{if $panelFieldCount == 0}

<script>document.getElementById("{{$label}}").style.display='none';</script>
{/if}
{{/foreach}}
{{if $useTabs}}
  </div>
{{/if}}

</div>
</div>
{{include file=$footerTpl}}
{{if $useTabs}}
<script type='text/javascript' src='{sugar_getjspath file='include/javascript/popup_helper.js'}'></script>
<script type="text/javascript" src="{sugar_getjspath file='cache/include/javascript/sugar_grp_yui_widgets.js'}"></script>
<script type="text/javascript">
var {{$module}}_detailview_tabs = new YAHOO.widget.TabView("{{$module}}_detailview_tabs");
{{$module}}_detailview_tabs.selectTab(0);
</script>
{{/if}}
<script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
{/if}
