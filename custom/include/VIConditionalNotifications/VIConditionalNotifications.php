<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
* This file is part of package Conditional Notifications.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Conditional Notifications is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
class VIConditionalNotifications{
	function VIConditionalNotifications($event,$arguments){
		$notificationsLabel = translate('LBL_NOTIFICATIONS_LABLE','Administration');
		if($_REQUEST['action'] == 'EditView'){
			$randomNumber = rand();
			echo '<script type="text/javascript" src="custom/include/VIConditionalNotifications/VIConditionalNotificationsEditViewCheckCondition.js?v='.$randomNumber.'"></script>';
		?>
		<div id="ConditionalNotificationsDialog" class="modal fade modal-search in" tabindex="-1" role="dialog" style="display: none;">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btn_close"><span aria-hidden="true">×</span></button>
						<h4 class="modal-title"><b><?php echo $notificationsLabel; ?></b></h4>
					</div>
					<div class="modal-body" id="searchList">
					</div>
					<div class="modal-footer">
						<input title="Cancel [Alt+l]" accesskey="l" class="button" type="button" name="button" value="Cancel" id="btn_cancle">
					</div>
				</div>
			</div>
		</div>
		<?php
		}//end of if
		if($_REQUEST['action'] == 'DetailView' || $_REQUEST['action'] == 'view_GanttChart'){
			$randomNumber = rand();
		?>
			<div id="ConditionalNotificationsDetailViewDialog" class="modal fade modal-search in" tabindex="-1" role="dialog" style="display: none;">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closePopup()"><span aria-hidden="true">×</span></button>
							<h4 class="modal-title"><b><?php echo $notificationsLabel; ?></b></h4>
						</div>
						<div class="modal-body" id="searchList">
						</div>
						<div class="modal-footer">
							<input title="Ok" accesskey="l" class="button" type="button" name="button" value="Ok" id="btn_cancle" onclick="closePopup()">
						</div>
					</div>
				</div>
			</div>
		<?php
			echo '<script type="text/javascript" src="custom/include/VIConditionalNotifications/VIConditionalNotificationsDetailViewCheckCondition.js?v='.$randomNumber.'"></script>';
		}

		if($GLOBALS['app']->controller->action == 'index'){
			global $suitecrm_version;
			if (version_compare($suitecrm_version, '7.10.2', '>=')){
				echo '<link rel="stylesheet" type="text/css" href="custom/include/VIConditionalNotifications/VIConditionalNotificationsIconCss.css">';
			}
		}//end of if  
	}//end of function
}//end of class