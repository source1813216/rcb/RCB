/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
var decodeUrl = decodeURIComponent(window.location.href);
function getParam( name )
{
     name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
     var regexS = "[\\?&]"+name+"=([^&#]*)";
     var regex = new RegExp( regexS );
     var results = regex.exec(decodeUrl);
     if( results == null )
      return "";
     else
     return results[1];
}
var recordId;
var recordId = getParam("record");
var moduleName;
var duplicateRecord;
var isDuplicate;
$.each($("form").serializeArray(), function (i, field) {
        if(field.name == 'module'){
            moduleName = field.value;
        }
        if(field.name == 'isDuplicate'){
            isDuplicate = field.value;
        }
        if(field.name == 'duplicateSave'){
            duplicateRecord = 1;
        }
        if(field.name == 'record'){
          if(field.value != ''){
            recordId = field.value;
          }
        }
});
if(duplicateRecord == 1 || isDuplicate == 'true'){
    var formData = $("#EditView").serialize();
    $.ajax({
              url: "index.php?entryPoint=VIConditionalNotificationsDuplicateCheckCondition",
              type: "post",
              data: {module : moduleName,
                    formData : formData
                    },
              dataType : "JSON",
              success: function(data) {
                if(data != null && data != ''){
                  $('#ConditionalNotificationsDialog').attr('style', 'display: block;');
                  var confirmationDialog = 0;
                    $.each(data, function (index, value) {
                      var actionTitle = value.actionTitle;
                      var message = value.bodyHTML;
                      if(value.notificationConfirmationDialog == 1){
                        confirmationDialog = 1;
                      }
                      var messageDisplay = $.parseHTML(message);
                      $('.modal-body').append("<h2>"+actionTitle+"</h2><br>");
                      $('.modal-body').append(messageDisplay);
                      if(index != data.length-1) {
                        $('.modal-body').append("<hr class='hrborder'>");
                        $('.hrborder').attr('style','border-top: 1px solid #e5e5e5');
                      }
                    });
                    if(confirmationDialog == 1){
                      $('#btn_ok').remove();
                      $('.modal-footer').prepend("<input type='button' name='btn_ok' id='btn_ok' value='Ok'>");  
                    }
                    $('#btn_ok').on('click',function(){
                        closePopup(0,'primary');
                    });
                    $('#btn_cancle').on('click',function(){
                        closePopup(1,'');
                    });
                    $('#btn_close').on('click',function(){
                        closePopup(1,'');
                    });
                }
              }
    });
}
$.ajax({
        url: "index.php?entryPoint=VIConditionalNotificationsConfiguration",
        type: "post",
        data: {module : moduleName},
        success: function (response) {
          if(response == "1"){
            $('.primary').attr("onclick","customSave('primary')");
            $('.primary').attr("type","button");
            $('.saveAndContinue').attr("onclick","customSave('saveAndContinue')");
            $('.saveAndContinue').attr("type","button");
          }
        }
});
function customSave(elementName){
    var formData = $("#EditView").serialize();
    $.ajax({
            url: "index.php?entryPoint=VIConditionalNotificationsOnSaveCheckCondition",
            type: "post",
            data: {module : moduleName,
                  formData : formData,
                  recordId : recordId},
            dataType : "JSON",
            success: function (response) {
                if(response != null && response != ''){
                  $('#ConditionalNotificationsDialog').attr('style', 'display: block;');
                  $(".modal-body").empty();
                  var notAllowSave = 0;
                  var confirmationDialog = 0;

                  $.each(response, function (index, value) {
                    if(value.notificationNotAllowSave == 1){
                      notAllowSave = 1;
                    }
                    if(value.notificationConfirmationDialog == 1){
                      confirmationDialog = 1;
                    }
                    var actionTitle = value.actionTitle;
                    var message = value.bodyHTML;
                    var messageDisplay = $.parseHTML(message);
                    $('.modal-body').append("<h2>"+actionTitle+"</h2><br>");
                    $('.modal-body').append(messageDisplay);
                    if(index != response.length-1) {
                      $('.modal-body').append("<hr class='hrborder'>");
                      $('.hrborder').attr('style','border-top: 1px solid #e5e5e5');
                    }
                  });
                  if(confirmationDialog == 1){
                    $('#btn_ok').remove();
                    $('.modal-footer').prepend("<input type='button' name='btn_ok' id='btn_ok' value='Ok'>");
                  }//end of if
                  if(notAllowSave == 1){
                      $('#btn_cancle').on('click',function(){
                        closePopup(1 ,elementName);
                      });
                      $('#btn_close').on('click',function(){
                        closePopup(1 ,elementName);
                    });
                  }else if(confirmationDialog == 1){
                      $('#btn_ok').on('click',function(){
                         closePopup(0,elementName)
                      });
                      $('#btn_cancle').on('click',function(){
                        closePopup(1,elementName);
                      });
                      $('#btn_close').on('click',function(){
                        closePopup(1,elementName);
                      });
                  }else{
                      $('#btn_cancle').on('click',function(){
                        closePopup(0 ,elementName);
                      });
                      $('#btn_close').on('click',function(){
                        closePopup(0 ,elementName);
                    });
                  }
                }else{
                  if(moduleName == 'AM_ProjectTemplates'){
                    SUGAR.project_template.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='view_GanttChart';  formSubmitCheck();
                    $('.primary').attr("onclick","SUGAR.project_template.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='view_GanttChart';  formSubmitCheck();");
                  }else if(moduleName == 'Project'){
                    SUGAR.projects.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='view_GanttChart';  formSubmitCheck();
                    $('.primary').attr("onclick","SUGAR.projects.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='view_GanttChart';  formSubmitCheck();");
                  }else if(moduleName == 'Meetings'){
                    SUGAR.meetings.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='DetailView';  formSubmitCheck();
                    $('.primary').attr("onclick","SUGAR.meetings.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='DetailView';  formSubmitCheck();");
                    document.EditView.send_invites.value='1';SUGAR.meetings.fill_invitees();document.EditView.action.value='Save';document.EditView.return_action.value='EditView';document.EditView.return_module.value='Meetings'; formSubmitCheck();
                    $('#save_and_send_invites_header').attr("onclick","document.EditView.send_invites.value='1';SUGAR.meetings.fill_invitees();document.EditView.action.value='Save';document.EditView.return_action.value='EditView';document.EditView.return_module.value='Meetings'; formSubmitCheck();");
                    $('.saveAndContinue').attr("onclick","SUGAR.saveAndContinue(this)");
                  }else{
                    $('.primary').attr("onclick","var _form = document.getElementById('EditView'); _form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;");
                    $('.saveAndContinue').attr("onclick","SUGAR.saveAndContinue(this)");
                    if(elementName == 'primary'){
                      var _form = document.getElementById('EditView'); _form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;
                    }else{
                      SUGAR.saveAndContinue(this);
                    }
                  }  
                }
            }
    });
}
$.ajax({
        url: "index.php?entryPoint=VIConditionalNotificationsModuleFieldLabel",
        type: "post",
        data: {module : moduleName},
        dataType : "JSON",
        success: function (response) {
            var fieldLabel = response['fieldLabel'];
            $.each(fieldLabel, function (index, value) {
             YAHOO.util.Event.onDOMReady(function(){
                if(moduleName == 'Leads'){
                  if(value == 'account_name'){
                    value = 'EditView_account_name';
                  }
                }
                YAHOO.util.Event.addListener(value, 'change', function(){
                    if(moduleName == 'Meetings' && value == 'duration'){
                      durationValue = $('#duration').val();
                      var minutes = Math.floor((durationValue) / 60);
                      if(minutes > 60){
                        value = 'duration_hours';
                        fieldValue = Math.floor(durationValue / 3600);
                      }else{
                        value = 'duration_minutes';
                        fieldValue = minutes;
                      }
                    }
                    var formData = $("#EditView").serialize();
                    var relateFieldName = '';
                        if($("div[field='"+value+"']").attr('type') == 'relate'){
                            relateFieldName = value;
                            value = $("div[field='"+value+"']").after().find("input[type='hidden']").attr('name');  
                        }
                       
                        fieldType = $("div[field='"+value+"']").attr('type');
                        fieldValue = $('#'+value).val();
                        if(value == 'EditView_account_name'){
                          value = 'account_name';
                        }
                    $.ajax({
                            url: "index.php?entryPoint=VIConditionalNotificationsEditviewCheckCondition",
                            type: "post",
                            data: {module : moduleName,
                                  relateFieldName : relateFieldName,
                                  fieldName : value,
                                  fieldValue : fieldValue,
                                  fieldType : fieldType,
                                  formData : formData,
                                  recordId : recordId},
                            dataType : "JSON",
                            success: function(data) {
                              if(data != null && data != ''){
                                var confirmationDialog = 0;
                                if(data['notificationConfirmationDialog'] == 1){
                                  confirmationDialog = 1;
                                }
                                var actionTitles = data['actionTitle'];
                                var message = data['bodyHTML'];
                                var messageDisplay = $.parseHTML(message);
                                if(actionTitles != ''){
                                  $('#ConditionalNotificationsDialog').attr('style', 'display: block;');
                                  $(".modal-body").empty();
                                  $('.modal-body').append("<h2>"+actionTitles+"</h2><br>");
                                  $('.modal-body').append(messageDisplay);
                                  if(confirmationDialog == 1){
                                    $('#btn_ok').remove();
                                    $('.modal-footer').prepend("<input type='button' name='btn_ok' id='btn_ok' value='Ok'>");
                                  }
                                  $('#btn_ok').on('click',function(){
                                     closePopup(0,'primary')
                                  });
                                  $('#btn_cancle').on('click',function(){
                                    closePopup(1,'');
                                  });
                                  $('#btn_close').on('click',function(){
                                    closePopup(1,'');
                                  });
                                }
                                
                              }
                            }
                    });
                });
              });
            });
        }
});

$.ajax({
        url: "index.php?entryPoint=VIConditionalNotificationsModuleFieldLabel",
        type: "post",
        data: {module : moduleName},
        dataType : "JSON",
        success: function (response) {
            var fieldLabel = response['fieldLabel'];
            $.each(fieldLabel, function (index, value) {
              YAHOO.util.Event.onDOMReady(function(){
                if(moduleName == 'Leads'){
                  if(value == 'account_name'){
                    value = 'EditView_account_name';
                  }
                }
                YAHOO.util.Event.addListener(value, 'change', function(){
                    if(moduleName == 'Meetings' && value == 'duration'){
                      durationValue = $('#duration').val();
                      var minutes = Math.floor((durationValue) / 60);
                      if(minutes > 60){
                        value = 'duration_hours';
                        fieldValue = Math.floor(durationValue / 3600);
                      }else{
                        value = 'duration_minutes';
                        fieldValue = minutes;
                      }
                    }
                    var formData = $("#EditView").serialize();
                    var relateFieldName = '';
                    if($("div[field='"+value+"']").attr('type') == 'relate'){
                        relateFieldName = value;
                        value = $("div[field='"+value+"']").after().find("input[type='hidden']").attr('name');
                          
                    }
                    fieldType = $("div[field='"+value+"']").attr('type');
                    fieldValue = $('#'+value).val();
                    if(value == 'EditView_account_name'){
                      value = 'account_name';
                    }
                    $.ajax({
                            url: "index.php?entryPoint=VIConditionalNotificationsEditviewDuplicateCheckCondition",
                            type: "post",
                            data: {module : moduleName,
                                  relateFieldName : relateFieldName,
                                  fieldName : value,
                                  fieldValue : fieldValue,
                                  fieldType : fieldType,
                                  formData : formData,
                                  recordId : recordId
                                  },
                            dataType : "JSON",
                            success: function(data) {
                              if(data != null && data != ''){
                                var actionTitles = data['actionTitle'];
                                var message = data['bodyHTML'];
                                var confirmationDialog = 0;
                                if(data['notificationConfirmationDialog'] == 1){
                                  confirmationDialog = 1;
                                }
                                var messageDisplay = $.parseHTML(message);
                                $('#ConditionalNotificationsDialog').attr('style', 'display: block;');
                                $(".modal-body").empty();
                                $('.modal-body').append("<h2>"+actionTitles+"</h2><br>");
                                $('.modal-body').append(messageDisplay);
                                if(confirmationDialog == 1){
                                  $('#btn_ok').remove();
                                  $('.modal-footer').prepend("<input type='button' name='btn_ok' id='btn_ok' value='Ok'>");
                                }
                                $('#btn_ok').on('click',function(){
                                   closePopup(0,'primary')
                                });
                                $('#btn_cancle').on('click',function(){
                                    closePopup(1,'');
                                });
                                $('#btn_close').on('click',function(){
                                    closePopup(1,'');
                                });
                              }
                            }
                    });
                });
              });
            });
        }
});
function closePopup(value,elementName){
  if(value == 1){
    $('#ConditionalNotificationsDialog').attr('style', 'display: none;');
    $('.primary').attr("onclick","customSave('primary')");
    $('.saveAndContinue').attr("onclick","customSave('saveAndContinue')");
  }else{
    if(moduleName == 'AM_ProjectTemplates'){
      $('#ConditionalNotificationsDialog').attr('style', 'display: none;');
      $('.primary').attr("onclick","SUGAR.project_template.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='view_GanttChart';  formSubmitCheck();");
      if(elementName == 'primary'){
          SUGAR.project_template.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='view_GanttChart';  formSubmitCheck();
      }
    }else if(moduleName == 'Project'){
      $('#ConditionalNotificationsDialog').attr('style', 'display: none;');
      $('.primary').attr("onclick","SUGAR.projects.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='view_GanttChart';  formSubmitCheck();");
      if(elementName == 'primary'){
        SUGAR.projects.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='view_GanttChart';  formSubmitCheck();
      }
    }else if(moduleName == 'Meetings'){
      $('#ConditionalNotificationsDialog').attr('style', 'display: none;');
      SUGAR.meetings.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='DetailView';  formSubmitCheck();
      $('.primary').attr("onclick","SUGAR.meetings.fill_invitees();document.EditView.action.value='Save'; document.EditView.return_action.value='DetailView';  formSubmitCheck();");
      document.EditView.send_invites.value='1';SUGAR.meetings.fill_invitees();document.EditView.action.value='Save';document.EditView.return_action.value='EditView';document.EditView.return_module.value='Meetings'; formSubmitCheck();
      $('#save_and_send_invites_header').attr("onclick","document.EditView.send_invites.value='1';SUGAR.meetings.fill_invitees();document.EditView.action.value='Save';document.EditView.return_action.value='EditView';document.EditView.return_module.value='Meetings'; formSubmitCheck();");
      $('.saveAndContinue').attr("onclick","SUGAR.saveAndContinue(this)");
    }else{
      $('#ConditionalNotificationsDialog').attr('style', 'display: none;');
      $('.primary').attr("onclick","var _form = document.getElementById('EditView'); _form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;");
      $('.saveAndContinue').attr("onclick","SUGAR.saveAndContinue(this)");
      if(elementName == 'primary'){
        var _form = document.getElementById('EditView'); _form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;
      }else{
        SUGAR.saveAndContinue(this);
      }
    }
    
  }
}//end of function

