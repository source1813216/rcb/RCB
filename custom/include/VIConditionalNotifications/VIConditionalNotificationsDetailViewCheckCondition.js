/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
var decodeUrl = decodeURIComponent(window.location.href);
function getParam( name )
{
     name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
     var regexS = "[\\?&]"+name+"=([^&#]*)";
     var regex = new RegExp( regexS );
     var results = regex.exec(decodeUrl);
     if( results == null )
      return "";
     else
     return results[1];
}
var moduleName = getParam('module');
var recordId = getParam('record');
$.ajax({
        url: "index.php?entryPoint=VIConditionalNotificationsDetailViewCheckCondition",
        type: "post",
        data: {module : moduleName,
               recordId : recordId},
        dataType : "JSON",
        success: function(data) {
        	if(data != null && data != ''){
        		$('#ConditionalNotificationsDetailViewDialog').attr('style', 'display: block;');
        		$.each(data, function (index, value) {
        			var actionTitle = value.actionTitle;
        			var message = value.bodyHTML;
        			var messageDisplay = $.parseHTML(message);
        			$('.modal-body').append("<h2>"+actionTitle+"</h2><br>");
        			$('.modal-body').append(messageDisplay);
        			if(index != data.length-1) {
		        		$('.modal-body').append("<hr class='hrborder'>");
		        		$('.hrborder').attr('style','border-top: 1px solid #e5e5e5');
        			}
	        		
        		});
        	}
        }
});
function closePopup(){
  $('#ConditionalNotificationsDetailViewDialog').attr('style', 'display: none;');
}//end of function