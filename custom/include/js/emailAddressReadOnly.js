/**
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Original Author Biztech Co.
 */

$(document).ready(function () {
    //Checks If Email Field Exists
    if ($('#email1_span').length) {
        var module = $('.dcQuickEdit').find("input[name='module']").val();
        var record = $('.dcQuickEdit').find("input[name='record']").val();
        //Check ACL Field Level Access
        $.ajax({
            url: "index.php",
            type: "GET",
            data: {module: 'Administration',
                action: 'fieldAccessControlHandler',
                module_name: module, record_id: record, method: 'check_if_access'},
            success: function (html) {
                //If Read Only
                var obj = $.parseJSON(html);
                if (obj == '1') {
                    //Then Apply Read Only Layout
                    $.ajax({
                        url: "index.php",
                        type: "GET",
                        data: {module: 'Administration',
                            action: 'fieldAccessControlHandler',
                            module_name: module, record_id: record, method: 'get_email_address_html'},
                        success: function (html) {
                            var obj = $.parseJSON(html);
                            $('#email1_span').parent('td').html(obj);
                            $('#email1_span').parent('div').html(obj);
                        }
                    });
                }
            }
        });
    }
});

