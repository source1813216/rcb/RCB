<?php
// created: 2021-09-03 20:03:17
$GLOBALS['tabStructure'] = array (
  'LBL_TABGROUP_SALES' => 
  array (
    'label' => 'LBL_TABGROUP_SALES',
    'modules' => 
    array (
      0 => 'Accounts',
      1 => 'Contacts',
      2 => 'STP_Stakeholder_Type',
    ),
  ),
  'LBL_GROUPTAB1_1621191393' => 
  array (
    'label' => 'LBL_GROUPTAB1_1621191393',
    'modules' => 
    array (
      0 => 'Leads',
      1 => 'USP_Update_Lead_Status',
    ),
  ),
  'LBL_GROUPTAB4_1625078550' => 
  array (
    'label' => 'LBL_GROUPTAB4_1625078550',
    'modules' => 
    array (
      0 => 'Emails',
      1 => 'Calendar',
    ),
  ),
  'LBL_GROUPTAB5_1626621414' => 
  array (
    'label' => 'LBL_GROUPTAB5_1626621414',
    'modules' => 
    array (
      0 => 'ResourceCalendar',
      1 => 'Project',
      2 => 'AM_ProjectTemplates',
      3 => 'Cases',
    ),
  ),
  'LBL_GROUPTAB6_1626621446' => 
  array (
    'label' => 'LBL_GROUPTAB6_1626621446',
    'modules' => 
    array (
      0 => 'AOR_Reports',
    ),
  ),
  'LBL_GROUPTAB2_1621685953' => 
  array (
    'label' => 'LBL_GROUPTAB2_1621685953',
    'modules' => 
    array (
      0 => 'LIP_Line_Institutes',
      1 => 'VNP_Venues',
      2 => 'SRP_StdApproval_Routine',
      3 => 'STP_Sales_Target',
    ),
  ),
  'LBL_GROUPTAB3_1623660167' => 
  array (
    'label' => 'LBL_GROUPTAB3_1623660167',
    'modules' => 
    array (
      0 => 'IEP_Initial_Engagement',
      1 => 'AOS_Quotes',
      2 => 'EAP_Event_Approval',
      3 => 'EDP_Endorsements',
      4 => 'EDP_Event_Documents',
      5 => 'AOS_Contracts',
      6 => 'AOS_Invoices',
      7 => 'MOU_MOU',
    ),
  ),
);