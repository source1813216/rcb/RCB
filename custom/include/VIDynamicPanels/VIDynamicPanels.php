<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VIDynamicPanels{
    function VIDynamicPanels($event,$arguments){
        if($GLOBALS['app']->controller->action == 'EditView' || $_REQUEST['action'] == 'EditView' || $_REQUEST['action'] == 'SubpanelCreates'){
            $actionName = $_REQUEST['action'];
            if($_REQUEST['action'] == 'SubpanelCreates'){
                $moduleName = json_encode($_REQUEST['target_module']);
            }else{
                $moduleName = json_encode($_REQUEST['module']);
            }
            if(isset($_REQUEST['record']) && !empty($_REQUEST['record'])){
                $recordId = json_encode($_REQUEST['record']);
            }else{
                $recordId = json_encode('');
            }
            $actionName = json_encode($actionName);
            if($_REQUEST['module'] != 'ModuleBuilder'){
                echo '<script> var actionName = '.$actionName.';
                               var moduleName = '.$moduleName.';
                               var recordId = '.$recordId.';</script>';
                $randomNumber = rand();
                echo '<script type="text/javascript" src="custom/include/VIDynamicPanels/VIDynamicPanelsCheckCondition.js?v='.$randomNumber.'"></script>';
            }
        }//end of if
        if($GLOBALS['app']->controller->action == 'DetailView' || $GLOBALS['app']->controller->action == 'view_GanttChart' || $_REQUEST['action'] == 'DetailView'){
            $randomNumber = rand();
            $actionName = json_encode($_REQUEST['action']);
            $moduleName = json_encode($_REQUEST['module']);
            $recordId = json_encode($_REQUEST['record']);
            
            echo '<script> var actionName = '.$actionName.';
                           var moduleName = '.$moduleName.';
                           var recordId = '.$recordId.';</script>';
            echo '<script type="text/javascript" src="custom/include/VIDynamicPanels/VIDynamicPanelsDetailViewCheckCondition.js?v='.$randomNumber.'"></script>';
        }//end of if  
        if($GLOBALS['app']->controller->action == 'index'){
            global $suitecrm_version;
            if (version_compare($suitecrm_version, '7.10.2', '>=')){
                echo '<link rel="stylesheet" type="text/css" href="custom/include/VIDynamicPanels/VIDynamicPanelsIcon.css">';
            }//end of if
        }//end of if
    }//end of function
}//end of class