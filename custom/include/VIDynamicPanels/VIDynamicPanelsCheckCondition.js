/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
if(actionName != 'EditView'){
    var formId = 'form_SubpanelQuickCreate_'+moduleName;
}else{
    var formId = 'EditView';
}//end of else

$.ajax({
        url: "index.php?entryPoint=VIDynamicPanelsConfiguration",
        type: "post",
        data: {module : moduleName},
        success: function (response) {
            if(response == "1"){
                var saveButtonOnClick;
                $('.primary').each(function () {
                    if($(this).attr('id') == 'SAVE'){
                        saveButtonOnClick = $(this).attr('onclick');
                        $(this).attr('onclick','');
                        $(this).attr('type','button');
                    }
                });
                
                var saveContinueButtonOnClick = $('.saveAndContinue').attr('onclick');
                var saveAndSendInvites = $('#save_and_send_invites_header').attr('onclick');
                $('.saveAndContinue').attr("onclick","");
                $('#save_and_send_invites_header').attr("onclick","");
                $('.saveAndContinue').attr("type","button");

                $('.primary').on('click',function(){
                    if($(this).attr('id') == 'SAVE'){
                        $('select:disabled').each(function () {
                           $(this).removeAttr('disabled');
                        });
                        $(this).attr('onclick',saveButtonOnClick);
                        $(this).trigger("onclick");
                    }
                });

                $('.saveAndContinue').on('click',function(){
                    $('select:disabled').each(function () {
                       $(this).removeAttr('disabled');
                    });
                    $('.saveAndContinue').attr('onclick',saveContinueButtonOnClick);
                    $(".saveAndContinue").trigger("onclick");
                });
                $('#save_and_send_invites_header').on('click',function(){
                    $('select:disabled').each(function () {
                       $(this).removeAttr('disabled');
                    });
                    $('#save_and_send_invites_header').attr('onclick',saveAndSendInvites);
                    $("#save_and_send_invites_header").trigger("onclick");
                });
            }
        }
});

//default panels and fields hide
$.ajax({
    url: "index.php?entryPoint=VIDynamicPanelsDefault",
    type: "post",
    data: {module : moduleName,
          actionName : actionName},
    dataType : "JSON",
    success: function (response) {
        //defautl field hide
        var defaultFieldsHide = response['defaultFieldsHide'];
        $.each(defaultFieldsHide, function (index, value) {
            var find = "_address_";
            if(value.indexOf(find) != -1){
                if(actionName != 'EditView'){
                    if(value != ''){
                        $('#'+formId+' #'+value).closest('tr').hide();
                    }//end of if
                }else{
                    $('#'+value).closest('tr').hide();
                }//end of else
            }else{
                if(actionName != 'EditView'){
                    if(value != ''){
                        $("#"+formId+" div[field='"+value+"']").parent().hide();
                    }//end of if
                }else{
                    $("div[field='"+value+"']").parent().hide();
                }//end of else    
            }//end of else
        });//end of each

        //default tabs hide
        var viewTabsHide = response['viewTabsHide'];
        viewTabsHide = jQuery.parseJSON(viewTabsHide);
        $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
            var panelName = $.trim($(this).text());
            var tabId = $(this).attr("id");
            if(jQuery.inArray(panelName,viewTabsHide) != '-1'){
                $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                var arr = tabId.substr(-1);
                $('div[id=tab-content-'+arr+']').hide();
            }//end of if
        });//end of each

        $('div .panel-heading').each(function(){
            var panels = $.trim($(this).closest('div').text());
            if(jQuery.inArray(panels,viewTabsHide) != '-1'){
                    $(this).closest('div').parent().hide();
            }//end of if 
        });//end of each

        //default panels hide
        var viewPanelsHide = response['viewPanelsHide'];
        viewPanelsHide = jQuery.parseJSON(viewPanelsHide);
        $('div .panel-heading').each(function(){
            var panels = $.trim($(this).closest('div').text());
            if(jQuery.inArray(panels,viewPanelsHide) != '-1'){
                    $(this).closest('div').parent().hide();
            }//end of if
        });//end of each

        $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
            var panelName = $.trim($(this).text());
            var tabId = $(this).attr("id");
            if(jQuery.inArray(panelName,viewPanelsHide) != '-1'){
                $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                var arr = tabId.substr(-1);
                $('div[id=tab-content-'+arr+']').hide();
            }//end of if
        });//end of each

        $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
            var panelName = $.trim($(this).text());
            var tabId = $(this).attr("id");
            $('body').on('click','ul.nav-tabs > li > a[id='+tabId+']',function(){
                var arr = tabId.substr(-1);
                $('div .panel-heading').each(function(){
                    var panels = $.trim($(this).closest('div').text());
                    var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                    if(arr == tabPanelId){
                        if(jQuery.inArray(panels,viewPanelsHide) != '-1'){
                            $(this).closest('div').parent().hide();
                        }//end of if 
                    }//end of if
                });//end of each

                if(jQuery.inArray(panelName,viewPanelsHide) != '-1'){
                    $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                    var arr = tabId.substr(-1);
                    $('div[id=tab-content-'+arr+']').hide();
                }//end of if

                if(jQuery.inArray(panelName,viewTabsHide) != '-1'){
                    $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                    var arr = tabId.substr(-1);
                    $('div[id=tab-content-'+arr+']').hide();
                }//end of if

                $('div .panel-heading').each(function(){
                    var panels = $.trim($(this).closest('div').text());
                    var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                    if(arr == tabPanelId){
                        if(jQuery.inArray(panels,viewTabsHide) != '-1'){
                                $(this).closest('div').parent().hide();
                        }//end of if
                    }           
                });//end of each
            });
        });

        //remove default fields required validation which is hide
        var removeFromValidateDefaultFields = response['removeFromValidateDefaultFields'];
        $.each(removeFromValidateDefaultFields, function (index, value) {
            removeFromValidate(formId,value);
        });//end of each

        //remove default panels required fields validation which is hide 
        var removeFromValidatePanelFields = response['removeFromValidatePanelFields'];
        removeFromValidatePanelFields = jQuery.parseJSON(removeFromValidatePanelFields);
        $.each(removeFromValidatePanelFields, function (index, value) {
            removeFromValidate(formId,value);
        });//end of each
    }//end of success
});//end of ajax

$.ajax({
    url: "index.php?entryPoint=VIDynamicPanelsModuleFieldLabel",
    type: "post",
    data: {module : moduleName,
          actionName : actionName},
    dataType : "JSON",
    success: function (response) {
        var fieldLabel = response['fieldLabel'];
        $.each(fieldLabel, function (index, value) {
            YAHOO.util.Event.onDOMReady(function(){
                fieldType = $("#"+formId+" div[field='"+value+"']").attr('type'); //field type
                if(fieldType == 'enum'){
                    val = "select[name="+value+"]";
                }else if(fieldType == 'multienum'){
                    val = "select[name='"+value+"[]']";
                }else if(fieldType == 'text'){
                    val = "textarea[name="+value+"]";
                }else {
                    val = "input[name="+value+"]";
                }
                YAHOO.util.Event.addListener(YAHOO.util.Selector.query(val), 'change', function(){
                    if($("#"+formId+" div[field='"+value+"']").attr('type') == 'relate'){
                        value = $("#"+formId+" div[field='"+value+"']").after().find("input[type='hidden']").attr('name');  
                    }//end of if

                    fieldType = $("#"+formId+" div[field='"+value+"']").attr('type'); //field type
                    
                    if($("#"+formId+" div[field='"+value+"']").attr('type') == 'bool'){
                        var element = $("#"+formId+" div[field='"+value+"']").after().find("input[type='checkbox']");
                        if(element.is(":checked")){
                            fieldValue = '1';
                        }else{
                            fieldValue = '0';
                        }
                        $("#"+formId+" div[field='"+value+"']").after().find("input[type='hidden']").val(fieldValue);
                    }else{
                        fieldValue = $('#'+formId+' #'+value).val(); //field value
                    }

                    var formData = $("#"+formId).serialize(); //formdata

                    $.ajax({
                        url: "index.php?entryPoint=VIDynamicPanelsCheckCondition",
                        type: "post",
                        data: {module : moduleName,
                              fieldName : value,
                              fieldValue : fieldValue,
                              fieldType : fieldType,
                              formData : formData,
                              actionName : actionName,
                              recordId : recordId},
                        dataType: "JSON",
                        success: function(result) {
                            $.each(result,function(index,data){
                                if(data['success'] == 'true'){
                                    var editViewTabsHide = data['editViewTabsHide'];
                                    var editViewPanelsHide = data['editViewPanelsHide'];
                                    var editViewTabsShow = data['editViewTabsShow'];
                                    var editViewPanelsShow = data['editViewPanelsShow'];
                                    var removeFromValidatePanelFields = data['removeFromValidatePanelFields'];
                                    var fieldsShow = data['fieldsShowArray'];
                                    var fieldsHide = data['fieldsHideArray'];
                                    var addToValidatePanelFieldsShow = data['addToValidatePanelFieldsShow'];
                                    var removeFromValidateFieldsHide = data['removeFromValidateFieldsHide'];
                                    var addToValidateFieldsShow = data['addToValidateFieldsShow'];
                                    var fieldsMandatoryArray = data['fieldsMandatoryArray'];
                                    var fieldsReadOnlyArray = data['fieldsReadOnlyArray'];
                                    var autoPopulateField = data['autoPopulateFieldArray'];


                                    //tabs Hide
                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        if(jQuery.inArray(panelName,editViewTabsHide) != '-1'){
                                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                                            var arr = tabId.substr(-1);;
                                            $('div[id=tab-content-'+arr+']').hide();
                                        }//end of if
                                    });//end of function
                                    $('div .panel-heading').each(function(){
                                        var panels = $.trim($(this).closest('div').text());
                                        if(jQuery.inArray(panels,editViewTabsHide) != '-1'){
                                            $(this).closest('div').parent().hide();
                                        }//end of if
                                    });//end of function

                                    //panels Hide
                                    $('div .panel-heading').each(function(){
                                        var panels = $.trim($(this).closest('div').text());
                                        if(jQuery.inArray(panels,editViewPanelsHide) != '-1'){
                                            $(this).closest('div').parent().hide();
                                        }//end of if
                                    });//end of function
                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        if(jQuery.inArray(panelName,editViewPanelsHide) != '-1'){
                                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                                            var arr = tabId.substr(-1);;
                                            $('div[id=tab-content-'+arr+']').hide();
                                        }//end of if
                                    });//end of function

                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        $('body').on('click','ul.nav-tabs > li > a[id='+tabId+']',function(){
                                            
                                            $('div .panel-heading').each(function(){
                                                var panels = $.trim($(this).closest('div').text());
                                                if(jQuery.inArray(panels,editViewPanelsHide) != '-1'){
                                                        $(this).closest('div').parent().hide();
                                                }//end of if 
                                            });//end of each

                                            if(jQuery.inArray(panelName,editViewPanelsHide) != '-1'){
                                                $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                                                var arr = tabId.substr(-1);
                                                $('div[id=tab-content-'+arr+']').hide();
                                            }//end of if

                                            if(jQuery.inArray(panelName,editViewTabsHide) != '-1'){
                                                $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                                                var arr = tabId.substr(-1);
                                                $('div[id=tab-content-'+arr+']').hide();
                                            }//end of if

                                            $('div .panel-heading').each(function(){
                                                var panels = $.trim($(this).closest('div').text());
                                                if(jQuery.inArray(panels,editViewTabsHide) != '-1'){
                                                        $(this).closest('div').parent().hide();
                                                }//end of if 
                                            });//end of each
                                        });
                                    });

                                    //tabs Show
                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        if(jQuery.inArray(panelName,editViewTabsShow) != '-1'){
                                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                                            var arr = tabId.substr(-1);
                                            var currentTab = $('li.active > a[data-toggle=tab]').attr('id');
                                            if(currentTab != '' && currentTab != undefined){
                                                var arr = currentTab.substr(-1);
                                                $('div[id=tab-content-'+arr+']').show();
                                            }else{
                                                var arr = '';
                                            }
                                        }//end of if
                                    });//end of function
                                    $('div .panel-heading').each(function(){
                                        var panels = $.trim($(this).closest('div').text());
                                        var tabPanelId = $.trim($(this).closest('div').attr('class').substr(-1));
                                        var currentTab = $('li.active > a[data-toggle=tab]').attr('id');
                                        if(currentTab != '' && currentTab != undefined){
                                            var arr = currentTab.substr(-1);
                                        }else{
                                            var arr = '';
                                        }
                                        if(jQuery.inArray(panels,editViewTabsShow) != '-1'){
                                            if(arr == '' || arr == undefined){
                                                $(this).closest('div').parent().show();
                                            }else{
                                                if(arr == tabPanelId){
                                                    $(this).closest('div').parent().show();
                                                }
                                            }
                                        }//end of if
                                    });//end of function

                                    //panels Show
                                    $('div .panel-heading').each(function(){
                                        var panels = $.trim($(this).closest('div').text());
                                        var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                                        var currentTab = $('li.active > a[data-toggle=tab]').attr('id');
                                        if(currentTab != '' && currentTab != undefined){
                                            var arr = currentTab.substr(-1);
                                        }else{
                                            var arr = '';
                                        }
                                        if(jQuery.inArray(panels,editViewPanelsShow) != '-1'){
                                            if(arr == '' || arr == undefined){
                                                $(this).closest('div').parent().show();
                                            }else{
                                                if(arr == tabPanelId){
                                                    $(this).closest('div').parent().show();
                                                }
                                            }
                                        }//end of if
                                    });//end of function
                                    
                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        if(jQuery.inArray(panelName,editViewPanelsShow) != '-1'){
                                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                                            var arr = tabId.substr(-1);;
                                            $('div[id=tab-content-'+arr+']').show();
                                        }//end of if
                                    });//end of function

                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        $('body').on('click','ul.nav-tabs > li > a[id='+tabId+']',function(){
                                            var arr = tabId.substr(-1);
                                            $('div .panel-heading').each(function(){
                                                var panels = $.trim($(this).closest('div').text());
                                                var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                                                if(arr == tabPanelId){
                                                    if(jQuery.inArray(panels,editViewPanelsShow) != '-1'){
                                                        $(this).closest('div').parent().show();
                                                    }//end of if 
                                                }//end of if
                                            });//end of each

                                            if(jQuery.inArray(panelName,editViewPanelsShow) != '-1'){
                                                $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                                                var arr = tabId.substr(-1);
                                                $('div[id=tab-content-'+arr+']').show();
                                            }//end of if

                                            if(jQuery.inArray(panelName,editViewTabsShow) != '-1'){
                                                $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                                                var arr = tabId.substr(-1);
                                                $('div[id=tab-content-'+arr+']').show();
                                            }//end of if

                                            $('div .panel-heading').each(function(){
                                                var panels = $.trim($(this).closest('div').text());
                                                var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                                                if(arr == tabPanelId){
                                                    if(jQuery.inArray(panels,editViewTabsShow) != '-1'){
                                                            $(this).closest('div').parent().show();
                                                    }//end of if 
                                                }//end of if
                                            });//end of each
                                        });
                                    });
                                    
                                    //remove panels required fields validation which is hide 
                                    $.each(removeFromValidatePanelFields, function (index, value) {
                                        removeFromValidate(formId,value);
                                    });//end of function

                                    //fields show
                                    $.each(fieldsShow, function (index, value) {
                                        var find = "_address_";
                                        if(value.indexOf(find) != -1){
                                            $('#'+formId+' #'+value).closest('tr').show(); 
                                        }else{
                                            $("#"+formId+" div[field='"+value+"']").parent().show(); 
                                        }//end of else
                                    });//end of function


                                    //fields hide
                                    $.each(fieldsHide, function (index, value) {
                                        var find = "_address_";
                                        if(value.indexOf(find) != -1){
                                            $('#'+formId+' #'+value).closest('tr').hide();
                                        }else{
                                            $("#"+formId+" div[field='"+value+"']").parent().hide();
                                        }//end of else
                                    });//end of function

                                    $.each(addToValidatePanelFieldsShow,function(index,value){
                                        if(value.type == 'file'){
                                            addToValidate(formId,''+value.field+'_file','',true,'Field is required.');
                                        }else{
                                            addToValidate(formId,''+value.field+'','',true,'Field is required.');
                                        }//end of else
                                    });//end of function 

                                    //remove fields required validation which is hide
                                    $.each(removeFromValidateFieldsHide, function (index, value) {
                                        removeFromValidate(formId,value);
                                    });//end of function 

                                    //add to validate fields which is show
                                    $.each(addToValidateFieldsShow,function(index,value){
                                        if(value.type == 'file'){
                                            addToValidate(formId,''+value.field+'_file','',true,'Field is required.');
                                        }else{
                                            addToValidate(formId,''+value.field+'','',true,'Field is required.');
                                        }//end of else
                                    });//end of function

                                    //field mandatory
                                    $.each(fieldsMandatoryArray,function(index,value){
                                        var find = "_address_";
                                        if(value.field.indexOf(find) != -1){
                                            var requiredClass = $('#'+formId+' #'+value.field).closest('td').prev('td').find('span').attr('class');
                                            if(requiredClass != 'required'){
                                                $('#'+formId+' #'+value.field).closest('td').prev('td').append('<span class="required">*</span>');
                                            }//end of if
                                            var requiredClass = $("#"+formId+" div[field='"+value.field+"']").prev().find('span').attr('class');
                                            if(requiredClass != 'required'){
                                                $("#"+formId+" div[field='"+value.field+"']").prev().append('<span class="required">*</span>');    
                                            }//end of if
                                        }else{
                                            var requiredClass = $("#"+formId+" div[field='"+value.field+"']").prev().find('span').attr('class');
                                            if(requiredClass != 'required'){
                                                $("#"+formId+" div[field='"+value.field+"']").prev().append('<span class="required">*</span>');    
                                            }//end of if
                                        }//end of else
                                        addToValidate(formId,''+value.field+'','',true,'Field is required.');
                                    });//end of function

                                    //field readonly
                                    $.each(fieldsReadOnlyArray, function (index, value) {
                                        if(value.field != ''){
                                            if(value.type == 'enum' || value.type == 'bool' || value.type == "dynamicenum" || value.type == "radioenum" || value.type == "multienum"){
                                                $("#"+formId+" #"+value.field).attr("disabled", true);
                                            }else if(value.type == 'date'){
                                                $("#"+formId+" #"+value.field).attr("readonly", true);
                                                $("#"+formId+" #"+value.field+"_trigger").attr("disabled", true);
                                            }else if(value.type == 'datetimecombo' || value.type == 'datetime'){
                                                $("#"+formId+" #"+value.field+"_date").attr("readonly", true);
                                                $("#"+formId+" #"+value.field+"_trigger").attr("disabled", true);
                                                $("#"+formId+" #"+value.field+"_hours").attr("disabled", true);
                                                $("#"+formId+" #"+value.field+"_minutes").attr("disabled", true);
                                            }else if(value.type == 'relate'){
                                                $("#"+formId+" #"+value.field).attr("readonly", true);
                                                $("#"+formId+" #btn_"+value.field).attr("disabled", true);
                                                $("#"+formId+" #btn_clr_"+value.field).attr("disabled", true);
                                            }else{
                                                if(value.field == 'currency_id'){
                                                    $("#"+formId+" #"+value.field+'_select').attr("disabled", true);
                                                }else if(value.field == 'revision'){
                                                    $("#"+formId+" input[name="+value.field+"]").attr("readonly", true);
                                                }else if(value.field == 'account_name' && moduleName == 'Leads'){
                                                    $("#"+formId+" input[name="+value.field+"]").attr("readonly", true);
                                                }else{
                                                    $("#"+formId+" #"+value.field).attr("readonly", true);    
                                                }
                                            }//end of else
                                        }//end of if
                                    });//end of function

                                    //field autopopulate 
                                    $.each(autoPopulateField, function (index, value) {
                                        if(value.type == 'relate'){
                                            $('#'+formId+' #'+value.autoPopulateFieldName).val(value.autopopulateFieldValue);
                                            $('#'+formId+' #'+value.relateFieldName).val(value.relateFieldValue);
                                        }else if(value.type == 'bool'){
                                            if(value.autopopulateFieldValue == '1'){
                                                $('#'+formId+' #'+value.autoPopulateFieldName).attr('checked',true);
                                            }//end of if
                                        }else if(value.type == 'datetimecombo'){
                                            var dbdate = value.autopopulateFieldValue.split(' ')[0];
                                            var date = new Date(value.autopopulateFieldValue);
                                            var hours = date.getHours();
                                            if(hours < 10){
                                                hours = '0' + hours;
                                            }//end of if
                                            var minutes = date.getMinutes();
                                            if(minutes < 10){
                                                minutes = '0' + minutes;
                                            }//end of if
                                            $('#'+formId+' #'+value.autoPopulateFieldName+'_date').val(dbdate);
                                            $('#'+formId+' #'+value.autoPopulateFieldName+'_hours').val(hours);
                                            $('#'+formId+' #'+value.autoPopulateFieldName+'_minutes').val(minutes);
                                        }else{
                                            if(value.autoPopulateFieldName != ''){
                                                $('#'+formId+' #'+value.autoPopulateFieldName).val(value.autopopulateFieldValue);
                                                $('#'+formId+' input[name='+value.autoPopulateFieldName+']').val(value.autopopulateFieldValue);    
                                            }//end of if
                                        }//end of else
                                    });

                                    //field color
                                    var fieldColorArray = data['fieldColorrArray'];
                                    $.each(fieldColorArray, function(index,value){
                                        if(value.type == 'datetimecombo'){
                                            $('#'+formId+' #'+value.fieldName+'_date').css('background','#'+value.fieldColor);
                                            $('#'+formId+' #'+value.fieldName+'_hours').css('background','#'+value.fieldColor);
                                            $('#'+formId+' #'+value.fieldName+'_minutes').css('background','#'+value.fieldColor);
                                        }else{
                                            if(value.fieldName == 'revision'){
                                                $('#'+formId+' input[name=revision]').css('background','#'+value.fieldColor);
                                            }else if(value.fieldName == 'currency_id'){
                                                $('#'+formId+' #currency_id_select').css('background','#'+value.fieldColor);
                                            }else{
                                                if(value.fieldName != ''){
                                                    $('#'+formId+' #'+value.fieldName).css('background','#'+value.fieldColor);
                                                }
                                            }//end of else
                                        }//end of else
                                    });//end of each 
                                }else{
                                    var displayAllFields = data['displayAllFields'];
                                    $.each(displayAllFields, function (index, value) {
                                        if(value != '(empty)' && value != '(filler)'){
                                            var find = "_address_";
                                            if(value.indexOf(find) != -1){
                                                if(value != ''){
                                                    $('#'+formId+' #'+value).closest('tr').show();    
                                                }//end of if
                                            }else{
                                                $("#"+formId+" div[field='"+value+"']").parent().show();
                                            }//end of else
                                        }//end of if
                                    });//end of each

                                    //tabs Hide
                                    var defaulteditViewTabsHide = data['defaulteditViewTabsHide'];
                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        if(jQuery.inArray(panelName,defaulteditViewTabsHide) != '-1'){
                                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                                            var arr = tabId.substr(-1);;
                                            $('div[id=tab-content-'+arr+']').hide();
                                        }//end of if
                                    });//end of function

                                    $('div .panel-heading').each(function(){
                                        var panels = $.trim($(this).closest('div').text());
                                        if(jQuery.inArray(panels,defaulteditViewTabsHide) != '-1'){
                                            $(this).closest('div').parent().hide();
                                        }//end of if
                                    });//end of function

                                    //panels Hide
                                    var defaulteditViewPanelsHide = data['defaulteditViewPanelsHide'];
                                    $('div .panel-heading').each(function(){
                                        var panels = $.trim($(this).closest('div').text());
                                        if(jQuery.inArray(panels,defaulteditViewPanelsHide) != '-1'){
                                            $(this).closest('div').parent().hide();
                                        }//end of if
                                    });//end of function

                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        if(jQuery.inArray(panelName,defaulteditViewPanelsHide) != '-1'){
                                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                                            var arr = tabId.substr(-1);;
                                            $('div[id=tab-content-'+arr+']').hide();
                                        }//end of if
                                    });//end of function

                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        $('body').on('click','ul.nav-tabs > li > a[id='+tabId+']',function(){
                                            $('div .panel-heading').each(function(){
                                                var panels = $.trim($(this).closest('div').text());
                                                if(jQuery.inArray(panels,defaulteditViewPanelsHide) != '-1'){
                                                        $(this).closest('div').parent().hide();
                                                }//end of if 
                                            });//end of each

                                            if(jQuery.inArray(panelName,defaulteditViewPanelsHide) != '-1'){
                                                $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                                                var arr = tabId.substr(-1);
                                                $('div[id=tab-content-'+arr+']').hide();
                                            }//end of if

                                            if(jQuery.inArray(panelName,defaulteditViewTabsHide) != '-1'){
                                                $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                                                var arr = tabId.substr(-1);
                                                $('div[id=tab-content-'+arr+']').hide();
                                            }//end of if

                                            $('div .panel-heading').each(function(){
                                                var panels = $.trim($(this).closest('div').text());
                                                if(jQuery.inArray(panels,defaulteditViewTabsHide) != '-1'){
                                                        $(this).closest('div').parent().hide();
                                                }//end of if 
                                            });//end of each
                                        });
                                    });

                                    //tabs Show
                                    var defaulteditViewTabsShow = data['defaulteditViewTabsShow'];
                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        if(jQuery.inArray(panelName,defaulteditViewTabsShow) != '-1'){
                                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                                            var arr = tabId.substr(-1);
                                            var currentTab = $('li.active > a[data-toggle=tab]').attr('id');
                                            if(currentTab != '' && currentTab != undefined){
                                                var arr = currentTab.substr(-1);
                                                $('div[id=tab-content-'+arr+']').show();
                                            }
                                        }//end of if
                                    });//end of function

                                    $('div .panel-heading').each(function(){
                                        var panels = $.trim($(this).closest('div').text());
                                        var tabPanelId = $.trim($(this).closest('div').attr('class').substr(-1));
                                        var currentTab = $('li.active > a[data-toggle=tab]').attr('id');
                                        if(currentTab != '' && currentTab != undefined){
                                            var arr = currentTab.substr(-1);
                                        }else{
                                            var arr = '';
                                        }
                                        if(jQuery.inArray(panels,defaulteditViewTabsShow) != '-1'){
                                            if(arr == '' || arr == undefined){
                                                $(this).closest('div').parent().css('display','block');
                                            }else{
                                                if(arr == tabPanelId){
                                                    $(this).closest('div').parent().css('display','block');
                                                }
                                            }
                                        }//end of if
                                    });//end of function

                                    //panels Show
                                    var defaulteditViewPanelsShow = data['defaulteditViewPanelsShow'];
                                    $('div .panel-heading').each(function(){
                                        var panels = $.trim($(this).closest('div').text());
                                        var tabPanelId = $.trim($(this).closest('div').attr('class').substr(-1));
                                        var currentTab = $('li.active > a[data-toggle=tab]').attr('id');
                                        if(currentTab != '' && currentTab != undefined){
                                            var arr = currentTab.substr(-1);
                                        }else{
                                            var arr = '';
                                        }
                                        if(jQuery.inArray(panels,defaulteditViewPanelsShow) != '-1'){
                                            if(arr == '' || arr == undefined){
                                                $(this).closest('div').parent().css('display','block');
                                            }else{
                                                if(arr == tabPanelId){
                                                    $(this).closest('div').parent().css('display','block');
                                                }
                                            }
                                        }//end of if
                                    });//end of function

                                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                                        var panelName = $.trim($(this).text());
                                        var tabId = $(this).attr("id");
                                        if(jQuery.inArray(panelName,defaulteditViewPanelsShow) != '-1'){
                                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                                            var arr = tabId.substr(-1);;
                                            $('div[id=tab-content-'+arr+']').css('display','block');
                                        }//end of if
                                    });//end of function

                                    var fieldsNonMandatoryArray = data['fieldsNonMandatoryArray'];
                                    $.each(fieldsNonMandatoryArray, function (index, value) {
                                        var find = "_address_";
                                        if(value.field.indexOf(find) != -1){
                                            if(value.field != ''){
                                                $('#'+formId+' #'+value.field).closest('td').prev('td').find('span').remove();
                                                $("#"+formId+" div[field='"+value.field+"']").prev().find("span").remove();
                                            }//end of if
                                        }else{
                                            if(value != ''){
                                                $("#"+formId+" div[field='"+value.field+"']").prev().find("span").remove();
                                            }//end of if
                                        }//end of else
                                        removeFromValidate(formId,value.field);
                                    });//end of each

                                    var removeFromValidatePanelHideFields = data['removeFromValidatePanelHideFields'];
                                    $.each(removeFromValidatePanelHideFields, function (index, value) {
                                        removeFromValidate(formId,value);
                                    });//end of function

                                    var addToValidateFieldsShow = data['addToValidateFieldsShow'];
                                    $.each(addToValidateFieldsShow, function (index, value) {
                                        if(value.type == 'file'){
                                            addToValidate(formId,''+value.field+'_file','',true,'Field is required.');
                                        }else{
                                            addToValidate(formId,''+value.field+'','',true,'Field is required.');
                                        }//end of else
                                    });//end of function

                                    var defaultFieldsHideArray = data['defaultFieldsHideArray'];
                                    $.each(defaultFieldsHideArray, function (index, value) {
                                        var find = "_address_";
                                        if(value.indexOf(find) != -1){
                                            if(value != ''){
                                                $('#'+formId+' #'+value).closest('tr').hide();    
                                            }//end of if
                                        }else{
                                            if(value != ''){
                                                $("#"+formId+" div[field='"+value+"']").parent().hide();    
                                            }//end of if
                                        }//end of else
                                    });//end of each

                                    var defaultFieldsShowArray = data['defaultFieldsShowArray'];
                                    $.each(defaultFieldsShowArray, function (index, value) {
                                        var find = "_address_";
                                        if(value.indexOf(find) != -1){
                                            if(value != ''){
                                                $('#'+formId+' #'+value).closest('tr').show();    
                                            }//end of if
                                        }else{
                                            if(value != ''){
                                                $("#"+formId+" div[field='"+value+"']").parent().show();    
                                            }//end of if
                                        }//end of else
                                    });//end of each

                                    var removeFromValidateDefaultFields = data['removeFromValidateDefaultFields'];
                                    $.each(removeFromValidateDefaultFields, function (index, value) {
                                        removeFromValidate(formId,value);
                                    });
                                    var removeReadOnly = data['removeReadOnly'];
                                    $.each(removeReadOnly, function (index, value) {
                                        if(value.field != ''){
                                            if(value.type == 'enum'  || value.type == 'bool' || value.type == "dynamicenum" || value.type == "radioenum" || value.type == "multienum"){
                                                $("#"+formId+" #"+value.field).removeAttr("disabled", true);
                                            }else if(value.type == 'date'){
                                                $("#"+formId+" #"+value.field).removeAttr("readonly", true);
                                                $("#"+formId+" #"+value.field+'_trigger').removeAttr("disabled", true);
                                            }else if(value.type == 'datetimecombo' || value.type == 'datetime'){
                                                $("#"+formId+" #"+value.field+"_date").removeAttr("readonly", true);
                                                $("#"+formId+" #"+value.field+'_trigger').removeAttr("disabled", true);
                                                $("#"+formId+" #"+value.field+'_hours').removeAttr("disabled", true);
                                                $("#"+formId+" #"+value.field+'_minutes').removeAttr("disabled", true);
                                            }else if(value.type == 'relate'){
                                                $("#"+formId+" #"+value.field).removeAttr("readonly", true);
                                                $("#"+formId+" #btn_"+value.field).removeAttr("disabled", true);
                                                $("#"+formId+" #btn_clr_"+value.field).removeAttr("disabled", true);
                                            }else{
                                                if(value.field == 'currency_id'){
                                                    $("#"+formId+" #"+value.field+'_select').removeAttr("disabled", true);
                                                }else if(value.field == 'revision'){
                                                    $("#"+formId+" input[name="+value.field+"]").removeAttr("readonly", true);
                                                }else if(value.field == 'account_name' && moduleName == 'Leads'){
                                                    $("#"+formId+" input[name="+value.field+"]").removeAttr("readonly", true);
                                                }else{
                                                    $("#"+formId+" #"+value.field).removeAttr("readonly", true);
                                                }//end of else
                                            }//end of else
                                        }//end of if
                                    });//end of each

                                    var removeautoPopulateFieldValue = data['removeautoPopulateFieldValue'];
                                    $.each(removeautoPopulateFieldValue, function (index, value) {
                                        if(value.type == 'relate'){
                                            if(value.autoPopulateFieldName != ''){
                                                $('#'+formId+' #'+value.autoPopulateFieldName).val('');
                                                $('#'+formId+' #'+value.relateFieldName).val('');    
                                            }//end of if
                                        }else if(value.type == 'bool'){
                                            if(value.autoPopulateFieldName != ''){
                                                $('#'+formId+' #'+value.autoPopulateFieldName).attr('checked',false);
                                            }//end of u=if
                                        }else if(value.type == 'datetimecombo'){
                                            if(value.autoPopulateFieldName != ''){
                                                dateValue = $('#'+formId+' #'+value.autoPopulateFieldName).val();
                                                var dbdate = dateValue.split(' ')[0];
                                                var date = new Date(dateValue);
                                                var hours = date.getHours();
                                                if(hours < 10){
                                                    hours = '0' + hours;
                                                }
                                                var minutes = date.getMinutes();
                                                if(minutes < 10){
                                                    minutes = '0' + minutes;
                                                }
                                                $('#'+formId+' #'+value.autoPopulateFieldName+'_date').val(dbdate);
                                                $('#'+formId+' #'+value.autoPopulateFieldName+'_hours').val(hours);
                                                $('#'+formId+' #'+value.autoPopulateFieldName+'_minutes').val(minutes);
                                            }//end of if
                                        }else{
                                            if(value.autoPopulateFieldName != ''){
                                                $("#"+formId+" #"+value.autoPopulateFieldName).val('');
                                            }//end of if
                                        }//end of else   
                                    });//end of each

                                    //field color
                                    var fieldColorArray = data['fieldColorrArray'];
                                    $.each(fieldColorArray, function(index,value){
                                        if(value.type == 'datetimecombo'){
                                            if(value.fieldName != ''){
                                                $('#'+formId+' #'+value.fieldName+'_date').css('background','');
                                                $('#'+formId+' #'+value.fieldName+'_hours').css('background','');
                                                $('#'+formId+' #'+value.fieldName+'_minutes').css('background','');
                                            }//end of if
                                        }else{
                                            if(value.fieldName == 'revision'){
                                                $('#'+formId+' input[name=revision]').css('background','');
                                            }else if(value.fieldName == 'currency_id'){
                                                $('#'+formId+' #currency_id_select').css('background','');
                                            }else{
                                                if(value.fieldName != ''){
                                                    $('#'+formId+' #'+value.fieldName).css('background','');
                                                }//end of if
                                            }//end of else 
                                        }//end of else  
                                    });//end of each
                                }//end of else 
                            });//end of each 
                        }//end of success        
                    });//end of ajax
                });//end of function
            });//end of function
        });//end of function
    }//end of success
});//end of ajax

if(recordId != ''){
    var formData = $("#EditView").serialize();
    $.ajax({
        url: "index.php?entryPoint=VIDynamicPanelsCheckCondition",
        type: "post",
        data: {module : moduleName,
              recordId : recordId,
              formData : formData,
              actionName : actionName},
        dataType : "JSON",
        success: function(result) {
            $.each(result,function(index,data){
                if(data['success'] == 'true'){
                    var editViewTabsHide = data['editViewTabsHide'];
                    var editViewPanelsHide = data['editViewPanelsHide'];
                    var editViewTabsShow = data['editViewTabsShow'];
                    var editViewPanelsShow = data['editViewPanelsShow'];

                    //tabs Hide
                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                        var panelName = $.trim($(this).text());
                        var tabId = $(this).attr("id");
                        if(jQuery.inArray(panelName,editViewTabsHide) != '-1'){
                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                            var arr = tabId.substr(-1);;
                            $('div[id=tab-content-'+arr+']').hide();
                        }//end of if
                    });//end of function

                    $('div .panel-heading').each(function(){
                        var panels = $.trim($(this).closest('div').text());
                        if(jQuery.inArray(panels,editViewTabsHide) != '-1'){
                            $(this).closest('div').parent().hide();
                        }//end of if
                    });//end of function

                    //panels Hide
                    $('div .panel-heading').each(function(){
                        var panels = $.trim($(this).closest('div').text());
                        if(jQuery.inArray(panels,editViewPanelsHide) != '-1'){
                            $(this).closest('div').parent().hide();
                        }//end of if
                    });//end of function

                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                        var panelName = $.trim($(this).text());
                        var tabId = $(this).attr("id");
                        if(jQuery.inArray(panelName,editViewPanelsHide) != '-1'){
                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                            var arr = tabId.substr(-1);;
                            $('div[id=tab-content-'+arr+']').hide();
                        }//end of if
                    });//end of function

                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                        var panelName = $.trim($(this).text());
                        var tabId = $(this).attr("id");
                        $('body').on('click','ul.nav-tabs > li > a[id='+tabId+']',function(){
                            
                            $('div .panel-heading').each(function(){
                                var panels = $.trim($(this).closest('div').text());
                                if(jQuery.inArray(panels,editViewPanelsHide) != '-1'){
                                        $(this).closest('div').parent().hide();
                                }//end of if 
                            });//end of each

                            if(jQuery.inArray(panelName,editViewPanelsHide) != '-1'){
                                $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                                var arr = tabId.substr(-1);
                                $('div[id=tab-content-'+arr+']').hide();
                            }//end of if

                            if(jQuery.inArray(panelName,editViewTabsHide) != '-1'){
                                $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                                var arr = tabId.substr(-1);
                                $('div[id=tab-content-'+arr+']').hide();
                            }//end of if

                            $('div .panel-heading').each(function(){
                                var panels = $.trim($(this).closest('div').text());
                                if(jQuery.inArray(panels,editViewTabsHide) != '-1'){
                                        $(this).closest('div').parent().hide();
                                }//end of if 
                            });//end of each
                        });
                    });

                    //tabs Show
                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                        var panelName = $.trim($(this).text());
                        var tabId = $(this).attr("id");
                        if(jQuery.inArray(panelName,editViewTabsShow) != '-1'){
                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                            var arr = tabId.substr(-1);;
                            var currentTab = $('li.active > a[data-toggle=tab]').attr('id');
                            if(currentTab != '' && currentTab != undefined){
                                var arr = currentTab.substr(-1);
                                $('div[id=tab-content-'+arr+']').show();
                            }
                        }//end of if
                    });//end of function

                    $('div .panel-heading').each(function(){
                        var panels = $.trim($(this).closest('div').text());
                        if(jQuery.inArray(panels,editViewTabsShow) != '-1'){
                            $(this).closest('div').parent().show();
                        }//end of if
                    });//end of function

                    //panels Show
                    $('div .panel-heading').each(function(){
                        var panels = $.trim($(this).closest('div').text());
                        var tabPanelId = $.trim($(this).closest('div').attr('class').substr(-1));
                        var currentTab = $('li.active > a[data-toggle=tab]').attr('id');
                        if(currentTab != '' && currentTab != undefined){
                            var arr = currentTab.substr(-1);
                        }
                        if(jQuery.inArray(panels,editViewPanelsShow) != '-1'){
                            if(arr == '' || arr == undefined){
                                $(this).closest('div').parent().show();
                            }else{
                                if(arr == tabPanelId){
                                    $(this).closest('div').parent().show();
                                }
                            }
                        }//end of if
                    });//end of function

                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                        var panelName = $.trim($(this).text());
                        var tabId = $(this).attr("id");
                        if(jQuery.inArray(panelName,editViewPanelsShow) != '-1'){
                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                            var arr = tabId.substr(-1);;
                            $('div[id=tab-content-'+arr+']').show();
                        }//end of if
                    });//end of function


                    $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                        var panelName = $.trim($(this).text());
                        var tabId = $(this).attr("id");
                        $('body').on('click','ul.nav-tabs > li > a[id='+tabId+']',function(){
                            var arr = tabId.substr(-1);
                            $('div .panel-heading').each(function(){
                                var panels = $.trim($(this).closest('div').text());
                                var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                                if(arr == tabPanelId){
                                    if(jQuery.inArray(panels,editViewPanelsShow) != '-1'){
                                            $(this).closest('div').parent().show();
                                    }//end of if
                                }//end of if 
                            });//end of each

                            if(jQuery.inArray(panelName,editViewPanelsShow) != '-1'){
                                $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                                var arr = tabId.substr(-1);
                                $('div[id=tab-content-'+arr+']').show();
                            }//end of if

                            if(jQuery.inArray(panelName,editViewTabsShow) != '-1'){
                                $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                                var arr = tabId.substr(-1);
                                $('div[id=tab-content-'+arr+']').show();
                            }//end of if

                            $('div .panel-heading').each(function(){
                                var panels = $.trim($(this).closest('div').text());
                                var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                                if(arr == tabPanelId){
                                    if(jQuery.inArray(panels,editViewTabsShow) != '-1'){
                                            $(this).closest('div').parent().show();
                                    }//end of if 
                                }//end of if
                            });//end of each
                        });
                    });

                    //remove panels required fields validation which is hide 
                    var removeFromValidatePanelFields = data['removeFromValidatePanelFields'];
                    $.each(removeFromValidatePanelFields, function (index, value) {
                        removeFromValidate(formId,value);
                    });//end of function

                    //fields hide
                    var fieldsHide = data['fieldsHideArray'];
                    $.each(fieldsHide, function (index, value) {
                        var find = "_address_";
                        if(value.indexOf(find) != -1){
                            $('#'+value).closest('tr').hide();
                        }else{
                           $("div[field='"+value+"']").parent().hide();
                        }//end of else
                    });//end of function 

                    //fields show
                    var fieldsShow = data['fieldsShowArray'];
                    $.each(fieldsShow, function (index, value) {
                        var find = "_address_";
                        if(value.indexOf(find) != -1){
                            $('#'+value).closest('tr').show();
                        }else{
                           $("div[field='"+value+"']").parent().show();
                        }//end of else
                    });//end of function 

                    //remove fields required validation which is hide
                    var removeFromValidateFieldsHide = data['removeFromValidateFieldsHide'];
                    $.each(removeFromValidateFieldsHide, function (index, value) {
                        removeFromValidate(formId,value);
                    });//end of function

                    //add to validate fields which is show
                    var addToValidateFieldsShow = data['addToValidateFieldsShow'];
                    $.each(addToValidateFieldsShow,function(index,value){
                        if(value.type == 'file'){
                            addToValidate(formId,''+value.field+'_file','',true,'Field is required.');
                        }else{
                            addToValidate(formId,''+value.field+'','',true,'Field is required.');
                        }//end of else
                    });//end of function

                    //add to validate panels fields which is show
                    var addToValidatePanelFieldsShow = data['addToValidatePanelFieldsShow'];
                    $.each(addToValidatePanelFieldsShow,function(index,value){
                        if(value.type == 'file'){
                            addToValidate(formId,''+value.field+'_file','',true,'Field is required.');
                        }else{
                            addToValidate(formId,''+value.field+'','',true,'Field is required.');
                        }//end of else
                    });//end of function

                    //field mandatory
                    var fieldsMandatoryArray = data['fieldsMandatoryArray'];
                    $.each(fieldsMandatoryArray,function(index,value){
                        var find = "_address_";
                        if(value.field.indexOf(find) != -1){
                            var requiredClass = $('#'+value.field).closest('td').prev('td').find('span').attr('class');
                            if(requiredClass != 'required'){
                                $('#'+value.field).closest('td').prev('td').append('<span class="required">*</span>');
                            }//end of if
                            var requiredClass = $("div[field='"+value.field+"']").prev().find('span').attr('class');
                            if(requiredClass != 'required'){
                                $("div[field='"+value.field+"']").prev().append('<span class="required">*</span>');    
                            }//end of if
                        }else{
                            var requiredClass = $("div[field='"+value.field+"']").prev().find('span').attr('class');
                            if(requiredClass != 'required'){
                                $("div[field='"+value.field+"']").prev().append('<span class="required">*</span>');    
                            }//end of if
                        }//end of else
                        addToValidate(formId,''+value.field+'','',true,'Field is required.');
                    });//end of function

                    //field readonly
                    var fieldsReadOnlyArray = data['fieldsReadOnlyArray'];
                    $.each(fieldsReadOnlyArray, function (index, value) {
                        if(value.type == 'enum'  || value.type == 'bool' || value.type == "dynamicenum" || value.type == "radioenum" || value.type == "multienum"){
                            $("#"+value.field).prop("disabled", true);
                        }else if(value.type == 'date'){
                            $("#"+value.field).prop("readonly", true);
                            $("#"+value.field+"_trigger").prop("disabled", true);
                        }else if(value.type == 'datetimecombo' || value.type == 'datetime'){
                            $("#"+value.field+"_date").prop("readonly", true);
                            $("#"+value.field+"_trigger").prop("disabled", true);
                            $("#"+value.field+"_hours").prop("disabled", true);
                            $("#"+value.field+"_minutes").prop("disabled", true);
                        }else if(value.type == 'relate'){
                            $("#"+value.field).prop("readonly", true);
                            $("#btn_"+value.field).prop("disabled", true);
                            $("#btn_clr_"+value.field).prop("disabled", true);
                        }else{
                            if(value.field == 'currency_id'){
                                $("#"+value.field+'_select').prop("disabled", true);
                            }else if(value.field == 'revision'){
                                $("input[name="+value.field+"]").prop("readonly", true);
                            }else if(value.field == 'account_name' && moduleName == 'Leads'){
                                $("input[name="+value.field+"]").removeAttr("readonly", true);
                            }else{
                                $("#"+value.field).prop("readonly", true);    
                            }//end pf else
                        }//end of else 
                    });//end of function  

                    //field autopopulate 
                    var autoPopulateField = data['autoPopulateFieldArray'];
                    $.each(autoPopulateField, function (index, value) {
                        if(value.type == 'bool'){
                            if(value.autopopulateFieldValue == '1'){
                                $('#'+value.autoPopulateFieldName).attr('checked',true);
                            }//end of if
                        }else if(value.type == 'datetimecombo'){
                            var dbdate = value.autopopulateFieldValue.split(' ')[0];
                            var date = new Date(value.autopopulateFieldValue);
                            var hours = date.getHours();
                            if(hours < 10){
                                hours = '0' + hours;
                            }
                            var minutes = date.getMinutes();
                            if(minutes < 10){
                                minutes = '0' + minutes;
                            }
                            $('#'+value.autoPopulateFieldName+'_date').val(dbdate);
                            $('#'+value.autoPopulateFieldName+'_hours').val(hours);
                            $('#'+value.autoPopulateFieldName+'_minutes').val(minutes);
                        }else{
                            $('#'+value.autoPopulateFieldName).val(value.autopopulateFieldValue);
                        }//end of else
                    });//end of each

                    //field color
                    var fieldColorArray = data['fieldColorrArray'];
                    $.each(fieldColorArray, function(index,value){
                        if(value.type == 'datetimecombo'){
                            $('#'+value.fieldName+'_date').css('background','#'+value.fieldColor);
                            $('#'+value.fieldName+'_hours').css('background','#'+value.fieldColor);
                            $('#'+value.fieldName+'_minutes').css('background','#'+value.fieldColor);
                        }else{
                            if(value.fieldName == 'revision'){
                                $('input[name=revision]').css('background','#'+value.fieldColor);
                            }else if(value.fieldName == 'currency_id'){
                                $('#currency_id_select').css('background','#'+value.fieldColor);
                            }else{
                                $('#'+value.fieldName).css('background','#'+value.fieldColor);
                            }//end of else
                        }//end of else
                    });//end of each
                }//end of if
            });//end of each
        }//end of success        
    });//end of ajax
}//end of if