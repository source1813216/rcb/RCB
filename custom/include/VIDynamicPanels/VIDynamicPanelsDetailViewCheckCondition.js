/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//default panels and fields hide
$.ajax({
    url: "index.php?entryPoint=VIDynamicPanelsDefault",
    type: "post",
    data: {module : moduleName,
          actionName : actionName},
    dataType : "JSON",
    success: function (response) {
        var defaultFieldsHide = response['defaultFieldsHide'];
        var viewTabsHide = response['viewTabsHide'];
        var viewPanelsHide = response['viewPanelsHide'];

        //default fields hide
        $.each(defaultFieldsHide, function (index, value) {
            var find = "_address_";
            if(value.indexOf(find) != -1){
                $('#'+value).closest('div').hide();
                $('#'+value).closest('div').parent().hide();
            }else{
                $("div[field='"+value+"']").parent().hide();    
            }//end of else
        });//end of function
            
        //default tabs hide
        viewTabsHide = jQuery.parseJSON(viewTabsHide);
        $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
            var panelName = $.trim($(this).text());
            var tabId = $(this).attr("id");
            if(jQuery.inArray(panelName,viewTabsHide) != '-1'){
                $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                var arr = tabId.substr(-1);;
                $('div[id=tab-content-'+arr+']').hide();
            }//end of if
        });//end of function

        $('div .panel-heading').each(function(){
            var panels = $.trim($(this).closest('div').text());
            if(jQuery.inArray(panels,viewTabsHide) != '-1'){
                $(this).closest('div').parent().hide();
            }//end of if
        });//end of function

        //default panels hide
        viewPanelsHide = jQuery.parseJSON(viewPanelsHide);
        $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
            var panelName = $.trim($(this).text());
            var tabId = $(this).attr("id");
            if(jQuery.inArray(panelName,viewPanelsHide) != '-1'){
                $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                var arr = tabId.substr(-1);;
                $('div[id=tab-content-'+arr+']').hide();
            }//end of if
        });//end of function

        $('div .panel-heading').each(function(){
            var panels = $.trim($(this).closest('div').text());
            if(jQuery.inArray(panels,viewPanelsHide) != '-1'){
                    $(this).closest('div').parent().hide();
            }//end of if
        });//end of function
        
        $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
            var panelName = $.trim($(this).text());
            var tabId = $(this).attr("id");
            $('body').on('click','ul.nav-tabs > li > a[id='+tabId+']',function(){
                var arr = tabId.substr(-1);
                $('div .panel-heading').each(function(){
                    var panels = $.trim($(this).closest('div').text());
                    var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                    if(arr == tabPanelId){
                        if(jQuery.inArray(panels,viewPanelsHide) != '-1'){
                            $(this).closest('div').parent().hide();
                        }//end of if 
                    }//end of if
                });//end of each

                if(jQuery.inArray(panelName,viewPanelsHide) != '-1'){
                    $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                    var arr = tabId.substr(-1);
                    $('div[id=tab-content-'+arr+']').hide();
                }//end of if

                if(jQuery.inArray(panelName,viewTabsHide) != '-1'){
                    $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                    var arr = tabId.substr(-1);
                    $('div[id=tab-content-'+arr+']').hide();
                }//end of if

                $('div .panel-heading').each(function(){
                    var panels = $.trim($(this).closest('div').text());
                    var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                    if(arr == tabPanelId){
                        if(jQuery.inArray(panels,viewTabsHide) != '-1'){
                                $(this).closest('div').parent().hide();
                        }//end of if 
                    }//end of if
                });//end of each
            });
        });
    }//end of success
});//end of ajax

$.ajax({
        url: "index.php?entryPoint=VIDynamicPanelsDetailViewCheckCondition",
        type: "post",
        data: {module : moduleName,
              recordId : recordId},
        dataType : "JSON",
        success: function(result) {
            $.each(result,function(index,data){
                var detailViewTabsHide = data['detailViewTabsHide'];
                var detailViewPanelsHide = data['detailViewPanelsHide']; 
                var detailViewTabsShow = data['detailViewTabsShow'];
                var detailViewPanelsShow = data['detailViewPanelsShow']; 
                var fieldsShow = data['fieldsShowArray'];
                var fieldsHide = data['fieldsHideArray'];
                var fieldsColorArray = data['fieldColorrArray'];
                var fieldsReadOnlyArray = data['fieldsReadOnlyArray'];
               
                //tabs Hide
                detailViewTabsHide = jQuery.parseJSON(detailViewTabsHide);
                $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                    var panelName = $.trim($(this).text());
                    var tabId = $(this).attr("id");
                    if(jQuery.inArray(panelName,detailViewTabsHide) != '-1'){
                        $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                        var arr = tabId.substr(-1);;
                        $('div[id=tab-content-'+arr+']').hide();
                    }
                });
                $('div .panel-heading').each(function(){
                    var panels = $.trim($(this).closest('div').text());
                    if(jQuery.inArray(panels,detailViewTabsHide) != '-1'){
                        $(this).closest('div').parent().hide();
                    }
                });
                //panels Hide
                detailViewPanelsHide = jQuery.parseJSON(detailViewPanelsHide);
                $('div .panel-heading').each(function(){
                    var panels = $.trim($(this).closest('div').text());
                    if(jQuery.inArray(panels,detailViewPanelsHide) != '-1'){
                        $(this).closest('div').parent().hide();
                    }
                });
                $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                    var panelName = $.trim($(this).text());
                    var tabId = $(this).attr("id");
                    if(jQuery.inArray(panelName,detailViewPanelsHide) != '-1'){
                        $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                        var arr = tabId.substr(-1);;
                        $('div[id=tab-content-'+arr+']').hide();
                    }
                });
                
                $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                    var panelName = $.trim($(this).text());
                    var tabId = $(this).attr("id");
                    $('body').on('click','ul.nav-tabs > li > a[id='+tabId+']',function(){
                        var arr = tabId.substr(-1);
                        $('div .panel-heading').each(function(){
                            var panels = $.trim($(this).closest('div').text());
                            var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                            if(arr == tabPanelId){
                                if(jQuery.inArray(panels,detailViewPanelsHide) != '-1'){
                                    $(this).closest('div').parent().hide();
                                }//end of if 
                            }//end of if
                        });//end of each

                        if(jQuery.inArray(panelName,detailViewPanelsHide) != '-1'){
                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                            var arr = tabId.substr(-1);
                            $('div[id=tab-content-'+arr+']').hide();
                        }//end of if

                        if(jQuery.inArray(panelName,detailViewTabsHide) != '-1'){
                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().hide();
                            var arr = tabId.substr(-1);
                            $('div[id=tab-content-'+arr+']').hide();
                        }//end of if

                        $('div .panel-heading').each(function(){
                            var panels = $.trim($(this).closest('div').text());
                            var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                            if(arr == tabPanelId){
                                if(jQuery.inArray(panels,detailViewTabsHide) != '-1'){
                                        $(this).closest('div').parent().hide();
                                }//end of if 
                            }//end of if
                        });//end of each
                    });
                });

                //tabs show
                detailViewTabsShow = jQuery.parseJSON(detailViewTabsShow);
                $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                    var panelName = $.trim($(this).text());
                    var tabId = $(this).attr("id");
                    if(jQuery.inArray(panelName,detailViewTabsShow) != '-1'){
                        $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                        var arr = tabId.substr(-1);;
                        $('div[id=tab-content-'+arr+']').show();
                    }
                });
                $('div .panel-heading').each(function(){
                    var panels = $.trim($(this).closest('div').text());
                    if(jQuery.inArray(panels,detailViewTabsShow) != '-1'){
                        $(this).closest('div').parent().show();
                    }
                }); 
                //panels show
                
                detailViewPanelsShow = jQuery.parseJSON(detailViewPanelsShow);
                $('div .panel-heading').each(function(){
                    var panels = $.trim($(this).closest('div').text());
                    if(jQuery.inArray(panels,detailViewPanelsShow) != '-1'){
                        var tabPanelId = $.trim($(this).closest('div').parent().attr('class').substr(-1));
                        if($.isNumeric(tabPanelId)){
                            if($('a#tab'+tabPanelId).closest('li').hasClass('active')){
                                $(this).closest('div').parent().show();
                            }
                        }else{
                            $(this).closest('div').parent().show();
                        }
                    }
                }); 
                $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                    var panelName = $.trim($(this).text());
                    var tabId = $(this).attr("id");
                    if(jQuery.inArray(panelName,detailViewPanelsShow) != '-1'){
                        $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                        var arr = tabId.substr(-1);;
                        $('div[id=tab-content-'+arr+']').show();
                    }
                });
                
                $('ul.nav-tabs > li > a[data-toggle=tab]').each(function(){
                    var panelName = $.trim($(this).text());
                    var tabId = $(this).attr("id");
                    $('body').on('click','ul.nav-tabs > li > a[id='+tabId+']',function(){
                        var arr = tabId.substr(-1);
                        $('div .panel-heading').each(function(){
                            var panels = $.trim($(this).closest('div').text());
                            var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                            if(arr == tabPanelId){
                                if(jQuery.inArray(panels,detailViewPanelsShow) != '-1'){
                                    $(this).closest('div').parent().show();
                                }//end of if 
                            }//end of if
                        });//end of each

                        if(jQuery.inArray(panelName,detailViewPanelsShow) != '-1'){
                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                            var arr = tabId.substr(-1);
                            $('div[id=tab-content-'+arr+']').show();
                        }//end of if

                        if(jQuery.inArray(panelName,detailViewTabsShow) != '-1'){
                            $('ul.nav-tabs > li > a[id='+tabId+']').parent().show();
                            var arr = tabId.substr(-1);
                            $('div[id=tab-content-'+arr+']').show();
                        }//end of if

                        $('div .panel-heading').each(function(){
                            var panels = $.trim($(this).closest('div').text());
                            var tabPanelId = $.trim($(this).parent('div').attr('class').substr(-1));
                            if(arr == tabPanelId){
                                if(jQuery.inArray(panels,detailViewTabsShow) != '-1'){
                                        $(this).closest('div').parent().show();
                                }//end of if 
                            }//end of if
                        });//end of each
                    });
                });
                
                //fields show
                $.each(fieldsShow, function (index, value) {
                    var find = "_address_";
                    if(value.indexOf(find) != -1){
                        $('#'+value).closest('div').show();
                        $('#'+value).closest('div').parent().show();
                    }else{
                        $("div[field='"+value+"']").parent().show();    
                    }
                }); 
                //fields hide
                $.each(fieldsHide, function (index, value) {
                    var find = "_address_";
                    if(value.indexOf(find) != -1){
                        $('#'+value).closest('div').hide();
                        $('#'+value).closest('div').parent().hide();
                    }else{
                        $("div[field='"+value+"']").parent().hide();    
                    }
                });

                 //fields color
                $.each(fieldsColorArray, function (index, value) {
                    var find = "_address_";
                    var val = value.fieldName;
                    if(val.indexOf(find) != -1){
                        $('#'+val).closest('div').css('background','#'+value.fieldColor);
                    }else{
                        if(moduleName == 'Spots'){
                            $("#"+val).css('background','#'+value.fieldColor);
                        }else{
                            $("div[field='"+val+"']").css('background','#'+value.fieldColor);
                        }
                    }
                });

                //field readonly    
                $.each(fieldsReadOnlyArray, function (index, value) {
                    var find = "_address_";
                    var val = value['field'];
                    if(val.indexOf(find) != -1){
                        $('#'+val).closest('div').css('pointer-events','none');
                    }else{
                        $("div[field='"+value['field']+"']").css('pointer-events','none');
                    }
                });
            });
        }
});
