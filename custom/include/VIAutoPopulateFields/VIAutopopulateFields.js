/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
var decodeUrl = decodeURIComponent(window.location.href);
function getParam(name){
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec(decodeUrl);
    if( results == null )
        return "";
  	else
        return results[1];
}//end of function

if(module == ''){
	var sourceModule = getParam('module');
}else{
	var sourceModule = module;
}//end of else

var returnModule = $('input[name="return_module"]').val();

$.ajax({		
  	type: 'POST',
  	url: 'index.php?entryPoint=VIAutoPopulateFieldsRelateFieldName',
  	data: {sourceModule: sourceModule},  
	dataType: 'JSON',
	success: function(response){
        $.each (response, function(key,value) {
        	$.each(value,function(relatedFieldName,relatedFieldData){
        		var fieldName = relatedFieldData['fieldName'];
        		//on load
        		if(action != 'EditView' || returnModule != ''){
					if(returnModule == ''){
						formId = 'form_SubpanelQuickCreate_'+sourceModule;
					}else{
						formId = 'EditView';
					}//end of else

					var relateFieldIdValue = $('#'+formId+' #'+relatedFieldData['fieldNameId']).val();
					var relateFieldNameValue = $('#'+formId+' #'+fieldName).val();

					if(relateFieldIdValue != '' && relateFieldNameValue != ''){
						autoPopulateRelatedFieldValue(formId,key,sourceModule,relateFieldIdValue,relateFieldNameValue,relatedFieldData);
					}//end of if
				}//end of if

				//on change
        		YAHOO.util.Event.addListener(fieldName, 'change', function(){
        			if(action == 'EditView'){
						formId = 'EditView';
					}else{
						formId = 'form_SubpanelQuickCreate_'+sourceModule;
					}//end of else

					var relateFieldIdValue = $('#'+formId+' #'+relatedFieldData['fieldNameId']).val();
					var relateFieldNameValue = $('#'+formId+' #'+fieldName).val();
					
					if(relateFieldIdValue != '' && relateFieldNameValue != ''){
						autoPopulateRelatedFieldValue(formId,key,sourceModule,relateFieldIdValue,relateFieldNameValue,relatedFieldData);
					}//end of if
    			});//end of function
        	});//end of each
		});//end of each
	}//end of success
});//end of ajax

function autoPopulateRelatedFieldValue(formId,key,sourceModule,relateFieldIdValue,relateFieldNameValue,relatedFieldData){
	$.ajax({		
		type: 'POST',
		url: 'index.php?entryPoint=VIAutoPopulateFieldsRelateFieldValue',
		data: {
			fieldType: key,
			sourceModule : sourceModule,		
			relateFieldIdValue : relateFieldIdValue,
			allConfigData: relatedFieldData},
		dataType : 'JSON',
		success: function(response){
			if(response['autoPopulateFieldMappingValue'] != ''){
				$.each(response['autoPopulateFieldMappingValue'],function(fkey,fvalue){
					//data
					autoPopulateFieldName = fvalue['autoPopulateFieldName'];
					autoPopulateFieldVal = fvalue['autoPopulateFieldVal'];
					autoPopulateFieldType = fvalue['autoPopulateFieldType'];

					if(autoPopulateFieldType == 'datetimecombo'){
						autoPopulateDateTimeTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName);
       				}else if(autoPopulateFieldType == 'radioenum'){
       					autoPopulateRadioTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName);
   					}else if(autoPopulateFieldType == 'multienum'){
   						autoPopulateMultiEnumTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName);
   					}else if(autoPopulateFieldType == 'bool'){
   						autoPopulateBoolTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName);
       				}else if(autoPopulateFieldType == 'dynamicenum'){
       					parentField = fvalue['parentField'];
       					autoPopulateEnumDynamicEnumTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName,parentField);
					}else if(autoPopulateFieldType == 'enum'){
						autoPopulateEnumDynamicEnumTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName,'');
					}else{
						autoPopulateFieldValue(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName);	
       				}//end of else
				});//end of each
			}//end of if

			if(response['autoPopulateCalculateFieldValue'] != ''){
				$.each(response['autoPopulateCalculateFieldValue'],function(ckey,cvalue){
					//data
					autoPopulateFieldName = cvalue['autoPopulateFieldName'];
					autoPopulateFieldVal = cvalue['autoPopulateFieldVal'];
					autoPopulateFieldType = cvalue['autoPopulateFieldType'];

					if(autoPopulateFieldType == 'datetimecombo' || autoPopulateFieldType == 'datetime'){
						autoPopulateDateTimeTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName);
       				}else if(autoPopulateFieldType == 'date'){
       					autoPopulateDateTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName);
       				}else{
       					autoPopulateFieldValue(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName);
       				}//end of else
				});//end of each
			}//end of if
		}//end of success
	});//end of ajax
}//end of function

function autoPopulateDateTimeTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName){
	var date = new Date(autoPopulateFieldVal);
	if(date != 'Invalid Date'){
		var datetime =autoPopulateFieldVal.split(' ')[0];
		//hours
		var hours = date.getHours();
		if (hours < 10){
			hours = '0' + hours;
		}//end of if

		//minutes 
		var min = date.getMinutes();
		if (min < 10){
			min = '0' + min;
		}//end of if

		if(relateFieldIdValue != '' && relateFieldNameValue != ''){
			$('#'+formId+' #'+autoPopulateFieldName+'_date').val(datetime);
			$('#'+formId+' #'+autoPopulateFieldName+'_hours').val(hours);
			$('#'+formId+' #'+autoPopulateFieldName+'_minutes').val(min);
		}else{
			$('#'+formId+' #'+autoPopulateFieldName+'_date').val('');
			$('#'+formId+' #'+autoPopulateFieldName+'_hours').val('');
			$('#'+formId+' #'+autoPopulateFieldName+'_minutes').val('');
		}//end of else
	}//end of if
}//end of function

function autoPopulateRadioTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName){
	if(relateFieldIdValue != '' && relateFieldNameValue != ''){
		$('#'+formId+' input[name="'+autoPopulateFieldName+'"][value="'+autoPopulateFieldVal+'"]').prop('checked', true);
	}else{
		$('#'+formId+' input[name="'+autoPopulateFieldName+'"][value="'+autoPopulateFieldVal+'"]').prop('checked', false);
	}//end of else
}//end of function

function autoPopulateMultiEnumTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName){
	if(relateFieldIdValue != '' && relateFieldNameValue != ''){
		$.each(autoPopulateFieldVal,function(k,multiVal){
			$("#"+formId+" select[id='"+autoPopulateFieldName+"']").find("option[value='"+multiVal+"']").attr("selected",true);
			$("#"+formId+" select[id='"+autoPopulateFieldName+"']").find("option[value='"+multiVal+"']").prop("selected",true);
		});//end of each
	}else{
		$.each(autoPopulateFieldName,function(k,multiVal){
			$("#"+formId+" select[id='"+autoPopulateFieldName+"']").find("option[value='"+multiVal+"']").attr("selected",false);
			$("#"+formId+" select[id='"+autoPopulateFieldName+"']").find("option[value='"+multiVal+"']").prop("selected",false);
		});//end of each
	}//end of else
}//end of function

function autoPopulateBoolTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName){
	if(relateFieldIdValue != '' && relateFieldNameValue != ''){
		if(autoPopulateFieldVal == '1'){
			$("#"+formId+" #"+autoPopulateFieldName).prop('checked',true);
			var elementId = $('#'+formId+' #'+autoPopulateFieldName);
            if( elementId.change != null ){
                $(elementId).trigger('change');           
            }//end of if
		}else{
			$("#"+formId+" #"+autoPopulateFieldName).prop('checked',false);
			var elementId = $('#'+formId+' #'+autoPopulateFieldName);
            if( elementId.change != null ){
                $(elementId).trigger('change');           
            }//end of if
		}//end of else
	}else{
		$("#"+formId+" #"+autoPopulateFieldName).prop('checked',false);
		var elementId = $('#'+formId+' #'+autoPopulateFieldName);
        if( elementId.change != null ){
            $(elementId).trigger('change');           
        }//end of if
	}//end of else
}//end of function

function autoPopulateEnumDynamicEnumTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName,parentField){
	if(relateFieldIdValue != '' && relateFieldNameValue != ''){
		if(parentField != ''){
			updateDynamicEnum(parentField,autoPopulateFieldName);
		}//end of if

		$('#'+formId+' #'+autoPopulateFieldName).val(autoPopulateFieldVal);
		$('#'+formId+' select[id='+autoPopulateFieldName+']').val(autoPopulateFieldVal);
	}else{
		$('#'+formId+' #'+autoPopulateFieldName).val('');
		$('#'+formId+' select[id='+autoPopulateFieldName+']').val('');
	}//end of else
}//end of function

function autoPopulateFieldValue(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName){
	if(relateFieldIdValue != '' && relateFieldNameValue != ''){
		$('#'+formId+' #'+autoPopulateFieldName).val(autoPopulateFieldVal);
		$('#'+formId+' input[name='+autoPopulateFieldName+']').val(autoPopulateFieldVal);
	}else{
		$('#'+formId+' #'+autoPopulateFieldName).val('');
		$('#'+formId+' input[name='+autoPopulateFieldName+']').val('');
	}//end of else
}//end of function

function autoPopulateDateTypeFieldVal(relateFieldIdValue,relateFieldNameValue,formId,autoPopulateFieldVal,autoPopulateFieldName){
	var date = new Date(autoPopulateFieldVal);
	if(date != 'Invalid Date'){
		var datetime = autoPopulateFieldVal.split(' ')[0];
		if(relateFieldIdValue != '' && relateFieldNameValue != ''){
			$('#'+formId+' #'+autoPopulateFieldName).val(datetime);
		}else{
			$('#'+formId+' #'+autoPopulateFieldName).val('');
		}//end of else
	}//end of if
}//end of function