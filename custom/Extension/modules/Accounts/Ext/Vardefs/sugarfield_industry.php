<?php
 // created: 2021-05-20 10:37:39
$dictionary['Account']['fields']['industry']['len']=100;
$dictionary['Account']['fields']['industry']['required']=true;
$dictionary['Account']['fields']['industry']['inline_edit']=true;
$dictionary['Account']['fields']['industry']['comments']='The company belongs in this industry';
$dictionary['Account']['fields']['industry']['merge_filter']='disabled';

 ?>