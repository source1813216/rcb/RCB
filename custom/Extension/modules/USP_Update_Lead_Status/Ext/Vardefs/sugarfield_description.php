<?php
 // created: 2021-08-17 16:21:28
$dictionary['USP_Update_Lead_Status']['fields']['description']['required']=true;
$dictionary['USP_Update_Lead_Status']['fields']['description']['inline_edit']=true;
$dictionary['USP_Update_Lead_Status']['fields']['description']['comments']='Full text of the note';
$dictionary['USP_Update_Lead_Status']['fields']['description']['merge_filter']='disabled';

 ?>