<?php
 // created: 2021-07-26 13:22:42
$layout_defs["AM_ProjectTemplates"]["subpanel_setup"]['am_projecttemplates_mou_mou_1'] = array (
  'order' => 100,
  'module' => 'MOU_MOU',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AM_PROJECTTEMPLATES_MOU_MOU_1_FROM_MOU_MOU_TITLE',
  'get_subpanel_data' => 'am_projecttemplates_mou_mou_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
