<?php
// created: 2021-08-03 18:02:01
$dictionary["Lead"]["fields"]["leads_project_1"] = array (
  'name' => 'leads_project_1',
  'type' => 'link',
  'relationship' => 'leads_project_1',
  'source' => 'non-db',
  'module' => 'Project',
  'bean_name' => 'Project',
  'side' => 'right',
  'vname' => 'LBL_LEADS_PROJECT_1_FROM_PROJECT_TITLE',
);
