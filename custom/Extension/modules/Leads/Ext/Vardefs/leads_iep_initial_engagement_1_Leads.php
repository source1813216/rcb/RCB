<?php
// created: 2021-05-18 07:14:04
$dictionary["Lead"]["fields"]["leads_iep_initial_engagement_1"] = array (
  'name' => 'leads_iep_initial_engagement_1',
  'type' => 'link',
  'relationship' => 'leads_iep_initial_engagement_1',
  'source' => 'non-db',
  'module' => 'IEP_Initial_Engagement',
  'bean_name' => 'IEP_Initial_Engagement',
  'side' => 'right',
  'vname' => 'LBL_LEADS_IEP_INITIAL_ENGAGEMENT_1_FROM_IEP_INITIAL_ENGAGEMENT_TITLE',
);
