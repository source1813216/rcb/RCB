<?php
 // created: 2022-08-01 13:08:01
$dictionary['Lead']['fields']['event_date_c']['inline_edit']='1';
$dictionary['Lead']['fields']['event_date_c']['options']='date_range_search_dom';
$dictionary['Lead']['fields']['event_date_c']['labelValue']='Event Start Date';
$dictionary['Lead']['fields']['event_date_c']['enable_range_search']='1';

 ?>