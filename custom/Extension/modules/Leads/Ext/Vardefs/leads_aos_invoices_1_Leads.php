<?php
// created: 2021-06-18 07:43:43
$dictionary["Lead"]["fields"]["leads_aos_invoices_1"] = array (
  'name' => 'leads_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'leads_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'side' => 'right',
  'vname' => 'LBL_LEADS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);
