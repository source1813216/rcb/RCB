<?php
 // created: 2022-12-13 17:59:21
$dictionary['Lead']['fields']['description']['inline_edit']=true;
$dictionary['Lead']['fields']['description']['comments']='Full text of the note';
$dictionary['Lead']['fields']['description']['merge_filter']='disabled';
$dictionary['Lead']['fields']['description']['audited']=true;

 ?>