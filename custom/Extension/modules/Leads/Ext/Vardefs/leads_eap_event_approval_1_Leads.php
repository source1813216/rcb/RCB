<?php
// created: 2021-06-07 14:48:34
$dictionary["Lead"]["fields"]["leads_eap_event_approval_1"] = array (
  'name' => 'leads_eap_event_approval_1',
  'type' => 'link',
  'relationship' => 'leads_eap_event_approval_1',
  'source' => 'non-db',
  'module' => 'EAP_Event_Approval',
  'bean_name' => 'EAP_Event_Approval',
  'side' => 'right',
  'vname' => 'LBL_LEADS_EAP_EVENT_APPROVAL_1_FROM_EAP_EVENT_APPROVAL_TITLE',
);
