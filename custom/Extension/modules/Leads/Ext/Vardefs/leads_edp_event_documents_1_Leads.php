<?php
// created: 2021-06-14 16:42:28
$dictionary["Lead"]["fields"]["leads_edp_event_documents_1"] = array (
  'name' => 'leads_edp_event_documents_1',
  'type' => 'link',
  'relationship' => 'leads_edp_event_documents_1',
  'source' => 'non-db',
  'module' => 'EDP_Event_Documents',
  'bean_name' => 'EDP_Event_Documents',
  'side' => 'right',
  'vname' => 'LBL_LEADS_EDP_EVENT_DOCUMENTS_1_FROM_EDP_EVENT_DOCUMENTS_TITLE',
);
