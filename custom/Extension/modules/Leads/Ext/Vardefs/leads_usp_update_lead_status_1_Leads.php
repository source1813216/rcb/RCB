<?php
// created: 2021-08-17 16:27:33
$dictionary["Lead"]["fields"]["leads_usp_update_lead_status_1"] = array (
  'name' => 'leads_usp_update_lead_status_1',
  'type' => 'link',
  'relationship' => 'leads_usp_update_lead_status_1',
  'source' => 'non-db',
  'module' => 'USP_Update_Lead_Status',
  'bean_name' => 'USP_Update_Lead_Status',
  'side' => 'right',
  'vname' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_USP_UPDATE_LEAD_STATUS_TITLE',
);
