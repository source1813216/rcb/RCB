<?php
 // created: 2021-06-15 16:46:24
$layout_defs["Leads"]["subpanel_setup"]['leads_edp_endorsements_1'] = array (
  'order' => 100,
  'module' => 'EDP_Endorsements',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_EDP_ENDORSEMENTS_TITLE',
  'get_subpanel_data' => 'leads_edp_endorsements_1',
  'top_buttons' => 
  array (
   /* 0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
	*/
  ),
);
