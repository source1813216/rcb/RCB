<?php
 // created: 2021-07-12 17:45:01
$layout_defs["Leads"]["subpanel_setup"]['leads_mou_mou_1'] = array (
  'order' => 100,
  'module' => 'MOU_MOU',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_MOU_MOU_1_FROM_MOU_MOU_TITLE',
  'get_subpanel_data' => 'leads_mou_mou_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
