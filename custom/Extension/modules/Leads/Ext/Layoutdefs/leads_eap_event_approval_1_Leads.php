<?php
 // created: 2021-06-07 14:48:33
$layout_defs["Leads"]["subpanel_setup"]['leads_eap_event_approval_1'] = array (
  'order' => 100,
  'module' => 'EAP_Event_Approval',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_EAP_EVENT_APPROVAL_1_FROM_EAP_EVENT_APPROVAL_TITLE',
  'get_subpanel_data' => 'leads_eap_event_approval_1',
  'top_buttons' => 
  array (
   /* 0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
	*/
  ),
);
