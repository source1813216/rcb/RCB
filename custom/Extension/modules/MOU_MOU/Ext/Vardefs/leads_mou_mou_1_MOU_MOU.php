<?php
// created: 2021-07-12 17:45:01
$dictionary["MOU_MOU"]["fields"]["leads_mou_mou_1"] = array (
  'name' => 'leads_mou_mou_1',
  'type' => 'link',
  'relationship' => 'leads_mou_mou_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_MOU_MOU_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_mou_mou_1leads_ida',
);
$dictionary["MOU_MOU"]["fields"]["leads_mou_mou_1_name"] = array (
  'name' => 'leads_mou_mou_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_MOU_MOU_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_mou_mou_1leads_ida',
  'link' => 'leads_mou_mou_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["MOU_MOU"]["fields"]["leads_mou_mou_1leads_ida"] = array (
  'name' => 'leads_mou_mou_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_mou_mou_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_MOU_MOU_1_FROM_MOU_MOU_TITLE',
);
