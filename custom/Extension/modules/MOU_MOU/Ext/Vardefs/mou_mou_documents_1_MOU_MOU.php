<?php
// created: 2021-07-09 09:34:34
$dictionary["MOU_MOU"]["fields"]["mou_mou_documents_1"] = array (
  'name' => 'mou_mou_documents_1',
  'type' => 'link',
  'relationship' => 'mou_mou_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_MOU_MOU_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
