<?php
// created: 2021-07-09 09:34:58
$dictionary["MOU_MOU"]["fields"]["mou_mou_tasks_1"] = array (
  'name' => 'mou_mou_tasks_1',
  'type' => 'link',
  'relationship' => 'mou_mou_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_MOU_MOU_TASKS_1_FROM_TASKS_TITLE',
);
