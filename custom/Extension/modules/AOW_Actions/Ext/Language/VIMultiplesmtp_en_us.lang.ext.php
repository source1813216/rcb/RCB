<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings = array('LBL_SENDEMAILUSINGMULTISMTP' => 'Send Email Using Multi SMTP');
$mod_strings['LBL_ENABLED_MULTI_SMTP_MSG'] = 'Multiple SMTP is not Enabled. Please Enable for Using Multiple SMTP feature';
$mod_strings['LBL_SELECT_USER'] = 'Outgoing Email Server(Select User)';
$mod_strings['LBL_SELECT_USER_INFO'] = "1. System will send email using SMTP set against selected user <br> 2. If Assigned User selected then it will consider Outgoing server of  Assigned User <br>3. If None is selected then first check current login user SMTP. If current login user have SMTP configured then consider his/her otherwise Global SMTP.";
?>