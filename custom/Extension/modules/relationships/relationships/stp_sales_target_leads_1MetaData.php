<?php
// created: 2021-08-15 18:03:18
$dictionary["stp_sales_target_leads_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'stp_sales_target_leads_1' => 
    array (
      'lhs_module' => 'STP_Sales_Target',
      'lhs_table' => 'stp_sales_target',
      'lhs_key' => 'id',
      'rhs_module' => 'Leads',
      'rhs_table' => 'leads',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'stp_sales_target_leads_1_c',
      'join_key_lhs' => 'stp_sales_target_leads_1stp_sales_target_ida',
      'join_key_rhs' => 'stp_sales_target_leads_1leads_idb',
    ),
  ),
  'table' => 'stp_sales_target_leads_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'stp_sales_target_leads_1stp_sales_target_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'stp_sales_target_leads_1leads_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'stp_sales_target_leads_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'stp_sales_target_leads_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'stp_sales_target_leads_1stp_sales_target_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'stp_sales_target_leads_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'stp_sales_target_leads_1leads_idb',
      ),
    ),
  ),
);