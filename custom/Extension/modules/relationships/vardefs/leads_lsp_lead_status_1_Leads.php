<?php
// created: 2021-07-15 11:19:53
$dictionary["Lead"]["fields"]["leads_lsp_lead_status_1"] = array (
  'name' => 'leads_lsp_lead_status_1',
  'type' => 'link',
  'relationship' => 'leads_lsp_lead_status_1',
  'source' => 'non-db',
  'module' => 'LSP_Lead_Status',
  'bean_name' => 'LSP_Lead_Status',
  'side' => 'right',
  'vname' => 'LBL_LEADS_LSP_LEAD_STATUS_1_FROM_LSP_LEAD_STATUS_TITLE',
);
