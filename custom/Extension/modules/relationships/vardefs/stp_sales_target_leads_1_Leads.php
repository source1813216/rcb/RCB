<?php
// created: 2021-08-15 18:03:18
$dictionary["Lead"]["fields"]["stp_sales_target_leads_1"] = array (
  'name' => 'stp_sales_target_leads_1',
  'type' => 'link',
  'relationship' => 'stp_sales_target_leads_1',
  'source' => 'non-db',
  'module' => 'STP_Sales_Target',
  'bean_name' => 'STP_Sales_Target',
  'vname' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_STP_SALES_TARGET_TITLE',
  'id_name' => 'stp_sales_target_leads_1stp_sales_target_ida',
);
$dictionary["Lead"]["fields"]["stp_sales_target_leads_1_name"] = array (
  'name' => 'stp_sales_target_leads_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_STP_SALES_TARGET_TITLE',
  'save' => true,
  'id_name' => 'stp_sales_target_leads_1stp_sales_target_ida',
  'link' => 'stp_sales_target_leads_1',
  'table' => 'stp_sales_target',
  'module' => 'STP_Sales_Target',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["stp_sales_target_leads_1stp_sales_target_ida"] = array (
  'name' => 'stp_sales_target_leads_1stp_sales_target_ida',
  'type' => 'link',
  'relationship' => 'stp_sales_target_leads_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_LEADS_TITLE',
);
