<?php
// created: 2021-07-26 13:22:42
$dictionary["MOU_MOU"]["fields"]["am_projecttemplates_mou_mou_1"] = array (
  'name' => 'am_projecttemplates_mou_mou_1',
  'type' => 'link',
  'relationship' => 'am_projecttemplates_mou_mou_1',
  'source' => 'non-db',
  'module' => 'AM_ProjectTemplates',
  'bean_name' => 'AM_ProjectTemplates',
  'vname' => 'LBL_AM_PROJECTTEMPLATES_MOU_MOU_1_FROM_AM_PROJECTTEMPLATES_TITLE',
  'id_name' => 'am_projecttemplates_mou_mou_1am_projecttemplates_ida',
);
$dictionary["MOU_MOU"]["fields"]["am_projecttemplates_mou_mou_1_name"] = array (
  'name' => 'am_projecttemplates_mou_mou_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AM_PROJECTTEMPLATES_MOU_MOU_1_FROM_AM_PROJECTTEMPLATES_TITLE',
  'save' => true,
  'id_name' => 'am_projecttemplates_mou_mou_1am_projecttemplates_ida',
  'link' => 'am_projecttemplates_mou_mou_1',
  'table' => 'am_projecttemplates',
  'module' => 'AM_ProjectTemplates',
  'rname' => 'name',
);
$dictionary["MOU_MOU"]["fields"]["am_projecttemplates_mou_mou_1am_projecttemplates_ida"] = array (
  'name' => 'am_projecttemplates_mou_mou_1am_projecttemplates_ida',
  'type' => 'link',
  'relationship' => 'am_projecttemplates_mou_mou_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AM_PROJECTTEMPLATES_MOU_MOU_1_FROM_MOU_MOU_TITLE',
);
