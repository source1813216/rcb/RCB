<?php
// created: 2021-07-12 17:45:01
$dictionary["Lead"]["fields"]["leads_mou_mou_1"] = array (
  'name' => 'leads_mou_mou_1',
  'type' => 'link',
  'relationship' => 'leads_mou_mou_1',
  'source' => 'non-db',
  'module' => 'MOU_MOU',
  'bean_name' => 'MOU_MOU',
  'side' => 'right',
  'vname' => 'LBL_LEADS_MOU_MOU_1_FROM_MOU_MOU_TITLE',
);
