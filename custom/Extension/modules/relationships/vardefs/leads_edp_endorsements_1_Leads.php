<?php
// created: 2021-06-15 16:46:24
$dictionary["Lead"]["fields"]["leads_edp_endorsements_1"] = array (
  'name' => 'leads_edp_endorsements_1',
  'type' => 'link',
  'relationship' => 'leads_edp_endorsements_1',
  'source' => 'non-db',
  'module' => 'EDP_Endorsements',
  'bean_name' => 'EDP_Endorsements',
  'side' => 'right',
  'vname' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_EDP_ENDORSEMENTS_TITLE',
);
