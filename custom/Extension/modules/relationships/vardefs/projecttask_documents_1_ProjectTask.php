<?php
// created: 2021-08-03 18:31:11
$dictionary["ProjectTask"]["fields"]["projecttask_documents_1"] = array (
  'name' => 'projecttask_documents_1',
  'type' => 'link',
  'relationship' => 'projecttask_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_PROJECTTASK_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
