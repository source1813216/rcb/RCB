<?php
// created: 2021-08-03 18:02:01
$dictionary["Project"]["fields"]["leads_project_1"] = array (
  'name' => 'leads_project_1',
  'type' => 'link',
  'relationship' => 'leads_project_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_PROJECT_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_project_1leads_ida',
);
$dictionary["Project"]["fields"]["leads_project_1_name"] = array (
  'name' => 'leads_project_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_PROJECT_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_project_1leads_ida',
  'link' => 'leads_project_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["Project"]["fields"]["leads_project_1leads_ida"] = array (
  'name' => 'leads_project_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_project_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_PROJECT_1_FROM_PROJECT_TITLE',
);
