<?php
// created: 2021-06-07 14:48:34
$dictionary["EAP_Event_Approval"]["fields"]["leads_eap_event_approval_1"] = array (
  'name' => 'leads_eap_event_approval_1',
  'type' => 'link',
  'relationship' => 'leads_eap_event_approval_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_EAP_EVENT_APPROVAL_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_eap_event_approval_1leads_ida',
);
$dictionary["EAP_Event_Approval"]["fields"]["leads_eap_event_approval_1_name"] = array (
  'name' => 'leads_eap_event_approval_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_EAP_EVENT_APPROVAL_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_eap_event_approval_1leads_ida',
  'link' => 'leads_eap_event_approval_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["EAP_Event_Approval"]["fields"]["leads_eap_event_approval_1leads_ida"] = array (
  'name' => 'leads_eap_event_approval_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_eap_event_approval_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_EAP_EVENT_APPROVAL_1_FROM_EAP_EVENT_APPROVAL_TITLE',
);
