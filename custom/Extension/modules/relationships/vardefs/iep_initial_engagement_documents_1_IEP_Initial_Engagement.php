<?php
// created: 2021-05-18 17:58:42
$dictionary["IEP_Initial_Engagement"]["fields"]["iep_initial_engagement_documents_1"] = array (
  'name' => 'iep_initial_engagement_documents_1',
  'type' => 'link',
  'relationship' => 'iep_initial_engagement_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_IEP_INITIAL_ENGAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
