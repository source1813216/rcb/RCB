<?php
// created: 2021-06-02 12:42:20
$dictionary["Note"]["fields"]["leads_notes_1"] = array (
  'name' => 'leads_notes_1',
  'type' => 'link',
  'relationship' => 'leads_notes_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_NOTES_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_notes_1leads_ida',
);
$dictionary["Note"]["fields"]["leads_notes_1_name"] = array (
  'name' => 'leads_notes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_NOTES_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_notes_1leads_ida',
  'link' => 'leads_notes_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["Note"]["fields"]["leads_notes_1leads_ida"] = array (
  'name' => 'leads_notes_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_notes_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_NOTES_1_FROM_NOTES_TITLE',
);
