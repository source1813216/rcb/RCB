<?php
// created: 2021-06-14 17:23:42
$dictionary["Document"]["fields"]["edp_event_documents_documents_1"] = array (
  'name' => 'edp_event_documents_documents_1',
  'type' => 'link',
  'relationship' => 'edp_event_documents_documents_1',
  'source' => 'non-db',
  'module' => 'EDP_Event_Documents',
  'bean_name' => 'EDP_Event_Documents',
  'vname' => 'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_EDP_EVENT_DOCUMENTS_TITLE',
  'id_name' => 'edp_event_documents_documents_1edp_event_documents_ida',
);
$dictionary["Document"]["fields"]["edp_event_documents_documents_1_name"] = array (
  'name' => 'edp_event_documents_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_EDP_EVENT_DOCUMENTS_TITLE',
  'save' => true,
  'id_name' => 'edp_event_documents_documents_1edp_event_documents_ida',
  'link' => 'edp_event_documents_documents_1',
  'table' => 'edp_event_documents',
  'module' => 'EDP_Event_Documents',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["edp_event_documents_documents_1edp_event_documents_ida"] = array (
  'name' => 'edp_event_documents_documents_1edp_event_documents_ida',
  'type' => 'link',
  'relationship' => 'edp_event_documents_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
