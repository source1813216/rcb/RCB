<?php
// created: 2021-07-12 17:37:00
$dictionary["Lead"]["fields"]["leads_jjwg_maps_1"] = array (
  'name' => 'leads_jjwg_maps_1',
  'type' => 'link',
  'relationship' => 'leads_jjwg_maps_1',
  'source' => 'non-db',
  'module' => 'jjwg_Maps',
  'bean_name' => 'jjwg_Maps',
  'side' => 'right',
  'vname' => 'LBL_LEADS_JJWG_MAPS_1_FROM_JJWG_MAPS_TITLE',
);
