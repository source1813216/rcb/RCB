<?php
// created: 2021-07-09 09:34:58
$dictionary["Task"]["fields"]["mou_mou_tasks_1"] = array (
  'name' => 'mou_mou_tasks_1',
  'type' => 'link',
  'relationship' => 'mou_mou_tasks_1',
  'source' => 'non-db',
  'module' => 'MOU_MOU',
  'bean_name' => 'MOU_MOU',
  'vname' => 'LBL_MOU_MOU_TASKS_1_FROM_MOU_MOU_TITLE',
  'id_name' => 'mou_mou_tasks_1mou_mou_ida',
);
$dictionary["Task"]["fields"]["mou_mou_tasks_1_name"] = array (
  'name' => 'mou_mou_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_MOU_MOU_TASKS_1_FROM_MOU_MOU_TITLE',
  'save' => true,
  'id_name' => 'mou_mou_tasks_1mou_mou_ida',
  'link' => 'mou_mou_tasks_1',
  'table' => 'mou_mou',
  'module' => 'MOU_MOU',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["mou_mou_tasks_1mou_mou_ida"] = array (
  'name' => 'mou_mou_tasks_1mou_mou_ida',
  'type' => 'link',
  'relationship' => 'mou_mou_tasks_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_MOU_MOU_TASKS_1_FROM_TASKS_TITLE',
);
