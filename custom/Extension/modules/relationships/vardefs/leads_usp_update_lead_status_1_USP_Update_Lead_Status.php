<?php
// created: 2021-08-17 16:27:33
$dictionary["USP_Update_Lead_Status"]["fields"]["leads_usp_update_lead_status_1"] = array (
  'name' => 'leads_usp_update_lead_status_1',
  'type' => 'link',
  'relationship' => 'leads_usp_update_lead_status_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_usp_update_lead_status_1leads_ida',
);
$dictionary["USP_Update_Lead_Status"]["fields"]["leads_usp_update_lead_status_1_name"] = array (
  'name' => 'leads_usp_update_lead_status_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_usp_update_lead_status_1leads_ida',
  'link' => 'leads_usp_update_lead_status_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["USP_Update_Lead_Status"]["fields"]["leads_usp_update_lead_status_1leads_ida"] = array (
  'name' => 'leads_usp_update_lead_status_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_usp_update_lead_status_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_USP_UPDATE_LEAD_STATUS_TITLE',
);
