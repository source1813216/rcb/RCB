<?php
// created: 2021-06-02 12:42:20
$dictionary["Lead"]["fields"]["leads_notes_1"] = array (
  'name' => 'leads_notes_1',
  'type' => 'link',
  'relationship' => 'leads_notes_1',
  'source' => 'non-db',
  'module' => 'Notes',
  'bean_name' => 'Note',
  'side' => 'right',
  'vname' => 'LBL_LEADS_NOTES_1_FROM_NOTES_TITLE',
);
