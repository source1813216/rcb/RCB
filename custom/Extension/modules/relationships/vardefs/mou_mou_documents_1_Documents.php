<?php
// created: 2021-07-09 09:34:34
$dictionary["Document"]["fields"]["mou_mou_documents_1"] = array (
  'name' => 'mou_mou_documents_1',
  'type' => 'link',
  'relationship' => 'mou_mou_documents_1',
  'source' => 'non-db',
  'module' => 'MOU_MOU',
  'bean_name' => 'MOU_MOU',
  'vname' => 'LBL_MOU_MOU_DOCUMENTS_1_FROM_MOU_MOU_TITLE',
  'id_name' => 'mou_mou_documents_1mou_mou_ida',
);
$dictionary["Document"]["fields"]["mou_mou_documents_1_name"] = array (
  'name' => 'mou_mou_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_MOU_MOU_DOCUMENTS_1_FROM_MOU_MOU_TITLE',
  'save' => true,
  'id_name' => 'mou_mou_documents_1mou_mou_ida',
  'link' => 'mou_mou_documents_1',
  'table' => 'mou_mou',
  'module' => 'MOU_MOU',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["mou_mou_documents_1mou_mou_ida"] = array (
  'name' => 'mou_mou_documents_1mou_mou_ida',
  'type' => 'link',
  'relationship' => 'mou_mou_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_MOU_MOU_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
