<?php
// created: 2021-06-15 16:46:24
$dictionary["EDP_Endorsements"]["fields"]["leads_edp_endorsements_1"] = array (
  'name' => 'leads_edp_endorsements_1',
  'type' => 'link',
  'relationship' => 'leads_edp_endorsements_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_edp_endorsements_1leads_ida',
);
$dictionary["EDP_Endorsements"]["fields"]["leads_edp_endorsements_1_name"] = array (
  'name' => 'leads_edp_endorsements_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_edp_endorsements_1leads_ida',
  'link' => 'leads_edp_endorsements_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["EDP_Endorsements"]["fields"]["leads_edp_endorsements_1leads_ida"] = array (
  'name' => 'leads_edp_endorsements_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_edp_endorsements_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_EDP_ENDORSEMENTS_TITLE',
);
