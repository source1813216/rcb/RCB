<?php
 // created: 2021-08-17 16:27:33
$layout_defs["Leads"]["subpanel_setup"]['leads_usp_update_lead_status_1'] = array (
  'order' => 100,
  'module' => 'USP_Update_Lead_Status',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_USP_UPDATE_LEAD_STATUS_1_FROM_USP_UPDATE_LEAD_STATUS_TITLE',
  'get_subpanel_data' => 'leads_usp_update_lead_status_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
