<?php
 // created: 2021-07-09 09:34:58
$layout_defs["MOU_MOU"]["subpanel_setup"]['mou_mou_tasks_1'] = array (
  'order' => 100,
  'module' => 'Tasks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_MOU_MOU_TASKS_1_FROM_TASKS_TITLE',
  'get_subpanel_data' => 'mou_mou_tasks_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
