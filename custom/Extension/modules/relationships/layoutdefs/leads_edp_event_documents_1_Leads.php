<?php
 // created: 2021-06-14 16:42:28
$layout_defs["Leads"]["subpanel_setup"]['leads_edp_event_documents_1'] = array (
  'order' => 100,
  'module' => 'EDP_Event_Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_EDP_EVENT_DOCUMENTS_1_FROM_EDP_EVENT_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'leads_edp_event_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
