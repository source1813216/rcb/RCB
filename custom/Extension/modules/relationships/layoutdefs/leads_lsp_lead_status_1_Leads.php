<?php
 // created: 2021-07-15 11:19:53
$layout_defs["Leads"]["subpanel_setup"]['leads_lsp_lead_status_1'] = array (
  'order' => 100,
  'module' => 'LSP_Lead_Status',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_LSP_LEAD_STATUS_1_FROM_LSP_LEAD_STATUS_TITLE',
  'get_subpanel_data' => 'leads_lsp_lead_status_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
