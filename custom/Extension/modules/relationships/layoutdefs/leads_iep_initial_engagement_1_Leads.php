<?php
 // created: 2021-05-18 07:14:04
$layout_defs["Leads"]["subpanel_setup"]['leads_iep_initial_engagement_1'] = array (
  'order' => 100,
  'module' => 'IEP_Initial_Engagement',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_IEP_INITIAL_ENGAGEMENT_1_FROM_IEP_INITIAL_ENGAGEMENT_TITLE',
  'get_subpanel_data' => 'leads_iep_initial_engagement_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
