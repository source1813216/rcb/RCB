<?php
 // created: 2021-07-12 17:37:00
$layout_defs["Leads"]["subpanel_setup"]['leads_jjwg_maps_1'] = array (
  'order' => 100,
  'module' => 'jjwg_Maps',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_LEADS_JJWG_MAPS_1_FROM_JJWG_MAPS_TITLE',
  'get_subpanel_data' => 'leads_jjwg_maps_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
