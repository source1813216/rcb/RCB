<?php
// created: 2021-06-23 10:34:25
$dictionary["EDP_Endorsements"]["fields"]["edp_endorsements_documents_1"] = array (
  'name' => 'edp_endorsements_documents_1',
  'type' => 'link',
  'relationship' => 'edp_endorsements_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_EDP_ENDORSEMENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
