<?php
// created: 2021-05-18 07:14:04
$dictionary["IEP_Initial_Engagement"]["fields"]["leads_iep_initial_engagement_1"] = array (
  'name' => 'leads_iep_initial_engagement_1',
  'type' => 'link',
  'relationship' => 'leads_iep_initial_engagement_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_IEP_INITIAL_ENGAGEMENT_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_iep_initial_engagement_1leads_ida',
);
$dictionary["IEP_Initial_Engagement"]["fields"]["leads_iep_initial_engagement_1_name"] = array (
  'name' => 'leads_iep_initial_engagement_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_IEP_INITIAL_ENGAGEMENT_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_iep_initial_engagement_1leads_ida',
  'link' => 'leads_iep_initial_engagement_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["IEP_Initial_Engagement"]["fields"]["leads_iep_initial_engagement_1leads_ida"] = array (
  'name' => 'leads_iep_initial_engagement_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_iep_initial_engagement_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_IEP_INITIAL_ENGAGEMENT_1_FROM_IEP_INITIAL_ENGAGEMENT_TITLE',
);
