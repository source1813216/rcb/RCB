<?php
// created: 2021-07-12 17:37:00
$dictionary["jjwg_Maps"]["fields"]["leads_jjwg_maps_1"] = array (
  'name' => 'leads_jjwg_maps_1',
  'type' => 'link',
  'relationship' => 'leads_jjwg_maps_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_JJWG_MAPS_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_jjwg_maps_1leads_ida',
);
$dictionary["jjwg_Maps"]["fields"]["leads_jjwg_maps_1_name"] = array (
  'name' => 'leads_jjwg_maps_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_JJWG_MAPS_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_jjwg_maps_1leads_ida',
  'link' => 'leads_jjwg_maps_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["jjwg_Maps"]["fields"]["leads_jjwg_maps_1leads_ida"] = array (
  'name' => 'leads_jjwg_maps_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_jjwg_maps_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_LEADS_JJWG_MAPS_1_FROM_JJWG_MAPS_TITLE',
);
