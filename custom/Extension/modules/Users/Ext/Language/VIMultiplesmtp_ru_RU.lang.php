<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_EMAILS_PER_RUN'] = 'Количество писем, отправленных за один пакет:';
$mod_strings['LBL_LOCATION_ONLY'] = 'Место нахождения';
$mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'] = 'Используйте только целочисленные значения, чтобы указать количество писем, отправленных за пакет';
$mod_strings['LBL_LIST_FROM_NAME'] = 'От имени';
$mod_strings['LBL_MAIL_SMTPSERVER'] = 'Почтовый сервер SMTP:';
$mod_strings['LBL_MAIL_SMTPPORT'] = 'Порт SMTP:';
$mod_strings['LBL_MAIL_SMTPUSER'] = 'Имя пользователя:';
$mod_strings['LBL_MAIL_SMTPPASS'] = 'Пароль:';
$mod_strings['LBL_EMAIL_OUTBOUND_CONFIGURATION'] = 'Конфигурация исходящей почты';
$mod_strings['LBL_CONFIGURATION_NOTE'] = 'Настройте сервер исходящей почты для отправки уведомлений по электронной почте, в том числе предупреждений рабочего процесса.';
$mod_strings['LBL_FROM_NAME'] = '"От имени:';
$mod_strings['LBL_FROM_ADDRESS'] = 'Адрес "От":';
$mod_strings['LBL_REQUIRED_SYMBOL'] = '*';
$mod_strings['LBL_NTC_REQUIRED'] = 'Обозначает обязательное поле';
$mod_strings['LBL_MAIL_SENDTYPE'] = 'Агент пересылки почты:';
$mod_strings['LBL_CHOOSE_EMAIL_PROVIDER'] = 'Выберите провайдера электронной почты';
$mod_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';
$mod_strings['LBL_SMTPTYPE_YAHOO'] = 'Почта Яху';
$mod_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';
$mod_strings['LBL_SMTPTYPE_OTHER'] = 'Другой';
$mod_strings['LBL_MAIL_SMTPAUTH_REQ'] =  'Использовать аутентификацию SMTP:';
$mod_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'Включить SMTP через SSL или TLS?';
$mod_strings['LBL_CHANGE_PASSWORD'] = 'Изменить пароль';
$mod_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Отправить тестовое письмо';
$mod_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'Адрес электронной почты для тестового уведомления';
$mod_strings['LBL_EMAIL_SEND'] = 'послать';
$mod_strings['LBL_CANCEL_BUTTON_LABEL'] = 'отменить';









?>