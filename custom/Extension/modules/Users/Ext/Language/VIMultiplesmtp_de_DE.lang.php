<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_EMAILS_PER_RUN'] = 'Anzahl der E-Mails, die pro Stapel gesendet werden:';
$mod_strings['LBL_LOCATION_ONLY'] = 'Ort';
$mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'] = 'Verwenden Sie nur ganzzahlige Werte, um die Anzahl der pro Stapel gesendeten E-Mails anzugeben';
$mod_strings['LBL_LIST_FROM_NAME'] = 'Von Namen';
$mod_strings['LBL_MAIL_SMTPSERVER'] = 'SMTP Mail Server:';
$mod_strings['LBL_MAIL_SMTPPORT'] = 'SMTP-Port:';
$mod_strings['LBL_MAIL_SMTPUSER'] = 'Nutzername::';
$mod_strings['LBL_MAIL_SMTPPASS'] = 'Passwort::';
$mod_strings['LBL_EMAIL_OUTBOUND_CONFIGURATION'] = 'Konfiguration für ausgehende E-Mails';
$mod_strings['LBL_CONFIGURATION_NOTE'] = 'Konfigurieren Sie den Postausgangsserver für das Senden von E-Mail-Benachrichtigungen, einschließlich Workflow-Benachrichtigungen.';
$mod_strings['LBL_FROM_NAME'] = '"Von Namen:';
$mod_strings['LBL_FROM_ADDRESS'] = '"Von der Adresse:';
$mod_strings['LBL_REQUIRED_SYMBOL'] = '*';
$mod_strings['LBL_NTC_REQUIRED'] = 'Kennzeichnet das erforderliche Feld';
$mod_strings['LBL_MAIL_SENDTYPE'] = 'Mail Transfer Agent:';
$mod_strings['LBL_CHOOSE_EMAIL_PROVIDER'] = 'Wählen Sie Ihren E-Mail-Anbieter';
$mod_strings['LBL_SMTPTYPE_GMAIL'] = 'Google Mail';
$mod_strings['LBL_SMTPTYPE_YAHOO'] = 'Yahoo Mail';
$mod_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';
$mod_strings['LBL_SMTPTYPE_OTHER'] = 'Andere';
$mod_strings['LBL_MAIL_SMTPAUTH_REQ'] =  'SMTP-Authentifizierung verwenden:';
$mod_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'SMTP über SSL oder TLS aktivieren?';
$mod_strings['LBL_CHANGE_PASSWORD'] = 'Ändere das Passwort';
$mod_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Test-E-Mail senden';
$mod_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'E-Mail-Adresse für Testbenachrichtigung';
$mod_strings['LBL_EMAIL_SEND'] = 'Senden';
$mod_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Stornieren';
?>