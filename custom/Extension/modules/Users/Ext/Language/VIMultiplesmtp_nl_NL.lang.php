<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_EMAILS_PER_RUN'] = 'Aantal verzonden e-mails per batch:';
$mod_strings['LBL_LOCATION_ONLY'] = 'Plaats';
$mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'] = 'Gebruik alleen gehele getallen om het aantal e-mails te specificeren dat per batch wordt verzonden';
$mod_strings['LBL_LIST_FROM_NAME'] = 'Van naam';
$mod_strings['LBL_MAIL_SMTPSERVER'] = 'SMTP Mail Server:';
$mod_strings['LBL_MAIL_SMTPPORT'] = 'SMTP-poort:';
$mod_strings['LBL_MAIL_SMTPUSER'] = 'Gebruikersnaam:';
$mod_strings['LBL_MAIL_SMTPPASS'] = 'Wachtwoord:';
$mod_strings['LBL_EMAIL_OUTBOUND_CONFIGURATION'] = 'Configuratie uitgaande e-mail';
$mod_strings['LBL_CONFIGURATION_NOTE'] = 'Configureer de uitgaande mailserver voor het verzenden van e-mailmeldingen, inclusief werkstroomwaarschuwingen.';
$mod_strings['LBL_FROM_NAME'] = '"Van naam:';
$mod_strings['LBL_FROM_ADDRESS'] = '"Van" adres:';
$mod_strings['LBL_REQUIRED_SYMBOL'] = '*';
$mod_strings['LBL_NTC_REQUIRED'] = 'Geeft verplicht veld aan';
$mod_strings['LBL_MAIL_SENDTYPE'] = 'Mail Transfer Agent:';
$mod_strings['LBL_CHOOSE_EMAIL_PROVIDER'] = 'Kies uw e-mailprovider';
$mod_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';
$mod_strings['LBL_SMTPTYPE_YAHOO'] = 'Yahoo Mail';
$mod_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';
$mod_strings['LBL_SMTPTYPE_OTHER'] = 'anders';
$mod_strings['LBL_MAIL_SMTPAUTH_REQ'] =  'Gebruik SMTP-authenticatie:';
$mod_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'Schakel SMTP in via SSL of TLS?';
$mod_strings['LBL_CHANGE_PASSWORD'] = 'Wachtwoord wijzigen';
$mod_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Verzend test-e-mail';
$mod_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'E-mailadres voor testmelding';
$mod_strings['LBL_EMAIL_SEND'] = 'Sturen';
$mod_strings['LBL_CANCEL_BUTTON_LABEL'] = 'annuleren';









?>