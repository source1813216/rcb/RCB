<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_EMAILS_PER_RUN'] = 'Número de emails enviados por lote:';
$mod_strings['LBL_LOCATION_ONLY'] = 'Localização';
$mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'] = 'Use apenas valores inteiros para especificar o número de emails enviados por lote';
$mod_strings['LBL_LIST_FROM_NAME'] = 'De nome';
$mod_strings['LBL_MAIL_SMTPSERVER'] = 'Servidor de correio SMTP:';
$mod_strings['LBL_MAIL_SMTPPORT'] = 'Porta SMTP:';
$mod_strings['LBL_MAIL_SMTPUSER'] = 'Nome de usuário:';
$mod_strings['LBL_MAIL_SMTPPASS'] = 'Senha:';
$mod_strings['LBL_EMAIL_OUTBOUND_CONFIGURATION'] = 'Configuração de correio de saída';
$mod_strings['LBL_CONFIGURATION_NOTE'] = 'Configure o servidor de correio de saída para enviar notificações por email, incluindo alertas de fluxo de trabalho.';
$mod_strings['LBL_FROM_NAME'] = '"De nome:';
$mod_strings['LBL_FROM_ADDRESS'] = '"A partir do endereço:';
$mod_strings['LBL_REQUIRED_SYMBOL'] = '*';
$mod_strings['LBL_NTC_REQUIRED'] = 'Indica campo obrigatório';
$mod_strings['LBL_MAIL_SENDTYPE'] = 'Agente de Transferência de Correio:';
$mod_strings['LBL_CHOOSE_EMAIL_PROVIDER'] = 'Escolha seu provedor de email';
$mod_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';
$mod_strings['LBL_SMTPTYPE_YAHOO'] = 'E-mail do Yahoo';
$mod_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';
$mod_strings['LBL_SMTPTYPE_OTHER'] = 'De outros';
$mod_strings['LBL_MAIL_SMTPAUTH_REQ'] =  'Use a autenticação SMTP:';
$mod_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'Ativar SMTP sobre SSL ou TLS?';
$mod_strings['LBL_CHANGE_PASSWORD'] = 'Mudar senha';
$mod_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Enviar email de teste';
$mod_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'Endereço de e-mail para notificação de teste';
$mod_strings['LBL_EMAIL_SEND'] = 'Mandar';
$mod_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Cancelar';









?>