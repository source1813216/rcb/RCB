<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_EMAILS_PER_RUN'] = 'Número de correos electrónicos enviados por lote:';
$mod_strings['LBL_LOCATION_ONLY'] = 'Ubicación';
$mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'] = 'Use solo valores enteros para especificar el número de correos electrónicos enviados por lote';
$mod_strings['LBL_LIST_FROM_NAME'] = 'De nombre';
$mod_strings['LBL_MAIL_SMTPSERVER'] = 'Servidor de correo SMTP:';
$mod_strings['LBL_MAIL_SMTPPORT'] = 'Puerto SMTP:';
$mod_strings['LBL_MAIL_SMTPUSER'] = 'Nombre de usuario:';
$mod_strings['LBL_MAIL_SMTPPASS'] = 'Contraseña:';
$mod_strings['LBL_EMAIL_OUTBOUND_CONFIGURATION'] = 'Configuración de correo saliente';
$mod_strings['LBL_CONFIGURATION_NOTE'] = 'Configure el servidor de correo saliente para enviar notificaciones por correo electrónico, incluidas las alertas de flujo de trabajo.';
$mod_strings['LBL_FROM_NAME'] = '"De" Nombre:';
$mod_strings['LBL_FROM_ADDRESS'] = '"De la Dirección:';
$mod_strings['LBL_REQUIRED_SYMBOL'] = '*';
$mod_strings['LBL_NTC_REQUIRED'] = 'Indica campo requerido';
$mod_strings['LBL_MAIL_SENDTYPE'] = 'Agente de transferencia de correo:';
$mod_strings['LBL_CHOOSE_EMAIL_PROVIDER'] = 'Elija su proveedor de correo electrónico';
$mod_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';
$mod_strings['LBL_SMTPTYPE_YAHOO'] = 'Correo de yahoo';
$mod_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';
$mod_strings['LBL_SMTPTYPE_OTHER'] = 'Otro';
$mod_strings['LBL_MAIL_SMTPAUTH_REQ'] =  'Utilice la autenticación SMTP:';
$mod_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = '¿Habilitar SMTP sobre SSL o TLS?';
$mod_strings['LBL_CHANGE_PASSWORD'] = 'Cambia la contraseña';
$mod_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Enviar correo electrónico de prueba';
$mod_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'Dirección de correo electrónico para la notificación de prueba';
$mod_strings['LBL_EMAIL_SEND'] = 'Enviar';
$mod_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Cancelar';









?>