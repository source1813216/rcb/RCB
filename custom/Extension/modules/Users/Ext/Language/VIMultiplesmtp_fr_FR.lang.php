<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_EMAILS_PER_RUN'] = "Nombre d'emails envoyés par lot:";
$mod_strings['LBL_LOCATION_ONLY'] = 'Emplacement';
$mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'] = 'Utilisez uniquement des valeurs entières pour spécifier le nombre de courriels envoyés par lot';
$mod_strings['LBL_LIST_FROM_NAME'] = 'De nom';
$mod_strings['LBL_MAIL_SMTPSERVER'] = 'Serveur mail SMTP:';
$mod_strings['LBL_MAIL_SMTPPORT'] = 'Port SMTP:';
$mod_strings['LBL_MAIL_SMTPUSER'] = "Nom d'utilisateur:";
$mod_strings['LBL_MAIL_SMTPPASS'] = 'Mot de passe:';
$mod_strings['LBL_EMAIL_OUTBOUND_CONFIGURATION'] = 'Configuration du courrier sortant';
$mod_strings['LBL_CONFIGURATION_NOTE'] = "Configurez le serveur de courrier sortant pour l'envoi de notifications par courrier électronique, y compris les alertes de flux de travail.";
$mod_strings['LBL_FROM_NAME'] = '"De" Nom:';
$mod_strings['LBL_FROM_ADDRESS'] = '"De l"adresse:';
$mod_strings['LBL_REQUIRED_SYMBOL'] = '*';
$mod_strings['LBL_NTC_REQUIRED'] = 'Indique le champ requis';
$mod_strings['LBL_MAIL_SENDTYPE'] = 'Agent de transfert de courrier:';
$mod_strings['LBL_CHOOSE_EMAIL_PROVIDER'] = 'Choisissez votre fournisseur de messagerie';
$mod_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';
$mod_strings['LBL_SMTPTYPE_YAHOO'] = 'Yahoo Mail';
$mod_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';
$mod_strings['LBL_SMTPTYPE_OTHER'] = 'Autre';
$mod_strings['LBL_MAIL_SMTPAUTH_REQ'] =  "Utiliser l'authentification SMTP:";
$mod_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'Activer SMTP sur SSL ou TLS?';
$mod_strings['LBL_CHANGE_PASSWORD'] = 'Changer le mot de passe';
$mod_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Envoyer un e-mail de test';
$mod_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'Adresse électronique pour la notification de test';
$mod_strings['LBL_EMAIL_SEND'] = 'Envoyer';
$mod_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Annuler';









?>