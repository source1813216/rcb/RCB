<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_EMAILS_PER_RUN'] = 'Кількість повідомлень, надісланих на партію:';
$mod_strings['LBL_LOCATION_ONLY'] = 'Розташування';
$mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'] = 'Використовуйте лише цілі значення, щоб вказати кількість електронних листів, надісланих на пакет';
$mod_strings['LBL_LIST_FROM_NAME'] = 'Від імені';
$mod_strings['LBL_MAIL_SMTPSERVER'] = 'Сервер пошти SMTP:';
$mod_strings['LBL_MAIL_SMTPPORT'] = 'Порт SMTP:';
$mod_strings['LBL_MAIL_SMTPUSER'] = "Ім'я користувача:";
$mod_strings['LBL_MAIL_SMTPPASS'] = 'Пароль::';
$mod_strings['LBL_EMAIL_OUTBOUND_CONFIGURATION'] = 'Конфігурація вихідної пошти';
$mod_strings['LBL_CONFIGURATION_NOTE'] = 'Налаштуйте сервер вихідної пошти для надсилання сповіщень електронною поштою, включаючи сповіщення про робочий процес.';
$mod_strings['LBL_FROM_NAME'] = 'Назва "Від":';
$mod_strings['LBL_FROM_ADDRESS'] = 'Адреса "Від":';
$mod_strings['LBL_REQUIRED_SYMBOL'] = '*';
$mod_strings['LBL_NTC_REQUIRED'] = "Вказує обов'язкові для заповнення поля";
$mod_strings['LBL_MAIL_SENDTYPE'] = 'Агент передачі пошти:';
$mod_strings['LBL_CHOOSE_EMAIL_PROVIDER'] = 'Виберіть постачальника послуг електронної пошти';
$mod_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';
$mod_strings['LBL_SMTPTYPE_YAHOO'] = 'Yahoo! Mail';
$mod_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';
$mod_strings['LBL_SMTPTYPE_OTHER'] = '
Інший';
$mod_strings['LBL_MAIL_SMTPAUTH_REQ'] =  'Використовувати автентифікацію SMTP:';
$mod_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'Увімкнути SMTP через SSL або TLS?';
$mod_strings['LBL_CHANGE_PASSWORD'] = 'Змінити пароль';
$mod_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Надіслати тестовий електронний лист';
$mod_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'Адреса електронної пошти для повідомлення про випробування';
$mod_strings['LBL_EMAIL_SEND'] = 'Надіслати';
$mod_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Скасувати';









?>