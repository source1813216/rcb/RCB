<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_EMAILS_PER_RUN'] = 'Numero di email inviate per batch:';
$mod_strings['LBL_LOCATION_ONLY'] = 'Posizione';
$mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'] = 'Utilizzare solo valori interi per specificare il numero di e-mail inviate per batch';
$mod_strings['LBL_LIST_FROM_NAME'] = 'Dal nome';
$mod_strings['LBL_MAIL_SMTPSERVER'] = 'Server di posta SMTP:';
$mod_strings['LBL_MAIL_SMTPPORT'] = 'Porta SMTP:';
$mod_strings['LBL_MAIL_SMTPUSER'] = 'Nome utente:';
$mod_strings['LBL_MAIL_SMTPPASS'] = "Parola dordine:";
$mod_strings['LBL_EMAIL_OUTBOUND_CONFIGURATION'] = 'Configurazione della posta in uscita';
$mod_strings['LBL_CONFIGURATION_NOTE'] = "Configurare il server di posta in uscita per l'invio di notifiche e-mail, inclusi gli avvisi del flusso di lavoro.";
$mod_strings['LBL_FROM_NAME'] = '"Dal nome:';
$mod_strings['LBL_FROM_ADDRESS'] = '"Da" Indirizzo:';
$mod_strings['LBL_REQUIRED_SYMBOL'] = '*';
$mod_strings['LBL_NTC_REQUIRED'] = 'Indica il campo richiesto';
$mod_strings['LBL_MAIL_SENDTYPE'] = 'Agente di trasferimento della posta:';
$mod_strings['LBL_CHOOSE_EMAIL_PROVIDER'] = 'Scegli il tuo provider di posta elettronica';
$mod_strings['LBL_SMTPTYPE_GMAIL'] = 'Gmail';
$mod_strings['LBL_SMTPTYPE_YAHOO'] = 'Yahoo Mail';
$mod_strings['LBL_SMTPTYPE_EXCHANGE'] = 'Microsoft Exchange';
$mod_strings['LBL_SMTPTYPE_OTHER'] = 'Altro';
$mod_strings['LBL_MAIL_SMTPAUTH_REQ'] =  "Usa l'autenticazione SMTP:";
$mod_strings['LBL_EMAIL_SMTP_SSL_OR_TLS'] = 'Abilita SMTP su SSL o TLS?';
$mod_strings['LBL_CHANGE_PASSWORD'] = 'Cambia la password';
$mod_strings['LBL_EMAIL_TEST_OUTBOUND_SETTINGS'] = 'Invia e-mail di prova';
$mod_strings['LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR'] = 'Indirizzo e-mail per la notifica di prova';
$mod_strings['LBL_EMAIL_SEND'] = 'Inviare';
$mod_strings['LBL_CANCEL_BUTTON_LABEL'] = 'Annulla';









?>