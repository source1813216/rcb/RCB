<?php
// created: 2021-06-14 17:23:42
$dictionary["EDP_Event_Documents"]["fields"]["edp_event_documents_documents_1"] = array (
  'name' => 'edp_event_documents_documents_1',
  'type' => 'link',
  'relationship' => 'edp_event_documents_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
