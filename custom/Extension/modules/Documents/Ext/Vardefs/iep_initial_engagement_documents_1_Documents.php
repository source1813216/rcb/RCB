<?php
// created: 2021-05-18 17:58:42
$dictionary["Document"]["fields"]["iep_initial_engagement_documents_1"] = array (
  'name' => 'iep_initial_engagement_documents_1',
  'type' => 'link',
  'relationship' => 'iep_initial_engagement_documents_1',
  'source' => 'non-db',
  'module' => 'IEP_Initial_Engagement',
  'bean_name' => 'IEP_Initial_Engagement',
  'vname' => 'LBL_IEP_INITIAL_ENGAGEMENT_DOCUMENTS_1_FROM_IEP_INITIAL_ENGAGEMENT_TITLE',
  'id_name' => 'iep_initial_engagement_documents_1iep_initial_engagement_ida',
);
$dictionary["Document"]["fields"]["iep_initial_engagement_documents_1_name"] = array (
  'name' => 'iep_initial_engagement_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_IEP_INITIAL_ENGAGEMENT_DOCUMENTS_1_FROM_IEP_INITIAL_ENGAGEMENT_TITLE',
  'save' => true,
  'id_name' => 'iep_initial_engagement_documents_1iep_initial_engagement_ida',
  'link' => 'iep_initial_engagement_documents_1',
  'table' => 'iep_initial_engagement',
  'module' => 'IEP_Initial_Engagement',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["iep_initial_engagement_documents_1iep_initial_engagement_ida"] = array (
  'name' => 'iep_initial_engagement_documents_1iep_initial_engagement_ida',
  'type' => 'link',
  'relationship' => 'iep_initial_engagement_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_IEP_INITIAL_ENGAGEMENT_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
