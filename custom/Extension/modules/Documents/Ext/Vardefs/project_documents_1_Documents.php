<?php
// created: 2021-08-03 18:30:24
$dictionary["Document"]["fields"]["project_documents_1"] = array (
  'name' => 'project_documents_1',
  'type' => 'link',
  'relationship' => 'project_documents_1',
  'source' => 'non-db',
  'module' => 'Project',
  'bean_name' => 'Project',
  'vname' => 'LBL_PROJECT_DOCUMENTS_1_FROM_PROJECT_TITLE',
);
