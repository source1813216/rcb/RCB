<?php
// created: 2021-08-03 18:31:11
$dictionary["Document"]["fields"]["projecttask_documents_1"] = array (
  'name' => 'projecttask_documents_1',
  'type' => 'link',
  'relationship' => 'projecttask_documents_1',
  'source' => 'non-db',
  'module' => 'ProjectTask',
  'bean_name' => 'ProjectTask',
  'vname' => 'LBL_PROJECTTASK_DOCUMENTS_1_FROM_PROJECTTASK_TITLE',
);
