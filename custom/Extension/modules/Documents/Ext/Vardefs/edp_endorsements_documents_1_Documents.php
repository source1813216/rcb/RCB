<?php
// created: 2021-06-23 10:34:25
$dictionary["Document"]["fields"]["edp_endorsements_documents_1"] = array (
  'name' => 'edp_endorsements_documents_1',
  'type' => 'link',
  'relationship' => 'edp_endorsements_documents_1',
  'source' => 'non-db',
  'module' => 'EDP_Endorsements',
  'bean_name' => 'EDP_Endorsements',
  'vname' => 'LBL_EDP_ENDORSEMENTS_DOCUMENTS_1_FROM_EDP_ENDORSEMENTS_TITLE',
  'id_name' => 'edp_endorsements_documents_1edp_endorsements_ida',
);
$dictionary["Document"]["fields"]["edp_endorsements_documents_1_name"] = array (
  'name' => 'edp_endorsements_documents_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_EDP_ENDORSEMENTS_DOCUMENTS_1_FROM_EDP_ENDORSEMENTS_TITLE',
  'save' => true,
  'id_name' => 'edp_endorsements_documents_1edp_endorsements_ida',
  'link' => 'edp_endorsements_documents_1',
  'table' => 'edp_endorsements',
  'module' => 'EDP_Endorsements',
  'rname' => 'name',
);
$dictionary["Document"]["fields"]["edp_endorsements_documents_1edp_endorsements_ida"] = array (
  'name' => 'edp_endorsements_documents_1edp_endorsements_ida',
  'type' => 'link',
  'relationship' => 'edp_endorsements_documents_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_EDP_ENDORSEMENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
