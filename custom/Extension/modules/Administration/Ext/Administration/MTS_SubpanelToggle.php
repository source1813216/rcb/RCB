<?php


$admin_option_defs = array();
$admin_option_defs['Administration']['MTS_SubpanelToggle'] = array('Administration', 'LBL_MTS_SUBPANEL_TOGGLE_CONFIG', 'LBL_MTS_SUBPANEL_TOGGLE_CONFIG_DESC', './index.php?module=MTS_SubpanelToggle&action=index');
$admin_option_defs['Administration']['MTS_SubpanelToggleLicense'] = array('Administration', 'LBL_MTS_SUBPANEL_TOGGLE_CONFIG_LICENSE', 'LBL_MTS_SUBPANEL_TOGGLE_CONFIG_LICENSE_DESC', './index.php?module=MTS_SubpanelToggle&action=license');

$admin_group_header[] = array('LBL_MTS_SUBPANEL_TOGGLE_CONFIG', '', false, $admin_option_defs, 'LBL_MTS_SUBPANEL_TOGGLE_CONFIG_TITLE');