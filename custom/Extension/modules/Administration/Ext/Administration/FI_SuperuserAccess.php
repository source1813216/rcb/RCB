<?php 
$admin_option_defs = array();

$admin_option_defs['FI_SuperuserAccess_info']= array('icon_AdminFI_SuperuserAccess','LBL_FI_SUPERUSERACCESSLICENSEADDON_LICENSE_TITLE','LBL_FI_SUPERUSERACCESSLICENSEADDON','./index.php?module=SuperuserAccess&action=license');
$admin_option_defs['FI_SuperuserAccess_Key'] = array('icon_FI_SuperuserAccess', 'Restrict access admin', 'Restrict access other admin users', './index.php?module=SuperuserAccess&action=fi_restrictaccess');

// Loop through the menus and add to the Users group
$tmp_menu_set = false;
foreach ($admin_group_header as $key => $values)
{
    if ($values[1] == 'FibreCRM Modules')
    {
    	$admin_group_header[$key][3]['Administration']['FI_SuperuserAccess_info'] = $admin_option_defs['FI_SuperuserAccess_info'];
        $admin_group_header[$key][3]['Administration']['FI_SuperuserAccess_Key'] = $admin_option_defs['FI_SuperuserAccess_Key'];
        $tmp_menu_set = true;
    }
}

// Else create new group
if (!$tmp_menu_set)
    $admin_group_header[] = array('<img src="custom/themes/default/images/fi_socialicon_fi_superuseraccess.jpg"> &nbsp; FibreCRM Modules','FibreCRM Modules',false,array('Administration' => $admin_option_defs),'');


?>
