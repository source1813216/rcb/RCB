<?php

/**
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Original Author Biztech Co.
 */
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
global $sugar_config;
$admin_option_defs = array();
//Licence Configuration Section
$admin_option_defs['Administration']['field_access_control_licence'] = array(
    '',
    //Title Of The Link 
    $mod_strings['LBL_FIELD_ACCESS_CONTROL_LICENCE'],
    //Description For The Link
    $mod_strings['LBL_FIELD_ACCESS_CONTROL_LINK_DESC'],
    //Where To Send The User When The Link Is Clicked
    './index.php?module=Administration&action=field_access_control_licence_config'
);
//Field Level Access Control Setting Section
$admin_option_defs['Administration']['field_access_control_view'] = array(
    '',
    //Title Of The Link 
    'LBL_APPLY_FIELD_ACCESS_CONTROL',
    //Description For The Link
    'LBL_APPLY_FIELD_ACCESS_CONTROL_DESC',
    //Where To Send The User When The Link Is Clicked
    'index.php?module=Administration&action=field_access_control_view'
);
//Main Section Header And Description
$admin_group_header[] = array(
    'LBL_APPLY_FIELD_ACCESS_CONTROL_TITLE',
    '',
    false,
    $admin_option_defs,
    'LBL_APPLY_FIELD_ACCESS_CONTROL_HEADER_DESC'
);
