<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('modules/VIMultiSMTPLicenseAddon/license/VIMultipleSMTPOutfittersLicense.php');
require_once('include/MVC/Controller/SugarController.php');
global $sugar_config;
$dynamicURL = $sugar_config['site_url'];
$url = $dynamicURL."/index.php?module=VIMultiSMTPLicenseAddon&action=license";
$sqlLicenseCheck = "SELECT * from config where name = 'lic_vimultiple-smtp'";
$result = $GLOBALS['db']->query($sqlLicenseCheck);
$resultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($sqlLicenseCheck));
if(!empty($resultData)){
    $validate_license = VIMultipleSMTPOutfittersLicense::isValid('VIMultiSMTPLicenseAddon');
    if($validate_license !== true) {
        if(is_admin($current_user)) {
            SugarApplication::appendErrorMessage('MultiSMTPLicenseAddon is no longer active due to the following reason: '.$validate_license.' Users will have limited to no access until the issue has been addressed <a href='.$url.'>Click Here</a>');
        }
            echo '<h2><p class="error">MultiSMTPLicenseAddon is no longer active. Please renew your subscription or check your license configuration.</p></h2><a href='.$url.'>Click Here</a>';
    }else{
         foreach ($admin_group_header as $key => $value) {
                $values[] = $value[0];
            }
            if (in_array("Other", $values))
            {
                $array['email-settings'] = array('email-settings','Multiple SMTP','Manage Multiple SMTP Configuration','./index.php?module=Administration&action=enablemultismtp','email-settings');
                $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
            }
            else
            {
                $admin_option_defs = array();
                $admin_option_defs['Administration']['email-settings'] = array(
                    //Icon name. Available icons are located in ./themes/default/images
                    'Administration',
                    
                    //Link name label 
                    'Multiple SMTP',
                    
                    //Link description label
                    'Manage Multiple SMTP Configuration',
                    
                    //Link URL
                    './index.php?module=Administration&action=enablemultismtp',
                    'email-settings',
                );

                $admin_group_header['Other'] = array(
                    //Section header label
                    'Other',
                    
                    //$other_text parameter for get_form_header()
                    '',
                    
                    //$show_help parameter for get_form_header()
                    false,
                    
                    //Section links
                    $admin_option_defs, 
                    
                    //Section description label
                    ''
                );
            }
        }
}else{
    foreach ($admin_group_header as $key => $value) {
        $values[] = $value[0];
    }
    if (in_array("Other", $values))
    {
        $array['email-settings'] = array('email-settings','Multiple SMTP','Manage Multiple SMTP Configuration','./index.php?module=VIMultiSMTPLicenseAddon&action=license','email-settings');
        $admin_group_header['Other'][3]['Administration'] = array_merge($admin_group_header['Other'][3]['Administration'],$array);
    }
    else
    {
        $admin_option_defs['Administration']['email-settings'] = array(
            //Icon name. Available icons are located in ./themes/default/images
            'Administration',
            
            //Link name label 
            'Multiple SMTP',
            
            //Link description label
            'Manage Multiple SMTP Configuration',
            
            //Link URL
            './index.php?module=VIMultiSMTPLicenseAddon&action=license',
            'email-settings',
        );

        $admin_group_header['Other'] = array(
            //Section header label
            'Other',
            
            //$other_text parameter for get_form_header()
            '',
            
            //$show_help parameter for get_form_header()
            false,
            
            //Section links
            $admin_option_defs, 
            
            //Section description label
            ''
        );
    }
}
?>