<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Voorwaardelijke meldingen';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Beheer voorwaardelijke kennisgeving voor modules';
$mod_strings['LBL_ADD_NEW'] = '+ Voeg nieuw toe';
$mod_strings['LBL_SELECTED_MODULE'] = 'module';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'Omschrijving';
$mod_strings['LBL_DELETE'] = 'Verwijder';
$mod_strings['LBL_CREATE_MESSAGE'] = 'U hebt momenteel geen records opgeslagen.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'een nu.';
$mod_strings['LBL_SELECT_MODULE'] = 'Selecteer module';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Voorwaarden toevoegen';
$mod_strings['LBL_ADD_TASKS'] = 'Taken toevoegen';
$mod_strings['LBL_CANCEL'] = 'annuleren';
$mod_strings['LBL_BACK'] = 'Terug';
$mod_strings['LBL_NEXT'] = 'volgende';
$mod_strings['LBL_SAVE'] = 'OPSLAAN';
$mod_strings['LBL_CLEAR'] = 'Duidelijk';
$mod_strings['LBL_MODULE_PATH'] = 'Geselecteerde module';
$mod_strings['LBL_FIELD'] = 'Veld';
$mod_strings['LBL_OPERATOR'] = 'operator';
$mod_strings['LBL_VALUE_TYPE'] = 'Waarde type';
$mod_strings['LBL_VALUE'] = 'Waarde';
$mod_strings['LBL_ACTION_TITLE'] = 'Actietitel';
$mod_strings['LBL_NOTIFICATION_EDITING'] = 'Melding tijdens het bewerken / creëren';
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = 'Melding Wanneer het record is bekeken';
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = 'Melding bij opslaan';
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = 'Mag record niet opslaan';
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = 'Melding Wanneer record Duplicaat is';
$mod_strings['LBL_INSERT'] = 'invoegen';
$mod_strings['LBL_INSERT_FIELDS'] = 'Velden invoegen';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'In deze stappen is het toegestaan om een module te selecteren waarop we een voorwaardelijke melding moeten instellen';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = 'Stel de conditie in op het veld van de module om de melding weer te geven';
$mod_strings['LBL_ADD_TASKS_NOTES'] = 'Stel verschillende acties in wanneer aan de voorwaarde bij stap # 2 is voldaan';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Voorwaardelijke operator';
$mod_strings['LBL_NOTE_LABEL'] = 'Notitie:';
$mod_strings['LBL_NOTE'] = 'Hieronder ziet u het voorbeeld van reguliere expressie bij het selecteren van de operator als reguliere expressie. Voeg geen schuine streep (/) toe vóór en na een reguliere expressie.'; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Alleen alfawaarden: = ([A-z], [a-z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Alleen numerieke waarden: = ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'Voor AlphaNumeric-waarden: = (ex. ^ [A-zA-Z0-9 _] * $).';
$mod_strings['LBL_DATE_VALUES'] = 'Voor datumwaarden: = (^ ([0-9] {1,2}) \\ / ([0-9] {1,2}) \\ / ([0-9] {4}) $)';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'Voor DateTime: = (^ ([0-9] {2,4}) - ([0-1] [0-9]) - ([0-3] [0-9]) (? [0-2 ] [0-9]): ([0-5] [0-9])? ([0-5] [0-9])) $)';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'Allemaal';
$mod_strings['LBL_CREATE'] = 'CREATE';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Selecteer records.";
$mod_strings['LBL_DELETE_MESSAGE'] = "Weet je zeker dat je wilt verwijderen";
$mod_strings['LBL_DELETE_THESE'] = 'deze';
$mod_strings['LBL_DELETE_THIS'] = 'deze';
$mod_strings['LBL_DELETE_ROW'] = 'rij?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Kies een optie';
$mod_strings['LBL_AND_OPERATOR'] = 'EN';
$mod_strings['LBL_OR_OPERATOR'] = 'OF';
$mod_strings['LBL_VALIDATION_MESSAGE'] = 'Selecteer een verplicht veld.';
$mod_strings['LBL_PLZ_ENTER_VALUE'] = 'Voer waarde in!';
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'meldingen';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Melding met bevestigingsdialoogvenster';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Licentie updaten';
?>



