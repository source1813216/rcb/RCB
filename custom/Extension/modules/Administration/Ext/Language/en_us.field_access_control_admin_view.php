<?php

/**
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Original Author Biztech Co.
 */
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

//Field ACL Section
$mod_strings['LBL_APPLY_FIELD_ACCESS_CONTROL'] = 'Field Level Access Control Configurations';
$mod_strings['LBL_APPLY_FIELD_ACCESS_CONTROL_DESC'] = 'Set user wise access on fields of different modules.';
//Main Section Of Field ACL
$mod_strings['LBL_APPLY_FIELD_ACCESS_CONTROL_TITLE'] = 'Field Level Access Control';
$mod_strings['LBL_APPLY_FIELD_ACCESS_CONTROL_HEADER_DESC'] = 'Apply user wise access control on different fields of different modules.';
//View Labels
$mod_strings['LBL_FIELD_ACCESS_CONTROL_ACCESS'] = 'Field Level Access Control Configurations';
$mod_strings['LBL_NO_MODULE_SELECTED'] = 'No Module Selected';
$mod_strings['LBL_MODULE_NAME'] = 'Administration';
//Licence Configuration Section
$mod_strings['LBL_FIELD_ACCESS_CONTROL_LICENCE'] = 'License Configuration';
$mod_strings['LBL_FIELD_ACCESS_CONTROL_LINK_DESC'] = 'Validate license and module configuration.';
$mod_strings['LBL_LICENCE_CONFIG_SECTION'] = 'License Configuration';
$mod_strings['LBL_LICENCE_CONFIG_SECTION_DESCRIPTION'] = 'Validate license and module configuration to access field level access';
