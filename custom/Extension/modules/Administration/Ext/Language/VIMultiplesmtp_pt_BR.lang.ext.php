<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_LINK_NAME'] = 'SMTP múltiplo';
$mod_strings['LBL_LINK_DESCRIPTION'] = 'Gerenciar várias configurações SMTP';
$mod_strings['LBL_ENABLED_MULTI_SMTP'] = 'Ativado SMTP múltiplo';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Atualizar licença';
?>