<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Notificações condicionais';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Gerenciar Notificação Condicional para módulos';
$mod_strings['LBL_ADD_NEW'] = '+ Adicionar Novo';
$mod_strings['LBL_SELECTED_MODULE'] = 'Módulo';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'Descrição';
$mod_strings['LBL_DELETE'] = 'Excluir';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Você atualmente não possui registros salvos.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'um agora.';
$mod_strings['LBL_SELECT_MODULE'] = 'Selecione o módulo';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Adicionar condições';
$mod_strings['LBL_ADD_TASKS'] = 'Adicionar tarefas';
$mod_strings['LBL_CANCEL'] = 'Cancelar';
$mod_strings['LBL_BACK'] = 'De volta';
$mod_strings['LBL_NEXT'] = 'Próximo';
$mod_strings['LBL_SAVE'] = 'SALVE';
$mod_strings['LBL_CLEAR'] = 'Claro';
$mod_strings['LBL_MODULE_PATH'] = 'Módulo Selecionado';
$mod_strings['LBL_FIELD'] = 'Campo';
$mod_strings['LBL_OPERATOR'] = 'Operador';
$mod_strings['LBL_VALUE_TYPE'] = 'Tipo de Valor';
$mod_strings['LBL_VALUE'] = 'Valor';
$mod_strings['LBL_ACTION_TITLE'] = 'Título da ação';
$mod_strings['LBL_NOTIFICATION_EDITING'] = 'Notificação durante a edição / criação';
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = 'Notificação quando o registro é visualizado';
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = 'Notificação em Salvar';
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = 'Não é permitido salvar registro';
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = 'Notificação Quando o registro é duplicado';
$mod_strings['LBL_INSERT'] = 'Inserir';
$mod_strings['LBL_INSERT_FIELDS'] = 'Inserir campos';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'Nestas etapas, é permitido selecionar o módulo no qual precisamos configurar a notificação condicional';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = 'Condição de configuração no campo do módulo para exibir a notificação';
$mod_strings['LBL_ADD_TASKS_NOTES'] = 'Configure várias ações quando a condição estiver satisfeita na etapa 2';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Operador Condicional';
$mod_strings['LBL_NOTE_LABEL'] = 'Nota:';
$mod_strings['LBL_NOTE'] = 'Abaixo está o exemplo da Expressão Regular ao selecionar o operador como Expressão Regular. Não adicione barra (/) antes e depois de qualquer Expressão Regular.'; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Apenas Valores Alfa: = ([A-z], [a-z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Somente valores numéricos: = ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'Para valores AlphaNumeric: = (ex. ^ [A-zA-Z0-9 _] * $).';
$mod_strings['LBL_DATE_VALUES'] = 'Para valores de data: = (^ ([0-9] {1,2}) \\ / ([0-9] {1,2}) \\ / ([0-9] {4}) $)';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'Para DateTime: = (^ ([0-9] {2,4}) - ([0-1] [0-9]) - ([0-3] [0-9]) (? [0-2 ] [0-9]): ([0-5] [0-9]): ([0-5] [0-9]))? $)';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'Todos';
$mod_strings['LBL_CREATE'] = 'CRIO';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Por favor selecione registros.";
$mod_strings['LBL_DELETE_MESSAGE'] = "Tem certeza de que deseja excluir";
$mod_strings['LBL_DELETE_THESE'] = 'estes';
$mod_strings['LBL_DELETE_THIS'] = 'esta';
$mod_strings['LBL_DELETE_ROW'] = 'linha?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Selecione uma opção';
$mod_strings['LBL_AND_OPERATOR'] = 'E';
$mod_strings['LBL_OR_OPERATOR'] = 'OU';
$mod_strings['LBL_VALIDATION_MESSAGE'] = 'Por favor, selecione um campo obrigatório.';
$mod_strings['LBL_PLZ_ENTER_VALUE'] = 'Por favor insira valor!';
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'Notificações';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Notificação com caixa de diálogo de confirmação';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Atualizar licença';
?>



