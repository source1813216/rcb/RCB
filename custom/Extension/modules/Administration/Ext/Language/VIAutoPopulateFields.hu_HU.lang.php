<?php
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Automatikus mezők kitöltése';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Kezelje az automatikus kitöltési mezőket a kapcsolódó mezőktől függően';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'HÁT';
$mod_strings['LBL_NEXT_BUTTON'] = 'KÖVETKEZŐ';
$mod_strings['LBL_CLEAR_BUTTON'] = 'EGYÉRTELMŰ';
$mod_strings['LBL_SAVE_BUTTON'] = 'MENTÉS';
$mod_strings['LBL_CANCEL_BUTTON'] = 'MEGSZÜNTETI';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Válassz egy lehetőséget';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Forrásmodul (adatok másolása)';
$mod_strings['LBL_STATUS'] = 'Állapot';
$mod_strings['LBL_SELECT_MODULE'] = 'Válassza a Modul lehetőséget';
$mod_strings['LBL_ACTIVE'] = 'Aktív';
$mod_strings['LBL_INACTIVE'] = 'tétlen';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Mező leképezés';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Terepi térképezés hozzáadása';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Válassza a Kapcsolódó mező lehetőséget';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Add Field Mapping Block';
$mod_strings['LBL_SOURCE_MODULE'] = 'Forrás modul';
$mod_strings['LBL_TARGET_MODULE'] = 'Célmodul';
$mod_strings['LBL_FIELDS'] = 'Fields';
$mod_strings['LBL_RELATED_FIELD'] = 'Kapcsolódó terület';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Számítsa ki a mezőt';
$mod_strings['LBL_FUNCTIONS'] = 'Funkciók';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Add a Számítás mező';
$mod_strings['LBL_INPUT_FORMULA'] = 'Bemeneti (képlet)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Add a Számítsuk ki a mezőblokkot';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'hozzáad';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'levon';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Szorzás';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Feloszt';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'String hossza';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'láncolat';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Log (logaritmus)';
$mod_strings['LBL_LN'] = 'Ln (természetes napló)';
$mod_strings['LBL_ABSOLUTE'] = 'Abszolút';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Átlagos';
$mod_strings['LBL_FUNCTION_POWER'] = 'Erő';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Dátumkülönbség';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'Százalék';
$mod_strings['LBL_FUNCTION_MOD'] = 'Mod (modulo)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'Minimális';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'tagad';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Padló';
$mod_strings['LBL_FUNCTION_CEIL'] = 'mennyezetet épít vmire';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Órák hozzáadása';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Add Day';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Hét hozzáadása';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Add hónap';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Add Year';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Hónapok';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Alnapok';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Alhét';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Alhónap';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Sub év';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Különböző napok';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Diff Hour';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Diff Perc';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Diff hónap';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Hét Diff';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Diff. Év';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = "A numerikus műveletekben csak az 'Integer' vagy 'Decimal' mezőket használjon";
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = 'Csak a "String" mezőket használja a String műveletekben';
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = "A Dátum műveletekben csak „Dátum” vagy „Datetime” mezőket használjon";
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = 'Kérjük, töltse ki az összes kötelező mezőt!!!';
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Kérjük, válassza ki a megfelelő típusú mezőt!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = 'Kérjük, adja meg az értéket';
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' A modulkonfiguráció már létezik. Válasszon másik modult.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Kérjük, válassza ki a Kapcsolódó mezőt, mielőtt rákattint a "Mezőmezős blokk hozzáadása" gombra.';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Kérjük, válassza a Field Mapping Field lehetőséget.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'mert ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = ', A mezőleképezési blokk már hozzáadva. Ha új mezőleképezést szeretne hozzáadni a következőhöz: ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' majd használja a meglévő blokkot.';

$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = ', Számítsa ki a már hozzáadott mezőblokkot. Ha új számítási mezőt szeretne hozzáadni a következőhöz: ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = 'Kérjük, töltse ki az összes mezőt !!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Kérjük, cserélje ki a "string_field" mezőnévvel, amely elérhető a Célmodul mezőiben a';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Kérjük, cserélje ki a "szám_mező" mezőnévvel, amely elérhető a Célmodul mezőiben a';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Kérjük, cserélje ki a "date_field" mezőnévvel, amely elérhető a Célmodul mezőiben a';
$mod_strings['LBL_BASE_ERROR'] = 'Kérjük, cserélje ki az "alap" paramétert bármely olyan log alapra, mint például 2, 10';
$mod_strings['LBL_DAYS_ERROR'] = 'Kérjük, cserélje ki a "nap" paramétert bármelyik számra';
$mod_strings['LBL_WEEK_ERROR'] = 'Kérjük, cserélje ki a "hét" paramétert bármelyik számra';
$mod_strings['LBL_MONTH_ERROR'] = 'Kérjük, cserélje ki a "hónap" paramétert bármelyik számra';
$mod_strings['LBL_YEAR_ERROR'] = 'Kérjük, cserélje ki a "év" paramétert bármelyik számra';
$mod_strings['LBL_HOURS_ERROR'] = 'Kérjük, cserélje ki a "óra" paramétert bármelyik számra';
$mod_strings['LBL_FUNCTION'] = 'Funkció';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Válassza ki a Matematikai vagy Logikai funkciót a számítás elvégzéséhez a mezőn.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Válassza ki azt a mezőt, amelyen számításokat kell végrehajtania'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Miután kiválasztotta a funkciót, automatikusan beírja az Input (Formula) mezőt, és ha kiválasztja azt a mezőt, amelyen a számítás elvégzésére van szüksége, akkor az is automatikusan feltöltődik. Ezt a mezőt át kell másolni és paraméterként be kell helyezni.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Válassza ki az Elsődleges modulmezőt a számítások automatikus kitöltéséhez a kapcsolódó mezőkiválasztás alapján';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Frissítse az engedélyt';
$mod_strings['LBL_ADD_NEW'] = '+ Új hozzáadása';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Jelenleg nincs mentett rekord.';
$mod_strings['LBL_CREATE'] = 'TEREMT';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' most egy.';
$mod_strings['LBL_DELETE'] = "Töröl";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Kérjük, válassza ki a feljegyzéseket.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'biztos, hogy törölni akarod';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'ezek';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'ez';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'sor?';
$mod_strings['LBL_EDIT'] = 'szerkesztése';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "Az állapot sikeresen aktiválva !!";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Az állapot sikeresen kikapcsolva !!";

$mod_strings['LBL_BULK_ACTION'] = 'BULK AKCIÓ';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' mező már kiválasztva van a Field Mapping számára. Kérjük, válasszon másik mezőt.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' A mező már kiválasztva van a Számítsuk ki a mezőt. Kérjük, válasszon másik mezőt.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Adjon hozzá egy "év" paramétert bármilyen számhoz';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Kérjük, adja hozzá a "hónap" paramétert bármely számmal a következőhöz:';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Adjon hozzá egy "hét" paramétert bármilyen számhoz';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Kérjük, adja hozzá a "napok" paramétert bármely számmal a következőre:';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Kérjük, adjon hozzá egy "base" paramétert bármilyen számhoz';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Adjon hozzá "órát" paramétert bármely számmal';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'a bemeneti képletben';