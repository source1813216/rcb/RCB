<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Selecteer primaire module';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Selecteer Default Panels to Hide';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Selecteer Standaardvelden om te verbergen';
$mod_strings['LBL_MODULE_PATH'] = 'Geselecteerde module';
$mod_strings['LBL_FIELD'] = 'Veld';
$mod_strings['LBL_OPERATOR'] = 'operator';
$mod_strings['LBL_VALUE_TYPE'] = 'Waarde type';
$mod_strings['LBL_VALUE'] = 'Waarde';
$mod_strings['LBL_SELECT_MODULE'] = 'Selecteer module';
$mod_strings['LBL_APPLY_CONDITION'] = 'Voorwaarde toepassen';
$mod_strings['LBL_SET_VISIBILITY'] = 'Zichtbaarheid instellen';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Waarde toewijzen';
$mod_strings['LBL_CANCEL'] = 'annuleren';
$mod_strings['LBL_BACK'] = 'Terug';
$mod_strings['LBL_NEXT'] = 'volgende';
$mod_strings['LBL_SAVE'] = 'OPSLAAN';
$mod_strings['LBL_CLEAR'] = 'Duidelijk';
$mod_strings['LBL_CONDITIONS'] = 'Voorwaarden';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Voorwaarden toevoegen';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Panelen om te verbergen';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Velden die moeten worden verborgen';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Panelen worden Show';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Velden worden weergegeven';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'Velden die alleen moeten worden gelezen';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = 'Fields to be Mandatory';
$mod_strings['LBL_ADD_FIELD'] = 'Voeg veld toe';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Dynamische panelen';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'Beheer de zichtbaarheid van velden en panelen bij selectie van specifieke velden.
';
$mod_strings['LBL_ADD_NEW'] = '+Voeg nieuw toe';
$mod_strings['LBL_DELETE'] = 'Verwijder';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = 'Naam';
$mod_strings['LBL_PRIMARY_MODULE'] = 'module';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'Standaard verborgen panelen';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'Standaard verborgen velden';
$mod_strings['LBL_CREATE_MESSAGE'] = 'U hebt momenteel geen records opgeslagen.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'een nu.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = 'Geef de voorwaarden op om panelen of velden te verbergen die zijn opgegeven in stap 3 (Zichtbaarheid instellen)';
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = 'Specificeer de waarde van elk veld van de module over het wijzigen van de veldwaarde in stap 2 (conditie toepassen)';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Verberg panelen wanneer de pagina de eerste keer wordt geladen.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'Verberg velden wanneer de pagina de eerste keer wordt geladen.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = 'Geselecteerde panelen worden verborgen als de waarde van het veld bij verandering van de veldwaarde in stap 2 (conditie toepassen)';
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = 'Geselecteerde panelen zijn zichtbaar als de waarde van het veld bij verandering van de veldwaarde in stap 2 (conditie toepassen)';
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = 'Geselecteerde velden worden verborgen wanneer de waarde van het veld bij verandering van de veldwaarde in stap 2 (conditie toepassen)';
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = 'Geselecteerde velden zijn zichtbaar als de waarde van het veld bij verandering van de veldwaarde in stap 2 (conditie toepassen)';
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = 'Selected Fields will be readonly when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = 'Geselecteerde velden zijn verplicht als de waarde van het veld bij verandering van de veldwaarde in stap 2 (conditie toepassen)';
$mod_strings['LBL_USER_TYPE'] = 'Gebruikerstype';
$mod_strings['LBL_USER_ROLES'] = 'Rollen';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = 'Selecteer Type gebruiker dat het effect Verbergen / tonen moet zien';
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Pas updates verbergen / weergeven toe op specifieke rollen';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Voorwaardelijke operator';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = 'Opmerking: deze voorwaarde is alleen van toepassing op de gebruiker met de hierboven geselecteerde rol.';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = 'voor meer informatie.';
$mod_strings['LBL_DATE_ENTERED'] = 'Datum gecreeërd';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Wijs achtergrondkleur toe';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = 'Specificeer achtergrondkleur van elk veld van de module over verandering van de achtergrondkleur van het veld in stap 2 (conditie toepassen)';
$mod_strings['LBL_CREATE'] = 'CREATE';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Selecteer alstublieft records.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Weet je zeker dat je wilt verwijderen';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'deze';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'deze';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'rij?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Kies een optie';
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'Alle gebruikers';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Gewone gebruiker';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'Systeembeheerder Gebruiker';
$mod_strings['LBL_AND'] = 'EN';
$mod_strings['LBL_OR'] = 'OF';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Voer de vereiste veldwaarden in';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = 'Voer alstublieft waarde in!';
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'Geselecteerde waarde is al in gebruik. Selecteer een andere waarde.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = "Selecteer een ander veld als voorwaarde, omdat u de bewerking hebt geselecteerd tussen voorwaarde 'AND'. U hebt dus niet hetzelfde veld opnieuw geselecteerd.";
$mod_strings['LBL_UPDATE_LICENSE'] = 'Licentie updaten';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Selecteer ter vergelijking het veld Multienum-type";