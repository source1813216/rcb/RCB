<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Select Primary Module';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Select Default Panels to Hide';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Select Default Fields to Hide';
$mod_strings['LBL_MODULE_PATH'] = 'Selected Module';
$mod_strings['LBL_FIELD'] = 'Field';
$mod_strings['LBL_OPERATOR'] = 'Operator';
$mod_strings['LBL_VALUE_TYPE'] = 'Value Type';
$mod_strings['LBL_VALUE'] = 'Value';
$mod_strings['LBL_SELECT_MODULE'] = 'Select Module';
$mod_strings['LBL_APPLY_CONDITION'] = 'Apply Condition';
$mod_strings['LBL_SET_VISIBILITY'] = 'Set Visibility';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Assign Value';
$mod_strings['LBL_CANCEL'] = 'Cancel';
$mod_strings['LBL_BACK'] = 'Back';
$mod_strings['LBL_NEXT'] = 'Next';
$mod_strings['LBL_SAVE'] = 'SAVE';
$mod_strings['LBL_CLEAR'] = 'Clear';
$mod_strings['LBL_CONDITIONS'] = 'Conditions';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Add Conditions';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Panels to be Hide';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Fields to be Hide';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Panels to be Show';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Fields to be Show';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'Fields to be Readonly';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = 'Fields to be Mandatory';
$mod_strings['LBL_ADD_FIELD'] = 'Add Field';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Dynamic Panels';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'Manage visibility of fields and panels on selection of specific fields.
';
$mod_strings['LBL_ADD_NEW'] = '+Add New';
$mod_strings['LBL_DELETE'] = 'Delete';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = 'Name';
$mod_strings['LBL_PRIMARY_MODULE'] = 'Module';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'Default Hidden Panels';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'Default Hidden Fields';
$mod_strings['LBL_CREATE_MESSAGE'] = 'You currently have no records saved.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'one now.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = 'Specify conditions to hide panels or fields specified in Step 3(Set Visibility)';
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = 'Specify value of any field of the module on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Hide panels when page load first time.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'Hide fields when page load first time.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = 'Selected Panels will be hidden when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = 'Selected Panels will be visible when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = 'Selected Fields will be hidden when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = 'Selected Fields will be visible when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = 'Selected Fields will be readonly when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = 'Selected Fields will be mandatory when value of the field on change of the field value in Step 2(Apply Condition)';
$mod_strings['LBL_USER_TYPE'] = 'User Type';
$mod_strings['LBL_USER_ROLES'] = 'Roles';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = 'Select Type of user who should see Hide/Show effect';
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Apply Hide/Show updates to specific roles';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Conditional Operator';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = 'Note: This condition Only applied to User with selected role above.';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = 'for more information.';
$mod_strings['LBL_DATE_ENTERED'] = 'Date Created';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Assign Background Color';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = 'Specify background color of any field of the module on change of the field background color in Step 2(Apply Condition)';
$mod_strings['LBL_CREATE'] = 'CREATE';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Please select records.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Are you sure you want to delete';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'these';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'this';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'row?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Select an Option';
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'All Users';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Regular User';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'System Administrator User';
$mod_strings['LBL_AND'] = 'AND';
$mod_strings['LBL_OR'] = 'OR';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Please Enter Required Field Values';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = 'Please Enter Value!';
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'Selected value is already used.Please select another value.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = "Please select another field for condition because you've selected operation between condition is 'AND'. So you didn't select same field again.";
$mod_strings['LBL_UPDATE_LICENSE'] = 'Update License';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Please select Multienum type field for comparison";


