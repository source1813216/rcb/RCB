<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Seleccionar módulo primario';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Seleccione los paneles predeterminados para ocultar';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Seleccione los campos predeterminados para ocultar';
$mod_strings['LBL_MODULE_PATH'] = 'Modulo seleccionado';
$mod_strings['LBL_FIELD'] = 'Campo';
$mod_strings['LBL_OPERATOR'] = 'Operadora';
$mod_strings['LBL_VALUE_TYPE'] = 'Tipo de valor';
$mod_strings['LBL_VALUE'] = 'Valor';
$mod_strings['LBL_SELECT_MODULE'] = 'Seleccionar módulo';
$mod_strings['LBL_APPLY_CONDITION'] = 'Aplicar condicion';
$mod_strings['LBL_SET_VISIBILITY'] = 'Establecer visibilidad';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Asignar valor';
$mod_strings['LBL_CANCEL'] = 'Cancelar';
$mod_strings['LBL_BACK'] = 'Espalda';
$mod_strings['LBL_NEXT'] = 'Siguiente';
$mod_strings['LBL_SAVE'] = 'SALVAR';
$mod_strings['LBL_CLEAR'] = 'Clara';
$mod_strings['LBL_CONDITIONS'] = 'Condiciones';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Añadir condiciones';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Paneles para ocultar';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Campos a ocultar';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Paneles para mostrar';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Campos a mostrar';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'Campos para ser Readonly';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = 'Campos a ser obligatorios';
$mod_strings['LBL_ADD_FIELD'] = 'Agregue campo';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Paneles dinámicos';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'Gestione la visibilidad de los campos y paneles en la selección de campos específicos.
';
$mod_strings['LBL_ADD_NEW'] = '+ Añadir nuevo';
$mod_strings['LBL_DELETE'] = 'Borrar';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = 'Nombre';
$mod_strings['LBL_PRIMARY_MODULE'] = 'Módulo';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'Paneles ocultos predeterminados';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'Campos ocultos predeterminados';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Actualmente no tiene registros guardados.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'uno ahora.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = 'Especifique las condiciones para ocultar los paneles o campos especificados en el Paso 3 (Establecer visibilidad)';
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = 'Especifique el valor de cualquier campo del módulo al cambiar el valor del campo en el Paso 2 (Aplicar condición)';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Ocultar paneles cuando la página se carga por primera vez.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'Ocultar paneles cuando la página se carga por primera vez.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = 'Los paneles seleccionados se ocultarán cuando el valor del campo al cambiar el valor del campo en el Paso 2 (Aplicar condición)';
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = 'Los paneles seleccionados serán visibles cuando el valor del campo al cambiar el valor del campo en el Paso 2 (Aplicar condición)';
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = 'Los campos seleccionados se ocultarán cuando el valor del campo al cambiar el valor del campo en el Paso 2 (Aplicar condición)';
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = 'Los campos seleccionados serán visibles cuando el valor del campo al cambiar el valor del campo en el Paso 2 (Aplicar condición)';
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = 'Los campos seleccionados se leerán solo cuando el valor del campo al cambiar el valor del campo en el Paso 2 (Aplicar condición)';
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = 'Los campos seleccionados serán obligatorios cuando el valor del campo al cambiar el valor del campo en el Paso 2 (Aplicar condición)';
$mod_strings['LBL_USER_TYPE'] = 'Tipo de usuario';
$mod_strings['LBL_USER_ROLES'] = 'Roles';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = 'Seleccione el tipo de usuario que debería ver el efecto Ocultar / Mostrar';
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Aplicar Ocultar / Mostrar actualizaciones a roles específicos';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Operador condicional';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = 'Nota: esta condición solo se aplica al usuario con el rol seleccionado arriba.';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = 'para más información.';
$mod_strings['LBL_DATE_ENTERED'] = 'fecha de creacion';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Asignar color de fondo';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = 'Especifique el color de fondo de cualquier campo del módulo al cambiar el color de fondo del campo en el Paso 2 (Aplicar condición)';
$mod_strings['LBL_CREATE'] = 'CREAR';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Por favor, seleccione los registros.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'estas seguro que quieres borrarlo';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'estas';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'esta';
$mod_strings['LBL_DELETE_MESSAGE_4'] = '¿fila?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Seleccione una opcion';
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'Todos los usuarios';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Usuario regular';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'Usuario administrador del sistema';
$mod_strings['LBL_AND'] = 'Y';
$mod_strings['LBL_OR'] = 'O';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Por favor ingrese los valores de campo requeridos';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = 'Por favor, introduzca el valor!';
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'El valor seleccionado ya está en uso. Seleccione otro valor.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = "Por favor, seleccione otro campo para condición porque ha seleccionado la operación entre condición es 'Y'. Así que no seleccionaste el mismo campo otra vez.";
$mod_strings['LBL_UPDATE_LICENSE'] = 'Actualizar licencia';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Seleccione el campo de tipo Multienum para comparar";


