<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Виберіть Основний модуль';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Виберіть "Стандартні панелі", щоб "Приховати"';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Виберіть Поля за умовчанням, щоб приховати';
$mod_strings['LBL_MODULE_PATH'] = 'Вибраний модуль';
$mod_strings['LBL_FIELD'] = 'Поле';
$mod_strings['LBL_OPERATOR'] = 'Оператор';
$mod_strings['LBL_VALUE_TYPE'] = 'Тип значення';
$mod_strings['LBL_VALUE'] = 'Значення';
$mod_strings['LBL_SELECT_MODULE'] = 'Виберіть Модуль';
$mod_strings['LBL_APPLY_CONDITION'] = 'Застосувати умову';
$mod_strings['LBL_SET_VISIBILITY'] = 'Встановити видимість';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Призначити значення';
$mod_strings['LBL_CANCEL'] = 'Скасувати';
$mod_strings['LBL_BACK'] = 'Назад';
$mod_strings['LBL_NEXT'] = 'Далі';
$mod_strings['LBL_SAVE'] = 'SAVE';
$mod_strings['LBL_CLEAR'] = 'Очистити';
$mod_strings['LBL_CONDITIONS'] = 'Умови';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Додати умови';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Панелі, які потрібно приховати';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Поля, які потрібно приховати';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Відображати панелі';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Поля для показу';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'Поля для читання лише для читання';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = "Поля обов'язкові для заповнення";
$mod_strings['LBL_ADD_FIELD'] = 'Додати поле';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Динамічні панелі';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'Керування видимістю полів і панелей при виборі конкретних полів.
';
$mod_strings['LBL_ADD_NEW'] = '+ Додати новий';
$mod_strings['LBL_DELETE'] = 'Видалити';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = "Ім'я";
$mod_strings['LBL_PRIMARY_MODULE'] = 'Модуль';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'За замовчуванням приховані панелі';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'За замовчуванням приховані поля';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Наразі у вас немає збережених записів.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'тепер.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = 'Вкажіть умови, щоб приховати панелі або поля, зазначені в кроці 3 (встановити видимість)';
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = 'Вкажіть значення будь-якого поля модуля при зміні значення поля на кроці 2 (Застосувати умова)';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Сховати панелі під час першого завантаження сторінки.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'Сховати поля під час першого завантаження сторінки.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = 'Вибрані панелі будуть приховані, коли значення поля при зміні значення поля в кроці 2 (застосувати умова)';
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = 'Вибрані панелі будуть видимі, коли значення поля при зміні значення поля в кроці 2 (застосувати умова)';
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = 'Вибрані поля будуть приховані, коли значення поля при зміні значення поля в кроці 2 (застосувати умови)';
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = 'Вибрані поля відображатимуться, коли значення поля при зміні значення поля в кроці 2 (застосувати умови)';
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = 'Вибрані поля будуть лише для читання, коли значення поля при зміні значення поля в кроці 2 (застосувати умови)';
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = "Вибрані поля будуть обов'язковими, коли значення поля при зміні значення поля в кроці 2 (застосувати умови)";
$mod_strings['LBL_USER_TYPE'] = 'Тип користувача';
$mod_strings['LBL_USER_ROLES'] = 'Ролі';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = 'Виберіть Тип користувача, який повинен бачити ефект приховування / показу';
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Застосувати Приховати / Показати оновлення для певних ролей';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Умовний оператор';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = 'Примітка: Ця умова застосовується лише до користувача з вибраною вище роллю.';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = 'для отримання додаткової інформації.';
$mod_strings['LBL_DATE_ENTERED'] = 'Дата створення';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Призначення кольору тла';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = 'Вкажіть колір фону будь-якого поля модуля при зміні кольору фону поля в кроці 2 (Застосувати умова)';
$mod_strings['LBL_CREATE'] = 'Створити';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Виберіть записи.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Дійсно видалити';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'ці';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'це';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'рядок?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Виберіть параметр';
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'Всі користувачі';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Регулярний користувач';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'Користувач системного адміністратора';
$mod_strings['LBL_AND'] = 'І';
$mod_strings['LBL_OR'] = 'АБО';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Введіть необхідні значення поля';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = 'Введіть значення!';
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'Вибране значення вже використовується. Виберіть інше значення.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = 'Виберіть інше поле для умови, оскільки вибрано операцію між умовою "AND". Таким чином, ви не вибрали те ж саме поле знову.';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Оновити ліцензію';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Для порівняння виберіть поле типу Multienum";