<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Выберите основной модуль';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Выберите панели по умолчанию, чтобы скрыть';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Выберите поля по умолчанию, чтобы скрыть';
$mod_strings['LBL_MODULE_PATH'] = 'Выбранный модуль';
$mod_strings['LBL_FIELD'] = 'поле';
$mod_strings['LBL_OPERATOR'] = 'оператор';
$mod_strings['LBL_VALUE_TYPE'] = 'Тип ценности';
$mod_strings['LBL_VALUE'] = 'Значение';
$mod_strings['LBL_SELECT_MODULE'] = 'Выберите модуль';
$mod_strings['LBL_APPLY_CONDITION'] = 'Применить условие';
$mod_strings['LBL_SET_VISIBILITY'] = 'Установить видимость';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Назначить значение';
$mod_strings['LBL_CANCEL'] = 'отменить';
$mod_strings['LBL_BACK'] = 'назад';
$mod_strings['LBL_NEXT'] = 'следующий';
$mod_strings['LBL_SAVE'] = 'СПАСТИ';
$mod_strings['LBL_CLEAR'] = 'Очистить';
$mod_strings['LBL_CONDITIONS'] = 'условия';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Добавить условия';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Панели должны быть Скрыть';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Поля, которые нужно скрыть';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Панели для шоу';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Поля для показа';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'Поля для чтения';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = 'Обязательные поля';
$mod_strings['LBL_ADD_FIELD'] = 'Добавить поле';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Динамические Панели';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'Управление видимостью полей и панелей по выбору конкретных полей.';
$mod_strings['LBL_ADD_NEW'] = '+Добавить новое';
$mod_strings['LBL_DELETE'] = 'удалять';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = 'название';
$mod_strings['LBL_PRIMARY_MODULE'] = 'модуль';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'Скрытые панели по умолчанию';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'Скрытые поля по умолчанию';
$mod_strings['LBL_CREATE_MESSAGE'] = 'В настоящее время у вас нет сохраненных записей.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'один сейчас.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = 'Укажите условия, чтобы скрыть панели или поля, указанные в шаге 3 (установить видимость)';
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = 'Укажите значение любого поля модуля при изменении значения поля в Шаге 2 (Применить условие)';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Скрыть панели при первой загрузке страницы.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'Скрыть поля при первой загрузке страницы.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = 'Выбранные панели будут скрыты при значении поля при изменении значения поля в Шаге 2 (Применить условие)';
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = 'Выбранные панели будут видны, когда значение поля при изменении значения поля в шаге 2 (применить условие)';
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = 'Выбранные поля будут скрыты при значении поля при изменении значения поля в Шаге 2 (Применить условие)';
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = 'Выбранные поля будут видны, когда значение поля при изменении значения поля в шаге 2 (применить условие)';
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = 'Выбранные поля будут только для чтения, когда значение поля при изменении значения поля в шаге 2 (применить условие))';
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = 'Выбранные поля будут обязательными при значении поля при изменении значения поля в Шаге 2 (Применить условие)';
$mod_strings['LBL_USER_TYPE'] = 'Тип пользователя';
$mod_strings['LBL_USER_ROLES'] = 'Роли';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = 'Выберите тип пользователя, который должен видеть эффект Скрыть / Показать';
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Применить Скрыть / Показать обновления для определенных ролей';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Условный оператор';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = 'Примечание. Это условие применимо только к пользователю с указанной выше ролью.';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = 'для дополнительной информации.';
$mod_strings['LBL_DATE_ENTERED'] = 'Дата создания';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Назначить цвет фона';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = 'Укажите цвет фона любого поля модуля при изменении цвета фона поля в Шаге 2 (Применить условие)';
$mod_strings['LBL_CREATE'] = 'СОЗДАЙТЕ';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Пожалуйста, выберите записи.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Вы уверены, что хотите удалить';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'эти';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'этот';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'строка?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Выберите опцию';
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'Все пользователи';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Обычный пользователь';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'Пользователь системного администратора';
$mod_strings['LBL_AND'] = 'А ТАКЖЕ';
$mod_strings['LBL_OR'] = 'ИЛИ ЖЕ';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Пожалуйста, введите обязательные значения поля';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = 'Пожалуйста, введите значение!';
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'Выбранное значение уже используется. Пожалуйста, выберите другое значение.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = "Пожалуйста, выберите другое поле для условия, потому что вы выбрали операцию между условиями «И». Таким образом, вы не выбрали то же поле снова.";
$mod_strings['LBL_UPDATE_LICENSE'] = 'Обновить лицензию';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Пожалуйста, выберите поле типа Multienum для сравнения";