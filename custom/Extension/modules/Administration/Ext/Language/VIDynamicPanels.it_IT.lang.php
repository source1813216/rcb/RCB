<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Seleziona il modulo primario';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Seleziona i pannelli predefiniti da nascondere';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Seleziona Campi predefiniti da nascondere';
$mod_strings['LBL_MODULE_PATH'] = 'Modulo selezionato';
$mod_strings['LBL_FIELD'] = 'Campo';
$mod_strings['LBL_OPERATOR'] = 'Operatrice';
$mod_strings['LBL_VALUE_TYPE'] = 'Tipo di valore';
$mod_strings['LBL_VALUE'] = 'Valore';
$mod_strings['LBL_SELECT_MODULE'] = 'Seleziona il modulo';
$mod_strings['LBL_APPLY_CONDITION'] = 'Applicare la condizione';
$mod_strings['LBL_SET_VISIBILITY'] = 'Imposta la visibilità';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Assegna valore';
$mod_strings['LBL_CANCEL'] = 'Annulla';
$mod_strings['LBL_BACK'] = 'Indietro';
$mod_strings['LBL_NEXT'] = 'Il prossimo';
$mod_strings['LBL_SAVE'] = 'SALVARE';
$mod_strings['LBL_CLEAR'] = 'Chiara';
$mod_strings['LBL_CONDITIONS'] = 'condizioni';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Aggiungi condizioni';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Pannelli da nascondere';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Campi da nascondere';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Pannelli da mostrare';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Campi da mostrare';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'I campi devono essere di sola lettura';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = 'I campi sono obbligatori';
$mod_strings['LBL_ADD_FIELD'] = 'Aggiungi campo';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Pannelli dinamici';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'Gestisci la visibilità di campi e pannelli sulla selezione di campi specifici.';
$mod_strings['LBL_ADD_NEW'] = '+Aggiungere nuova';
$mod_strings['LBL_DELETE'] = 'Elimina';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = 'Nome';
$mod_strings['LBL_PRIMARY_MODULE'] = 'Modulo';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'Pannelli nascosti predefiniti';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'Campi nascosti predefiniti';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Al momento non ci sono record salvati.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'uno ora.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = 'Specificare le condizioni per nascondere pannelli o campi specificati nel passaggio 3 (Imposta visibilità)';
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = 'Specificare il valore di qualsiasi campo del modulo in caso di modifica del valore del campo nel passaggio 2 (Apply Condition)';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Nascondi i pannelli quando la pagina viene caricata per la prima volta.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'Nascondi i campi quando la pagina viene caricata per la prima volta.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = 'I pannelli selezionati saranno nascosti quando il valore del campo sul cambiamento del valore del campo nel passaggio 2 (Applica condizione)';
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = 'I pannelli selezionati saranno visibili quando il valore del campo sul cambiamento del valore del campo nel passaggio 2 (Applica condizione)';
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = 'I campi selezionati saranno nascosti quando il valore del campo sul cambiamento del valore del campo nel passaggio 2 (Applica condizione)';
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = 'I campi selezionati saranno visibili quando il valore del campo sul cambiamento del valore del campo nel passaggio 2 (Applica condizione)';
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = 'I campi selezionati saranno di sola lettura quando il valore del campo al cambiamento del valore del campo nel passaggio 2 (Applica condizione)';
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = 'I campi selezionati saranno obbligatori quando il valore del campo sul cambiamento del valore del campo nel passaggio 2 (Applica condizione)';
$mod_strings['LBL_USER_TYPE'] = 'Tipologia di utente';
$mod_strings['LBL_USER_ROLES'] = 'ruoli';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = "Seleziona il tipo di utente che dovrebbe vedere l'effetto Nascondi / Mostra";
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Applica Nascondi / Mostra aggiornamenti per ruoli specifici';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Operatore condizionale';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = "Nota: questa condizione si applica solo all'utente con il ruolo selezionato sopra.";
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = 'per maggiori informazioni.';
$mod_strings['LBL_DATE_ENTERED'] = 'data di creazione';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Assegna colore di sfondo';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = 'Specificare il colore di sfondo di qualsiasi campo del modulo alla modifica del colore di sfondo del campo nel passaggio 2 (Apply Condition)';
$mod_strings['LBL_CREATE'] = 'CREARE';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Si prega di selezionare i record.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Sei sicuro di voler eliminare';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'questi';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'Questo';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'riga?';
$mod_strings['LBL_SELECT_AN_OPTION'] = "Seleziona un'opzione";
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'Tutti gli utenti';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Utente normale';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'Utente amministratore di sistema';
$mod_strings['LBL_AND'] = 'E';
$mod_strings['LBL_OR'] = 'O';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Inserisci i valori dei campi richiesti';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = 'Si prega di inserire valore!';
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'Il valore selezionato è già utilizzato. Seleziona un altro valore.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = "Seleziona un altro campo per condizione perché hai selezionato l'operazione tra condizione è 'AND'. Quindi non hai selezionato di nuovo lo stesso campo.";
$mod_strings['LBL_UPDATE_LICENSE'] = 'Aggiorna licenza';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Si prega di selezionare il campo Tipo multienum per il confronto";