<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Wählen Sie Primärmodul aus';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Wählen Sie die zu verdeckenden Standardbereiche aus';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Wählen Sie Standardfelder zum Ausblenden';
$mod_strings['LBL_MODULE_PATH'] = 'Ausgewähltes Modul';
$mod_strings['LBL_FIELD'] = 'Feld';
$mod_strings['LBL_OPERATOR'] = 'Operator';
$mod_strings['LBL_VALUE_TYPE'] = 'Werttyp';
$mod_strings['LBL_VALUE'] = 'Wert';
$mod_strings['LBL_SELECT_MODULE'] = 'Modul auswählen';
$mod_strings['LBL_APPLY_CONDITION'] = 'Bedingung anwenden';
$mod_strings['LBL_SET_VISIBILITY'] = 'Sichtbarkeit einstellen';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Wert zuweisen';
$mod_strings['LBL_CANCEL'] = 'Stornieren';
$mod_strings['LBL_BACK'] = 'Zurück';
$mod_strings['LBL_NEXT'] = 'Nächster';
$mod_strings['LBL_SAVE'] = 'SPAREN';
$mod_strings['LBL_CLEAR'] = 'klar';
$mod_strings['LBL_CONDITIONS'] = 'Bedingungen';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Bedingungen hinzufügen';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Panels zum Verstecken';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Felder, die ausgeblendet werden sollen';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Panels, um Show zu sein';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Felder zum Anzeigen';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'Felder zum Readonly';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = 'Pflichtfelder';
$mod_strings['LBL_ADD_FIELD'] = 'Feld hinzufügen';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Dynamische Panels';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'Verwalten Sie die Sichtbarkeit von Feldern und Bereichen bei der Auswahl bestimmter Felder.';
$mod_strings['LBL_ADD_NEW'] = '+ Neu hinzufügen';
$mod_strings['LBL_DELETE'] = 'Löschen';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = 'Name';
$mod_strings['LBL_PRIMARY_MODULE'] = 'Modul';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'Standardmäßig ausgeblendete Bedienfelder';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'Standardmäßig ausgeblendete Felder';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Sie haben derzeit keine Datensätze gespeichert.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'Eine jetzt.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = 'Angeben von Bedingungen zum Ausblenden der in Schritt 3 angegebenen Fenster oder Felder (Sichtbarkeit festlegen)';
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = 'Geben Sie den Wert eines beliebigen Felds des Moduls an, wenn Sie den Feldwert in Schritt 2 ändern (Bedingung anwenden).';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Blenden Sie die Bereiche aus, wenn Sie die Seite zum ersten Mal laden.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'Felder ausblenden, wenn Seite zum ersten Mal geladen wird.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = 'Ausgewählte Fenster werden ausgeblendet, wenn der Wert des Felds bei Änderung des Feldwerts in Schritt 2 (Bedingung anwenden) übernommen wird.';
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = 'Die ausgewählten Bereiche werden angezeigt, wenn der Wert des Felds bei Änderung des Feldwerts in Schritt 2 (Bedingung anwenden) angezeigt wird.';
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = 'Ausgewählte Felder werden ausgeblendet, wenn der Wert des Felds bei Änderung des Feldwerts in Schritt 2 (Bedingung übernehmen) übernommen wird.';
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = 'Ausgewählte Felder sind sichtbar, wenn der Wert des Felds bei Änderung des Feldwerts in Schritt 2 (Bedingung anwenden) angezeigt wird.';
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = 'Ausgewählte Felder werden nur gelesen, wenn der Wert des Felds bei Änderung des Feldwerts in Schritt 2 (Bedingung übernehmen) gilt.';
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = 'Ausgewählte Felder sind obligatorisch, wenn der Wert des Felds bei Änderung des Feldwerts in Schritt 2 (Bedingung anwenden) gilt.';
$mod_strings['LBL_USER_TYPE'] = 'Benutzertyp';
$mod_strings['LBL_USER_ROLES'] = 'Rollen';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = 'Wählen Sie den Typ des Benutzers aus, der den Effekt ausblenden / anzeigen sehen soll';
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Anwenden von Hide / Show-Updates für bestimmte Rollen';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Bedingter Operator';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = 'Hinweis: Diese Bedingung gilt nur für Benutzer mit der oben ausgewählten Rolle.';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = 'für mehr Informationen.';
$mod_strings['LBL_DATE_ENTERED'] = 'Datum erstellt';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Hintergrundfarbe zuweisen';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = 'Geben Sie die Hintergrundfarbe eines beliebigen Feldes des Moduls bei Änderung der Feldhintergrundfarbe in Schritt 2 an (Bedingung anwenden).';
$mod_strings['LBL_CREATE'] = 'ERSTELLEN';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Bitte wählen Sie Datensätze.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Sind Sie sicher, dass Sie löschen möchten';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'diese';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'diese';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'Reihe?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Wähle eine Option';
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'Alle Nutzer';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Regulärer Benutzer';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'Systemadministrator-Benutzer';
$mod_strings['LBL_AND'] = 'UND';
$mod_strings['LBL_OR'] = 'ODER';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Bitte geben Sie die erforderlichen Feldwerte ein';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = 'Bitte Wert eingeben!';
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'Der ausgewählte Wert wird bereits verwendet. Bitte wählen Sie einen anderen Wert.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = "Bitte wählen Sie ein anderes Feld für die Bedingung aus, da die Operation zwischen Bedingung und ausgewählt wurde. Sie haben also nicht dasselbe Feld erneut ausgewählt.";
$mod_strings['LBL_UPDATE_LICENSE'] = 'Lizenz aktualisieren';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Bitte wählen Sie zum Vergleich das Feld Multienum-Typ";