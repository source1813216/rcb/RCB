<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Условные уведомления';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Управление условным уведомлением для модулей';
$mod_strings['LBL_ADD_NEW'] = '+ Добавить новый';
$mod_strings['LBL_SELECTED_MODULE'] = 'модуль';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'Описание';
$mod_strings['LBL_DELETE'] = 'удалять';
$mod_strings['LBL_CREATE_MESSAGE'] = 'В настоящее время у вас нет сохраненных записей.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'один сейчас.';
$mod_strings['LBL_SELECT_MODULE'] = 'Выберите модуль';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Добавить условия';
$mod_strings['LBL_ADD_TASKS'] = 'Добавить задачи';
$mod_strings['LBL_CANCEL'] = 'отменить';
$mod_strings['LBL_BACK'] = 'назад';
$mod_strings['LBL_NEXT'] = 'следующий';
$mod_strings['LBL_SAVE'] = 'СПАСТИ';
$mod_strings['LBL_CLEAR'] = 'Очистить';
$mod_strings['LBL_MODULE_PATH'] = 'Выбранный модуль';
$mod_strings['LBL_FIELD'] = 'поле';
$mod_strings['LBL_OPERATOR'] = 'оператор';
$mod_strings['LBL_VALUE_TYPE'] = 'Тип ценности';
$mod_strings['LBL_VALUE'] = 'Значение';
$mod_strings['LBL_ACTION_TITLE'] = 'Название действия';
$mod_strings['LBL_NOTIFICATION_EDITING'] = 'Уведомление при редактировании / создании';
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = 'Уведомление при просмотре записи';
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = 'Уведомление о сохранении';
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = 'Не разрешается сохранять записи';
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = 'Уведомление, когда запись дублируется';
$mod_strings['LBL_INSERT'] = 'Вставить';
$mod_strings['LBL_INSERT_FIELDS'] = 'Вставить поля';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'На этом шаге можно выбрать модуль, для которого нам нужно установить условное уведомление.';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = 'Настройка условия в поле модуля для отображения уведомлений';
$mod_strings['LBL_ADD_TASKS_NOTES'] = 'Настройте различные действия, когда условие выполнено на шаге № 2';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Условный оператор';
$mod_strings['LBL_NOTE_LABEL'] = 'Заметка:';
$mod_strings['LBL_NOTE'] = 'Ниже приведен пример регулярного выражения при выборе оператора в качестве регулярного выражения. Не добавляйте косую черту (/) до и после любого регулярного выражения'; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Только альфа-значения: = ([A-z], [a-z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Только числовые значения: = ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'Для буквенно-цифровых значений: = (например, ^ [a-zA-Z0-9 _] * $).';
$mod_strings['LBL_DATE_VALUES'] = 'Для значений даты: = (^ ([0-9] {1,2}) \\ / ([0-9] {1,2}) \\ / ([0-9] {4}) $)';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'Для DateTime: = (^ ([0-9] {2,4}) - ([0-1] [0-9]) - ([0-3] [0-9]) (? [0-2 ] [0-9]): ([0-5] [0-9]): ([0-5] [0-9])) $)';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'Все';
$mod_strings['LBL_CREATE'] = 'СОЗДАЙТЕ';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Пожалуйста, выберите записи.";
$mod_strings['LBL_DELETE_MESSAGE'] = "Вы уверены, что хотите удалить";
$mod_strings['LBL_DELETE_THESE'] = 'эти';
$mod_strings['LBL_DELETE_THIS'] = 'этот';
$mod_strings['LBL_DELETE_ROW'] = 'строка?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Выберите опцию';
$mod_strings['LBL_AND_OPERATOR'] = 'А ТАКЖЕ';
$mod_strings['LBL_OR_OPERATOR'] = 'ИЛИ ЖЕ';
$mod_strings['LBL_VALIDATION_MESSAGE'] = 'Пожалуйста, выберите обязательное поле.';
$mod_strings['LBL_PLZ_ENTER_VALUE'] = 'Пожалуйста, введите значение!';
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'Уведомления';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Уведомление с диалогом подтверждения';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Обновить лицензию';
?>
