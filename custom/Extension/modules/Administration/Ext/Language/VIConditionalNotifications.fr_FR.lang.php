<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Notifications conditionnelles';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Gérer la notification conditionnelle pour les modules';
$mod_strings['LBL_ADD_NEW'] = '+ Ajouter un nouveau';
$mod_strings['LBL_SELECTED_MODULE'] = 'Module';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'La description';
$mod_strings['LBL_DELETE'] = 'Effacer';
$mod_strings['LBL_CREATE_MESSAGE'] = "Vous n'avez actuellement aucun enregistrement sauvegardé.";
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'un maintenant.';
$mod_strings['LBL_SELECT_MODULE'] = 'Sélectionnez le module';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Ajouter des conditions';
$mod_strings['LBL_ADD_TASKS'] = 'Ajouter des tâches';
$mod_strings['LBL_CANCEL'] = 'Annuler';
$mod_strings['LBL_BACK'] = 'Retour';
$mod_strings['LBL_NEXT'] = 'Suivant';
$mod_strings['LBL_SAVE'] = 'ENREGISTRER';
$mod_strings['LBL_CLEAR'] = 'Clair';
$mod_strings['LBL_MODULE_PATH'] = 'Module sélectionné';
$mod_strings['LBL_FIELD'] = 'Champ';
$mod_strings['LBL_OPERATOR'] = 'Opérateur';
$mod_strings['LBL_VALUE_TYPE'] = 'Type de valeur';
$mod_strings['LBL_VALUE'] = 'Valeur';
$mod_strings['LBL_ACTION_TITLE'] = "Titre de l'action";
$mod_strings['LBL_NOTIFICATION_EDITING'] = "Notification lors de l'édition / création";
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = "Notification lorsque l'enregistrement est visualisé";
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = "Notification lors de l'enregistrement";
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = "Ne pas autoriser l'enregistrement";
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = "Notification lorsque l'enregistrement est dupliqué";
$mod_strings['LBL_INSERT'] = 'Insérer';
$mod_strings['LBL_INSERT_FIELDS'] = 'Insérer des champs';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'Dans cette étape, il permet de sélectionner le module sur lequel nous devons configurer la notification conditionnelle.';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = "Condition d'installation sur le champ du module pour afficher la notification";
$mod_strings['LBL_ADD_TASKS_NOTES'] = "Configurez diverses actions lorsque la condition est remplie à l'étape 2";
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Opérateur conditionnel';
$mod_strings['LBL_NOTE_LABEL'] = 'Remarque:';
$mod_strings['LBL_NOTE'] = "Vous trouverez ci-dessous un exemple d'expression régulière lors de la sélection d'un opérateur en tant qu'expression régulière. N'ajoutez pas de barre oblique (/) avant et après une expression régulière."; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Seules les valeurs alpha: = ([A-z], [a-z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Seules les valeurs numériques: = ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'Pour les valeurs AlphaNumeric: = (ex. ^ [A-zA-Z0-9 _] * $).';
$mod_strings['LBL_DATE_VALUES'] = 'Pour les valeurs de date: = (^ ([0-9] {1,2}) \\ / ([0-9] {1,2}) \\ / ([0-9] {4}) $)';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'Pour DateTime: = (^ ([0-9] {2,4}) - ([0-1] [0-9]) - ([0-3] [0-9]) (? [0-2 ] [0-9]): ([0-5] [0-9]): ([0-5] [0-9]))? $)';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'Tout';
$mod_strings['LBL_CREATE'] = 'CRÉER';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Veuillez sélectionner des enregistrements.";
$mod_strings['LBL_DELETE_MESSAGE'] = "Etes-vous sûr que vous voulez supprimer";
$mod_strings['LBL_DELETE_THESE'] = 'celles-ci';
$mod_strings['LBL_DELETE_THIS'] = 'ce';
$mod_strings['LBL_DELETE_ROW'] = 'rangée?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Sélectionner une option';
$mod_strings['LBL_AND_OPERATOR'] = 'ET';
$mod_strings['LBL_OR_OPERATOR'] = 'OU';
$mod_strings['LBL_VALIDATION_MESSAGE'] = 'Veuillez sélectionner un champ requis.';
$mod_strings['LBL_PLZ_ENTER_VALUE'] = "S'il vous plaît entrer la valeur!";
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'Les notifications';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Notification avec boîte de dialogue de confirmation';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Mettre à jour la licence';
?>



