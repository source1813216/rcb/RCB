<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Умовне сповіщення';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Управління умовними повідомленнями для модулів';
$mod_strings['LBL_ADD_NEW'] = '+ Додати новий';
$mod_strings['LBL_SELECTED_MODULE'] = 'Модуль';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'Опис';
$mod_strings['LBL_DELETE'] = 'Видалити';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Наразі у вас немає збережених записів.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'тепер.';
$mod_strings['LBL_SELECT_MODULE'] = 'Виберіть Модуль';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Додати умови';
$mod_strings['LBL_ADD_TASKS'] = 'Додати завдання';
$mod_strings['LBL_CANCEL'] = 'Скасувати';
$mod_strings['LBL_BACK'] = 'Назад';
$mod_strings['LBL_NEXT'] = 'Далі';
$mod_strings['LBL_SAVE'] = 'SAVE';
$mod_strings['LBL_CLEAR'] = 'Очистити';
$mod_strings['LBL_MODULE_PATH'] = 'Вибраний модуль';
$mod_strings['LBL_FIELD'] = 'Поле';
$mod_strings['LBL_OPERATOR'] = 'Оператор';
$mod_strings['LBL_VALUE_TYPE'] = 'Тип значення';
$mod_strings['LBL_VALUE'] = 'Значення';
$mod_strings['LBL_ACTION_TITLE'] = 'Назва дії';
$mod_strings['LBL_NOTIFICATION_EDITING'] = 'Повідомлення під час редагування / створення';
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = 'Повідомлення При перегляді запису';
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = 'Повідомлення про збереження';
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = 'Не дозволяється зберегти запис';
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = 'Повідомлення Коли запис є дублікатом';
$mod_strings['LBL_INSERT'] = 'Вставити';
$mod_strings['LBL_INSERT_FIELDS'] = 'Вставити поля';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'На цьому кроки її дозволяють вибрати модуль, на якому нам потрібно налаштувати умовне повідомлення';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = 'Налаштування умов на полі модуля для відображення сповіщення';
$mod_strings['LBL_ADD_TASKS_NOTES'] = 'Налаштуйте різні дії, коли умова виконана на етапі # 2';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Умовний оператор';
$mod_strings['LBL_NOTE_LABEL'] = 'Примітка:';
$mod_strings['LBL_NOTE'] = 'Нижче наведено приклад регулярного виразу при виборі оператора як регулярного виразу. Не додайте косу риску (/) до та після будь-якого регулярного виразу.'; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Тільки значення Alpha: = ([A-z], [a-z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Тільки числові значення: = ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'Для значення AlphaNumeric: = (напр. ^ [A-zA-Z0-9 _] * $).';
$mod_strings['LBL_DATE_VALUES'] = 'Для значень дат: = (^ ([0-9] {1,2}): ((0-9) {1,2})';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'Для DateTime: = (^ ([0-9] {2,4}) - ([0-1] [0-9]) - ([0-3] [0-9]) (? [0-2] ] [0-9]): ([0-5] [0-9]): ([0-5] [0-9]))?';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'Усі';
$mod_strings['LBL_CREATE'] = 'СТВОРИТИ';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Виберіть записи.";
$mod_strings['LBL_DELETE_MESSAGE'] = "Ви впевнені, що хочете видалити";
$mod_strings['LBL_DELETE_THESE'] = 'ці';
$mod_strings['LBL_DELETE_THIS'] = 'це';
$mod_strings['LBL_DELETE_ROW'] = 'рядок?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Виберіть варіант';
$mod_strings['LBL_AND_OPERATOR'] = 'І';
$mod_strings['LBL_OR_OPERATOR'] = 'АБО';
$mod_strings['LBL_VALIDATION_MESSAGE'] = "Виберіть обов'язкове поле.";
$mod_strings['LBL_PLZ_ENTER_VALUE'] = 'Будь ласка, введіть цінність!';
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'Сповіщення';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Повідомлення з діалогом підтвердження';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Оновити ліцензію';
?>



