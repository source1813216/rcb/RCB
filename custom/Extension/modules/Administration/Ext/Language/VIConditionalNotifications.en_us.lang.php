<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Conditional Notifications';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Manage Conditional Notification for modules';
$mod_strings['LBL_ADD_NEW'] = '+Add New';
$mod_strings['LBL_SELECTED_MODULE'] = 'Module';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'Description';
$mod_strings['LBL_DELETE'] = 'Delete';
$mod_strings['LBL_CREATE_MESSAGE'] = 'You currently have no records saved.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'one now.';
$mod_strings['LBL_SELECT_MODULE'] = 'Select Module';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Add Conditions';
$mod_strings['LBL_ADD_TASKS'] = 'Add Tasks';
$mod_strings['LBL_CANCEL'] = 'Cancel';
$mod_strings['LBL_BACK'] = 'Back';
$mod_strings['LBL_NEXT'] = 'Next';
$mod_strings['LBL_SAVE'] = 'SAVE';
$mod_strings['LBL_CLEAR'] = 'Clear';
$mod_strings['LBL_MODULE_PATH'] = 'Selected Module';
$mod_strings['LBL_FIELD'] = 'Field';
$mod_strings['LBL_OPERATOR'] = 'Operator';
$mod_strings['LBL_VALUE_TYPE'] = 'Value Type';
$mod_strings['LBL_VALUE'] = 'Value';
$mod_strings['LBL_ACTION_TITLE'] = 'Action Title';
$mod_strings['LBL_NOTIFICATION_EDITING'] = 'Notification While Editing/Creating';
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = 'Notification When record is Viewed';
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = 'Notification On Save';
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = 'Do not allowed to Save record';
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = 'Notification When record is Duplicate';
$mod_strings['LBL_INSERT'] = 'Insert';
$mod_strings['LBL_INSERT_FIELDS'] = 'Insert Fields';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'In this steps its allow to select module on which we needs to setup conditional notification';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = 'Setup condition on field of module to display notification';
$mod_strings['LBL_ADD_TASKS_NOTES'] = 'Setup various action when condition satisfied on step # 2';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Conditional Operator';
$mod_strings['LBL_NOTE_LABEL'] = 'Note:';
$mod_strings['LBL_NOTE'] = 'Below are the example of Regular Expression when selecting operator as Regular Expression. Do not add slash(/) before and after of any Regular Expression.'; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Only Alpha Values   :=  ([A-z],[a-z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Only Numeric Values   :=  ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'For AlphaNumeric values := (ex.^[a-zA-Z0-9_]*$).';
$mod_strings['LBL_DATE_VALUES'] = 'For Date values := (^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$)';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'For DateTime := (^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])(? [0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$)';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'All';
$mod_strings['LBL_CREATE'] = 'CREATE';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Please select records.";
$mod_strings['LBL_DELETE_MESSAGE'] = "Are you sure you want to delete";
$mod_strings['LBL_DELETE_THESE'] = 'these';
$mod_strings['LBL_DELETE_THIS'] = 'this';
$mod_strings['LBL_DELETE_ROW'] = 'row?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Select an Option';
$mod_strings['LBL_AND_OPERATOR'] = 'AND';
$mod_strings['LBL_OR_OPERATOR'] = 'OR';
$mod_strings['LBL_VALIDATION_MESSAGE'] = 'Please select a required field.';
$mod_strings['LBL_PLZ_ENTER_VALUE'] = 'Please Enter Value!';
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'Notifications';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Notification with Confirmation Dialog';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Update License';
?>



