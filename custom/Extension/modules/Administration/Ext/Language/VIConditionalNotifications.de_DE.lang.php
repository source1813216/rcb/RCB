<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Bedingte Benachrichtigungen';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Bedingte Benachrichtigung f�r Module verwalten';
$mod_strings['LBL_ADD_NEW'] = '+ Neu hinzuf�gen';
$mod_strings['LBL_SELECTED_MODULE'] = 'Modul';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'Beschreibung';
$mod_strings['LBL_DELETE'] = 'L�schen';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Sie haben derzeit keine Datens�tze gespeichert.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'Eine jetzt.';
$mod_strings['LBL_SELECT_MODULE'] = 'Modul ausw�hlen';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Bedingungen hinzuf�gen';
$mod_strings['LBL_ADD_TASKS'] = 'Aufgaben hinzuf�gen';
$mod_strings['LBL_CANCEL'] = 'Stornieren';
$mod_strings['LBL_BACK'] = 'Zur�ck';
$mod_strings['LBL_NEXT'] = 'N�chster';
$mod_strings['LBL_SAVE'] = 'SPAREN';
$mod_strings['LBL_CLEAR'] = 'klar';
$mod_strings['LBL_MODULE_PATH'] = 'Ausgew�hltes Modul';
$mod_strings['LBL_FIELD'] = 'Feld';
$mod_strings['LBL_OPERATOR'] = 'Operator';
$mod_strings['LBL_VALUE_TYPE'] = 'Werttyp';
$mod_strings['LBL_VALUE'] = 'Wert';
$mod_strings['LBL_ACTION_TITLE'] = 'Titel der Aktion';
$mod_strings['LBL_NOTIFICATION_EDITING'] = 'Benachrichtigung beim Bearbeiten / Erstellen';
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = 'Benachrichtigung, wenn der Datensatz angezeigt wird';
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = 'Benachrichtigung beim Speichern';
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = 'Datensatz darf nicht gespeichert werden';
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = 'Benachrichtigung, wenn Datensatz doppelt vorhanden ist';
$mod_strings['LBL_INSERT'] = 'Einf�gen';
$mod_strings['LBL_INSERT_FIELDS'] = 'Felder einf�gen';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'In diesen Schritten k�nnen Sie das Modul ausw�hlen, auf dem die bedingte Benachrichtigung eingerichtet werden muss';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = 'Setup-Bedingung f�r das Feld des Moduls zum Anzeigen der Benachrichtigung';
$mod_strings['LBL_ADD_TASKS_NOTES'] = 'Richten Sie verschiedene Ma�nahmen ein, wenn die Bedingung in Schritt 2 erf�llt ist';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Bedingter Operator';
$mod_strings['LBL_NOTE_LABEL'] = 'Hinweis:';
$mod_strings['LBL_NOTE'] = 'Im Folgenden finden Sie das Beispiel f�r regul�re Ausdr�cke, wenn Sie den Operator als regul�re Ausdr�cke ausw�hlen. F�gen Sie vor und nach einem regul�ren Ausdruck keinen Schr�gstrich (/) ein.'; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Nur Alpha-Werte: = ([A-Z], [A-Z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Nur numerische Werte: = ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'F�r alphanumerische Werte: = (z. B. [a-zA-Z0-9 _] * $).';
$mod_strings['LBL_DATE_VALUES'] = 'F�r Datumswerte: = (^ ([0-9] {1,2}) \\ / ([0-9] {1,2}) \\ / ([0-9] {4}) $)';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'F�r DateTime: = (^ ([0-9] {2,4}) - ([0-1] [0-9]) - ([0-3] [0-9]) (& lgr; ] [0-9]): ([0-5] [0-9]): ([0-5] [0-9]))? $)';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'Alles';
$mod_strings['LBL_CREATE'] = 'ERSTELLEN';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Bitte w�hlen Sie Datens�tze.";
$mod_strings['LBL_DELETE_MESSAGE'] = "Sind Sie sicher, dass Sie l�schen m�chten";
$mod_strings['LBL_DELETE_THESE'] = 'diese';
$mod_strings['LBL_DELETE_THIS'] = 'diese';
$mod_strings['LBL_DELETE_ROW'] = 'Reihe?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'W�hle eine Option';
$mod_strings['LBL_AND_OPERATOR'] = 'UND';
$mod_strings['LBL_OR_OPERATOR'] = 'ODER';
$mod_strings['LBL_VALIDATION_MESSAGE'] = 'Bitte w�hlen Sie ein erforderliches Feld aus.';
$mod_strings['LBL_PLZ_ENTER_VALUE'] = 'Bitte Wert eingeben!';
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'Benachrichtigungen';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Benachrichtigung mit Best�tigungsdialog';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Lizenz aktualisieren';
?>



