<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Feltételes értesítések';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Modulok feltételes értesítésének kezelése';
$mod_strings['LBL_ADD_NEW'] = '+ Új hozzáadása';
$mod_strings['LBL_SELECTED_MODULE'] = 'modul';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'Leírás';
$mod_strings['LBL_DELETE'] = 'Töröl';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Jelenleg nincsenek mentve a rekordok.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'most.';
$mod_strings['LBL_SELECT_MODULE'] = 'Válassza a Modul lehetőséget';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Feltételek hozzáadása';
$mod_strings['LBL_ADD_TASKS'] = 'Feladatok hozzáadása';
$mod_strings['LBL_CANCEL'] = 'Megszünteti';
$mod_strings['LBL_BACK'] = 'Hát';
$mod_strings['LBL_NEXT'] = 'Következő';
$mod_strings['LBL_SAVE'] = 'MENTÉS';
$mod_strings['LBL_CLEAR'] = 'Egyértelmű';
$mod_strings['LBL_MODULE_PATH'] = 'Kiválasztott modul';
$mod_strings['LBL_FIELD'] = 'Mező';
$mod_strings['LBL_OPERATOR'] = 'Operátor';
$mod_strings['LBL_VALUE_TYPE'] = 'Érték tipusa';
$mod_strings['LBL_VALUE'] = 'Érték';
$mod_strings['LBL_ACTION_TITLE'] = 'Művelet címe';
$mod_strings['LBL_NOTIFICATION_EDITING'] = 'Értesítés szerkesztés / létrehozás közben';
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = 'Értesítés A felvétel megtekintésekor';
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = 'Értesítés mentéskor';
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = 'Ne engedélyezze a rekord mentését';
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = 'Értesítés Ha a rekord Duplicate';
$mod_strings['LBL_INSERT'] = 'Insert';
$mod_strings['LBL_INSERT_FIELDS'] = 'Mezők beszúrása';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'Ebben a lépésben lehetővé teszi a modul kiválasztását, amelyhez feltételes értesítést kell beállítanunk';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = 'Telepítési feltétel a modul mezőjén az értesítés megjelenítéséhez';
$mod_strings['LBL_ADD_TASKS_NOTES'] = 'Állítsa be a különböző műveleteket, ha a 2. lépésben teljesült a feltétel';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Feltételes operátor';
$mod_strings['LBL_NOTE_LABEL'] = 'Jegyzet:';
$mod_strings['LBL_NOTE'] = 'Az alábbiakban bemutatjuk a rendszeres kifejezések példáját, amikor rendszergazdai kifejezést választunk. Ne adjon hozzá sávot (/) a rendszeres kifejezés előtt és után.'; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Csak alfa értékek: = ([A-z], [a-z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Csak a numerikus értékek: = ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'AlphaNumerikus értékek esetén: = (pl. ^ [A-zA-Z0-9 _] * $).';
$mod_strings['LBL_DATE_VALUES'] = 'Dátumértékek esetén: = (^ ([0-9] {1,2}) / ([0-9] {1,2}) / ([0-9] {4}) $)';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'DateTime esetén: = (^ ([0-9] {2,4}) - ([0-1] [0-9]) - ([0-3] [0-9]) (? [0-2 ] [0-9]): ([0-5] [0-9]): ([0-5] [0-9]))? $)';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'Minden';
$mod_strings['LBL_CREATE'] = 'CREATE';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Kérjük, válassza ki a feljegyzéseket.";
$mod_strings['LBL_DELETE_MESSAGE'] = "biztos, hogy törölni akarod";
$mod_strings['LBL_DELETE_THESE'] = 'ezek';
$mod_strings['LBL_DELETE_THIS'] = 'ez';
$mod_strings['LBL_DELETE_ROW'] = 'sor?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Válassz egy lehetőséget';
$mod_strings['LBL_AND_OPERATOR'] = 'ÉS';
$mod_strings['LBL_OR_OPERATOR'] = 'VAGY';
$mod_strings['LBL_VALIDATION_MESSAGE'] = 'Kérjük, válassza ki a kívánt mezőt.';
$mod_strings['LBL_PLZ_ENTER_VALUE'] = 'Kérjük, adja meg az értéket!';
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'értesítések';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Értesítés megerősítő párbeszédpanellel';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Frissítse az engedélyt';
?>



