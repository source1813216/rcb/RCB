<?php
$mod_strings['LBL_MTS_SUBPANEL_TOGGLE_CONFIG'] = 'MTS Subpanel Toggle Settings';
$mod_strings['LBL_MTS_SUBPANEL_TOGGLE_CONFIG_TITLE'] = 'Config MTS Subpanel Toggle add-on';
$mod_strings['LBL_MTS_SUBPANEL_TOGGLE_CONFIG_DESC'] = 'Change settings for MTS Subpanel Toggle';

$mod_strings['LBL_MTS_SUBPANEL_TOGGLE_CONFIG_LICENSE'] = 'MTS Subpanel Toggle License Configuration';
$mod_strings['LBL_MTS_SUBPANEL_TOGGLE_CONFIG_LICENSE_DESC'] = 'Manage and configure the license for MTS Subpanel Toggle License add-on';