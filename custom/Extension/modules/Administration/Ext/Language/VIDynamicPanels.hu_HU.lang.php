<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Válassza az Elsődleges modul lehetőséget';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Válassza ki az elrejtendő alapértelmezett paneleket';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Válassza ki az elrejtendő alapértelmezett mezőket';
$mod_strings['LBL_MODULE_PATH'] = 'Kiválasztott modul';
$mod_strings['LBL_FIELD'] = 'Mező';
$mod_strings['LBL_OPERATOR'] = 'Operátor';
$mod_strings['LBL_VALUE_TYPE'] = 'Érték tipusa';
$mod_strings['LBL_VALUE'] = 'Érték';
$mod_strings['LBL_SELECT_MODULE'] = 'Válassza a Modul lehetőséget';
$mod_strings['LBL_APPLY_CONDITION'] = 'Feltétel alkalmazása';
$mod_strings['LBL_SET_VISIBILITY'] = 'Láthatóság beállítása';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Érték hozzárendelése';
$mod_strings['LBL_CANCEL'] = 'Megszünteti';
$mod_strings['LBL_BACK'] = 'Hát';
$mod_strings['LBL_NEXT'] = 'Következő';
$mod_strings['LBL_SAVE'] = 'MENTÉS';
$mod_strings['LBL_CLEAR'] = 'Egyértelmű';
$mod_strings['LBL_CONDITIONS'] = 'Körülmények';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Feltételek hozzáadása';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Elrejtendő panelek';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Elrejtendő mezők';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Panels to be Show';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Megjelenítendő mezők';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'Csak olvasható mezők';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = 'Kötelező mezők';
$mod_strings['LBL_ADD_FIELD'] = 'Mező hozzáadása';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Dinamikus panelek';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'A mezők és panelek láthatóságának kezelése bizonyos mezők kiválasztásakor.
';
$mod_strings['LBL_ADD_NEW'] = '+Új hozzáadása';
$mod_strings['LBL_DELETE'] = 'Töröl';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = 'Név';
$mod_strings['LBL_PRIMARY_MODULE'] = 'modul';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'Alapértelmezett rejtett panelek';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'Alapértelmezett rejtett mezők';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Jelenleg nincsenek mentve a rekordok.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'most.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = 'A 3. lépésben meghatározott panelek vagy mezők elrejtésének feltételei (Láthatóság beállítása)';
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = 'Adja meg a modul bármely mezőjének értékét a mező értékének megváltoztatásakor a 2. lépésben (Feltétel feltétele)';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Elrejtve az oldalak betöltését először.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'A mezők elrejtése az oldal betöltésekor.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = 'A kiválasztott panelek elrejtésre kerülnek, ha a mező értéke a mező értékének megváltoztatásakor a 2. lépésben (Alkalmazás feltétele)';
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = 'A kiválasztott panelek akkor lesznek láthatóak, ha a mező értéke a 2. mezőben a mező értékének megváltoztatásakor (Alkalmazási feltétel) lesz látható';
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = 'A kiválasztott mezők elrejtésre kerülnek, ha a mező értéke a mező értékének megváltoztatásakor a 2. lépésben (Alkalmazás feltétele)';
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = 'A kiválasztott mezők akkor jelennek meg, ha a mező értéke a mező értékének megváltoztatásakor a 2. lépésben (feltétel alkalmazása)';
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = 'A kiválasztott mezők csak akkor fognak olvasni, ha a mező értéke a 2. mezőben a mező értékének megváltoztatásakor (Alkalmazási feltétel)';
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = 'A kiválasztott mezők akkor kötelezőek, ha a mező értéke a 2. mezőben a mező értékének megváltoztatásakor (Alkalmazás feltétele)';
$mod_strings['LBL_USER_TYPE'] = 'Felhasználói típus';
$mod_strings['LBL_USER_ROLES'] = 'szerepek';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = 'Válassza ki a Felhasználó típusa lehetőséget, aki látja a Hide / Show hatás elrejtését';
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Alkalmazza a Hide / Show frissítéseket bizonyos szerepkörökhöz';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Feltételes operátor';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = 'Megjegyzés: Ez a feltétel Csak a fenti szerepkörrel rendelkező felhasználóra vonatkozik.';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = 'további információért.';
$mod_strings['LBL_DATE_ENTERED'] = 'Létrehozás dátuma';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Háttérszín hozzárendelése';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = 'Adja meg a modul bármelyik mezőjének háttérszínét a mező háttérszínének megváltoztatásakor a 2. lépésben (Feltétel feltétele)';
$mod_strings['LBL_CREATE'] = 'CREATE';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Kérjük, válassza ki a rekordokat.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'biztos, hogy törölni akarod';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'ezek';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'ez';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'sor?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Válassz egy lehetőséget';
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'Minden felhasználó';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Rendszeres felhasználó';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'Rendszeradminisztrátor felhasználó';
$mod_strings['LBL_AND'] = 'ÉS';
$mod_strings['LBL_OR'] = 'VAGY';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Kérjük, adja meg a szükséges mezőértékeket';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = 'Kérjük, adja meg az értéket!';
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'A kiválasztott érték már használatban van. Kérjük, válasszon másik értéket.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = "Kérjük, válasszon egy másik mezőt a feltételhez, mert a művelet között az 'AND'. Tehát nem választotta újra ugyanazt a mezőt.";
$mod_strings['LBL_UPDATE_LICENSE'] = 'Frissítse az engedélyt';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Az összehasonlításhoz válassza a Multienum típus mezőjét";