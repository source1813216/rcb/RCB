<?php

$mod_strings['LBL_FI_SUPERUSERACCESSLICENSEADDON'] = 'Superuser Access License Add-on';
$mod_strings['LBL_FI_SUPERUSERACCESSLICENSEADDON_LICENSE_TITLE'] = 'Superuser Access License';
$mod_strings['LBL_FI_SUPERUSERACCESSLICENSEADDON_LICENSE'] = 'Manage and configure the license for this add-on';
