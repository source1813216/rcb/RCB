<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Notificaciones condicionales';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Gestionar notificaciones condicionales para módulos.';
$mod_strings['LBL_ADD_NEW'] = '+ Añadir nuevo';
$mod_strings['LBL_SELECTED_MODULE'] = 'Módulo';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'Descripción';
$mod_strings['LBL_DELETE'] = 'Borrar';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Actualmente no tiene registros guardados.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'uno ahora.';
$mod_strings['LBL_SELECT_MODULE'] = 'Seleccionar módulo';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Añadir condiciones';
$mod_strings['LBL_ADD_TASKS'] = 'Añadir tareas';
$mod_strings['LBL_CANCEL'] = 'Cancelar';
$mod_strings['LBL_BACK'] = 'Espalda';
$mod_strings['LBL_NEXT'] = 'Siguiente';
$mod_strings['LBL_SAVE'] = 'SALVAR';
$mod_strings['LBL_CLEAR'] = 'Claro';
$mod_strings['LBL_MODULE_PATH'] = 'Modulo seleccionado';
$mod_strings['LBL_FIELD'] = 'Campo';
$mod_strings['LBL_OPERATOR'] = 'Operador';
$mod_strings['LBL_VALUE_TYPE'] = 'Tipo de valor';
$mod_strings['LBL_VALUE'] = 'Valor';
$mod_strings['LBL_ACTION_TITLE'] = 'Título de la acción';
$mod_strings['LBL_NOTIFICATION_EDITING'] = 'Notificación durante la edición / creación';
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = 'Notificación cuando se ve el registro';
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = 'Notificación en Guardar';
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = 'No se permite guardar el registro';
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = 'Notificación cuando el registro es duplicado';
$mod_strings['LBL_INSERT'] = 'Insertar';
$mod_strings['LBL_INSERT_FIELDS'] = 'Insertar campos';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'En estos pasos se permite seleccionar el módulo en el que se necesita configurar la notificación condicional.';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = 'Condición de configuración en el campo del módulo para mostrar la notificación';
$mod_strings['LBL_ADD_TASKS_NOTES'] = 'Configure varias acciones cuando la condición se cumpla en el paso # 2';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Operador condicional';
$mod_strings['LBL_NOTE_LABEL'] = 'Nota:';
$mod_strings['LBL_NOTE'] = 'A continuación se muestra el ejemplo de expresión regular al seleccionar el operador como expresión regular. No agregue una barra (/) antes y después de ninguna Expresión regular.'; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Sólo valores alfa: = ([A-z], [a-z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Sólo valores numéricos: = ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'Para valores AlphaNumeric: = (ej. ^ [A-zA-Z0-9 _] * $).';
$mod_strings['LBL_DATE_VALUES'] = 'Para valores de fecha: = (^ ([0-9] {1,2}) \\ / ([0-9] {1,2}) \\ / ([0-9] {4}) $)';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'Para DateTime: = (^ [0-9] {2,4}) - ([0-1] [0-9]) - ([0-3] [0-9]) ( ] [0-9]): ([0-5] [0-9]): ([0-5] [0-9]))? $)';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'Todos';
$mod_strings['LBL_CREATE'] = 'CREAR';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Por favor seleccione registros.";
$mod_strings['LBL_DELETE_MESSAGE'] = "estas seguro que quieres borrarlo";
$mod_strings['LBL_DELETE_THESE'] = 'estas';
$mod_strings['LBL_DELETE_THIS'] = 'esta';
$mod_strings['LBL_DELETE_ROW'] = '¿fila?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Seleccione una opcion';
$mod_strings['LBL_AND_OPERATOR'] = 'Y';
$mod_strings['LBL_OR_OPERATOR'] = 'O';
$mod_strings['LBL_VALIDATION_MESSAGE'] = 'Por favor seleccione un campo requerido.';
$mod_strings['LBL_PLZ_ENTER_VALUE'] = 'Por favor, introduzca el valor!';
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'Notificaciones';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Notificación con diálogo de confirmación';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Actualizar licencia';
?>



