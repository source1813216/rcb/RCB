<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Sélectionnez le module principal';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Sélectionner les panneaux par défaut à masquer';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Sélectionner les champs par défaut à masquer';
$mod_strings['LBL_MODULE_PATH'] = 'Module sélectionné';
$mod_strings['LBL_FIELD'] = 'Champ';
$mod_strings['LBL_OPERATOR'] = 'Opératrice';
$mod_strings['LBL_VALUE_TYPE'] = 'Type de valeur';
$mod_strings['LBL_VALUE'] = 'Valeur';
$mod_strings['LBL_SELECT_MODULE'] = 'Sélectionnez le module';
$mod_strings['LBL_APPLY_CONDITION'] = 'Appliquer la condition';
$mod_strings['LBL_SET_VISIBILITY'] = 'Définir la visibilité';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Attribuer une valeur';
$mod_strings['LBL_CANCEL'] = 'Annuler';
$mod_strings['LBL_BACK'] = 'Retour';
$mod_strings['LBL_NEXT'] = 'Suivante';
$mod_strings['LBL_SAVE'] = 'ENREGISTRER';
$mod_strings['LBL_CLEAR'] = 'Claire';
$mod_strings['LBL_CONDITIONS'] = 'Conditions';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Ajouter des conditions';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Panneaux à cacher';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Champs à masquer';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Panneaux à montrer';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Champs à afficher';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'Champs à être en lecture seule';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = 'Champs obligatoires';
$mod_strings['LBL_ADD_FIELD'] = 'Ajouter le champ';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Panneaux dynamiques';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'Gérer la visibilité des champs et des panneaux lors de la sélection de champs spécifiquess.
';
$mod_strings['LBL_ADD_NEW'] = '+ Ajouter un nouveau';
$mod_strings['LBL_DELETE'] = 'Effacer';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = 'prénom';
$mod_strings['LBL_PRIMARY_MODULE'] = 'Module';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'Panneaux cachés par défaut';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'Champs masqués par défaut';
$mod_strings['LBL_CREATE_MESSAGE'] = "Vous n'avez actuellement aucun enregistrement sauvegardé.";
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'un maintenant.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = "Spécifier des conditions pour masquer les panneaux ou les champs spécifiés à l'étape 3 (Définir la visibilité)";
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = "Spécifiez la valeur de n'importe quel champ du module lors du changement de la valeur du champ à l'étape 2 (Appliquer la condition).";
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Masquer les panneaux lors du premier chargement de la page.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'Masquer les champs lors du premier chargement de la page.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = "Les panneaux sélectionnés seront masqués lorsque la valeur du champ sera modifiée lors de la modification de la valeur du champ à l'étape 2 (Appliquer la condition).";
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = "Les panneaux sélectionnés seront visibles lorsque la valeur du champ sera modifiée lors de la modification de la valeur du champ à l'étape 2 (condition d'application).";
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = "Les champs sélectionnés seront masqués lorsque la valeur du champ sera modifiée lors de la modification de la valeur du champ à l'étape 2 (Appliquer la condition).";
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = "Les champs sélectionnés seront visibles lorsque la valeur du champ sera modifiée lors de la modification de la valeur du champ à l'étape 2 (Appliquer la condition).";
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = "Les champs sélectionnés seront en lecture seule lorsque la valeur du champ sera modifiée à l'étape 2 (Appliquer la condition).";
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = "Les champs sélectionnés seront obligatoires lorsque la valeur du champ sera modifiée lors de la modification de la valeur du champ à l'étape 2 (Appliquer la condition).";
$mod_strings['LBL_USER_TYPE'] = "Type d'utilisateur";
$mod_strings['LBL_USER_ROLES'] = 'Rôles';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = "Sélectionnez le type d'utilisateur qui devrait voir Cacher / Montrer l'effet";
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Appliquer Cacher / Afficher les mises à jour à des rôles spécifiques';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Opérateur conditionnel';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = "Remarque: Cette condition s'applique uniquement à l'utilisateur avec le rôle sélectionné ci-dessus.";
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = "pour plus d'informations.";
$mod_strings['LBL_DATE_ENTERED'] = 'date créée';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Attribuer une couleur de fond';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = "Spécifiez la couleur d'arrière-plan de n'importe quel champ du module lors du changement de la couleur d'arrière-plan du champ à l'étape 2 (Appliquer la condition).";
$mod_strings['LBL_CREATE'] = 'CRÉER';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Veuillez sélectionner des enregistrements.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Etes-vous sûr que vous voulez supprimer';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'celles-ci';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'ce';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'rangée?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Sélectionner une option';
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'Tous les utilisateurs';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Utilisateur régulier';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'Administrateur système utilisateur';
$mod_strings['LBL_AND'] = 'ET';
$mod_strings['LBL_OR'] = 'OU';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Veuillez entrer les valeurs de champ requises';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = "S'il vous plaît entrer la valeur!";
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'La valeur sélectionnée est déjà utilisée.Veuillez sélectionner une autre valeur.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = "Veuillez sélectionner un autre champ pour la condition car vous avez sélectionné l'opération entre la condition est 'AND'. Donc, vous n'avez pas sélectionné le même champ à nouveau.";
$mod_strings['LBL_UPDATE_LICENSE'] = 'Mettre à jour la licence';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Veuillez sélectionner le champ Type multienum pour la comparaison";