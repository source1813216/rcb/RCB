<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_SELECT_PRIMARY_MODULE'] = 'Selecione o Módulo Primário';
$mod_strings['LBL_SELECT_DEFAULT_PANELS_TO_HIDE'] = 'Selecione painéis padrão para ocultar';
$mod_strings['LBL_SELECT_DEFAULT_FIELDS_TO_HIDE'] = 'Selecione campos padrão para ocultar';
$mod_strings['LBL_MODULE_PATH'] = 'Módulo Selecionado';
$mod_strings['LBL_FIELD'] = 'Campo';
$mod_strings['LBL_OPERATOR'] = 'Operadora';
$mod_strings['LBL_VALUE_TYPE'] = 'Tipo de Valor';
$mod_strings['LBL_VALUE'] = 'Valor';
$mod_strings['LBL_SELECT_MODULE'] = 'Selecione o módulo';
$mod_strings['LBL_APPLY_CONDITION'] = 'Aplicar condição';
$mod_strings['LBL_SET_VISIBILITY'] = 'Definir visibilidade';
$mod_strings['LBL_ASSIGN_VALUE'] = 'Atribuir valor';
$mod_strings['LBL_CANCEL'] = 'Cancelar';
$mod_strings['LBL_BACK'] = 'De volta';
$mod_strings['LBL_NEXT'] = 'Próxima';
$mod_strings['LBL_SAVE'] = 'SALVE';
$mod_strings['LBL_CLEAR'] = 'Clara';
$mod_strings['LBL_CONDITIONS'] = 'Condições';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Adicionar condições';
$mod_strings['LBL_PANELS_TO_HIDE'] = 'Painéis a serem escondidos';
$mod_strings['LBL_FIELDS_TO_HIDE'] = 'Campos a serem escondidos';
$mod_strings['LBL_PANELS_TO_SHOW'] = 'Painéis para serem exibidos';
$mod_strings['LBL_FIELDS_TO_SHOW'] = 'Campos para serem exibidos';
$mod_strings['LBL_FIELDS_TO_READONLY'] = 'Campos para serem exibidos';
$mod_strings['LBL_FIELDS_TO_MANDATORY'] = 'Campos a serem obrigatórios';
$mod_strings['LBL_ADD_FIELD'] = 'Adicionar campo';
$mod_strings['LBL_DYNAMIC_PANELS'] = 'Painéis Dinâmicos';
$mod_strings['LBL_DYNAMIC_PANELS_DESCRIPTION'] = 'Gerenciar a visibilidade de campos e painéis na seleção de campos específicos.';
$mod_strings['LBL_ADD_NEW'] = '+Adicionar novo';
$mod_strings['LBL_DELETE'] = 'Excluir';
$mod_strings['LBL_DYNAMIC_PANELS_NAME'] = 'Nome';
$mod_strings['LBL_PRIMARY_MODULE'] = 'Módulo';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS'] = 'Painéis ocultos padrão';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS'] = 'Campos ocultos padrão';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Atualmente você não tem registros salvos.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'um agora.';
$mod_strings['LBL_CONDITIONS_MESSAGE'] = 'Especifique condições para ocultar painéis ou campos especificados na Etapa 3 (Definir Visibilidade)';
$mod_strings['LBL_ASSIGN_VALUE_MESSAGE'] = 'Especifique o valor de qualquer campo do módulo na alteração do valor do campo na Etapa 2 (Aplicar condição)';
$mod_strings['LBL_DEFAULT_HIDDEN_PANELS_TOOLTIP'] = 'Ocultar painéis quando a página carregar pela primeira vez.';
$mod_strings['LBL_DEFAULT_HIDDEN_FIELDS_TOOLTIP'] = 'Ocultar campos quando o carregamento da página é feito pela primeira vez.';
$mod_strings['LBL_PANELS_HIDE_TOOLTIP'] = 'Os Painéis Selecionados serão ocultados quando o valor do campo for alterado no valor do campo na Etapa 2 (Aplicar Condição)';
$mod_strings['LBL_PANELS_SHOW_TOOLTIP'] = 'Os Painéis Selecionados estarão visíveis quando o valor do campo for alterado no valor do campo na Etapa 2 (Aplicar Condição)';
$mod_strings['LBL_FIELDS_HIDE_TOOLTIP'] = 'Os campos selecionados serão ocultados quando o valor do campo for alterado no valor do campo na Etapa 2 (Aplicar condição)';
$mod_strings['LBL_FIELDS_SHOW_TOOLTIP'] = 'Campos selecionados serão visíveis quando o valor do campo na alteração do valor do campo na Etapa 2 (Aplicar condição)';
$mod_strings['LBL_FIELDS_REDONLY_TOOLTIP'] = 'Os campos selecionados serão lidos somente quando o valor do campo for alterado no valor do campo na Etapa 2 (Aplicar condição)';
$mod_strings['LBL_FIELDS_MANDATORY_TOOLTIP'] = 'Campos selecionados serão obrigatórios quando o valor do campo na alteração do valor do campo na Etapa 2 (Aplicar condição)';
$mod_strings['LBL_USER_TYPE'] = 'Tipo de usuário';
$mod_strings['LBL_USER_ROLES'] = 'Papéis';
$mod_strings['LBL_USER_TYPE_TOOLTIP'] = 'Selecione o tipo de usuário que deve ver o efeito Hide / Show';
$mod_strings['LBL_USER_ROLE_TOOLTIP'] = 'Aplicar ocultar / mostrar atualizações para funções específicas';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Operador Condicional';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE'] = 'Nota: Esta condição só se aplica ao usuário com função selecionada acima.';
$mod_strings['LBL_DYNAMIC_PANELS_NOTE1'] = 'Para maiores informações.';
$mod_strings['LBL_DATE_ENTERED'] = 'Data Criada';
$mod_strings['LBL_ASSIGN_BACKGROUND_COLOR'] = 'Atribuir cor do plano de fundo';
$mod_strings['LBL_ASSIGN_BACKGROUND_MESSAGE'] = 'Especifique a cor de fundo de qualquer campo do módulo na mudança da cor de fundo do campo na Etapa 2 (Aplicar condição)';
$mod_strings['LBL_CREATE'] = 'CRIO';
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Por favor selecione registros.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Tem certeza de que deseja excluir';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'estes';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'isto';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'linha?';
$mod_strings['LBL_SELECT_AN_OPTION'] = 'Selecione uma opção';
$mod_strings['LBL_USER_TYPE_ALL_USERS'] = 'Todos os usuários';
$mod_strings['LBL_USER_TYPE_REGULAR_USER'] = 'Utilizador regular';
$mod_strings['LBL_USER_TYPE_ADMIN_USER'] = 'Usuário Administrador do Sistema';
$mod_strings['LBL_AND'] = 'E';
$mod_strings['LBL_OR'] = 'OU';
$mod_strings['LBL_REQUIRED_VALIDATION'] = 'Por favor, insira os valores obrigatórios do campo';
$mod_strings['LBL_CONDITIONS_VALIDATION'] = 'Por favor insira valor!';
$mod_strings['LBL_PANEL_SHOW_VALIDATION'] = 'O valor selecionado já está sendo usado. Selecione outro valor.';
$mod_strings['LBL_CONDITION_FIELD_LABEL'] = "Por favor, selecione outro campo para condição porque você selecionou operação entre condição é 'AND'. Então você não selecionou o mesmo campo novamente.";
$mod_strings['LBL_UPDATE_LICENSE'] = 'Atualizar licença';
$mod_strings['LBL_CHECK_VALID_FIELD_TYPE'] = "Por favor, selecione o campo Tipo Multieno para comparação";