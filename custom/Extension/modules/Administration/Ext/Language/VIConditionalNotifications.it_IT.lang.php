<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS'] = 'Notifiche condizionali';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_DESCRIPTION'] = 'Gestisci notifiche condizionali per i moduli';
$mod_strings['LBL_ADD_NEW'] = '+ Aggiungi nuovo';
$mod_strings['LBL_SELECTED_MODULE'] = 'Modulo';
$mod_strings['LBL_CONDITIONAL_NOTIFICATIONS_MODULE_DESCRIPTION'] = 'Descrizione	';
$mod_strings['LBL_DELETE'] = 'Elimina';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Al momento non ci sono record salvati.';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = 'uno ora.';
$mod_strings['LBL_SELECT_MODULE'] = 'Seleziona il modulo';
$mod_strings['LBL_ADD_CONDITIONS'] = 'Aggiungi condizioni';
$mod_strings['LBL_ADD_TASKS'] = 'Aggiungi attività';
$mod_strings['LBL_CANCEL'] = 'Annulla';
$mod_strings['LBL_BACK'] = 'Indietro';
$mod_strings['LBL_NEXT'] = 'Il prossimo';
$mod_strings['LBL_SAVE'] = 'SALVARE';
$mod_strings['LBL_CLEAR'] = 'Chiaro';
$mod_strings['LBL_MODULE_PATH'] = 'Modulo selezionato';
$mod_strings['LBL_FIELD'] = 'Campo';
$mod_strings['LBL_OPERATOR'] = 'Operatore';
$mod_strings['LBL_VALUE_TYPE'] = 'Tipo di valore';
$mod_strings['LBL_VALUE'] = 'Valore';
$mod_strings['LBL_ACTION_TITLE'] = 'Azione Titolo';
$mod_strings['LBL_NOTIFICATION_EDITING'] = 'Notifica durante la modifica / creazione';
$mod_strings['LBL_NOTIFICATION_ON_VIEW'] = 'Notifica quando viene visualizzato il record';
$mod_strings['LBL_NOTIFICATION_ON_SAVE'] = 'Notifica su Salva';
$mod_strings['LBL_DO_NOT_ALLOW_SAVE'] = 'Non è permesso salvare il record';
$mod_strings['LBL_NOTIFICATION_ON_DUPLICATE'] = 'Notifica quando il record è duplicato';
$mod_strings['LBL_INSERT'] = 'Inserire';
$mod_strings['LBL_INSERT_FIELDS'] = 'Inserisci campi';
$mod_strings['LBL_SELECT_MODULE_NOTES'] = 'In questa procedura consente di selezionare il modulo su cui è necessario impostare la notifica condizionale';
$mod_strings['LBL_ADD_CONDITIONS_NOTES'] = 'Condizione di installazione sul campo del modulo per visualizzare la notifica';
$mod_strings['LBL_ADD_TASKS_NOTES'] = 'Imposta varie azioni quando la condizione è soddisfatta al punto # 2';
$mod_strings['LBL_CONDITIONAL_OPERATOR'] = 'Operatore condizionale';
$mod_strings['LBL_NOTE_LABEL'] = 'Nota:';
$mod_strings['LBL_NOTE'] = "Di seguito sono riportati gli esempi di Espressione regolare quando si seleziona l'operatore come espressione regolare. Non aggiungere la barra (/) prima e dopo di qualsiasi espressione regolare."; 
$mod_strings['LBL_ONLY_ALPHA_VALUES'] = 'Solo valori alfa: = ([A-z], [a-z])';
$mod_strings['LBL_ONLY_NUMERIC_VALUES'] = 'Solo valori numerici: = ([0-9])';
$mod_strings['LBL_ALPHANUMERIC_VALUES'] = 'Per i valori alfanumerici: = (ad esempio ^ [a-zA-Z0-9 _] * $).';
$mod_strings['LBL_DATE_VALUES'] = 'Per i valori Date: = (^ ([0-9] {1,2}) \\ / ([0-9] {1,2}) \\ / ([0-9] {4}) $)';
$mod_strings['LBL_DATE_TIME_VALUES'] = 'Per DateTime: = (^ ([0-9] {2,4}) - ([0-1] [0-9]) - ([0-3] [0-9]) (? [0-2 ] [0-9]): ([0-5] [0-9]): ([0-5] [0-9])) $)';
$mod_strings['LBL_MODULE_FILTER_ALL'] = 'Tutti';
$mod_strings['LBL_CREATE'] = 'CREARE';
$mod_strings['LBL_PLZ_SELECT_RECORDS'] = "Seleziona i record.";
$mod_strings['LBL_DELETE_MESSAGE'] = "Sei sicuro di voler eliminare";
$mod_strings['LBL_DELETE_THESE'] = 'questi';
$mod_strings['LBL_DELETE_THIS'] = 'Questo';
$mod_strings['LBL_DELETE_ROW'] = 'riga?';
$mod_strings['LBL_SELECT_AN_OPTION'] = "Seleziona un'opzione";
$mod_strings['LBL_AND_OPERATOR'] = 'E';
$mod_strings['LBL_OR_OPERATOR'] = 'O';
$mod_strings['LBL_VALIDATION_MESSAGE'] = 'Seleziona un campo obbligatorio';
$mod_strings['LBL_PLZ_ENTER_VALUE'] = 'Inserisci il valore!';
$mod_strings['LBL_NOTIFICATIONS_LABLE'] = 'notifiche';
$mod_strings['LBL_CONFIRMATION_DIALOG_LABEL'] = 'Notifica con finestra di dialogo di conferma';
$mod_strings['LBL_UPDATE_LICENSE'] = 'Aggiorna licenza';
?>



