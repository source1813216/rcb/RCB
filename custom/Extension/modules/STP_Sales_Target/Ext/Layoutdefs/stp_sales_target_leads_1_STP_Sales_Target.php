<?php
 // created: 2021-08-15 18:03:18
$layout_defs["STP_Sales_Target"]["subpanel_setup"]['stp_sales_target_leads_1'] = array (
  'order' => 100,
  'module' => 'Leads',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_LEADS_TITLE',
  'get_subpanel_data' => 'stp_sales_target_leads_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
