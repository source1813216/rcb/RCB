<?php
// created: 2021-08-15 18:03:18
$dictionary["STP_Sales_Target"]["fields"]["stp_sales_target_leads_1"] = array (
  'name' => 'stp_sales_target_leads_1',
  'type' => 'link',
  'relationship' => 'stp_sales_target_leads_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'side' => 'right',
  'vname' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_LEADS_TITLE',
);
