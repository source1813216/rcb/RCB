<?php

function getAvailableSubpanelToggleModules()
{
  $mts = new MTS_SubpanelToggle();
  return $mts->getAvailableModules();
}
