<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
    $entry_point_registry['VIDynamicPanelsModuleFields'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFields.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsModuleRelationships'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleRelationships.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsModuleOperatorField'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleOperatorField.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsFieldTypeOptions'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsFieldTypeOptions.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsModuleFieldType'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFieldType.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsAllModulePanels'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsAllModulePanels.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsAllModuleFields'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsAllModuleFields.php',
        'auth' => true
    );
    $entry_point_registry['VIAddDynamicPanels'] = array(
        'file' => 'custom/VIDynamicPanels/VIAddDynamicPanels.php',
        'auth' => true
    );
    $entry_point_registry['VIDeleteDynamicPanels'] = array(
        'file' => 'custom/VIDynamicPanels/VIDeleteDynamicPanels.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsModuleFieldLabel'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFieldLabel.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsDisplayRoles'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsDisplayRoles.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsDefault'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsDefault.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsCheckCondition'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsModuleFieldValue'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFieldValue.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsDetailViewCheckCondition'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsDetailViewCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsConfiguration'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsConfiguration.php',
        'auth' => true
    ); 
    $entry_point_registry['VIDynamicPanelsCheckValidFieldType'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsCheckValidFieldType.php',
        'auth' => true
    );   
?>