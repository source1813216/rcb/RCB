<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
    $entry_point_registry['VIConditionalNotificationsModuleRelationships'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleRelationships.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationModuleFields'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationModuleFields.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsModuleOperatorField'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleOperatorField.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsFieldTypeOptions'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsFieldTypeOptions.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsModuleFieldType'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleFieldType.php',
        'auth' => true
    );
    $entry_point_registry['VIAddConditionalNotifications'] = array(
        'file' => 'custom/VIConditionalNotifications/VIAddConditionalNotifications.php',
        'auth' => true
    );
    $entry_point_registry['VIDeleteConditionalNotifications'] = array(
        'file' => 'custom/VIConditionalNotifications/VIDeleteConditionalNotifications.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsModuleFieldLabel'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleFieldLabel.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsEditviewCheckCondition'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsEditviewCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsDetailViewCheckCondition'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsDetailViewCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsOnSaveCheckCondition'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsOnSaveCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsEditviewDuplicateCheckCondition'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsEditviewDuplicateCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsDuplicateCheckCondition'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsDuplicateCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsConfiguration'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsConfiguration.php',
        'auth' => true
    );
?>