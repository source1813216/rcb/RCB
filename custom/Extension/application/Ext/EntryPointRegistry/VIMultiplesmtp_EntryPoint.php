<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
	$entry_point_registry['VIMultiplesmtp_Status'] = array(
        'file' => 'custom/VIMultiSMTP/VIMultiplesmtp_Status.php',
        'auth' => true
    );
    $entry_point_registry['VIOutgoing_Server_Setting'] = array(
        'file' => 'custom/VIMultiSMTP/VIOutgoing_Server_Setting.php',
        'auth' => true
    );
    $entry_point_registry['VICheckMultiSMTPStatus'] = array(
        'file' => 'custom/VIMultiSMTP/VICheckMultiSMTPStatus.php',
        'auth' => true
    );
?>