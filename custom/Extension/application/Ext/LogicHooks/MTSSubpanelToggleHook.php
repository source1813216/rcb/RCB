<?php


if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_ui_frame']) || !is_array($hook_array['after_ui_frame'])) {
    $hook_array['after_ui_frame'] = array();
}
$hook_array['after_ui_frame'][] = array(9669, 'MTS Subpanel Toggle', 'modules/MTS_SubpanelToggle/MTSSubpanelToggleHook.php', 'MTSSubpanelToggleHook', 'appendSubpanelToggle');
