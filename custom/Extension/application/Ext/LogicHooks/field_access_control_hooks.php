<?php
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['process_record']) || !is_array($hook_array['process_record'])) {
    $hook_array['process_record'] = array();
}
if (!isset($hook_array['after_retrieve']) || !is_array($hook_array['after_retrieve'])) {
    $hook_array['after_retrieve'] = array();
}
$hook_array['process_record'][] = Array(101, 'access_control_fields', 'modules/access_control_fields/logic_for_subpanelAndDashletsList.php', 'fields_acl_sub_dash_class', 'accesible_views_sub_dash');
$hook_array['after_retrieve'][] = Array(101, 'access_control_fields', 'modules/access_control_fields/fields_logic.php', 'fields_acl_class', 'accesible_views');
?>