<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VIAutoPopulateFieldsDisplayCalculateFieldsFormula{
	public function __construct(){
		$this->getCalculateFieldsFormula();
	} 
	public function getCalculateFieldsFormula(){
		$functionVal = $_REQUEST['functionValue'];
		switch($functionVal){
			case 'Add':
						$value = "add(number_field1,number_field2,...)";
						break;
			case 'Subtract':
						$value = "sub(number_field1,number_field2,...)";
						break;
			case 'Multiply':
						$value = "mul(number_field1,number_field2,...)";
						break;
			case 'Divide':
						$value = "div(number_field1,number_field2)";
						break;
			case 'String Length':
						$value = "strlen(string_field)";
						break;
			case 'Concatenation':
						$value = "concat(string_field1,string_field2,...)";
						break;
			case 'Logarithm':
						$value = "log(number_field,base)";
						break;
			case 'Ln':
						$value = "ln(number_field)";
						break;
			case 'Absolute':
						$value = "abs(number_field)";
						break;
			case 'Average':
						$value = "avg(number_field1,number_field2,...)";
						break;
			case 'Power':
						$value = "pow(number_field1,number_field2)";
						break;
			case 'Date Difference':
						$value = "date_difference(date_field1,date_field2)";
						break;
			case 'Percentage':
						$value = "percentage(number_field1,number_field2)";
						break;
			case 'Mod':
						$value = "mod(number_field1,number_field2)";
						break;
			case 'Minimum':
						$value = "min(number_field1,number_field2,...)";
						break;
			case 'Negate':
						$value = "negate(number_field)";
						break;
			case 'Floor':
						$value = "floor(number_field)";
						break;
			case 'Ceil':
						$value = "ceil(number_field)";
						break;
			case 'Add Hours':
						$value = "add(date_field,hours)";
						break;
			case 'Add Day':
						$value = "add(date_field,days)";
						break;
			case 'Add Week':
						$value = "add(date_field,week)";
						break;
			case 'Add Month':
						$value = "add(date_field,month)";
						break;
			case 'Add Year':
						$value = "add(date_field,year)";
						break;
			case 'Sub Hours':
						$value = "sub(date_field,hours)";
						break;
			case 'Sub Days':
						$value = "sub(date_field,days)";
						break;
			case 'Sub Week':
						$value = "sub(date_field,week)";
						break;
			case 'Sub Month':
						$value = "sub(date_field,month)";
						break;
			case 'Sub Year':
						$value = "sub(date_field,year)";
						break;
			case 'Diff Days':
						$value = "diff_days(date_field1,date_field2)";
						break;
			case 'Diff Hour':
						$value = "diff_hours(date_field1,date_field2)";
						break;
			case 'Diff Minute':
						$value = "diff_minute(date_field1,date_field2)";
						break;
			case 'Diff Month':
						$value = "diff_month(date_field1,date_field2)";
						break;
			case 'Week Diff':
						$value = "diff_week(date_field1,date_field2)";
						break;
			case 'Diff Year':
						$value = "diff_year(date_field1,date_field2)";
						break;
			default:
				$value = '';
				break;
		}
		echo $value;
	}//end of function
}//end of class
new VIAutoPopulateFieldsDisplayCalculateFieldsFormula();
?>	