<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
require_once('custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php');
class VIAutoPopulateFieldsSaveConfiguration{
	public function __construct(){
		$this->saveModuleConfiguration();
	} 
	public function saveModuleConfiguration() { 
		if(isset($_REQUEST['formData'])){
			parse_str($_REQUEST['formData'], $formData); // form data
			$recordId = $_REQUEST['recordId']; //record if

			$sourceModule = $formData['module'];
			$status = $formData['status'];
		
			$autoPopulateFieldsId = create_guid();
			$insertUpdateData = array(
										'autopopulatefields_id' => "'".$autoPopulateFieldsId."'",
										'module' => "'".$sourceModule."'",
										'status' => "'".$status."'",
										'deleted' => 0
		                            );
		
			if($recordId == ''){
				insertAutoPopulateFieldsRecord('vi_autopopulatefields',$insertUpdateData);
				$recordId = $autoPopulateFieldsId;
			}else{
				unset($insertUpdateData['autopopulatefields_id']);
				$where = array('autopopulatefields_id' => $recordId);
				updateAutoPopulateFieldsRecord('vi_autopopulatefields',$insertUpdateData,$where);
				delAutoPopulateFieldsRecord('vi_autopopulatefields_mapping',$where);

				delAutoPopulateFieldsRecord('vi_autopopulate_calculate_fields',$where);
			}//end of else
			
			if(isset($formData['related_field_name'])){
				foreach ($formData['related_field_name'] as $key => $value) {
					$sourceModuleFieldsArray = $targetModuleFieldsArray = array();
					$relatedFieldModule = $formData['related_field_module'][$key];
					$relatedFieldName = $value;

					//source module field
					if(isset($formData[$value.'_source_module_fields'])){
						foreach ($formData[$value.'_source_module_fields'] as $skey => $svalue) {
							if($formData[$value.'_conditions_deleted'][$skey] == 0){
								$sourceModuleFieldsArray[] = $svalue;
							}//end of if
						}//end of foreach
					}//end of if

					//target module fields
					if(isset($formData[$value.'_target_module_fields'])){
						foreach ($formData[$value.'_target_module_fields'] as $tkey => $tvalue) {
							if($formData[$value.'_conditions_deleted'][$tkey] == 0){
								$targetModuleFieldsArray[] = $tvalue;
							}//end of if
						}//end of foreach
					}//end of if

					//insert field mapping data
					if(!empty($sourceModuleFieldsArray) && !empty($targetModuleFieldsArray)){
						$fieldMappingId = create_guid();
						$fieldMappingData = array(
										'field_mapping_id' => "'".$fieldMappingId."'",
										'related_field_module' => "'".$relatedFieldModule."'",
										'related_field_name' => "'".$relatedFieldName."'",
										'source_module_fields' => "'".json_encode($sourceModuleFieldsArray)."'",
										'target_module_fields' => "'".json_encode($targetModuleFieldsArray)."'",
										'autopopulatefields_id' => "'".$recordId."'",
										'deleted' => 0
									);
						insertAutoPopulateFieldsRecord('vi_autopopulatefields_mapping',$fieldMappingData);
					}//end of if
				}//end of foreach
			}//end of if

			if(isset($formData['calculate_related_field_name'])){
				foreach ($formData['calculate_related_field_name'] as $key => $value) {
					$functionNameArray = $inputFormulaArray = $sourceModuleCalculateFieldsArray = array();
					$calculateRelatedFieldModule = $formData['calculate_related_field_module'][$key];
					$calculateRelatedFieldName = $value;

					//function name
					if(isset($formData[$value.'_function_name'])){
						foreach ($formData[$value.'_function_name'] as $fkey => $fvalue) {
							if($formData[$value.'_calculate_conditions_deleted'][$fkey] == 0){
								$functionNameArray[] = $fvalue;
							}//end of if
						}//end of foreach
					}//end of if

					//input formula
					if(isset($formData[$value.'_input_formula'])){
						foreach ($formData[$value.'_input_formula'] as $ikey => $ivalue) {
							if($formData[$value.'_calculate_conditions_deleted'][$ikey] == 0){
								$inputFormulaArray[] = rtrim($ivalue);
							}//end of if
						}//end of foreach
					}//end of if

					//calculate source moduel fields
					if(isset($formData[$value.'_calculate_source_module_fields'])){
						foreach ($formData[$value.'_calculate_source_module_fields'] as $skey => $svalue) {
							if($formData[$value.'_calculate_conditions_deleted'][$skey] == 0){
								$sourceModuleCalculateFieldsArray[] = $svalue;
							}//end of if
						}//end of foreach
					}//end of if

					//insert calculate fields data
					if(!empty($functionNameArray) && !empty($inputFormulaArray) && !empty($sourceModuleCalculateFieldsArray)){
						$calculateFielId = create_guid();
						$calculateFieldMapping = array(
											'calculate_fields_id' => "'".$calculateFielId."'",
											'calculate_relate_field_module' => "'".$calculateRelatedFieldModule."'",
											'calculate_relate_field_name' => "'".$calculateRelatedFieldName."'",
											'function_name' => "'".json_encode($functionNameArray)."'",
											'input_formula' => "'".json_encode($inputFormulaArray)."'",
											'calculate_source_module_fields' => "'".json_encode($sourceModuleCalculateFieldsArray)."'",
											'autopopulatefields_id' => "'".$recordId."'",
											'deleted' => 0
										);
						insertAutoPopulateFieldsRecord('vi_autopopulate_calculate_fields',$calculateFieldMapping);
					}//end of if
				}//end of foreach
			}//end of if
		}else if(isset($_REQUEST['enableAutoPopulateConfig'])){
			if($_REQUEST['enableAutoPopulateConfig'] == 1){
				$status = 'Active';
			}else{
				$status = 'Inactive';
			}//end of else

			if(isset($_REQUEST['id'])){
				$id = $_REQUEST['id'];
				$updateStatus = array('status' => "'".$status."'");
				$where = array('autopopulatefields_id' => $id);
				echo updateAutoPopulateFieldsRecord('vi_autopopulatefields',$updateStatus,$where);
			}else if(isset($_REQUEST['recordId'])){
				$records = explode(',',$_REQUEST['recordId']);
				
				foreach ($records as $key => $value) {
					$updateStatus = array('status' => "'".$status."'");
					$where = array('autopopulatefields_id' => $value);
					echo updateAutoPopulateFieldsRecord('vi_autopopulatefields',$updateStatus,$where);
				}//end of foreach
			}//end of else if
		}//end of else if
	}//end of function
}//end of class
new VIAutoPopulateFieldsSaveConfiguration();
?>