<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
require_once('custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php');
class VIAutoPopulteFieldsFetchRelateModule{
    public function __construct(){
        $this->fetchRelateModule();
    } 
    public function fetchRelateModule(){
        $sourceModule = $_REQUEST['sourceModule']; //source mdoue

        //get realted fields
        $fieldList = getModuleRelatedFields($sourceModule);
        
        echo get_select_options_with_id($fieldList,'');
    }//end of function
}//end of class
new VIAutoPopulteFieldsFetchRelateModule();
?>