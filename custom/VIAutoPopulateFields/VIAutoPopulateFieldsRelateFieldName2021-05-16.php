<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php");
class VIAutoPopulateFieldsRelateFieldName{
    public function __construct(){
        $this->fetchRelateFieldName();
    } 
    public function fetchRelateFieldName() {
        //data
        $sourceModule = $_REQUEST['sourceModule'];

        $bean = BeanFactory::newBean($sourceModule);
        $fieldDefs = $bean->getFieldDefinitions();


        $where = array('module' => "'".$sourceModule."'",'status' => "'Active'",'deleted' => 0);
        $checkModuleConfig = getAutoPopulateFieldsRecord('vi_autopopulatefields',$fieldNames = array(),$where);
        $checkModuleConfigData = $GLOBALS['db']->fetchOne($checkModuleConfig);

        if(!empty($checkModuleConfigData)){
            $data = array();
            $autoPopulateFieldsId = $checkModuleConfigData['autopopulatefields_id'];
            
            //get field mapping data
            $where = array('autopopulatefields_id' => "'".$autoPopulateFieldsId."'",'deleted' => 0);
            $selMappingData = getAutoPopulateFieldsRecord('vi_autopopulatefields_mapping',$fieldNames = array(),$where);
            $selMappingDataResult = $GLOBALS['db']->query($selMappingData);

            while($selMappingDataRow = $GLOBALS['db']->fetchByAssoc($selMappingDataResult)){
                $relatedFieldName = $selMappingDataRow['related_field_name'];
                
                if(strpos($relatedFieldName, 'parent_type_') !== false){
                    $fieldData = $bean->field_defs['parent_name'];
                    $fieldName = $fieldData['name'];
                    $fieldNameId = $fieldData['id_name'];
                }else{
                    $fieldData = $bean->field_defs[$relatedFieldName];
                    $fieldName = $relatedFieldName;
                    $fieldNameId = $fieldData['id_name'];
                }//end of else 

                $relatedFieldModule = $selMappingDataRow['related_field_module'];
                $sourceModuleFields = json_decode(html_entity_decode($selMappingDataRow['source_module_fields']));
                $targetModuleFields = json_decode(html_entity_decode($selMappingDataRow['target_module_fields']));

                $data['field_mapping'][$relatedFieldName] = array('sourceModuleFields' => $sourceModuleFields, 'targetModuleFields' => $targetModuleFields, 'fieldName' => $fieldName, 'fieldNameId' => $fieldNameId,'relatedFieldModule' => $relatedFieldModule,'type' => 'fieldMapping');
            }//end of while

            //get calculate field data
            $where = array('autopopulatefields_id' => "'".$autoPopulateFieldsId."'",'deleted' => 0);
            $selCalculateFieldData = getAutoPopulateFieldsRecord('vi_autopopulate_calculate_fields',$fieldNames = array(),$where);
            $selCalculateFieldDataResult = $GLOBALS['db']->query($selCalculateFieldData);

            while($selCalculateFieldDataRow = $GLOBALS['db']->fetchByAssoc($selCalculateFieldDataResult)){
                $calculateRelatedFieldName = $selCalculateFieldDataRow['calculate_relate_field_name'];

                if(strpos($calculateRelatedFieldName, 'parent_type_') !== false){
                    $fieldData = $bean->field_defs['parent_name'];
                    $fieldName = $fieldData['name'];
                    $fieldNameId = $fieldData['id_name'];
                }else{
                    $fieldData = $bean->field_defs[$calculateRelatedFieldName];
                    $fieldName = $calculateRelatedFieldName;
                    $fieldNameId = $fieldData['id_name'];
                }//end of else 

                $calculateRelatedFieldModule = $selCalculateFieldDataRow['calculate_relate_field_module'];

                $functionName = json_decode(html_entity_decode($selCalculateFieldDataRow['function_name']));
                $inputFormula = json_decode(html_entity_decode($selCalculateFieldDataRow['input_formula']));
                $calculateSourceModuleFields = json_decode(html_entity_decode($selCalculateFieldDataRow['calculate_source_module_fields']));

                $data['calculate_fields'][$calculateRelatedFieldName] = array('functionName' => $functionName, 'inputFormula' => $inputFormula, 'calculateSourceModuleFields' => $calculateSourceModuleFields, 'fieldName' => $fieldName, 'fieldNameId' => $fieldNameId,'relatedFieldModule' => $calculateRelatedFieldModule,'type' => 'calculateFields');
            }//end of while

            echo json_encode($data);
        }//end of if
    } //end of function
}//end of class
new VIAutoPopulateFieldsRelateFieldName();
?>