<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php");
class VIAutoPopulateFieldsDeleteRecord{
	public function __construct(){
    	$this->deleteAutoPopulateFieldsRecords();
  	}//end of function

  	public function deleteAutoPopulateFieldsRecords(){
	  	if(isset($_POST['delId'])){
	        $delId = explode(',',$_POST['delId']);
	        foreach($delId as $id){
				//delete autopopupfields data
				$data = array('deleted' => 1);
				$where = array('autopopulatefields_id' => $id);

				updateAutoPopulateFieldsRecord('vi_autopopulatefields',$data,$where);
				
				//delete autopopulate field mapping data
				updateAutoPopulateFieldsRecord('vi_autopopulatefields_mapping',$data,$where);
				
				//delete autopopulate calculate field data
				updateAutoPopulateFieldsRecord('vi_autopopulate_calculate_fields',$data,$where);
			}//end of foreach
		}//end of if
	}//end of function
}//end of class
new VIAutoPopulateFieldsDeleteRecord();
?>	