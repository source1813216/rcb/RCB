<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
require_once("custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php");
class VIAutoPopulateFieldsRelateFieldValue{
	public function __construct(){
		$this->fetchAutoPopulateRelateFieldValue();
	} 
	public function fetchAutoPopulateRelateFieldValue(){
		//data
		$fieldType = $_REQUEST['fieldType'];
		$sourceModule = $_REQUEST['sourceModule'];
		$relateFieldIdValue = $_REQUEST['relateFieldIdValue'];
		$allConfigData = $_REQUEST['allConfigData'];
		
		$relatedModule = $allConfigData['relatedFieldModule'];

		//source module bean
		$sourceModuleBean = BeanFactory::newBean($sourceModule);
		$sourceModuleFieldDefs = $sourceModuleBean->getFieldDefinitions();

		//related module bean
		$relatedModuleBean = BeanFactory::getBean($relatedModule,$relateFieldIdValue);
		$relatedModuleFieldDefs = $relatedModuleBean->getFieldDefinitions();

		$autoPopulateFieldMappingValue = array();
		$autoPopulateCalculateFieldValue = array();
		
		if($allConfigData['type'] == 'fieldMapping'){
			$targetModuleFields = $allConfigData['targetModuleFields'];
			$sourceModuleFields = $allConfigData['sourceModuleFields'];

			$autoPopulateFieldMappingValue = getAutoPopulateFieldMappingValue($sourceModuleBean,$relatedModuleBean,$targetModuleFields,$sourceModuleFields);
		}else{
			$functionNames = $allConfigData['functionName'];
			$inputFormulas = $allConfigData['inputFormula'];
			$calculateSourceModuleFields = $allConfigData['calculateSourceModuleFields'];

			$autoPopulateCalculateFieldValue = getAutoPopulateCalculateFieldValue($functionNames,$inputFormulas,$calculateSourceModuleFields,$relatedModuleBean,$relatedModuleFieldDefs,$sourceModuleBean);
		}//end of else

		$data = array('autoPopulateFieldMappingValue' => $autoPopulateFieldMappingValue, 'autoPopulateCalculateFieldValue' => $autoPopulateCalculateFieldValue);

		echo json_encode($data);
	}//end of function
}//end of class
new VIAutoPopulateFieldsRelateFieldValue();
?>