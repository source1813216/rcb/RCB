<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
require_once("custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php");
class VIAutoPopulateFieldsCheckDuplicateConfiguration{
    public function __construct(){
        $this->checkDuplicateModuleConfiguration();
    }//end of function

    public function checkDuplicateModuleConfiguration() {
        $sourceModule = $_REQUEST['sourceModule']; //source module
        
        //check duplicate record
        $where = array('module' => "'".$sourceModule."'", 'deleted' => 0);
        $selAutoPopulateFieldData = getAutoPopulateFieldsRecord('vi_autopopulatefields',$fieldNames = array(),$where);
        $selAutoPopulateFieldDataRow = $GLOBALS['db']->fetchOne($selAutoPopulateFieldData);

        if(!empty($selAutoPopulateFieldDataRow)){
            echo "1";
        }else{
            echo "0";
        }//end of else
    }//end of function
}//end of class
new VIAutoPopulateFieldsCheckDuplicateConfiguration();
?>