<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
require_once('custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php');
class VIAutoPopulateFieldsSourceTargetModuleFields{
    public function __construct(){
        $this->getSourceTargetModuleFields();
    }//end of function 

    public function getSourceTargetModuleFields() {
        $sourceModule = $_REQUEST['sourceModule']; // source module
        $targetModule = $_REQUEST['targetModule']; // target module

        //source module fields
        $sourceModuleAddressFields  = getAddressFieldsList($sourceModule);
        $sourceModuleEditviewFields = getAutoPopulateEditDetailViewFields('editview',$sourceModule);
        $sourceModuleDetailViewFields = getAutoPopulateEditDetailViewFields('detailview',$sourceModule);

        $value = '';
        $sourceModuleFieldsMerge = array_merge($sourceModuleEditviewFields,$sourceModuleAddressFields,$sourceModuleDetailViewFields); //merge array
        asort($sourceModuleFieldsMerge);
        $sourceModuleFieldsList = get_select_options_with_id($sourceModuleFieldsMerge,'');


        //target module fields
        $targetModuleAddressFields  = getAddressFieldsList($targetModule);
        $targetModuleEditviewFields = getAutoPopulateEditDetailViewFields('editview',$targetModule);
        $targetModuleDetailViewFields = getAutoPopulateEditDetailViewFields('detailview',$targetModule);

        $value = '';
        $targetModuleFieldsMerge = array_merge($targetModuleEditviewFields,$targetModuleAddressFields,$targetModuleDetailViewFields); //merge array
        asort($targetModuleFieldsMerge);
        $targetModuleFieldsList = get_select_options_with_id($targetModuleFieldsMerge,'');

        $functionsList = getAllFunctionsList();

        echo json_encode(array('sourceModuleFieldsList' => $sourceModuleFieldsList,'targetModuleFieldsList' => $targetModuleFieldsList,'functionsList' => $functionsList));
    }//end of function
}//end of class
new VIAutoPopulateFieldsSourceTargetModuleFields();
?>

