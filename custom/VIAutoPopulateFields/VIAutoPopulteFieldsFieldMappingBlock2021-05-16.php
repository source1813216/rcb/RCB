<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
require_once('custom/VIAutoPopulateFields/VIAutoPopulateFieldsFunctions.php');
class VIAutoPopulteFieldsFieldMappingBlock{
    public function __construct(){
        $this->fetchFieldMappingBlockContent();
    } 
    public function fetchFieldMappingBlockContent(){
        $sourceModule = $_REQUEST['sourceModule']; //spurce module
        $blockType = $_REQUEST['blockType']; // block type

        if($blockType == 'fieldMapping'){
            $relatedFieldName = $_REQUEST['relatedFieldName']; //related field name
            $relatedFieldLabel = $_REQUEST['relatedFieldLabel']; //related field label

            $fieldBlockContent = addFieldMappingBlock($sourceModule,$relatedFieldName,$relatedFieldLabel);
        }else{
            $calculateRelatedFieldName = $_REQUEST['calculateRelatedFieldName']; //calculate related field name
            $calculateRelatedFieldLabel = $_REQUEST['calculateRelatedFieldLabel']; //calculate related field label

            $fieldBlockContent = addCalculateFieldBlock($sourceModule,$calculateRelatedFieldName,$calculateRelatedFieldLabel);
        }//end of else
        echo $fieldBlockContent;
    }//end of function
}//end of class
new VIAutoPopulteFieldsFieldMappingBlock();
?>