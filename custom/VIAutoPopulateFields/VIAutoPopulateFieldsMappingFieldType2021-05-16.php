<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
class VIAutoPopulateFieldsMappingFieldType{
    public function __construct(){
        $this->checkMappingFieldType();
    }//end of function 

    public function checkMappingFieldType() {
        //data
        $sourceModuleField = $_REQUEST['sourceModuleField'];
        $targetModuleField = $_REQUEST['targetModuleField'];
        $sourceModuleName = $_REQUEST['sourceModuleName'];
        $relateFieldModule = $_REQUEST['relateFieldModule'];

        if($sourceModuleField != '' && $targetModuleField != ''){
            //source module bean
            $sourceModuleBean = BeanFactory::newBean($sourceModuleName);
            $sourceModuleFieldData = $sourceModuleBean->field_defs[$sourceModuleField];

            //target module bean
            $targetModuleBean = BeanFactory::newBean($relateFieldModule);
            $targetModuleFieldData = $targetModuleBean->field_defs[$targetModuleField];

            //check field type
            if($sourceModuleFieldData['type'] != $targetModuleFieldData['type']){
                if($sourceModuleFieldData['type'] == "name" && $targetModuleFieldData['type'] == "varchar"){
                    echo 1; //true
                }else if($sourceModuleFieldData['type'] == "varchar" && $targetModuleFieldData['type'] == "name"){
                    echo 1; //true
                }else{
                    echo 0; //false
                }//end of else       
            }else{
                if($sourceModuleFieldData['type'] == 'relate'){
                    if($sourceModuleFieldData['module'] == $targetModuleFieldData['module']){
                        echo 1; //true  
                    }else{
                        echo 0;
                    }//end of else
                }//end of if
            }//end of else
        }//end of if
    }//end of function
}//end of class
new VIAutoPopulateFieldsMappingFieldType();
?>