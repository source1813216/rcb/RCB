<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
class VIAutoPopulateFieldsCheckCalculateFieldType{
    public function __construct(){
        $this->checkCalculateFieldType();
    }//end of function 
    public function checkCalculateFieldType() {
        //data
        $targetModule = $_REQUEST['targetModule'];
        $targetModuleField = $_REQUEST['targetModuleField'];
        $functionName = $_REQUEST['functionName'];
        
        $bean = BeanFactory::newBean($targetModule);
        
        if($targetModule != '' && $targetModuleField != ''){
            $fieldData = $bean->field_defs[$targetModuleField];
            
            $fieldType = $fieldData['type'];

            //function array
            $numericOperations = array('Add','Subtract','Multiply','Divide','Logarithm','Ln','Absolute','Average','Power','Percentage','Mod','Minimum','Negate','Floor','Ceil');
            $stringOperations = array('String Length','Concatenation');
            $dateOperations = array('Date Difference','Add Hours','Add Day','Add Week','Add Month','Add Year','Sub Hours','Sub Days','Sub Week','Sub Month','Sub Year','Diff Days','Diff Hour','Diff Minute','Diff Month','Week Diff','Diff Year');

            $response = '';
            if(in_array($functionName,$numericOperations)){
                if($fieldType == 'int' || $fieldType == 'Decimal' || $fieldType == 'float' || $fieldType == 'currency'){
                    $response = 1;
                }else{
                    $response = translate('LBL_NUMERIC_FIELD_VALIDATION' ,'Administration');
                }//end of else
            }else if(in_array($functionName,$stringOperations)){
                if($fieldType == 'dynamicenum' || $fieldType == 'enum' || $fieldType == 'multienum' || $fieldType == "radioenum" || $fieldType == 'phone' || $fieldType == 'text' || $fieldType == 'url' || $fieldType == 'varchar' || $fieldType == "name"){
                    $response = 1;
                }else{
                    $response = translate('LBL_STRING_FIELD_VALIDATION' ,'Administration');
                }//end of else
            }else if(in_array($functionName,$dateOperations)){
                if($fieldType == 'date' || $fieldType == 'datetimecombo' || $fieldType == 'datetime'){
                    $response = 1;
                }else{
                    $response = translate('LBL_DATE_FIELD_VALIDATION' ,'Administration');
                }//end of else
            }//end of else if

            echo $response;
        }//end of if
        
    }//end of function
}//end of class
new VIAutoPopulateFieldsCheckCalculateFieldType();
?>