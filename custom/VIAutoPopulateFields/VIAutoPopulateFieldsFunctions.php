<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
* This file is part of package Auto Populate Fields.
* 
* Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
* All rights (c) 2020 by Variance InfoTech PVT LTD
*
* This Version of Auto Populate Fields is licensed software and may only be used in 
* alignment with the License Agreement received with this Software.
* This Software is copyrighted and may not be further distributed without
* written consent of Variance InfoTech PVT LTD
* 
* You can contact via email at info@varianceinfotech.com
* 
********************************************************************************/
//insert record
function insertAutoPopulateFieldsRecord($tabelName,$fielddata){
    //data key
    $key = array_keys($fielddata);
    $fieldName = implode(",",$key);
    
    //data val
    $val = array_values($fielddata);
    $fieldVal = implode(",",$val);
    
    //insert
    $insertAutoPopulateFieldsData = "INSERT INTO $tabelName ($fieldName) VALUES($fieldVal)";
    $insertAutoPopulateFieldsDataResult = $GLOBALS['db']->query($insertAutoPopulateFieldsData);

    return $insertAutoPopulateFieldsDataResult;
}//end of function

//update record
function updateAutoPopulateFieldsRecord($tabelName,$data,$where){
    //update
    $updateAutoPopulateFieldsData = "UPDATE $tabelName SET";

    $fieldName = array_keys($data); //database field name
    $fieldValue = array_values($data); //field value

    $whereFieldName = array_keys($where); //where condition field name
    $whereFieldValue = array_values($where); // where condition field value

    $i=0;
    $count = count($data);
    foreach($data as $fieldData){
        if($count == $i+1){
            $updateAutoPopulateFieldsData .= " $fieldName[$i]=$fieldValue[$i]";
        }else{
            $updateAutoPopulateFieldsData .= " $fieldName[$i]=$fieldValue[$i],";
        }//end of else
        $i++;
    }//end of foreach

    $j=0;
    $updateAutoPopulateFieldsData .= " where";
    foreach($where as $whereConditionData){
        if($j == 0){
            $updateAutoPopulateFieldsData .=" $whereFieldName[$j]='$whereFieldValue[$j]'";
        }else{
            $updateAutoPopulateFieldsData .=" and $whereFieldName[$j]='$whereFieldValue[$j]'";
        }//end of else 
        $j++;
    }//end of foreach
    $updateAutoPopulateFieldsDataResult = $GLOBALS['db']->query($updateAutoPopulateFieldsData);

    return $updateAutoPopulateFieldsDataResult;
}//end of function

//get record
function getAutoPopulateFieldsRecord($tableName,$fieldNames,$where){
    $selectAutoPopulateFieldsData = '';
    //select
    $selectAutoPopulateFieldsData .= "SELECT ";
    if(!empty($fieldNames)){
        foreach($fieldNames as $key => $value){
            if($key == 0){
                $selectAutoPopulateFieldsData .= $value;
            }else{
                $selectAutoPopulateFieldsData .= ",".$value;
            }//end of else
        }//end of foreach
    }else{
        $selectAutoPopulateFieldsData .= '*';
    }//end of else
    

    $selectAutoPopulateFieldsData .= " FROM $tableName"; 
    $whereFieldName = array_keys($where); //where condition field name
    $whereFieldValue = array_values($where); // where condition field value

    $j=0;
    if(!empty($where)){
        $selectAutoPopulateFieldsData .= " WHERE";
    }
    $count = count($where);
    foreach($where as $key => $w){
        $fieldName = $whereFieldName[$j];
        $fieldValue = $whereFieldValue[$j];
        if($count > 1 && $j >= 1){
            $selectAutoPopulateFieldsData .=" AND $fieldName=$fieldValue";
        }else{
            $selectAutoPopulateFieldsData .=" $fieldName=$fieldValue";
        }
        $j++;
    }//end of foreach
    return $selectAutoPopulateFieldsData;
}//end of function

//autopopulate fields config data
function getAllModuleAutoPopulateFieldsConfigData(){
    //get configuration data
    $autoPopulateConfigData = array();
    
    $where = array('deleted' => 0);
    $selAutoPopulateFieldsData = getAutoPopulateFieldsRecord('vi_autopopulatefields',$fieldNames = array(),$where);
    $selAutoPopulateFieldsDataResult = $GLOBALS['db']->query($selAutoPopulateFieldsData);

    while($selAutoPopulateFieldsRow = $GLOBALS['db']->fetchByAssoc($selAutoPopulateFieldsDataResult)){
        $autoPopulateFieldsId = $selAutoPopulateFieldsRow['autopopulatefields_id'];
        $sourceModule = $selAutoPopulateFieldsRow['module'];

        $moduleBean = BeanFactory::newBean($sourceModule);
        
        //field mapping fields
        $fieldMappingRelateFieldList = array();

        $where = array('autopopulatefields_id' => "'".$autoPopulateFieldsId."'", 'deleted' => 0);
        $selFieldMappingData = getAutoPopulateFieldsRecord('vi_autopopulatefields_mapping',$fieldNames = array(),$where);
        $selFieldMappingDataResult = $GLOBALS['db']->query($selFieldMappingData);
        while($selFieldMappingRow = $GLOBALS['db']->fetchByAssoc($selFieldMappingDataResult)){
            $relatedFieldName = $selFieldMappingRow['related_field_name'];

            $fieldMappingRelateFieldList[] = getModuleRelatedFieldLabel($relatedFieldName,$moduleBean,$sourceModule);
        }//end of while

        //calculate fields
        $calculateFieldRelateFieldList = array();

        $where = array('autopopulatefields_id' => "'".$autoPopulateFieldsId."'", 'deleted' => 0);
        $selCalculateFieldData = getAutoPopulateFieldsRecord('vi_autopopulate_calculate_fields',$fieldNames = array(),$where);
        $selCalculateFieldDataResult = $GLOBALS['db']->query($selCalculateFieldData);

        while($selCalculateFieldDataRow = $GLOBALS['db']->fetchByAssoc($selCalculateFieldDataResult)){
            $calculateRelatedFieldName = $selCalculateFieldDataRow['calculate_relate_field_name'];

            $calculateFieldRelateFieldList[] = getModuleRelatedFieldLabel($calculateRelatedFieldName,$moduleBean,$sourceModule);
        }//end of while

        if($selAutoPopulateFieldsRow['status'] == 'Active'){
            $status = 1;
        }else{
            $status = 0;
        }//end of else
        $autoPopulateConfigData[$autoPopulateFieldsId] = array(
                                                        'moduleName' => translate($sourceModule), 
                                                        'status' => $status,
                                                        'fieldMappingRelateFieldList' => $fieldMappingRelateFieldList,
                                                        'calculateFieldRelateFieldList' => $calculateFieldRelateFieldList
                                                    );
    }//end of while

    return $autoPopulateConfigData;
}//end of function

//delete record
function delAutoPopulateFieldsRecord($tableName,$where){
    //delete
    $delAutoPopulateFieldsData = "DELETE FROM $tableName";

    $whereFieldName = array_keys($where); //where condition field name
    $whereFieldValue = array_values($where); // where condition field value
    
    $j=0;
    $delAutoPopulateFieldsData .= " where";
    foreach($where as $key => $w){
        if($key != 0){
            $delAutoPopulateFieldsData .= ' and';
        }
        $delAutoPopulateFieldsData .=" $whereFieldName[$j]='$whereFieldValue[$j]'";
        $j++;
    }//end of foreach
    $delAutoPopulateFieldsDataResult = $GLOBALS['db']->query($delAutoPopulateFieldsData);

    return $delAutoPopulateFieldsDataResult;
}//end of function

//get all module list
function getModuleList(){
    $controller = new TabController();
    $tabs = $controller->get_tabs_system();
    foreach ($tabs[0] as $key=>$value){
      $enabled[$key] = translate($key);
    }
    unset($enabled['Home']);
    unset($enabled['Calendar']);

    return $enabled;
}//end of function

//get editview detailview fields
function getAutoPopulateEditDetailViewFields($view,$module){
    require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
    $view_array = ParserFactory::getParser($view,$module);
    $panelArray = $view_array->_viewdefs['panels'];//editview panels
    $bean = BeanFactory::newBean($module);
    $field = $bean->getFieldDefinitions();

    //editview fields
    $editViewFieldArray = array();
    foreach ($panelArray as $key => $value) {
        foreach ($value as $keys => $values) {
            $editViewFieldArray[] = $values;
        }//end of foreach
    }//end of foreach

    $fieldTypeArray = array('varchar','name','phone','datetimecombo','int','datetime','date','currency','text','relate','radioenum','multienum','float','enum','dynamicenum','bool','url');
    $data = array('' => '--None--');
    foreach($editViewFieldArray as $key => $value) {
        foreach($value as $k => $v) {
            if(array_key_exists($v, $field)) {
                require_once('include/utils.php');
                $type = $field[$v]['type'];
                
                if(in_array($type,$fieldTypeArray)){
                    if($v == 'flow_run_on'){
                        $fieldLabel = translate('LBL_FLOW_RUN_ON',$module);
                    }else{
                        $fieldLabel = translate($field[$v]['vname'],$module);
                    }//end of else

                    if(strpos($fieldLabel, ': ')){
                        $fieldLabel = substr_replace($fieldLabel, "", -2);
                    }else if(strpos($fieldLabel, ':')){
                        $fieldLabel = substr_replace($fieldLabel, "", -1);
                    }//end of else
          
                    $fieldName = $v;
                    $data[$fieldName] = $fieldLabel;
                }//end of if
            }//end of if 
        }//end of foreach
    }//end of foreach

    //unset fields
    $unsetData = array('sample','insert_fields','update_text','case_update_form','aop_case_updates_threaded','internal','survey_questions_display','line_items','email1','email2','suggestion_box','filename','product_image','configurationGUI','invite_templates','action_lines','condition_lines','survey_url_display','reminders','related_doc_name','related_doc_rev_number','revision','last_run','schedule','email_recipients','date_entered','date_modified');
    foreach ($unsetData as $key => $value) {
        unset($data[$value]);
    }//end of foreach

    if($module == 'jjwg_Maps' || $module == 'Notes' || $module == 'Tasks'){
        unset($data['parent_name']);
    }//end of if

    if($module == "Cases"){
        unset($data['description']);
    }//end of if
    return $data;
}//end of function

//get address fields
function getAddressFieldsList($moduleName){
    require_once('include/utils.php');
    $bean = BeanFactory::newBean($moduleName);
    $field = $bean->getFieldDefinitions();

    $addressFieldData = array();
    foreach($field as $value){
        if($value['type'] == 'varchar'){
            if(isset($value['group'])){
                $fieldName = $value['name'];
                $fieldLabel = translate($value['vname'],$moduleName);
                if(strpos($fieldLabel, ':')){
                    $fieldLabel = substr_replace($fieldLabel, "", -1);
                }//end of if
                $addressFieldData[$fieldName] = $fieldLabel;
            }//end of if
        }//end of if
    }//end of foreach

    unset($addressFieldData['email1']);
    unset($addressFieldData['email2']);

    return $addressFieldData;
}//end of function

//get module related fields
function getModuleRelatedFields($sourceModule){
    global $app_list_strings;
    $editFields = array();
    $editFieldArray = array('' => '--None--');
  
    //primary module bean
    $bean = BeanFactory::newBean($sourceModule);
    $fieldDefs = $bean->getFieldDefinitions();

    //editview
    require_once("modules/ModuleBuilder/parsers/ParserFactory.php");
    $editviewArray = ParserFactory::getParser('editview',$sourceModule);
    $editviewPanels = $editviewArray->_viewdefs['panels'];

    //editview fields
    foreach ($editviewPanels as $key => $value) {
        foreach($value as $k => $v){
            $editFields[] = $v;
        }//end of foreach
    }//end of foreach

    foreach($editFields as $key => $value) {
        foreach($value as $k => $v){
            if(array_key_exists($v, $fieldDefs)){
                $fieldType = $fieldDefs[$v]['type'];
                if($fieldType == "relate"){
                    $moduleName = $fieldDefs[$v]['module'];
                    if($moduleName != 'Users'){
                        if($moduleName != 'Documents' && $sourceModule != 'Documents'){
                            $fieldLabel = translate($fieldDefs[$v]['vname'],$sourceModule);
                            
                            $lastChar = substr($fieldLabel, -1);
                            if($lastChar == ':'){
                                $fieldLabel = substr_replace($fieldLabel, "", -1);
                            }//end of if

                            $editFieldArray[$fieldDefs[$v]['name']] = $fieldLabel.'('.$fieldDefs[$v]['name'].')';
                        }//end of if
                    }//end of if
                }else if($fieldType == 'parent'){
                    $fieldLabel = translate($fieldDefs['parent_name']['vname'],$sourceModule);
                    
                    $lastChar = substr($fieldLabel, -1);
                    if($lastChar == ':'){
                        $fieldLabel = substr_replace($fieldLabel, "", -1);
                    }//end of if

                    $domName = $fieldDefs['parent_name']['options'];
                    $moduleList = $app_list_strings[$domName];
                    foreach ($moduleList as $mkey => $mvalue) {
                        $editFieldArray['parent_type_'.$mkey] = translate($mkey).'('.$fieldLabel.' - parent_name)';
                    }//enf of foreach
                }//end of else if
            }//end of if
        }//end of foreach
    }//end of foreach

    return $editFieldArray;
}//end of function

function getModuleRelatedFieldLabel($relatedFieldName,$moduleBean,$sourceModule){
    $allRelatedFields = getModuleRelatedFields($sourceModule);

    $fieldLabel = '';
    if(array_key_exists($relatedFieldName, $allRelatedFields)){
        if(strpos($relatedFieldName, 'parent_type_') !== false){
            $fieldData = $moduleBean->getFieldDefinitions();
            
            $fieldLabel = translate($fieldData['parent_name']['vname'],$sourceModule);
            
            $lastChar = substr($fieldLabel, -1);
            if($lastChar == ':'){
                $fieldLabel = substr_replace($fieldLabel, "", -1);
            }//end of if

            $moduleFromParentType = explode('parent_type_',$relatedFieldName);
            
            $fieldLabel = translate($moduleFromParentType[1]).'('.$fieldLabel.' - parent_name)';
        }else{
            $fieldData = $moduleBean->field_defs[$relatedFieldName];
            $fieldLabel = translate($fieldData['vname'],$sourceModule);

            if(strpos($fieldLabel, ':')){
                $fieldLabel = substr_replace($fieldLabel, "", -1);
            }//end of if

            $fieldLabel = $fieldLabel.'('.$fieldData['name'].')';
        }//end of else
    }//end of if
    return $fieldLabel;
}//end of function

//get field mapping block html
function addFieldMappingBlock($sourceModule,$relatedField,$relatedFieldLabel){
    $html = '';
    $spanId = $relatedField.'_lines_span';
    $tabelId = $relatedField.'_conditionLines';

    $bean = BeanFactory::newBean($sourceModule);

    if(strpos($relatedField, 'parent_type_') !== false){
        $moduleFromParentType = explode('parent_type_',$relatedField);
        $fieldDataModule = $moduleFromParentType[1];
        $relatedFieldModuleName = "'".$moduleFromParentType[1]."'";
        $relatedFieldModuleNameLabel = "'".translate($moduleFromParentType[1])."'";
    }else{
        $fieldData = $bean->field_defs[$relatedField];
        $fieldDataModule = $fieldData['module'];
        $relatedFieldModuleName = "'".$fieldData['module']."'";
        $relatedFieldModuleNameLabel = "'".translate($fieldData['module'])."'";
    }//end of else
    
    $btnLabel = translate('LBL_ADD_FIELD_MAPPING_BUTTON' ,'Administration');

    $sourceModuleName = "'".$sourceModule."'"; 
    $sourceModuleLabel = "'".translate($sourceModule)."'";

    $relatedFieldName = "'".$relatedField."'";
    $relatedFieldNameLabel = "'".$relatedFieldLabel."'";
    
    $html .= '<div style="border:1px solid #ddd;padding-left:10px;"><br><table width="100%" border="0" cellspacing="10" cellpadding="0" class="tblFieldMapping">
        <tbody>
          <tr>
            <th colspan="4">
            <h4 class="header-4"><b></b>'.$relatedFieldLabel.' '.translate('LBL_RELATED_FIELD','Administration').'</h4></th>
          </tr>
        </tbody>';

    $html .= '<input type="hidden" name="related_field_module[]" value="'.$fieldDataModule.'">';
    $html .= '<input type="hidden" name="related_field_name[]" value="'.$relatedField.'">';

    $html .= '</table>';

    $sourceModuleFieldVal = $targetModuleFieldVal = "''";

    $html .= '<br><span id="'.$spanId.'" class="conditionLinespan"><table id="'.$tabelId.'" width="100%" cellspacing="4" border="0" class="conditionLines"></table>
                <div style="padding-top: 10px; padding-bottom:10px;"><input tabindex="116" class="button" value="'.$btnLabel.'" onclick="insertFieldMappingBlock('.$sourceModuleName.','.$sourceModuleLabel.','.$relatedFieldName.','.$relatedFieldNameLabel.','.$relatedFieldModuleName.','.$relatedFieldModuleNameLabel.','.$sourceModuleFieldVal.','.$targetModuleFieldVal.')" type="button"></div></span></div><br>';
    return $html;
}//end of function

//get calculate field block html
function addCalculateFieldBlock($sourceModule,$calculateRelatedField,$calculateRelatedFieldLabel){
    $html = '';
    $spanId = $calculateRelatedField.'_calculate_lines_span';
    $tabelId = $calculateRelatedField.'_calculate_conditionLines';

    $bean = BeanFactory::newBean($sourceModule);

    if(strpos($calculateRelatedField, 'parent_type_') !== false){
        $moduleFromParentType = explode('parent_type_',$calculateRelatedField);
        $fieldDataModule = $moduleFromParentType[1];
        $calculateRelatedFieldModuleName = "'".$moduleFromParentType[1]."'";
        $calculateRelatedFieldModuleNameLabel = "'".translate($moduleFromParentType[1])."'";
    }else{
        $fieldData = $bean->field_defs[$calculateRelatedField];
        $fieldDataModule = $fieldData['module'];
        $calculateRelatedFieldModuleName = "'".$fieldData['module']."'";
        $calculateRelatedFieldModuleNameLabel = "'".translate($fieldData['module'])."'";
    }//end of else

    $btnLabel = translate('LBL_ADD_CALCULATE_FIELD_BUTTON' ,'Administration');

    $sourceModuleName = "'".$sourceModule."'"; 
    $sourceModuleLabel = "'".translate($sourceModule)."'";

    $calculateRelatedFieldName = "'".$calculateRelatedField."'";
    $calculateRelatedFieldNameLabel = "'".$calculateRelatedFieldLabel."'";
    
    $html .= '<div style="border:1px solid #ddd;padding-left:10px;"><br><table width="100%" border="0" cellspacing="10" cellpadding="0" class="tblCalculateField">
        <tbody>
          <tr>
            <th colspan="4">
            <h4 class="header-4"><b></b>'.$calculateRelatedFieldLabel.' '.translate('LBL_RELATED_FIELD','Administration').'</h4></th>
          </tr>
        </tbody>';

    $html .= '<input type="hidden" name="calculate_related_field_module[]" value="'.$fieldDataModule.'">';
    $html .= '<input type="hidden" name="calculate_related_field_name[]" value="'.$calculateRelatedField.'">';

    $html .= '</table>';

    $sourceModuleFieldVal = $functionNameVal = $inputFormulaVal =  "''";

    $html .= '<br><span id="'.$spanId.'" class="calculateConditionLinespan"><table id="'.$tabelId.'" width="100%" cellspacing="4" border="0" class="calculateConditionLines"></table>
                <div style="padding-top: 10px; padding-bottom:10px;"><input tabindex="116" class="button" value="'.$btnLabel.'" onclick="insertCalculateFieldBlock('.$sourceModuleName.','.$sourceModuleLabel.','.$calculateRelatedFieldName.','.$calculateRelatedFieldNameLabel.','.$calculateRelatedFieldModuleName.','.$calculateRelatedFieldModuleNameLabel.','.$sourceModuleFieldVal.','.$functionNameVal.','.$inputFormulaVal.')" type="button"></div></span></div><br>';
    return $html;
}//end of function

//get function list
function getAllFunctionsList(){
  $functionListArray = array('' => '--None--',
                            'Add' => translate('LBL_FUNCTION_ADD','Administration'),
                           'Subtract' => translate('LBL_FUNCTION_SUBTRACT','Administration'),
                           'Multiply' => translate('LBL_FUNCTION_MULTIPLY','Administration'),
                           'Divide' => translate('LBL_FUNCTION_DIVIDE','Administration'),
                           'String Length' => translate('LBL_FUNCTION_STRING_LENGTH','Administration'),
                           'Concatenation'=> translate('LBL_FUNCTION_CONCAT','Administration'),
                           'Logarithm' => translate('LBL_FUNCTION_LOGARITHM','Administration'),
                           'Ln' => translate('LBL_LN','Administration'),
                           'Absolute' => translate('LBL_ABSOLUTE','Administration'),
                           'Average' => translate('LBL_FUNCTION_AVERAGE','Administration'),
                           'Power' => translate('LBL_FUNCTION_POWER','Administration'),
                           'Date Difference' => translate('LBL_FUNCTION_DATE_DIFF','Administration'),
                           'Percentage' => translate('LBL_FUNCTION_PERCENTAGE','Administration'),
                           'Mod' => translate('LBL_FUNCTION_MOD','Administration'),
                           'Minimum' => translate('LBL_FUNCTION_MINIMUM','Administration'),
                           'Negate' => translate('LBL_FUNCTION_NEGATE','Administration'),
                           'Floor' => translate('LBL_FUNCTION_FLOOR','Administration'),  
                           'Ceil' => translate('LBL_FUNCTION_CEIL','Administration'),
                           'Add Hours' => translate('LBL_FUNCTION_ADD_HOURS','Administration'),
                           'Add Day' => translate('LBL_FUNCTION_ADD_DAY','Administration'),
                           'Add Week' => translate('LBL_FUNCTION_ADD_WEEK','Administration'),
                           'Add Month' => translate('LBL_FUNCTION_ADD_MONTH','Administration'),
                           'Add Year' => translate('LBL_FUNCTION_ADD_YEAR','Administration'),
                           'Sub Hours' => translate('LBL_FUNCTION_SUB_HOURS','Administration'),
                           'Sub Days' => translate('LBL_FUNCTION_SUB_DAYS','Administration'),
                           'Sub Week' => translate('LBL_FUNCTION_SUB_WEEK','Administration'),
                           'Sub Month' => translate('LBL_FUNCTION_SUB_MONTH','Administration'),
                           'Sub Year' => translate('LBL_FUNCTION_SUB_YEAR','Administration'),
                           'Diff Days' => translate('LBL_FUNCTION_DIFF_DAYS','Administration'),
                           'Diff Hour' => translate('LBL_FUNCTION_DIFF_HOUR','Administration'),
                           'Diff Minute' => translate('LBL_FUNCTION_DIFF_MINUTE','Administration'),
                           'Diff Month' => translate('LBL_FUNCTION_DIFF_MONTH','Administration'),
                           'Week Diff' => translate('LBL_FUNCTION_DIFF_WEEK','Administration'),
                           'Diff Year' => translate('LBL_FUNCTION_DIFF_YEAR','Administration'),
                        );
  return get_select_options_with_id($functionListArray,'');
}//end of function

//field mapping html
function getAutoPopulateFieldsFieldMappingContent($recordId,$sourceModule){
    $fieldMappingContent = '';
    $moduleBean = BeanFactory::newBean($sourceModule);

    $where = array('autopopulatefields_id' => "'".$recordId."'",'deleted' => 0);
    $selFieldMappingData = getAutoPopulateFieldsRecord('vi_autopopulatefields_mapping',$fieldNames = array(),$where);
    $selFieldMappingDataResult = $GLOBALS['db']->query($selFieldMappingData);

    while($selFieldMappingRow = $GLOBALS['db']->fetchByAssoc($selFieldMappingDataResult)){
        $relatedFieldName = $selFieldMappingRow['related_field_name'];
        $fieldLabel = getModuleRelatedFieldLabel($relatedFieldName,$moduleBean,$sourceModule);
        if($fieldLabel != ''){
            $fieldMappingContent .= addFieldMappingBlock($sourceModule,$relatedFieldName,$fieldLabel);
            $fieldMappingContent .= '<script>';
          
            $sourceModuleFields = json_decode(html_entity_decode($selFieldMappingRow['source_module_fields']));
            $targetModuleFields = json_decode(html_entity_decode($selFieldMappingRow['target_module_fields']));

            foreach ($sourceModuleFields as $key => $value) {
                $data = array(
                              'autopopulatefields_id' => $selFieldMappingRow['autopopulatefields_id'],
                              'related_field_module' => $selFieldMappingRow['related_field_module'],
                              'related_field_name' => $selFieldMappingRow['related_field_name'],
                              'source_module_fields' => $value,
                              'target_module_fields' => $targetModuleFields[$key],
                              'source_module' => $sourceModule,
                              'sourceModuleLabel' => translate($sourceModule),
                              'relatedFieldLabel' => $fieldLabel,
                              'relatedModuleNameLabel' => translate($selFieldMappingRow['related_field_module'])
                        );
                $condition_item = json_encode($data);
                $fieldMappingContent .= "loadFieldMappingLine(".$condition_item.");";
            }//end of foreach

            $fieldMappingContent .= '</script>';
        }//end of if
    }//end of while

  return $fieldMappingContent;
}//end of function

//calculate field html
function getAutoPopulateFieldsCalculateFieldContent($recordId,$sourceModule){
    $allRelatedFields = getModuleRelatedFields($sourceModule);
    $calculateFieldContent = '';
    $moduleBean = BeanFactory::newBean($sourceModule);

    $where = array('autopopulatefields_id' => "'".$recordId."'",'deleted' => 0);
    $selCalculateFieldData = getAutoPopulateFieldsRecord('vi_autopopulate_calculate_fields',$fieldNames = array(),$where);
    $selCalculateFieldDataResult = $GLOBALS['db']->query($selCalculateFieldData);

    while($selCalculateFieldDataRow = $GLOBALS['db']->fetchByAssoc($selCalculateFieldDataResult)){
        $calculateRelatedFieldName = $selCalculateFieldDataRow['calculate_relate_field_name'];
        $fieldLabel = getModuleRelatedFieldLabel($calculateRelatedFieldName,$moduleBean,$sourceModule);
        if($fieldLabel != ''){
            $calculateFieldContent .= addCalculateFieldBlock($sourceModule,$calculateRelatedFieldName,$fieldLabel);

            $calculateFieldContent .= '<script>';
            $functionName = json_decode(html_entity_decode($selCalculateFieldDataRow['function_name']));
            $inputFormula = json_decode(html_entity_decode($selCalculateFieldDataRow['input_formula']));
            $calculateSourceModuleFields = json_decode(html_entity_decode($selCalculateFieldDataRow['calculate_source_module_fields']));

            foreach ($functionName as $key => $value) {
                $data = array(
                          'autopopulatefields_id' => $selCalculateFieldDataRow['autopopulatefields_id'],
                          'calculate_related_field_module' => $selCalculateFieldDataRow['calculate_relate_field_module'],
                          'calculate_related_field_name' => $selCalculateFieldDataRow['calculate_relate_field_name'],
                          'function_name' => $value,
                          'input_formula' => $inputFormula[$key],
                          'calculate_source_module_fields' => $calculateSourceModuleFields[$key],
                            'source_module' => $sourceModule,
                          'sourceModuleLabel' => translate($sourceModule),
                          'calculateRelatedFieldLabel' => $fieldLabel,
                          'calculateRelatedModuleNameLabel' => translate($selCalculateFieldDataRow['calculate_relate_field_module'])
                      );
                $condition_item = json_encode($data);
                $calculateFieldContent .= "loadCalculateFieldLine(".$condition_item.");";
            }//end of foreach

            $calculateFieldContent .= '</script>';
        }//end of if
    }//end of while

    return $calculateFieldContent;
}//end of function

//get field mapping array
function getAutoPopulateFieldMappingValue($sourceModuleBean,$relatedModuleBean,$targetModuleFields,$sourceModuleFields){
    $autoPopulateFieldMappingValue = array();
    foreach ($targetModuleFields as $tkey => $tvalue) {
        $fieldData = $relatedModuleBean->field_defs[$tvalue];
        if($fieldData['type'] == 'relate'){
            $idName = $fieldData['id_name'];
            $sourceFieldData = $sourceModuleBean->field_defs[$sourceModuleFields[$tkey]];
            $autoPopulateFieldMappingValue[] = array('autoPopulateFieldName' => $sourceModuleFields[$tkey],'autoPopulateFieldVal' => $relatedModuleBean->$tvalue,'autoPopulateFieldType' => $fieldData['type']);
            $autoPopulateFieldMappingValue[] = array('autoPopulateFieldName' => $sourceFieldData['id_name'],'autoPopulateFieldVal' => $relatedModuleBean->$idName,'autoPopulateFieldType' => $fieldData['type']);
        }else if($fieldData['type'] == 'multienum'){
            $autoPopulateFieldMappingValue[] = array('autoPopulateFieldName' => $sourceModuleFields[$tkey],'autoPopulateFieldVal' => unencodeMultienum($relatedModuleBean->$tvalue),'autoPopulateFieldType' => $fieldData['type']);
        }else if($fieldData['type'] == 'dynamicenum'){
            $autoPopulateFieldMappingValue[] = array('autoPopulateFieldName' => $sourceModuleFields[$tkey],'autoPopulateFieldVal' => $relatedModuleBean->$tvalue, 'autoPopulateFieldType' => $fieldData['type'], 'parentField' => $fieldData['parentenum']);
        }else{
            $autoPopulateFieldMappingValue[] = array('autoPopulateFieldName' => $sourceModuleFields[$tkey],'autoPopulateFieldVal' => $relatedModuleBean->$tvalue,'autoPopulateFieldType' => $fieldData['type']); 
        }//end of else 
    }//end of foreach

    return $autoPopulateFieldMappingValue;
}//end of function

//get calculate field array
function getAutoPopulateCalculateFieldValue($functionNames,$inputFormulas,$calculateSourceModuleFields,$relatedModuleBean,$relatedModuleFieldDefs,$sourceModuleBean){
    $autoPopulateCalculateFieldsValues = array();

    foreach ($calculateSourceModuleFields as $skey => $svalue) {
        $autoPopulateCalculateVal = '';
        $functionName = $functionNames[$skey];
        $inputFormula = $inputFormulas[$skey];

        $fieldData = $sourceModuleBean->field_defs[$svalue];

        preg_match('#\((.*?)\)#', $inputFormula, $match);
        
        $calculateFieldName = $match[1];
        $calculateFieldName = explode(',',$match[1]);

        $mathematicsFunctions = array('Add','Subtract','Multiply','Divide','Average','Minimum');
        $scientificMathFunctions = array('Logarithm','Ln','Absolute','Power','Date Difference','Percentage','Mod','Negate','Floor','Ceil');
        $addDateTimeFunctions = array('Add Hours','Add Day','Add Week','Add Month','Add Year');
        $subDateTimeFunctions = array('Sub Hours','Sub Days','Sub Week','Sub Month','Sub Year');
        $dateDiffTimeFunctions = array('Diff Days','Diff Hour','Diff Minute','Diff Month','Week Diff','Diff Year');

        if($functionName =='String Length'){
            $autoPopulateCalculateVal = autoPopulateStringLength($inputFormula,$relatedModuleBean,$relatedModuleFieldDefs);
        }else if($functionName == 'Concatenation'){
            $autoPopulateCalculateVal = autoPopulateStringContact($inputFormula,$relatedModuleBean,$relatedModuleFieldDefs);
        }else if(in_array($functionName, $mathematicsFunctions)){
            $calculateFieldValue = array();
            foreach ($calculateFieldName as $k => $v) {
                if(array_key_exists($v,$relatedModuleFieldDefs)){
                    $calculateFieldValue[] = $relatedModuleBean->$v;
                }else{
                    $v = str_replace('&quot;', '', $v);
                    $v = str_replace('&#039;', '', $v);
                    $calculateFieldValue[] = $v;
                }//end of else
            }//end of foreach

            if($functionName == 'Add'){
                $autoPopulateCalculateVal = autoPopulateFieldsAddFunction($calculateFieldValue);
            }else if($functionName == 'Subtract'){
                $autoPopulateCalculateVal = autoPopulateFieldsSubFunction($calculateFieldValue);
            }else if($functionName == 'Multiply'){
                $autoPopulateCalculateVal = autoPopulateFieldsMultiplyFunction($calculateFieldValue);
            }else if($functionName == 'Divide'){
                $autoPopulateCalculateVal = autoPopulateFieldsDivideFunction($calculateFieldValue);
            }else if($functionName == 'Average'){
                $autoPopulateCalculateVal = autoPopulateFieldsAverageFunction($calculateFieldValue);
            }else if($functionName == 'Minimum'){
                $autoPopulateCalculateVal = autoPopulateFieldsMinimumFunction($calculateFieldValue);
            }//end of else if
        }else if(in_array($functionName, $scientificMathFunctions) || in_array($functionName, $addDateTimeFunctions) || in_array($functionName, $subDateTimeFunctions) || in_array($functionName, $dateDiffTimeFunctions)){

            $flag = 0;
            
            if($functionName == 'Logarithm' || $functionName == 'Power' || $functionName == 'Date Difference' || $functionName == 'Percentage' || $functionName == 'Mod' || in_array($functionName, $addDateTimeFunctions) || in_array($functionName, $subDateTimeFunctions) || in_array($functionName, $dateDiffTimeFunctions)){
                if(count($calculateFieldName) == 2){
                    $flag = 1;
                }//end of if
            }else if($functionName == 'Ln' || $functionName == 'Absolute' || $functionName == 'Negate' || $functionName == 'Floor' || $functionName == 'Ceil'){
                if(count($calculateFieldName) == 1){
                    $flag = 1;
                }//end of if
            }//end of else if

            if($flag == 1){
                $calculateFieldValue = array();
                foreach ($calculateFieldName as $k => $v) {
                    if(array_key_exists($v,$relatedModuleFieldDefs)){
                        $calculateFieldValue[] = $relatedModuleBean->$v;
                    }else{
                        $v = str_replace('&quot;', '', $v);
                        $v = str_replace('&#039;', '', $v);
                        $calculateFieldValue[] = $v;
                    }//end of else
                }//end of foreach

                if($functionName == 'Logarithm'){
                    $autoPopulateCalculateVal = autoPopulateFieldsLogarithmFunction($calculateFieldValue);
                }else if($functionName == 'Ln'){
                    $autoPopulateCalculateVal = autoPopulateFieldsLnFunction($calculateFieldValue);
                }else if($functionName == 'Absolute'){
                    $autoPopulateCalculateVal = autoPopulateFieldsAbsoluteFunction($calculateFieldValue);
                }else if($functionName == 'Power'){
                    $autoPopulateCalculateVal = autoPopulateFieldsPowerFunction($calculateFieldValue);
                }else if($functionName == 'Date Difference'){
                    $autoPopulateCalculateVal = autoPopulateFieldsDateDiffFunction($calculateFieldValue);
                }else if($functionName == 'Percentage'){
                    $autoPopulateCalculateVal = autoPopulateFieldsPercentageFunction($calculateFieldValue);
                }else if($functionName == 'Mod'){
                    $autoPopulateCalculateVal = autoPopulateFieldsModuloFunction($calculateFieldValue);
                }else if($functionName == 'Negate'){
                    $autoPopulateCalculateVal = autoPopulateFieldsNegateFunction($calculateFieldValue);
                }else if($functionName == 'Floor'){
                    $autoPopulateCalculateVal = autoPopulateFieldsFloorFunction($calculateFieldValue);
                }else if($functionName == 'Ceil'){
                    $autoPopulateCalculateVal = autoPopulateFieldsCeilFunction($calculateFieldValue);
                }else if(in_array($functionName,$addDateTimeFunctions)){
                    $autoPopulateCalculateVal = autoPopulateFieldsAddDateTimeFunction($calculateFieldValue,$functionName);
                }else if(in_array($functionName,$subDateTimeFunctions)){
                    $autoPopulateCalculateVal = autoPopulateFieldsSubDateTimeFunction($calculateFieldValue,$functionName);
                }else if(in_array($functionName,$dateDiffTimeFunctions)){
                    $autoPopulateCalculateVal = autoPopulateFieldsDateDifferenceFunction($calculateFieldValue,$functionName);
                }//end of else if
            }//end of if
        }//end of else if

        $autoPopulateCalculateFieldsValues[] = array('autoPopulateFieldName' => $svalue,'autoPopulateFieldVal' => $autoPopulateCalculateVal,'autoPopulateFieldType' => $fieldData['type']); 
    }//end of foreach
    return $autoPopulateCalculateFieldsValues;
}//end of function

//calculate fields functions
function autoPopulateStringLength($inputFormula,$relatedModuleBean,$relatedModuleFieldDefs){
    $calculateFieldValue = '';
    preg_match('#\((.*?)\)#', $inputFormula, $match);
    $calculateFieldName = $match[1];

    if(array_key_exists($calculateFieldName,$relatedModuleFieldDefs)){
        $calculateFieldValue = $relatedModuleBean->$calculateFieldName;
    }else{
        $calculateFieldName = str_replace('&quot;', '', $calculateFieldName);
        $calculateFieldName = str_replace('&#039;', '', $calculateFieldName);
        $calculateFieldValue = $calculateFieldName;
    }//end of else

    return strlen($calculateFieldValue);
}//end of function

function autoPopulateStringContact($inputFormula,$relatedModuleBean,$relatedModuleFieldDefs){
    $inputFormula = str_replace("&#039;", '', $inputFormula);
    preg_match('#\((.*?)\)#', $inputFormula, $match);
    $calculateFieldName = explode(',',$match[1]);

    $calculateFieldValue = array();
    foreach ($calculateFieldName as $k => $v) {
        if($v == ' '){
            $calculateFieldValue[] = ' ';
        }else{
            if(array_key_exists($v,$relatedModuleFieldDefs)){
                $calculateFieldValue[] = $relatedModuleBean->$v;
            }else{
                $v = str_replace('&quot;', '', $v);
                $v = str_replace('&#039;', '', $v);
                $calculateFieldValue[] = $v;
            }//end of else
        }//end of else
    }//end of foreach

    //string concat
    $calculateFieldVal = '';
    foreach ($calculateFieldValue as $ckey => $cvalue) {
        if($cvalue == ''){
            $calculateFieldVal .= ' ';
        }else{
            $calculateFieldVal .= $cvalue;
        }//end of else
    }//end of foreach

    return $calculateFieldVal;
}//end of function

function autoPopulateFieldsAddFunction($value){
    $val = 0;
    foreach ($value as $key => $value) {
        $val += (float)$value;
    }//end of foreach
    $val = (float)$val;
    return $val;
}//end of function

function autoPopulateFieldsSubFunction($value){
    $val = 0;
    foreach ($value as $key => $value) {
        if($key == 0){
            $val = (float)$value;
        }else{
            $val -= (float)$value;
        }//end of else
    }//end of foreach
    $val = (float)$val;
    return $val;    
}//end of function

function autoPopulateFieldsMultiplyFunction($value){
    $val = 0;
    foreach ($value as $key => $value) {
        if($key == 0){
            $val = (float)$value;
        }else{
            $val = (float)$val * (float)$value;
        }
    }
    $val = (float)$val;
    return $val;    
}//end of function

function autoPopulateFieldsDivideFunction($value){
    $val = (float)$value[0]/$value[1];
    return $val;
}//end of function

function autoPopulateFieldsAverageFunction($value){
    $valueArray = array_filter($value);
    $sumArray = array_sum($valueArray);
    if($sumArray != 0){
        $val = array_sum($valueArray)/count($valueArray);
    }else{
        $val = 0;
    }//end of else
    return $val;
}//end of function

function autoPopulateFieldsMinimumFunction($value){
    $val = min($value);
    return $val;
}//end of function

function autoPopulateFieldsLogarithmFunction($value){
    $val = (float)log($value[0],$value[1]);
    return $val;
}//end of function

function autoPopulateFieldsLnFunction($value){
    $val = (float)log($value[0]);
    return $val;
}//end of function

function autoPopulateFieldsAbsoluteFunction($value){
    $val = abs($value[0]);
    return $val;
}//end of function

function autoPopulateFieldsPowerFunction($value){
    $val = pow($value[0],$value[1]);
    return $val;
}//end of function

function autoPopulateFieldsDateDiffFunction($value){
    $date1 = date_create($value[0]);
    $date2 = date_create($value[1]);
    $dateDiff = date_diff($date1,$date2);
    $val = $dateDiff->format('%a');
    if($val > 1){
        $val .= ' days';
    }else{
        $val .= ' day';
    }//end of else
    return $val;
}//end of function

function autoPopulateFieldsPercentageFunction($value){
    $percentage = (float)($value[1] / 100);
    $val  = $value[0] * $percentage;
    $val = (float)$val;
    return $val;
}//end of function

function autoPopulateFieldsModuloFunction($value){
    $val = $value[0] % $value[1];
    return $val;
}//end of function

function autoPopulateFieldsNegateFunction($value){
    if($value[0] < 0){
        $val = $value[0];    
    }else{
        $val = '-'.$value[0];
    }//end of else
    return $val;
}//end of function

function autoPopulateFieldsFloorFunction($value){
    $val = floor($value[0]);
    return $val;
}//end of function

function autoPopulateFieldsCeilFunction($value){
    $val = ceil($value[0]);
    $val = (float)$val;
    return $val;
}//end of function

function autoPopulateFieldsAddDateTimeFunction($value,$functionName){
    global $current_user;
    if($value[0] != ''){
        $fieldValue = date("Y-m-d H:i:s", strtotime($value[0]));
        if($functionName == 'Add Hours'){
            $addDate = date('Y-m-d H:i:s',strtotime('+'.$value[1].'hour',strtotime($fieldValue)));
        }else if($functionName == 'Add Day'){
            $addDate = date('Y-m-d H:i:s',strtotime('+'.$value[1].'day',strtotime($fieldValue)));
        }else if($functionName == 'Add Week'){
            $addDate = date('Y-m-d H:i:s',strtotime('+'.$value[1].'week',strtotime($fieldValue)));
        }else if($functionName == 'Add Month'){
            $addDate = date('Y-m-d H:i:s',strtotime('+'.$value[1].'month',strtotime($fieldValue)));
        }else if($functionName == 'Add Year'){
            $addDate = date('Y-m-d H:i:s',strtotime('+'.$value[1].'year',strtotime($fieldValue)));
        }//end of else if
        $dateFormat = $current_user->getPreference('datef');
        $val = date($dateFormat.' H:i',strtotime($addDate));
        return $val;
    }//end of if
}//end of function

function autoPopulateFieldsSubDateTimeFunction($value,$functionName){
    global $current_user;
    if($value[0] != ''){
        $fieldValue = date("Y-m-d H:i:s", strtotime($value[0]));
        if($functionName == 'Sub Hours'){
            $subDate = date('Y-m-d H:i:s',strtotime('-'.$value[1].'hour',strtotime($fieldValue)));
        }else if($functionName == 'Sub Days'){
            $subDate = date('Y-m-d H:i:s',strtotime('-'.$value[1].'day',strtotime($fieldValue)));
        }else if($functionName == 'Sub Week'){
            $subDate = date('Y-m-d H:i:s',strtotime('-'.$value[1].'week',strtotime($fieldValue)));
        }else if($functionName == 'Sub Month'){
            $subDate = date('Y-m-d H:i:s',strtotime('-'.$value[1].'month',strtotime($fieldValue)));
        }else if($functionName == 'Sub Year'){
            $subDate = date('Y-m-d H:i:s',strtotime('-'.$value[1].'year',strtotime($fieldValue)));
        }//end of else if
        $dateFormat = $current_user->getPreference('datef');
        $val = date($dateFormat.' H:i',strtotime($subDate));
        return $val;
    }//end of if
}//end of function

function autoPopulateFieldsDateDifferenceFunction($value,$functionName){
    $date1 = date_create($value[0]);
    $date2 = date_create($value[1]);
    if($value[0] != '' && $value[1] != ''){
        $dateDiff = date_diff($date1,$date2);
        if($functionName == 'Diff Days'){
            $val = $dateDiff->format('%a');
            if($val > 1){
                $val .= ' days';
            }else{
                $val .= ' day';
            }//end of else
        }else if($functionName == 'Diff Hour'){
            $hours = $dateDiff->days * 24;
            $hours += $dateDiff->h;
            if($hours > 1){
                $val = $hours.' hours';
            }else{
                $val = $hours.' hour';
            }//end of else
        }else if($functionName == 'Diff Minute'){
            $minutes = $dateDiff->days * 24 * 60;
            $minutes += $dateDiff->h * 60;
            $minutes += $dateDiff->i;
            if($minutes > 1){
                $val = $minutes.' minutes';
            }else{
                $val = $minutes.' minute';
            }//end of else
        }else if($functionName == 'Diff Month'){
            $date1 = strtotime($value[0]);
            $date2 = strtotime($value[1]);
            $year1 = date('Y', $date1);
            $year2 = date('Y', $date2);
            $month1 = date('m', $date1);
            $month2 = date('m', $date2);
            $monthDiff = (($year2 - $year1) * 12) + ($month2 - $month1);
            if($monthDiff > 1){
                $val = $monthDiff.' months';
            }else{
                $val = $monthDiff.' month';
            }//end of else
        }else if($functionName == 'Week Diff'){
            $weekDiff = floor($dateDiff->days/7);
            if($weekDiff > 1){
                $val = $weekDiff.' weeks';
            }else{
                $val = $weekDiff.' week';
            }//end of else
        }else if($functionName == 'Diff Year'){
            $yearDiff = $dateDiff->y;
            if($yearDiff > 1){
                $val = $yearDiff.' years';
            }else{
                $val = $yearDiff.' year';
            }//end of else
        }//end of else if
        return $val;
    }//end of if
}//end of function

function getAutoPopulateRelatedFields($sourceModule,$relatedModule){
    $bean = BeanFactory::newBean($sourceModule);
    $fieldDefs = $bean->getFieldDefinitions();

    //editview
    require_once("modules/ModuleBuilder/parsers/ParserFactory.php");
    $editviewArray = ParserFactory::getParser('editview',$sourceModule);
    $editviewPanels = $editviewArray->_viewdefs['panels'];

    //editview fields
    $editFields = array();
    foreach ($editviewPanels as $key => $value) {
        foreach($value as $k => $v){
            $editFields[] = $v;
        }//end of foreach
    }//end of foreach

    $relateFieldName = array();
    foreach ($editFields as $key => $value) {
        foreach($value as $k => $v){
            if(array_key_exists($v, $fieldDefs)){
                $fieldType = $fieldDefs[$v]['type'];
                if($fieldType == "relate"){
                    $moduleName = $fieldDefs[$v]['module'];
                    if($moduleName == $relatedModule){
                        $relateFieldName[] = $fieldDefs[$v]['name'];
                    }
                }else if($fieldType == 'parent'){
                    $domName = $fieldDefs['parent_name']['options'];
                    $moduleList = $app_list_strings[$domName];
                    foreach ($moduleList as $mkey => $mvalue) {
                        if($mkey == $relatedModule){
                            $relateFieldName[] = 'parent_type_'.$mkey;
                        }//end of if
                    }//end of foreach
                }//end of else if
            }//end of if
        }//end of foreach
    }//end of foreach

    return $relateFieldName;
}//end of function

//help box content
function getAutoPopulateFieldsHelpBoxHtml($url){
    global $suitecrm_version, $theme, $current_language;
    
    $helpBoxContent = '';
    $curl = curl_init();

    $postData = json_encode(array("suiteCRMVersion" => $suitecrm_version, "themeName" => $theme, 'currentLanguage' => $current_language));
    
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,false);
    
    $data = curl_exec($curl);
    $httpCode = curl_getinfo($curl,CURLINFO_HTTP_CODE);
    if($httpCode == 200){
        $helpBoxContent = $data;
    }//end of if
    curl_close($curl);

    return $helpBoxContent;
}//end of function