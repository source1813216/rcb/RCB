<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
include("custom/VIConditionalNotifications/VIConditionalNotificationsFunctions.php");
class VIConditionalNotificationsEditviewCheckCondition{
    public function __construct(){
        $this->conditionalNotificationsEditviewCheckCondition();
    } 
    public function conditionalNotificationsEditviewCheckCondition(){
        $moduleName = $_REQUEST['module'];
        $recordId = $_REQUEST['recordId'];
        $bean = BeanFactory::newBean($moduleName);
        
        $fieldData = array();
        $data = array();
        
        //get conditions data
        $conditionalNotificationsId = conditionalNotificationsId($moduleName);
        $conditionalOperator = conditionalNotificationsOperator($moduleName);

        $conditionalNotificationsArray = getConditionalNotifcationsConditions($conditionalNotificationsId,$moduleName,'onEditview');

        if(!empty($conditionalNotificationsArray)){
            $matchConditionArray = array();
            parse_str($_REQUEST['formData'],$searchArray);
            foreach($conditionalNotificationsArray as $key => $value){
                $matchCondition = array();
                foreach ($value as $keys => $values) {
                    $fieldName = $_REQUEST['fieldName'];
                    if($values['notificationOnEdit'] == 1){
                        if($fieldName == $values['field']){
                            $matchCondition[] = matchConditionanlNotificationCondition($searchArray,$values,$moduleName,$_REQUEST['fieldValue'],$recordId);
                        }else{
                            $matchCondition[] = matchConditionanlNotificationCondition($searchArray,$values,$moduleName,$val = '',$recordId);
                        }//end of else
                        $actionTitle[$key] = $values['actionTitle'];
                        $bodyHTML[$key] = $values['bodyHTML'];
                        $notificationConfirmationDialog[$key] = $values['notificationConfirmationDialog']; 
                    }//end of if
                }//end of foreach
                if(!empty($matchCondition)){
                    if($conditionalOperator[$key] == 'AND'){
                        if(in_array('0',$matchCondition)){
                            $matchConditionArray[$key] = '0'; 
                        }else{
                            $matchConditionArray[$key] = '1';
                        }//end of else
                    }else{
                        if(in_array('1',$matchCondition)){
                            $matchConditionArray[$key] = '1'; 
                        }else{
                            $matchConditionArray[$key] = '0';
                        }//end of else
                    }//end of else   
                }//end of if
            }//end of foreach
            $result = array();
            foreach ($matchConditionArray as $key => $value) {
                if($value == '1'){
                    $actionName = $actionTitle[$key];
                    $messages = html_entity_decode($bodyHTML[$key]);
                    $result[$key] = array('actionTitle' => $actionName,'bodyHTML' => $messages, 'notificationConfirmationDialog' => $notificationConfirmationDialog[$key]);
                }//end of if
            }//end of foreach

            $relateFieldName = $_REQUEST['relateFieldName'];
            foreach($result as $key => $value){
                if($relateFieldName != ''){
                    $fields = $relateFieldName;
                }else{
                    $fields = $fieldName;
                }//end of else
                
                if($fieldName == 'billing_account_id'){
                    $fields = 'billing_account';
                }else if($fieldName == 'billing_contact_id'){
                    $fields = 'billing_contact';
                }//end of else if

                $selectData = "SELECT * FROM vi_conditional_notifications_conditions WHERE conditional_notifications_id = '$key' AND field = '$fields' AND deleted = 0";
                $selectResult = $GLOBALS['db']->query($selectData);
                $selectResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selectData));
                if(!empty($selectResultData)){
                    $data = array('actionTitle' => $value['actionTitle'],'bodyHTML' => $value['bodyHTML'], 'notificationConfirmationDialog' => $value['notificationConfirmationDialog']); 
                }//end of if 
            }//end of foreach
            echo json_encode($data);
        }else{
            echo json_encode($data);
        }//end of else
    }//end of function
}//end of class
new VIConditionalNotificationsEditviewCheckCondition();
?>