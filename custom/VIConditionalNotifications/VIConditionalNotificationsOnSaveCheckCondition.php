<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
include("custom/VIConditionalNotifications/VIConditionalNotificationsFunctions.php");
class VIConditionalNotificationsOnSaveCheckCondition{
    public function __construct(){
        $this->conditionalNotificationsOnSaveCheckCondition();
    } 
    public function conditionalNotificationsOnSaveCheckCondition(){
        parse_str($_REQUEST['formData'],$searchArray);
        $recordId = $_REQUEST['recordId'];
        $module = $searchArray['module'];
        
        $bean = BeanFactory::newBean($module);

        $fieldData = array();
        
        //get conditional notifications data
        $conditionalNotificationsId = conditionalNotificationsId($module);
        $conditionalOperator = conditionalNotificationsOperator($module);

        $conditionalNotificationsArray = getConditionalNotifcationsConditions($conditionalNotificationsId,$module,'onSave');

        $result = array();
        if(!empty($conditionalNotificationsId)){
            $matchConditionArray = array();
            if(!empty($conditionalNotificationsArray)){
                foreach($conditionalNotificationsArray as $key => $value){
                    $matchCondition = array();
                    foreach ($value as $keys => $values) {
                        if($values['notificationOnSave'] == 1 || $values['notificationNotAllowSave'] == 1 || $values['notificationConfirmationDialog'] == 1){
                            $matchCondition[] = matchConditionanlNotificationCondition($searchArray,$values,$module,$val = '',$recordId);
                            $actionTitle[$key] = $values['actionTitle'];
                            $bodyHTML[$key] = $values['bodyHTML'];
                            $notificationNotAllowSave[$key] = $values['notificationNotAllowSave'];
                            $notificationConfirmationDialog[$key] = $values['notificationConfirmationDialog'];
                        }//end of if
                    }//end of foreach
                    if(!empty($matchCondition)){
                        if($conditionalOperator[$key] == 'AND'){
                            if(in_array('0',$matchCondition)){
                                $matchConditionArray[$key] = '0'; 
                            }else{
                                $matchConditionArray[$key] = '1';
                            }//end of else
                        }else{
                            if(in_array('1',$matchCondition)){
                              $matchConditionArray[$key] = '1'; 
                            }else{
                              $matchConditionArray[$key] = '0';
                            }//end of else
                        }//end of else   
                    }//end of if
                }//end of foreach
                foreach ($matchConditionArray as $key => $value) {
                    if($value == '1'){
                        $actionName = $actionTitle[$key];
                        $messages = html_entity_decode($bodyHTML[$key]);
                        $result[$key] = array('actionTitle' => $actionName,'bodyHTML' => $messages,'notificationNotAllowSave' => $notificationNotAllowSave[$key], 'notificationConfirmationDialog' => $notificationConfirmationDialog[$key]);
                    }//end of if
                }//end of foreach
            }//end of if
            echo json_encode($result);  
        }else{
            echo json_encode($result);
        }//end of else
    }//end of function
}//end of class
new VIConditionalNotificationsOnSaveCheckCondition();
?>