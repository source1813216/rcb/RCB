<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("modules/AOW_WorkFlow/aow_utils.php");
class VIConditionalNotificationsModuleFieldType{
    public function __construct(){
        $this->getModuleFieldType();
    }

    public function getModuleDateField($module, $aow_field, $view, $value = null, $field_option = true,$fieldname){
        global $app_list_strings;
        // set $view = 'EditView' as default
        if (!$view) {
            $view = 'EditView';
        }

        $value = json_decode(html_entity_decode_utf8($value), true);

        if(!file_exists('modules/AOBH_BusinessHours/AOBH_BusinessHours.php')) unset($app_list_strings['aow_date_type_list']['business_hours']);

        $field = '';

        if($view == 'EditView'){
            $field .= "<select type='text' name='$aow_field".'[0]'."' id='$aow_field".'[0]'."' title='' tabindex='116'>". $this->getModuleDateFields($module, $view, $value[0], $field_option,$fieldname) ."</select>&nbsp;&nbsp;";
            $field .= "<select type='text' name='$aow_field".'[1]'."' id='$aow_field".'[1]'."' onchange='date_field_change(\"$aow_field\")'  title='' tabindex='116'>". get_select_options_with_id($app_list_strings['aow_date_operator'], $value[1]) ."</select>&nbsp;";
            $display = 'none';
            if($value[1] == 'plus' || $value[1] == 'minus') $display = '';
            $field .= "<input  type='text' style='display:$display' name='$aow_field".'[2]'."' id='$aow_field".'[2]'."' title='' value='$value[2]' tabindex='116'>&nbsp;";
            $field .= "<select type='text' style='display:$display' name='$aow_field".'[3]'."' id='$aow_field".'[3]'."' title='' tabindex='116'>". get_select_options_with_id($app_list_strings['aow_date_type_list'], $value[3]) ."</select>";
        }
        return $field;
    }

    public function getModuleDateFields($module, $view='EditView',$value = '', $field_option = true,$fieldname) {
        global $beanList, $app_list_strings;

        $fields = $app_list_strings['aow_date_options'];

        if(!$field_option) unset($fields['field']);
        $mod = new $beanList[$module]();
        $fieldData = $mod->field_defs[$fieldname];
        $fieldType = $fieldData['type'];
        if ($module != '') {
            if(isset($beanList[$module]) && $beanList[$module]){
                $mod = new $beanList[$module]();
                foreach($mod->field_defs as $name => $arr){

                    if($fieldType == 'date' ){
                        if($arr['type'] == 'date'){
                            if(isset($arr['vname']) && $arr['vname'] != ''){
                                $fieldLabel = translate($arr['vname'],$mod->module_dir);
                                if(strpos($fieldLabel, ':')){
                                    $fieldLabel = substr_replace($fieldLabel, "", -1);
                                }
                                $fields[$name] = $fieldLabel;
                            } else {
                                $fields[$name] = $name;
                            }
                        }
                    }else if($fieldType == 'datetime' || $fieldType == 'datetimecombo'){
                        if($arr['type'] == 'datetime' || $arr['type'] == 'datetimecombo'){
                            if(isset($arr['vname']) && $arr['vname'] != ''){
                                $fieldLabel = translate($arr['vname'],$mod->module_dir);
                                if(strpos($fieldLabel, ':')){
                                    $fieldLabel = substr_replace($fieldLabel, "", -1);
                                }
                                $fields[$name] = $fieldLabel;
                            } else {
                                $fields[$name] = $name;
                            }
                        }
                    }
                } //End loop.
            }
        }
        if($view == 'EditView'){
            return get_select_options_with_id($fields, $value);
        }
    }

    public function getModuleFieldType(){
        if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != ''){
            $rel_module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
        } else {
            $rel_module = $_REQUEST['aow_module'];
        }
        $module = $_REQUEST['aow_module'];
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = ' ';
        
        switch($_REQUEST['aow_type']) {
            case 'Date':
                    echo $this->getModuleDateField($module, $aow_field, "EditView", $value, false,$fieldname);
                    break;
            case 'Value':
            default:
                if($view == 'vi_conditionalnotificationseditview'){
                    echo getModuleField($rel_module,$fieldname, $aow_field, 'EditView', $value);
                }
                break;
        }
        die;
    }//end of function
}//end of class
new VIConditionalNotificationsModuleFieldType();
?>