<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VIDeleteConditionalNotifications{
    public function __construct(){
        $this->deleteConditionalNotifications();
    } 
    public function deleteConditionalNotifications(){
        if(isset($_POST['del_id'])){
            $delId = explode(',',$_POST['del_id']);
            foreach($delId as $id){
                //delete conditional notifications data
                $updateConditionalNotifications = "UPDATE vi_conditional_notifications SET deleted = 1 
                                                                                       WHERE conditional_notifications_id = '$id'";
                $updateConditionalNotificationsResult = $GLOBALS['db']->query($updateConditionalNotifications);

                //delete conditional notifications conditions data
                $updateConditionalNotificationsConditions = "UPDATE vi_conditional_notifications_conditions SET deleted = 1                                                         WHERE conditional_notifications_id = '$id'";
                $updateConditionalNotificationsConditionsResult = $GLOBALS['db']->query($updateConditionalNotificationsConditions);
            }//end of foreach
        }//end of if
    }//end of function   
}//end of class
new VIDeleteConditionalNotifications();
?>
