<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
 require_once("modules/AOW_WorkFlow/aow_utils.php");
class VIConditionalNotificationsModuleOperatorField{
	public function __construct(){
		$this->getModuleOperatorField();
	} 
	public function getModuleOperatorField(){
		global $app_list_strings, $beanFiles, $beanList;
        
        $module = $_REQUEST['aow_module'];
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];
        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = '';

        require_once($beanFiles[$beanList[$module]]);
        $focus = new $beanList[$module];
        $vardef = $focus->getFieldDefinition($fieldname);
        if($vardef){
            switch($vardef['type']) {
                case 'double':
                case 'decimal':
                case 'float':
                case 'currency':
                $valid_opp = array('Equal_To','Not_Equal_To','Greater_Than','Less_Than','Greater_Than_or_Equal_To','Less_Than_or_Equal_To','is_null','regular_expression');
                    break;
                case 'uint':
                case 'ulong':
                case 'long':
                case 'short':
                case 'tinyint':
                case 'int':
                $valid_opp = array('Equal_To','Not_Equal_To','Greater_Than','Less_Than','Greater_Than_or_Equal_To','Less_Than_or_Equal_To','is_null','regular_expression');
                    break;
                case 'date':
                case 'datetime':
                case 'datetimecombo':
                $valid_opp = array('Equal_To','Not_Equal_To','Greater_Than','Less_Than','Greater_Than_or_Equal_To','Less_Than_or_Equal_To','is_null','regular_expression');
                    break;
                case 'enum':
                case 'multienum':
                $valid_opp = array('Equal_To','Not_Equal_To','is_null','regular_expression');
                    break;
                default:
                $valid_opp = array('Equal_To','Not_Equal_To','Contains', 'Starts_With', 'Ends_With','is_null','regular_expression');
                break;
            }
            $app_list_strings['aow_operator_list']['regular_expression'] = 'Regular Expression';
            foreach($app_list_strings['aow_operator_list'] as $key => $keyValue){
                if(!in_array($key, $valid_opp)){
                    unset($app_list_strings['aow_operator_list'][$key]);
                }
            }
           $app_list_strings['aow_operator_list'];
            if($view == 'vi_conditionalnotificationseditview'){
                echo "<select type='text' name='$aow_field' id='$aow_field' title='' tabindex='116'>". get_select_options_with_id($app_list_strings['aow_operator_list'], $value) ."</select>";
            }else{
                echo $app_list_strings['aow_operator_list'][$value];
            }
        }
        die;
    }//end of function
}//end of class
new VIConditionalNotificationsModuleOperatorField();