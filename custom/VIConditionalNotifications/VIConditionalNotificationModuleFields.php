<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("modules/AOW_WorkFlow/aow_utils.php");
class VIConditionalNotificationModuleFields{
    public function __construct(){
        $this->getModuleFields();
    } 
    public function getEditDetailViewFields($view,$module,$validType) {
        require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
        $view_array = ParserFactory::getParser($view,$module);
        $panelArray = $view_array->_viewdefs['panels'];//editview panels
        $bean = BeanFactory::newBean($module);
        $field = $bean->getFieldDefinitions();

        //editview fields
        $editViewFieldArray = array();
        foreach ($panelArray as $key => $value) {
            foreach ($value as $keys => $values) {
                $editViewFieldArray[] = $values;
            }//end of foreach
        }//end of foreach
    
        $data = array('' => '--None--');
        foreach($editViewFieldArray as $key => $value) {
            foreach($value as $k => $v) {
                if(array_key_exists($v, $field)) {
                    if((empty($validType) || in_array($field[$v]['type'], $validType))){
                        require_once('include/utils.php');
                        $fieldLabel = translate($field[$v]['vname'], $module);
                        if(strpos($fieldLabel, ':')){
                            $fieldLabel = substr_replace($fieldLabel, "", -1);
                        }//end of if
                        $fieldName = $v;
                        $data[$fieldName] = $fieldLabel;
                    }
                }//end of if 
            }//end of foreach
        }//end of foreach

        //unset fields
        $unsetData = array('sample','insert_fields','update_text','case_update_form','aop_case_updates_threaded','internal','survey_questions_display','line_items','email1','email2','suggestion_box','filename','product_image','configurationGUI','invite_templates','action_lines','condition_lines','survey_url_display','reminders','pdffooter','pdffooter');
        foreach ($unsetData as $key => $value) {
            unset($data[$value]);
        }//end of foreach

        if($module == 'jjwg_Maps' || $module == 'Meetings' || $module == 'Notes' || $module == 'Tasks' || $module == 'Calls'){
            unset($data['parent_name']);
        }//end of if

        if($module == 'AOS_PDF_Templates' || $module == 'AOK_KnowledgeBase' || $module == 'Cases'){
            unset($data['description']);
        }
        if($module == 'AOS_Invoices'){
            unset($data['number']);
        }
        return $data;
    }//end of function
    public function getModuleFields(){
        $module = $_REQUEST['aow_module'];
        $val = isset($_REQUEST['aow_value']) ? $_REQUEST['aow_value'] : '';
        require_once('include/utils.php');
        $bean = BeanFactory::newBean($module);
        $field = $bean->getFieldDefinitions();

        $validType = array();
        if(isset($_REQUEST['type'])){
            $validType = getValidFieldsTypes($module,$_REQUEST['fieldName']);
        }

        $addressFieldData = array();
        foreach($field as $value){
            if($value['type'] == 'varchar' && (empty($validType) || in_array($value['type'], $validType))){
                if(isset($value['group'])){
                    $fieldName = $value['name'];
                    $fieldLabel = translate($value['vname'],$module);
                    if(strpos($fieldLabel, ':')){
                        $fieldLabel = substr_replace($fieldLabel, "", -1);
                    }//end of if
                $addressFieldData[$fieldName] = $fieldLabel;
                }//end of if
            }//end of if
        }//end of foreach

        unset($addressFieldData['email1']);
        unset($addressFieldData['email2']);

        $editViewFieldData = $this->getEditDetailViewFields('editview',$module,$validType);
        $detailViewFieldData = $this->getEditDetailViewFields('detailview',$module,$validType);

        $arrayMerge = array_merge($editViewFieldData,$addressFieldData,$detailViewFieldData); //merge array
        asort($arrayMerge);
        echo get_select_options_with_id($arrayMerge,$val);
        die;
    }//end of function 
}//end of class
new VIConditionalNotificationModuleFields();
?>

