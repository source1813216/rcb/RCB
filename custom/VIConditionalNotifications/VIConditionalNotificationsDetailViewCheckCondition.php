<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
include("custom/VIConditionalNotifications/VIConditionalNotificationsFunctions.php");
class VIConditionalNotificationsDetailViewCheckCondition{
    public function __construct(){
        $this->conditionalNotificationsDetailViewCheckCondition();
    } 
    public function conditionalNotificationsDetailViewCheckCondition(){
        $module = $_REQUEST['module'];
        $recordId = $_REQUEST['recordId'];
        
        $bean = BeanFactory::newBean($module);
        
        if($recordId != '' && $module != ''){
            //get conditions data
            $conditionalNotificationsId = conditionalNotificationsId($module);
            $conditionalOperator = conditionalNotificationsOperator($module);

            $conditionalNotificationsArray = getConditionalNotifcationsConditions($conditionalNotificationsId,$module,'onDetailView');
            if(!empty($conditionalNotificationsArray)){
                $bean->retrieve($recordId);
                $matchConditionArray = array();
                foreach ($conditionalNotificationsArray as $key => $value) {
                    $matchCondition = array();
                    foreach ($value as $keys => $values) {
                        $formData = '';
                        if($values['notificationOnView'] == 1){
                            $fieldValue = $bean->fetched_row[$values['field']];
                            $matchCondition[] = matchConditionanlNotificationCondition($formData,$values,$module,$fieldValue,$recordId);
                            $actionTitle[$key] = $values['actionTitle'];
                            $bodyHTML[$key] = $values['bodyHTML'];
                        }//end of if
                    }//end of foreach
                    if(!empty($matchCondition)){
                        if($conditionalOperator[$key] == 'AND'){
                            if(in_array('0',$matchCondition)){
                                $matchConditionArray[$key] = '0'; 
                            }else{
                                $matchConditionArray[$key] = '1';
                            }//end of else
                        }else{
                            if(in_array('1',$matchCondition)){
                              $matchConditionArray[$key] = '1'; 
                            }else{
                              $matchConditionArray[$key] = '0';
                            }//end of else
                        }//end of else   
                    }//end of if
                }//end of foreach
                $result = array();
                foreach ($matchConditionArray as $key => $value) {
                    if($value == '1'){
                        $actionName = $actionTitle[$key];
                        $messages = html_entity_decode($bodyHTML[$key]);
                        $result[$key] = array('actionTitle' => $actionName,'bodyHTML' => $messages);
                    }//end of if
                }//end of foreach   
                echo json_encode($result);
            }//end of if
        }//end of if   
    }//end of function
}//end of class
new VIConditionalNotificationsDetailViewCheckCondition();
?>