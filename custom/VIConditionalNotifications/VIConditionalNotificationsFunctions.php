<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
 //match configuration condition
function matchConditionanlNotificationCondition($formData,$conditionaNotificationsData,$module,$val,$recordId){
    global $timedate, $current_user,$app_list_strings;
    $moduleBean = BeanFactory::newBean($module);
    if($val != ''){
        $fieldValue = $val;
    }else{
        $fieldValue = $formData[$conditionaNotificationsData['field']]; 
    }//end of else
    
    if(isset($formData[$conditionaNotificationsData['field']])){
        if($fieldValue != $formData[$conditionaNotificationsData['field']]){
            $fieldValue = $formData[$conditionaNotificationsData['field']];
        }
    }

    if($fieldValue == '' && $recordId != ''){
        $bean = BeanFactory::getBean($module,$recordId);
        $fieldValue = $bean->$conditionaNotificationsData['field'];
    }

    $operator = $conditionaNotificationsData['operator'];
    $value = $conditionaNotificationsData['value'];
    $valueType = $conditionaNotificationsData['valueType'];
    if($valueType == 'Field'){
        $field = $conditionaNotificationsData['value'];
        $fieldData = $moduleBean->field_defs[$field];
        $fieldType = $fieldData['type'];
        if($recordId != ''){
            if(!isset($formData[$field])){
                $recordBean = BeanFactory::getBean($module,$recordId);
                $value = $recordBean->$field;
                if($fieldData['type'] == 'relate'){
                    $idName = $fieldData['id_name'];
                    $value = $recordBean->$idName;
                }
            }else{
                $value = $formData[$field];
            }
        }else{
            $value = $formData[$field];
            if($fieldData['type'] == 'relate'){
                $idName = $fieldData['id_name'];
                $value = $formData[$idName];
            }
        }
    }else if($valueType == 'Date'){
        $params = unserialize(base64_decode($value));
        $dateType = 'datetime';
        if($params[0] == 'now'){
            $value = date('Y-m-d H:i');
            $fieldValue = strtotime(date('Y-m-d H:i',strtotime($fieldValue)));
        }else if($params[0] == 'today'){
            $dateType = 'date';
            $value = date('Y-m-d');
            $fieldValue = strtotime(date('Y-m-d', strtotime($fieldValue)));
        }else {
            $fieldName = $params[0];
            if($recordId != ''){
                $recordBean = BeanFactory::getBean($module,$recordId);
                if(!isset($formData[$fieldName])){
                    $value = $recordBean->$fieldName;
                }else{
                    $value = $formData[$fieldName];
                }
            }else{
                $value = $formData[$fieldName];
            }
            $fieldValue = strtotime(date('Y-m-d H:i',strtotime($fieldValue)));
        }

        if($params[1] != 'now'){
            switch($params[3]) {
                case 'business_hours';
                    if(file_exists('modules/AOBH_BusinessHours/AOBH_BusinessHours.php')){
                        require_once('modules/AOBH_BusinessHours/AOBH_BusinessHours.php');

                        $businessHours = new AOBH_BusinessHours();

                        $amount = $params[2];
                        if($params[1] != "plus"){
                            $amount = 0-$amount;
                        }
                        $value = date('Y-m-d H:i:s',strtotime($value));
                        $value = $businessHours->addBusinessHours($amount, $timedate->fromDb($value));
                        $value = strtotime($timedate->asDbType( $value, $dateType ));
                        break;
                    }
                    //No business hours module found - fall through.
                    $params[3] = 'hours';
                default:
                    $value = strtotime($value.' '.$app_list_strings['aow_date_operator'][$params[1]]." $params[2] ".$params[3]);
                    if($dateType == 'date') $value = strtotime(date('Y-m-d', $value));
                    break;
            }
        }else{
            $value = strtotime($value);
        }
    }
    if($recordId != ''){
        if($valueType == 'Field'){
            if(!isset($formData[$conditionaNotificationsData['field']])){
                $recordBean = BeanFactory::getBean($module,$recordId);
                $fieldValue = $recordBean->$conditionaNotificationsData['field'];    
            }
        }
    }
    
    $fieldDef = $moduleBean->field_defs[$conditionaNotificationsData['field']];
    $fieldType = $fieldDef['type'];
    if($valueType == 'Field' || $valueType == 'Value'){
        if($fieldType == 'datetimecombo' || $fieldType == 'datetime'){
            if($fieldValue != ''){
                if($valueType == 'Field'){
                    $value = $timedate->to_db($value);
                    $value = date('Y-m-d H:i',strtotime($value));
                }else{
                    $value = date('Y-m-d H:i',strtotime($value));
                }
                $fieldValue = $timedate->to_db($fieldValue);
                $fieldValue = date('Y-m-d H:i',strtotime($fieldValue));
            }//end of if
        }else if($fieldType == 'date'){
            if($fieldValue != ''){
                $value = date('Y-m-d',strtotime($value));
                $fieldValue = date('Y-m-d',strtotime($fieldValue));
            }    
        }else if($fieldType == 'multienum'){                          
            $fieldValue = encodeMultienumValue($fieldValue);
        }//end of if
    }
    
    $regularExpression = $conditionaNotificationsData['regularExpression'];
    switch ($operator) {
        case 'Equal_To':
            if($fieldValue == $value){
                $matchCondition = '1';
            }else{
                $matchCondition = '0';
            }
            break;
        case 'Not_Equal_To':
            if($fieldValue != $value){
                $matchCondition = '1';
            }else{
                $matchCondition = '0';
            }//end of else
            break;
        case 'is_null':
            if(empty($fieldValue)){
                $matchCondition = '1';    
            }else{
                $matchCondition = '0';
            }//end of else
            break;
        case 'Greater_Than':
            if($fieldValue > $value){
                $matchCondition = '1';
            }else{
                $matchCondition = '0';
            }//end of else
            break;
        case 'Less_Than':
            if($fieldValue < $value){
                $matchCondition = '1'; 
            }else{
                $matchCondition = '0';
            }
            break;
        case 'Greater_Than_or_Equal_To':
            if($fieldValue >= $value){
                $matchCondition = '1';
            }else{
                $matchCondition = '0';
            }
            break;
        case 'Less_Than_or_Equal_To':
            if($fieldValue <= $value){
                $matchCondition = '1';
            }else{
                $matchCondition = '0';
            }
            break;
        case 'Contains':
            if(condtionalNotificationsContains($fieldValue,$value)){
                $matchCondition = '1';
            }else{
                $matchCondition = '0';
            }
            break;
        case 'Starts_With':
            if(conditionalNotificationsStartsWiths($fieldValue,$value)){
                $matchCondition = '1';
            }else{
                $matchCondition = '0';
            }
            break;
        case 'Ends_With':
            if(conditionalNotificationsEndsWiths($fieldValue,$value)){
                $matchCondition = '1';
            }else{
                $matchCondition = '0';
            }
            break;
       case 'regular_expression':
            if($fieldType == 'date'){
                $dateFormat = $current_user->getPreference('datef');
                $fieldValue = date($dateFormat,strtotime($fieldValue));                                        
            }else if($fieldType == 'datetimecombo'){
                $fieldValue  = date("Y-m-d H:i:s", strtotime($fieldValue));
            }
            if (preg_match("/".$regularExpression."/", $fieldValue)){   
                $matchCondition = '1';
            }else{
                $matchCondition = '0';
            }
            break;
        default:
            echo "";
            break;
    }//end of switch
    return $matchCondition;
}//end of function

//contains 
function condtionalNotificationsContains($str,$substr){
    if(strpos($str,$substr) !== false){
         return true;
    }//end of if
}//end of function

//startwith
function conditionalNotificationsStartsWiths($str,$substr){
    $sl = strlen($str);
    $ssl = strlen($substr);
    if ($sl >= $ssl) {
        if(strpos($str, $substr, 0) === 0){
            return true;
        }//end of if
    }//end of if
}//end of function

//endswith
function conditionalNotificationsEndsWiths($str, $subStr) {
    $sl = strlen($str);
    $ssl = strlen($subStr);
    if ($sl >= $ssl) {
        if(substr_compare($str, $subStr, $sl - $ssl, $ssl) == 0){
            return true;
        }//end of if
    }//end of if
}//end of function

function conditionalNotificationsId($module){
	$conditionalNotificationsId = array();
	$selConditionalNotifications = "SELECT * FROM vi_conditional_notifications WHERE module_name = '$module' 
                                                                               AND deleted = 0";
    $selConditionalNotificationsResult = $GLOBALS['db']->query($selConditionalNotifications);
   	while($selDataRow = $GLOBALS['db']->fetchByAssoc($selConditionalNotificationsResult)){
        $conditionalNotificationsId[] = $selDataRow['conditional_notifications_id'];
    }//end of while
    return $conditionalNotificationsId;
}//end of function

function conditionalNotificationsOperator($module){
	$conditionalOperator = array();
	$selConditionalNotifications = "SELECT * FROM vi_conditional_notifications WHERE module_name = '$module' 
                                                                               AND deleted = 0";
    $selConditionalNotificationsResult = $GLOBALS['db']->query($selConditionalNotifications);
   	while($selDataRow = $GLOBALS['db']->fetchByAssoc($selConditionalNotificationsResult)){
        $conditionalOperator[$selDataRow['conditional_notifications_id']] = $selDataRow['conditional_operator'];
    }//end of while
    return $conditionalOperator;
}//end of function
 
function getConditionalNotifcationsConditions($conditionalNotificationsIds,$moduleName,$notificationOn){
	$conditionalNotificationsArray = array();
	if(!empty($conditionalNotificationsIds)){
		$moduleBeanData = BeanFactory::newBean($moduleName);
		foreach ($conditionalNotificationsIds as $key => $id) {
            $selConditions = "SELECT * FROM vi_conditional_notifications 
                                    JOIN vi_conditional_notifications_conditions ON vi_conditional_notifications.conditional_notifications_id = vi_conditional_notifications_conditions.conditional_notifications_id
                                    WHERE vi_conditional_notifications.conditional_notifications_id = '$id' 
                                    AND vi_conditional_notifications.deleted = 0 
                                    AND vi_conditional_notifications_conditions.deleted = 0";
            $selConditionsResult = $GLOBALS['db']->query($selConditions);
            while($selConditionRow = $GLOBALS['db']->fetchByAssoc($selConditionsResult)){
                $field = $selConditionRow['field'];
                $value = $selConditionRow['value'];
                $valueType = $selConditionRow['value_type'];
                if($notificationOn == 'onDuplicate' || $notificationOn == 'onDetailView'){
                	if($moduleName == 'FP_events'){
	                    if($field == 'duration'){
	                        $minutes = ($value / 60) % 60;
	                        if($minutes > 60){
	                            $field = 'duration_hours';
	                            $value = floor($value / 3600);
	                        }else{
	                            $field = 'duration_minutes';
	                            $value = $minutes;
	                        }//end of else
	                    }//end of if
                	}//end of if	
                }//end of if
                
                $fieldData = $moduleBeanData->field_defs[$field];
                if($fieldData['type'] == 'relate'){
                    $field = $fieldData['id_name'];
                }//end of if 
                $conditionalNotificationsArray[$id][] = array(
                                                            'conditionalNotificationsId' => $id,
                                                            'module' => $selConditionRow['module_path'],
                                                            'actionTitle' => $selConditionRow['action_title'],
                                                            'notificationOnEdit' => $selConditionRow['notification_on_edit'],
                                                            'notificationOnView' => $selConditionRow['notification_on_view'],
                                                            'notificationOnDuplicate' => $selConditionRow['notification_on_duplicate'],
                                                            'notificationOnSave' => $selConditionRow['notification_on_save'],
                                         					'notificationNotAllowSave' => $selConditionRow['notification_not_allow_save'],
                                                            'notificationConfirmationDialog' => $selConditionRow['notification_confirmation_dialog'],
                                                            'bodyHTML' => $selConditionRow['body_html'],
                                                            'field' => $field,
                                                            'operator' => $selConditionRow['operator'],
                                                            'value' => $value,
                                                            'valueType' => $valueType,
                                                            'regularExpression' => $selConditionRow['regular_expression']
                                                        );
            }//end of while
        }//end of foreach
	}//end of if
	return $conditionalNotificationsArray;
}//end of function

function getConditionalNotificationsHelpBoxHtml($url){
    global $suitecrm_version, $theme, $current_language;
    
    $helpBoxContent = '';
    $curl = curl_init();

    $postData = json_encode(array("suiteCRMVersion" => $suitecrm_version, "themeName" => $theme, 'currentLanguage' => $current_language));
    
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,false);
    
    $data = curl_exec($curl);
    $httpCode = curl_getinfo($curl,CURLINFO_HTTP_CODE);
    if($httpCode == 200){
        $helpBoxContent = $data;
    }//end of if
    curl_close($curl);

    return $helpBoxContent;
}//end of function