<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
 require_once("modules/AOW_WorkFlow/aow_utils.php");
class VIConditionalNotificationsModuleRelationships{
	public function __construct(){
		$this->getModuleRelationships();
	} 
	public function getModuleRelationships(){
		if (!empty($_REQUEST['aow_module']) && $_REQUEST['aow_module'] != '') {
            if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != ''){
                $module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
            } else {
                $module = $_REQUEST['aow_module'];
            }
            echo getModuleRelationships($module);
        }
        die;
    }//end of function
}//end of class
new VIConditionalNotificationsModuleRelationships();
?>