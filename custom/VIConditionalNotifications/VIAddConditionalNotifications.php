<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("modules/AOW_WorkFlow/aow_utils.php");
class VIAddConditionalNotifications{
    public function __construct(){
        $this->addConditionalNotifications();
    } 
    public function addConditionalNotifications(){
        global $timedate;
        parse_str($_POST['val'], $formData);
        $body_html = html_entity_decode($_POST['body_html']);

        $recordId = $_REQUEST['id'];
        $moduleName = $formData['flow_module'];
        $description = $formData['description'];
        $actionTitle = $formData['action_title'];
        $notificationOnEdit = $formData['notification_on_edits'];
        $notificationOnView = $formData['notification_on_views'];
        $notificationOnSave = $formData['notification_on_saves'];
        $notificationNotAllowSave = $formData['notification_not_allow_saves'];
        $notifcationConfirmationDialog = $formData['notification_confirmation_dialogs'];
        $notificationOnDuplicate = $formData['notification_on_duplicates'];
        $conditionalOperator = $formData['conditional_operator'];
        
        if($recordId == ''){
            $conditionalNotificationsId = create_guid();
            //insert conditional notifications data
            $insData = "INSERT INTO vi_conditional_notifications(conditional_notifications_id,module_name,description,action_title,notification_on_edit,notification_on_view,notification_on_save,notification_not_allow_save,notification_on_duplicate,body_html,conditional_operator,notification_confirmation_dialog)values('$conditionalNotificationsId','$moduleName','$description','$actionTitle',$notificationOnEdit,$notificationOnView,$notificationOnSave,$notificationNotAllowSave,$notificationOnDuplicate,'$body_html','$conditionalOperator','$notifcationConfirmationDialog')";
            $insResult = $GLOBALS['db']->query($insData);
            $recordId = $conditionalNotificationsId;
        }else{
            //update conditional notifications data
            $updateConditionalNotifications = "UPDATE vi_conditional_notifications SET module_name = '$moduleName',
                                                                            description = '$description',
                                                                            action_title = '$actionTitle',
                                                                            notification_on_edit = $notificationOnEdit,
                                                                            notification_on_view = $notificationOnView,
                                                                            notification_on_save = $notificationOnSave,
                                                                            notification_not_allow_save = $notificationNotAllowSave,
                                                                            notification_on_duplicate = $notificationOnDuplicate,
                                                                            body_html = '$body_html',
                                                                            conditional_operator = '$conditionalOperator',
                                                                            notification_confirmation_dialog = $notifcationConfirmationDialog
                                                                            WHERE conditional_notifications_id = '$recordId'";
            $updateConditionalNotificationsResult = $GLOBALS['db']->query($updateConditionalNotifications);

            //delete conditions
            $delConditions = "DELETE FROM vi_conditional_notifications_conditions WHERE conditional_notifications_id = '$recordId'";
            $delConditionsResult = $GLOBALS['db']->query($delConditions);
        }//end of else

        //insert conditional notifications conditions data
        $conditionId = array();
        if(isset($formData['aow_conditions_deleted'])){
            $delId = $formData['aow_conditions_deleted'];
        }else{
            $delId = '';
        }//end of else

        //modules
        if(isset($formData['aow_conditions_module_path'])){
            foreach($formData['aow_conditions_module_path'] as $key => $values) {
                foreach ($values as $key => $value) {
                    $id = create_guid();
                     $insCondition = "INSERT INTO vi_conditional_notifications_conditions(id,module_path,conditional_notifications_id)values('$id','$value','$recordId')";
                    $result = $GLOBALS['db']->query($insCondition);
                    $conditionId[] = $id;  
                }//end of foreach
            }//end of foreach
        }//end of if

        //field 
        foreach($formData['aow_conditions_field'] as $key => $values) {
            $updateField = "UPDATE vi_conditional_notifications_conditions SET field = '$values',
                                                                               deleted = '$delId[$key]' 
                                                                           WHERE id = '$conditionId[$key]' ";
            $updateFieldResult = $GLOBALS['db']->query($updateField);
        }

        //operator
        foreach($formData['aow_conditions_operator'] as $key => $values) {
            $updateOperaotr = "UPDATE vi_conditional_notifications_conditions SET operator = '$values',
                                                                                  deleted = '$delId[$key]' 
                                                                             WHERE id = '$conditionId[$key]'";
            $updateOperaotrResult = $GLOBALS['db']->query($updateOperaotr);
        }

        //value type
        foreach($formData['aow_conditions_value_type'] as $key => $values) {
            $updateValueType = "UPDATE vi_conditional_notifications_conditions SET value_type = '$values',
                                                                                   deleted = '$delId[$key]' 
                                                                                    WHERE id = '$conditionId[$key]'";
            $updateValueTypeResult = $GLOBALS['db']->query($updateValueType);
        }

        //value
        foreach($formData['aow_conditions_value'] as $key => $values) {
            $bean = BeanFactory::newBean($moduleName);
            $field = $formData['aow_conditions_field'][$key];
            $valueType = $formData['aow_conditions_value_type'][$key];
            $fieldDef = $bean->field_defs[$field];
            if($valueType == 'Value'){
                if($fieldDef['type'] == 'datetime' || $fieldDef['type'] == 'datetimecombo'){
                    $date = date('Y-m-d',strtotime($values));
                    $time = date('h:i:s',strtotime($values));
                    $dbtime = $timedate->to_db_time($values);
                    $values = $date.' '.$dbtime;
                }else if($fieldDef['type'] == 'multienum'){
                    $values = encodeMultienumValue($values);
                }else if($fieldDef['type'] == 'date'){
                    $date = date('Y-m-d',strtotime($values));
                    $values = $date;
                }else if($fieldDef['type'] == 'enum'){
                    $valueType = $formData['aow_conditions_value_type'][$key];
                    if($valueType == 'Multi'){
                        $values = encodeMultienumValue($values);
                    }
                }else{
                    $numericValue = str_replace( ',', '', $values);
                    if( is_numeric( $numericValue ) ) {
                        $values = $numericValue;
                    }
                }
            }else if($valueType == 'Date'){
                $values = base64_encode(serialize($values));
            }
            $updateValue = "UPDATE vi_conditional_notifications_conditions SET value = '$values',
                                                                               deleted = '$delId[$key]' 
                                                                               WHERE id = '$conditionId[$key]'";
            $updateValueResult = $GLOBALS['db']->query($updateValue);       
        }//end of foreach

        //regular expression
        foreach($formData['aow_conditions_regular_expression'] as $key => $values) {
            $updateRegularExpression = "UPDATE vi_conditional_notifications_conditions SET regular_expression = '$values',
                                                                                   deleted = '$delId[$key]' 
                                                                               WHERE id = '$conditionId[$key]'";
            $updateRegularExpressionResult = $GLOBALS['db']->query($updateRegularExpression);
        }//end of foreach
    }//end of function 
}//end of class
new VIAddConditionalNotifications();
?>

