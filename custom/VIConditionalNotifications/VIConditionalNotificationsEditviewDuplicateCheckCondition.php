<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
include("custom/VIConditionalNotifications/VIConditionalNotificationsFunctions.php");
class VIConditionalNotificationsEditviewDuplicateCheckCondition{
    public function __construct(){
        $this->conditionalNotificationsEditviewDuplicateCheckCondition();
    } 
    public function conditionalNotificationsEditviewDuplicateCheckCondition(){
        $moduleName = $_REQUEST['module'];
        $recordId = $_REQUEST['recordId'];
        $fieldData = array();
        $data = array();
        
        $bean = BeanFactory::newBean($moduleName);
        
        //get conditions data
        $conditionalNotificationsId = conditionalNotificationsId($moduleName);
        $conditionalOperator = conditionalNotificationsOperator($moduleName);

        $conditionalNotificationsArray = getConditionalNotifcationsConditions($conditionalNotificationsId,$moduleName,'onEditview');

        if(!empty($conditionalNotificationsArray)){
            $matchConditionArray = array();
            parse_str($_REQUEST['formData'],$searchArray);
            foreach($conditionalNotificationsArray as $key => $value){
                $matchCondition = array();
                foreach ($value as $keys => $values) {
                    $fieldName = $_REQUEST['fieldName'];
                    if($values['notificationOnDuplicate'] == 1){
                        if($fieldName == $values['field']){
                            $matchCondition[] = matchConditionanlNotificationCondition($searchArray,$values,$moduleName,$_REQUEST['fieldValue'],$recordId);
                        }else{
                            $matchCondition[] = matchConditionanlNotificationCondition($searchArray,$values,$moduleName,$val = '',$recordId);
                        }//end of else
                        $actionTitle[$key] = $values['actionTitle'];
                        $bodyHTML[$key] = $values['bodyHTML'];
                    }//end of if
                }//end of foreach

                if(!empty($matchCondition)){
                    if($conditionalOperator[$key] == 'AND'){
                        if(in_array('0',$matchCondition)){
                            $matchConditionArray[$key] = '0'; 
                        }else{
                            $matchConditionArray[$key] = '1';
                        }//end of else
                    }else{
                        if(in_array('1',$matchCondition)){
                          $matchConditionArray[$key] = '1'; 
                        }else{
                          $matchConditionArray[$key] = '0';
                        }//end of else
                    }//end of else   
                }//end of if
            }//end of foreach
            
            foreach ($matchConditionArray as $key => $value) {
                if($value == '1'){
                    $actionName = $actionTitle[$key];
                    $messages = html_entity_decode($bodyHTML[$key]);
                    $fieldName = $_REQUEST['fieldName'];
                    $fieldValue = $_REQUEST['fieldValue'];
                    $tableName = strtolower($moduleName);
                    $data1 = $bean->field_defs[$fieldName];
                    $customTable = $bean->get_custom_table_name();
                    $fieldType = $data1['type'];
                    if($fieldType == 'datetimecombo' || $fieldType == 'datetime'){
                        $value = date('Y-m-d',strtotime($values['value']));
                        $dbhour = date('H',strtotime($values['value']));
                        $dbminute = date('i',strtotime($values['value']));
                        $fieldValue = $timedate->to_db($fieldValue);
                        $fieldDate = date('Y-m-d',strtotime($fieldValue));
                        $hour = date('H',strtotime($fieldValue));
                        $minute = date('i',strtotime($fieldValue));
                        $value = $value.' '.$dbhour.':'.$dbminute;
                        $fieldValue = $fieldDate.' '.$hour.':'.$minute;
                    }else if($fieldType == 'date'){
                        $value = date('Y-m-d',strtotime($values['value']));
                        $fieldValue = date('Y-m-d',strtotime($fieldValue));
                    }else if($fieldType == 'multienum'){
                        $fieldValue = encodeMultienumValue($fieldValue);
                    }
                    if(isset($data1['relationship'])){
                        $relationshipTableName = SugarRelationshipFactory::getInstance()->getRelationshipDef($data1['relationship']); 
                    }else{
                        $relationshipTableName = '';
                    }//end of else

                    if((isset($data1['source'])) && $data1['source'] == 'custom_fields'){
                        $sel = "SELECT * FROM $tableName JOIN $customTable ON $tableName.id = $customTable.id_c WHERE $fieldName = '$fieldValue' AND deleted = 0";
                    }else if(!empty($relationshipTableName)){
                        $sel = "SELECT * FROM $tableName JOIN ".$relationshipTableName['join_table']." ON $tableName.id = ".$relationshipTableName['join_table'].".".$relationshipTableName['join_key_rhs']." WHERE $fieldName = '$fieldValue' AND ".$relationshipTableName['join_table'].".deleted = 0 AND $tableName.deleted = 0";
                    }else if($moduleName == 'Contacts' || $fieldName == 'account_id'){
                        $joinId = strtolower(get_singular_bean_name($moduleName));
                        $sel = "SELECT * FROM $tableName JOIN ".$data1['table']."_$tableName ON ".$tableName.".id = ".$data1['table']."_$tableName.".$joinId."_id WHERE $fieldName = '$fieldValue' AND $tableName.deleted = 0 AND ".$data1['table']."_$tableName.deleted = 0";  
                    }else{
                        $sel = "SELECT * FROM $tableName WHERE $fieldName = '$fieldValue' AND deleted = 0";
                    }//end of else
                    $selResult = $GLOBALS['db']->query($sel);
                    $selResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($sel));
                    if(!empty($selResultData)){
                        $result[$key] = array('actionTitle' => $actionName,'bodyHTML' => $messages);
                    }//end of if
                }//end of if
            }//end of foreach

            $relateFieldName = $_REQUEST['relateFieldName'];
            if(!empty($result)){
                foreach($result as $key => $value){
                    if($relateFieldName != ''){
                        $fields = $relateFieldName;
                    }else{
                        $fields = $fieldName;
                    }//end of else
                    if($fieldName == 'billing_account_id'){
                        $fields = 'billing_account';
                    }else if($fieldName == 'billing_contact_id'){
                        $fields = 'billing_contact';
                    }//end of else if
                    $select = "SELECT * FROM vi_conditional_notifications_conditions WHERE conditional_notifications_id = '$key' AND field = '$fields' AND deleted = 0";
                    $selectResult = $GLOBALS['db']->query($select);
                    $selectResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($select));
                    if(!empty($selectResultData)){
                        $data = array('actionTitle' => $value['actionTitle'],'bodyHTML' => $value['bodyHTML']); 
                    }//end of if 
                }//end of foreach
            }//end of if
            echo json_encode($data);
        }//end of if
    }//end of function
}//end of class
new VIConditionalNotificationsEditviewDuplicateCheckCondition();
?>