<?php

/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
require_once 'include/utils.php';
require_once 'modules/ModuleBuilder/parsers/parser.label.php';
require_once 'modules/ModuleBuilder/Module/StudioModuleFactory.php';
require_once 'modules/ModuleBuilder/parsers/views/GridLayoutMetaDataParser.php';

class Field_access_controller {

    /**
     * Description :: This function is used to check license validation.
     * 
     * @return bool '$result' - 1 - license validated
     *                          0 - license not validated
     */
    function validateLicence_FAC() {
        require_once('custom/modules/Administration/field_access_control_assign_view.php');
        $key = $_REQUEST['k'];
        $CheckResult = checkPluginLicenceFAC($key);
        return $CheckResult;
    }
    //Gets Email Address Body Of Detail View
    function get_email_address_html() {
        $sea = new SugarEmailAddress();
        $sea->setView('EditView');
        $focus = BeanFactory::getBean($_REQUEST['module_name'], $_REQUEST['record_id']);
        return $sea->getEmailAddressWidgetDetailView($focus);
    }

    //Checks Access For Email Address Field
    function check_if_access() {
        global $current_user;
        require_once('custom/modules/Administration/field_access_control_assign_view.php');
        $disabled_plugin_loginslider = checkPluginStatus_FAC();
        if (!$disabled_plugin_loginslider) {
            $acl_module = $_REQUEST['module_name'];
            require_once 'modules/access_control_fields/acl_checkaccess.php';
            require_once('custom/modules/Administration/field_access_control_assign_view.php');
            $focus = BeanFactory::getBean($_REQUEST['module_name'], $_REQUEST['record_id']);
            $obj = $focus->object_name;
            $user_id = $current_user->id;
            $is_owner = $focus->isOwner($current_user->id);
            limit_access_of_users($_REQUEST['module_name'], $obj, $user_id, $refresh = false);
            if (array_key_exists('email1', $_SESSION['ACL'][$user_id][$acl_module]['fields'])) {
                $acl_access_code = checkIfAccess('email1', $acl_module, $user_id, $is_owner);
            }
        }
        return $acl_access_code;
    }

    function manageVisiblityFieldACL() {
        require_once('modules/Administration/Administration.php');
        $enabled = $_REQUEST['enabled'];
        $administrationObj = new Administration();
        $administrationObj->retrieveSettings('FieldAccessControlPlugin');
        switch ($_REQUEST['enabled']) {
            case '1':
                $administrationObj->saveSetting("FieldAccessControlPlugin", "ModuleEnabled", 1);
                $administrationObj->saveSetting("FieldAccessControlPlugin", "LastValidationMsg", "");
                break;
            case '0':
                $administrationObj->saveSetting("FieldAccessControlPlugin", "ModuleEnabled", 0);
                $administrationObj->saveSetting("FieldAccessControlPlugin", "LastValidationMsg", "This module is disabled, to enable it go to admin section.");
                break;
            default:
                $administrationObj->saveSetting("FieldAccessControlPlugin", "ModuleEnabled", 0);
                $administrationObj->saveSetting("FieldAccessControlPlugin", "LastValidationMsg", "This module is disabled, to enable it go to admin section.");
        }
    }
    function show_field_list_by_module() {
        global $app_strings;
        require_once('modules/access_control_fields/EditView.php');
        $resetAccess = translate('LBL_RESET_ACCESES', 'access_control_fields');
        $role_obj = new ACLRole();
        $role_obj->retrieve($_REQUEST['role_id']);
        $colorPalette = "<div class='colorPaletteMainDiv'>";
        foreach ($GLOBALS['aclFieldRestrictOptionsColors'] as $key => $value) {
            $colorPalette .= "<div class='colorPaletteSubDivs' style='background-color:{$value};'></div><div class='colorPalleteSugar'><b>" .
                    translate($GLOBALS['aclFieldRestrictOptions'][$key], 'access_control_fields') . "</b></div>";
        }
        $colorPalette .= "</div>";
        $sugar_smarty_obj = new Sugar_Smarty();
        $sugar_smarty_obj->assign('ROLE', $role_obj->toArray());
        $sugar_smarty_obj->assign('APP', $app_strings);
        $sugar_smarty_obj->assign('RESET_ACCESS', $resetAccess);
        $sugar_smarty_obj->assign('COLOR_PALETTE', $colorPalette);
        $html = $sugar_smarty_obj->fetch('custom/modules/Administration/tpls/EditRole.tpl');
        $html .= access_control_fieldsshowEditView::showFieldACLView($_REQUEST['role_module'], $_REQUEST['role_id']);
        $html .= '</form>';
        return $html;
    }

    function reset_field_accesses() {
        global $db;
        $result['role_id']  = $_REQUEST['role_id'];
        $result['role_module'] = $_REQUEST['flc_module'];
        $db->query("DELETE FROM access_control_fields where role_id = '{$result['role_id']}' and category = '{$result['role_module']}'");
        return $result;
    }

    function save_field_list_by_module() {
        $result = array();
        $acl_obj = new ACLRole();
        $acl_obj->id = $_REQUEST['role_id'];
        $acl_flc_module = 'All';
        foreach ($_REQUEST as $acl_name => $acl_value) {
            if (substr_count($acl_name, 'act_guid') > 0) {
                $acl_name = str_replace('act_guid', '', $acl_name);

                $acl_obj->setAction($acl_obj->id, $acl_name, $acl_value);
            }
            if (substr_count($acl_name, 'flc_guid') > 0) {
                $acl_flc_module = $_REQUEST['flc_module'];
                $acl_name = str_replace('flc_guid', '', $acl_name);
                $id = access_control_fields::storeAccessControlFields($acl_flc_module, $acl_obj->id, $acl_name, $acl_value);
                if(!empty($id)){
                    $result['success'] = true;
                }
            }
        }
        $result['role_id'] = $acl_obj->id;
        $result['role_module'] = $acl_flc_module;
        return $result;
    }

}
