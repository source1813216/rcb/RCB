<?php 
 //WARNING: The contents of this file are auto-generated


 /*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$entry_point_registry['VIAutoPopulteFieldsFetchRelateModule'] = array(
  'file' => 'custom/VIAutoPopulateFields/VIAutoPopulteFieldsFetchRelateModule.php',
  'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsSaveConfiguration'] = array(
  'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsSaveConfiguration.php',
  'auth' => true
);

 $entry_point_registry['VIAutoPopulateFieldsSourceTargetModuleFields'] = array(
 	'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsSourceTargetModuleFields.php',
  'auth' => true
 );

$entry_point_registry['VIAutoPopulateFieldsDeleteRecord'] = array(
  'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsDeleteRecord.php',
  'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsMappingFieldType'] = array(
  'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsMappingFieldType.php',
  'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsRelateFieldName'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsRelateFieldName.php',
    'auth' => true
  );

$entry_point_registry['VIAutoPopulateFieldsRelateFieldValue'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsRelateFieldValue.php',
    'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsCheckDuplicateConfiguration'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsCheckDuplicateConfiguration.php',
    'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsDisplayCalculateFieldsFormula'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsDisplayCalculateFieldsFormula.php',
    'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsCheckCalculateFieldType'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsCheckCalculateFieldType.php',
    'auth' => true
);
$entry_point_registry['VIAutoPopulteFieldsFieldMappingBlock'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulteFieldsFieldMappingBlock.php',
    'auth' => true
);

/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
    $entry_point_registry['VIConditionalNotificationsModuleRelationships'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleRelationships.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationModuleFields'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationModuleFields.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsModuleOperatorField'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleOperatorField.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsFieldTypeOptions'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsFieldTypeOptions.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsModuleFieldType'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleFieldType.php',
        'auth' => true
    );
    $entry_point_registry['VIAddConditionalNotifications'] = array(
        'file' => 'custom/VIConditionalNotifications/VIAddConditionalNotifications.php',
        'auth' => true
    );
    $entry_point_registry['VIDeleteConditionalNotifications'] = array(
        'file' => 'custom/VIConditionalNotifications/VIDeleteConditionalNotifications.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsModuleFieldLabel'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleFieldLabel.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsEditviewCheckCondition'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsEditviewCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsDetailViewCheckCondition'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsDetailViewCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsOnSaveCheckCondition'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsOnSaveCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsEditviewDuplicateCheckCondition'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsEditviewDuplicateCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsDuplicateCheckCondition'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsDuplicateCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIConditionalNotificationsConfiguration'] = array(
        'file' => 'custom/VIConditionalNotifications/VIConditionalNotificationsConfiguration.php',
        'auth' => true
    );


/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
    $entry_point_registry['VIDynamicPanelsModuleFields'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFields.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsModuleRelationships'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleRelationships.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsModuleOperatorField'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleOperatorField.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsFieldTypeOptions'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsFieldTypeOptions.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsModuleFieldType'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFieldType.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsAllModulePanels'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsAllModulePanels.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsAllModuleFields'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsAllModuleFields.php',
        'auth' => true
    );
    $entry_point_registry['VIAddDynamicPanels'] = array(
        'file' => 'custom/VIDynamicPanels/VIAddDynamicPanels.php',
        'auth' => true
    );
    $entry_point_registry['VIDeleteDynamicPanels'] = array(
        'file' => 'custom/VIDynamicPanels/VIDeleteDynamicPanels.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsModuleFieldLabel'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFieldLabel.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsDisplayRoles'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsDisplayRoles.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsDefault'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsDefault.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsCheckCondition'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsModuleFieldValue'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFieldValue.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsDetailViewCheckCondition'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsDetailViewCheckCondition.php',
        'auth' => true
    );
    $entry_point_registry['VIDynamicPanelsConfiguration'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsConfiguration.php',
        'auth' => true
    ); 
    $entry_point_registry['VIDynamicPanelsCheckValidFieldType'] = array(
        'file' => 'custom/VIDynamicPanels/VIDynamicPanelsCheckValidFieldType.php',
        'auth' => true
    );   


/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
	$entry_point_registry['VIMultiplesmtp_Status'] = array(
        'file' => 'custom/VIMultiSMTP/VIMultiplesmtp_Status.php',
        'auth' => true
    );
    $entry_point_registry['VIOutgoing_Server_Setting'] = array(
        'file' => 'custom/VIMultiSMTP/VIOutgoing_Server_Setting.php',
        'auth' => true
    );
    $entry_point_registry['VICheckMultiSMTPStatus'] = array(
        'file' => 'custom/VIMultiSMTP/VICheckMultiSMTPStatus.php',
        'auth' => true
    );

?>