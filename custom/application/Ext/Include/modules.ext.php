<?php 
 //WARNING: The contents of this file are auto-generated

 
 //WARNING: The contents of this file are auto-generated
$beanList['VIAutoPopulateFieldsLicenseAddon'] = 'VIAutoPopulateFieldsLicenseAddon';
$beanFiles['VIAutoPopulateFieldsLicenseAddon'] = 'modules/VIAutoPopulateFieldsLicenseAddon/VIAutoPopulateFieldsLicenseAddon.php';
$modules_exempt_from_availability_check['VIAutoPopulateFieldsLicenseAddon'] = 'VIAutoPopulateFieldsLicenseAddon';
$report_include_modules['VIAutoPopulateFieldsLicenseAddon'] = 'VIAutoPopulateFieldsLicenseAddon';
$modInvisList[] = 'VIAutoPopulateFieldsLicenseAddon';


 
 //WARNING: The contents of this file are auto-generated
$beanList['VIConditionalNotificationsLicenseAddon'] = 'VIConditionalNotificationsLicenseAddon';
$beanFiles['VIConditionalNotificationsLicenseAddon'] = 'modules/VIConditionalNotificationsLicenseAddon/VIConditionalNotificationsLicenseAddon.php';
$modules_exempt_from_availability_check['VIConditionalNotificationsLicenseAddon'] = 'VIConditionalNotificationsLicenseAddon';
$report_include_modules['VIConditionalNotificationsLicenseAddon'] = 'VIConditionalNotificationsLicenseAddon';
$modInvisList[] = 'VIConditionalNotificationsLicenseAddon';


 
 //WARNING: The contents of this file are auto-generated
$beanList['VIDynamicPanelsLicenseAddon'] = 'VIDynamicPanelsLicenseAddon';
$beanFiles['VIDynamicPanelsLicenseAddon'] = 'modules/VIDynamicPanelsLicenseAddon/VIDynamicPanelsLicenseAddon.php';
$modules_exempt_from_availability_check['VIDynamicPanelsLicenseAddon'] = 'VIDynamicPanelsLicenseAddon';
$report_include_modules['VIDynamicPanelsLicenseAddon'] = 'VIDynamicPanelsLicenseAddon';
$modInvisList[] = 'VIDynamicPanelsLicenseAddon';


 
 //WARNING: The contents of this file are auto-generated
$beanList['EDP_Endorsements'] = 'EDP_Endorsements';
$beanFiles['EDP_Endorsements'] = 'modules/EDP_Endorsements/EDP_Endorsements.php';
$moduleList[] = 'EDP_Endorsements';


 
 //WARNING: The contents of this file are auto-generated
$beanList['EAP_Event_Approval'] = 'EAP_Event_Approval';
$beanFiles['EAP_Event_Approval'] = 'modules/EAP_Event_Approval/EAP_Event_Approval.php';
$moduleList[] = 'EAP_Event_Approval';


 
 //WARNING: The contents of this file are auto-generated
$beanList['EDP_Event_Documents'] = 'EDP_Event_Documents';
$beanFiles['EDP_Event_Documents'] = 'modules/EDP_Event_Documents/EDP_Event_Documents.php';
$moduleList[] = 'EDP_Event_Documents';


 
 //WARNING: The contents of this file are auto-generated
$beanList['IEP_Initial_Engagement'] = 'IEP_Initial_Engagement';
$beanFiles['IEP_Initial_Engagement'] = 'modules/IEP_Initial_Engagement/IEP_Initial_Engagement.php';
$moduleList[] = 'IEP_Initial_Engagement';


 
 //WARNING: The contents of this file are auto-generated
$beanList['LSP_Lead_Status'] = 'LSP_Lead_Status';
$beanFiles['LSP_Lead_Status'] = 'modules/LSP_Lead_Status/LSP_Lead_Status.php';
$moduleList[] = 'LSP_Lead_Status';


 
 //WARNING: The contents of this file are auto-generated
$beanList['LIP_Line_Institutes'] = 'LIP_Line_Institutes';
$beanFiles['LIP_Line_Institutes'] = 'modules/LIP_Line_Institutes/LIP_Line_Institutes.php';
$moduleList[] = 'LIP_Line_Institutes';


 
 //WARNING: The contents of this file are auto-generated
$beanList['MOU_MOU'] = 'MOU_MOU';
$beanFiles['MOU_MOU'] = 'modules/MOU_MOU/MOU_MOU.php';
$moduleList[] = 'MOU_MOU';


 
 //WARNING: The contents of this file are auto-generated
$beanList['MTS_SubpanelToggle'] = 'MTS_SubpanelToggle';
$beanFiles['MTS_SubpanelToggle'] = 'modules/MTS_SubpanelToggle/MTS_SubpanelToggle.php';
$moduleList[] = 'MTS_SubpanelToggle';


 
 //WARNING: The contents of this file are auto-generated
$beanList['VIMultiSMTPLicenseAddon'] = 'VIMultiSMTPLicenseAddon';
$beanFiles['VIMultiSMTPLicenseAddon'] = 'modules/VIMultiSMTPLicenseAddon/VIMultiSMTPLicenseAddon.php';
$modules_exempt_from_availability_check['VIMultiSMTPLicenseAddon'] = 'VIMultiSMTPLicenseAddon';
$report_include_modules['VIMultiSMTPLicenseAddon'] = 'VIMultiSMTPLicenseAddon';
$modInvisList[] = 'VIMultiSMTPLicenseAddon';


 
 //WARNING: The contents of this file are auto-generated
$beanList['STP_Sales_Target'] = 'STP_Sales_Target';
$beanFiles['STP_Sales_Target'] = 'modules/STP_Sales_Target/STP_Sales_Target.php';
$moduleList[] = 'STP_Sales_Target';


 
 //WARNING: The contents of this file are auto-generated
$beanList['STP_Stakeholder_Type'] = 'STP_Stakeholder_Type';
$beanFiles['STP_Stakeholder_Type'] = 'modules/STP_Stakeholder_Type/STP_Stakeholder_Type.php';
$moduleList[] = 'STP_Stakeholder_Type';


 
 //WARNING: The contents of this file are auto-generated
$beanList['SRP_StdApproval_Routine'] = 'SRP_StdApproval_Routine';
$beanFiles['SRP_StdApproval_Routine'] = 'modules/SRP_StdApproval_Routine/SRP_StdApproval_Routine.php';
$moduleList[] = 'SRP_StdApproval_Routine';


 
 //WARNING: The contents of this file are auto-generated
$beanList['access_control_fields'] = 'access_control_fields';
$beanFiles['access_control_fields'] = 'modules/access_control_fields/access_control_fields.php';
$modules_exempt_from_availability_check['access_control_fields'] = 'access_control_fields';
$report_include_modules['access_control_fields'] = 'access_control_fields';
$modInvisList[] = 'access_control_fields';


 
 //WARNING: The contents of this file are auto-generated
$beanList['USP_Update_Lead_Status'] = 'USP_Update_Lead_Status';
$beanFiles['USP_Update_Lead_Status'] = 'modules/USP_Update_Lead_Status/USP_Update_Lead_Status.php';
$moduleList[] = 'USP_Update_Lead_Status';


 
 //WARNING: The contents of this file are auto-generated
$beanList['VNP_Venues'] = 'VNP_Venues';
$beanFiles['VNP_Venues'] = 'modules/VNP_Venues/VNP_Venues.php';
$moduleList[] = 'VNP_Venues';


?>