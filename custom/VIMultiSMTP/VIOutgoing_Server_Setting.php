<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VIOutgoing_Server_Setting {
  public function __construct(){
    $this->outgoing_server_setting();
  }//end of construct 
  public function outgoing_server_setting(){
    $user_id = $_REQUEST['id'];
    $user_name = $_REQUEST['user_name'];
    $from_name = $_REQUEST['from_name'];
    $from_address = $_REQUEST['from_address'];
    $mail_smtpserver = $_REQUEST['mail_smtpserver'];
    $mail_smtpport = $_REQUEST['mail_smtpport'];
    $mail_smtpauth_req = $_REQUEST['mail_smtpauth_req'];
    $mail_smtpuser = $_REQUEST['mail_smtpuser'];
    $mail_smtppass = $_REQUEST['mail_smtppass'];
    $mail_smtpssl = $_REQUEST['mail_smtpssl'];
    $mail_sendtype = $_REQUEST['mail_sendtype'];
    
    if($mail_smtpauth_req == '1'){
      $mail_smtpauth_req = 1;
    }else{
      $mail_smtpauth_req = 0;
    }

    $selSMTPSettings = "SELECT * FROM vi_st_multiple_smtp WHERE user_id = '$user_id'";
    $result = $GLOBALS['db']->query($selSMTPSettings);
    $resultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selSMTPSettings));
    if(empty($resultData)) {
        $insSMTPSettings = "INSERT INTO vi_st_multiple_smtp(name,user_id,smtp_from_name,smtp_from_addr,mail_smtpserver,mail_smtpport,mail_smtpuser,mail_smtppass,mail_smtpauth_req,mail_smtpssl,mail_sendtype)values('$user_name','$user_id','$from_name','$from_address','$mail_smtpserver','$mail_smtpport',' $mail_smtpuser','$mail_smtppass',$mail_smtpauth_req,'$mail_smtpssl','$mail_sendtype')";
         $result1 = $GLOBALS['db']->query($insSMTPSettings);
    } else {
        $updateSMTPSettings = "UPDATE vi_st_multiple_smtp
                          SET name = '$user_name',
                              smtp_from_name = '$from_name',
                              smtp_from_addr = '$from_address',
                              mail_smtpserver = '$mail_smtpserver',
                              mail_smtpport = '$mail_smtpport',
                              mail_smtpuser = '$mail_smtpuser',
                              mail_smtppass = '$mail_smtppass',
                              mail_smtpauth_req = $mail_smtpauth_req,
                              mail_smtpssl = '$mail_smtpssl',
                              mail_sendtype = '$mail_sendtype'
                         WHERE user_id = '$user_id'";
        $updateResult = $GLOBALS['db']->query($updateSMTPSettings);
    }
  }//end of function
}//end of class
new VIOutgoing_Server_Setting();
?>