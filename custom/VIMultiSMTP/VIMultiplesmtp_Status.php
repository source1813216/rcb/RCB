<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VIMultiplesmtp_Status{
    public function __construct(){
    	$this->multiple_smtp_status();
    } 
    public function multiple_smtp_status(){
    	$value = $_REQUEST['val'];
        $selSettings = "SELECT *
                    FROM vi_multiple_smtp_settings";
        $result = $GLOBALS['db']->query($selSettings);
        $resultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selSettings));
        if(empty($resultData)) {
            $queryEnableSettings = "INSERT INTO vi_multiple_smtp_settings(enable) values('$value')";                     
            $resultEnableSettings = $GLOBALS['db']->query($queryEnableSettings); 
        }else {
            $queryUpdateSettings = "UPDATE vi_multiple_smtp_settings 
                                        SET enable = '$value'";                   
            $resultUpdateSettings = $GLOBALS['db']->query($queryUpdateSettings);
        } 
    }//end of function
}//end of class
new VIMultiplesmtp_Status();
?>