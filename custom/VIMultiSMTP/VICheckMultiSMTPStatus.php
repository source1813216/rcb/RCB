<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VICheckMultiSMTPStatus{
    public function __construct(){
    	$this->checkMultiSMTPStatus();
    } 
    public function checkMultiSMTPStatus(){
    	$selSettings = "SELECT *
                    FROM vi_multiple_smtp_settings";
        $resultData = $GLOBALS['db']->fetchOne($selSettings);
        if(empty($resultData)) {
            echo translate('LBL_ENABLED_MULTI_SMTP_MSG','AOW_Actions'); 
        }else {
          	if($resultData['enable'] == '0'){
          		echo translate('LBL_ENABLED_MULTI_SMTP_MSG','AOW_Actions');
          	} 
        } 
    }//end of function
}//end of class
new VICheckMultiSMTPStatus();
?>