<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
include("custom/VIDynamicPanels/VIDynamicPanelsFunctions.php");
class VIDynamicPanelsCheckCondition{
    public function __construct(){
        $this->dynamicPanelsCheckCondition();
    } 
    public function dynamicPanelsCheckCondition(){
        global $current_user,$app_strings,$mod_strings,$sugar_config;
        $databaseType = $sugar_config['dbconfig']['db_type'];
        $currentUserId = $current_user->id; //user id
        $currentUserType = $current_user->is_admin; //user type
        
        if($currentUserType == 1){
            $currentUserTypeName = 'Administrator';
        }else if($currentUserType == 0){
            $currentUserTypeName = 'RegularUser';
        }//end of else if

        //get data
        $actionName = $_REQUEST['actionName'];
        $moduleName = $_REQUEST['module'];
        $recordId = '';
        if(isset($_REQUEST['recordId']) && !empty($_REQUEST['recordId'])){
            $recordId = $_REQUEST['recordId'];
        }else{
            $recordId = '';
        }//end of else

        if(isset($_REQUEST['fieldName']) && !empty($_REQUEST['fieldName'])){
            $fieldName = $_REQUEST['fieldName']; 
        }else{
            $fieldName = '';
        }//end of else
        

        //get user roles
        $selectRoles = "SELECT * FROM acl_roles_users WHERE user_id = '$currentUserId' AND deleted = 0";
        $selectRoleResult = $GLOBALS['db']->query($selectRoles);
        $selectRoleData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selectRoles));
        $currentUserRoleId = array();
        if(!empty($selectRoleData)){
            while($selectRolesRow = $GLOBALS['db']->fetchByAssoc($selectRoleResult)){
                $currentUserRoleId[] = $selectRolesRow['role_id']; //userrole id
            }
        }else{
            $currentUserRoleId = array();
        }//end of else
        
        $fieldData = array();
        $fieldDef = array();
        $datas = array();
        $data = array();
        $autoPopulateFieldData = array();
        $autoPopulateFieldArray =array();
        $conditionalOperator = array();
        $condition = array();
        parse_str($_REQUEST['formData'],$formData);

        $bean = BeanFactory::newBean($moduleName); //bean

        //get dynamic panels configuration
        $selDynamicPanels = "SELECT * FROM vi_dynamic_panels WHERE primary_module = '$moduleName' 
                                                             AND (user_type = 'AllUsers' OR user_type = '$currentUserTypeName') AND deleted = 0";
        $selDynamicPanelsResult = $GLOBALS['db']->query($selDynamicPanels);
        $selDynamicPanelsResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selDynamicPanels));
        if(!empty($selDynamicPanelsResultData)){
            while($selDynamicPanelsRow = $GLOBALS['db']->fetchByAssoc($selDynamicPanelsResult)){
                $roleId = explode(',',$selDynamicPanelsRow['role_id']);
                $flag = 0;
                foreach ($roleId as $key => $value) {
                    if(in_array($value,$currentUserRoleId)){
                        $flag = 1;
                    }
                }
                if($flag == 1){
                    $dynamicPanelsId = $selDynamicPanelsRow['dynamic_panels_id']; //dynamic panels id

                    $conditionalOperator[$dynamicPanelsId] = $selDynamicPanelsRow['conditional_operator']; 

                    //get dynamic panels conditions data
                    $selDynamicPanelsCondition = "SELECT * FROM vi_dynamic_panels_condition 
                                                           WHERE dynamic_panels_id = '$dynamicPanelsId' 
                                                           AND deleted = 0";
                    $selDynamicPanelsConditionResult = $GLOBALS['db']->query($selDynamicPanelsCondition);
                    $selDynamicPanelsConditionResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selDynamicPanelsCondition));
                    if(!empty($selDynamicPanelsConditionResultData)){
                        $dynamicPanelsConditionArray = array();
                        while($selConditionRow = $GLOBALS['db']->fetchByAssoc($selDynamicPanelsConditionResult)){
                            $fieldDefs = $bean->field_defs[$selConditionRow['field']];
                            if($fieldDefs['type'] == 'relate'){
                                $field = $fieldDefs['id_name'];
                            }else{
                                $field = $selConditionRow['field'];
                            }//end of else

                            $dynamicPanelsConditionArray[$dynamicPanelsId][] =  array(
                                                                                    'dynamicPanelsId' => $dynamicPanelsId,
                                                                                    'modulePath' => $selConditionRow['module_path'],
                                                                                    'field' => $field,
                                                                                    'operator'=> $selConditionRow['operator'],
                                                                                    'valueType' => $selConditionRow['value_type'],
                                                                                    'value' => $selConditionRow['value']
                                                                                );
                        }//end of while 

                        //match condition 
                        $matchData = matchDynamicPanelsCondition($formData,$dynamicPanelsConditionArray,$fieldName,$moduleName,$conditionalOperator,$recordId);
                        foreach ($matchData as $k => $v) {
                            $matchConditionArray[$k] = $v;
                        }  
                    }//end of if
                    $condition[$dynamicPanelsId] = '1';
                }
            }//end of while 
        }//end of if
        
        
        foreach ($condition as $ck => $val) {
            if(array_key_exists($ck,$matchConditionArray)){
                //get dynamic panels data
                $selDynamicPanelsData = "SELECT * FROM vi_dynamic_panels WHERE dynamic_panels_id = '$ck'";
                $selDynamicPanelsDataRow = $GLOBALS['db']->fetchOne($selDynamicPanelsData);

                //data
                $panelHide = explode(',',$selDynamicPanelsDataRow['panel_hide']);
                $panelShow = explode(',',$selDynamicPanelsDataRow['panel_show']);

                if($actionName != 'EditView'){
                    $panelArray = getEditviewDetailviewPanels('quickcreate',$moduleName);
                    $tabArray = getEditviewDetailviewTabs('quickcreate',$moduleName);
                    $requiredFieldsArray = getEditviewDetailviewRequiredFields('quickcreate',$moduleName);
                }else{
                    $panelArray = getEditviewDetailviewPanels('editview',$moduleName);
                    $tabArray = getEditviewDetailviewTabs('editview',$moduleName);
                    $requiredFieldsArray = getEditviewDetailviewRequiredFields('editview',$moduleName);
                }//end of else

                //field mandatory
                $fieldsMandatory = explode(',',$selDynamicPanelsDataRow['field_mandatory']);
                $fieldsMandatoryArray = array();
                foreach ($fieldsMandatory as $key => $value) {
                    if($value != ''){
                        if(!in_array($value,$requiredFieldsArray)){
                            $mandatoryFieldData = $bean->field_defs[$value];
                            $type = $mandatoryFieldData['type'];
                            $fieldsMandatoryArray[] = array('field' => $value,'type' => $type);
                        }//end of if    
                    }//end of if
                }//end of foreach

                //field readonly
                $fieldsReadOnly = explode(',',$selDynamicPanelsDataRow['field_readonly']);
                $fieldsReadOnlyArray = array();
                foreach ($fieldsReadOnly as $key => $value) {
                    if($value != ''){
                        $readOnlyFieldData = $bean->field_defs[$value];
                        $type = $readOnlyFieldData['type'];
                        $fieldsReadOnlyArray[] = array('field' => $value,'type' => $type);
                    }//end of if
                }//end of foreach

                //display all panels
                foreach($panelArray as $key=>$value){
                    $key = strtoupper($key);
                    $displayAllPanels[] = translate($key,$moduleName);
                }//end of foreach

                //display all fields
                foreach($panelArray as $key=>$value){
                    $key = strtoupper($key);
                    foreach ($value as $keys => $values) {
                        foreach ($values as $field) {
                            $displayAllFields[] = $field;
                        }//end of foreach
                    }//end of foreach
                }//end of foreach

                $editViewTabArray = array();
                foreach ($tabArray as $key => $value) {
                    $key = strtoupper($key);
                    if($value['newTab'] == 1){
                        $editViewTabArray[] = translate($key,$moduleName);
                    }//end of if
                }//end of foreach

                if($val == $matchConditionArray[$ck]){
                    $panelFieldArray = array();
                    foreach($panelArray as $key=>$value){
                        $key = strtoupper($key);
                        foreach ($value as $values) {
                            if(in_array($key,$panelHide)){
                                foreach ($values as $field) {
                                    $panelFieldArray[] = $field;
                                }//end of foreach   
                            }//end of if
                        }//end of foreach
                    }//end of foreach

                    //remove default panels required fields validation which is hide 
                    $removeFromValidatePanelFields = array_values(array_intersect($panelFieldArray, $requiredFieldsArray));
                    
                    //panel fields show
                    $panelShowFieldArray = array();
                    foreach($panelArray as $key=>$value){
                        $key = strtoupper($key);
                        foreach ($value as $values) {
                            if(in_array($key,$panelShow)){
                                foreach ($values as $field) {
                                    $panelShowFieldArray[] = $field;
                                }//end of foreach   
                            }//end of if
                        }//end of foreach
                    }//end of foreach

                    //panel hide
                    $panelsHideArray = array();
                    foreach($panelArray as $key=>$value){
                        $key = strtoupper($key);
                        if(in_array($key,$panelHide)){
                            $panelsHideArray[] = translate($key,$moduleName);
                        }//end of if
                    }//end of foreach

                    $editViewTabsHide = array();
                    $editViewPanelsHide = array();
                    foreach ($panelsHideArray as $key => $value) {
                        if(in_array($value,$editViewTabArray)){
                            $editViewTabsHide[] = $value;
                        }else{
                            $editViewPanelsHide[] = $value;
                        }//end of else
                    }//end of foreach

                    //panel show
                    $panelShowArray = array();
                    foreach($panelArray as $key=>$value){
                        $key = strtoupper($key);
                        if(in_array($key,$panelShow)){
                            $panelShowArray[] = translate($key,$moduleName);
                        }//end of if
                    }//end of foreach

                    $editViewTabsShow = array();
                    $editViewPanelsShow = array();
                    foreach ($panelShowArray as $key => $value) {
                        if(in_array($value,$editViewTabArray)){
                            $editViewTabsShow[] = $value;
                        }else{
                            $editViewPanelsShow[] = $value;
                        }//end of else
                    }//end of foreach

                    $addToValidatePanelFieldsShow = array();
                    $addToValidatePanelFields = array_values(array_intersect($panelShowFieldArray, $requiredFieldsArray));
                    foreach ($addToValidatePanelFields as $key => $value) {
                        $validatePanelField = $bean->field_defs[$value];
                        $type = $validatePanelField['type'];
                        $addToValidatePanelFieldsShow[] = array('field' => $value,'type' => $type); 
                    }//end of foreach

                    //field hide
                    $fieldsHide = explode(',',$selDynamicPanelsDataRow['field_hide']);
                    $fieldsHideArray = array();
                    foreach ($fieldsHide as $key => $value) {
                        $fieldsHideArray[] = $value;
                    }//end of foreach

                    //remove fields required validation which is hide
                    $removeFromValidateFieldsHide = array_values(array_intersect($fieldsHideArray, $requiredFieldsArray));

                    //field show
                    $fieldsShow = explode(',',$selDynamicPanelsDataRow['field_show']);
                    $fieldsShowArray = array();
                    foreach ($fieldsShow as $key => $value) {
                        $fieldsShowArray[] = $value;
                    }//end of foreach

                    $addToValidateFieldsShow = array();
                    $addToValidateFields  = array_values(array_intersect($fieldsShowArray, $requiredFieldsArray));
                    foreach ($addToValidateFields as $key => $value) {
                        $validateField = $bean->field_defs[$value];
                        $type = $validateField['type'];
                        $addToValidateFieldsShow[] = array('field' => $value,'type' => $type);
                    }//end of foreach

                    $dynamicPanelsId = $selDynamicPanelsDataRow['dynamic_panels_id'];
                    
                    //autopopulate fields
                    $autoPopulateFieldArray = getDynamicPanelsAutoPopulateFieldsArray($dynamicPanelsId,$moduleName);
                    
                    //fields color
                    $fieldColorrArray = getDynamicPanelsFieldsColorArray($dynamicPanelsId,$moduleName);
                    
                    $data[$dynamicPanelsId] = array('success' => 'true',
                                                     'editViewTabsHide' => $editViewTabsHide,
                                                     'editViewPanelsHide' => $editViewPanelsHide,
                                                     'editViewTabsShow' => $editViewTabsShow,
                                                     'editViewPanelsShow' => $editViewPanelsShow,
                                                     'removeFromValidatePanelFields' => $removeFromValidatePanelFields,
                                                     'fieldsShowArray' => $fieldsShowArray,
                                                     'fieldsHideArray' => $fieldsHideArray,
                                                     'addToValidatePanelFieldsShow' => $addToValidatePanelFieldsShow,
                                                     'removeFromValidateFieldsHide' =>$removeFromValidateFieldsHide,
                                                     'addToValidateFieldsShow' =>$addToValidateFieldsShow,
                                                     'fieldsMandatoryArray' => $fieldsMandatoryArray,
                                                     'fieldsReadOnlyArray' => $fieldsReadOnlyArray,
                                                     'autoPopulateFieldArray' => $autoPopulateFieldArray,
                                                     'fieldColorrArray' => $fieldColorrArray
                                                );  
                }else{
                    $selDynamicPanelsDatas = "SELECT * FROM vi_dynamic_panels WHERE primary_module = '$moduleName' 
                                                                              AND (user_type = 'AllUsers' OR user_type = '$currentUserTypeName')  AND deleted = 0
                                                                              ORDER BY date_created ASC";
                    $selDataResult = $GLOBALS['db']->query($selDynamicPanelsDatas);
                    $selResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selDynamicPanelsDatas));
                    $configData = array();
                    if(!empty($selResultData)){
                        while($selDataRow = $GLOBALS['db']->fetchByAssoc($selDataResult)){
                            $roleId = explode(',',$selDataRow['role_id']);
                            $flag = 0;
                            foreach ($roleId as $key => $value) {
                                if(in_array($value,$currentUserRoleId)){
                                    $flag = 1;
                                    $configData[$selDataRow['dynamic_panels_id']] = $selDataRow['dynamic_panels_id'];
                                }
                            }
                        }
                    }
                    $dynamicPanelsIdData = reset($configData);
                    $selData = "SELECT * FROM vi_dynamic_panels WHERE dynamic_panels_id = '$dynamicPanelsIdData'";
                    $selDynamicPanelsDataRowss = $GLOBALS['db']->fetchOne($selData); 
                    
                    $defaultPanels = explode(',',$selDynamicPanelsDataRowss['default_panel_hide']);

                    $allPanel = array();
                    foreach($panelArray as $key=>$value){
                        $key = strtoupper($key);
                        $allPanel[] = $key; 
                    }//end of foreach

                    $defaultPanelsHide = array();
                    //default panels
                    foreach($panelArray as $key=>$value){
                        $key = strtoupper($key);
                        if (isset($mod_strings[$key])){
                            if(in_array($key,$defaultPanels)){
                                $defaultPanelsHide[] = $key;
                            }//end of if
                        }//end of if
                        if (isset($app_strings[$key])){
                            if(in_array($key,$defaultPanels)){
                                $defaultPanelsHide[] = $key;
                            }//end of if 
                        }//end of if
                    }//end of foreach

                    $defaultPanelsHideArray = array();
                    $defaultPanelsShowArray = array();
                    foreach($allPanel as $key => $value){
                        if(in_array($value,$defaultPanelsHide)){
                            $defaultPanelsHideArray[] = translate($value,$moduleName);
                        }else{
                            $defaultPanelsShowArray[] = translate($value,$moduleName);
                        }//end of else
                    }//end of foreach
                    
                    $defaulteditViewTabsHide = array();
                    $defaulteditViewPanelsHide = array();
                    foreach ($defaultPanelsHideArray as $key => $value) {
                        if(in_array($value,$editViewTabArray)){
                            $defaulteditViewTabsHide[] = $value;
                        }else{
                            $defaulteditViewPanelsHide[] = $value;
                        }//end of else
                    }//end of foreach

                    $defaulteditViewTabsShow = array();
                    $defaulteditViewPanelsShow = array();
                    foreach ($defaultPanelsShowArray as $key => $value) {
                        if(in_array($value,$editViewTabArray)){
                            $defaulteditViewTabsShow[] = $value;
                        }else{
                            $defaulteditViewPanelsShow[] = $value;
                        }//end of else
                    }//end of foreach

                    $defaultPanelHideFields = array();
                    foreach($panelArray as $key=>$value){
                        $key = strtoupper($key);
                        foreach ($value as $values) {
                            if(in_array($key,$defaultPanelsHideArray)){
                                foreach ($values as $field) {
                                    $defaultPanelHideFields[] = $field;
                                }//end of foreach   
                            }//end of if
                        }//end of foreach
                    }//end of foreach

                    $defaultPanelShowFields = array();
                    foreach($panelArray as $key=>$value){
                        $key = strtoupper($key);
                        foreach ($value as $values) {
                            if(in_array($key,$defaultPanelsShowArray)){
                                foreach ($values as $field) {
                                    $defaultPanelShowFields[] = $field;
                                }//end of foreach   
                            }//end of if
                        }//end of foreach
                    }//end of foreach

                    $removeFromValidatePanelHideFields = array_intersect($defaultPanelHideFields,$requiredFieldsArray);

                    $addToValidatePanelFields = array_intersect($defaultPanelShowFields,$requiredFieldsArray);
                    $addToValidateFieldsShow = array();
                    foreach ($addToValidatePanelFields as $key => $value) {
                        $validateField = $bean->field_defs[$value];
                        $type = $validateField['type'];
                        $addToValidateFieldsShow[] = array('field' => $value,'type' => $type);
                    }//end of foreach

                    $defaultFields = explode(',',$selDynamicPanelsDataRowss['default_field_hide']);
                    $field = $bean->getFieldDefinitions();

                    $data1 = array();
                    foreach($field as $value){
                        if($value['type'] == 'varchar'){
                            if(isset($value['group']) && !empty($value['group'])){
                                $fieldName = $value['name'];
                                $data1[] = $fieldName;
                            }//end of if
                        }//end of if
                    }//end of foreach

                    $editViewFieldArray = array();
                    foreach ($panelArray as $key => $value) {
                        foreach ($value as $keys => $values) {
                            $editViewFieldArray[] = $values;
                        }//end of foreach
                    }//end of foreach

                    $fieldDataArray = array();
                    foreach($editViewFieldArray as $key => $value) {
                        foreach($value as $k => $v) {
                            if(array_key_exists($v, $field)) {
                                $fieldName = $v;
                                $fieldDataArray[] = $fieldName;
                            }//end of if
                        }//end of foreach
                    }//end of foreach

                    $arrayMerge = array_merge($fieldDataArray,$data1);
                    $allFields = array_unique($arrayMerge,SORT_REGULAR);
                    $defaultFieldsHideArray = array();
                    $defaultFieldsShowArray = array();
                    foreach ($allFields as $key => $value) {
                        if(in_array($value,$defaultFields)){
                            $defaultFieldsHideArray[] = $value;
                        }//end of if
                    }//end of foreach

                    $removeFromValidateDefaultFields = array_values(array_intersect($defaultFieldsHideArray,$requiredFieldsArray));
                    $addToValidateDefaultField = array();
                    $addToValidateDefaultFields = array();
                    foreach($addToValidateDefaultField as $key => $value) {
                        $validateField = $bean->field_defs[$value];
                        $type = $validateField['type'];
                        $addToValidateDefaultFields[] = array('field' => $value,'type' => $type);
                    }//end of foreach

                    $dynamicPanelsId = $selDynamicPanelsDataRow['dynamic_panels_id'];

                    //autopopulate fields
                    $autoPopulateFieldArray = getDynamicPanelsAutoPopulateFieldsArray($dynamicPanelsId,$moduleName);

                    //fields color
                    $fieldColorrArray = getDynamicPanelsFieldsColorArray($dynamicPanelsId,$moduleName);

                    $datas[$dynamicPanelsId] = array('success' => 'false',
                                    'displayAllPanels' => $displayAllPanels,
                                    'displayAllFields' => $displayAllFields,
                                    'defaulteditViewTabsHide' => $defaulteditViewTabsHide,
                                    'defaulteditViewPanelsHide' => $defaulteditViewPanelsHide,
                                    'defaulteditViewTabsShow' => $defaulteditViewTabsShow,
                                    'defaulteditViewPanelsShow' => $defaulteditViewPanelsShow,
                                    'defaultPanelsShowArray' => $defaultPanelsShowArray,
                                    'removeFromValidatePanelHideFields' => $removeFromValidatePanelHideFields,
                                    'fieldsNonMandatoryArray' => $fieldsMandatoryArray,
                                    'addToValidateFieldsShow' => $addToValidateFieldsShow,
                                    'defaultFieldsHideArray' => $defaultFieldsHideArray,
                                    'defaultFieldsShowArray' => $defaultPanelShowFields,
                                    'removeFromValidateDefaultFields' => $removeFromValidateDefaultFields,
                                    'removeReadOnly' => $fieldsReadOnlyArray,
                                    'removeautoPopulateFieldValue' => $autoPopulateFieldArray,
                                    'fieldColorrArray' => $fieldColorrArray);
                }
            }//end of if
        }//end of foreach

        $result = array_merge($datas,$data);
        if(empty($result)){
            if(empty($datas)){
                $resultArray = $data;
            }else{
                $resultArray = $datas;
            }
        }else{
            $resultArray = $result;
        }
        echo json_encode($resultArray);
    }//end of function  
}//end of class
new VIDynamicPanelsCheckCondition();
?>

