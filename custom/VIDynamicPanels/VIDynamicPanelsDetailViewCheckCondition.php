<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
include("custom/VIDynamicPanels/VIDynamicPanelsFunctions.php");
class VIDynamicPanelsDetailViewCheckCondition{
    public function __construct(){
        $this->dynamicPanelsCheckCondition();
    } 
    public function dynamicPanelsCheckCondition(){
        global $current_user;
        $currentUserId = $current_user->id;
        $currentUserType = $current_user->is_admin;
        if($currentUserType == 1){
            $currentUserTypeName = 'Administrator';
        }else if($currentUserType == 0){
            $currentUserTypeName = 'RegularUser';
        }
        $selectRoles = "SELECT * FROM acl_roles_users WHERE user_id = '$currentUserId' AND deleted = 0";
        $selectRoleResult = $GLOBALS['db']->query($selectRoles);
        $selectRoleData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selectRoles));
        $currentUserRoleId = array();
        if(!empty($selectRoleData)){
            while($selectRolesRow = $GLOBALS['db']->fetchByAssoc($selectRoleResult)){
                $currentUserRoleId[] = $selectRolesRow['role_id']; //userrole id
            }
        }else{
            $currentUserRoleId = array();
        }//end of else
        
        $moduleName = $_REQUEST['module'];
        $recordId = $_REQUEST['recordId'];      
        $dynamicPanelsConditionArray = array();
        $conditionalOperator = array();
        $bean = BeanFactory::newBean($moduleName);
        $selDynamicPanels = "SELECT * FROM vi_dynamic_panels WHERE primary_module = '$moduleName' AND (user_type = 'AllUsers' OR user_type = '$currentUserTypeName')";
        $selDynamicPanelsResult = $GLOBALS['db']->query($selDynamicPanels);
        $selDynamicPanelsResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selDynamicPanels));
        if(!empty($selDynamicPanelsResultData)){
            while($selDynamicPanelsRow = $GLOBALS['db']->fetchByAssoc($selDynamicPanelsResult)){
                    $roleId = explode(',',$selDynamicPanelsRow['role_id']);
                    $flag = 0;
                    foreach ($roleId as $key => $value) {
                        if(in_array($value,$currentUserRoleId)){
                            $flag = 1;
                        }
                    }
                    if($flag == 1){
                        $dynamicPanelsId = $selDynamicPanelsRow['dynamic_panels_id'];
                        $conditionalOperator[$dynamicPanelsId] = $selDynamicPanelsRow['conditional_operator']; 
                        $selDynamicPanelsCondition = "SELECT * FROM vi_dynamic_panels_condition 
                                                               WHERE dynamic_panels_id = '$dynamicPanelsId' 
                                                               AND deleted = 0";
                        $selDynamicPanelsConditionResult = $GLOBALS['db']->query($selDynamicPanelsCondition);
                        $selDynamicPanelsConditionResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selDynamicPanelsCondition));
                        if(!empty($selDynamicPanelsConditionResultData)){
                            while($selDynamicPanelsConditionRow = $GLOBALS['db']->fetchByAssoc($selDynamicPanelsConditionResult)){
                                $dynamicPanelsId = $selDynamicPanelsConditionRow['dynamic_panels_id'];
                                $modulePath = $selDynamicPanelsConditionRow['module_path'];
                                $field = $selDynamicPanelsConditionRow['field'];
                                $fieldData = $bean->field_defs[$field];
                                if($fieldData['type'] == 'relate'){
                                    $field = $fieldData['id_name'];
                                }
                                $value = $selDynamicPanelsConditionRow['value']; 
                                if($moduleName == 'Meetings'){
                                    if($field == 'duration'){
                                        $minutes = ($value / 60) % 60;
                                        if($minutes > 60){
                                            $field = 'duration_hours';
                                            $value = floor($value / 3600);
                                        }else{
                                            $field = 'duration_minutes';
                                            $value = $minutes;
                                        }
                                    }
                                } 
                                $operator = $selDynamicPanelsConditionRow['operator'];
                                $valueType = $selDynamicPanelsConditionRow['value_type'];
                                
                                $dynamicPanelsConditionArray[$dynamicPanelsId][] = array('dynamicPanelsId' => $dynamicPanelsId,
                                                                       'modulePath' => $modulePath,
                                                                       'field' => $field,
                                                                       'operator'=> $operator,
                                                                       'valueType' => $valueType,
                                                                       'value' =>$value);
                            }   
                        }
                    }
            } 
        }
        $data = array();
        if($recordId != ''){
            $matchConditionArray = array();
            global $timedate,$app_list_strings;
            $recordBean = BeanFactory::getBean($moduleName,$recordId);
            foreach ($dynamicPanelsConditionArray as $key => $value) {
                $matchCondition = array();
                foreach ($value as $keys => $values) {
                    $fieldDef = $recordBean->field_defs[$values['field']];
                    $fieldType = $fieldDef['type'];
                    $operator = $values['operator'];
                    $field = $values['field'];
                    $recordValue = $recordBean->$field;
                    $fieldValue = $values['value'];
                    $valueType = $values['valueType'];


                    $dataField = getRecordFieldValue($valueType,$values,$recordBean,$recordId,'',$moduleName,$fieldType,$fieldValue,$recordValue);

                    $fieldValue = $dataField['value'];
                    $recordValue = $dataField['fieldValue'];

                    if($fieldValue != '' || $recordValue != ''){
                        switch ($operator) {
                            case 'Equal_To':
                                    if($fieldType == 'multienum'){
                                        if($values['valueType'] == 'Value'){
                                            if($recordValue == $fieldValue){
                                                $matchCondition[] = '1';
                                            }else{
                                                $matchCondition[] = '0';
                                            }
                                        }else{
                                            $fieldValue = unencodeMultienum($fieldValue);
                                            $recordValue = unencodeMultienum($recordValue);
                                            $result = array_intersect($recordValue,$fieldValue);
                                            if(empty($result)){
                                                $matchCondition[] = '0';
                                            }else{
                                                $matchCondition[] = '1';
                                            }
                                        }
                                    }else if($fieldType == 'enum'){
                                        if($values['valueType'] == 'Value'){
                                            if($recordValue == $fieldValue){
                                                $matchCondition[] = '1';
                                            }else{
                                                $matchCondition[] = '0';
                                            }
                                        }else{
                                            $fieldValue = unencodeMultienum($fieldValue);
                                            if(!in_array($recordValue,$fieldValue)){
                                                $matchCondition[] = '0';
                                            }else{
                                                $matchCondition[] = '1';
                                            }
                                        }
                                    }else{
                                        if($recordValue == $fieldValue){
                                            $matchCondition[] = '1';
                                        }else{
                                            $matchCondition[] = '0';
                                        }
                                    }    
                                    break;
                            case 'Not_Equal_To':
                                    if($fieldType == 'multienum'){
                                        if($values['valueType'] == 'Value'){
                                            if($recordValue != $fieldValue){
                                                $matchCondition[] = '1';
                                            }else{
                                                $matchCondition[] = '0';
                                            } 
                                        }else{
                                            $fieldValue = unencodeMultienum($fieldValue);
                                            $recordValue = unencodeMultienum($recordValue);
                                            $result = array_intersect($recordValue,$fieldValue);
                                            if(empty($result)){
                                                $matchCondition[] = '1';
                                            }else{
                                                $matchCondition[] = '0';
                                            }
                                        }
                                    }else if($fieldType == 'enum'){
                                        if($values['valueType'] == 'Value'){
                                            if($recordValue != $fieldValue){
                                                $matchCondition[] = '1';
                                            }else{
                                                $matchCondition[] = '0';
                                            }  
                                        }else{
                                            $fieldValue = unencodeMultienum($fieldValue);
                                            if(!in_array($recordValue,$fieldValue)){
                                                $matchCondition[] = '1';
                                            }else{
                                                $matchCondition[] = '0';
                                            }
                                        }
                                    }else{
                                        if($recordValue != $fieldValue){
                                            $matchCondition[] = '1';
                                        }else{
                                            $matchCondition[] = '0';
                                        }  
                                    }
                                    break;
                            case 'is_null':
                                if(empty($recordValue)){
                                    $matchCondition[] = '1';    
                                }else{
                                    $matchCondition[] = '0';
                                }
                                break;
                            case 'is_not_null':
                                if(!empty($recordValue)){
                                    $matchCondition[] = '1';    
                                }else{
                                    $matchCondition[] = '0';
                                }
                                break;
                            case 'Greater_Than':
                                if($recordValue > $fieldValue){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }
                                break;
                            case 'Less_Than':
                                if($recordValue < $fieldValue){
                                    $matchCondition[] = '1'; 
                                }else{
                                    $matchCondition[] = '0';
                                }
                                break;
                            case 'Greater_Than_or_Equal_To':
                                if($recordValue >= $fieldValue){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }
                                break;
                            case 'Less_Than_or_Equal_To':
                                if($recordValue <= $fieldValue){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }
                                break;
                            case 'Contains':
                                if($this->contains($recordValue,$fieldValue)){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }
                                break;
                            case 'Starts_With':
                                if($this->startsWith($recordValue,$fieldValue)){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }
                                break;
                            case 'Ends_With':
                                if($this->endsWith($recordValue,$fieldValue)){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }
                                break;
                            default:
                                echo "";
                                break;
                        }    
                    }else{
                        $matchCondition[] = '0';
                    }
                    
                    if(!empty($matchCondition)){
                        if($conditionalOperator[$key] == 'AND'){
                            if(in_array('0',$matchCondition)){
                                $matchConditionArray[$key] = '0'; 
                            }else{
                                $matchConditionArray[$key] = '1';
                            }    
                        }else{
                            if(in_array('1',$matchCondition)){
                                $matchConditionArray[$key] = '1'; 
                            }else{
                                $matchConditionArray[$key] = '0';
                            } 
                        }   
                    }
                }
            }
            $condition = array();
            foreach ($dynamicPanelsConditionArray as $key => $value) {
                $condition[$key] = '1';
            }
            foreach ($condition as $key => $value) {
                if(array_key_exists($key,$matchConditionArray)){
                    if($value == $matchConditionArray[$key]){
                        require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
                        global $app_strings,$mod_strings;
                        $selDynamicPanelsData = "SELECT * FROM vi_dynamic_panels WHERE dynamic_panels_id = '$key'";
                        $selDynamicPanelsDataResult = $GLOBALS['db']->query($selDynamicPanelsData);
                        $selDynamicPanelsDataRow = $GLOBALS['db']->fetchByAssoc($selDynamicPanelsDataResult);
                        $dynamicPanelsId = $selDynamicPanelsDataRow['dynamic_panels_id'];
                        $panelHide = $selDynamicPanelsDataRow['panel_hide'];
                        $panelHide = explode(',',$panelHide);

                        $panelShow = $selDynamicPanelsDataRow['panel_show'];
                        $panelShow = explode(',',$panelShow);
                        $editview_array = ParserFactory::getParser('editview',$moduleName);
                        $editViewPanelArray = $editview_array->_viewdefs['panels'];
                        
                        
                        $detailview_array = ParserFactory::getParser('detailview',$moduleName);
                        $detailViewPanelArray = $detailview_array->_viewdefs['panels'];
                        $tabArrays = $detailview_array->_viewdefs['templateMeta']['tabDefs'];
                        $detailViewTabsArray = array();
                        foreach ($tabArrays as $key => $value) {
                            $key = translate(strtoupper($key),$moduleName);
                            if($value['newTab'] == 1){
                                $detailViewTabsArray[] = $key;
                            }   
                        }
                        $arrayMerge = array_merge($detailViewPanelArray,$editViewPanelArray);
                        $panelArray = array_unique($arrayMerge,SORT_REGULAR);

                        //panel hide
                        $panelsHideArray = array();
                        foreach($panelArray as $key=>$value){
                            $key = strtoupper($key);
                            if(in_array($key,$panelHide)){
                                $panelsHideArray[] = translate($key,$moduleName);
                            }
                        }
                        $detailViewTabsHide = array();
                        $detailViewPanelsHide = array();
                        foreach($panelsHideArray as $key => $value) {
                            if(in_array($value,$detailViewTabsArray)){
                                $detailViewTabsHide[] = $value;
                            }else{
                                $detailViewPanelsHide[] = $value;
                            }
                        } 
                        //panel show
                        $panelShowArray = array();
                        foreach($panelArray as $key=>$value){
                            $key = strtoupper($key);
                            if(in_array($key,$panelShow)){
                                $panelShowArray[] = translate($key,$moduleName);
                            }
                        }
                        $detailViewTabsShow = array();
                        $detailViewPanelsShow = array();
                        foreach($panelShowArray as $key => $value) {
                            if(in_array($value,$detailViewTabsArray)){
                                $detailViewTabsShow[] = $value;
                            }else{
                                $detailViewPanelsShow[] = $value;
                            }
                        } 
                        //field hide
                        $fieldsHide = $selDynamicPanelsDataRow['field_hide'];
                        $fieldsHide = explode(',',$fieldsHide);
                        $fieldsHideArray = array();
                        foreach ($fieldsHide as $key => $value) {
                            $fieldsHideArray[] = $value;
                        }
                        //field show
                        $fieldsShow = $selDynamicPanelsDataRow['field_show'];
                        $fieldsShow = explode(',',$fieldsShow);
                        $fieldsShowArray = array();
                        foreach ($fieldsShow as $key => $value) {
                            $fieldsShowArray[] = $value;
                        }
                        //fields color
                        $selDynamicPanelsFieldsColor = "SELECT * FROM vi_dynamic_panels_fields_color 
                                                            WHERE dynamic_panels_id = '$dynamicPanelsId' 
                                                            AND deleted = 0";
                        $selDynamicPanelsFieldsColorResult = $GLOBALS['db']->query($selDynamicPanelsFieldsColor);
                        $selDynamicPanelsFieldsColorResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selDynamicPanelsFieldsColor));
                        $fieldColorrArray = array();
                        if(!empty($selDynamicPanelsFieldsColorResultData)){
                            while($selFieldColorRow = $GLOBALS['db']->fetchByAssoc($selDynamicPanelsFieldsColorResult)){
                                $fieldColorrArray[] = array('fieldName' => $selFieldColorRow['field_color_name'] , 'fieldColor' => $selFieldColorRow['field_color']);
                            }
                        }

                        $fieldsReadOnly = explode(',',$selDynamicPanelsDataRow['field_readonly']);
                        $fieldsReadOnlyArray = array();
                        foreach ($fieldsReadOnly as $key => $value) {
                            if($value != ''){
                                $readOnlyFieldData = $recordBean->field_defs[$value];
                                $type = $readOnlyFieldData['type'];
                                $fieldsReadOnlyArray[] = array('field' => $value,'type' => $type);
                            }//end of if
                        }//end of foreach
                        
                        $data[$dynamicPanelsId] = array('detailViewTabsHide' => json_encode($detailViewTabsHide),'detailViewPanelsHide' => json_encode($detailViewPanelsHide),'detailViewTabsShow' => json_encode($detailViewTabsShow),'detailViewPanelsShow' => json_encode($detailViewPanelsShow),'fieldsHideArray' => $fieldsHideArray,'fieldsShowArray' => $fieldsShowArray ,'fieldColorrArray' => $fieldColorrArray, 'fieldsReadOnlyArray' => $fieldsReadOnlyArray);
                    }
                }
            }
            echo json_encode($data);
        }
    }
    function contains($str,$substr){
        if(strpos($str,$substr) !== false){
             return true;
         }
    }
    function startsWith($str,$substr){
        $sl = strlen($str);
        $ssl = strlen($subStr);
        if ($sl >= $ssl) {
            if(strpos($str, $substr, 0) === 0){
                return true;
            }
        }
    }
    function endsWith($str, $subStr) {
        $sl = strlen($str);
        $ssl = strlen($subStr);
        if ($sl >= $ssl) {
            if(substr_compare($str, $subStr, $sl - $ssl, $ssl) == 0){
                return true;
            }
        }
    }
}
new VIDynamicPanelsDetailViewCheckCondition();
?>