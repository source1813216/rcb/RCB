<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("modules/AOW_WorkFlow/aow_utils.php");
class VIDynamicPanelsModuleFieldValue{
	public function __construct(){
		$this->getModule();
	}
	function getFieldValue($module,$fieldname,$aow_field,$view='EditView',$value = '',$alt_type = '',$currency_id = '',$params= array()
	){
		global $current_language,$app_strings,$app_list_strings,$current_user,$beanFiles,$beanList;

	   	// use the mod_strings for this module
	    $mod_strings = return_module_language($current_language,$module);
		
		// set the filename for this control
        $file = create_cache_directory('modules/AOW_WorkFlow/')
            . $module
            . $view
            . $alt_type
            . $fieldname.'1'
            . '.tpl';
	    
        $displayParams = array();

	    if (!is_file($file) || inDeveloperMode() || !empty($_SESSION['developerMode'])) {
	        if (!isset($vardef)) {
	            require_once($beanFiles[$beanList[$module]]);
	            $focus = new $beanList[$module];
	            $vardef = $focus->getFieldDefinition($fieldname);
	        }//end of if

	        // Bug: check for AOR value SecurityGroups value missing
	        if(stristr($fieldname, 'securitygroups') != false && empty($vardef)) {
	            require_once($beanFiles[$beanList['SecurityGroups']]);
	            $module = 'SecurityGroups';
	            $focus = new $beanList[$module];
	            $vardef = $focus->getFieldDefinition($fieldname);
	        }//end of if

	        // if this is the id relation field, then don't have a pop-up selector.
	        if( $vardef['type'] == 'relate' && $vardef['id_name'] == $vardef['name']) {
	            $vardef['type'] = 'varchar';
	        }//end of if

	        if(isset($vardef['precision'])) unset($vardef['precision']);

	        if( $vardef['type'] == 'datetime') {
	            $vardef['type'] = 'datetimecombo';
	        }//end of if

	        if( $vardef['type'] == 'datetimecombo') {
	            $displayParams['originalFieldName'] = $aow_field;
	            // Replace the square brackets by a deliberately complex alias to avoid JS conflicts
	            $displayParams['idName'] = createBracketVariableAlias($aow_field);
	        }//end of if

	        // trim down textbox display
	        if( $vardef['type'] == 'text' ) {
	            $vardef['rows'] = 2;
	            $vardef['cols'] = 32;
	        }//end of if

	        // create the dropdowns for the parent type fields
	        if ( $vardef['type'] == 'parent_type' ) {
	            $vardef['type'] = 'enum';
	        }//end of if

	        if($vardef['type'] == 'link'){
	            $vardef['type'] = 'relate';
	            $vardef['rname'] = 'name';
	            $vardef['id_name'] = $vardef['name'].'_id';
	            if((!isset($vardef['module']) || $vardef['module'] == '') && $focus->load_relationship($vardef['name'])) {
	                $relName = $vardef['name'];
	                $vardef['module'] = $focus->$relName->getRelatedModuleName();
	            }//end of if
	        }//end of if

	        //check for $alt_type
	        if ( $alt_type != '' ) {
	            $vardef['type'] = $alt_type;
	        }//end of if

	        // remove the special text entry field function 'getEmailAddressWidget'
	        if ( isset($vardef['function']) && ( $vardef['function'] == 'getEmailAddressWidget' || $vardef['function']['name'] == 'getEmailAddressWidget' ) )
	            unset($vardef['function']);

	        if(isset($vardef['name']) && ($vardef['name'] == 'date_entered' || $vardef['name'] == 'date_modified')){
	            $vardef['name'] = 'aow_temp_date';
	        }//end of if

	        // load SugarFieldHandler to render the field tpl file
	        static $sfh;

	        if(!isset($sfh)) {
	            require_once('include/SugarFields/SugarFieldHandler.php');
	            $sfh = new SugarFieldHandler();
	        }//end of if

	        $contents = $sfh->displaySmarty('fields', $vardef, $view, $displayParams);
	        // Remove all the copyright comments
	        $contents = preg_replace('/\{\*[^\}]*?\*\}/', '', $contents);

	        if ($view == 'EditView' && ($vardef['type'] == 'relate' || $vardef['type'] == 'parent')) {
	            $contents = str_replace('"' . $vardef['id_name'] . '"',
	                '{/literal}"{$fields.' . $vardef['name'] . '.id_name}"{literal}', $contents);
	            $contents = str_replace('"' . $vardef['name'] . '"',
	                '{/literal}"{$fields.' . $vardef['name'] . '.name}"{literal}', $contents);
	        }//end of if

	        if ($view == 'DetailView' && $vardef['type'] == 'image') {
		    	// Because TCPDF could not read image from download entryPoint, we need change entryPoint link to image path to resolved issue Image is not showing in PDF report
			   	if($_REQUEST['module'] == 'AOR_Reports' && $_REQUEST['action'] == 'DownloadPDF') {
	                global $sugar_config;
	                $upload_dir = isset($sugar_config['upload_dir']) ? $sugar_config['upload_dir'] : 'upload/';
	                $contents = str_replace('index.php?entryPoint=download&id=', $upload_dir, $contents);
	                $contents = str_replace('&type={$module}', '', $contents);
		        }//end of if
	            $contents = str_replace('{$fields.id.value}', '{$record_id}', $contents);
	        }//end of if

	        // hack to disable one of the js calls in this control
	        if (isset($vardef['function']) && ($vardef['function'] == 'getCurrencyDropDown' || $vardef['function']['name'] == 'getCurrencyDropDown')) {
	            $contents .= "{literal}<script>function CurrencyConvertAll() { return; }</script>{/literal}";
	        }//end of if
	        // Save it to the cache file
	        if ($fh = @sugar_fopen($file, 'w')) {
	            fputs($fh, $contents);
	            fclose($fh);
	        }//end of if
	    }//end of if
	    
	    // Now render the template we received
	    $ss = new Sugar_Smarty();

	    // Create Smarty variables for the Calendar picker widget
	    global $timedate;
	    $time_format = $timedate->get_user_time_format();
	    $date_format = $timedate->get_cal_date_format();
	    $ss->assign('USER_DATEFORMAT', $timedate->get_user_date_format());
	    $ss->assign('TIME_FORMAT', $time_format);
	    $time_separator = ":";
	    $match = array();
	    if(preg_match('/\d+([^\d])\d+([^\d]*)/s', $time_format, $match)) {
	        $time_separator = $match[1];
	    }
	    $t23 = strpos($time_format, '23') !== false ? '%H' : '%I';
	    if(!isset($match[2]) || $match[2] == '') {
	        $ss->assign('CALENDAR_FORMAT', $date_format . ' ' . $t23 . $time_separator . "%M");
	    }else {
	        $pm = $match[2] == "pm" ? "%P" : "%p";
	        $ss->assign('CALENDAR_FORMAT', $date_format . ' ' . $t23 . $time_separator . "%M" . $pm);
	    }//end of else

	    $ss->assign('CALENDAR_FDOW', $current_user->get_first_day_of_week());

	    // populate the fieldlist from the vardefs
	    $fieldlist = array();
	    if ( !isset($focus) || !($focus instanceof SugarBean) )
	        require_once($beanFiles[$beanList[$module]]);
	    $focus = new $beanList[$module];
	    // create the dropdowns for the parent type fields
	    $vardefFields = $focus->getFieldDefinitions();
	    if (isset($vardefFields[$fieldname]['type']) && $vardefFields[$fieldname]['type'] == 'parent_type' ) {
	        $focus->field_defs[$fieldname]['options'] = $focus->field_defs[$vardefFields[$fieldname]['group']]['options'];
	    }//end of if
	    foreach ( $vardefFields as $name => $properties ) {
	        $fieldlist[$name] = $properties;
	        // fill in enums
	        if(isset($fieldlist[$name]['options']) && is_string($fieldlist[$name]['options']) && isset($app_list_strings[$fieldlist[$name]['options']]))
	            $fieldlist[$name]['options'] = $app_list_strings[$fieldlist[$name]['options']];
	        // Bug 32626: fall back on checking the mod_strings if not in the app_list_strings
	        elseif(isset($fieldlist[$name]['options']) && is_string($fieldlist[$name]['options']) && isset($mod_strings[$fieldlist[$name]['options']]))
	            $fieldlist[$name]['options'] = $mod_strings[$fieldlist[$name]['options']];
	        // Bug 22730: make sure all enums have the ability to select blank as the default value.
	        if(!isset($fieldlist[$name]['options']['']))
	            $fieldlist[$name]['options'][''] = '';
	    }//end of foreach

	    // fill in function return values
	    if ( !in_array($fieldname,array('email1','email2')) )
	    {
	        if (!empty($fieldlist[$fieldname]['function']['returns']) && $fieldlist[$fieldname]['function']['returns'] == 'html')
	        {
	            $function = $fieldlist[$fieldname]['function']['name'];
	            // include various functions required in the various vardefs
	            if ( isset($fieldlist[$fieldname]['function']['include']) && is_file($fieldlist[$fieldname]['function']['include']))
	                require_once($fieldlist[$fieldname]['function']['include']);
	            $_REQUEST[$fieldname] = $value;
	            $value = $function($focus, $fieldname, $value, $view);

	            $value = str_ireplace($fieldname, $aow_field, $value);
	        }
	    }

	    if(isset($fieldlist[$fieldname]['type']) && $fieldlist[$fieldname]['type'] == 'link'){
	        $fieldlist[$fieldname]['id_name'] = $fieldlist[$fieldname]['name'].'_id';

	        if((!isset($fieldlist[$fieldname]['module']) || $fieldlist[$fieldname]['module'] == '') && $focus->load_relationship($fieldlist[$fieldname]['name'])) {
	            $relName = $fieldlist[$fieldname]['name'];
	            $fieldlist[$fieldname]['module'] = $focus->$relName->getRelatedModuleName();
	        }
	    }

	    if(isset($fieldlist[$fieldname]['name']) && ($fieldlist[$fieldname]['name'] == 'date_entered' || $fieldlist[$fieldname]['name'] == 'date_modified')){
	        $fieldlist[$fieldname]['name'] = 'aow_temp_date';
	        $fieldlist['aow_temp_date'] = $fieldlist[$fieldname];
	        $fieldname = 'aow_temp_date';
	    }

	    $quicksearch_js = '';
	    if(isset( $fieldlist[$fieldname]['id_name'] ) && $fieldlist[$fieldname]['id_name'] != '' && $fieldlist[$fieldname]['id_name'] != $fieldlist[$fieldname]['name']){
	        $rel_value = $value;

	        require_once("include/TemplateHandler/TemplateHandler.php");
	        $template_handler = new TemplateHandler();
	        $quicksearch_js = $template_handler->createQuickSearchCode($fieldlist,$fieldlist,$view);
	        $quicksearch_js = str_replace($fieldname, $aow_field.'_display', $quicksearch_js);
	        $quicksearch_js = str_replace($fieldlist[$fieldname]['id_name'], $aow_field, $quicksearch_js);

	        echo $quicksearch_js;

	        if(isset($fieldlist[$fieldname]['module']) && $fieldlist[$fieldname]['module'] == 'Users'){
	            $rel_value = get_assigned_user_name($value);
	        } else if(isset($fieldlist[$fieldname]['module'])){
	            require_once($beanFiles[$beanList[$fieldlist[$fieldname]['module']]]);
	            $rel_focus = new $beanList[$fieldlist[$fieldname]['module']];
	            $rel_focus->retrieve($value);
	            if(isset($fieldlist[$fieldname]['rname']) && $fieldlist[$fieldname]['rname'] != ''){
	                $relDisplayField = $fieldlist[$fieldname]['rname'];
	            } else {
	                $relDisplayField = 'name';
	            }
	            $rel_value = $rel_focus->$relDisplayField;
	        }

	        $fieldlist[$fieldlist[$fieldname]['id_name']]['value'] = $value;
	        $fieldlist[$fieldname]['value'] = $rel_value;
	        $fieldlist[$fieldname]['id_name'] = $aow_field;
	        $fieldlist[$fieldlist[$fieldname]['id_name']]['name'] = $aow_field;
	        $fieldlist[$fieldname]['name'] = $aow_field.'_display';
	    } else if(isset( $fieldlist[$fieldname]['type'] ) && $view == 'DetailView' && ($fieldlist[$fieldname]['type'] == 'datetimecombo' || $fieldlist[$fieldname]['type'] == 'datetime' || $fieldlist[$fieldname]['type'] == 'date')){
	        $value = $focus->convertField($value, $fieldlist[$fieldname]);
	        if(!empty($params['date_format']) && isset($params['date_format'])){
	            $convert_format = "Y-m-d H:i:s";
	            if($fieldlist[$fieldname]['type'] == 'date') $convert_format = "Y-m-d";
	            $fieldlist[$fieldname]['value'] = $timedate->to_display($value, $convert_format, $params['date_format']);
	        }else{
	            $fieldlist[$fieldname]['value'] = $timedate->to_display_date_time($value, true, true);
	        }
	        $fieldlist[$fieldname]['name'] = $aow_field;
	    } else if(isset( $fieldlist[$fieldname]['type'] ) && ($fieldlist[$fieldname]['type'] == 'datetimecombo' || $fieldlist[$fieldname]['type'] == 'datetime' || $fieldlist[$fieldname]['type'] == 'date')){
	        $value = $focus->convertField($value, $fieldlist[$fieldname]);
	        $displayValue = $timedate->to_display_date_time($value);
	        $fieldlist[$fieldname]['value'] = $fieldlist[$aow_field]['value'] = $displayValue;
	        $fieldlist[$fieldname]['name'] = $aow_field;
	    } else {
	        $fieldlist[$fieldname]['value'] = $value;
	        $fieldlist[$fieldname]['name'] = $aow_field;

	    }

	    if (isset($fieldlist[$fieldname]['type']) && $fieldlist[$fieldname]['type'] == 'datetimecombo' || $fieldlist[$fieldname]['type'] == 'datetime' ) {
	        $fieldlist[$aow_field]['aliasId'] = createBracketVariableAlias($aow_field);
	        $fieldlist[$aow_field]['originalId'] = $aow_field;
	    }

	    if(isset($fieldlist[$fieldname]['type']) && $fieldlist[$fieldname]['type'] == 'currency' && $view != 'EditView'){
	        static $sfh;

	        if(!isset($sfh)) {
	            require_once('include/SugarFields/SugarFieldHandler.php');
	            $sfh = new SugarFieldHandler();
	        }

	        if($currency_id != '' && !stripos($fieldname, '_USD')){
	            $userCurrencyId = $current_user->getPreference('currency');
	            if($currency_id != $userCurrencyId){
	                $currency = new Currency();
	                $currency->retrieve($currency_id);
	                $value = $currency->convertToDollar($value);
	                $currency->retrieve($userCurrencyId);
	                $value = $currency->convertFromDollar($value);
	            }
	        }

	        $parentfieldlist[strtoupper($fieldname)] = $value;

	        return($sfh->displaySmarty($parentfieldlist, $fieldlist[$fieldname], 'ListView', $displayParams));
	    }

	    $ss->assign("QS_JS", $quicksearch_js);
	    $ss->assign("fields", $fieldlist);
	    $ss->assign("form_name", $view);
	    $ss->assign("bean", $focus);

	    // Add in any additional strings
	    $ss->assign("MOD", $mod_strings);
	    $ss->assign("APP", $app_strings);
	    $ss->assign("module", $module);
	    if (isset($params['record_id']) && $params['record_id']) {
	        $ss->assign("record_id", $params['record_id']);
	    }

	    return $ss->fetch($file);
	}
	function getModuleFieldValue($module, $fieldname, $aow_field, $view='EditView',$value = '', $alt_type = '', $currency_id = '', $params= array()){
	    global $current_language, $app_strings, $app_list_strings, $current_user, $beanFiles, $beanList;

	    // use the mod_strings for this module
	    $mod_strings = return_module_language($current_language,$module);

	    // set the filename for this control
	    $file = create_cache_directory('modules/AOW_WorkFlow/') . $module . $view . $alt_type . $fieldname . '1.tpl';

	    $displayParams = array();

	    if ( !is_file($file)
	        || inDeveloperMode()
	        || !empty($_SESSION['developerMode']) ) {

	        if ( !isset($vardef) ) {
	            require_once($beanFiles[$beanList[$module]]);
	            $focus = new $beanList[$module];
	            $vardef = $focus->getFieldDefinition($fieldname);
	        }

	        // Bug: check for AOR value SecurityGroups value missing
	        if(stristr($fieldname, 'securitygroups') != false && empty($vardef)) {
	            require_once($beanFiles[$beanList['SecurityGroups']]);
	            $module = 'SecurityGroups';
	            $focus = new $beanList[$module];
	            $vardef = $focus->getFieldDefinition($fieldname);
	        }


	        //$displayParams['formName'] = 'EditView';

	        // if this is the id relation field, then don't have a pop-up selector.
	        if( $vardef['type'] == 'relate' && $vardef['id_name'] == $vardef['name']) {
	            $vardef['type'] = 'varchar';
	        }

	        if(isset($vardef['precision'])) unset($vardef['precision']);

	        //$vardef['precision'] = $locale->getPrecedentPreference('default_currency_significant_digits', $current_user);

	        //TODO Fix datetimecomebo
	        //temp work around
	        if( $vardef['type'] == 'datetimecombo') {
	            $vardef['type'] = 'datetime';
	        }

	        // trim down textbox display
	        if( $vardef['type'] == 'text' ) {
	            $vardef['rows'] = 2;
	            $vardef['cols'] = 32;
	        }

	        // create the dropdowns for the parent type fields
	        if ( $vardef['type'] == 'parent_type' ) {
	            $vardef['type'] = 'enum';
	        }

	        if($vardef['type'] == 'link'){
	            $vardef['type'] = 'relate';
	            $vardef['rname'] = 'name';
	            $vardef['id_name'] = $vardef['name'].'_id';
	            if((!isset($vardef['module']) || $vardef['module'] == '') && $focus->load_relationship($vardef['name'])) {
	                $relName = $vardef['name'];
	                $vardef['module'] = $focus->$relName->getRelatedModuleName();
	            }

	        }

	        //check for $alt_type
	        if ( $alt_type != '' ) {
	            $vardef['type'] = $alt_type;
	        }

	        // remove the special text entry field function 'getEmailAddressWidget'
	        if ( isset($vardef['function'])
	            && ( $vardef['function'] == 'getEmailAddressWidget'
	                || $vardef['function']['name'] == 'getEmailAddressWidget' ) )
	            unset($vardef['function']);

	        if(isset($vardef['name']) && ($vardef['name'] == 'date_entered' || $vardef['name'] == 'date_modified')){
	            $vardef['name'] = 'aow_temp_date';
	        }

	        // load SugarFieldHandler to render the field tpl file
	        static $sfh;

	        if(!isset($sfh)) {
	            require_once('include/SugarFields/SugarFieldHandler.php');
	            $sfh = new SugarFieldHandler();
	        }

	        $contents = $sfh->displaySmarty('fields', $vardef, $view, $displayParams);

	        // Remove all the copyright comments
	        $contents = preg_replace('/\{\*[^\}]*?\*\}/', '', $contents);

	        if( $view == 'EditView' &&  ($vardef['type'] == 'relate' || $vardef['type'] == 'parent')){
	            $contents = str_replace('"'.$vardef['id_name'].'"','{/literal}"{$fields.'.$vardef['name'].'.id_name}"{literal}', $contents);
	            $contents = str_replace('"'.$vardef['name'].'"','{/literal}"{$fields.'.$vardef['name'].'.name}"{literal}', $contents);
	        }

	        // hack to disable one of the js calls in this control
	        if ( isset($vardef['function']) && ( $vardef['function'] == 'getCurrencyDropDown' || $vardef['function']['name'] == 'getCurrencyDropDown' ) )
	            $contents .= "{literal}<script>function CurrencyConvertAll() { return; }</script>{/literal}";

	        // Save it to the cache file
	        if($fh = @sugar_fopen($file, 'w')) {
	            fputs($fh, $contents);
	            fclose($fh);
	        }
	    }

	    // Now render the template we received
	    $ss = new Sugar_Smarty();

	    // Create Smarty variables for the Calendar picker widget
	    global $timedate;
	    $time_format = $timedate->get_user_time_format();
	    $date_format = $timedate->get_cal_date_format();
	    $ss->assign('USER_DATEFORMAT', $timedate->get_user_date_format());
	    $ss->assign('TIME_FORMAT', $time_format);
	    $time_separator = ":";
	    $match = array();
	    if(preg_match('/\d+([^\d])\d+([^\d]*)/s', $time_format, $match)) {
	        $time_separator = $match[1];
	    }
	    $t23 = strpos($time_format, '23') !== false ? '%H' : '%I';
	    if(!isset($match[2]) || $match[2] == '') {
	        $ss->assign('CALENDAR_FORMAT', $date_format . ' ' . $t23 . $time_separator . "%M");
	    }
	    else {
	        $pm = $match[2] == "pm" ? "%P" : "%p";
	        $ss->assign('CALENDAR_FORMAT', $date_format . ' ' . $t23 . $time_separator . "%M" . $pm);
	    }

	    $ss->assign('CALENDAR_FDOW', $current_user->get_first_day_of_week());

	    // populate the fieldlist from the vardefs
	    $fieldlist = array();
	    if ( !isset($focus) || !($focus instanceof SugarBean) )
	        require_once($beanFiles[$beanList[$module]]);
	    $focus = new $beanList[$module];
	    // create the dropdowns for the parent type fields
	    $vardefFields = $focus->getFieldDefinitions();
	    if (isset($vardefFields[$fieldname]['type']) && $vardefFields[$fieldname]['type'] == 'parent_type' ) {
	        $focus->field_defs[$fieldname]['options'] = $focus->field_defs[$vardefFields[$fieldname]['group']]['options'];
	    }
	    foreach ( $vardefFields as $name => $properties ) {
	        $fieldlist[$name] = $properties;
	        // fill in enums
	        if(isset($fieldlist[$name]['options']) && is_string($fieldlist[$name]['options']) && isset($app_list_strings[$fieldlist[$name]['options']]))
	            $fieldlist[$name]['options'] = $app_list_strings[$fieldlist[$name]['options']];
	        // Bug 32626: fall back on checking the mod_strings if not in the app_list_strings
	        elseif(isset($fieldlist[$name]['options']) && is_string($fieldlist[$name]['options']) && isset($mod_strings[$fieldlist[$name]['options']]))
	            $fieldlist[$name]['options'] = $mod_strings[$fieldlist[$name]['options']];
	        // Bug 22730: make sure all enums have the ability to select blank as the default value.
	        if(!isset($fieldlist[$name]['options']['']))
	            $fieldlist[$name]['options'][''] = '';
	    }

	    // fill in function return values
	    if ( !in_array($fieldname,array('email1','email2')) )
	    {
	        if (!empty($fieldlist[$fieldname]['function']['returns']) && $fieldlist[$fieldname]['function']['returns'] == 'html')
	        {
	            $function = $fieldlist[$fieldname]['function']['name'];
	            // include various functions required in the various vardefs
	            if ( isset($fieldlist[$fieldname]['function']['include']) && is_file($fieldlist[$fieldname]['function']['include']))
	                require_once($fieldlist[$fieldname]['function']['include']);
	            $_REQUEST[$fieldname] = $value;
	            $value = $function($focus, $fieldname, $value, $view);

	            $value = str_ireplace($fieldname, $aow_field, $value);
	        }
	    }

	    if(isset($fieldlist[$fieldname]['type']) && $fieldlist[$fieldname]['type'] == 'link'){
	        $fieldlist[$fieldname]['id_name'] = $fieldlist[$fieldname]['name'].'_id';

	        if((!isset($fieldlist[$fieldname]['module']) || $fieldlist[$fieldname]['module'] == '') && $focus->load_relationship($fieldlist[$fieldname]['name'])) {
	            $relName = $fieldlist[$fieldname]['name'];
	            $fieldlist[$fieldname]['module'] = $focus->$relName->getRelatedModuleName();
	        }
	    }

	    if(isset($fieldlist[$fieldname]['name']) && ($fieldlist[$fieldname]['name'] == 'date_entered' || $fieldlist[$fieldname]['name'] == 'date_modified')){
	        $fieldlist[$fieldname]['name'] = 'aow_temp_date';
	        $fieldlist['aow_temp_date'] = $fieldlist[$fieldname];
	        $fieldname = 'aow_temp_date';
	    }

	    $quicksearch_js = '';
	    if(isset( $fieldlist[$fieldname]['id_name'] ) && $fieldlist[$fieldname]['id_name'] != '' && $fieldlist[$fieldname]['id_name'] != $fieldlist[$fieldname]['name']){
	        $rel_value = $value;

	        require_once("include/TemplateHandler/TemplateHandler.php");
	        $template_handler = new TemplateHandler();
	        $quicksearch_js = $template_handler->createQuickSearchCode($fieldlist,$fieldlist,$view);
	        $quicksearch_js = str_replace($fieldname, $aow_field.'_display', $quicksearch_js);
	        $quicksearch_js = str_replace($fieldlist[$fieldname]['id_name'], $aow_field, $quicksearch_js);

	        echo $quicksearch_js;

	        if(isset($fieldlist[$fieldname]['module']) && $fieldlist[$fieldname]['module'] == 'Users'){
	            $rel_value = get_assigned_user_name($value);
	        } else if(isset($fieldlist[$fieldname]['module'])){
	            require_once($beanFiles[$beanList[$fieldlist[$fieldname]['module']]]);
	            $rel_focus = new $beanList[$fieldlist[$fieldname]['module']];
	            $rel_focus->retrieve($value);
	            if(isset($fieldlist[$fieldname]['rname']) && $fieldlist[$fieldname]['rname'] != ''){
	                $relDisplayField = $fieldlist[$fieldname]['rname'];
	            } else {
	                $relDisplayField = 'name';
	            }//end of else
	            $rel_value = $rel_focus->$relDisplayField;
	        }//end of else if

	        $fieldlist[$fieldlist[$fieldname]['id_name']]['value'] = $value;
	        $fieldlist[$fieldname]['value'] = $rel_value;
	        $fieldlist[$fieldname]['id_name'] = $aow_field;
	        $fieldlist[$fieldlist[$fieldname]['id_name']]['name'] = $aow_field;
	        $fieldlist[$fieldname]['name'] = $aow_field.'_display';
	    } else if(isset( $fieldlist[$fieldname]['type'] ) && $view == 'DetailView' && ($fieldlist[$fieldname]['type'] == 'datetimecombo' || $fieldlist[$fieldname]['type'] == 'datetime' || $fieldlist[$fieldname]['type'] == 'date')){
	        $value = $focus->convertField($value, $fieldlist[$fieldname]);
	        if(!empty($params['date_format']) && isset($params['date_format'])){
	            $convert_format = "Y-m-d H:i:s";
	            if($fieldlist[$fieldname]['type'] == 'date') $convert_format = "Y-m-d";
	            $fieldlist[$fieldname]['value'] = $timedate->to_display($value, $convert_format, $params['date_format']);
	        }else{
	            $fieldlist[$fieldname]['value'] = $timedate->to_display_date_time($value, true, true);
	        }//end of else
	        $fieldlist[$fieldname]['name'] = $aow_field;
	    } else if(isset( $fieldlist[$fieldname]['type'] ) && ($fieldlist[$fieldname]['type'] == 'datetimecombo' || $fieldlist[$fieldname]['type'] == 'datetime' || $fieldlist[$fieldname]['type'] == 'date')){
	        $value = $focus->convertField($value, $fieldlist[$fieldname]);
	        $fieldlist[$fieldname]['value'] = $timedate->to_display_date($value);
	        $fieldlist[$fieldname]['name'] = $aow_field;
	    } else {
	        $fieldlist[$fieldname]['value'] = $value;
	        $fieldlist[$fieldname]['name'] = $aow_field;
	    }//end of else

	    if(isset($fieldlist[$fieldname]['type']) && $fieldlist[$fieldname]['type'] == 'currency' && $view != 'EditView'){
	        static $sfh;
	        if(!isset($sfh)) {
	            require_once('include/SugarFields/SugarFieldHandler.php');
	            $sfh = new SugarFieldHandler();
	        }//end of if

	        if($currency_id != '' && !stripos($fieldname, '_USD')){
	            $userCurrencyId = $current_user->getPreference('currency');
	            if($currency_id != $userCurrencyId){
	                $currency = new Currency();
	                $currency->retrieve($currency_id);
	                $value = $currency->convertToDollar($value);
	                $currency->retrieve($userCurrencyId);
	                $value = $currency->convertFromDollar($value);
	            }//end of if
	        }//end of if

	        $parentfieldlist[strtoupper($fieldname)] = $value;

	        return($sfh->displaySmarty($parentfieldlist, $fieldlist[$fieldname], 'ListView', $displayParams));
	    }//end of if

	    $ss->assign("QS_JS", $quicksearch_js);
	    $ss->assign("fields",$fieldlist);
	    $ss->assign("form_name",$view);
	    $ss->assign("bean",$focus);

	    // add in any additional strings
	    $ss->assign("MOD", $mod_strings);
	    $ss->assign("APP", $app_strings);

	    return $ss->fetch($file);
	}//end of function
	public function getModule(){
		if(isset($_REQUEST['rel_field']) &&  $_REQUEST['rel_field'] != ''){
            $rel_module = getRelatedModule($_REQUEST['aow_module'],$_REQUEST['rel_field']);
        } else {
            $rel_module = $_REQUEST['aow_module'];
        }
        $module = $_REQUEST['aow_module'];
        $fieldname = $_REQUEST['aow_fieldname'];
        $aow_field = $_REQUEST['aow_newfieldname'];

        if(isset($_REQUEST['view'])) $view = $_REQUEST['view'];
        else $view= 'EditView';

        if(isset($_REQUEST['aow_value'])) $value = $_REQUEST['aow_value'];
        else $value = ' ';
        switch($_REQUEST['aow_type']) {
            case 'Value':
            default:
                if($view == 'vi_dynamicpanelseditview'){
                	global $suitecrm_version;
					if (version_compare($suitecrm_version, '7.9.10', '>=')) {
						echo $this->getFieldValue($rel_module,$fieldname, $aow_field, 'EditView', $value);
					}else {
						echo $this->getModuleFieldValue($rel_module,$fieldname, $aow_field, 'EditView', $value);	 
					}                    
                }else{
                	global $suitecrm_version;
					if (version_compare($suitecrm_version, '7.9.10', '>=')) {
						echo $this->getFieldValue($rel_module,$fieldname, $aow_field, 'DetailView', $value);
					}else {
						echo $this->getModuleFieldValue($rel_module,$fieldname, $aow_field, 'DetailView', $value);	 
					}//end of else     
                }//end of else
                break;
        }//end of switch
        die;
    }//end of function
}//end of class
new VIDynamicPanelsModuleFieldValue();
?>