<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VIDynamicPanelsCheckValidFieldType{
    public function __construct(){
        $this->checkValidFieldType();
    }
    public function checkValidFieldType(){
        $moduleName = $_REQUEST['moduleName'];
        $fieldName = $_REQUEST['fieldName'];
        $fieldVal = $_REQUEST['fieldVal'];

        $bean = BeanFactory::newBean($moduleName);
        $fieldDef = $bean->field_defs[$fieldName];
        $fieldData = $bean->field_defs[$fieldVal];

        if($fieldDef['type'] == 'multienum'){
            if($fieldDef['type'] == $fieldData['type']){
                echo "1";
            }else{
                echo "0";
            }
        }else{
            echo "1";
        }
    }//end of functions
}//end of class
new VIDynamicPanelsCheckValidFieldType();
?>