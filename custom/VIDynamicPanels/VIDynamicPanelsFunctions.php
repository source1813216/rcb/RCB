<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//insert record
function insertDynamicPanelRecord($tabelName,$data){
    //data key
    $key = array_keys($data);
    $fieldName = implode(",",$key);
    
    //data val
    $val = array_values($data);
    $fieldVal = implode(",",$val);
    
    //insert
    $insertDynamicPanelData = "INSERT INTO $tabelName ($fieldName) VALUES($fieldVal)";
    $insDataResult = $GLOBALS['db']->query($insertDynamicPanelData);

    return $insDataResult;
}//end of function

//update record
function updateDynamicPanelRecord($tabelName,$data,$where){
    //update
    $updateDynamicPanelData = "UPDATE $tabelName SET";

    $fieldName = array_keys($data); //database field name
    $fieldValue = array_values($data); //field value

    $whereFieldName = array_keys($where); //where condition field name
    $whereFieldValue = array_values($where); // where condition field value
    $i=0;
    $count = count($data);
    foreach($data as $d){
        if($count == $i+1){
            $updateDynamicPanelData .= " $fieldName[$i]= $fieldValue[$i]";
        }else{
            $updateDynamicPanelData .= " $fieldName[$i]= $fieldValue[$i],";
        }//end of else
        $i++;
    }//end of foreach
    $j=0;
    $updateDynamicPanelData .= " where 1=1";
    foreach($where as $w){
        $updateDynamicPanelData .=" and $whereFieldName[$j]='$whereFieldValue[$j]'";
        $j++;
    }//end of foreach
    $updateDynamicPanelDataResult = $GLOBALS['db']->query($updateDynamicPanelData);
    return $updateDynamicPanelDataResult;
}//end of function

//contains 
function containsStrings($str,$substr){
    if(strpos($str,$substr) !== false){
         return true;
    }//end of if
}//end of function

//startwith
function startsWiths($str,$substr){
    $sl = strlen($str);
    $ssl = strlen($substr);
    if ($sl >= $ssl) {
        if(strpos($str, $substr, 0) === 0){
            return true;
        }//end of if
    }//end of if
}//end of function

//endswith
function endsWiths($str, $subStr) {
    $sl = strlen($str);
    $ssl = strlen($subStr);
    if ($sl >= $ssl) {
        if(substr_compare($str, $subStr, $sl - $ssl, $ssl) == 0){
            return true;
        }//end of if
    }//end of if
}//end of function


function getFieldsData($module) {
    require_once('include/utils.php');
    $bean = BeanFactory::newBean($module);
    $field = $bean->getFieldDefinitions();
    $validType = array();
    $addressFieldData = array();
    foreach($field as $value){
        if($value['type'] == 'varchar'){
            if(isset($value['group'])){
                $fieldName = $value['name'];
                $fieldLabel = translate($value['vname'],$module);
                $lastChar = substr($fieldLabel, -1);
                if($lastChar == ':'){
                    $fieldLabel = substr_replace($fieldLabel, "", -1);
                }
            $addressFieldData[$fieldName] = $fieldLabel;
            }//end of if
        }//end of if
    }//end of foreach

    unset($addressFieldData['email1']);
    unset($addressFieldData['email2']);

    $editViewFieldData = getEditDetailViewFields('editview',$module,$validType);
    $detailViewFieldData = getEditDetailViewFields('detailview',$module,$validType);

    $value = '';
    $arrayMerge = array_merge($editViewFieldData,$addressFieldData,$detailViewFieldData); //merge array
    asort($arrayMerge);
    return get_select_options_with_id($arrayMerge,$value);
}//end of function

function getEditDetailViewFields($view,$module,$validType){
    require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
    $view_array = ParserFactory::getParser($view,$module);
    $panelArray = $view_array->_viewdefs['panels'];//editview panels
    $bean = BeanFactory::newBean($module);
    $field = $bean->getFieldDefinitions();

    //editview fields
    $editViewFieldArray = array();
    foreach ($panelArray as $key => $value) {
        foreach ($value as $keys => $values) {
            $editViewFieldArray[] = $values;
        }//end of foreach
    }//end of foreach

    $data = array('' => '--None--');
    foreach($editViewFieldArray as $key => $value) {
        foreach($value as $k => $v) {
            if(array_key_exists($v, $field)) {
                if(empty($validType) || in_array($field[$v]['type'], $validType)){
                    require_once('include/utils.php');
                    $fieldLabel = translate($field[$v]['vname'], $module);
                    $lastChar = substr($fieldLabel, -1);
                    if($lastChar == ':'){
                        $fieldLabel = substr_replace($fieldLabel, "", -1);
                    }
                    $fieldName = $v;
                    $data[$fieldName] = $fieldLabel;
                }
            }//end of if 
        }//end of foreach
    }//end of foreach

    if($module == 'AOS_PDF_Templates'){
        $data['date_entered'] = 'Date Created';
        $data['date_modified'] = 'Date Modified';
    }//end of if

    //unset fields
    $unsetData = array('sample','pdfheader','line_items','configurationGUI','insert_fields','survey_questions_display','email1','action_lines','condition_lines','reminders','email2','aop_case_updates_threaded','internal','suggestion_box','case_update_form','update_text','email_opt_out','reschedule_history','pdffooter','product_image','filename','invite_templates','related_doc_rev_number','related_doc_name');
    foreach ($unsetData as $key => $value) {
        unset($data[$value]);
    }//end of foreach

    if($module == 'AOK_KnowledgeBase' || $module == 'AOS_PDF_Templates'){
        unset($data['description']);
    }
    if($module == 'jjwg_Maps' || $module == 'Meetings' || $module == 'Notes' || $module == 'Tasks'){
        unset($data['parent_name']);
    }
    return $data;
}//end of function    


function getEditviewDetailviewPanels($view,$module){
    require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
    $view_array = ParserFactory::getParser($view,$module);
    $panelArray = $view_array->_viewdefs['panels'];
    return $panelArray;
}//end of function

function getEditviewDetailviewTabs($view,$module){
    require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
    $view_array = ParserFactory::getParser($view,$module);
    if(isset($view_array->_viewdefs['templateMeta']['tabDefs']) && !empty($view_array->_viewdefs['templateMeta']['tabDefs'])){
        $tabArrays = $view_array->_viewdefs['templateMeta']['tabDefs'];
    }else{
        $tabArrays = array();
    }//end of else
    return $tabArrays;
}//end of function

function getEditviewDetailviewRequiredFields($view,$module){
    require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
    $view_array = ParserFactory::getParser($view,$module);
    $requiredFieldsArray = $view_array->getRequiredFields();
    $requiredFieldsArray = str_replace("\"", "", $requiredFieldsArray);
    return $requiredFieldsArray;
}//end of functions

function matchDynamicPanelsCondition($formData,$conditionArray,$fieldName,$moduleName,$conditionalOperator,$recordId){
    global $app_list_strings;
    $matchConditionArray = array();
    global $timedate;
    $bean = BeanFactory::newBean($moduleName);
    foreach ($conditionArray as $dynamicPanelId => $conditionsData) {
        $matchCondition = array();
        if(!empty($conditionsData)){
            foreach ($conditionsData as $keys => $values) {
                $val = 0;
                if(array_key_exists($values['field'],$formData)){
                    $fieldValue = $formData[$values['field']];
                    $fieldDef = $bean->field_defs[$values['field']];
                    $val = 1;
                }else{
                    if($recordId != ''){
                        $recordBean = BeanFactory::getBean($moduleName,$recordId);
                        $field = $values['field'];
                        $fieldValue = $recordBean->$field;
                        $fieldDef = $bean->field_defs[$field];
                        $val = 1;
                    }//end of if
                }//end of else
                
                if($val == 1){
                    $fieldType = $fieldDef['type'];
                    $operator = $values['operator'];
                    $value = $values['value'];
                    $valueType = $values['valueType'];

                    $dataField = getRecordFieldValue($valueType,$values,$bean,$recordId,$formData,$moduleName,$fieldType,$value,$fieldValue);

                    $fieldValue = $dataField['fieldValue'];
                    $value = $dataField['value'];

                    if($fieldValue != '' || $value != ''){
                        switch($operator){
                            case 'Equal_To':
                                if($values['valueType'] != 'Multi' ){
                                    if($fieldValue == $value){
                                        $matchCondition[] = '1';
                                    }else{
                                        $matchCondition[] = '0';
                                    }//end of else
                                }else{
                                    if($fieldType == 'multienum'){
                                        $result = array_intersect($fieldValue,$value);
                                        if(empty($result)){
                                            $matchCondition[] = '0';
                                        }else{
                                            $matchCondition[] = '1';
                                        }//end of else
                                    }else if($fieldType == 'enum'){
                                        if(!in_array($fieldValue,$value)){
                                            $matchCondition[] = '0';
                                        }else{
                                            $matchCondition[] = '1';
                                        }//end of else 
                                    }else{
                                        if($fieldValue == $value){
                                            $matchCondition[] = '1';
                                        }else{
                                            $matchCondition[] = '0';
                                        }//end of else
                                    }//end of else   
                                }//end of else
                                break; 
                            case 'Not_Equal_To':
                                    if($values['valueType'] != 'Multi'){
                                        if($fieldValue != $value){
                                            $matchCondition[] = '1';
                                        }else{
                                            $matchCondition[] = '0';
                                        }//end of else 
                                    }else{
                                        if($fieldType == 'multienum'){
                                            $result = array_intersect($fieldValue,$value);
                                            if(empty($result)){
                                                $matchCondition[] = '1';
                                            }else{
                                                $matchCondition[] = '0';
                                            }//end of else
                                        }else if($fieldType == 'enum'){
                                            if(!in_array($fieldValue,$value)){
                                                $matchCondition[] = '1';
                                            }else{
                                                $matchCondition[] = '0';
                                            }//end of else 
                                        }else{
                                            if($fieldValue != $value){
                                                $matchCondition[] = '1';
                                            }else{
                                                $matchCondition[] = '0';
                                            }//end of else  
                                        }//end of else
                                    }//end of else
                                    break;
                            case 'is_null':
                                if(empty($fieldValue)){
                                    $matchCondition[] = '1';    
                                }else{
                                    $matchCondition[] = '0';
                                }//end of else
                                break;
                            case 'is_not_null':
                                if(!empty($fieldValue)){
                                    $matchCondition[] = '1';    
                                }else{
                                    $matchCondition[] = '0';
                                }//end of else
                                break;
                            case 'Greater_Than':
                                if($fieldValue > $value){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }//end of else
                                break;
                            case 'Less_Than':
                                if($fieldValue < $value){
                                    $matchCondition[] = '1'; 
                                }else{
                                    $matchCondition[] = '0';
                                }//end of else
                                break;
                            case 'Greater_Than_or_Equal_To':
                                if($fieldValue >= $value){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }//end of else
                                break;
                            case 'Less_Than_or_Equal_To':
                                if($fieldValue <= $value){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }//end of else
                                break;
                            case 'Contains':
                                if(containsStrings($fieldValue,$value)){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }//end of else
                                break;
                            case 'Starts_With':
                                if(startsWiths($fieldValue,$value)){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }//end of else
                                break;
                            case 'Ends_With':
                                if(endsWiths($fieldValue,$value)){
                                    $matchCondition[] = '1';
                                }else{
                                    $matchCondition[] = '0';
                                }//end of else
                                break;
                            default:
                                echo "";
                                break;        
                        }//end of switch    
                    }else{
                        $matchCondition[] = '0';
                    }//end of else
                }//end of if
            }//end of foreach
            
            if(!empty($matchCondition)){
                if($conditionalOperator[$dynamicPanelId] == 'AND'){
                    if(in_array('0',$matchCondition)){
                        $matchConditionArray[$dynamicPanelId] = '0'; 
                    }else{
                        $matchConditionArray[$dynamicPanelId] = '1';
                    }    
                }else{
                    if(in_array('1',$matchCondition)){
                        $matchConditionArray[$dynamicPanelId] = '1'; 
                    }else{
                        $matchConditionArray[$dynamicPanelId] = '0';
                    } 
                }    
            }    
        }else{
            $matchConditionArray[$dynamicPanelId] = '0';
        }
    }//end of foreach
    return $matchConditionArray;
}//end of function


function getDynamicPanelsAutoPopulateFieldsArray($dynamicPanelId,$moduleName){
    global $timedate;
    $bean = BeanFactory::newBean($moduleName);
    $selDynamicPanelsFields = "SELECT * FROM vi_dynamic_panels_fields 
                                        WHERE dynamic_panels_id = '$dynamicPanelId' 
                                        AND deleted = 0";
    $selDynamicPanelsFieldsResult = $GLOBALS['db']->query($selDynamicPanelsFields);
    $selDynamicPanelsFieldsResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selDynamicPanelsFields));
    $autoPopulateFieldArray =array();
    if(!empty($selDynamicPanelsFieldsResultData)){
         while($selDynamicPanelsFieldsRow = $GLOBALS['db']->fetchByAssoc($selDynamicPanelsFieldsResult)){
            $relateFieldName = '';
            $relateFieldValue = '';
            $autoPopulateFieldName = $selDynamicPanelsFieldsRow['field'];
            if($autoPopulateFieldName != ''){
                $autoPopulateFieldData = $bean->field_defs[$autoPopulateFieldName];
                $type = $autoPopulateFieldData['type'];
                if($autoPopulateFieldData['type'] == 'relate'){
                    $autoPopulateFieldName = $autoPopulateFieldData['id_name'];
                    $relateFieldModule = $autoPopulateFieldData['module'];
                    $id =$selDynamicPanelsFieldsRow['field_value'];
                    $relateFieldbean = BeanFactory::getBean($relateFieldModule,$id);
                    $relateFieldValue = $relateFieldbean->name;
                    $relateFieldName = $selDynamicPanelsFieldsRow['field'];
                }//end of if
                $autopopulateFieldValue = $selDynamicPanelsFieldsRow['field_value'];
                if($autoPopulateFieldData['type'] == 'date'){
                    $autopopulateFieldValue = $timedate->asUserDate($timedate->fromString($autopopulateFieldValue));
                }else if($autoPopulateFieldData['type'] == 'datetimecombo'){
                    global $current_user;
                    $autopopulateFieldValue = $timedate->to_display_date_time($selDynamicPanelsFieldsRow['field_value'], true, true, $current_user);
                }//end of else if
                $autoPopulateFieldArray[] = array('autoPopulateFieldName' => $autoPopulateFieldName,
                                                 'autopopulateFieldValue' => $autopopulateFieldValue,
                                                 'relateFieldName' => $relateFieldName,
                                                 'relateFieldValue' => $relateFieldValue,'type' => $type);
            }//end of if
        }//end of while
    }else{
        $autoPopulateFieldArray = array();
    }
    return $autoPopulateFieldArray;
}//end of function

function getDynamicPanelsFieldsColorArray($dynamicPanelId,$moduleName){
    $bean = BeanFactory::newBean($moduleName);
    $selDynamicPanelsFieldsColor = "SELECT * FROM vi_dynamic_panels_fields_color 
                                                             WHERE dynamic_panels_id = '$dynamicPanelId' 
                                                             AND deleted = 0";
    $selDynamicPanelsFieldsColorResult = $GLOBALS['db']->query($selDynamicPanelsFieldsColor);
    $selDynamicPanelsFieldsColorResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selDynamicPanelsFieldsColor));
    $fieldColorrArray = array();
    if(!empty($selDynamicPanelsFieldsColorResultData)){
        while($selFieldColorRow = $GLOBALS['db']->fetchByAssoc($selDynamicPanelsFieldsColorResult)){
            $fieldType = $bean->field_defs[$selFieldColorRow["field_color_name"]];
            $fieldColorrArray[] = array('fieldName' => $selFieldColorRow['field_color_name'] , 
                                       'fieldColor' => $selFieldColorRow['field_color'],
                                       'type' => $fieldType['type']);
        }//end of while
    }//end of if
    return $fieldColorrArray;
}//end of function

function getRecordFieldValue($valueType,$values,$bean,$recordId,$formData,$moduleName,$fieldType,$value,$fieldValue){
    if($valueType == 'Field'){
        $fieldValMatch = $values['value'];
        $fieldData = $bean->field_defs[$fieldValMatch];

        if($formData == ''){
            $value = $bean->$fieldValMatch;
            if($fieldData['type'] == 'relate'){
                $idName = $fieldData['id_name'];
                $value = $bean->$idName;
            }else if($fieldData['type'] == 'multienum'){
                $value = encodeMultienumValue($value);
            }
        }else{
            if($recordId != ''){
                if(!isset($formData[$fieldValMatch])){
                    $recordBean = BeanFactory::getBean($moduleName,$recordId);
                    $value = $recordBean->$fieldValMatch;
                    if($fieldData['type'] == 'relate'){
                        $idName = $fieldData['id_name'];
                        $value = $recordBean->$idName;
                    }
                }else{
                    if($fieldType == 'multienum'){
                        $value = encodeMultienumValue($formData[$fieldValMatch]);    
                    }else{
                        $value = $formData[$fieldValMatch];
                    }
                }
            }else{
                if(isset($formData[$fieldValMatch])){
                    if($fieldType == 'multienum'){
                        $value = encodeMultienumValue($formData[$fieldValMatch]);    
                    }else{
                        $value = $formData[$fieldValMatch];
                    }    
                }else{
                    $value = '';
                }
                if($fieldData['type'] == 'relate'){
                    $idName = $fieldData['id_name'];
                    $value = $formData[$idName];
                }
            }    
        }                       
    }else if($valueType == 'Date'){
        $params = unserialize(base64_decode($value));
        $dateType = 'datetime';
        if($params[0] == 'now'){
            $value = date('Y-m-d H:i');
            $fieldValue = strtotime(date('Y-m-d H:i',strtotime($fieldValue)));
        }else if($params[0] == 'today'){
            $dateType = 'date';
            $value = date('Y-m-d');
            $fieldValue = strtotime(date('Y-m-d', strtotime($fieldValue)));
        }else {
            $fieldNames = $params[0];
            if($formData == ''){
                $value = $bean->$fieldNames;
            }else{
                if($recordId != ''){
                    $recordBean = BeanFactory::getBean($moduleName,$recordId);
                    if(!isset($formData[$fieldNames])){
                        $value = $recordBean->$fieldNames;
                    }else{
                        $value = $formData[$fieldNames];
                    }
                }else{
                    $value = $formData[$fieldNames];
                }    
            }
            $fieldValue = strtotime(date('Y-m-d H:i',strtotime($fieldValue)));
        }
        if($params[1] != 'now'){
            switch($params[3]) {
                case 'business_hours';
                    if(file_exists('modules/AOBH_BusinessHours/AOBH_BusinessHours.php')){
                        require_once('modules/AOBH_BusinessHours/AOBH_BusinessHours.php');

                        $businessHours = new AOBH_BusinessHours();

                        $amount = $params[2];
                        if($params[1] != "plus"){
                            $amount = 0-$amount;
                        }
                        $value = date('Y-m-d H:i:s',strtotime($value));
                        $value = $businessHours->addBusinessHours($amount, $timedate->fromDb($value));
                        $value = strtotime($timedate->asDbType( $value, $dateType ));
                        break;
                    }
                    //No business hours module found - fall through.
                    $params[3] = 'hours';
                default:
                    $value = strtotime($value.' '.$app_list_strings['aow_date_operator'][$params[1]]." $params[2] ".$params[3]);
                    if($dateType == 'date') $value = strtotime(date('Y-m-d', $value));
                    break;
            }
        }else{
            $value = strtotime($value);
        }
    }

    if($valueType == 'Field' || $valueType == 'Value' || $valueType == 'Multi'){
        if($fieldType == 'datetimecombo' || $fieldType == 'datetime'){
            if($fieldValue != ''){
                if($valueType == 'Field'){
                    if($value != ''){
                        $value = $timedate->to_db($value);
                        $value = date('Y-m-d H:i',strtotime($value));
                    }
                }else{
                    if($value != ''){
                        $value = date('Y-m-d H:i',strtotime($value));
                    }
                }
                $fieldValue = $timedate->to_db($fieldValue);
                $fieldValue = date('Y-m-d H:i',strtotime($fieldValue));
            }//end of if
        }else if($fieldType == 'date'){
            if($fieldValue != ''){
                if($value != ''){
                    $value = date('Y-m-d',strtotime($value));    
                }
                $fieldValue = date('Y-m-d',strtotime($fieldValue));
            }    
        }else if($fieldType == 'multienum'){ 
            if($valueType == 'Value' || $valueType == 'Field'){
                $fieldValue = encodeMultienumValue($fieldValue);
            }else if($valueType == 'Multi'){
                $value = unencodeMultienum($value);  
            }//end of else                         
        }else if($fieldType == 'enum'){
            if($values['valueType'] == 'Multi'){
                $value = unencodeMultienum($value);
            }//end of if
        }//end of else if
    }

    return array('value' => $value, 'fieldValue' => $fieldValue);
}
?>