<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once('modules/ACLRoles/ACLRole.php');
class VIDynamicPanelsDisplayRoles{
    public function __construct(){
        $this->displayRoles();
    } 
    public function displayRoles() {
        $aclRole = new ACLRole();
        $rolesList =$aclRole->getAllRoles(true);
        foreach ($rolesList as $key => $value) {
            $data[] = array('id' => $value['id'], 'name' => $value['name']);
        }//end of foreach
        echo json_encode($data);
    }//end of function
}//end of class
new VIDynamicPanelsDisplayRoles();
?>