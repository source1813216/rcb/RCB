<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VIDeleteDynamicPanels{
    public function __construct(){
        $this->deleteDynamicPanels();
    } 
    public function deleteDynamicPanels(){
        if(isset($_POST['del_id'])){
            $delId = explode(',',$_POST['del_id']);
            foreach($delId as $id){
                //delete dynamic panels data
                $deleteDynamicPanels = "UPDATE vi_dynamic_panels SET deleted = 1 WHERE dynamic_panels_id = '$id'";
                $deleteDynamicPanelsResult = $GLOBALS['db']->query($deleteDynamicPanels);

                //delete dynamic panels conditions data
                $deleteDynamicPanelsConditions = "UPDATE vi_dynamic_panels_condition SET deleted = 1 
                                                                                     WHERE dynamic_panels_id = '$id'";
                $deleteDynamicPanelsConditionsResult = $GLOBALS['db']->query($deleteDynamicPanelsConditions);

                //delete dynamic panels fields data
                $deleteDynamicPanelsFields = "UPDATE vi_dynamic_panels_fields SET deleted = 1 
                                                                              WHERE dynamic_panels_id = '$id'";
                $deleteDynamicPanelsFieldsResult = $GLOBALS['db']->query($deleteDynamicPanelsFields);

                //delete dynamic panels fields  color data
                $deleteDynamicPanelsFieldsColor = "UPDATE vi_dynamic_panels_fields_color SET deleted = 1 
                                                                                         WHERE dynamic_panels_id = '$id'";
                $deleteDynamicPanelsFieldsColorResult = $GLOBALS['db']->query($deleteDynamicPanelsFieldsColor);
            }//end of foreach
        }//end of if
    }//end of function   
}//end of class
new VIDeleteDynamicPanels();
?>