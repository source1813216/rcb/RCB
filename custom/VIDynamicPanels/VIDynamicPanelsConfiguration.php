<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VIDynamicPanelsConfiguration{
    public function __construct(){
        $this->getConfiguration();
    } 
	public function getConfiguration(){
		$module = $_REQUEST['module'];
		$selData = "SELECT * FROM vi_dynamic_panels WHERE primary_module = '$module' AND deleted = 0";
		$selDataResult = $GLOBALS['db']->fetchOne($selData);
		if(!empty($selDataResult)){
			echo "1";
		}else{
			echo "0";
		}
	}
}
new VIDynamicPanelsConfiguration();
?>