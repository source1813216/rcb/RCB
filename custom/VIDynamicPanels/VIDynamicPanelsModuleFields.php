<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
include("custom/VIDynamicPanels/VIDynamicPanelsFunctions.php");
require_once("modules/AOW_WorkFlow/aow_utils.php");
class VIDynamicPanelsModuleFields{
    public function __construct(){
        $this->getModuleFields();
    } 
    public function getModuleFields(){
        require_once('include/utils.php');
        $module = $_REQUEST['aow_module']; //module
        $bean = BeanFactory::newBean($module); //bean
        $field = $bean->getFieldDefinitions(); //all fields defs
       
        $validType = array();
        if(isset($_REQUEST['type'])){
            $validType = getValidFieldsTypes($module,$_REQUEST['fieldName']);
        }

        $addressFieldData = array();
        foreach($field as $value){
            if($value['type'] == 'varchar' && (empty($validType) || in_array($value['type'], $validType))){
                if(isset($value['group'])){
                    $fieldName = $value['name'];
                    $fieldLabel = translate($value['vname'],$module);
                    $lastChar = substr($fieldLabel, -1);
                    if($lastChar == ':'){
                        $fieldLabel = substr_replace($fieldLabel, "", -1);
                    }
                    $addressFieldData[$fieldName] = $fieldLabel;
                }//end of if
            }//end of if
        }//end of if

        unset($addressFieldData['email1']);
        unset($addressFieldData['email2']);

        $data = getEditDetailViewFields('editview',$module,$validType);
        $datas = getEditDetailViewFields('detailview',$module,$validType);

        $value = '';
        $arrayMerge = array_merge($data,$addressFieldData,$datas); //merge array
        asort($arrayMerge);
        echo get_select_options_with_id($arrayMerge,$value);
        die;
    }//end of function  
}//end of class
new VIDynamicPanelsModuleFields();
?>