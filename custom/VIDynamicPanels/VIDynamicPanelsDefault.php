<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
include("custom/VIDynamicPanels/VIDynamicPanelsFunctions.php");
class VIDynamicPanelsDefault{
    public function __construct(){
        $this->defaultFunctionality();
    } 
    public function defaultFunctionality(){
        global $current_user, $app_strings, $mod_strings,$sugar_config;        
        $databaseType = $sugar_config['dbconfig']['db_type'];
        $currentUserId = $current_user->id; //user id
        $currentUserType = $current_user->is_admin; //user type
        if($currentUserType == 1){
            $currentUserTypeName = 'Administrator';
        }else if($currentUserType == 0){
            $currentUserTypeName = 'RegularUser';
        }//end of else if

        //get data
        $moduleName = $_REQUEST['module'];
        $actionName = $_REQUEST['actionName'];

        //user role
        $selectRoles = "SELECT * FROM acl_roles_users WHERE user_id = '$currentUserId' AND deleted = 0";
        $selectRoleResult = $GLOBALS['db']->query($selectRoles);
        $selectRoleData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selectRoles));
        $currentUserRoleId = array();
        if(!empty($selectRoleData)){
            while($selectRolesRow = $GLOBALS['db']->fetchByAssoc($selectRoleResult)){
                $currentUserRoleId[] = $selectRolesRow['role_id']; //userrole id
            }
        }else{
            $currentUserRoleId = array();
        }//end of else

        $bean = BeanFactory::newBean($moduleName);
        
        //select dynamic panels details
        $selDynamicPanels = "SELECT * FROM vi_dynamic_panels 
                                      WHERE primary_module = '$moduleName' 
                                      AND (user_type = 'AllUsers' OR user_type = '$currentUserTypeName') 
                                      AND deleted = 0 ORDER BY date_created ASC";
        $selDynamicPanelsResult = $GLOBALS['db']->query($selDynamicPanels);
        $selDynamicPanelsResultData = $GLOBALS['db']->fetchRow($GLOBALS['db']->query($selDynamicPanels));
        if(!empty($selDynamicPanelsResultData)){
            $configData = array();
            while($selDataRow = $GLOBALS['db']->fetchByAssoc($selDynamicPanelsResult)){
                $roleId = explode(',',$selDataRow['role_id']);
                $flag = 0;
                foreach ($roleId as $key => $value) {
                    if(in_array($value,$currentUserRoleId)){
                        $flag = 1;
                        $configData[$selDataRow['dynamic_panels_id']] = $selDataRow['dynamic_panels_id'];
                    }
                }
            }
            $dynamicPanelsId = reset($configData);
            if($flag == 1){
                $selData = "SELECT * FROM vi_dynamic_panels WHERE dynamic_panels_id = '$dynamicPanelsId'";
                $selDynamicPanelsRow = $GLOBALS['db']->fetchOne($selData);
                $defaultPanels = explode(',',$selDynamicPanelsRow['default_panel_hide']);

                //get panels and tabs
                if($actionName == 'EditView'){
                    $view = 'editview';
                }else if ($actionName == 'DetailView' || $actionName == 'view_GanttChart'){
                    $view = 'detailview';
                }else if($actionName == 'SubpanelCreates'){
                    $view = 'quickcreate';
                }//end of else if

                $panelArray = getEditviewDetailviewPanels($view,$moduleName);
                $tabArrays = getEditviewDetailviewTabs($view,$moduleName);

                $viewTabArray = array();
                foreach($tabArrays as $key=>$value){
                    $key = translate(strtoupper($key),$moduleName);
                    if($value['newTab'] == 1){
                        $viewTabArray[] = $key;
                    }//end of if
                }//end of foreach    
                
                //requiredFields
                $requiredFieldsArray = getEditviewDetailviewRequiredFields($view,$moduleName);//get all required fields

                //get panel fields
                $panelFieldArray = array();
                foreach($panelArray as $key=>$value){
                    $key = strtoupper($key);
                    foreach ($value as $values) {   
                        if (isset($mod_strings[$key])){
                            if(in_array($key,$defaultPanels)){
                               foreach ($values as $field) {
                                  $panelFieldArray[] = $field;
                               }//end of foreach   
                            }//end of if
                        }//end of if
                        if (isset($app_strings[$key])){
                            if(in_array($key,$defaultPanels)){
                                foreach ($values as $field) {
                                  $panelFieldArray[] = $field;
                                }//end of foreach
                            }//end of if
                        }//end of if
                    }//end of foreach
                }//end of foreach
                
                //remove default panels required fields validation which is hide 
                $removeFromValidatePanelFields = array_values(array_intersect($panelFieldArray, $requiredFieldsArray));

                $panelName = array();
                foreach($panelArray as $key=>$value){
                    $key = strtoupper($key);
                    $panelName[] = $key;
                }//end of foreach
            
                //default panel hide
                $defaultPanelsHide = array();
                foreach($panelName as $key=>$value){
                    $value = strtoupper($value);
                    if(in_array($value,$defaultPanels)){
                        $defaultPanelsHide[] = translate($value,$moduleName);
                    }//end of if   
                }//end of foreach
                $viewTabsHide = array();
                $viewPanelsHide = array();
                foreach($defaultPanelsHide as $key => $value) {
                    if(in_array($value,$viewTabArray)){
                        $viewTabsHide[] = $value;
                    }else{
                        $viewPanelsHide[] = $value;
                    }
                } 
                //default field hide
                $defaultFields = explode(',',$selDynamicPanelsRow['default_field_hide']);
                foreach ($defaultFields as $key => $value) {
                    $defaultFieldsHide[] = $value;
                }//end of foreach

                //remove default fields required validation which is hide
                $removeFromValidateDefaultFields = array_values(array_intersect($defaultFieldsHide, $requiredFieldsArray));
                $removeFromValidatePanelFields = json_encode($removeFromValidatePanelFields);

                $data = array('defaultFieldsHide' => $defaultFieldsHide,'viewTabsHide' => json_encode($viewTabsHide),'viewPanelsHide' => json_encode($viewPanelsHide),'removeFromValidatePanelFields' => $removeFromValidatePanelFields,'removeFromValidateDefaultFields' => $removeFromValidateDefaultFields);
                echo json_encode($data);
            }
        }//end of if
    }//end of function
}//end of class
new VIDynamicPanelsDefault();
?>