<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
include("custom/VIDynamicPanels/VIDynamicPanelsFunctions.php");
class VIAddDynamicPanels{
    public function __construct(){
        $this->addDynamicPanels();
    } 
    public function addDynamicPanels() {
        global $timedate;
        parse_str($_REQUEST['val'], $formData); //form data
        $recordId = $_REQUEST['id']; //record id

        $primaryModule = $formData['flow_module']; //module
        $name = $formData['dynamic_panels_name']; //dynamic panel name
        $userType = $formData['user_type']; //user type

        if(isset($formData['user_roles']) && !empty($formData['user_roles'])){
            $userRoles = implode(',',$formData['user_roles']);
        }else{
            $userRoles = '';
        }//end of else

        if(isset($formData['defaultpanel']) && !empty($formData['defaultpanel'])){
            $defaultPanels = implode(',',$formData['defaultpanel']);
        }else{
            $defaultPanels = '';
        }//end of else

        if(isset($formData['defaultfield']) && !empty($formData['defaultfield'])){
            $defaultFields = implode(',',$formData['defaultfield']);
        }else{
            $defaultFields = '';
        }//end of else

        if(isset($formData['panel_hide']) && !empty($formData['panel_hide'])){
            $panelHide = implode(',',$formData['panel_hide']);
        }else{
            $panelHide = '';
        }//end of else

        if(isset($formData['field_hide']) && !empty($formData['field_hide'])){
            $fieldHide = implode(',',$formData['field_hide']);
        }else{
            $fieldHide = '';
        }//end of else

        if(isset($formData['panel_show']) && !empty($formData['panel_show'])){
            $panelShow = implode(',',$formData['panel_show']);
        }else{
            $panelShow = '';
        }//end of else

        if(isset($formData['field_show']) && !empty($formData['field_show'])){
            $fieldShow = implode(',',$formData['field_show']);
        }else{
            $fieldShow = '';
        }//end of else

        if(isset($formData['field_readonly']) && !empty($formData['field_readonly'])){
            $fieldReadOnly = implode(',',$formData['field_readonly']);
        }else{
            $fieldReadOnly = '';
        }//end of else

        if(isset($formData['field_mandatory']) && !empty($formData['field_mandatory'])){
            $fieldMandatory = implode(',',$formData['field_mandatory']);
        }else{
            $fieldMandatory = '';
        }//end of else 

        $conditionalOperator = $formData['conditional_operator']; //conditional_operator
        $dateCreated = date('Y-m-d H:i:s'); //date_created

        //table name
        $dynamicPanelTable = 'vi_dynamic_panels';
        $dynamicPanelConditionTable = 'vi_dynamic_panels_condition';
        $dynamicPanelFieldTable = 'vi_dynamic_panels_fields';
        $dynamicPanelFieldColorTable = 'vi_dynamic_panels_fields_color';
        
        //insert data
        $dynamicPanelsId = create_guid();
        $insData = array("dynamic_panels_id" => "'".$dynamicPanelsId."'",
                        "name" => "'".$name."'",
                        "user_type" => "'".$userType."'",
                        "role_id" => "'".$userRoles."'",
                        "primary_module" => "'".$primaryModule."'",
                        "default_panel_hide" => "'".$defaultPanels."'",
                        "default_field_hide" => "'".$defaultFields."'",
                        "panel_hide" => "'".$panelHide."'",
                        "field_hide" => "'".$fieldHide."'",
                        "panel_show" => "'".$panelShow."'",
                        "field_show" => "'".$fieldShow."'",
                        "field_readonly" => "'".$fieldReadOnly."'",
                        "field_mandatory" => "'".$fieldMandatory."'",
                        "date_created" => "'".$dateCreated."'",
                        "conditional_operator" => "'".$conditionalOperator."'");
        if($recordId == ''){
            //insert dynamic panels data
            $insResult = insertDynamicPanelRecord($dynamicPanelTable,$insData);
        }else{
            //update dynamic panels data
            unset($insData['dynamic_panels_id']);
            unset($insData['date_created']);
            $where = array('dynamic_panels_id' => $recordId);
            $updateData = updateDynamicPanelRecord($dynamicPanelTable,$insData,$where);
        }//end of else

        $conditionId = array();
        $delId = $formData['aow_conditions_deleted'];
        if($recordId != ''){
            $delCondition = "DELETE FROM vi_dynamic_panels_condition WHERE dynamic_panels_id = '$recordId'";
            $delConditionResult = $GLOBALS['db']->query($delCondition);

            $delFieldData = "DELETE FROM vi_dynamic_panels_fields WHERE dynamic_panels_id = '$recordId'";
            $delFieldDataResult = $GLOBALS['db']->query($delFieldData);

            $delFieldColorData = "DELETE FROM vi_dynamic_panels_fields_color WHERE dynamic_panels_id = '$recordId'";
            $delFieldColorDataResult = $GLOBALS['db']->query($delFieldColorData);
        }//end of if
        if($recordId == ''){
            $dynPenalId = $dynamicPanelsId;     
        }else{
            $dynPenalId = $recordId;
        }//end of else

        //insert conditions
        //module_path
        foreach($formData['aow_conditions_module_path'] as $key => $values) {
            foreach ($values as $key => $value) {
                $id = create_guid();
                $insConditionData = array('id' => "'".$id."'",
                                         'module_path' => "'".$value."'",
                                         'dynamic_panels_id' => "'".$dynPenalId."'");
                $insResult = insertDynamicPanelRecord($dynamicPanelConditionTable,$insConditionData);
                $conditionId[] = $id;  
            }//end of if 
        }//end of foreach
        
        //field 
        foreach($formData['aow_conditions_field'] as $key => $values) {
            $updateFieldData = array('field' => "'".$values."'",
                                     'deleted' => $delId[$key]);
            $where = array('id' => $conditionId[$key]);
            $updateData = updateDynamicPanelRecord($dynamicPanelConditionTable,$updateFieldData,$where);
        }//end of foreach


        //operator
        foreach($formData['aow_conditions_operator'] as $key => $values) {
            $updateOperatorData = array('operator' => "'".$values."'",
                                       'deleted' => $delId[$key]);
            $where = array('id' => $conditionId[$key]);
            $updateData = updateDynamicPanelRecord($dynamicPanelConditionTable,$updateOperatorData,$where);
        }//end of foreach

        //value type
        foreach($formData['aow_conditions_value_type'] as $key => $values) {
            $updateValueTypeData = array('value_type' => "'".$values."'",
                                       'deleted' => $delId[$key]);
            $where = array('id' => $conditionId[$key]);
            $updateData = updateDynamicPanelRecord($dynamicPanelConditionTable,$updateValueTypeData,$where);
        }//end of foreach

        //value
        foreach($formData['aow_conditions_value'] as $key => $values) {
            $bean = BeanFactory::newBean($primaryModule);
            $field = $formData['aow_conditions_field'][$key];
            $valueType = $formData['aow_conditions_value_type'][$key];
            $fieldDef = $bean->field_defs[$field];
            if($valueType == 'Value'){
                if($fieldDef['type'] == 'datetime' || $fieldDef['type'] == 'datetimecombo'){
                    $date = date('Y-m-d',strtotime($values));
                    $time = date('h:i:s',strtotime($values));
                    $dbtime = $timedate->to_db_time($values);
                    $values = $date.' '.$dbtime;
                }else if($fieldDef['type'] == 'date'){
                    $values = date('Y-m-d',strtotime($values));
                }else if($fieldDef['type'] == 'multienum'){
                    $values = encodeMultienumValue($values);
                }else if($fieldDef['type'] == 'enum'){
                    $valueType = $formData['aow_conditions_value_type'][$key];
                    if($valueType == 'Multi'){
                        $values = encodeMultienumValue($values);
                    }//end of if
                }else {
                    $numericValue = str_replace( ',', '', $values);
                    if( is_numeric( $numericValue ) ) {
                        $values = $numericValue;
                    }
                }//end of else
            }else if($valueType == 'Date'){
                $values = base64_encode(serialize($values));
            }
            
            $updateValueData = array('value' => "'".$values."'",
                                     'deleted' => $delId[$key]);
            $where = array('id' => $conditionId[$key]);
            $updateData = updateDynamicPanelRecord($dynamicPanelConditionTable,$updateValueData,$where);         
        }//end of foreach

        //insert dynamic panels fields data
        $modulefieldId = array();
        $fieldDelId = $formData['field_deleted'];

        //field name
        foreach($formData['field'] as $key => $values) {
            if($values != ''){
                $fieldId = create_guid();
                $insFieldData = array('id' => "'".$fieldId."'",
                                     'field' => "'".$values."'",
                                     'dynamic_panels_id' => "'".$dynPenalId."'");
                $insResult = insertDynamicPanelRecord($dynamicPanelFieldTable,$insFieldData);
                $modulefieldId[$key] = $fieldId;
            }//end of if
        }//end of foreach

        //field value
        foreach($formData['field_value'] as $key => $values) {
            $bean = BeanFactory::newBean($primaryModule);
            $field = $formData['field'][$key];
            if($field != ''){
                $fieldDef = $bean->field_defs[$field];
                if($fieldDef['type'] == 'datetime' || $fieldDef['type'] == 'datetimecombo'){
                    $date = date('Y-m-d',strtotime($values));
                    $time = date('h:i:s',strtotime($values));
                    $dbtime = $timedate->to_db_time($values);
                    $values = $date.' '.$dbtime;
                }else if($fieldDef['type'] == 'date'){
                    $values = date('Y-m-d',strtotime($values));
                }else if($fieldDef['type'] == 'multienum'){
                    $values = encodeMultienumValue($values);    
                }else{
                    $numericValue = str_replace( ',', '', $values);
                    if( is_numeric( $numericValue ) ) {
                        $values = $numericValue;
                    }//end of if 
                }//end of else    
            }//end of if
            
            $updateFieldValueData = array('field_value' => "'".$values."'",
                                         'deleted' => $fieldDelId[$key]);
            $where = array('id' => $modulefieldId[$key]);
            $updateData = updateDynamicPanelRecord($dynamicPanelFieldTable,$updateFieldValueData,$where);    
        }//end of foreach
        
        //insert dynamic panels fields color data
        $modulefieldColorId = array();
        $fieldColorDelId = $formData['field_color_deleted'];

        //field name for color
        foreach($formData['field_color_name'] as $key => $values) {
            if($values != ''){
                $fieldId = create_guid();
                $insFieldColorData = array('id' => "'".$fieldId."'",
                                 'field_color_name' => "'".$values."'",
                                 'dynamic_panels_id' => "'".$dynPenalId."'");
                $insResult = insertDynamicPanelRecord($dynamicPanelFieldColorTable,$insFieldColorData);
                $modulefieldColorId[$key] = $fieldId;   
            }//end of if
        }//end of foreach

        //field color
        foreach($formData['field_color'] as $key => $values) {
            $updateFieldColorValueData = array('field_color' => "'".$values."'",
                                         'deleted' => $fieldColorDelId[$key]);
            $where = array('id' => $modulefieldColorId[$key]);
            $updateData = updateDynamicPanelRecord($dynamicPanelFieldColorTable,$updateFieldColorValueData,$where);
        }//end of foreach
    }//end of function
}//end of class
new VIAddDynamicPanels();
?>