<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("modules/AOW_WorkFlow/aow_utils.php");
class VIDynamicPanelsAllModulePanels{
    public function __construct(){
        $this->getPanels();
    } 
    public function getPanels(){
        require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
        global $app_strings,$mod_strings;
        $moduleName = $_REQUEST['flow_module'];
        $view_array = ParserFactory::getParser('editview',$moduleName);
        $panelArray = $view_array->_viewdefs['panels'];
        $detailview_array = ParserFactory::getParser('detailview',$moduleName);
        $panelArrays = $detailview_array->_viewdefs['panels'];

        $quickcreate_array = ParserFactory::getParser('quickcreate',$moduleName);
        $quickCreatePanelArray = $quickcreate_array->_viewdefs['panels'];

        $panelName =array();
        $editViewPanelName = array();
        $detaiViewPanelName = array();
        $quickCreatePanelName = array();
        foreach($panelArray as $key=>$value){
            $key = strtoupper($key);
            $editViewPanelName[] = array('panelLabel' => $key ,'panelName' => translate($key,$moduleName));
        }//end of foreach
        foreach($panelArrays as $key=>$value){
            $key = strtoupper($key);
            $detaiViewPanelName[] = array('panelLabel' => $key ,'panelName' => translate($key,$moduleName));
        }//end of foreeach
        foreach($quickCreatePanelArray as $key=>$value){
            $key = strtoupper($key);
            $quickCreatePanelName[] = array('panelLabel' => $key ,'panelName' => translate($key,$moduleName));
        }//end of foreach
        
        $arrayMerge = array_merge($editViewPanelName,$detaiViewPanelName,$quickCreatePanelName);
        $panelName = array_unique($arrayMerge,SORT_REGULAR);
        echo json_encode(array_values($panelName));
        die();
    }//end of function
}//end of class
new VIDynamicPanelsAllModulePanels();
?>