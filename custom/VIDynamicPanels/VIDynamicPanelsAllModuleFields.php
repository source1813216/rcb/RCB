<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
require_once("modules/AOW_WorkFlow/aow_utils.php");
class VIDynamicPanelsAllModuleFields{
    public function __construct(){
        $this->getFields();
    } 
    public function getFields(){
        require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
        require_once('include/utils.php');
        $module = $_REQUEST['flow_module'];
        $view_array = ParserFactory::getParser('editview',$module);
        $panelArray = $view_array->_viewdefs['panels'];
        $bean = BeanFactory::newBean($module);
        $field = $bean->getFieldDefinitions();
        $addressFieldData = array();
        foreach($field as $value){
            if($value['type'] == 'varchar'){
                if(isset($value['group']) && !empty($value['group'] )){
                    $fieldName = $value['name'];
                    $fieldLabel = translate($value['vname'],$module);
                    $lastChar = substr($fieldLabel, -1);
                    if($lastChar == ':'){
                        $fieldLabel = substr_replace($fieldLabel, "", -1);
                    }
                    $addressFieldData[] = array('fieldname'=> $fieldName,'label' => $fieldLabel,'type' => $value['type']);
                }
            }
        }
        $editviewData = $this->getEditDetailViewFields('editview',$module);
        $detailviewData = $this->getEditDetailViewFields('detailview',$module);
        foreach ($addressFieldData as $key => $value) {
            if($value['fieldname'] == 'email1'){
                unset($addressFieldData[$key]);
            }
            if($value['fieldname'] == 'email2'){
                unset($addressFieldData[$key]);
            }
        }
        $editviewData = array_values($editviewData);
        $detailviewData = array_values($detailviewData);
        $addressFieldData = array_values($addressFieldData);
        $arrayMerge = array_merge($editviewData,$addressFieldData,$detailviewData);
        $uniqueArray = array_unique($arrayMerge,SORT_REGULAR);
        echo json_encode(array_values($uniqueArray));
        die;
    }
    function getEditDetailViewFields($view,$module){
        require_once('modules/ModuleBuilder/parsers/ParserFactory.php');
        $view_array = ParserFactory::getParser($view,$module);
        $panelArray = $view_array->_viewdefs['panels'];//editview panels
        $bean = BeanFactory::newBean($module);
        $field = $bean->getFieldDefinitions();

        //editview fields
        $editViewFieldArray = array();
        foreach ($panelArray as $key => $value) {
            foreach ($value as $keys => $values) {
                $editViewFieldArray[] = $values;
            }//end of foreach
        }//end of foreach

        $data = array();
        foreach($editViewFieldArray as $key => $value) {
            foreach($value as $k => $v) {
                if(array_key_exists($v, $field)) {
                    require_once('include/utils.php');
                    $fieldLabel = translate($field[$v]['vname'], $module);
                    $lastChar = substr($fieldLabel, -1);
                    if($lastChar == ':'){
                        $fieldLabel = substr_replace($fieldLabel, "", -1);
                    }
                    $fieldName = $v;
                    $data[] = array('fieldname'=> $fieldName,'label' => $fieldLabel,'type' => $field[$v]['type']);
                }//end of if 
            }//end of foreach
        }//end of foreach

        //unset fields
        $unsetData = array('sample','pdfheader','line_items','configurationGUI','insert_fields','survey_questions_display','email1','action_lines','condition_lines','reminders','email2','aop_case_updates_threaded','internal','suggestion_box','case_update_form','update_text','email_opt_out','reschedule_history','pdffooter','product_image','filename','invite_templates','related_doc_rev_number','related_doc_name');
        
        foreach ($data as $key => $value) {
            if(in_array($value['fieldname'],$unsetData)){
                unset($data[$key]);
            }//end of if
        }//end of foreach

        if($module == 'AOK_KnowledgeBase' || $module == 'AOS_PDF_Templates'){
            foreach ($data as $key => $value) {
                if($value['fieldname'] == 'description'){
                    unset($data[$key]);
                    break;
                }//end of if
            }//end of foreach
        }//end of if
           
        if($module == 'jjwg_Maps' || $module == 'Meetings' || $module == 'Notes' || $module == 'Tasks'){
            foreach ($data as $key => $value) {
                if($value['fieldname'] == 'parent_name'){
                    unset($data[$key]);
                    break;
                }//end of if
            }//end of foreach
        }//end of if
        return $data;
    }//end of function
}//end of class
new VIDynamicPanelsAllModuleFields();
?>