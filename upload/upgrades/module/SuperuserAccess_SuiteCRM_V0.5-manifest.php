<?php
global $sugar_version, $sugar_flavor;
$manifest = array(
    'acceptable_sugar_versions' =>
        array(
            'regex_matches' =>
            array(
                '6.5.*',
                '6.7.*',
            ),
        ),
    'acceptable_sugar_flavors' =>
        array(
            'CE',
    ),
	'readme'=> '',
	'author' => 'FibreCRM',
	'description' => 'FibreCRM Super User Access - Restrict Admin Users from accessing specified admin features',
	'icon' => '',
	'is_uninstallable' => true,
	'name' => 'FibreCRM Super User Access',
	'published_date' => '24 Jan 2019',
	'type' => 'module',
	'version' => 'V0.5', // scripts/pre_install.php change version in backup file name also
	'remove_tables' => 'prompt',
);

$installdefs = array();
$installdefs['id'] = 'SuperuserAccess';

$installdefs['copy'] = array();
$installdefs['copy'] [] = array(
    'from' => '<basepath>/SugarModules/SuperuserAccess',
	'to' => 'modules/SuperuserAccess',
);
$installdefs['copy'] [] = array(
  	'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Administration/FI_SuperuserAccess.php',
  	'to' => 'custom/Extension/modules/Administration/Ext/Administration/FI_SuperuserAccess.php',
);
$installdefs['copy'] [] = array(
  	'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/en_us.FI_SuperuserAccess.php',
  	'to' => 'custom/Extension/modules/Administration/Ext/Language/en_us.FI_SuperuserAccess.php',
);
$installdefs['copy'] [] = array(
  	'from' => '<basepath>/custom/modules/Administration/index.php',
  	'to' => 'custom/modules/Administration/index.php',
);
$installdefs['copy'] [] = array(
	'from' => '<basepath>/custom/themes/default/images/',
	'to' => 'custom/themes/default/images/',
);
  
$installdefs['post_uninstall'] = array();
$installdefs['post_uninstall'] []= '<basepath>/scripts/post_uninstall.php';
