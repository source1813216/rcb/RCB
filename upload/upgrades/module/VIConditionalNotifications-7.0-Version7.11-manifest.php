<?php
/*********************************************************************************
 * This file is part of package Conditional Notifications.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Conditional Notifications is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$manifest = array (
  0 => 
  array (
    'acceptable_sugar_versions' => 
    array (
      0 => '',
    ),
  ),
  1 => 
  array (
    'acceptable_sugar_flavors' => 
    array (
      0 => 'CE',
      1 => 'PRO',
      2 => 'ENT',
    ),
  ),
  'readme' => '',
  'key' => '',
  'author' => 'Variance Infotech PVT. LTD',
  'description' => 'Conditional Notifications Plugin',
  'icon' => '',
  'is_uninstallable' => true,
  'name' => 'Conditional Notifications',
  'published_date' => '2020-04-27 15:20:54',
  'type' => 'module',
  'version' => 'v7.0',
  'remove_tables' => 'prompt',
);
$installdefs = array (
  'id' => 'Conditional Notifications',
  'beans' => //remove this bean or replace with your own module name.
        array (
            array (
              'module' => 'VIConditionalNotificationsLicenseAddon',
              'class' => 'VIConditionalNotificationsLicenseAddon',
              'path' => 'modules/VIConditionalNotificationsLicenseAddon/VIConditionalNotificationsLicenseAddon.php',
              'tab' => false,
            ),
        ),
  'post_execute' => array(  0 =>  '<basepath>/scripts/post_execute.php',),
  'post_install' => array(  0 =>  '<basepath>/scripts/post_install.php',),
  'post_uninstall' => array(  0 =>  '<basepath>/scripts/post_uninstall.php',),
  'pre_execute' => array(  0 =>  '<basepath>/scripts/pre_execute.php',),
  'copy' => 
  array (
	  0 => 
    array (
      'from' => '<basepath>/custom/Extension/application/Ext/EntryPointRegistry/VIConditionalNotificationsEntryPoint.php',
      'to' => 'custom/Extension/application/Ext/EntryPointRegistry/VIConditionalNotificationsEntryPoint.php',
    ),
    1 => 
    array (
      'from' => '<basepath>/custom/Extension/application/Ext/LogicHooks/VIConditionalNotifications_Hook.php',
      'to' => 'custom/Extension/application/Ext/LogicHooks/VIConditionalNotifications_Hook.php',
    ),
    2 => 
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/ActionViewMap/VIConditionalNotificationsAction_View_Map.ext.php',
      'to' => 'custom/Extension/modules/Administration/Ext/ActionViewMap/VIConditionalNotificationsAction_View_Map.ext.php',
    ), 
    3 => 
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Administration/VIConditionalNotificationsAdministration.ext.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Administration/VIConditionalNotificationsAdministration.ext.php',
    ),
    4 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.en_us.lang.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.en_us.lang.php',
    ),
    5 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/VIConditionalNotificationsLicenseAddon/Ext/ActionViewMap/VIConditionalNotificationsLicenseAddon_actionviewmap.php',
      'to' => 'custom/Extension/modules/VIConditionalNotificationsLicenseAddon/Ext/ActionViewMap/VIConditionalNotificationsLicenseAddon_actionviewmap.php',
    ),
    6 => 
    array (
      'from' => '<basepath>/custom/include/VIConditionalNotifications/VIConditionalNotifications.php',
      'to' => 'custom/include/VIConditionalNotifications/VIConditionalNotifications.php',
    ),
    7 => 
    array (
      'from' => '<basepath>/custom/include/VIConditionalNotifications/VIConditionalNotificationsDetailViewCheckCondition.js',
      'to' => 'custom/include/VIConditionalNotifications/VIConditionalNotificationsDetailViewCheckCondition.js',
    ),
    8 => 
    array (
      'from' => '<basepath>/custom/include/VIConditionalNotifications/VIConditionalNotificationsEditViewCheckCondition.js',
      'to' => 'custom/include/VIConditionalNotifications/VIConditionalNotificationsEditViewCheckCondition.js',
    ),
    9 => 
    array (
      'from' => '<basepath>/custom/include/VIConditionalNotifications/VIConditionalNotificationsIconCss.css',
      'to' => 'custom/include/VIConditionalNotifications/VIConditionalNotificationsIconCss.css',
    ),
    10 => 
    array (
      'from' => '<basepath>/custom/modules/Administration/css/VIConditionalNotificationsCss.css',
      'to' => 'custom/modules/Administration/css/VIConditionalNotificationsCss.css',
    ),
    11 => 
    array (
      'from' => '<basepath>/custom/modules/Administration/js/VIConditionalNotificationsConditionLines.js',
      'to' => 'custom/modules/Administration/js/VIConditionalNotificationsConditionLines.js',
    ),
    12 => 
    array (
      'from' => '<basepath>/custom/modules/Administration/js/VIConditionalNotificationsEditView.js',
      'to' => 'custom/modules/Administration/js/VIConditionalNotificationsEditView.js',
    ),
    13 => 
    array (
      'from' => '<basepath>/custom/modules/Administration/tpl/vi_conditionalnotificationseditview.tpl',
      'to' => 'custom/modules/Administration/tpl/vi_conditionalnotificationseditview.tpl',
    ),
    14 => 
    array (
      'from' => '<basepath>/custom/modules/Administration/tpl/vi_conditionalnotificationslistview.tpl',
      'to' => 'custom/modules/Administration/tpl/vi_conditionalnotificationslistview.tpl',
    ),
    15 => 
    array (
      'from' => '<basepath>/custom/modules/Administration/views/view.vi_conditionalnotificationseditview.php',
      'to' => 'custom/modules/Administration/views/view.vi_conditionalnotificationseditview.php',
    ),
    16 => 
    array (
      'from' => '<basepath>/custom/modules/Administration/views/view.vi_conditionalnotificationslistview.php',
      'to' => 'custom/modules/Administration/views/view.vi_conditionalnotificationslistview.php',
    ),
    17 =>
    array (
      'from' => '<basepath>/custom/modules/VIConditionalNotificationsLicenseAddon',
      'to' => 'custom/modules/VIConditionalNotificationsLicenseAddon',
    ),
    18 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIAddConditionalNotifications.php',
      'to' => 'custom/VIConditionalNotifications/VIAddConditionalNotifications.php',
    ),
    19 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationModuleFields.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationModuleFields.php',
    ),
    20 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsDetailViewCheckCondition.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsDetailViewCheckCondition.php',
    ),
    21 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsEditviewCheckCondition.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsEditviewCheckCondition.php',
    ),
    22 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsEditviewDuplicateCheckCondition.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsEditviewDuplicateCheckCondition.php',
    ),
    23 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsFieldTypeOptions.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsFieldTypeOptions.php',
    ),
    24 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsModuleFieldLabel.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleFieldLabel.php',
    ),
    25 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsModuleFieldType.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleFieldType.php',
    ),
    26 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsModuleOperatorField.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleOperatorField.php',
    ),
    27 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsModuleRelationships.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsModuleRelationships.php',
    ),
    28 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsOnSaveCheckCondition.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsOnSaveCheckCondition.php',
    ),
    29 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIDeleteConditionalNotifications.php',
      'to' => 'custom/VIConditionalNotifications/VIDeleteConditionalNotifications.php',
    ),
    30 =>
    array (
      'from' => '<basepath>/images/ConditionalNotifications.png',
      'to' => 'themes/SuiteP/images/ConditionalNotifications.png',
    ),
    31 =>
    array (
      'from' => '<basepath>/images/ConditionalNotifications.svg',
      'to' => 'themes/SuiteP/images/ConditionalNotifications.svg',
    ),
    32 =>
    array (
      'from' => '<basepath>/images/ConditionalNotifications.svg',
      'to' => 'themes/default/images/ConditionalNotifications.svg',
    ),
    33 =>
    array (
      'from' => '<basepath>/images/ConditionalNotifications.png',
      'to' => 'themes/default/images/ConditionalNotifications.png',
    ),
    34 =>
    array (
      'from' => '<basepath>/images/ConditionalNotification.png',
      'to' => 'themes/SuiteP/images/ConditionalNotification.png',
    ),
    35 =>
    array (
      'from' => '<basepath>/images/ConditionalNotification.png',
      'to' => 'themes/default/images/ConditionalNotification.png',
    ),
    36 =>
    array (
      'from' => '<basepath>/modules/VIConditionalNotificationsLicenseAddon',
      'to' => 'modules/VIConditionalNotificationsLicenseAddon',
    ),
    37 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsDuplicateCheckCondition.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsDuplicateCheckCondition.php',
    ),
    38 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsConfiguration.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsConfiguration.php',
    ),
    39 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.de_DE.lang.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.de_DE.lang.php',
    ),
    40 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.es_ES.lang.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.es_ES.lang.php',
    ),
    41 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.fr_FR.lang.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.fr_FR.lang.php',
    ),
    42 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.hu_HU.lang.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.hu_HU.lang.php',
    ),
    43 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.it_IT.lang.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.it_IT.lang.php',
    ),
    44 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.nl_NL.lang.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.nl_NL.lang.php',
    ),
    45 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.pt_BR.lang.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.pt_BR.lang.php',
    ),
    46 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.ru_RU.lang.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.ru_RU.lang.php',
    ),
    47 =>
    array (
      'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.ua_UA.lang.php',
      'to' => 'custom/Extension/modules/Administration/Ext/Language/VIConditionalNotifications.ua_UA.lang.php',
    ),
    48 =>
    array (
      'from' => '<basepath>/custom/VIConditionalNotifications/VIConditionalNotificationsFunctions.php',
      'to' => 'custom/VIConditionalNotifications/VIConditionalNotificationsFunctions.php',
    ),
  ),
);
?>