<?php
/*********************************************************************************
 * This file is part of package Dynamic Panels.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Dynamic Panels is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$manifest = array (
    0 =>
        array (
            'acceptable_sugar_versions' => 
                array (
                    0 => '',
                ),
        ),
    1 => 
        array (
            'acceptable_sugar_flavors' => 
                array (
                    0 => 'CE',
                    1 => 'PRO',
                    2 => 'ENT',
                ),
        ),
    'readme' => '',
    'key' => '',
    'author' => 'Variance InfoTech PVT. LTD',
    'description' => 'Dynamic Panels Plugin',
    'icon' => '',
    'is_uninstallable' => true,
    'name' => 'Dynamic Panels',
    'published_date' => '2020-05-20 18:20:54',
    'type' => 'module',
    'version' => 'v11.0',
    'remove_tables' => 'prompt',
);
$installdefs = array (
    'id' => 'Dynamic Panels',
    'beans' => //remove this bean or replace with your own module name.
            array (
                array (
                    'module' => 'VIDynamicPanelsLicenseAddon',
                    'class' => 'VIDynamicPanelsLicenseAddon',
                    'path' => 'modules/VIDynamicPanelsLicenseAddon/VIDynamicPanelsLicenseAddon.php',
                    'tab' => false,
                ),
            ),
    'post_execute' => array(  0 =>  '<basepath>/scripts/post_execute.php',),
    'post_install' => array(  0 =>  '<basepath>/scripts/post_install.php',),
    'post_uninstall' => array(  0 =>  '<basepath>/scripts/post_uninstall.php',),
    'pre_execute' => array(  0 =>  '<basepath>/scripts/pre_execute.php',),
    'copy' => 
        array (
            0 => 
                array (
                    'from' => '<basepath>/custom/Extension/application/Ext/EntryPointRegistry/VIDynamicPanelsEntryPoint.php',
                    'to' => 'custom/Extension/application/Ext/EntryPointRegistry/VIDynamicPanelsEntryPoint.php',
                ),
            1 => 
                array (
                    'from' => '<basepath>/custom/Extension/application/Ext/LogicHooks/VIDynamicPanels_Hook.php',
                    'to' => 'custom/Extension/application/Ext/LogicHooks/VIDynamicPanels_Hook.php',
                ),
            2 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/ActionViewMap/VIDynamicPanelsAction_View_Map.ext.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/ActionViewMap/VIDynamicPanelsAction_View_Map.ext.php',
                ), 
            3 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Administration/VIDynamicPanelsAdministration.ext.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Administration/VIDynamicPanelsAdministration.ext.php',
                ),
            4 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.de_DE.lang.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.de_DE.lang.php',
                ),
            5 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.en_us.lang.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.en_us.lang.php',
                ),
            6 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.es_ES.lang.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.es_ES.lang.php',
                ),
            7 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.fr_FR.lang.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.fr_FR.lang.php',
                ),
            8 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.hu_HU.lang.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.hu_HU.lang.php',
                ),
            9 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.it_IT.lang.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.it_IT.lang.php',
                ),
            10 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.nl_NL.lang.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.nl_NL.lang.php',
                ),
            11 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.pt_BR.lang.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.pt_BR.lang.php',
                ),
            12 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.ru_RU.lang.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.ru_RU.lang.php',
                ),
            13 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.ua_UA.lang.php',
                    'to' => 'custom/Extension/modules/Administration/Ext/Language/VIDynamicPanels.ua_UA.lang.php',
                ),
            14 => 
                array (
                    'from' => '<basepath>/custom/Extension/modules/VIDynamicPanelsLicenseAddon/Ext/ActionViewMap/VIDynamicPanelsLicenseAddon_actionviewmap.php',
                    'to' => 'custom/Extension/modules/VIDynamicPanelsLicenseAddon/Ext/ActionViewMap/VIDynamicPanelsLicenseAddon_actionviewmap.php',
                ),
            15 => 
                array (
                    'from' => '<basepath>/custom/include/VIDynamicPanels/VIDynamicPanels.php',
                    'to' => 'custom/include/VIDynamicPanels/VIDynamicPanels.php',
                ),
            16 => 
                array (
                    'from' => '<basepath>/custom/include/VIDynamicPanels/VIDynamicPanelsCheckCondition.js',
                    'to' => 'custom/include/VIDynamicPanels/VIDynamicPanelsCheckCondition.js',
                ),
            17 => 
                array (
                    'from' => '<basepath>/custom/include/VIDynamicPanels/VIDynamicPanelsDetailViewCheckCondition.js',
                    'to' => 'custom/include/VIDynamicPanels/VIDynamicPanelsDetailViewCheckCondition.js',
                ),
            18 => 
                array (
                    'from' => '<basepath>/custom/include/VIDynamicPanels/VIDynamicPanelsIcon.css',
                    'to' => 'custom/include/VIDynamicPanels/VIDynamicPanelsIcon.css',
                ),
            19 => 
                array (
                    'from' => '<basepath>/custom/modules/Administration/css/VIDynamicPanelsCss.css',
                    'to' => 'custom/modules/Administration/css/VIDynamicPanelsCss.css',
                ),
            20 => 
                array (
                    'from' => '<basepath>/custom/modules/Administration/tpl/vi_dynamicpanelseditview.tpl',
                    'to' => 'custom/modules/Administration/tpl/vi_dynamicpanelseditview.tpl',
                ),
            21 => 
                array (
                    'from' => '<basepath>/custom/modules/Administration/tpl/vi_dynamicpanelslistview.tpl',
                    'to' => 'custom/modules/Administration/tpl/vi_dynamicpanelslistview.tpl',
                ),
            22 => 
                array (
                    'from' => '<basepath>/custom/modules/Administration/views/view.vi_dynamicpanelseditview.php',
                    'to' => 'custom/modules/Administration/views/view.vi_dynamicpanelseditview.php',
                ),
            23 => 
                array (
                    'from' => '<basepath>/custom/modules/Administration/js/VIDynamicPanelsConditionLines.js',
                    'to' => 'custom/modules/Administration/js/VIDynamicPanelsConditionLines.js',
                ),
            24 => 
                array (
                    'from' => '<basepath>/custom/modules/Administration/js/VIDynamicPanelsEditView.js',
                    'to' => 'custom/modules/Administration/js/VIDynamicPanelsEditView.js',
                ),
            25 => 
                array (
                    'from' => '<basepath>/custom/modules/Administration/js/VIDynamicPanelsJscolor.js',
                    'to' => 'custom/modules/Administration/js/VIDynamicPanelsJscolor.js',
                ),
            26 =>
                array (
                    'from' => '<basepath>/custom/modules/VIDynamicPanelsLicenseAddon',
                    'to' => 'custom/modules/VIDynamicPanelsLicenseAddon',
                ),
            27 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIAddDynamicPanels.php',
                    'to' => 'custom/VIDynamicPanels/VIAddDynamicPanels.php',
                ),
            28  =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDeleteDynamicPanels.php',
                    'to' => 'custom/VIDynamicPanels/VIDeleteDynamicPanels.php',
                ),
            29 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsAllModuleFields.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsAllModuleFields.php',
                ),
            30 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsAllModulePanels.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsAllModulePanels.php',
                ),
            31 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsCheckCondition.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsCheckCondition.php',
                ),
            32 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsDefault.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsDefault.php',
                ),
            33 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsDetailViewCheckCondition.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsDetailViewCheckCondition.php',
                ),
            34 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsDisplayRoles.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsDisplayRoles.php',
                ),
            35 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsFieldTypeOptions.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsFieldTypeOptions.php',
                ),
            36 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsFunctions.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsFunctions.php',
                ),
            37 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsModuleFieldLabel.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFieldLabel.php',
                ),
            38 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsModuleFields.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFields.php',
                ),
            39 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsModuleFieldType.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFieldType.php',
                ),
            40 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsModuleFieldValue.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleFieldValue.php',
                ),
            41 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsModuleOperatorField.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleOperatorField.php',
                ),
            42 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsModuleRelationships.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsModuleRelationships.php',
                ),
            43 =>
                array (
                    'from' => '<basepath>/images/DynamicPanels.png',
                    'to' => 'themes/SuiteP/images/DynamicPanels.png',
                ),
            44 =>
                array (
                    'from' => '<basepath>/images/DynamicPanels.svg',
                    'to' => 'themes/SuiteP/images/DynamicPanels.svg',
                ),
            45 =>
                array (
                    'from' => '<basepath>/images/DynamicPanels.png',
                    'to' => 'themes/default/images/DynamicPanels.png',
                ),
            46 =>
                array (
                    'from' => '<basepath>/images/DynamicPanels.svg',
                    'to' => 'themes/default/images/DynamicPanels.svg',
                ),
            47 =>
                array (
                    'from' => '<basepath>/images/DynamicPanel.png',
                    'to' => 'themes/default/images/DynamicPanel.png',
                ),
            48 =>
                array (
                    'from' => '<basepath>/images/DynamicPanel.png',
                    'to' => 'themes/SuiteP/images/DynamicPanel.png',
                ),
            49 =>
                array (
                    'from' => '<basepath>/modules/VIDynamicPanelsLicenseAddon',
                    'to' => 'modules/VIDynamicPanelsLicenseAddon',
                ),
            50 => 
                array (
                    'from' => '<basepath>/custom/modules/Administration/views/view.vi_dynamicpanelslistview.php',
                    'to' => 'custom/modules/Administration/views/view.vi_dynamicpanelslistview.php',
                ),
            51 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsConfiguration.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsConfiguration.php',
                ),
            52 =>
                array (
                    'from' => '<basepath>/custom/VIDynamicPanels/VIDynamicPanelsCheckValidFieldType.php',
                    'to' => 'custom/VIDynamicPanels/VIDynamicPanelsCheckValidFieldType.php',
                ),
        ),
);
?>