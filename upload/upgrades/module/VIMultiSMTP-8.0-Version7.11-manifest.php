<?php
/*********************************************************************************
 * This file is part of package MultiSMTP.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of MultiSMTP is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
  $manifest = array (
    0 => 
    array (
      'acceptable_sugar_versions' => 
      array (
        0 => '',
      ),
    ),
    1 => 
    array (
      'acceptable_sugar_flavors' => 
      array (
        0 => 'CE',
        1 => 'PRO',
        2 => 'ENT',
      ),
    ),
    'readme' => '',
    'key' => '',
    'author' => 'Variance Infotech PVT. LTD',
    'description' => 'Multi SMTP Plugin',
    'icon' => '',
    'is_uninstallable' => true,
    'name' => 'Multi SMTP',
    'published_date' => '2020-04-23 15:26:54',
    'type' => 'module',
    'version' => 'v8.0',
    'remove_tables' => 'prompt',
  );


  $installdefs = array (
    'id' => 'MultiSMTP',
    'beans' => //remove this bean or replace with your own module name.
        array (
            array (
              'module' => 'VIMultiSMTPLicenseAddon',
              'class' => 'VIMultiSMTPLicenseAddon',
              'path' => 'modules/VIMultiSMTPLicenseAddon/VIMultiSMTPLicenseAddon.php',
              'tab' => false,
            ),
        ),
    'pre_execute' => array( 0 => '<basepath>/scripts/pre_execute.php',),
    'post_execute' => array(  0 =>  '<basepath>/scripts/post_execute.php',),
    'post_install' => array(  0 =>  '<basepath>/scripts/post_install.php',),
    'post_uninstall' => array(  0 =>  '<basepath>/scripts/post_uninstall.php',),
    'copy' => 
    array (
  	 0 => 
      array (
        'from' => '<basepath>/custom/Extension/application/Ext/EntryPointRegistry/VIMultiplesmtp_EntryPoint.php',
        'to' => 'custom/Extension/application/Ext/EntryPointRegistry/VIMultiplesmtp_EntryPoint.php',
      ),
  	 1 =>
      array (
        'from' => '<basepath>/custom/VIMultiSMTP/VIMultiplesmtp_Status.php',
        'to' => 'custom/VIMultiSMTP/VIMultiplesmtp_Status.php',
      ),
     2 =>
      array (
        'from' => '<basepath>/custom/VIMultiSMTP/VIOutgoing_Server_Setting.php',
        'to' => 'custom/VIMultiSMTP/VIOutgoing_Server_Setting.php',
      ),
     3 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/ActionViewMap/VIMultiplesmtp_action_view_map.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/ActionViewMap/VIMultiplesmtp_action_view_map.ext.php',
      ),
     4 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Administration/VIMultiplesmtp_administration.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Administration/VIMultiplesmtp_administration.ext.php',
      ),
     5 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_en_us.lang.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_en_us.lang.ext.php',
      ),
     6 =>
      array (
        'from' => '<basepath>/custom/modules/Administration/views/view.enablemultismtp.php',
        'to' => 'custom/modules/Administration/views/view.enablemultismtp.php',
      ),
     7 =>
      array (
        'from' => '<basepath>/custom/modules/Administration/tpl/enablemultismtp.tpl',
        'to' => 'custom/modules/Administration/tpl/enablemultismtp.tpl',
      ),
     8 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/AOW_Actions/Ext/Actions/VIMultiplesmtp_actions.ext.php',
        'to' => 'custom/Extension/modules/AOW_Actions/Ext/Actions/VIMultiplesmtp_actions.ext.php',
      ),
     9 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/AOW_Actions/Ext/Language/VIMultiplesmtp_en_us.lang.ext.php',
        'to' => 'custom/Extension/modules/AOW_Actions/Ext/Language/VIMultiplesmtp_en_us.lang.ext.php',
      ),
     10 =>
      array (
        'from' => '<basepath>/custom/modules/AOW_Actions/actions/actionSendEmailUsingMultiSMTP.php',
        'to' => 'custom/modules/AOW_Actions/actions/actionSendEmailUsingMultiSMTP.php',
      ),
     11 =>
      array (
        'from' => '<basepath>/custom/modules/AOW_Actions/actions/actionSendEmailUsingMultiSMTP.js',
        'to' => 'custom/modules/AOW_Actions/actions/actionSendEmailUsingMultiSMTP.js',
      ),
     12 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/VIMultiSMTPLicenseAddon/Ext/ActionViewMap',
        'to' => 'custom/Extension/modules/VIMultiSMTPLicenseAddon/Ext/ActionViewMap',
      ),
     13 =>
      array (
        'from' => '<basepath>/custom/modules/VIMultiSMTPLicenseAddon',
        'to' => 'custom/modules/VIMultiSMTPLicenseAddon',
      ),
     14 =>
      array (
        'from' => '<basepath>/modules/VIMultiSMTPLicenseAddon',
        'to' => 'modules/VIMultiSMTPLicenseAddon',
      ),
     15 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/ActionViewMap/VIMultiplesmtp_users_actionviewmap.php',
        'to' => 'custom/Extension/modules/Users/Ext/ActionViewMap/VIMultiplesmtp_users_actionviewmap.php',
      ),
     16 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/LogicHooks/VIMultiplesmtp_logichooks.php',
        'to' => 'custom/Extension/modules/Users/Ext/LogicHooks/VIMultiplesmtp_logichooks.php',
      ),
     17 =>
      array (
        'from' => '<basepath>/custom/modules/Users/tpl/multismtp.tpl',
        'to' => 'custom/modules/Users/tpl/multismtp.tpl',
      ),
     18 =>
      array (
        'from' => '<basepath>/custom/modules/Users/views/view.multismtp.php',
        'to' => 'custom/modules/Users/views/view.multismtp.php',
      ),
     19 =>
      array (
        'from' => '<basepath>/custom/modules/Users/VIMultiplesmtp_custom_forms.php',
        'to' => 'custom/modules/Users/VIMultiplesmtp_custom_forms.php',
      ),
     20 =>
      array (
        'from' => '<basepath>/custom/modules/Users/outgoing_server_settings.php',
        'to' => 'custom/modules/Users/outgoing_server_settings.php',
      ),
     21 =>
      array (
        'from' => '<basepath>/custom/modules/Emails/controller.php',
        'to' => 'custom/modules/Emails/controller.php',
      ), 
     22 =>
      array (
        'from' => '<basepath>/custom/modules/Emails/EmailsController.php',
        'to' => 'custom/modules/Emails/EmailsController.php',
      ),
      23 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_es_ES.lang.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_es_ES.lang.ext.php',
      ),
      24 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_de_DE.lang.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_de_DE.lang.ext.php',
      ),
      25 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_fr_FR.lang.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_fr_FR.lang.ext.php',
      ),
      26 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_hu_HU.lang.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_hu_HU.lang.ext.php',
      ),
      27 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_it_IT.lang.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_it_IT.lang.ext.php',
      ),
      28 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_nl_NL.lang.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_nl_NL.lang.ext.php',
      ),
      29 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_pt_BR.lang.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_pt_BR.lang.ext.php',
      ),
      30 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_ru_RU.lang.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_ru_RU.lang.ext.php',
      ),
      31 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_ua_UA.lang.ext.php',
        'to' => 'custom/Extension/modules/Administration/Ext/Language/VIMultiplesmtp_ua_UA.lang.ext.php',
      ),
      32 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_de_DE.lang.php',
        'to' => 'custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_de_DE.lang.php',
      ),
      33 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_en_us.lang.php',
        'to' => 'custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_en_us.lang.php',
      ),
      34 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_es_ES.lang.php',
        'to' => 'custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_es_ES.lang.php',
      ),
      35 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_fr_FR.lang.php',
        'to' => 'custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_fr_FR.lang.php',
      ),
      36 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_nl_NL.lang.php',
        'to' => 'custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_nl_NL.lang.php',
      ),
      37 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_pt_BR.lang.php',
        'to' => 'custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_pt_BR.lang.php',
      ),
      38 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_hu_HU.lang.php',
        'to' => 'custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_hu_HU.lang.php',
      ),
      39 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_it_IT.lang.php',
        'to' => 'custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_it_IT.lang.php',
      ),
      40 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_ru_RU.lang.php',
        'to' => 'custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_ru_RU.lang.php',
      ),
      41 =>
      array (
        'from' => '<basepath>/custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_ua_UA.lang.php',
        'to' => 'custom/Extension/modules/Users/Ext/Language/VIMultiplesmtp_ua_UA.lang.php',
      ),
      42 =>
      array (
        'from' => '<basepath>/custom/VIMultiSMTP/VICheckMultiSMTPStatus.php',
        'to' => 'custom/VIMultiSMTP/VICheckMultiSMTPStatus.php',
      ),
    ), 
);
?>