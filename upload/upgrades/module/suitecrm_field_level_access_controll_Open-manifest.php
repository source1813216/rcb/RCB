<?php

/**
 * 
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
global $sugar_config, $db, $current_user;
$manifest = array(
    'acceptable_sugar_flavors' => array('CE'),
    'acceptable_sugar_versions' => array(
        'regex_matches' => array(
            0 => "6.5.*",
        ),
    ),
    'name' => 'SuiteCRM Field Level Access Control',
    'readme' => '',
    'key' => 'bc',
    'author' => 'Biztech Consultancy',
    'description' => 'Restrict the access (view/edit) of certain fields for certain users based on assigned roles to those users.',
    'icon' => '',
    'is_uninstallable' => true,
    'published_date' => '2018-06-29 00:00:00',
    'type' => 'module',
    'version' => '1.1',
    'remove_tables' => 'prompt',
);
$installdefs = array(
    'id' => 'SuiteCRM_Field_Access_Control_Plugin',
    'beans' =>
    array(
        0 =>
        array(
            'module' => 'access_control_fields',
            'class' => 'access_control_fields',
            'path' => 'modules/access_control_fields/access_control_fields.php',
            'tab' => false,
        ),
    ),
    'layoutdefs' =>
    array(
    ),
    'relationships' =>
    array(
    ),
    'pre_execute' => array(
        0 => '<basepath>/scripts/pre_execute.php',
    ),
    'post_execute' => array(
        0 => '<basepath>/scripts/post_execute.php',
    ),
    'post_uninstall' => array(
        0 => '<basepath>/scripts/post_uninstall.php',
    ),
    //Common files in both suite and sugar
    'copy' =>
    array(
        0 =>
        array(
            'from' => '<basepath>/Modules/modules/access_control_fields/',
            'to' => 'modules/access_control_fields/',
        ),
        1 =>
        array(
            'from' => '<basepath>/Modules/custom/',
            'to' => 'custom/',
        ),
        2 =>
        array(
            'from' => '<basepath>/Modules/tab_panel/',
            'to' => 'custom/themes/tab_panel/',
        ),
        3 =>
        array(
            'from' => '<basepath>/Modules/custom/include/ListView/ListViewGeneric.tpl',
            'to' => 'custom/themes/SuiteP/include/ListView/ListViewGeneric.tpl',
        ),
        4 =>
        array(
            'from' => '<basepath>/Modules/custom/include/ListView/ListViewGeneric.tpl',
            'to' => 'custom/modules/AM_ProjectTemplates/tpls/ListViewGeneric.tpl',
        ),
        5 =>
        array(
            'from' => '<basepath>/Modules/custom/include/SubPanel/tpls/SubPanelDynamic.tpl',
            'to' => 'custom/themes/SuiteP/include/SubPanel/tpls/SubPanelDynamic.tpl',
        ),
        6 =>
        array(
            'from' => '<basepath>/Modules/custom/include/Popups/tpls/PopupGeneric.tpl',
            'to' => 'custom/themes/SuiteP/include/Popups/tpls/PopupGeneric.tpl',
        ),
    ),
);
/* * *************************************Suite CRM Smarty Files******************************** */
if (version_compare($sugar_config['suitecrm_version'], '7.10', '<')) {
    array_push($installdefs['copy'], array(
        'from' => '<basepath>/Modules/include/Sugar_Smarty.php',
        'to' => 'include/Sugar_Smarty.php',
    ));
}