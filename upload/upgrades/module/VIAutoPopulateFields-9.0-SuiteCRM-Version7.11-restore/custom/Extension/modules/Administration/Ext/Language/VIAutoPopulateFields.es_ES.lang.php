<?php
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Campos de auto rellenar';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Administrar campos de llenado automático según los campos relacionados';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'ATRÁS';
$mod_strings['LBL_NEXT_BUTTON'] = 'SIGUIENTE';
$mod_strings['LBL_CLEAR_BUTTON'] = 'CLARA';
$mod_strings['LBL_SAVE_BUTTON'] = 'SALVAR';
$mod_strings['LBL_CANCEL_BUTTON'] = 'CANCELAR';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Seleccione una opcion';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Módulo de origen (Copiar datos a)';
$mod_strings['LBL_STATUS'] = 'Estado';
$mod_strings['LBL_SELECT_MODULE'] = 'Seleccionar módulo';
$mod_strings['LBL_ACTIVE'] = 'Activo';
$mod_strings['LBL_INACTIVE'] = 'Inactivo';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Mapeo de campo';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Añadir mapeo de campo';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Seleccionar campo relacionado';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Agregar bloque de mapeo de campo';
$mod_strings['LBL_SOURCE_MODULE'] = 'Módulo fuente';
$mod_strings['LBL_TARGET_MODULE'] = 'Módulo objetivo';
$mod_strings['LBL_FIELDS'] = 'Campos';
$mod_strings['LBL_RELATED_FIELD'] = 'Campo relacionado';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Calcular campo';
$mod_strings['LBL_FUNCTIONS'] = 'Funciones';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Agregar campo de cálculo';
$mod_strings['LBL_INPUT_FORMULA'] = 'Entrada (Formula)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Añadir Calcular bloque de campo';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'Añadir';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'Sustraer';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Multiplicar';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Dividir';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'Longitud de la cuerda';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'Concatenación';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Log (logaritmo)';
$mod_strings['LBL_LN'] = 'Ln (Log Natural)';
$mod_strings['LBL_ABSOLUTE'] = 'Absoluto';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Promedio';
$mod_strings['LBL_FUNCTION_POWER'] = 'Poder';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Diferencia de fecha';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'Porcentaje';
$mod_strings['LBL_FUNCTION_MOD'] = 'Mod (modulo)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'Mínimo';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'Negar';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Piso';
$mod_strings['LBL_FUNCTION_CEIL'] = 'Hacer techo';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Agregar horas';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Agregar día';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Agregar semana';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Agregar mes';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Agregar año';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Sub horas';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Días secundarios';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Sub semana';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Sub mes';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Sub año';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Diff Days';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Hora diferencial';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Diferencia de minuto';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Mes diferencial';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Diferencia de semana';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Año diferencial';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = "Utilice solo los campos 'Integer' O 'Decimal' en las operaciones numéricas";
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = "Utilice solo campos 'String' en las operaciones de String";
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = 'Utilice solo los campos "Fecha" O "Fecha y hora" en las operaciones de fecha';
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = 'Por favor, rellene todos los campos requeridos!!!';
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Por favor, seleccione el campo Tipo de coincidencia!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = 'Por favor ingrese el valor';
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' La configuración del módulo ya existe. Por favor, seleccione otro módulo.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Seleccione el campo relacionado antes de hacer clic en el botón "Agregar bloque de asignación de campo".';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Por favor, seleccione Campo Asignación de campo.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'por ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = ', Bloque de mapeo de campo ya agregado. Si desea agregar otra asignación de campo para ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' luego use el bloque existente.';

$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = ', Calcular bloque de campo ya agregado. Si desea agregar otro campo de cálculo para ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = '¡¡¡Por favor llena todos los espacios!!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Reemplace "string_field" con el nombre del campo que está disponible en los campos del módulo de destino para';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Reemplace "number_field" con el nombre del campo que está disponible en los campos del módulo de destino para';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Reemplace "date_field" con el nombre del campo que está disponible en los campos del módulo de destino para';
$mod_strings['LBL_BASE_ERROR'] = 'Reemplace el parámetro "base" con cualquier base de registro como 2, 10 para';
$mod_strings['LBL_DAYS_ERROR'] = 'Reemplace el parámetro "días" con cualquier número para';
$mod_strings['LBL_WEEK_ERROR'] = 'Reemplace el parámetro "semana" con cualquier número para';
$mod_strings['LBL_MONTH_ERROR'] = 'Reemplace el parámetro "mes" con cualquier número para';
$mod_strings['LBL_YEAR_ERROR'] = 'Reemplace el parámetro "año" con cualquier número para';
$mod_strings['LBL_HOURS_ERROR'] = 'Reemplace el parámetro "horas" con cualquier número para';
$mod_strings['LBL_FUNCTION'] = 'Función';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Seleccione la función matemática o lógica para realizar el cálculo en el campo.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Seleccione el campo en el que necesita realizar el cálculo'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Una vez que seleccione la función, se completará automáticamente en el cuadro de entrada (Fórmula) y si selecciona el campo en el que necesita realizar el cálculo, también se completará automáticamente. Necesitas copiar ese campo y poner en función como parámetro.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Seleccione Campo de módulo primario para completar automáticamente el cálculo en función de la selección de campos relacionados';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Actualizar licencia';
$mod_strings['LBL_ADD_NEW'] = '+ Agregar nuevo';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Actualmente no tienes registros guardados.';
$mod_strings['LBL_CREATE'] = 'CREAR';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' uno ahora.';
$mod_strings['LBL_DELETE'] = "Eliminar";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Por favor seleccione registros.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'estas seguro que quieres borrarlo';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'estas';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'esta';
$mod_strings['LBL_DELETE_MESSAGE_4'] = '¿fila?';
$mod_strings['LBL_EDIT'] = 'Editar';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "¡Estado activado con éxito!";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Estado desactivado con éxito !!";

$mod_strings['LBL_BULK_ACTION'] = 'ACCIÓN MASIVA';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' El campo ya está seleccionado para la asignación de campos. Seleccione otro campo.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' El campo ya está seleccionado para Calcular campo. Seleccione otro campo.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Agregue el parámetro "año" con cualquier número para';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Agregue el parámetro "mes" con cualquier número para';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Agregue el parámetro "semana" con cualquier número para';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Agregue el parámetro "días" con cualquier número para';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Agregue el parámetro "base" con cualquier número para';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Agregue el parámetro "horas" con cualquier número para';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'en la fórmula de entrada';