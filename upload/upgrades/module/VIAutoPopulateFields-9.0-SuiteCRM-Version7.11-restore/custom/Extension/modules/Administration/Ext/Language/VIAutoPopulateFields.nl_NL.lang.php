<?php
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Automatisch velden vullen';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Beheer automatisch invulvelden afhankelijk van gerelateerde velden';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'TERUG';
$mod_strings['LBL_NEXT_BUTTON'] = 'NEXT';
$mod_strings['LBL_CLEAR_BUTTON'] = 'DUIDELIJK';
$mod_strings['LBL_SAVE_BUTTON'] = 'OPSLAAN';
$mod_strings['LBL_CANCEL_BUTTON'] = 'ANNULEREN';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Kies een optie';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Bronmodule (gegevens kopiëren naar)';
$mod_strings['LBL_STATUS'] = 'staat';
$mod_strings['LBL_SELECT_MODULE'] = 'Selecteer module';
$mod_strings['LBL_ACTIVE'] = 'Actief';
$mod_strings['LBL_INACTIVE'] = 'Inactief';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Veldtoewijzing';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Veldtoewijzing toevoegen';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Selecteer Gerelateerd veld';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Veldtoewijzingsblok toevoegen';
$mod_strings['LBL_SOURCE_MODULE'] = 'Bronmodule';
$mod_strings['LBL_TARGET_MODULE'] = 'Doelmodule';
$mod_strings['LBL_FIELDS'] = 'Velden';
$mod_strings['LBL_RELATED_FIELD'] = 'Gerelateerd veld';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Veld berekenen';
$mod_strings['LBL_FUNCTIONS'] = 'functies';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Bereken veld toevoegen';
$mod_strings['LBL_INPUT_FORMULA'] = 'Ingang (Formule)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Bereken veldblok toevoegen';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'Toevoegen';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'Aftrekken';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Vermenigvuldigen';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Verdelen';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'Snaarlengte';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'Aaneenschakeling';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Log (Logaritme)';
$mod_strings['LBL_LN'] = 'Ln (natuurlijke log)';
$mod_strings['LBL_ABSOLUTE'] = 'absoluut';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Gemiddelde';
$mod_strings['LBL_FUNCTION_POWER'] = 'macht';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Dataverschil';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'Percentage';
$mod_strings['LBL_FUNCTION_MOD'] = 'Mod (Modulo)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'Minimum';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'ontkennen';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Verdieping';
$mod_strings['LBL_FUNCTION_CEIL'] = 'ceil';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Uren toevoegen';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Dag toevoegen';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Week toevoegen';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Maand toevoegen';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Jaar toevoegen';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Suburen';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Subdagen';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Subweek';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Submaand';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Subjaar';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Diff dagen';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Diff uur';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Diff Minuut';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Diff maand';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Weekversch';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Diff jaar';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = "Gebruik alleen de velden 'Geheel getal' OF 'Decimaal' in Numerieke bewerkingen";
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = "Gebruik alleen 'String'-velden in String-bewerkingen";
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = "Gebruik alleen de velden 'Datum' OF 'Datetime' in Datumbewerkingen";
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = 'Vul alstublieft alle verplichte velden in!!!';
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Selecteer alstublieft Matching Type Field!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = 'Voer alstublieft waarde in';
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' Moduleconfiguratie bestaat al. Selecteer een andere module.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Selecteer Verwant veld voordat u op de knop "Veldtoewijzing toevoegen" klikt.';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Selecteer Field Mapping Field.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'Voor ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = ', Veldtoewijzingsblok al toegevoegd. Als u nog een veldtoewijzing wilt toevoegen voor ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' gebruik dan het bestaande blok.';

$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = ', Bereken Veldblok al toegevoegd. Als u nog een rekenveld wilt toevoegen voor ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = 'Vul alle velden in !!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Vervang "string_field" door veldnaam die beschikbaar is in de doelmodulevelden voor';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Vervang "number_field" door veldnaam die beschikbaar is in de doelmodulevelden voor';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Vervang "date_field" door veldnaam die beschikbaar is in de doelmodulevelden voor';
$mod_strings['LBL_BASE_ERROR'] = 'Vervang de "basis" -parameter door een willekeurige logbasis zoals 2, 10 voor';
$mod_strings['LBL_DAYS_ERROR'] = 'Vervang de parameter "dagen" door een willekeurig nummer voor';
$mod_strings['LBL_WEEK_ERROR'] = 'Vervang de "week" -parameter door een willekeurig nummer voor';
$mod_strings['LBL_MONTH_ERROR'] = 'Vervang de parameter "maand" door een willekeurig nummer voor';
$mod_strings['LBL_YEAR_ERROR'] = 'Vervang de parameter "jaar" door een willekeurig nummer voor';
$mod_strings['LBL_HOURS_ERROR'] = 'Vervang de parameter "uren" door een willekeurig nummer voor';
$mod_strings['LBL_FUNCTION'] = 'Functie';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Selecteer Wiskundige of logische functie om een ​​berekening op veld uit te voeren.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Selecteer een veld waarop u de berekening moet uitvoeren'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Zodra u de functie selecteert, wordt deze automatisch ingevuld in het vak Invoer (formule) en als u een veld selecteert waarop u een berekening moet uitvoeren, wordt het ook automatisch ingevuld. U moet dat veld kopiëren en als parameter gebruiken.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Selecteer Primair moduleveld om automatisch de berekening op te vullen op basis van gerelateerde veldselectie';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Licentie updaten';
$mod_strings['LBL_ADD_NEW'] = '+ Nieuw toevoegen';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Je hebt momenteel geen records opgeslagen.';
$mod_strings['LBL_CREATE'] = 'MAKEN';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' een nu.';
$mod_strings['LBL_DELETE'] = "Verwijderen";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Selecteer records.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Weet je zeker dat je wilt verwijderen';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'deze';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'dit';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'rij?';
$mod_strings['LBL_EDIT'] = 'Bewerk';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "Status succesvol geactiveerd !!";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Status succesvol gedeactiveerd !!";

$mod_strings['LBL_BULK_ACTION'] = 'BULK ACTIE';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' veld is al geselecteerd voor Veldtoewijzing. Selecteer een ander veld.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' veld is al geselecteerd voor Veld berekenen. Selecteer een ander veld.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Voeg een "jaar" -parameter toe met een willekeurig nummer voor';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Voeg een "maand" parameter toe met een willekeurig nummer voor';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Voeg een "week" -parameter toe met een willekeurig nummer voor';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Voeg de parameter "dagen" toe met een willekeurig nummer voor';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Voeg een "basis" -parameter toe met een willekeurig nummer voor';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Voeg de parameter "uren" toe met een willekeurig nummer voor';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'in invoerformule';