<?php
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Felder automatisch ausfüllen';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Verwalten Sie Felder zum automatischen Auffüllen abhängig von den zugehörigen Feldern';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'ZURÜCK';
$mod_strings['LBL_NEXT_BUTTON'] = 'NÄCHSTER';
$mod_strings['LBL_CLEAR_BUTTON'] = 'KLAR';
$mod_strings['LBL_SAVE_BUTTON'] = 'SPAREN';
$mod_strings['LBL_CANCEL_BUTTON'] = 'STORNIEREN';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Wähle eine Option';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Quellmodul (Daten kopieren nach)';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_SELECT_MODULE'] = 'Modul auswählen';
$mod_strings['LBL_ACTIVE'] = 'Aktiv';
$mod_strings['LBL_INACTIVE'] = 'Inaktiv';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Feldzuordnung';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Feldzuordnung hinzufügen';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Wählen Sie Verwandtes Feld';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Feldzuordnungsblock hinzufügen';
$mod_strings['LBL_SOURCE_MODULE'] = 'Quellmodul';
$mod_strings['LBL_TARGET_MODULE'] = 'Zielmodul';
$mod_strings['LBL_FIELDS'] = 'Felder';
$mod_strings['LBL_RELATED_FIELD'] = 'Verwandtes Gebiet';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Feld berechnen';
$mod_strings['LBL_FUNCTIONS'] = 'Funktionen';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Berechnungsfeld hinzufügen';
$mod_strings['LBL_INPUT_FORMULA'] = 'Eingabe (Formel)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Feldblock berechnen hinzufügen';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'Hinzufügen';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'Subtrahieren';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Multiplizieren';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Teilen';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'String-Länge';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'Verkettung';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Log (Logarithmus)';
$mod_strings['LBL_LN'] = 'Ln (natürliches Protokoll)';
$mod_strings['LBL_ABSOLUTE'] = 'Absolut';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Durchschnittlich';
$mod_strings['LBL_FUNCTION_POWER'] = 'Leistung';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Datumsunterschied';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'Prozentsatz';
$mod_strings['LBL_FUNCTION_MOD'] = 'Mod (Modulo)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'Minimum';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'Negieren';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Fußboden';
$mod_strings['LBL_FUNCTION_CEIL'] = 'Ceil';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Stunden hinzufügen';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Tag hinzufügen';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Woche hinzufügen';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Monat hinzufügen';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Jahr hinzufügen';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Unterstunden';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Untertage';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Unterwoche';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Untermonat';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Unterjahr';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Diff Tage';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Diff Stunde';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Diff Minute';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Diff Monat';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Woche Diff';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Diff Jahr';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = "Verwenden Sie in numerischen Operationen nur Ganzzahl- ODER Dezimalfelder";
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = "Verwenden Sie nur 'String'-Felder in String-Operationen";
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = "Verwenden Sie in Datumsoperationen nur die Felder 'Datum' ODER 'Datum / Uhrzeit'";
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = 'Bitte füllen Sie alle erforderlichen Felder aus!!!';
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Bitte passendes Feld auswählen!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = 'Bitte geben Sie einen Wert ein';
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' Modulkonfiguration existiert bereits. Bitte wählen Sie ein anderes Modul.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Bitte wählen Sie das zugehörige Feld aus, bevor Sie auf die Schaltfläche "Feldzuordnungsblock hinzufügen" klicken.';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Bitte wählen Sie Feldzuordnungsfeld.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'Zum ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = ', Feldzuordnungsblock bereits hinzugefügt. Wenn Sie eine weitere Feldzuordnung für hinzufügen möchten ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' Verwenden Sie dann den vorhandenen Block.';

$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = ', Bereits hinzugefügten Feldblock berechnen. Wenn Sie ein weiteres Berechnungsfeld für hinzufügen möchten ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = 'Bitte alle Felder ausfüllen!!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Bitte ersetzen Sie "string_field" durch den Feldnamen, der in den Feldern des Zielmoduls für verfügbar ist';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Bitte ersetzen Sie "number_field" durch den Feldnamen, der in den Feldern des Zielmoduls für verfügbar ist';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Bitte ersetzen Sie "date_field" durch den Feldnamen, der in den Feldern des Zielmoduls für verfügbar ist';
$mod_strings['LBL_BASE_ERROR'] = 'Bitte ersetzen Sie den Parameter "base" durch eine beliebige Basis des Protokolls wie 2, 10 für';
$mod_strings['LBL_DAYS_ERROR'] = 'Bitte ersetzen Sie den Parameter "Tage" durch eine beliebige Zahl für';
$mod_strings['LBL_WEEK_ERROR'] = 'Bitte ersetzen Sie den Parameter "Woche" durch eine beliebige Zahl für';
$mod_strings['LBL_MONTH_ERROR'] = 'Bitte ersetzen Sie den Parameter "Monat" durch eine beliebige Zahl für';
$mod_strings['LBL_YEAR_ERROR'] = 'Bitte ersetzen Sie den Parameter "Jahr" durch eine beliebige Zahl für';
$mod_strings['LBL_HOURS_ERROR'] = 'Bitte ersetzen Sie den Parameter "Stunden" durch eine beliebige Zahl für';
$mod_strings['LBL_FUNCTION'] = 'Funktion';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Wählen Sie die mathematische oder logische Funktion, um die Berechnung für das Feld durchzuführen.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Wählen Sie das Feld aus, für das Sie eine Berechnung durchführen möchten'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Sobald Sie die Funktion ausgewählt haben, wird sie automatisch in das Eingabefeld (Formel) eingefügt. Wenn Sie das Feld auswählen, für das Sie eine Berechnung durchführen möchten, wird sie automatisch ausgefüllt. Sie müssen dieses Feld kopieren und die Funktion als Parameter eingeben.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Wählen Sie Primäres Modulfeld, um die Berechnung basierend auf der Auswahl des zugehörigen Felds automatisch aufzufüllen';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Lizenz aktualisieren';
$mod_strings['LBL_ADD_NEW'] = '+ Neu hinzufügen';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Sie haben derzeit keine Datensätze gespeichert.';
$mod_strings['LBL_CREATE'] = 'ERSTELLEN';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' Eine jetzt.';
$mod_strings['LBL_DELETE'] = "Löschen";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Bitte wählen Sie Datensätze aus.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Sind Sie sicher, dass Sie löschen möchten';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'diese';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'Dies';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'Reihe?';
$mod_strings['LBL_EDIT'] = 'Bearbeiten';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "Status erfolgreich aktiviert !!";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Status erfolgreich deaktiviert !!";

$mod_strings['LBL_BULK_ACTION'] = 'BULK ACTION';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' Feld ist bereits für die Feldzuordnung ausgewählt. Bitte wählen Sie ein anderes Feld.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' Feld ist bereits für Feld berechnen ausgewählt. Bitte wählen Sie ein anderes Feld.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Bitte fügen Sie den Parameter "Jahr" mit einer beliebigen Zahl für hinzu';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Bitte fügen Sie den Parameter "Monat" mit einer beliebigen Zahl für hinzu';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Bitte fügen Sie den Parameter "Woche" mit einer beliebigen Nummer für hinzu';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Bitte fügen Sie den Parameter "Tage" mit einer beliebigen Zahl für hinzu';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Bitte fügen Sie "Basis" -Parameter mit einer beliebigen Nummer für hinzu';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Bitte fügen Sie den Parameter "Stunden" mit einer beliebigen Zahl für hinzu';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'in der Eingabeformel';