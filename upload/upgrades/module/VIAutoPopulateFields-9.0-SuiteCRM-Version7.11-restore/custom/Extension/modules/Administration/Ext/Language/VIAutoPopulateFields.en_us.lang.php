<?php
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Auto Populate Fields';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Manage Auto Fill Up Fields Depending on Relate Fields';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'BACK';
$mod_strings['LBL_NEXT_BUTTON'] = 'NEXT';
$mod_strings['LBL_CLEAR_BUTTON'] = 'CLEAR';
$mod_strings['LBL_SAVE_BUTTON'] = 'SAVE';
$mod_strings['LBL_CANCEL_BUTTON'] = 'CANCEL';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Select an Option';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Source Module(Copy Data To)';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_SELECT_MODULE'] = 'Select Module';
$mod_strings['LBL_ACTIVE'] = 'Active';
$mod_strings['LBL_INACTIVE'] = 'Inactive';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Field Mapping';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Add Field Mapping';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Select Related Field';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Add Field Mapping Block';
$mod_strings['LBL_SOURCE_MODULE'] = 'Source Module';
$mod_strings['LBL_TARGET_MODULE'] = 'Target Module';
$mod_strings['LBL_FIELDS'] = 'Fields';
$mod_strings['LBL_RELATED_FIELD'] = 'Related Field';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Calculate Field';
$mod_strings['LBL_FUNCTIONS'] = 'Functions';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Add Calculate Field';
$mod_strings['LBL_INPUT_FORMULA'] = 'Input(Formula)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Add Calculate Field Block';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'Add';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'Subtract';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Multiply';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Divide';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'String Length';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'Concatenation';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Log(Logarithm)';
$mod_strings['LBL_LN'] = 'Ln(Natural Log)';
$mod_strings['LBL_ABSOLUTE'] = 'Absolute';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Average';
$mod_strings['LBL_FUNCTION_POWER'] = 'Power';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Date Difference';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'Percentage';
$mod_strings['LBL_FUNCTION_MOD'] = 'Mod(Modulo)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'Minimum';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'Negate';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Floor';
$mod_strings['LBL_FUNCTION_CEIL'] = 'Ceil';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Add Hours';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Add Day';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Add Week';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Add Month';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Add Year';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Sub Hours';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Sub Days';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Sub Week';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Sub Month';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Sub Year';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Diff Days';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Diff Hour';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Diff Minute';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Diff Month';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Week Diff';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Diff Year';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = "Use only 'Integer' OR 'Decimal' fields in Numeric operations";
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = "Use only 'String' fields in String operations";
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = "Use only 'Date' OR 'Datetime' fields in Date operations";
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = 'Please Fill All Required Field!!!';
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Please Select Matching Type Field!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = 'Please Enter Value';
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' Module Configuration already Exists. Please Select another Module.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Please Select Related Field before Clicking on the "Add Field Mapping Block" button.';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Please Select Field Mapping Field.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'For ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = ', Field Mapping block already added. If you want to add another field mapping for ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' then use existing block.';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Add Calculate Field Block';
$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = ', Calculate Field block already added. If you want to add another calculate field for ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = 'Please fill All Fields!!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Please Replace "string_field" with Field Name which is available in Target Module Fields for';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Please Replace "number_field" with Field Name which is available in Target Module Fields for';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Please Replace "date_field" with Field Name which is available in Target Module Fields for';
$mod_strings['LBL_BASE_ERROR'] = 'Please Replace "base" Parameter with any base of log like 2, 10 for';
$mod_strings['LBL_DAYS_ERROR'] = 'Please Replace "days" Parameter with any Number for';
$mod_strings['LBL_WEEK_ERROR'] = 'Please Replace "week" Parameter with any Number for';
$mod_strings['LBL_MONTH_ERROR'] = 'Please Replace "month" Parameter with any Number for';
$mod_strings['LBL_YEAR_ERROR'] = 'Please Replace "year" Parameter with any Number for';
$mod_strings['LBL_HOURS_ERROR'] = 'Please Replace "hours" Parameter with any Number for';
$mod_strings['LBL_FUNCTION'] = 'Function';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Select Mathematical or Logical function to perform calculation on field.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Select field on which you need to perform calculation'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Once you select function, it will auto populate in Input(Formula) box and if you select field on which you need to perform calculation then it is also auto populate. You need to copy that field and put in function as parameter.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Select Primary Module Field to auto fill up calculation based on related field selection';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Update License';
$mod_strings['LBL_ADD_NEW'] = '+ Add New';
$mod_strings['LBL_CREATE_MESSAGE'] = 'You currently have no records saved.';
$mod_strings['LBL_CREATE'] = 'CREATE';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' one now.';
$mod_strings['LBL_DELETE'] = "Delete";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Please select records.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Are you sure you want to delete';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'these';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'this';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'row?';
$mod_strings['LBL_EDIT'] = 'Edit';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "Status Activated Successfully!!";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Status Deactivated Successfully!!";

$mod_strings['LBL_BULK_ACTION'] = 'BULK ACTION';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' field is already selected for Field Mapping. Please select another Field.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' field is already selected for Calculate Field. Please select another Field.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Please add "year" Parameter with any Number for';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Please add "month" Parameter with any Number for';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Please add "week" Parameter with any Number for';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Please add "days" Parameter with any Number for';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Please add "base" Parameter with any Number for';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Please add "hours" Parameter with any Number for';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'in Input Formula';