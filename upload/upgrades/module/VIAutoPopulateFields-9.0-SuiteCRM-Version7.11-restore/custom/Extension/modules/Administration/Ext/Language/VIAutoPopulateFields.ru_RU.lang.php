<?php
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Автозаполнение полей';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Управление автоматическим заполнением полей в зависимости от связанных полей';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'НАЗАД';
$mod_strings['LBL_NEXT_BUTTON'] = 'СЛЕДУЮЩИЙ';
$mod_strings['LBL_CLEAR_BUTTON'] = 'ЧИСТО';
$mod_strings['LBL_SAVE_BUTTON'] = 'СПАСТИ';
$mod_strings['LBL_CANCEL_BUTTON'] = 'ОТМЕНИТЬ';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Selecione uma opção';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Исходный модуль (Копирование данных в)';
$mod_strings['LBL_STATUS'] = 'Положение дел';
$mod_strings['LBL_SELECT_MODULE'] = 'Выберите модуль';
$mod_strings['LBL_ACTIVE'] = 'активный';
$mod_strings['LBL_INACTIVE'] = 'Неактивный';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Полевое картографирование';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Добавить сопоставление полей';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Выберите связанное поле';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Добавить блок отображения поля';
$mod_strings['LBL_SOURCE_MODULE'] = 'Исходный модуль';
$mod_strings['LBL_TARGET_MODULE'] = 'Целевой модуль';
$mod_strings['LBL_FIELDS'] = 'поля';
$mod_strings['LBL_RELATED_FIELD'] = 'Связанное поле';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Рассчитать поле';
$mod_strings['LBL_FUNCTIONS'] = 'функции';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Добавить поле вычисления';
$mod_strings['LBL_INPUT_FORMULA'] = 'Входные данные (формула)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Добавить блок вычисления поля';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'добавлять';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'вычитать';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Умножение';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Делить';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'Длина строки';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'конкатенация';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Log (логарифм)';
$mod_strings['LBL_LN'] = 'Ln (Натуральный Лог)';
$mod_strings['LBL_ABSOLUTE'] = 'абсолют';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Средний';
$mod_strings['LBL_FUNCTION_POWER'] = 'Мощность';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Разница в дате';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'процент';
$mod_strings['LBL_FUNCTION_MOD'] = 'Мод (Модульный)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'минимальный';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'сводить на нет';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Этаж';
$mod_strings['LBL_FUNCTION_CEIL'] = 'перекрывать';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Добавить часы';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Добавить день';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Добавить неделю';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Добавить месяц';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Добавить год';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Дополнительные часы';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Дополнительные дни';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Суб неделя';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Суб месяц';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Младший год';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Разные дни';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Diff Hour';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Минуты различий';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Месяц различий';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Недельная разница';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Год различия';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = "Используйте только целочисленные или десятичные поля в числовых операциях";
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = "Используйте только строковые поля в строковых операциях";
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = "Используйте только поля «Дата» ИЛИ «Дата / время» в операциях «Дата»";
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = 'Пожалуйста, заполните все обязательные поля!!!';
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Пожалуйста, выберите поле соответствующего типа!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = 'Пожалуйста, введите значение';
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' Конфигурация модуля уже существует. Пожалуйста, выберите другой модуль.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Пожалуйста, выберите Связанное поле, прежде чем нажимать кнопку «Добавить блок отображения поля».';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Пожалуйста, выберите Field Mapping Field.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'Для ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = 'Блок отображения полей уже добавлен. Если вы хотите добавить другое отображение поля для ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' затем используйте существующий блок.';

$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = 'Блок вычислений поля уже добавлен. Если вы хотите добавить другое вычисляемое поле для ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = 'Пожалуйста заполните все поля!!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Замените "string_field" на имя поля, которое доступно в полях целевого модуля для';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Пожалуйста, замените "число_поле" на имя поля, которое доступно в полях целевого модуля для';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Пожалуйста, замените поле "date_field" именем поля, которое доступно в полях целевого модуля для';
$mod_strings['LBL_BASE_ERROR'] = 'Замените «базовый» параметр на любое основание журнала, например 2, 10 для';
$mod_strings['LBL_DAYS_ERROR'] = 'Пожалуйста, замените параметр "days" любым числом для';
$mod_strings['LBL_WEEK_ERROR'] = 'Пожалуйста, замените параметр "недели" любым числом для';
$mod_strings['LBL_MONTH_ERROR'] = 'Пожалуйста, замените параметр "месяц" любым числом для';
$mod_strings['LBL_YEAR_ERROR'] = 'Пожалуйста, замените параметр "год" любым числом для';
$mod_strings['LBL_HOURS_ERROR'] = 'Пожалуйста, замените параметр "часы" на любое число для';
$mod_strings['LBL_FUNCTION'] = 'функция';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Выберите математическую или логическую функцию, чтобы выполнить расчет на поле.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Выберите поле, на котором вам нужно выполнить расчет'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Как только вы выберете функцию, она будет автоматически заполнена в поле ввода (формула), и если вы выберете поле, в котором вам нужно выполнить расчет, она также будет автоматически заполнена. Вам нужно скопировать это поле и ввести функцию в качестве параметра.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Выберите поле основного модуля, чтобы автоматически заполнить расчет на основе выбора соответствующего поля';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Обновить лицензию';
$mod_strings['LBL_ADD_NEW'] = '+ Добавить новый';
$mod_strings['LBL_CREATE_MESSAGE'] = 'В настоящее время у вас нет сохраненных записей.';
$mod_strings['LBL_CREATE'] = 'СОЗДАЙТЕ';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' один сейчас.';
$mod_strings['LBL_DELETE'] = "Удалить";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Пожалуйста, выберите записи.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Вы уверены, что хотите удалить';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'эти';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'этот';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'строка?';
$mod_strings['LBL_EDIT'] = 'редактировать';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "Статус активирован успешно !!
";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Статус Деактивирован успешно !!";

$mod_strings['LBL_BULK_ACTION'] = 'ОБЪЕМ ДЕЙСТВИЙ';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' поле уже выбрано для сопоставления полей. Пожалуйста, выберите другое поле.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' поле уже выбрано для вычисления поля. Пожалуйста, выберите другое поле.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Пожалуйста, добавьте параметр "год" с любым числом для';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Пожалуйста, добавьте параметр "месяц" с любым числом для';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Пожалуйста, добавьте параметр "неделя" с любым числом для';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Пожалуйста, добавьте параметр "days" с любым числом для';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Пожалуйста, добавьте "базовый" параметр с любым числом для';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Пожалуйста, добавьте параметр "часы" с любым числом для';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'в формуле ввода';