<?php
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Champs à remplissage automatique';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Gérer les champs de remplissage automatique en fonction des champs liés';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'RETOUR';
$mod_strings['LBL_NEXT_BUTTON'] = 'SUIVANTE';
$mod_strings['LBL_CLEAR_BUTTON'] = 'CLAIRE';
$mod_strings['LBL_SAVE_BUTTON'] = 'ENREGISTRER';
$mod_strings['LBL_CANCEL_BUTTON'] = 'ANNULER';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Sélectionnez une option';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Module source (copier les données vers)';
$mod_strings['LBL_STATUS'] = 'Statut';
$mod_strings['LBL_SELECT_MODULE'] = 'Sélectionnez le module';
$mod_strings['LBL_ACTIVE'] = 'actif';
$mod_strings['LBL_INACTIVE'] = 'Inactif';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Cartographie de terrain';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Ajouter un mappage de champs';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Sélectionnez un champ associé';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Ajouter un bloc de mappage de champ';
$mod_strings['LBL_SOURCE_MODULE'] = 'Module source';
$mod_strings['LBL_TARGET_MODULE'] = 'Module cible';
$mod_strings['LBL_FIELDS'] = 'Des champs';
$mod_strings['LBL_RELATED_FIELD'] = 'Domaine connexe';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Calculer le champ';
$mod_strings['LBL_FUNCTIONS'] = 'Les fonctions';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Ajouter un champ de calcul';
$mod_strings['LBL_INPUT_FORMULA'] = 'Entrée (formule)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Ajouter un bloc de champ Calculer';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'Ajouter';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'Soustraire';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Multiplier';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Diviser';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'Longueur de chaine';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'Enchaînement';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Log (logarithme)';
$mod_strings['LBL_LN'] = 'Ln (bûche naturelle)';
$mod_strings['LBL_ABSOLUTE'] = 'Absolu';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Moyenne';
$mod_strings['LBL_FUNCTION_POWER'] = 'Puissance';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Différence de date';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'Pourcentage';
$mod_strings['LBL_FUNCTION_MOD'] = 'Mod (Modulo)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'Le minimum';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'Nier';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Sol';
$mod_strings['LBL_FUNCTION_CEIL'] = 'Ceil';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Ajouter des heures';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Ajouter un jour';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Ajouter une semaine';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Ajouter un mois';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Ajouter une année';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Sous-heures';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Sous-jours';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Sous-semaine';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Sous-mois';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Sous-année';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Jours de Diff';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Heure de Diff';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Diff Minute';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Mois de Diff';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Diff semaine';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Année Diff';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = "Utilisez uniquement les champs 'Entier' OU 'Décimal' dans les opérations numériques";
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = "Utiliser uniquement les champs 'String' dans les opérations String";
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = 'Utilisez uniquement les champs "Date" ou "Date et heure" dans les opérations sur les dates';
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = "S'il vous plaît remplir tous les champs obligatoires!!!";
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Veuillez sélectionner le champ de type correspondant!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = "S'il vous plaît entrer la valeur";
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' La configuration du module existe déjà. Veuillez sélectionner un autre module.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Veuillez sélectionner le champ associé avant de cliquer sur le bouton "Ajouter un bloc de mappage de champ".';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Veuillez sélectionner le champ de mappage de champ.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'Pour ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = ', Bloc de mappage de champ déjà ajouté. Si vous souhaitez ajouter un autre mappage de champ pour ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' puis utilisez le bloc existant.';

$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = ', Bloc de calcul de champ déjà ajouté. Si vous souhaitez ajouter un autre champ de calcul pour ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = 'Merci de compléter tous les champs!!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Remplacez "string_field" par le nom du champ disponible dans les champs du module cible pour';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Remplacez "number_field" par le nom du champ disponible dans les champs du module cible pour';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Remplacez "date_field" par le nom du champ disponible dans les champs du module cible pour';
$mod_strings['LBL_BASE_ERROR'] = 'Veuillez remplacer le paramètre "base" par nimporte quelle base de journal comme 2, 10 pour';
$mod_strings['LBL_DAYS_ERROR'] = 'Veuillez remplacer le paramètre "jours" par nimporte quel nombre pour';
$mod_strings['LBL_WEEK_ERROR'] = 'Veuillez remplacer le paramètre "semaine" par nimporte quel nombre pour';
$mod_strings['LBL_MONTH_ERROR'] = 'Veuillez remplacer le paramètre "mois" par nimporte quel nombre pour';
$mod_strings['LBL_YEAR_ERROR'] = 'Veuillez remplacer le paramètre "année" par nimporte quel nombre pour';
$mod_strings['LBL_HOURS_ERROR'] = 'Veuillez remplacer le paramètre "heures" par nimporte quel nombre pour';
$mod_strings['LBL_FUNCTION'] = 'Fonction';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Sélectionnez la fonction mathématique ou logique pour effectuer un calcul sur le terrain.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Sélectionnez le champ sur lequel vous devez effectuer un calcul'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Une fois que vous avez sélectionné une fonction, il est automatiquement renseigné dans la zone Entrée (Formule). Si vous sélectionnez un champ sur lequel vous devez effectuer un calcul, il est également rempli automatiquement. Vous devez copier ce champ et le mettre en fonction en tant que paramètre.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Sélectionnez Champ de module principal pour remplir automatiquement le calcul en fonction de la sélection du champ associé.';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Mettre à jour la licence';
$mod_strings['LBL_ADD_NEW'] = '+ Ajouter nouveau';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Vous navez actuellement aucun enregistrement enregistré.';
$mod_strings['LBL_CREATE'] = 'CRÉER';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' un maintenant.';
$mod_strings['LBL_DELETE'] = "Supprimer";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Veuillez sélectionner des enregistrements.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Etes-vous sûr que vous voulez supprimer';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'celles-ci';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'ce';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'rangée?';
$mod_strings['LBL_EDIT'] = 'Éditer';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "Statut activé avec succès !!";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Statut désactivé avec succès !!";

$mod_strings['LBL_BULK_ACTION'] = 'ACTION EN GROS';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' field est déjà sélectionné pour Field Mapping. Veuillez sélectionner un autre champ.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' champ est déjà sélectionné pour Calculer le champ. Veuillez sélectionner un autre champ.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Veuillez ajouter le paramètre "année" avec nimporte quel nombre pour';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Veuillez ajouter le paramètre "mois" avec nimporte quel nombre pour';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Veuillez ajouter le paramètre "semaine" avec nimporte quel nombre pour';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Veuillez ajouter le paramètre "jours" avec nimporte quel nombre pour';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Veuillez ajouter le paramètre "de base" avec nimporte quel nombre pour';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Veuillez ajouter le paramètre "heures" avec nimporte quel nombre pour';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'dans la formule dentrée';