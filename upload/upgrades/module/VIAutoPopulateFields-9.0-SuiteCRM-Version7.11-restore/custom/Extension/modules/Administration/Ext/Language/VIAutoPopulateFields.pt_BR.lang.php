<?php
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Auto preencher campos';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Gerenciar campos de preenchimento automático dependendo dos campos relacionados';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'DE VOLTA';
$mod_strings['LBL_NEXT_BUTTON'] = 'PRÓXIMA';
$mod_strings['LBL_CLEAR_BUTTON'] = 'CLARO';
$mod_strings['LBL_SAVE_BUTTON'] = 'SALVE ';
$mod_strings['LBL_CANCEL_BUTTON'] = 'CANCELAR';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Selecione uma opção';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Módulo de origem (copiar dados para)';
$mod_strings['LBL_STATUS'] = 'Status';
$mod_strings['LBL_SELECT_MODULE'] = 'Selecione o módulo';
$mod_strings['LBL_ACTIVE'] = 'Ativo';
$mod_strings['LBL_INACTIVE'] = 'Inativo';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Mapeamento de Campo';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Adicionar mapeamento de campo';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Selecionar campo relacionado';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Adicionar bloco de mapeamento de campo';
$mod_strings['LBL_SOURCE_MODULE'] = 'Módulo de origem';
$mod_strings['LBL_TARGET_MODULE'] = 'Módulo de destino';
$mod_strings['LBL_FIELDS'] = 'Campos';
$mod_strings['LBL_RELATED_FIELD'] = 'Campo relacionado';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Calcular Campo';
$mod_strings['LBL_FUNCTIONS'] = 'Funções';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Adicionar campo de cálculo';
$mod_strings['LBL_INPUT_FORMULA'] = 'Entrada (Fórmula)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Adicionar bloco de campo de cálculo';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'Adicionar';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'Subtrair';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Multiplicar';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Dividir';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'Comprimento da corda';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'Concatenação';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Log (logaritmo)';
$mod_strings['LBL_LN'] = 'Ln (log natural)';
$mod_strings['LBL_ABSOLUTE'] = 'Absoluto';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Média';
$mod_strings['LBL_FUNCTION_POWER'] = 'Poder';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Diferença de data';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'Percentagem';
$mod_strings['LBL_FUNCTION_MOD'] = 'Mod (Modulo)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'Mínimo';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'Negar';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Chão';
$mod_strings['LBL_FUNCTION_CEIL'] = 'Ceil';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Adicionar Horário';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Adicionar dia';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Adicionar semana';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Adicionar mês';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Adicione ano';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Sub horas';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Sub dias';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Sub semana';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Sub mês';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Sub ano';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Diff Dias';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Diff Hour';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Diff Minute';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Diff Mês';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Semana Diff';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Diff Year';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = "Use somente campos 'Integer' OR 'Decimal' em operações numéricas";
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = "Use apenas campos 'String' em operações String";
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = "Use somente os campos 'Data' OU 'Data e Hora' em operações de Data";
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = 'Por favor, preencha todos os campos obrigatórios!!!';
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Por favor, selecione o campo Tipo de Correspondência!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = 'Por favor insira valor';
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' A configuração do módulo já existe. Por favor, selecione outro módulo.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Selecione o campo relacionado antes de clicar no botão "Adicionar bloco de mapeamento de campo".';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Selecione Campo de mapeamento de campo.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'Para ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = ', Bloco Mapeamento de Campo já adicionado. Se você deseja adicionar outro mapeamento de campo para ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' então use o bloco existente.';

$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = ', Bloco Calcular campo já adicionado. Se você deseja adicionar outro campo de cálculo para ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = 'Por favor preencha todos os campos!!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Substitua "string_field" por Nome do campo, disponível em Campos do módulo de destino para';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Substitua "number_field" pelo nome do campo, disponível nos campos do módulo de destino para';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Substitua "date_field" pelo nome do campo, disponível nos campos do módulo de destino para';
$mod_strings['LBL_BASE_ERROR'] = 'Substitua o parâmetro "base" por qualquer base de log como 2, 10 para';
$mod_strings['LBL_DAYS_ERROR'] = 'Substitua o parâmetro "days" por qualquer número para';
$mod_strings['LBL_WEEK_ERROR'] = 'Substitua o parâmetro "week" por qualquer número para';
$mod_strings['LBL_MONTH_ERROR'] = 'Substitua o parâmetro "month" por qualquer número para';
$mod_strings['LBL_YEAR_ERROR'] = 'Substitua o parâmetro "ano" por qualquer número para';
$mod_strings['LBL_HOURS_ERROR'] = 'Substitua o parâmetro "horas" por qualquer número para';
$mod_strings['LBL_FUNCTION'] = 'Função';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Selecione a função matemática ou lógica para realizar o cálculo no campo.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Selecione o campo no qual você precisa realizar o cálculo'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Depois de selecionar a função, ela será preenchida automaticamente na caixa Entrada (Fórmula) e, se você selecionar o campo no qual precisará executar o cálculo, ela também será preenchida automaticamente. Você precisa copiar esse campo e colocar em função como parâmetro.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Selecione o Campo do Módulo Primário para o cálculo do preenchimento automático com base na seleção de campo relacionada';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Atualizar licença';
$mod_strings['LBL_ADD_NEW'] = '+ Adicionar novo';
$mod_strings['LBL_CREATE_MESSAGE'] = 'No momento, você não possui registros salvos.';
$mod_strings['LBL_CREATE'] = 'CRIO';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' um agora.';
$mod_strings['LBL_DELETE'] = "Excluir";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Por favor, selecione registros.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Tem certeza de que deseja excluir';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'estes';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'isto';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'linha?';
$mod_strings['LBL_EDIT'] = 'Editar';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "Status ativado com sucesso !!";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Status desativado com sucesso !!";

$mod_strings['LBL_BULK_ACTION'] = 'AÇÃO EM GRANEL';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' campo já está selecionado para mapeamento de campo. Selecione outro campo.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' campo já está selecionado para Calcular campo. Selecione outro campo.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Adicione o parâmetro "ano" com qualquer número para';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Adicione o parâmetro "mês" com qualquer número para';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Adicione o parâmetro "semana" com qualquer número para';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Adicione o parâmetro "dias" com qualquer número para';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Adicione o parâmetro "base" com qualquer número para';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Adicione o parâmetro "horas" com qualquer número para';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'na fórmula de entrada';