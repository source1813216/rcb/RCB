<?php
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Campi popolati automaticamente';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Gestisci campi di riempimento automatico a seconda dei campi correlati';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'INDIETRO';
$mod_strings['LBL_NEXT_BUTTON'] = 'IL PROSSIMO';
$mod_strings['LBL_CLEAR_BUTTON'] = 'CHIARA';
$mod_strings['LBL_SAVE_BUTTON'] = 'SALVARE';
$mod_strings['LBL_CANCEL_BUTTON'] = 'ANNULLA';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Seleziona unopzione';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Modulo sorgente (Copia dati in)';
$mod_strings['LBL_STATUS'] = 'Stato';
$mod_strings['LBL_SELECT_MODULE'] = 'Seleziona il modulo';
$mod_strings['LBL_ACTIVE'] = 'Attivo';
$mod_strings['LBL_INACTIVE'] = 'Inattivo';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Mappatura del campo';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Aggiungi mappatura del campo';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Seleziona il campo correlato';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Aggiungi blocco mappatura campi';
$mod_strings['LBL_SOURCE_MODULE'] = 'Modulo sorgente';
$mod_strings['LBL_TARGET_MODULE'] = 'Modulo target';
$mod_strings['LBL_FIELDS'] = 'campi';
$mod_strings['LBL_RELATED_FIELD'] = 'Campo correlato';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Calcola il campo';
$mod_strings['LBL_FUNCTIONS'] = 'funzioni';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Aggiungi campo di calcolo';
$mod_strings['LBL_INPUT_FORMULA'] = 'Ingresso (Formula)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Aggiungi calcola blocco campo';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'Inserisci';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'Sottrarre';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Moltiplicare';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Dividere';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'Lunghezza della stringa';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'Concatenazione';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Log (Logaritmo)';
$mod_strings['LBL_LN'] = 'Ln (registro naturale)';
$mod_strings['LBL_ABSOLUTE'] = 'Assoluto';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Media';
$mod_strings['LBL_FUNCTION_POWER'] = 'Energia';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Differenza di data';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'Percentuale';
$mod_strings['LBL_FUNCTION_MOD'] = 'Mod (Modulo)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'Minimo';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'Negare';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Pavimento';
$mod_strings['LBL_FUNCTION_CEIL'] = 'Ceil';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Aggiungi ore';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Aggiungi giorno';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Aggiungi settimana';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Aggiungi mese';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Aggiungi anno';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Sub Hours';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Sub Days';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Settimana secondaria';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Mese secondario';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Anno secondario';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Giorni diff';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Ora diff';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Minuto diff';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Mese diff';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Settimana Diff';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Anno Diff';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = 'Utilizzare solo campi "interi" o "decimali" in operazioni numeriche';
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = 'Utilizzare solo i campi "String" nelle operazioni String';
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = 'Utilizzare solo i campi "Data" o "Data / ora" nelle operazioni Date';
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = 'Si prega di compilare tutto il campo richiesto!!!';
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Si prega di selezionare il tipo di campo corrispondente!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = 'Si prega di inserire valore';
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' La configurazione del modulo esiste già. Seleziona un altro modulo.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Seleziona il campo correlato prima di fare clic sul pulsante "Aggiungi blocco mappatura campo".';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Seleziona il campo di mappatura dei campi.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'Per ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = ', Blocco di mappatura dei campi già aggiunto. Se si desidera aggiungere un altro mapping dei campi per ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' quindi usa il blocco esistente.';

$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = ', Calcola Blocco campo già aggiunto. Se si desidera aggiungere un altro campo di calcolo per ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = 'Per favore compila tutti i campi!!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Sostituisci "string_field" con Nome campo che è disponibile nei campi del modulo di destinazione per';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Sostituisci "numero_campo" con Nome campo che è disponibile nei campi del modulo di destinazione per';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Sostituisci "date_field" con Nome campo che è disponibile nei campi del modulo di destinazione per';
$mod_strings['LBL_BASE_ERROR'] = 'Sostituisci il parametro "base" con qualsiasi base di registro come 2, 10 per';
$mod_strings['LBL_DAYS_ERROR'] = 'Sostituisci il parametro "giorni" con qualsiasi numero per';
$mod_strings['LBL_WEEK_ERROR'] = 'Sostituisci il parametro "settimana" con qualsiasi numero per';
$mod_strings['LBL_MONTH_ERROR'] = 'Sostituisci il parametro "mese" con qualsiasi numero per';
$mod_strings['LBL_YEAR_ERROR'] = 'Sostituisci il parametro "year" con qualsiasi numero per';
$mod_strings['LBL_HOURS_ERROR'] = 'Sostituisci il parametro "hours" con qualsiasi numero per';
$mod_strings['LBL_FUNCTION'] = 'Funzione';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Selezionare la funzione matematica o logica per eseguire il calcolo sul campo.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Selezionare il campo su cui è necessario eseguire il calcolo'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Una volta selezionata la funzione, si popolerà automaticamente nella casella Input (Formula) e se si seleziona il campo su cui è necessario eseguire il calcolo, verrà automaticamente compilato. Devi copiare quel campo e metterlo in funzione come parametro.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Selezionare Campo modulo primario per il calcolo del riempimento automatico in base alla selezione del campo correlato';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Aggiorna licenza';
$mod_strings['LBL_ADD_NEW'] = '+ Aggiungi nuovo';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Al momento non ci sono record salvati.';
$mod_strings['LBL_CREATE'] = 'CREARE';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' uno adesso.';
$mod_strings['LBL_DELETE'] = "Elimina";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Seleziona i record.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Sei sicuro di voler eliminare';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'questi';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'Questo';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'riga?';
$mod_strings['LBL_EDIT'] = 'modificare';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "Stato attivato con successo !!";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Stato disattivato con successo !!";

$mod_strings['LBL_BULK_ACTION'] = 'AZIONE BULK';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' il campo è già selezionato per Mappatura campi. Seleziona un altro campo.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' campo è già selezionato per Calcola campo. Seleziona un altro campo.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Aggiungi il parametro "anno" con qualsiasi numero per';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Aggiungi il parametro "mese" con qualsiasi numero per';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Aggiungi il parametro "settimana" con qualsiasi numero per';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Aggiungi il parametro "giorni" con qualsiasi numero per';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Si prega di aggiungere il parametro "base" con qualsiasi numero per';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Aggiungi il parametro "ore" con qualsiasi numero per';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'in Input Formula';