<?php
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
//administration link
$mod_strings['LBL_AUTOPOPULATE_FIELDS'] = 'Автоматично заповнювати поля';
$mod_strings['LBL_AUTOPOPULATE_FIELDS_DESCRIPTION'] = 'Керуйте автоматичним заповненням полів залежно від полів відношення';

//editview
//button
$mod_strings['LBL_BACK_BUTTON'] = 'НАЗАД';
$mod_strings['LBL_NEXT_BUTTON'] = 'NEXT';
$mod_strings['LBL_CLEAR_BUTTON'] = 'CLEAR';
$mod_strings['LBL_SAVE_BUTTON'] = 'SAVE';
$mod_strings['LBL_CANCEL_BUTTON'] = 'CANCEL';

$mod_strings['LBL_SELECT_AN_OPTION'] = 'Виберіть варіант';

//step 1
$mod_strings['LBL_PRIMARY_MODULE'] = 'Модуль джерела (Скопіюйте дані в)';
$mod_strings['LBL_STATUS'] = 'Статус';
$mod_strings['LBL_SELECT_MODULE'] = 'Виберіть Модуль';
$mod_strings['LBL_ACTIVE'] = 'Активний';
$mod_strings['LBL_INACTIVE'] = 'Неактивний';

//step2
$mod_strings['LBL_FIELD_MAPPING'] = 'Поле зіставлення';
$mod_strings['LBL_ADD_FIELD_MAPPING_BUTTON'] = 'Додати зіставлення поля';
$mod_strings['LBL_SELECT_REALTE_FIELD'] = 'Виберіть повязане поле';
$mod_strings['LBL_ADD_FIELD_MAPPING_BLOCK'] = 'Додати блок картографії поля';
$mod_strings['LBL_SOURCE_MODULE'] = 'Модуль джерела';
$mod_strings['LBL_TARGET_MODULE'] = 'Цільовий модуль';
$mod_strings['LBL_FIELDS'] = 'Поля';
$mod_strings['LBL_RELATED_FIELD'] = 'Споріднене поле';

//step 3
$mod_strings['LBL_CALCULATE_FIELD'] = 'Розрахувати поле';
$mod_strings['LBL_FUNCTIONS'] = 'Функції';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BUTTON'] = 'Додати поле Обчислити';
$mod_strings['LBL_INPUT_FORMULA'] = 'Введення (формула)';
$mod_strings['LBL_ADD_CALCULATE_FIELD_BLOCK'] = 'Додати поле обчислення поля';

//function name
$mod_strings['LBL_FUNCTION_ADD'] = 'Додати';
$mod_strings['LBL_FUNCTION_SUBTRACT'] = 'Відняти';
$mod_strings['LBL_FUNCTION_MULTIPLY'] = 'Помножте';
$mod_strings['LBL_FUNCTION_DIVIDE'] = 'Розділити';
$mod_strings['LBL_FUNCTION_STRING_LENGTH'] = 'Довжина рядка';
$mod_strings['LBL_FUNCTION_CONCAT'] = 'Конкатенація';
$mod_strings['LBL_FUNCTION_LOGARITHM'] = 'Журнал (логарифм)';
$mod_strings['LBL_LN'] = 'Ln (природний журнал)';
$mod_strings['LBL_ABSOLUTE'] = 'Абсолют';
$mod_strings['LBL_FUNCTION_AVERAGE'] = 'Середній';
$mod_strings['LBL_FUNCTION_POWER'] = 'Потужність';
$mod_strings['LBL_FUNCTION_DATE_DIFF'] = 'Різниця дат';
$mod_strings['LBL_FUNCTION_PERCENTAGE'] = 'Процент';
$mod_strings['LBL_FUNCTION_MOD'] = 'Мод (по модулю)';
$mod_strings['LBL_FUNCTION_MINIMUM'] = 'Мінімум';
$mod_strings['LBL_FUNCTION_NEGATE'] = 'Відхилити';
$mod_strings['LBL_FUNCTION_FLOOR'] = 'Поверх';
$mod_strings['LBL_FUNCTION_CEIL'] = 'Ceil';
$mod_strings['LBL_FUNCTION_ADD_HOURS'] = 'Додати години';
$mod_strings['LBL_FUNCTION_ADD_DAY'] = 'Додати день';
$mod_strings['LBL_FUNCTION_ADD_WEEK'] = 'Додати тиждень';
$mod_strings['LBL_FUNCTION_ADD_MONTH'] = 'Додати місяць';
$mod_strings['LBL_FUNCTION_ADD_YEAR'] = 'Додати рік';
$mod_strings['LBL_FUNCTION_SUB_HOURS'] = 'Підгодини';
$mod_strings['LBL_FUNCTION_SUB_DAYS'] = 'Sub Days';
$mod_strings['LBL_FUNCTION_SUB_WEEK'] = 'Sub тиждень';
$mod_strings['LBL_FUNCTION_SUB_MONTH'] = 'Підмісячний місяць';
$mod_strings['LBL_FUNCTION_SUB_YEAR'] = 'Підрік';
$mod_strings['LBL_FUNCTION_DIFF_DAYS'] = 'Різні дні';
$mod_strings['LBL_FUNCTION_DIFF_HOUR'] = 'Різна година';
$mod_strings['LBL_FUNCTION_DIFF_MINUTE'] = 'Різна хвилина';
$mod_strings['LBL_FUNCTION_DIFF_MONTH'] = 'Різний місяць';
$mod_strings['LBL_FUNCTION_DIFF_WEEK'] = 'Різниця тижня';
$mod_strings['LBL_FUNCTION_DIFF_YEAR'] = 'Річний рік';

//validation msg
$mod_strings['LBL_NUMERIC_FIELD_VALIDATION'] = 'Використовуйте лише поля "Integer" або "Decimal" у числових операціях';
$mod_strings['LBL_STRING_FIELD_VALIDATION'] = 'Використовуйте лише поля "String" у операціях String';
$mod_strings['LBL_DATE_FIELD_VALIDATION'] = 'Використовуйте лише поля "Дата" АБО "Дата" у операціях Дата';
$mod_strings['LBL_REQUIRED_FIELD_VALIDATION'] = "Будь ласка, заповніть все обов'язкове поле!!!";
$mod_strings['LBL_FIELD_TYPE_MATCH_VALIDATION'] = 'Виберіть поле "Тип відповідності"!!!';
$mod_strings['LBL_PLEASE_ENTER_VALUE'] = 'Введіть значення';
$mod_strings['LBL_DUPLICATE_RECORD_MESSAGE'] = ' Конфігурація модуля вже існує. Виберіть інший модуль.';
$mod_strings['LBL_SELECT_REALTE_FIELD_VALIDATION'] = 'Будь ласка, виберіть відповідне поле перед натисканням на кнопку "Додати блок картографії поля".';
$mod_strings['LBL_SELECT_MAPPING_FIELD'] = 'Виберіть поле для картографування поля.';

$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_1'] = 'Для ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_2'] = ', Блок картографії вже доданий. Якщо ви хочете додати ще одне зіставлення поля для ';
$mod_strings['LBL_RELATED_FIELD_DUPLICATE_MSG_3'] = ' потім використовуйте існуючий блок.';

$mod_strings['LBL_RELATED_CALCULATE_FIELD_DUPLICATE_MSG_2'] = ', Обчисліть вже доданий блок поля. Якщо ви хочете додати інше поле для обчислення для ';

$mod_strings['LBL_PLEASE_FILL_ALL_FIELDS'] = 'Будь ласка, заповніть усі поля !!!';
$mod_strings['LBL_STRING_FIELD_ERROR'] = 'Будь ласка, замініть "string_field" на імя поля, яке доступне в полях цільового модуля для';
$mod_strings['LBL_NUMBER_FIELD_ERROR'] = 'Замініть "number_field" на імя поля, яке доступне в полях цільового модуля для';
$mod_strings['LBL_DATE_FIELD_ERROR'] = 'Будь ласка, замініть "date_field" на імя поля, яке доступне в полях цільового модуля для';
$mod_strings['LBL_BASE_ERROR'] = 'Будь ласка, замініть параметр "base" на будь-яку базу журналу, наприклад, 2, 10 for';
$mod_strings['LBL_DAYS_ERROR'] = 'Будь ласка, замініть параметр "днів" на будь-яке число для';
$mod_strings['LBL_WEEK_ERROR'] = 'Будь ласка, замініть параметр "тиждень" на будь-яке число для';
$mod_strings['LBL_MONTH_ERROR'] = 'Будь ласка, замініть параметр "місяць" на будь-яке число';
$mod_strings['LBL_YEAR_ERROR'] = 'Будь ласка, замініть параметр "year" на будь-яке число для';
$mod_strings['LBL_HOURS_ERROR'] = 'Будь ласка, замініть параметр "годин" на будь-яке число для';
$mod_strings['LBL_FUNCTION'] = 'Функція';

//info
$mod_strings['LBL_FUNCTION_INFO'] = 'Виберіть математичну або логічну функцію для виконання розрахунку на полі.';
$mod_strings['LBL_SECONDARY_MODULE_FIELD_INFO'] = 'Виберіть поле, на якому потрібно виконати розрахунок'; 
$mod_strings['LBL_INPUT_FORMULA_INFO'] = "Після того, як ви виберете функцію, вона автоматично заповнить в поле вводу (Формула), і якщо ви виберете поле, на якому вам потрібно виконати розрахунок, то також буде автоматично заповнено. Потрібно скопіювати це поле і покласти його в функцію як параметр.";
$mod_strings['LBL_PRIMARY_MODULE_FIELD_INFO'] = 'Виберіть "Основне поле модуля" для автоматичного розрахунку на основі відповідного вибору поля';

//listview
$mod_strings['LBL_UPDATE_LICENSE'] = 'Оновити ліцензію';
$mod_strings['LBL_ADD_NEW'] = '+ Додати нове';
$mod_strings['LBL_CREATE_MESSAGE'] = 'Наразі у вас немає збережених записів.';
$mod_strings['LBL_CREATE'] = 'СТВОРИТИ';
$mod_strings['LBL_CREATE_MESSAGE_ONE'] = ' один зараз.';
$mod_strings['LBL_DELETE'] = "Видалити";
$mod_strings['LBL_PLEASE_SELECT_RECORDS'] = 'Виберіть записи.';
$mod_strings['LBL_DELETE_MESSAGE_1'] = 'Ви впевнені, що хочете видалити';
$mod_strings['LBL_DELETE_MESSAGE_2'] = 'ці';
$mod_strings['LBL_DELETE_MESSAGE_3'] = 'це';
$mod_strings['LBL_DELETE_MESSAGE_4'] = 'рядок?';
$mod_strings['LBL_EDIT'] = 'Редагувати';

$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_ACTIVATED'] = "Статус активовано успішно !!";
$mod_strings['LBL_AUTO_POPULATE_FIELDS_STATUS_DEACTIVATED'] = "Статус деактивовано успішно !!";

$mod_strings['LBL_BULK_ACTION'] = 'БУЛЬКА ДІЯ';

$mod_strings['LBL_FIELD_MAPPING_VALIDATION'] = ' поле вже вибране для картографування поля. Виберіть інше поле.';
$mod_strings['LBL_CALCULATE_FIELD_MAPPING_VALIDATION'] = ' поле вже вибране для поля обчислення. Виберіть інше поле.';

$mod_strings['LBL_YEAR_PARAMETER_ERROR'] = 'Будь ласка, додайте параметр "рік" із будь-яким номером для';
$mod_strings['LBL_MONTH_PARAMETER_ERROR'] = 'Будь ласка, додайте параметр "місяць" із будь-яким номером для';
$mod_strings['LBL_WEEK_PARAMETER_ERROR'] = 'Будь ласка, додайте параметр "тиждень" із будь-яким номером для';
$mod_strings['LBL_DAYS_PARAMETER_ERROR'] = 'Будь ласка, додайте параметр "днів" з будь-яким номером для';
$mod_strings['LBL_BASE_PARAMETER_ERROR'] = 'Будь ласка, додайте параметр "базовий" з будь-яким номером для';
$mod_strings['LBL_HOURS_PARAMETER_ERROR'] = 'Будь ласка, додайте параметр "годин" з будь-яким номером для';
$mod_strings['LBL_INPUT_FORMULA_ERROR'] = 'у формулі введення';