<?php
 /*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
$entry_point_registry['VIAutoPopulteFieldsFetchRelateModule'] = array(
  'file' => 'custom/VIAutoPopulateFields/VIAutoPopulteFieldsFetchRelateModule.php',
  'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsSaveConfiguration'] = array(
  'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsSaveConfiguration.php',
  'auth' => true
);

 $entry_point_registry['VIAutoPopulateFieldsSourceTargetModuleFields'] = array(
 	'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsSourceTargetModuleFields.php',
  'auth' => true
 );

$entry_point_registry['VIAutoPopulateFieldsDeleteRecord'] = array(
  'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsDeleteRecord.php',
  'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsMappingFieldType'] = array(
  'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsMappingFieldType.php',
  'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsRelateFieldName'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsRelateFieldName.php',
    'auth' => true
  );

$entry_point_registry['VIAutoPopulateFieldsRelateFieldValue'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsRelateFieldValue.php',
    'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsCheckDuplicateConfiguration'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsCheckDuplicateConfiguration.php',
    'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsDisplayCalculateFieldsFormula'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsDisplayCalculateFieldsFormula.php',
    'auth' => true
);

$entry_point_registry['VIAutoPopulateFieldsCheckCalculateFieldType'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulateFieldsCheckCalculateFieldType.php',
    'auth' => true
);
$entry_point_registry['VIAutoPopulteFieldsFieldMappingBlock'] = array(
    'file' => 'custom/VIAutoPopulateFields/VIAutoPopulteFieldsFieldMappingBlock.php',
    'auth' => true
);