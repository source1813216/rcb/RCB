<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * This file is part of package Auto Populate Fields.
 * 
 * Author : Variance InfoTech PVT LTD (http://www.varianceinfotech.com)
 * All rights (c) 2020 by Variance InfoTech PVT LTD
 *
 * This Version of Auto Populate Fields is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * written consent of Variance InfoTech PVT LTD
 * 
 * You can contact via email at info@varianceinfotech.com
 * 
 ********************************************************************************/
class VIAutoPopulateFields{
    function VIAutoPopulateFields(){ 
        if($_REQUEST['action'] == 'EditView' || $GLOBALS['app']->controller->action == 'EditView' || $_REQUEST['action'] == 'SubpanelCreates' || $GLOBALS['app']->controller->action == 'SubpanelCreates'){
            //data
            if($_REQUEST['action'] == 'EditView'){
                $primaryModule = json_encode($_REQUEST['module']);
            }else{
                 $primaryModule = json_encode($_REQUEST['target_module']);
            }//end of else
            
            $action = json_encode($_REQUEST['action']);
            
            echo '<script type="text/javascript">var module = '.$primaryModule.';var action = '.$action.';';
            echo 'var script = document.createElement("script");script.type  = "text/javascript";';
            echo 'script.src = "custom/include/VIAutoPopulateFields/VIAutopopulateFields.js?v="+Math.random();document.body.appendChild(script);</script>';
        }//end of if

        if($GLOBALS['app']->controller->action == 'index' && $_REQUEST['module'] == 'Administration'){
            global $suitecrm_version;
            if (version_compare($suitecrm_version, '7.10.2', '>=')){
                echo '<link rel="stylesheet" type="text/css" href="custom/include/VIAutoPopulateFields/VIAutoPopulateFieldsIcon.css">';
            }//end of if
        }//end of if 
    }//end of function
}//end of class


