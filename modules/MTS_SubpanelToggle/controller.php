<?php

class MTS_SubpanelToggleController extends SugarController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function process()
    {
        require_once ('modules/MTS_SubpanelToggle/license/OutfittersLicense.php');
        # Validate in local before
        $validate_license = MTS_SubpanelToggleOutfittersLicense::isValid('MTS_SubpanelToggle');
        if ($validate_license !== true && $this->action == 'index') {
            if (is_admin($GLOBALS['current_user'])) {
                SugarApplication::appendErrorMessage('MTS Subpanel Toggle Addon is no longer active due to the following reason: ' . $validate_license . ' Users will have limited to no access until the issue has been addressed.');
            }
            echo '<h2><p class="error">MTS Subpanel Toggle Addon is no longer active</p></h2><p class="error">Please renew your subscription or check your <a href="index.php?module=MTS_SubpanelToggle&action=license">license configuration</a>.</p>';
            die;
        }
        if( !is_admin($GLOBALS['current_user'])) {
            $this->hasAccess = false;
        }
        parent::process();
    }
}
