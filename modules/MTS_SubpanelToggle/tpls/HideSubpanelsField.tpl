<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td colspan="100">

            <script type="text/javascript" src="cache/include/javascript/sugar_grp_yui_widgets.js"></script>
            <link rel="stylesheet" type="text/css" href="modules/Connectors/tpls/tabs.css"/>
                <input type="hidden" name="hide_subpanels" value="">
                <div class='add_table' style='margin-bottom:5px'>
                    <table id="SubpanelConfig" class="FieldConfig edit view" style='margin-bottom:0px;'
                           border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width='1%'>
                                <div id="enabled_div"></div>
                            </td>
                            <td>
                                <div id="disabled_div"></div>
                            </td>
                        </tr>
                    </table>
                </div>
        </td>
    </tr>
</table>

<script type="text/javascript">
    (function () {ldelim}
        var Connect = YAHOO.util.Connect;
        Connect.url = 'index.php';
        Connect.method = 'POST';
        Connect.timeout = 300000;
        var get = YAHOO.util.Dom.get;

        var hide_subpanels = {$hide_subpanels};
        var display_subpanels = {$display_subpanels};
        var modStrings = {$mod_strings};
        var lblEnabled = modStrings.LBL_HIDE_SUBPANELS;
        var lblDisabled = modStrings.LBL_DISPLAY_SUBPANELS;
        {literal}
        SUGAR.hideSubpanels = new YAHOO.SUGAR.DragDropTable(
            "enabled_div",
            [{key: "label", label: lblEnabled, width: 200, sortable: false},
                {key: "name", label: lblEnabled, hidden: true}],
            new YAHOO.util.LocalDataSource(hide_subpanels, {
                responseSchema: {fields: [{key: "name"}, {key: "label"}]}
            }),
            {height: "300px"}
        );
        SUGAR.displaySubpanels = new YAHOO.SUGAR.DragDropTable(
            "disabled_div",
            [{key: "label", label: lblDisabled, width: 200, sortable: false},
                {key: "name", label: lblDisabled, hidden: true}],
            new YAHOO.util.LocalDataSource(display_subpanels, {
                responseSchema: {fields: [{key: "name"}, {key: "label"}]}
            }),
            {height: "300px"}
        );

        SUGAR.hideSubpanels.disableEmptyRows = true;
        SUGAR.displaySubpanels.disableEmptyRows = true;
        SUGAR.hideSubpanels.addRow({name: "", label: ""});
        SUGAR.displaySubpanels.addRow({name: "", label: ""});
        SUGAR.hideSubpanels.render();
        SUGAR.displaySubpanels.render();

        SUGAR.getHideSubpanels = function () {
            var enabledTable = SUGAR.hideSubpanels;
            var fields = [];
            for (var i = 0; i < enabledTable.getRecordSet().getLength(); i++) {
                var data = enabledTable.getRecord(i).getData();
                if (data.name && data.name != '')
                    fields.push(data.name);
            }
            return fields.join(',');
        };
    })();
    function check_form(formname) {
        if (typeof (siw) != 'undefined' && siw && typeof (siw.selectingSomething) != 'undefined' && siw.selectingSomething)
            return false;
        $('form[name=' + formname + ']').find('input[name=hide_subpanels]').val(SUGAR.getHideSubpanels());
        return validate_form(formname, '');
    }
    {/literal}
</script>
<script type="text/javascript">
</script>
