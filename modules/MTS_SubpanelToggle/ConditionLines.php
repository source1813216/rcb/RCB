<?php


function displayConditionLines($focus, $field, $value, $view)
{
    global $mod_strings;

    $html = '';

    if (!is_file('cache/jsLanguage/AOW_Conditions/' . $GLOBALS['current_language'] . '.js')) {
        require_once('include/language/jsLanguage.php');
        jsLanguage::createModuleStringsCache('AOW_Conditions', $GLOBALS['current_language']);
    }
    $html .= '<script src="cache/jsLanguage/AOW_Conditions/'. $GLOBALS['current_language'] . '.js"></script>';

    if ($view == 'EditView') {
        $html .= '<script src="modules/MTS_SubpanelToggle/js/ConditionLines.js"></script>';
        $html .= "<table border='0' cellspacing='4' width='100%' id='aow_conditionLines'></table>";

        $html .= "<div style='padding-top: 10px; padding-bottom:10px;'>";
        $html .= "<input type=\"button\" tabindex=\"116\" class=\"button\" value=\"".$mod_strings['LBL_ADD_CONDITION']."\" id=\"btn_ConditionLine\" onclick=\"insertConditionLine()\" disabled/>";
        $html .= "</div>";


        if (isset($focus->target_module) && $focus->target_module != '') {
            require_once("modules/AOW_WorkFlow/aow_utils.php");
            $html .= "<script>";
            $html .= "target_module = \"".$focus->target_module."\";";
            $html .= "document.getElementById('btn_ConditionLine').disabled = '';";
            if ($focus->id != '') {
                $conditions = unserialize(html_entity_decode($focus->conditions));
                foreach ($conditions as $condition) {
                    if ($condition['value_type'] == 'Date') {
                        $condition['value'] = unserialize(base64_decode($condition['value']));
                    }
                    $html .= "flow_fields = \"".trim(preg_replace('/\s+/', ' ', getModuleFields($focus->target_module)))."\";";
                    $condition_item = json_encode($condition);
                    $html .= "loadConditionLine(".$condition_item.");";
                }
            }
            $html .= "flow_fields = \"".trim(preg_replace('/\s+/', ' ', getModuleFields($focus->target_module)))."\";";
            $html .= "</script>";
        }
    } elseif ($view == 'DetailView') {
        $html .= '<style>#conditions_span{ display: unset !important;}</style><script src="modules/MTS_SubpanelToggle/js/ConditionLines.js"></script>';
        $html .= "<table border='0' cellspacing='0' width='100%' id='aow_conditionLines'></table>";

        if (isset($focus->target_module) && $focus->target_module != '') {
            require_once("modules/AOW_WorkFlow/aow_utils.php");
            $html .= "<script>";
            $html .= "target_module = \"".$focus->target_module."\";";
            $conditions = unserialize(html_entity_decode($focus->conditions));
            foreach ($conditions as $condition) {
                if ($condition['value_type'] == 'Date') {
                    $condition['value'] = unserialize(base64_decode($condition['value']));
                }
                $html .= "flow_fields = \"".trim(preg_replace('/\s+/', ' ', getModuleFields($focus->target_module)))."\";";
                $condition_item = json_encode($condition);
                $html .= "loadConditionLine(".$condition_item.");";
            }
            $html .= "</script>";
        }
    }
    return $html;
}
