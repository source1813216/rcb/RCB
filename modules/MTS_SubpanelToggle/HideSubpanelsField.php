<?php

function displayHideSupanels ($focus, $field, $value, $view) {
    $mts_sub_toggle = new MTS_SubpanelToggle();
    return $mts_sub_toggle->getHideSubpanelsFieldHtml($focus->target_module, $focus->id, $view);
}