<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */


class MTS_SubpanelToggle extends Basic
{
    public $new_schema = true;
    public $module_dir = 'MTS_SubpanelToggle';
    public $object_name = 'MTS_SubpanelToggle';
    public $table_name = 'mts_subpaneltoggle';
    public $importable = false;

    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $SecurityGroups;
    public $disable_vardefs = false;
	
    public function bean_implements($interface)
    {
        switch($interface)
        {
            case 'ACL':
                return true;
        }

        return false;
    }

    public function display()
    {
        $bean = BeanFactory::getBean($_REQUEST['module'], $_REQUEST['record']);
        if ($bean) {
            if ($this->checkLicense()) {
                $this->addHideSubpanelScript($bean);
            }
        }
    }

    private function getAvailableSubpanels($module) {
        $bean = BeanFactory::getBean($module);
        $GLOBALS['focus'] = $bean;
        require_once('include/SubPanel/SubPanelTiles.php');
        $subpanel = new SubPanelTiles($bean, $module);
        $tabs = $subpanel->getTabs(false, '') ;
        $subpanel_list = array();
        foreach ($tabs as $t => $tab) {
            if (empty($tab)) {
                continue;
            }
            $thisPanel = $subpanel->subpanel_definitions->load_subpanel($tab);
            if ($thisPanel === false) {
                continue;
            }
            $subpanel_list[$tab] = $thisPanel->get_title();
        }
        return $subpanel_list;
    }

    private function getSubpanels($module, $record = '') {
        $hide_subpanels = array();
        $display_subpanels = array();
        if (!empty($module)) {
            $available_subpanels = $this->getAvailableSubpanels($module);
            $sub_toggle_bean = new MTS_SubpanelToggle();
            if (!empty($record)) {
                $sub_toggle_bean->retrieve($record);
            }
            if ($sub_toggle_bean) {
                $hide_subpanel_keys = explode(',', $sub_toggle_bean->hide_subpanels);
                foreach ($available_subpanels as $key => $label) {
                    if (!in_array($key, $hide_subpanel_keys)) {
                        $display_subpanels[] = array(
                            'name' => $key,
                            'label' => $label
                        );
                    } else {
                        $hide_subpanels[] = array(
                            'name' => $key,
                            'label' => $label
                        );
                    }
                }
            }
        }
        return array(
            'hide_subpanels' => $hide_subpanels,
            'display_subpanels' => $display_subpanels
        );
    }
    private function getHiddenSubpanel($bean)
    {
        $sub_toggle_bean = new MTS_SubpanelToggle();
        $list = $sub_toggle_bean->get_full_list("", "target_module='{$bean->module_dir}'");
        if (empty($list)) {
            return array();
        }
        $hide_subpanels = array();
        foreach($list as $config_bean) {
            if (empty($config_bean->hide_subpanels)) {
                continue;
            }
            if ($this->passedConditions($bean, $config_bean)) {
                $hide_subpanels = array_merge($hide_subpanels, explode(',', $config_bean->hide_subpanels));
            }
        }
        return $hide_subpanels;
    }

    public function addHideSubpanelScript($bean) {
        $hide_subpanels = $this->getHiddenSubpanel($bean);
        if (empty($hide_subpanels)) {
            return '';
        }
        $selectors = '#whole_subpanel_' . implode(", #whole_subpanel_", $hide_subpanels);
        $css = "<style>{$selectors} {
                display: none !important;
        }</style>";
        echo $css;
    }

    public function checkLicense()
    {
        global $current_user;
        require_once('modules/MTS_SubpanelToggle/license/OutfittersLicense.php');
        # Validate in local before
        $validate_license = MTS_SubpanelToggleOutfittersLicense::isValid('MTS_SubpanelToggle');
        if ($validate_license !== true) {
            if (is_admin($current_user)) {
                SugarApplication::appendErrorMessage('MTS History Summary Addon is no longer active due to the following reason: ' . $validate_license . ' Users will have limited to no access until the issue has been addressed.');
                echo '<h2><p class="error">MTS Subpanel Toggle Addon is no longer active</p></h2><p class="error">Please renew your subscription or check your <a href="index.php?module=MTS_SubpanelToggle&action=license">license configuration</a>.</p>';
            }
            else {
                echo '<h2><p class="error">MTS Subpanel Toggle Addon is no longer active</p></h2><p class="error">Please contact administrator.</p>';
            }
            return false;
        }
        return true;
    }

    public function getHideSubpanelsFieldHtml($module = '', $record = '', $view = 'EditView') {
        global $current_language;
        $ss = new Sugar_Smarty();
        $mod = return_module_language($current_language, 'MTS_SubpanelToggle');
        $subpanels = $this->getSubpanels($module, $record);
        if ($view == 'EditView') {
            $ss->assign('hide_subpanels', json_encode($subpanels['hide_subpanels']));
            $ss->assign('display_subpanels', json_encode($subpanels['display_subpanels']));
            $ss->assign('mod_strings', json_encode($mod));
            return $ss->fetch('modules/MTS_SubpanelToggle/tpls/HideSubpanelsField.tpl');
        } else {
            $hide_subpanels = array();
            foreach($subpanels['hide_subpanels'] as $hide_subpanel) {
                $hide_subpanels[] = $hide_subpanel['label'];
            }
            return implode(', ', $hide_subpanels);
        }
    }

    public function getAvailableModules()
    {
        global $beanList;
        $myModules = array();

        if (!is_array($beanList)) {
            return $myModules;
        }

        static $ignoredModuleList = array('iFrames', 'Feeds', 'Home', 'Dashboard', 'Calendar', 'Activities', 'Reports', 'MTS_SubpanelToggle');


        $actions = ACLAction::getUserActions($this->id);

        foreach ($beanList as $module => $val) {
            $module = $this->fixupModuleForACL($module);
            if (in_array($module, $myModules)) {
                continue;
            }
            if (in_array($module, $ignoredModuleList)) {
                continue;
            }

            $focus = SugarModule::get($module)->loadBean();
            if ($focus instanceof SugarBean) {
                $key = $focus->acltype;
            } else {
                $key = 'module';
            }

            if (isset($actions[$module][$key])
            ) {
                $myModules[$module] = $GLOBALS['app_list_strings']['moduleList'][$module];
            }
        }

        return $myModules;
    }

    protected function fixupModuleForACL($module)
    {
        if ($module == 'ContractTypes') {
            $module = 'Contracts';
        }
        if (preg_match('/Product[a-zA-Z]*/', $module)) {
            $module = 'Products';
        }

        return $module;
    }

    public function get_list_view_data()
    {
        $temp_array = parent::get_list_view_data();
        if (isset($temp_array['ID'])) {
            $row = $this->db->fetchOne("SELECT target_module FROM mts_subpaneltoggle WHERE id='{$temp_array['ID']}'");
            if (isset($row['target_module'])) {
                $temp_array["HIDE_SUBPANELS"] = $this->getHideSubpanelsFieldHtml($row['target_module'], $temp_array['ID'], 'ListView');
            }
        }
        return $temp_array;
    }


    public function save($check_notify = false)
    {

        global $current_user;
        if (!$current_user->is_admin) {
            ACLController::displayNoAccess(true);
        }
        $this->conditions = serialize($this->getConditions());
        return parent::save($check_notify); // TODO: Change the autogenerated stub
    }

    public function getConditions()
    {
        require_once('modules/AOW_WorkFlow/aow_utils.php');
        $conditions = array();
        if (isset($_REQUEST['aow_conditions_field'])) {
            for ($i = 0; $i < count($_REQUEST['aow_conditions_field']); $i++) {
                if (empty($_REQUEST['aow_conditions_field'][$i]) || $_REQUEST['aow_conditions_deleted'][$i] == '1') continue;
                if (is_array($_REQUEST['aow_conditions_value'][$i])) {
                    switch ($_REQUEST['aow_conditions_value_type'][$i]) {
                        case 'Date':
                            $value = base64_encode(serialize($_REQUEST['aow_conditions_value'][$i]));
                            break;
                        default:
                            $value = encodeMultienumValue($_REQUEST['aow_conditions_value'][$i]);
                    }
                } else {
                    if ($_REQUEST['aow_conditions_value_type'][$i] === 'Value') {
                        $value = fixUpFormatting($_REQUEST['target_module'], $_REQUEST['aow_conditions_field'][$i], $_REQUEST['aow_conditions_value'][$i]);
                    } else {
                        $value = $_REQUEST['aow_conditions_value'][$i];
                    }
                }
                $conditions[] = array (
                    'field' => $_REQUEST['aow_conditions_field'][$i],
                    'operator' => $_REQUEST['aow_conditions_operator'][$i],
                    'value_type' => $_REQUEST['aow_conditions_value_type'][$i],
                    'value' => $value
                );
            }
        }
        return $conditions;
    }

    public function passedConditions(SugarBean $bean, $sub_toggle)
    {
        global $app_list_strings, $timedate;
        if (!$sub_toggle) return false;
        $conditions = unserialize(html_entity_decode($sub_toggle->conditions));
        if (empty($conditions)) {
            return false;
        }
        $count_right_cond = 0;
        foreach ($conditions as $condition) {
            $field = $condition['field'];
            $value = $condition['value'];

            $dateFields = array('date','datetime', 'datetimecombo');
            if ($this->isSQLOperator($condition['operator'])) {
                $data = $bean->field_defs[$field];

                if ($data['type'] === 'relate' && isset($data['id_name'])) {
                    $field = $data['id_name'];
                    $condition->field = $data['id_name'];
                }
                $field = $bean->$field;

                if (in_array($data['type'], $dateFields)) {
                    $field = strtotime($field);
                }

                switch ($condition['value_type']) {
                    case 'Field':
                        $data = $bean->field_defs[$value];

                        if ($data['type'] === 'relate' && isset($data['id_name'])) {
                            $value = $data['id_name'];
                        }
                        $value = $bean->$value;

                        if (in_array($data['type'], $dateFields)) {
                            $value = strtotime($value);
                        }

                        break;

                    case 'Date':
                        $params =  unserialize(base64_decode($value));
                        $dateType = 'datetime';
                        if ($params[0] == 'now') {
                            $value = date('Y-m-d H:i:s');
                        } elseif ($params[0] == 'today') {
                            $dateType = 'date';
                            $value = date('Y-m-d');
                            $field = strtotime(date('Y-m-d', $field));
                        } else {
                            $fieldName = $params[0];
                            $value = $bean->$fieldName;
                        }

                        if ($params[1] != 'now') {
                            switch ($params[3]) {
                                case 'business_hours':
                                    if (file_exists('modules/AOBH_BusinessHours/AOBH_BusinessHours.php')) {
                                        require_once('modules/AOBH_BusinessHours/AOBH_BusinessHours.php');

                                        $businessHours = new AOBH_BusinessHours();

                                        $amount = $params[2];
                                        if ($params[1] != "plus") {
                                            $amount = 0-$amount;
                                        }

                                        $value = $businessHours->addBusinessHours($amount, $timedate->fromDb($value));
                                        $value = strtotime($timedate->asDbType($value, $dateType));
                                        break;
                                    }
                                    //No business hours module found - fall through.
                                    $params[3] = 'hours';
                                // no break
                                default:
                                    $value = strtotime($value.' '.$app_list_strings['aow_date_operator'][$params[1]]." $params[2] ".$params[3]);
                                    if ($dateType == 'date') {
                                        $value = strtotime(date('Y-m-d', $value));
                                    }
                                    break;
                            }
                        } else {
                            $value = strtotime($value);
                        }
                        break;

                    case 'Multi':

                        $value = unencodeMultienum($value);
                        if ($data['type'] == 'multienum') {
                            $field = unencodeMultienum($field);
                        }
                        switch ($condition['operator']) {
                            case 'Not_Equal_To':
                                $condition['operator'] = 'Not_One_of';
                                break;
                            case 'Equal_To':
                            default:
                                $condition['operator'] = 'One_of';
                                break;
                        }
                        break;
                    case 'SecurityGroup':
                        if (file_exists('modules/SecurityGroups/SecurityGroup.php')) {
                            $sg_module = $bean->module_dir;
                            if (isset($data['module']) && $data['module'] != '') {
                                $sg_module = $data['module'];
                            }
                            $value = $this->check_in_group($field, $sg_module, $value);
                            $field = true;
                            break;
                        }
                    // no break
                    case 'Value':
                    default:
                        if (in_array($data['type'], $dateFields) && trim($value) != '') {
                            $value = strtotime($value);
                        } elseif ($data['type'] == 'bool' && (!boolval($value) || strtolower($value) == 'false')) {
                            $value = 0;
                        }
                        break;
                }
                $GLOBALS['log']->fatal('Field ' . $field . ', value ' . $value);
                if (!($this->compare_condition($field, $value, $condition['operator']))) {
                    break;
                } else {
                    $count_right_cond ++;
                }
            }
        }
        if (count($conditions) == $count_right_cond && $count_right_cond > 0) {
            return true;
        }
        return false;
    }

    public function compare_condition($var1, $var2, $operator = 'Equal_To')
    {
        switch ($operator) {
            case "Not_Equal_To": return $var1 != $var2;
            case "Greater_Than":  return $var1 >  $var2;
            case "Less_Than":  return $var1 <  $var2;
            case "Greater_Than_or_Equal_To": return $var1 >= $var2;
            case "Less_Than_or_Equal_To": return $var1 <= $var2;
            case "Contains": return strpos($var1, $var2);
            case "Starts_With": return strrpos($var1, $var2, -strlen($var1));
            case "Ends_With": return strpos($var1, $var2, strlen($var1) - strlen($var2));
            case "is_null": return $var1 == '';
            case "One_of":
                if (is_array($var1)) {
                    foreach ($var1 as $var) {
                        if (in_array($var, $var2)) {
                            return true;
                        }
                    }
                    return false;
                }
                return in_array($var1, $var2);

            case "Not_One_of":
                if (is_array($var1)) {
                    foreach ($var1 as $var) {
                        if (in_array($var, $var2)) {
                            return false;
                        }
                    }
                    return true;
                }
                return !in_array($var1, $var2);

            case "Equal_To":
            default: return $var1 == $var2;
        }
    }

    public function check_in_group($bean_id, $module, $group)
    {
        $sql = "SELECT id FROM securitygroups_records WHERE record_id = '".$bean_id."' AND module = '".$module."' AND securitygroup_id = '".$group."' AND deleted=0";
        if ($module == 'Users') {
            $sql = "SELECT id FROM securitygroups_users WHERE user_id = '".$bean_id."' AND securitygroup_id = '".$group."' AND deleted=0";
        }
        $id = $this->db->getOne($sql);
        if ($id != '') {
            return true;
        }
        return false;
    }

    private function isSQLOperator($key)
    {
        return $this->getSQLOperator($key) ? true : false;
    }

    private function getSQLOperator($key)
    {
        $sqlOperatorList['Equal_To'] = '=';
        $sqlOperatorList['Not_Equal_To'] = '!=';
        $sqlOperatorList['Greater_Than'] = '>';
        $sqlOperatorList['Less_Than'] = '<';
        $sqlOperatorList['Greater_Than_or_Equal_To'] = '>=';
        $sqlOperatorList['Less_Than_or_Equal_To'] = '<=';
        $sqlOperatorList['Contains'] = 'LIKE';
        $sqlOperatorList['Starts_With'] = 'LIKE';
        $sqlOperatorList['Ends_With'] = 'LIKE';
        $sqlOperatorList['is_null'] = 'IS NULL';
        if (!isset($sqlOperatorList[$key])) {
            return false;
        }
        return $sqlOperatorList[$key];
    }

}