function loadHideSubpanelsField() {
    $.get('index.php?module=MTS_SubpanelToggle&action=loadHideSubpanelsField&sugar_body_only=true&target_module=' + $('#target_module').val() + '&record=' + $('input[name=record]').val() , function (subpanelHtml) {
        $('#hide_subpanels_span').html(subpanelHtml);
    });
}

$(document).ready(function () {
    $('#target_module').change(function () {
        loadHideSubpanelsField();
    });
});
