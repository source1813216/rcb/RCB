<?php


class MTSSubpanelToggleHook
{
    function appendSubpanelToggle()
    {
        if(isset($_REQUEST['action']) && strtolower($_REQUEST['action']) == 'detailview' && isset($_REQUEST['record']) && !empty($_REQUEST['record'])) {
            $sub_toggle = new MTS_SubpanelToggle();
            $sub_toggle->display();
        }
    }
}