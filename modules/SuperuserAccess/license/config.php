<?php

$outfitters_config = array(
    'name' => 'simba_dd_importer',
    'shortname' => 'superuseraccess',
    'public_key' => '3d51a92c112dccf9c373f8ce5f90335b',
    'api_url' => 'https://store.suitecrm.com/api/v1',
    'validate_users' => false,
    'manage_licensed_users' => false,
    'validation_frequency' => 'weekly',
    'continue_url' => 'index.php?module=SuperuserAccess&action=fi_restrictaccess',
);


// [OPTIONAL] If you want to make a single package install on both Sugar 6 and Sugar 7
// You may need different continue URLs.
// Usually you would put your Sugar 7 url in the array above
// Below you can check to see if it's Sugar 6, then set the continue_url to your Sugar 6 url

global $sugar_version;
if(preg_match( "/^6.*/", $sugar_version) ) {
     $outfitters_config['continue_url'] = 'index.php?module=SuperuserAccess&action=fi_restrictaccess';
}
