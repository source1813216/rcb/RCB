{literal}
<script type="text/javascript" src='modules/SuperuserAccess/js/jquery-sortable-min.js'></script>
<link href="modules/SuperuserAccess/js/jquery.multiselect.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src='modules/SuperuserAccess/js/jquery.multiselect.js'></script>
<script type="text/javascript">
function clicktosavebtn()
{
    if(confirm('Are you sure you want to Hide link for Other admin users.'))
    {
        $("#list_map_link li").each(function( index ) {
            $('#frm_settings').append('<input type="hidden" name="link_list[]" value="'+$( this ).find('input').val()+'" />');
            //console.log( index + ": " + $( this ).find('input').val() );
        });
        $('#OEPL_AjaxWaiting').show();
        return true;
    }
    return false;
}
</script>
<style>
.SC_main{float: left; width:830px; border: 1px solid #ccc;}
.SC_Head{width: 820px; float:left; padding:5px; background: #ccc; font-weight:bold;}
.SC_list{width: 820px; float:left; padding:5px;}
.SC_list:hover{background: #e9e9e9}
.SC_field{width: 190px; float:left; padding: 0 5px;}
.SC_alsofield{width: 490px; float:left; padding: 0 5px;}
.SC_condition{width: 90px; float:left; padding: 0 5px;}
.SC_closebtn{width: 20px; float:left; padding-top: 5px; text-align:center}
.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}

ol.vertical {
  margin: 0 0 9px 5px;
  min-height: 10px;
  height: 300px;
  overflow: auto;
  border: 1px solid #ccc; 
  list-style-type: none;
  padding-left: 10px;
}
/* line 13, */
ol.vertical li {
    display: block;
    margin: 5px;
    padding: 5px;
    border: 1px solid #cccccc;
    color: #000;
    background: #eeeeee; 
    cursor: move;
}
/* line 20, */
ol.vertical li.placeholder {
    position: relative;
    margin: 0;
    padding: 0;
    border: none; 
}
ol.vertical li.placeholder:before {
  position: absolute;
  content: "";
  width: 0;
  height: 0;
  margin-top: -5px;
  left: -5px;
  top: -4px;
  border: 5px solid transparent;
  border-left-color: red;
  border-right: none; 
}

ol.vertical i.icon-move {
    cursor: pointer; 
}
.other1 {
    border: medium none;
    margin-bottom: 20px !important;
    padding: 0 !important;
    width: 100%;
}
.other1 td {
    background-color: #ffffff;
    border-left-color: #dfdfdf;
    color: #444444;
}
.other1 td {
    font-weight: normal;
    padding: 4px 10px 4px 6px;
    vertical-align: middle;
}
</style>
{/literal}
<form method='post' id='frm_settings' action='index.php'>
<input type='hidden' name='action' value='fi_restrictaccess'/>
<input type='hidden' name='module' value='{$MODULE}'/>
<input type='hidden' name='option' value='save'/>
<h2>&nbsp;Restrict access link for other admin users</h2>
<table  border='1' cellspacing='0' cellpadding='0' class='other1 view' style="border: 1px solid #ccc !important;">
    <tr height="10">
    	<td style='text-align:right;' width='20%'></td>
		<td width='80%' style="color:red; font-weight: bold;">{$message}</td>
    </tr>
    <tr><td colspan="2" height="10"></td></tr>
    <tr>
        <td></td>
        <td style="text-align: left"><table cellpadding="0" cellspacing="0" border="0" class="OEPL_content_sub_table" width="100%">
        {$link_list_layout}
        </table></td>
    </tr>
    <tr><td colspan="2" height="10"></td></tr>
    <tr>
        <td></td>
        <td>
            <input type='submit' class='button primary' value='Save' onclick="return clicktosavebtn()" />
            <div id='response_text' style='color:red;'></div>
        </td>
    </tr>
    <tr><td colspan="2" height="10"></td></tr>
</table> 
</form>
{literal}
<script type="text/javascript">
$("ol.list_with_animation").sortable({
  group: 'list_with_animation',
  pullPlaceholder: false,
  isValidTarget: function  ($item, container) {
    if($item.is(".nothighlight"))
      return $item.parent("ol")[0] == container.el[0];
    else
      return true;
  },
  // animation on drop
  onDrop: function  ($item, container, _super) {
    var $clonedItem = $('<li/>').css({height: 0});
    $item.before($clonedItem);
    $clonedItem.animate({'height': $item.height()});

    $item.animate($clonedItem.position(), function  () {
      $clonedItem.detach();
      _super($item, container);
    });
  },

  // set $item relative to cursor position
  onDragStart: function ($item, container, _super) {
    var offset = $item.offset(),
        pointer = container.rootGroup.pointer;

    adjustment = {
      left: pointer.left - offset.left,
      top: pointer.top - offset.top
    };

    _super($item, container);
  },
  onDrag: function ($item, position) {
    $item.css({
      left: position.left - adjustment.left,
      top: position.top - adjustment.top
    });
  }
});
</script>
{/literal}
