<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/SugarView.php');

class ViewFI_Restrictaccess extends SugarView
{
    public function preDisplay()
    {
        global $current_user;

        if($current_user->id != '1') {
            sugar_die("Unauthorized access to administration.");
        }
    }

    public function display()
    {
        global $current_user, $app_strings, $sugar_config, $currentModule;

		$fi_module_name = 'FI_SuperuserAccess';   
		
		require_once('modules/Administration/Administration.php');
        $Administration = new Administration();
        $Administration->retrieveSettings($fi_module_name);     
        
        if (isset($_REQUEST['option'])) { 
     
            switch($_REQUEST['option']) {
                //SAVE SMS SETTING
                case "save":
                    ## START Only Admin User Allow for Save Settings
                    if($current_user->id != '1'){
                        sugar_die('Admin Only');
                        break;  
                    }
                    ## END Only Admin User Allow for Save Settings
                    
                    $jsonApps = base64_encode(serialize($_REQUEST['link_list']));
			        
			        $Administration->saveSetting($fi_module_name, 'Restrict', $jsonApps);
                    
                    $queryParams = array(
			        'module' => $currentModule,
			        'action' => 'fi_restrictaccess',
			        'xstatus' => 'Y',
			        'message' => 'Successfully Saved.'
			        );
			         
			        SugarApplication::redirect('index.php?' . http_build_query($queryParams));
			        exit;
                    
                    
                    break;
                
                default:
                    echo "";
            } 
        
        } else  {   // just draw the gateway settings panel        
        
        	
            //get the module links..
			require('modules/Administration/metadata/adminpaneldefs.php');
			global $admin_group_header;  ///variable defined in the file above.
			
			#echo '<pre>';
			#print_r($admin_group_header);
			#$GLOBALS['admin_access_control_links'];
			#echo '</pre>';
			
			$tab = array();
			$header_image = array();
			$url = array();
			$onclick = array();
			$label_tab = array();
			$id_tab = array();
			$description = array();
			$group = array(); $group_right = array();
			$sugar_smarty = new Sugar_Smarty();
			$values_3_tab = array();
			$admin_group_header_tab = array();
			$j=0;
			
			foreach ($admin_group_header as $key=>$values) {
			    $module_index = array_keys($values[3]);
			    $addedHeaderGroups = array();
			    foreach ($module_index as $mod_key=>$mod_val) {
			        if(
			        (!isset($addedHeaderGroups[$values[0]]))) {
			            $admin_group_header_tab[]=$values;
			            $group_header_value=get_form_header(translate($values[0],'Administration'),$values[1],$values[2]);
			        	$group[$j][0] = '<h3>' . translate($values[0],'Administration') . '</h3>';
						$group_right[$j][0] = translate($values[0],'Administration');
			        	$addedHeaderGroups[$values[0]] = 1;
			        	if (isset($values[4]))
			    	       $group[$j][1] = '' . translate($values[4]) . '';
			    	    else
			    	       $group[$j][2] = '';
			            $colnum=0;
			            $i=0;
			            $fix = array_keys($values[3]);
			            if(count($values[3])>1){
			
			                //////////////////
			                $tmp_array = $values[3];
			                $return_array = array();
			                foreach ($tmp_array as $mod => $value){
			                    $keys = array_keys($value);
			                    foreach ($keys as $key){
			                        $return_array[$key] = $value[$key];
			                    }
			                }
			                $values_3_tab[]= $return_array;
			                $mod = $return_array;
			            }
			           else {
			                $mod = $values[3][$fix[0]];
			    	        $values_3_tab[]= $mod;
			           }
			
			            foreach ($mod as $link_idx =>$admin_option) {
			                if(!empty($GLOBALS['admin_access_control_links']) && in_array($link_idx, $GLOBALS['admin_access_control_links'])){
			                    continue;
			                }
			                
			                $colnum+=1;
			                
			                $url[$j][$i] = $admin_option[3];
			                if(!empty($admin_option[5])) {
			                	$onclick[$j][$i] = $admin_option[5];
			                }
			                $label = translate($admin_option[1],'Administration');
			                if(!empty($admin_option['additional_label']))$label.= ' '. $admin_option['additional_label'];
			                if(!empty($admin_option[4])){
			                	$label = ' <font color="red">'. $label . '</font>';
			                }
			
			                $label_tab[$j][$i]= $label;
			                $id_tab[$j][$i] = $link_idx;
			                
			                $description[$j][$i]= translate($admin_option[2],'Administration');
			
			                if (($colnum % 2) == 0) {
			                    $tab[$j][$i]= ($colnum % 2);
			                }
			                else {
			                    $tab[$j][$i]= 10;
			                }
			                $i+=1;
			            }
			
			        	//if the loop above ends with an odd entry add a blank column.
			        	if (($colnum % 2) != 0) {
			        	    $tab[$j][$i]= 10;
			        	}
			        $j+=1;
			    }
			  }
			}

			#echo '<pre>';
			#print_r($group);
			#print_r($values_3_tab);
			#print_r($label_tab);
			#echo '</pre>';
			
			$link_list_arr =  array('upgrade_wizard', 'theme_settings', 'studio', 'moduleBuilder', 'configure_tabs', 'configure_group_tabs', 'rename_tabs', 'module_loader');
	        if(!empty($Administration->settings[$fi_module_name.'_Restrict'])){
	            $link_list_arr = unserialize(base64_decode($Administration->settings[$fi_module_name.'_Restrict']));
	        }
			
			$link_list_li = ''; $link_list_selected_li = '';
	        if(count($values_3_tab) > 0)
	        {
	        	foreach($values_3_tab as $key => $properties_s)
	            {
	            	$link_list_li .= '<li class="nothighlight">'.$group[$key][0].'</li>';
	            	$lp = 0;
	            	foreach($properties_s as $link_idx => $admin_option)
					{
						if(empty($label_tab[$key][$lp])){ $lp++; continue; } 
						if(in_array($link_idx, $link_list_arr))
	                    	$link_list_selected_li .= '<li>'.$group_right[$key][0].' - '.$label_tab[$key][$lp++].'<input type="hidden" value="'.$link_idx.'" /></li>';
		                else{
		                    $link_list_li .= '<li>'.$label_tab[$key][$lp++].'<input type="hidden" name="l_link" value="'.$link_idx.'" /></li>';
		                }
					} 
	            }
	        }
	        $link_list_layout = '
	        <tr>
	            <td style="padding-left:5px; text-align: center; padding-bottom: 5px;"><b>Admin link</b></Td>
	            <td style="padding-left:5px; text-align: center; padding-bottom: 5px;"><b>Hide link for other Admin users</b></td>
	            <td></td>
	        </tr>
	        <tr>
	            <td style="padding-left:5px;" valign="top" width="30%">
	                <ol class="list_with_animation vertical">
	                '.$link_list_li.'
	                </ol>
	            </td>
	            <td style="padding-right:5px;" valign="top" width="30%" >
	                <ol class="list_with_animation vertical" id="list_map_link">
	                '.$link_list_selected_li.'
	                </ol>
	            </td>
	            <td style="text-align:left; vertical-align:top;" width="40%">
	            </td>
	        </tr>    
	        <tr height="20">
	            <td></td>
	        </tr>';
	        $this->ss->assign('link_list_layout',$link_list_layout);
            
            //todo: redirect appropriately
            $this->ss->assign("RETURN_MODULE", "Administration");
            $this->ss->assign("RETURN_ACTION", "index");
        
            $this->ss->assign("MODULE", $currentModule);
			
			$message = '';
			if(!empty($_REQUEST['xstatus'])) $message = $_REQUEST['message'];
			$this->ss->assign("message", $message);
        
            if(preg_match( "/^6.*/", $sugar_version) ) {
                $this->ss->assign("IS_SUGAR_6", TRUE);
            }
            
            $this->ss->display('modules/'.$currentModule.'/tpls/fi_restrictaccess.tpl');
        } 
    }
    
}

?>