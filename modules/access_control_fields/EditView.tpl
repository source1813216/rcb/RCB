{**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 *}
{php}
global $current_user;
$this->_tpl_vars['current_theme'] = $current_user->getPreference('user_theme');
{/php}
{if !empty($FIELDS)}
    <link rel="stylesheet" type="text/css" href="modules/ModuleBuilder/tpls/ListEditor.css" />
    <h3 class="selectedModuleLabel">{sugar_translate label='LBL_FIELDS' module='access_control_fields'}: {$MODULE_SELECTED}</h3>
    <div class="fieldAccessSubDescMatrix">Double click on a cell to change value.</div>
    <input type='hidden' name='flc_module' value='{$FLC_MODULE}'> 
    <table  class='detail view' border='0' cellpadding=0 cellspacing = 1  width='100%'>
        {counter start=0 name='colCount' assign='colCount'}
        {foreach from=$FIELDS key="NAME" item="DEF"}

            {if ($colCount % 4 == 0 || $colCount == 0) }
                {if $colCount != 0}
                </tr>
            {/if}
            <tr>
            {/if}
            <td scope='row' class="fieldNameCustom" valign="center">{sugar_translate label=$DEF.label module=$LBL_MODULE}{if $DEF.required}<span class='customRequired'>*</span>{/if}
                {if count($DEF.fields) > 0}
                    {if $current_theme == "Suite7"}
                        <a id="d_{$NAME}_anchor" class='link' onclick='displayMoreDetailsSugarCE("d_{$NAME}")' href='javascript:void(0)' class="linkFieldNameInfo">
                        <button type="button" class="__web-inspector-hide-shortcut__"><img class="fieldAccessPlusMinusButton" src="themes/default/images/id-ff-add.png?v=FkOi8fcLbiYeHIGQHkliXA"></button></a>
                    {else}
                    <a id="d_{$NAME}_anchor" class='link' onclick='displayMoreDetails("d_{$NAME}")' href='javascript:void(0)' class="linkFieldNameInfo"><button type="button" class="btn buttonFieldNameInfo"><span class="glyphicon glyphicon-plus"></span></button></a>
                    {/if}
                    <div id='d_{$NAME}' style='display:none'>
                            {foreach from=$DEF.fields key='subField' item='subLabel'}
                                    {sugar_translate label=$subLabel module=$FLC_MODULE}<br><span class='fieldValue'>[{$subField}]</span><br>
                        {/foreach}
                    </div>
                {/if}
            </td>

            <td class="aclTypesFields" valign="middle">
                <div  style="display: none" id="{$DEF.key}">
                    <select name='flc_guid{$DEF.key}' class="flaGlobalDropdown" id = 'flc_guid{$DEF.key}'onblur="document.getElementById('{$DEF.key}link').innerHTML = this.options[this.selectedIndex].text; aclviewer.toggleDisplay('{$DEF.key}');" class="flc_dropdown" >
                        {if !empty($DEF.required)}
                            {html_options options=$OPTIONS_REQUIRED selected=$DEF.aclaccess }
                        {else}
                            {html_options options=$OPTIONS selected=$DEF.aclaccess }
                        {/if}

                    </select>
                </div>
                <div  id="{$DEF.key}link" class="fieldAccessCursor" style='margin-top:4px;' onclick="aclviewer.toggleDisplay('{$DEF.key}')">
                    <b> {if !empty($OPTIONS[$DEF.aclaccess])}
                        <span style="color:{$OPTIONS_COLOR[$DEF.aclaccess]}">{$OPTIONS[$DEF.aclaccess]}</span>
                    {else}
                        Not Defined
                        {/if} </b>
                </div>
            </td>
            {counter name='colCount'}
        {/foreach}
    </tr>	
</table>
{/if}

