<?php
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
class fields_acl_class {

    function accesible_views(&$field_acl_module_obj, $acl_event, $acl_arguments = null) {
        global $current_user;
        $reqRecord = isset($_REQUEST['record']) ? $_REQUEST['record'] : '';
        $reqAction = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
        $reqModule = isset($_REQUEST['module']) ? $_REQUEST['module'] : '';
        $reqTargetModule = isset($_REQUEST['target_module']) ? $_REQUEST['target_module'] : '';
        require_once 'modules/access_control_fields/acl_checkaccess.php';
        if (($reqAction == 'EditView' || $reqAction == 'SubpanelCreates') && (is_null($reqRecord) || empty($reqRecord))) {
            $acl_module_name = ($reqAction == 'SubpanelCreates') ? $reqTargetModule : $reqModule;
            $field_acl_module_obj = loadBean($acl_module_name);
            $acl_obj = $field_acl_module_obj->object_name;
            $user_id = $current_user->id;
            $acl_module = $field_acl_module_obj->module_dir;
            if ($field_acl_module_obj) {
                $is_owner = $field_acl_module_obj->isOwner($current_user->id);
            }
        } else {
            $acl_obj = $field_acl_module_obj->object_name;
            $user_id = $current_user->id;
            $acl_module = $field_acl_module_obj->module_dir;
            $is_owner = $field_acl_module_obj->isOwner($current_user->id);
        }
        limit_access_of_users($acl_module, $acl_obj, $user_id, $refresh = false);

        if (is_array($_SESSION['ACL'][$user_id][$acl_module]['fields']))
            foreach ($field_acl_module_obj->field_defs as $acl_field_name => $limit) {
                if(!$is_owner && $field_acl_module_obj->assigned_user_id == '' && $field_acl_module_obj->created_by == $current_user->id){
                    $is_owner = true;
                }
                if (array_key_exists($acl_field_name, $_SESSION['ACL'][$user_id][$acl_module]['fields'])) {
                    $field_acl_module_obj->field_defs[$acl_field_name]['acl'] = checkIfAccess($acl_field_name, $acl_module, $user_id, $is_owner);
                } else {
                    $field_acl_module_obj->field_defs[$acl_field_name]['acl'] = 4;
                }
            }
    }

}

?>
