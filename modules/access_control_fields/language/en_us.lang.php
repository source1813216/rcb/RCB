<?php
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
$mod_strings = array(
    'LBL_DESCRIPTION' => 'Description',
    'LBL_DELETED' => 'Deleted',
    'LBL_NAME' => 'Field',
    'LBL_CREATED_USER' => 'Created by User',
    'LBL_MODIFIED_USER' => 'Modified by User',
    'LBL_LIST_FORM_TITLE' => 'Fields List',
    'LBL_MODULE_NAME' => 'Fields',
    'LBL_MODULE_TITLE' => 'Fields',
    'LBL_HOMEPAGE_TITLE' => 'My Fields',
    'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
    'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
    'LBL_ID' => 'ID',
    'LBL_DATE_ENTERED' => 'Date Created',
    'LBL_DATE_MODIFIED' => 'Date Modified',
    'VALUE' => 'Field',
    'LBL_ACLACCESS' => 'Access',
    'LBL_ROLE' => 'Role',
    'LBL_DEFAULT' => 'Not Set',
    'LBL_READ_WRITE' => 'Read/Write',
    'LBL_READ_OWNER_WRITE' => 'Read/Owner Write',
    'LBL_READ_ONLY' => 'Read Only',
    'LBL_OWNER_READ_WRITE' => 'Owner Read/Owner Write',
    'LBL_ALLOW_NONE' => 'None',
    'LBL_FIELDS' => 'Fields Matrix',
    'LBL_MODIFIED' => 'Modified By',
    'LBL_MODIFIED_ID' => 'Modified By Id',
    'LBL_MODIFIED_NAME' => 'Modified By Name',
    'LBL_CREATED' => 'Created By',
    'LBL_CREATED_ID' => 'Created By Id',
    'LNK_NEW_RECORD' => 'Create Field Limit',
    'LNK_LIST' => 'Field Limits',
    'LBL_SEARCH_FORM_TITLE' => 'Search Fields',
    'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
    'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
    'LBL_ACCESS_CONTROL_SUBPANEL_TITLE' => 'Fields',
    'LBL_NEW_FORM_TITLE' => 'New Fields',
    'LBL_CATEGORY' => 'Module',
    'LBL_RESET_ACCESES' => 'Reset Accesses',
);
?>
