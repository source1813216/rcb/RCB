<?php
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
$dictionary['access_control_fields'] = array(
    'table' => 'access_control_fields',
    'audited' => true,
    'fields' => array(
        'category' =>
        array(
            'required' => '1',
            'name' => 'category',
            'vname' => 'LBL_CATEGORY',
            'type' => 'varchar',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => 0,
            'reportable' => true,
            'len' => 100,
        ),
        'aclaccess' =>
        array(
            'required' => false,
            'name' => 'aclaccess',
            'vname' => 'LBL_ACLACCESS',
            'type' => 'int',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => 0,
            'reportable' => true,
            'len' => '3',
            'disable_num_format' => '',
        ),
        'role_id' =>
        array(
            'required' => false,
            'name' => 'role_id',
            'vname' => 'LBL_ROLE',
            'type' => 'id',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'audited' => 0,
            'reportable' => 0,
            'len' => 36,
        ),
    ),
    'relationships' => array(
    ),
    'optimistic_lock' => true,
);
if (!class_exists('VardefManager')){
        require_once('include/SugarObjects/VardefManager.php');
}
VardefManager::createVardef('access_control_fields', 'access_control_fields', array('basic'));
