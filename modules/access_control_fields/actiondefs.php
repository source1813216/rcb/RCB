<?php
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
global $current_user;
$current_theme = $current_user->getPreference('user_theme');
$current_subTheme = $current_user->getPreference('subtheme');


if (!defined('ACL_READ_ONLY')) {
    define('ACL_READ_ONLY', 50);
    define('ACL_READ_WRITE', 99);
    define('ACL_OWNER_READ_WRITE', 40);
    define('ACL_READ_OWNER_WRITE', 60);
    if (!defined('ACL_ALLOW_NONE')) {
        define('ACL_ALLOW_NONE', -99);
        define('ACL_ALLOW_DEFAULT', 0);
    }
    define('ACL_FIELD_DEFAULT', 99);
}

$GLOBALS['aclFieldRestrictOptions'] = array(
    ACL_ALLOW_DEFAULT => 'LBL_DEFAULT',
    ACL_READ_WRITE => 'LBL_READ_WRITE',
    ACL_READ_OWNER_WRITE => 'LBL_READ_OWNER_WRITE',
    ACL_READ_ONLY => 'LBL_READ_ONLY',
    ACL_OWNER_READ_WRITE => 'LBL_OWNER_READ_WRITE',
    ACL_ALLOW_NONE => 'LBL_ALLOW_NONE',
);
if($current_theme == 'SuiteP' && $current_subTheme == 'Night'){
    $GLOBALS['aclFieldRestrictOptionsColors'] = array(
        ACL_ALLOW_DEFAULT => '#e6a06d', //Orange
        ACL_READ_WRITE => '#A5D6A7', //Green
        ACL_READ_OWNER_WRITE => '#CCCC00', //Yellow
        ACL_READ_ONLY => '#c5cae9 ', //Teal
        ACL_OWNER_READ_WRITE => '#B2EBF2', //Blue
        ACL_ALLOW_NONE => '#FF0000', //Red
    );
}else{
    $GLOBALS['aclFieldRestrictOptionsColors'] = array(
    ACL_ALLOW_DEFAULT => '#f08377', //Orange
    ACL_READ_WRITE => '	#008000', //Green
    ACL_READ_OWNER_WRITE => '#CCCC00', //Yellow
    ACL_READ_ONLY => '#31708f', //Teal
    ACL_OWNER_READ_WRITE => '#0000FF', //Blue
    ACL_ALLOW_NONE => '#FF0000', //Red
);
}
?>