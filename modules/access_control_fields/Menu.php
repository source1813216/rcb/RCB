<?php
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

global $mod_strings, $app_strings, $sugar_config, $current_user;

if (is_admin($current_user)) {
    $module_menu[] = Array("index.php?module=access_control_fields&action=EditAclFields&return_module=access_control_fields&return_action=DetailView", $mod_strings['LNK_NEW_RECORD'], "CreateTeams");
    $module_menu[] = Array("index.php?module=access_control_fields&action=index&return_module=access_control_fields&return_action=DetailView", $mod_strings['LNK_LIST'], "Teams");
}
