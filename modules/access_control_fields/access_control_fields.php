<?PHP
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
require_once('modules/access_control_fields/actiondefs.php');

class access_control_fields extends ACLAction {

    var $new_schema = true;
    var $module_dir = 'access_control_fields';
    var $object_name = 'access_control_fields';
    var $table_name = 'access_control_fields';
    var $importable = false;
    var $acl_id;
    var $name;
    var $date_entered;
    var $date_modified;
    var $modified_user_id;
    var $modified_by_name;
    var $created_by;
    var $created_by_name;
    var $description;
    var $deleted;
    var $created_by_link;
    var $modified_user_link;
    var $assigned_user_id;
    var $assigned_user_name;
    var $assigned_user_link;
    var $category;
    var $aclaccess;
    var $team_id_c;
    var $role;

    function access_control_fields() {
        parent::ACLAction();
        $this->disable_row_level_security = true;
    }

    function bean_implements($interface) {
        switch ($interface) {
            case 'ACL': return true;
        }
        return false;
    }

    static function fetchFieldsByModule($field_acl_module, $filed_acl_obj = false) {
        static $field_acl_exclude_fields = array('deleted','date_entered');
        static $field_acl_available_fields = array();
        if (!isset($field_acl_available_fields[$field_acl_module])) {
            if (empty($GLOBALS['dictionary'][$filed_acl_obj]['fields'])) {
                $field_acl_class = $GLOBALS['beanList'][$field_acl_module];
                require_once($GLOBALS['beanFiles'][$field_acl_class]);
                $field_acl_mod = new $field_acl_class();
                if (!$field_acl_mod->acl_fields)
                    return array();
                $field_acl_field_defs = $field_acl_mod->field_defs;
            }else {
                $field_acl_field_defs = $GLOBALS['dictionary'][$filed_acl_obj]['fields'];
                if (isset($GLOBALS['dictionary'][$filed_acl_obj]['access_control_fields']) && $GLOBALS['dictionary'][$filed_acl_obj] === false) {
                    return array();
                }
            }

            $availableFields = array();
            foreach ($field_acl_field_defs as $field => $def) {

                if ((!empty($def['source']) && $def['source'] == 'custom_fields') || !empty($def['group']) || (empty($def['hideacl']) && !empty($def['type']) && !in_array($field, $field_acl_exclude_fields) &&
                        ((empty($def['source']) && $def['type'] != 'id' && (empty($def['dbType']) || ($def['dbType'] != 'id' ))
                        ) || !empty($def['link']) || $def['type'] == 'relate')
                        )) {
                    if(in_array($field, $field_acl_exclude_fields))
                            continue;
                    if (empty($def['vname']))
                        $def['vname'] = '';
                    $fkey = (!empty($def['group'])) ? $def['group'] : $field;
                    $label = (!empty($field_acl_field_defs[$fkey]['vname'])) ? $field_acl_field_defs[$fkey]['vname'] : $def['vname'];
                    $fkey = strtolower($fkey);
                    $field = strtolower($field);
                    $required = !empty($def['required']);
                    if ($field == 'name') {
                        $required = true;
                    }
                    if (empty($availableFields[$fkey])) {
                        $availableFields[$fkey] = array('id' => $fkey, 'required' => $required, 'key' => $fkey, 'name' => $field, 'label' => $label, 'category' => $field_acl_module, 'role_id' => '', 'aclaccess' => ACL_ALLOW_DEFAULT, 'fields' => array($field => $label));
                    } else {
                        if (!empty($required)) {
                            $availableFields[$fkey]['required'] = 1;
                        }
                        $availableFields[$fkey]['fields'][strtolower($field)] = $label;
                    }
                }
            }
            $field_acl_available_fields[$field_acl_module] = $availableFields;
        }
        return $field_acl_available_fields[$field_acl_module];
    }

    function fetchAllFields($field_acl_module, $user_id = '', $field_acl_role_id = '') {
        static $userFields = array();
        $access_control_fields = access_control_fields::fetchFieldsByModule($field_acl_module, false);
        if (!empty($field_acl_role_id)) {
            $query = "SELECT  af.id, af.name, af.category, af.role_id, af.aclaccess FROM access_control_fields af ";
            if (!empty($user_id)) {
                $query .= " INNER JOIN acl_roles_users aru ON aru.user_id = '$user_id' AND aru.deleted=0
                            INNER JOIN acl_roles ar ON aru.role_id = ar.id AND ar.id = af.role_id AND ar.deleted = 0";
            }

            $query .= " WHERE af.deleted = 0 ";
            $query .= " AND af.role_id='$field_acl_role_id' ";
            $query .= " AND af.category='$field_acl_module'";
            $result = $GLOBALS['db']->query($query);
            while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
                if (!empty($access_control_fields[$row['name']]) && ($row['aclaccess'] < $access_control_fields[$row['name']]['aclaccess'] || $access_control_fields[$row['name']]['aclaccess'] == 0)) {
                    $row['key'] = $row['name'];
                    $row['label'] = $access_control_fields[$row['name']]['label'];
                    $row['fields'] = $access_control_fields[$row['name']]['fields'];
                    if (isset($access_control_fields[$row['name']]['required'])) {
                        $row['required'] = $access_control_fields[$row['name']]['required'];
                    }
                    $access_control_fields[$row['name']] = $row;
                }
            }
        }

        ksort($access_control_fields);
        return $access_control_fields;
    }

    function fetchRoleWiseFields($field_acl_role_id) {
        $query = "SELECT  af.id, af.name, af.category, af.role_id, af.aclaccess FROM access_control_fields af ";
        $query .= " WHERE af.deleted = 0 ";
        $query .= " AND af.role_id='$field_acl_role_id' ";
        $result = $GLOBALS['db']->query($query);
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $access_control_fields[$row['id']] = $row;
        }
        return $access_control_fields;
    }

    function storeAccessControlFields($field_acl_module, $field_acl_role_id, $field_acl_id, $field_acl_access) {
        $field_acl_obj = new access_control_fields();
        $acl_id = md5($field_acl_module . $field_acl_role_id . $field_acl_id);
        if (!$field_acl_obj->retrieve($acl_id)) {
            if (empty($field_acl_access))
                return false;
            $field_acl_obj->id = $acl_id;
            $field_acl_obj->new_with_id = true;
        }

        $field_acl_obj->aclaccess = $field_acl_access;
        $field_acl_obj->category = $field_acl_module;
        $field_acl_obj->name = $field_acl_id;
        $field_acl_obj->role_id = $field_acl_role_id;
        $id = $field_acl_obj->save();
        return $id;
    }

}

?>
