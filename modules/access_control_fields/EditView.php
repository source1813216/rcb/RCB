<?php
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
class access_control_fieldsshowEditView {

    function showFieldACLView($field_acl_module, $role_id) {
        require_once 'modules/access_control_fields/access_control_fields.php';
        $selectedModule = $GLOBALS['app_list_strings']['moduleList'][$field_acl_module]; //20-02-2018
        $access_control_fields = access_control_fields::fetchAllFields($field_acl_module, '', $role_id);
        $sugar_smarty_obj = new Sugar_Smarty();
        if (substr($field_acl_module, 0, 2) == 'KB') {
            $sugar_smarty_obj->assign('LBL_MODULE', 'KBDocuments');
        } else {
            $sugar_smarty_obj->assign('LBL_MODULE', $field_acl_module);
        }
        $sugar_smarty_obj->assign('MOD', $GLOBALS['mod_strings']);
        $sugar_smarty_obj->assign('APP', $GLOBALS['app_strings']);
        $sugar_smarty_obj->assign('FLC_MODULE', $field_acl_module);
        $sugar_smarty_obj->assign('MODULE_SELECTED', $selectedModule); //20-02-2018
        $sugar_smarty_obj->assign('APP_LIST', $GLOBALS['app_list_strings']);
        $sugar_smarty_obj->assign('FIELDS', $access_control_fields);
        foreach ($GLOBALS['aclFieldRestrictOptions'] as $acl_key => $acl_option) {
            $GLOBALS['aclFieldRestrictOptions'][$acl_key] = translate($acl_option, 'access_control_fields');
        }
        $sugar_smarty_obj->assign('OPTIONS', $GLOBALS['aclFieldRestrictOptions']);
        $sugar_smarty_obj->assign('OPTIONS_COLOR', $GLOBALS['aclFieldRestrictOptionsColors']);
        $field_acl_requested_options = $GLOBALS['aclFieldRestrictOptions'];
        unset($field_acl_requested_options[-99]);
        $sugar_smarty_obj->assign('OPTIONS_REQUIRED', $field_acl_requested_options);
        return $sugar_smarty_obj->fetch('modules/access_control_fields/EditView.tpl');
    }

}

?>