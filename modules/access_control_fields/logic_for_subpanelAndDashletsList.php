<?php

/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
class fields_acl_sub_dash_class {

    function accesible_views_sub_dash(&$subpanel_obj, $acl_event, $acl_arguments = null) {
        global $current_user, $acl_access_fla;
        require_once 'modules/access_control_fields/acl_checkaccess.php';
        require_once('custom/modules/Administration/field_access_control_assign_view.php');
        $disabled_plugin_loginslider = checkPluginStatus_FAC();
        $target_module = isset($_REQUEST['target_module']) ? $_REQUEST['target_module'] : '';
        $reqModule = isset($_REQUEST['module']) ? $_REQUEST['module'] : '';
        $reqAction = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
        $reqRecord = isset($_REQUEST['record']) ? $_REQUEST['record'] : '';
        if (($reqAction == 'EditView' || $reqAction == 'SubpanelCreates') && (is_null($reqRecord) || empty($reqRecord))) {
            $acl_module_name = ($reqAction == 'SubpanelCreates') ? $target_module : $reqModule;
            $subpanel_obj = loadBean($acl_module_name);
            $obj = $subpanel_obj->object_name;
            $user_id = $current_user->id;
            $acl_module = $subpanel_obj->module_dir;
            $is_owner = $subpanel_obj->isOwner($current_user->id);
        } else {
            $obj = $subpanel_obj->object_name;
            $user_id = $current_user->id;
            $acl_module = $subpanel_obj->module_dir;
            $is_owner = $subpanel_obj->isOwner($current_user->id);
        }
        limit_access_of_users($acl_module, $obj, $user_id, $refresh = false);
        $acl_field_logic_array = array('first_name', 'last_name');
        if (is_array($_SESSION['ACL'][$user_id][$acl_module]['fields'])) {
            foreach ($subpanel_obj->field_defs as $fieldname => $limit) {
                if (array_key_exists($fieldname, $_SESSION['ACL'][$user_id][$acl_module]['fields'])) {
                    if (!$disabled_plugin_loginslider) {
                        if ($subpanel_obj->assigned_user_id == '' && $subpanel_obj->created_by == $current_user->id) {
                            $is_owner = true;
                        }
                        $subpanel_obj->field_defs[$fieldname]['acl_access_code'] = $_SESSION['ACL'][$user_id][$acl_module]['fields'][$fieldname];
                    }
                    $acl_access_code = checkIfAccess($fieldname, $acl_module, $user_id, $is_owner);
                    $subpanel_obj->field_defs[$fieldname]['acl'] = $acl_access_code;
                    if ($acl_access_code == 0 && !$disabled_plugin_loginslider) {
                        if (in_array($fieldname, $acl_field_logic_array)) {
                            $subpanel_obj->$fieldname = '';
                        } else {
                            $fieldnameLower = strtolower($fieldname);
                            $acl_access_fla['ACL'][$user_id][$subpanel_obj->id][$fieldnameLower] = 'No Access';
                        }
                    }
                } else {
                    $subpanel_obj->field_defs[$fieldname]['acl'] = 4;
                }
            }
        }
    }

}

?>
