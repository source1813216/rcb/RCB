<?php
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
function limit_access_of_users($acl_category, $acl_obj, $acl_user_id, $refresh = false) {
    global $sugar_config;
    require_once 'modules/access_control_fields/access_control_fields.php';
    if (!$refresh && isset($_SESSION['ACL'][$acl_user_id][$acl_category]['fields']))
        return $_SESSION['ACL'][$acl_user_id][$acl_category]['fields'];
    if (empty($_SESSION['ACL'][$acl_user_id])) {
        ACLAction::getUserActions($acl_user_id);
    }
    $acl_query = "SELECT  af.name, af.aclaccess FROM access_control_fields af ";
    $acl_query .= " INNER JOIN acl_roles_users aru ON aru.user_id = '$acl_user_id' AND aru.deleted=0
                        INNER JOIN acl_roles ar ON aru.role_id = ar.id AND ar.id = af.role_id AND ar.deleted = 0";
    $acl_query .= " WHERE af.deleted = 0 ";
    $acl_query .= " AND af.category='$acl_category'";
    if(isset($sugar_config['suitecrm_version'])){
        $acl_query .= " UNION SELECT af.name, af.aclaccess FROM access_control_fields af ";
        $acl_query .= " INNER JOIN securitygroups_users aru ON aru.user_id = '$acl_user_id' AND aru.deleted = 0
                            INNER JOIN securitygroups_acl_roles ar ON aru.securitygroup_id = ar.securitygroup_id AND ar.role_id = af.role_id AND ar.deleted = 0";
        $acl_query .= " WHERE af.deleted = 0 ";
        $acl_query .= " AND af.category='$acl_category'";
    }
    $result = $GLOBALS['db']->query($acl_query);
    if (!is_null($acl_category)) {
        $allFields = access_control_fields::fetchFieldsByModule($acl_category, $acl_obj);
    }
    $_SESSION['ACL'][$acl_user_id][$acl_category]['fields'] = array();
    while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
        if ($row['aclaccess'] != 0 && (empty($_SESSION['ACL'][$acl_user_id][$acl_category]['fields'][$row['name']]) || $_SESSION['ACL'][$acl_user_id][$acl_category]['fields'][$row['name']] > $row['aclaccess'])) {
            $_SESSION['ACL'][$acl_user_id][$acl_category]['fields'][$row['name']] = $row['aclaccess'];
            if (!empty($allFields[$row['name']])) {
                foreach ($allFields[$row['name']]['fields'] as $acl_field => $label) {
                    $_SESSION['ACL'][$acl_user_id][$acl_category]['fields'][strtolower($acl_field)] = $row['aclaccess'];
                }
            }
        }
    }
}

/**
 * checkIfAccess
 * IF 0 - has no access
 * IF 1 - Can read
 * IF 2 - Can write
 * IF 4 - Can do both
 * By biztechcs
 */
function checkIfAccess($acl_field, $acl_module, $acl_user_id, $is_owner) {
    require_once 'modules/access_control_fields/actiondefs.php';
    require_once('custom/modules/Administration/field_access_control_assign_view.php');
    $is_admin = null;
    $disabled_field_access_control = checkPluginStatus_FAC();

    if (is_null($is_admin)) {
        $is_admin = is_admin($GLOBALS['current_user']);
        if (!$is_admin) {
            $is_admin = $GLOBALS['current_user']->isAdminForModule($acl_module);
        }
    }
    if ($is_admin || $disabled_field_access_control) {
        return 4;
    }
    if (!isset($_SESSION['ACL'][$acl_user_id][$acl_module]['fields'][$acl_field])) {
        return 4;
    }
    $acl_access = $_SESSION['ACL'][$acl_user_id][$acl_module]['fields'][$acl_field];

    if ($acl_access == ACL_READ_WRITE || ($is_owner && ($acl_access == ACL_READ_OWNER_WRITE || $acl_access == ACL_OWNER_READ_WRITE))) {
        return 4;
    } elseif ($acl_access == ACL_READ_ONLY || $acl_access == ACL_READ_OWNER_WRITE) {
        return 1;
    }
    return 0;
}
