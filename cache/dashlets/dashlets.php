<?php
// created: 2022-11-30 11:13:20
$dashletsFiles = array (
  'MyAccountsDashlet' => 
  array (
    'file' => 'modules/Accounts/Dashlets/MyAccountsDashlet/MyAccountsDashlet.php',
    'class' => 'MyAccountsDashlet',
    'meta' => 'modules/Accounts/Dashlets/MyAccountsDashlet/MyAccountsDashlet.meta.php',
    'module' => 'Accounts',
  ),
  'AM_ProjectTemplatesDashlet' => 
  array (
    'file' => 'modules/AM_ProjectTemplates/Dashlets/AM_ProjectTemplatesDashlet/AM_ProjectTemplatesDashlet.php',
    'class' => 'AM_ProjectTemplatesDashlet',
    'meta' => 'modules/AM_ProjectTemplates/Dashlets/AM_ProjectTemplatesDashlet/AM_ProjectTemplatesDashlet.meta.php',
    'module' => 'AM_ProjectTemplates',
  ),
  'AM_TaskTemplatesDashlet' => 
  array (
    'file' => 'modules/AM_TaskTemplates/Dashlets/AM_TaskTemplatesDashlet/AM_TaskTemplatesDashlet.php',
    'class' => 'AM_TaskTemplatesDashlet',
    'meta' => 'modules/AM_TaskTemplates/Dashlets/AM_TaskTemplatesDashlet/AM_TaskTemplatesDashlet.meta.php',
    'module' => 'AM_TaskTemplates',
  ),
  'AOK_KnowledgeBaseDashlet' => 
  array (
    'file' => 'modules/AOK_KnowledgeBase/Dashlets/AOK_KnowledgeBaseDashlet/AOK_KnowledgeBaseDashlet.php',
    'class' => 'AOK_KnowledgeBaseDashlet',
    'meta' => 'modules/AOK_KnowledgeBase/Dashlets/AOK_KnowledgeBaseDashlet/AOK_KnowledgeBaseDashlet.meta.php',
    'module' => 'AOK_KnowledgeBase',
  ),
  'AOK_Knowledge_Base_CategoriesDashlet' => 
  array (
    'file' => 'modules/AOK_Knowledge_Base_Categories/Dashlets/AOK_Knowledge_Base_CategoriesDashlet/AOK_Knowledge_Base_CategoriesDashlet.php',
    'class' => 'AOK_Knowledge_Base_CategoriesDashlet',
    'meta' => 'modules/AOK_Knowledge_Base_Categories/Dashlets/AOK_Knowledge_Base_CategoriesDashlet/AOK_Knowledge_Base_CategoriesDashlet.meta.php',
    'module' => 'AOK_Knowledge_Base_Categories',
  ),
  'AORReportsDashlet' => 
  array (
    'file' => 'modules/AOR_Reports/Dashlets/AORReportsDashlet/AORReportsDashlet.php',
    'class' => 'AORReportsDashlet',
    'meta' => 'modules/AOR_Reports/Dashlets/AORReportsDashlet/AORReportsDashlet.meta.php',
    'module' => 'AOR_Reports',
  ),
  'AOS_ContractsDashlet' => 
  array (
    'file' => 'modules/AOS_Contracts/Dashlets/AOS_ContractsDashlet/AOS_ContractsDashlet.php',
    'class' => 'AOS_ContractsDashlet',
    'meta' => 'modules/AOS_Contracts/Dashlets/AOS_ContractsDashlet/AOS_ContractsDashlet.meta.php',
    'module' => 'AOS_Contracts',
  ),
  'AOS_InvoicesDashlet' => 
  array (
    'file' => 'modules/AOS_Invoices/Dashlets/AOS_InvoicesDashlet/AOS_InvoicesDashlet.php',
    'class' => 'AOS_InvoicesDashlet',
    'meta' => 'modules/AOS_Invoices/Dashlets/AOS_InvoicesDashlet/AOS_InvoicesDashlet.meta.php',
    'module' => 'AOS_Invoices',
  ),
  'AOS_PDF_TemplatesDashlet' => 
  array (
    'file' => 'modules/AOS_PDF_Templates/Dashlets/AOS_PDF_TemplatesDashlet/AOS_PDF_TemplatesDashlet.php',
    'class' => 'AOS_PDF_TemplatesDashlet',
    'meta' => 'modules/AOS_PDF_Templates/Dashlets/AOS_PDF_TemplatesDashlet/AOS_PDF_TemplatesDashlet.meta.php',
    'module' => 'AOS_PDF_Templates',
  ),
  'AOS_ProductsDashlet' => 
  array (
    'file' => 'modules/AOS_Products/Dashlets/AOS_ProductsDashlet/AOS_ProductsDashlet.php',
    'class' => 'AOS_ProductsDashlet',
    'meta' => 'modules/AOS_Products/Dashlets/AOS_ProductsDashlet/AOS_ProductsDashlet.meta.php',
    'module' => 'AOS_Products',
  ),
  'AOS_Product_CategoriesDashlet' => 
  array (
    'file' => 'modules/AOS_Product_Categories/Dashlets/AOS_Product_CategoriesDashlet/AOS_Product_CategoriesDashlet.php',
    'class' => 'AOS_Product_CategoriesDashlet',
    'meta' => 'modules/AOS_Product_Categories/Dashlets/AOS_Product_CategoriesDashlet/AOS_Product_CategoriesDashlet.meta.php',
    'module' => 'AOS_Product_Categories',
  ),
  'AOS_QuotesDashlet' => 
  array (
    'file' => 'modules/AOS_Quotes/Dashlets/AOS_QuotesDashlet/AOS_QuotesDashlet.php',
    'class' => 'AOS_QuotesDashlet',
    'meta' => 'modules/AOS_Quotes/Dashlets/AOS_QuotesDashlet/AOS_QuotesDashlet.meta.php',
    'module' => 'AOS_Quotes',
  ),
  'AOW_ProcessedDashlet' => 
  array (
    'file' => 'modules/AOW_Processed/Dashlets/AOW_ProcessedDashlet/AOW_ProcessedDashlet.php',
    'class' => 'AOW_ProcessedDashlet',
    'meta' => 'modules/AOW_Processed/Dashlets/AOW_ProcessedDashlet/AOW_ProcessedDashlet.meta.php',
    'module' => 'AOW_Processed',
  ),
  'AOW_WorkFlowDashlet' => 
  array (
    'file' => 'modules/AOW_WorkFlow/Dashlets/AOW_WorkFlowDashlet/AOW_WorkFlowDashlet.php',
    'class' => 'AOW_WorkFlowDashlet',
    'meta' => 'modules/AOW_WorkFlow/Dashlets/AOW_WorkFlowDashlet/AOW_WorkFlowDashlet.meta.php',
    'module' => 'AOW_WorkFlow',
  ),
  'MyBugsDashlet' => 
  array (
    'file' => 'modules/Bugs/Dashlets/MyBugsDashlet/MyBugsDashlet.php',
    'class' => 'MyBugsDashlet',
    'meta' => 'modules/Bugs/Dashlets/MyBugsDashlet/MyBugsDashlet.meta.php',
    'module' => 'Bugs',
  ),
  'CalendarDashlet' => 
  array (
    'file' => 'modules/Calendar/Dashlets/CalendarDashlet/CalendarDashlet.php',
    'class' => 'CalendarDashlet',
    'meta' => 'modules/Calendar/Dashlets/CalendarDashlet/CalendarDashlet.meta.php',
    'module' => 'Calendar',
  ),
  'MyCallsDashlet' => 
  array (
    'file' => 'modules/Calls/Dashlets/MyCallsDashlet/MyCallsDashlet.php',
    'class' => 'MyCallsDashlet',
    'meta' => 'modules/Calls/Dashlets/MyCallsDashlet/MyCallsDashlet.meta.php',
    'module' => 'Calls',
  ),
  'TopCampaignsDashlet' => 
  array (
    'file' => 'modules/Campaigns/Dashlets/TopCampaignsDashlet/TopCampaignsDashlet.php',
    'class' => 'TopCampaignsDashlet',
    'meta' => 'modules/Campaigns/Dashlets/TopCampaignsDashlet/TopCampaignsDashlet.meta.php',
    'module' => 'Campaigns',
  ),
  'MyCasesDashlet' => 
  array (
    'file' => 'modules/Cases/Dashlets/MyCasesDashlet/MyCasesDashlet.php',
    'class' => 'MyCasesDashlet',
    'meta' => 'modules/Cases/Dashlets/MyCasesDashlet/MyCasesDashlet.meta.php',
    'module' => 'Cases',
  ),
  'CampaignROIChartDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/CampaignROIChartDashlet/CampaignROIChartDashlet.php',
    'class' => 'CampaignROIChartDashlet',
    'meta' => 'modules/Charts/Dashlets/CampaignROIChartDashlet/CampaignROIChartDashlet.meta.php',
    'module' => 'Campaigns',
  ),
  'MyPipelineBySalesStageDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/MyPipelineBySalesStageDashlet/MyPipelineBySalesStageDashlet.php',
    'class' => 'MyPipelineBySalesStageDashlet',
    'meta' => 'modules/Charts/Dashlets/MyPipelineBySalesStageDashlet/MyPipelineBySalesStageDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OpportunitiesByLeadSourceByOutcomeDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceByOutcomeDashlet/OpportunitiesByLeadSourceByOutcomeDashlet.php',
    'class' => 'OpportunitiesByLeadSourceByOutcomeDashlet',
    'meta' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceByOutcomeDashlet/OpportunitiesByLeadSourceByOutcomeDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OpportunitiesByLeadSourceDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceDashlet/OpportunitiesByLeadSourceDashlet.php',
    'class' => 'OpportunitiesByLeadSourceDashlet',
    'meta' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceDashlet/OpportunitiesByLeadSourceDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OutcomeByMonthDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/OutcomeByMonthDashlet/OutcomeByMonthDashlet.php',
    'class' => 'OutcomeByMonthDashlet',
    'meta' => 'modules/Charts/Dashlets/OutcomeByMonthDashlet/OutcomeByMonthDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'PipelineBySalesStageDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/PipelineBySalesStageDashlet/PipelineBySalesStageDashlet.php',
    'class' => 'PipelineBySalesStageDashlet',
    'meta' => 'modules/Charts/Dashlets/PipelineBySalesStageDashlet/PipelineBySalesStageDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'MyContactsDashlet' => 
  array (
    'file' => 'modules/Contacts/Dashlets/MyContactsDashlet/MyContactsDashlet.php',
    'class' => 'MyContactsDashlet',
    'meta' => 'modules/Contacts/Dashlets/MyContactsDashlet/MyContactsDashlet.meta.php',
    'module' => 'Contacts',
  ),
  'MyDocumentsDashlet' => 
  array (
    'file' => 'modules/Documents/Dashlets/MyDocumentsDashlet/MyDocumentsDashlet.php',
    'class' => 'MyDocumentsDashlet',
    'meta' => 'modules/Documents/Dashlets/MyDocumentsDashlet/MyDocumentsDashlet.meta.php',
    'module' => 'Documents',
  ),
  'EAP_Event_ApprovalDashlet' => 
  array (
    'file' => 'modules/EAP_Event_Approval/Dashlets/EAP_Event_ApprovalDashlet/EAP_Event_ApprovalDashlet.php',
    'class' => 'EAP_Event_ApprovalDashlet',
    'meta' => 'modules/EAP_Event_Approval/Dashlets/EAP_Event_ApprovalDashlet/EAP_Event_ApprovalDashlet.meta.php',
    'module' => 'EAP_Event_Approval',
  ),
  'EDP_EndorsementsDashlet' => 
  array (
    'file' => 'modules/EDP_Endorsements/Dashlets/EDP_EndorsementsDashlet/EDP_EndorsementsDashlet.php',
    'class' => 'EDP_EndorsementsDashlet',
    'meta' => 'modules/EDP_Endorsements/Dashlets/EDP_EndorsementsDashlet/EDP_EndorsementsDashlet.meta.php',
    'module' => 'EDP_Endorsements',
  ),
  'EDP_Event_DocumentsDashlet' => 
  array (
    'file' => 'modules/EDP_Event_Documents/Dashlets/EDP_Event_DocumentsDashlet/EDP_Event_DocumentsDashlet.php',
    'class' => 'EDP_Event_DocumentsDashlet',
    'meta' => 'modules/EDP_Event_Documents/Dashlets/EDP_Event_DocumentsDashlet/EDP_Event_DocumentsDashlet.meta.php',
    'module' => 'EDP_Event_Documents',
  ),
  'MyEmailsDashlet' => 
  array (
    'file' => 'modules/Emails/Dashlets/MyEmailsDashlet/MyEmailsDashlet.php',
    'class' => 'MyEmailsDashlet',
    'meta' => 'modules/Emails/Dashlets/MyEmailsDashlet/MyEmailsDashlet.meta.php',
    'module' => 'Emails',
  ),
  'FavoritesDashlet' => 
  array (
    'file' => 'modules/Favorites/Dashlets/Favorites/FavoritesDashlet.php',
    'class' => 'FavoritesDashlet',
  ),
  'FP_eventsDashlet' => 
  array (
    'file' => 'modules/FP_events/Dashlets/FP_eventsDashlet/FP_eventsDashlet.php',
    'class' => 'FP_eventsDashlet',
    'meta' => 'modules/FP_events/Dashlets/FP_eventsDashlet/FP_eventsDashlet.meta.php',
    'module' => 'FP_events',
  ),
  'FP_Event_LocationsDashlet' => 
  array (
    'file' => 'modules/FP_Event_Locations/Dashlets/FP_Event_LocationsDashlet/FP_Event_LocationsDashlet.php',
    'class' => 'FP_Event_LocationsDashlet',
    'meta' => 'modules/FP_Event_Locations/Dashlets/FP_Event_LocationsDashlet/FP_Event_LocationsDashlet.meta.php',
    'module' => 'FP_Event_Locations',
  ),
  'ChartsDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/ChartsDashlet/ChartsDashlet.php',
    'class' => 'ChartsDashlet',
    'meta' => 'modules/Home/Dashlets/ChartsDashlet/ChartsDashlet.meta.php',
  ),
  'iFrameDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/iFrameDashlet/iFrameDashlet.php',
    'class' => 'iFrameDashlet',
    'meta' => 'modules/Home/Dashlets/iFrameDashlet/iFrameDashlet.meta.php',
    'module' => 'Home',
  ),
  'InvadersDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.php',
    'class' => 'InvadersDashlet',
    'meta' => 'modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.meta.php',
    'icon' => 'modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.icon.jpg',
  ),
  'JotPadDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/JotPadDashlet/JotPadDashlet.php',
    'class' => 'JotPadDashlet',
    'meta' => 'modules/Home/Dashlets/JotPadDashlet/JotPadDashlet.meta.php',
  ),
  'RSSDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/RSSDashlet/RSSDashlet.php',
    'class' => 'RSSDashlet',
    'meta' => 'modules/Home/Dashlets/RSSDashlet/RSSDashlet.meta.php',
    'icon' => 'modules/Home/Dashlets/RSSDashlet/RSSDashlet.icon.jpg',
  ),
  'SugarNewsDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/SugarNewsDashlet/SugarNewsDashlet.php',
    'class' => 'SugarNewsDashlet',
    'meta' => 'modules/Home/Dashlets/SugarNewsDashlet/SugarNewsDashlet.meta.php',
    'module' => 'Home',
  ),
  'IEP_Initial_EngagementDashlet' => 
  array (
    'file' => 'modules/IEP_Initial_Engagement/Dashlets/IEP_Initial_EngagementDashlet/IEP_Initial_EngagementDashlet.php',
    'class' => 'IEP_Initial_EngagementDashlet',
    'meta' => 'modules/IEP_Initial_Engagement/Dashlets/IEP_Initial_EngagementDashlet/IEP_Initial_EngagementDashlet.meta.php',
    'module' => 'IEP_Initial_Engagement',
  ),
  'jjwg_AreasDashlet' => 
  array (
    'file' => 'modules/jjwg_Areas/Dashlets/jjwg_AreasDashlet/jjwg_AreasDashlet.php',
    'class' => 'jjwg_AreasDashlet',
    'meta' => 'modules/jjwg_Areas/Dashlets/jjwg_AreasDashlet/jjwg_AreasDashlet.meta.php',
    'module' => 'jjwg_Areas',
  ),
  'jjwg_MapsDashlet' => 
  array (
    'file' => 'modules/jjwg_Maps/Dashlets/jjwg_MapsDashlet/jjwg_MapsDashlet.php',
    'class' => 'jjwg_MapsDashlet',
    'meta' => 'modules/jjwg_Maps/Dashlets/jjwg_MapsDashlet/jjwg_MapsDashlet.meta.php',
    'module' => 'jjwg_Maps',
  ),
  'jjwg_MarkersDashlet' => 
  array (
    'file' => 'modules/jjwg_Markers/Dashlets/jjwg_MarkersDashlet/jjwg_MarkersDashlet.php',
    'class' => 'jjwg_MarkersDashlet',
    'meta' => 'modules/jjwg_Markers/Dashlets/jjwg_MarkersDashlet/jjwg_MarkersDashlet.meta.php',
    'module' => 'jjwg_Markers',
  ),
  'MyLeadsDashlet' => 
  array (
    'file' => 'modules/Leads/Dashlets/MyLeadsDashlet/MyLeadsDashlet.php',
    'class' => 'MyLeadsDashlet',
    'meta' => 'modules/Leads/Dashlets/MyLeadsDashlet/MyLeadsDashlet.meta.php',
    'module' => 'Leads',
  ),
  'LIP_Line_InstitutesDashlet' => 
  array (
    'file' => 'modules/LIP_Line_Institutes/Dashlets/LIP_Line_InstitutesDashlet/LIP_Line_InstitutesDashlet.php',
    'class' => 'LIP_Line_InstitutesDashlet',
    'meta' => 'modules/LIP_Line_Institutes/Dashlets/LIP_Line_InstitutesDashlet/LIP_Line_InstitutesDashlet.meta.php',
    'module' => 'LIP_Line_Institutes',
  ),
  'LSP_Lead_StatusDashlet' => 
  array (
    'file' => 'modules/LSP_Lead_Status/Dashlets/LSP_Lead_StatusDashlet/LSP_Lead_StatusDashlet.php',
    'class' => 'LSP_Lead_StatusDashlet',
    'meta' => 'modules/LSP_Lead_Status/Dashlets/LSP_Lead_StatusDashlet/LSP_Lead_StatusDashlet.meta.php',
    'module' => 'LSP_Lead_Status',
  ),
  'MyMeetingsDashlet' => 
  array (
    'file' => 'modules/Meetings/Dashlets/MyMeetingsDashlet/MyMeetingsDashlet.php',
    'class' => 'MyMeetingsDashlet',
    'meta' => 'modules/Meetings/Dashlets/MyMeetingsDashlet/MyMeetingsDashlet.meta.php',
    'module' => 'Meetings',
  ),
  'MOU_MOUDashlet' => 
  array (
    'file' => 'modules/MOU_MOU/Dashlets/MOU_MOUDashlet/MOU_MOUDashlet.php',
    'class' => 'MOU_MOUDashlet',
    'meta' => 'modules/MOU_MOU/Dashlets/MOU_MOUDashlet/MOU_MOUDashlet.meta.php',
    'module' => 'MOU_MOU',
  ),
  'MyNotesDashlet' => 
  array (
    'file' => 'modules/Notes/Dashlets/MyNotesDashlet/MyNotesDashlet.php',
    'class' => 'MyNotesDashlet',
    'meta' => 'modules/Notes/Dashlets/MyNotesDashlet/MyNotesDashlet.meta.php',
    'module' => 'Notes',
  ),
  'MyClosedOpportunitiesDashlet' => 
  array (
    'file' => 'modules/Opportunities/Dashlets/MyClosedOpportunitiesDashlet/MyClosedOpportunitiesDashlet.php',
    'class' => 'MyClosedOpportunitiesDashlet',
    'meta' => 'modules/Opportunities/Dashlets/MyClosedOpportunitiesDashlet/MyClosedOpportunitiesDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'MyOpportunitiesDashlet' => 
  array (
    'file' => 'modules/Opportunities/Dashlets/MyOpportunitiesDashlet/MyOpportunitiesDashlet.php',
    'class' => 'MyOpportunitiesDashlet',
    'meta' => 'modules/Opportunities/Dashlets/MyOpportunitiesDashlet/MyOpportunitiesDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OutboundEmailAccountsDashlet' => 
  array (
    'file' => 'modules/OutboundEmailAccounts/Dashlets/OutboundEmailAccountsDashlet/OutboundEmailAccountsDashlet.php',
    'class' => 'OutboundEmailAccountsDashlet',
    'meta' => 'modules/OutboundEmailAccounts/Dashlets/OutboundEmailAccountsDashlet/OutboundEmailAccountsDashlet.meta.php',
    'module' => 'OutboundEmailAccounts',
  ),
  'MyProjectDashlet' => 
  array (
    'file' => 'modules/Project/Dashlets/MyProjectDashlet/MyProjectDashlet.php',
    'class' => 'MyProjectDashlet',
    'meta' => 'modules/Project/Dashlets/MyProjectDashlet/MyProjectDashlet.meta.php',
    'module' => 'Project',
  ),
  'MyProjectTaskDashlet' => 
  array (
    'file' => 'modules/ProjectTask/Dashlets/MyProjectTaskDashlet/MyProjectTaskDashlet.php',
    'class' => 'MyProjectTaskDashlet',
    'meta' => 'modules/ProjectTask/Dashlets/MyProjectTaskDashlet/MyProjectTaskDashlet.meta.php',
    'module' => 'ProjectTask',
  ),
  'SpotsDashlet' => 
  array (
    'file' => 'modules/Spots/Dashlets/SpotsDashlet/SpotsDashlet.php',
    'class' => 'SpotsDashlet',
    'meta' => 'modules/Spots/Dashlets/SpotsDashlet/SpotsDashlet.meta.php',
  ),
  'SRP_StdApproval_RoutineDashlet' => 
  array (
    'file' => 'modules/SRP_StdApproval_Routine/Dashlets/SRP_StdApproval_RoutineDashlet/SRP_StdApproval_RoutineDashlet.php',
    'class' => 'SRP_StdApproval_RoutineDashlet',
    'meta' => 'modules/SRP_StdApproval_Routine/Dashlets/SRP_StdApproval_RoutineDashlet/SRP_StdApproval_RoutineDashlet.meta.php',
    'module' => 'SRP_StdApproval_Routine',
  ),
  'STP_Sales_TargetDashlet' => 
  array (
    'file' => 'modules/STP_Sales_Target/Dashlets/STP_Sales_TargetDashlet/STP_Sales_TargetDashlet.php',
    'class' => 'STP_Sales_TargetDashlet',
    'meta' => 'modules/STP_Sales_Target/Dashlets/STP_Sales_TargetDashlet/STP_Sales_TargetDashlet.meta.php',
    'module' => 'STP_Sales_Target',
  ),
  'STP_Stakeholder_TypeDashlet' => 
  array (
    'file' => 'modules/STP_Stakeholder_Type/Dashlets/STP_Stakeholder_TypeDashlet/STP_Stakeholder_TypeDashlet.php',
    'class' => 'STP_Stakeholder_TypeDashlet',
    'meta' => 'modules/STP_Stakeholder_Type/Dashlets/STP_Stakeholder_TypeDashlet/STP_Stakeholder_TypeDashlet.meta.php',
    'module' => 'STP_Stakeholder_Type',
  ),
  'SugarFeedDashlet' => 
  array (
    'file' => 'modules/SugarFeed/Dashlets/SugarFeedDashlet/SugarFeedDashlet.php',
    'class' => 'SugarFeedDashlet',
    'meta' => 'modules/SugarFeed/Dashlets/SugarFeedDashlet/SugarFeedDashlet.meta.php',
    'module' => 'SugarFeed',
  ),
  'SurveyQuestionOptionsDashlet' => 
  array (
    'file' => 'modules/SurveyQuestionOptions/Dashlets/SurveyQuestionOptionsDashlet/SurveyQuestionOptionsDashlet.php',
    'class' => 'SurveyQuestionOptionsDashlet',
    'meta' => 'modules/SurveyQuestionOptions/Dashlets/SurveyQuestionOptionsDashlet/SurveyQuestionOptionsDashlet.meta.php',
    'module' => 'SurveyQuestionOptions',
  ),
  'SurveyQuestionResponsesDashlet' => 
  array (
    'file' => 'modules/SurveyQuestionResponses/Dashlets/SurveyQuestionResponsesDashlet/SurveyQuestionResponsesDashlet.php',
    'class' => 'SurveyQuestionResponsesDashlet',
    'meta' => 'modules/SurveyQuestionResponses/Dashlets/SurveyQuestionResponsesDashlet/SurveyQuestionResponsesDashlet.meta.php',
    'module' => 'SurveyQuestionResponses',
  ),
  'SurveyQuestionsDashlet' => 
  array (
    'file' => 'modules/SurveyQuestions/Dashlets/SurveyQuestionsDashlet/SurveyQuestionsDashlet.php',
    'class' => 'SurveyQuestionsDashlet',
    'meta' => 'modules/SurveyQuestions/Dashlets/SurveyQuestionsDashlet/SurveyQuestionsDashlet.meta.php',
    'module' => 'SurveyQuestions',
  ),
  'SurveyResponsesDashlet' => 
  array (
    'file' => 'modules/SurveyResponses/Dashlets/SurveyResponsesDashlet/SurveyResponsesDashlet.php',
    'class' => 'SurveyResponsesDashlet',
    'meta' => 'modules/SurveyResponses/Dashlets/SurveyResponsesDashlet/SurveyResponsesDashlet.meta.php',
    'module' => 'SurveyResponses',
  ),
  'SurveysDashlet' => 
  array (
    'file' => 'modules/Surveys/Dashlets/SurveysDashlet/SurveysDashlet.php',
    'class' => 'SurveysDashlet',
    'meta' => 'modules/Surveys/Dashlets/SurveysDashlet/SurveysDashlet.meta.php',
    'module' => 'Surveys',
  ),
  'MyTasksDashlet' => 
  array (
    'file' => 'modules/Tasks/Dashlets/MyTasksDashlet/MyTasksDashlet.php',
    'class' => 'MyTasksDashlet',
    'meta' => 'modules/Tasks/Dashlets/MyTasksDashlet/MyTasksDashlet.meta.php',
    'module' => 'Tasks',
  ),
  'USP_Update_Lead_StatusDashlet' => 
  array (
    'file' => 'modules/USP_Update_Lead_Status/Dashlets/USP_Update_Lead_StatusDashlet/USP_Update_Lead_StatusDashlet.php',
    'class' => 'USP_Update_Lead_StatusDashlet',
    'meta' => 'modules/USP_Update_Lead_Status/Dashlets/USP_Update_Lead_StatusDashlet/USP_Update_Lead_StatusDashlet.meta.php',
    'module' => 'USP_Update_Lead_Status',
  ),
  'VNP_VenuesDashlet' => 
  array (
    'file' => 'modules/VNP_Venues/Dashlets/VNP_VenuesDashlet/VNP_VenuesDashlet.php',
    'class' => 'VNP_VenuesDashlet',
    'meta' => 'modules/VNP_Venues/Dashlets/VNP_VenuesDashlet/VNP_VenuesDashlet.meta.php',
    'module' => 'VNP_Venues',
  ),
);