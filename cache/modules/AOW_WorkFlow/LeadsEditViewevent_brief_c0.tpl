
<link rel="stylesheet" type="text/css" href="include/SugarFields/Fields/Wysiwyg/css/wysiwyg-editview.css" />

{if empty($fields.event_brief_c.value)}
    {assign var="value" value=$fields.event_brief_c.default_value }
{else}
    {assign var="value" value=$fields.event_brief_c.value }
{/if}

    {literal}
        <script type="text/javascript" language="Javascript"> tinyMCE.init({"convert_urls":false,"valid_children":"+body[style]","height":300,"width":"100%","theme":"modern","theme_advanced_toolbar_align":"left","theme_advanced_toolbar_location":"top","theme_advanced_buttons1":"","theme_advanced_buttons2":"","theme_advanced_buttons3":"","strict_loading_mode":true,"mode":"exact","language":"en","plugins":"print code preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern ","elements":"# #event_brief_c","extended_valid_elements":"style[dir|lang|media|title|type],hr[class|width|size|noshade],@[class|style]","content_css":"vendor\/tinymce\/tinymce\/skins\/lightgray\/content.min.css","apply_source_formatting":false,"cleanup_on_startup":true,"relative_urls":false,"selector":"# #event_brief_c","toolbar1":"formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat"});</script>
    {/literal}
    <div class="wysiwyg">
        <textarea
            id="{$fields.event_brief_c.name}"
            name="{$fields.event_brief_c.name}"
            rows="4"
            cols="60"
            title=''
            tabindex="1"
            
        >{$value}</textarea>
    </div>

<br />