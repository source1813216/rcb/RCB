<?php
// created: 2022-11-29 17:32:08
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_DESCRIPTION' => 'Instructions',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Docket',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_ASCENDING' => 'Ascending',
  'LBL_DESCENDING' => 'Descending',
  'LBL_OPT_IN' => 'Opt In',
  'LBL_OPT_IN_PENDING_EMAIL_NOT_SENT' => 'Pending Confirm opt in, Confirm opt in not sent',
  'LBL_OPT_IN_PENDING_EMAIL_SENT' => 'Pending Confirm opt in, Confirm opt in sent',
  'LBL_OPT_IN_CONFIRMED' => 'Opted in',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_SECURITYGROUPS' => 'Security Groups',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_LIST_FORM_TITLE' => 'Bid Submission List',
  'LBL_MODULE_NAME' => 'Event Documents',
  'LBL_MODULE_TITLE' => 'Event Documents',
  'LBL_HOMEPAGE_TITLE' => 'My Bid Submission',
  'LNK_NEW_RECORD' => 'Create Bid Submission',
  'LNK_LIST' => 'View Bid Submission',
  'LNK_IMPORT_EDP_EVENT_DOCUMENTS' => 'Import Bid Submission',
  'LBL_SEARCH_FORM_TITLE' => 'Search Bid Submission',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_EDP_EVENT_DOCUMENTS_SUBPANEL_TITLE' => 'Event Documents',
  'LBL_NEW_FORM_TITLE' => 'New Event Documents',
  'LBL_CLIENT_ACCOUNT_ID' => '&#039;Client&#039; (related &#039;Client&#039; ID)',
  'LBL_CLIENT' => 'Client',
  'LBL_PRIMARY_CONTACT_CONTACT_ID' => '&#039;Primary Contact&#039; (related &#039;Contact&#039; ID)',
  'LBL_PRIMARY_CONTACT' => 'Primary Contact',
  'LBL_EVENT_BRIEF' => 'Event Brief',
  'LBL_EDITVIEW_PANEL1' => 'Instructions/Remarks',
  'LBL_DETAILVIEW_PANEL1' => 'Records Infor',
  'LBL_DETAILVIEW_PANEL2' => 'Event Brief',
  'LBL_DETAILVIEW_PANEL3' => 'Document Status',
  'LBL_EVENT_BUDGET' => 'Event Budget',
  'LBL_APPROVED_ENDROSEMENTS' => 'Approved Endrosements',
  'LBL_BID_BOOK' => 'Bid Book',
  'LBL_EVENT_COORDINATOR_USER_ID' => '&#039;Event Coordinator&#039; (related &#039;User&#039; ID)',
  'LBL_EVENT_COORDINATOR' => 'Event Coordinator',
  'LBL_EDITVIEW_PANEL2' => 'Select Event Coordinator',
  'LBL_DETAILVIEW_PANEL4' => 'New Panel 4',
  'LBL_BUDGET_DOCS' => 'Budget Docs',
  'LBL_BID_DOCS' => 'Bid Docs',
  'LBL_ENDORSEMENTS' => 'Endorsements',
  'LBL_VALIDATE_BUDGETDOCS' => '1',
  'LBL_VALIDATE_ENDORSEMENTS' => '2',
  'LBL_VALIDATE_BIDDOCS' => '3',
  'LBL_EDITVIEW_PANEL3' => 'Document Check List',
  'LBL_EDP_EVENT_DOCUMENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE' => 'Documents',
  'LBL_LEADS_EDP_EVENT_DOCUMENTS_1_FROM_LEADS_TITLE' => 'Leads',
);