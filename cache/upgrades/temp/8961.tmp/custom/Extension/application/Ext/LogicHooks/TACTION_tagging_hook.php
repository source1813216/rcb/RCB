<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Logic hook file to 'Add Tags' button and 'Tag Search' in ui_frame
 * 
 */

if (!isset($hook_array) || !is_array($hook_array)) {
    $hook_array = array();
}
if (!isset($hook_array['after_ui_frame']) || !is_array($hook_array['after_ui_frame'])) {
    $hook_array['after_ui_frame'] = array();
}
$hook_array['after_ui_frame'][] = Array(111,'after_ui_frame Add Tags','custom/modules/tac_Tags/taction_add_tags_class.php','taction_add_tags_class', 'add_tags_method');

