<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Dashlet metadata file
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

global $app_strings;

$dashletMeta['tac_TagsDashlet'] = array(
    'module' => 'tac_Tags',
    'title' => translate('LBL_HOMEPAGE_TITLE', 'tac_Tags'),
    'description' => 'A customizable view into tac_Tags',
    'category' => 'Module Views'
);
