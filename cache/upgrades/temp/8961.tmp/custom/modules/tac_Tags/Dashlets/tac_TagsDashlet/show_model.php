<?php 
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Dashlet Tags related beans pop-up for
 * 
 */

$tag_id=$_POST['tag_id'];
$tag_name=$_POST['tag_name'];
?>

<style>
table {
    //border-collapse: collapse;
    //border-spacing: 0;
   // width: auto;
    //border: 1px solid #ddd;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
<?php
echo'
<div id="myModal_'.$tag_id.'" class="modal fade" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal">X</button>
				<h4 class="modal-title">'.$tag_name.'</h4>
			</div>
				<div class="modal-body" style="overflow-x:auto;">
					<table>';
							
								$sql="SELECT DISTINCT bean_module FROM `tac_tag_bean_rel` where tag_id='$tag_id' and deleted=0";
								$res=$GLOBALS['db']->query($sql);
								$module_arr=array();
								$bean_name=array();
								$url_arr=array();
								while($r=$GLOBALS['db']->fetchByAssoc($res)){
									$module = $r['bean_module'];
									$module_arr[]= $r['bean_module'];
								
								}
									
								echo '<tr>';
									$c_mod=count($module_arr); 
									for($i=0;$i<$c_mod;$i++)
									{
									
									echo '<th>'.$module_arr[$i].'</th>';
									
									}
									
								echo ' </tr>
								<tr>';
									
									$c_mod=count($module_arr); 
									if($c_mod<1){
										echo "<h5 class='text-center' style='color:red'>There is no related module about this tag</h5>";
									}
									for($j=0;$j<$c_mod;$j++)
									{
										$sql2="Select bean_id FROM `tac_tag_bean_rel` where tag_id='$tag_id' and bean_module='".$module_arr[$j]."' and deleted=0 group by bean_id";
										$res2=$GLOBALS['db']->query($sql2);
										
										echo '<td style="overflow-y:scroll;height:20px">';
										
										while($r2=$GLOBALS['db']->fetchByAssoc($res2)){
											$record_id = $r2['bean_id'];
											$bean = BeanFactory::getBean($module_arr[$j], $record_id);
											$url="index.php?action=DetailView&module=$module_arr[$j]&record=$record_id";
											
											echo '<li class="noBullet"><a href="'.$url.'">'.$bean->name.'</a></li>';
											 
										}
										
										echo'</td>';
										
									}
									
								echo'	
								</tr>
					<tr><td colspan='.$c_mod.'>&nbsp;</td></tr>
				</table>
			</div>
		</div>
	</div>
</div>';
?>
