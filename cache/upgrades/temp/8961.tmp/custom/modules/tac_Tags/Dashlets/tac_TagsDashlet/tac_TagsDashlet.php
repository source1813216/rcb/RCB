<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Dashlet tpl action file
 * 
 */
 
 
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

require_once('include/Dashlets/Dashlet.php');
require_once('modules/tac_Tags/tac_Tags.php');

class tac_TagsDashlet extends Dashlet {
    function __construct($id, $def = null)
    {
        global $current_user, $app_strings;
        require('modules/tac_Tags/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if (empty($def['title'])) {
            $this->title = translate('LBL_HOMEPAGE_TITLE', 'tac_Tags');
        }

        $this->searchFields = $dashletData['tac_TagsDashlet']['searchFields'];
        $this->columns = $dashletData['tac_TagsDashlet']['columns'];

        $this->seedBean = new tac_Tags();    
        
        // Fetching Tags
        $qry = "SELECT id,name from tac_tags WHERE deleted=0";
		//$count = $this->seedBean->db->query($this->seedBean->create_list_count_query($qry));
		$result = $this->seedBean->db->query($qry);
		$ROW=array();
		$row_count=array();	
		while($row = $this->seedBean->db->fetchByAssoc($result)){
				$ROW[$row['id']]=$row['name'];
				$count_qry = "select count(*) as count_tag from tac_tag_bean_rel where tag_id='".$row['id']."' and deleted=0";
				$count_res = $this->seedBean->db->query($count_qry);
				$row2 = $this->seedBean->db->fetchByAssoc($count_res);
				$row_count[$row['id']]=$row2['count_tag'];
				
		}
		$maximum=max($row_count);
		$class=array();	
		foreach ($row_count as  $key => $term){
			
			$percent = floor(($term/ $maximum) * 100);
			
			if ($percent < 20)
				$class[$key] = 'smallest'; 
			elseif ($percent >= 20 && $percent < 40)
			   $class[$key] = 'small'; 
			elseif ($percent >= 40 && $percent < 60)
			   $class[$key] = 'medium';
			elseif ($percent >= 60 && $percent < 80)
			   $class[$key] = 'large';
			else
			   $class[$key] = 'largest';
			   
			
		}
		
		$this->row=$ROW;
		$this->row_count=$row_count;
		$this->classes=$class;
    }
    public function display()
    {	
    	$ss = new Sugar_Smarty();
    	//Validate License
		require_once('modules/tac_Tags/license/OutfittersLicense.php');
		$validate_license = OutfittersLicense::isValid('tac_Tags',$current_user->id);
		if($validate_license !== true) 
		{
			$validation= '<h2><p class="error">Tags Cloud is no longer active</p></h2><p class="error">'.$validate_license.'</p>';
			$ss->assign('title', $validation);
		}
		else
		{
    	// Assigning to tpls
		$ss->assign('title', $this->row);
		$ss->assign('row_count', $this->row_count);
		$ss->assign('class', $this->classes);
		}
    	return parent::display() . $ss->fetch('custom/modules/tac_Tags/Dashlets/tac_TagsDashlet/tac_TagsDashlet.tpl');
		
    }
    public function displayOptions() 
    {
        $ss = new Sugar_Smarty();
		return $ss->fetch('custom/modules/tac_Tags/Dashlets/tac_TagsDashlet/tac_TagsDashlet.tpl');        
    }
}
