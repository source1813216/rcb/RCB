<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Detailview of Tags
 * 
 */
 
require_once('include/MVC/View/views/view.detail.php');

class tac_TagsViewDetail extends ViewDetail
{
    function display()
    {
        parent::display();
        $id= $_REQUEST['record'];
        $related_beans="SELECT * FROM `tac_tag_bean_rel` where tag_id='$id' and deleted=0";
        $result_related_qry =$GLOBALS['db']->query($related_beans);
		if($GLOBALS['db']->getRowCount($result_related_qry)>0){
			echo '<br/><br/><b>Related records</b><br/>';
			echo '<div id="tag_bean" class="tag_bean-container" style="height:500px; overflow-y:auto;">';
			while($row_bean=$GLOBALS["db"]->fetchByAssoc($result_related_qry)){
			$record_id = $row_bean['bean_id'];
			$module = $row_bean['bean_module'];
			$bean = BeanFactory::getBean($module, $record_id);
			echo $display_related_beans='<table cellpadding="0" cellspacing="0" width="100%" border="0" class="list View">
			<tbody><tr><td width="1%" nowrap="nowrap" ></td></tr></tbody>
			  <thead>
				<tr height="20">
					<th scope="col" width="10%" data-hide="phone">
							<span sugar="sugar1">
								<div style="white-space: nowrap;" width="100%" align="left">
									Module                    </div>
							</span>
					</th>
					<th scope="col" width="30%" data-toggle="true">
							<span sugar="sugar1">
								<div style="white-space: nowrap;" width="100%" align="left">
									Name                    </div>
							</span>
					</th>
					<th scope="col" width="30%" data-hide="phone">
							<span sugar="sugar1">
								<div style="white-space: nowrap;" width="100%" align="left">
									Description                    </div>
							</span>
					</th>
					<th scope="col" width="25%" data-hide="phone,phonelandscape">
						<div style="white-space: nowrap;" width="100%" align="left">
							Date Created            </div>
					</th>
					<th scope="col" width="25%" data-hide="phone,phonelandscape">
						<div style="white-space: nowrap;" width="100%" align="left">
							Date Modified            </div>
					</th>
				</tr>
				</thead>
				<tbody><tr><td>'.$module.'</td><td><a href="index.php?module='.$module.'&amp;action=DetailView&amp;record='.$record_id.'">'.$bean->name.'</a></td><td>'.$bean->description.'</td><td>'.$bean->date_entered.'</td><td>'.$bean->date_modified.'</td></tr></tbody></table><br/>';
			}
			
			echo '</div>';
		}
	}
}
