<!-- 
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Pop up Form
 */
-->

<form action="index.php?entryPoint=tac_Tags" id="my_form" method="post" name="my_form">
			<input type="hidden" id="source_id" name="source_id" style="display:color=#ffffff" value="<?php echo $_REQUEST['record']; ?>"/>
			<input type="hidden" id="source_type" name="source_type" value="<?php echo $module; ?>"/>
			<table  width="100%" cellpadding="10" cellspacing="15" border="0">
			<tr>
				<td nowrap width="25%" scope="row">Create New Tag:</td>
				<td width="75%"><input id="create_tag" name="create_tag" type="text" placeholder="Tag name" style="width:66%"></td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
				<td nowrap width="25%" scope="row">Select Tag:</td>
				<td width="75%">
					<div class="col-sm-8" id="select_tag_div">
						<select id="select_tag" name="select_tag[]" style="width:100%" class="select_tag_class" multiple="multiple">
							<?php $sql="Select id,name from tac_tags where deleted=0";
								$result=$GLOBALS['db']->query($sql);
								if($GLOBALS['db']->getRowCount($result) > 0)
								{
									while($row = $GLOBALS['db']->fetchByAssoc($result)){
							?>
							<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
							<?php
								}
							}
							?>
						</select>
					</div>
				</td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
				<td nowrap width="25%" scope="row">Description:</td>
				<td width="75%"><textarea id="description" name="description" placeholder="Description about new tag" rows="4" cols="55" style="width:67%"></textarea></td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			
			<tr>
				<td nowrap width="25%" scope="row">&nbsp;</td>
				<td width="75%"><button href="javascript:%20check_empty()" class="btn btn-default" id="submit" style="width:20%">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			</table>
		</form>
