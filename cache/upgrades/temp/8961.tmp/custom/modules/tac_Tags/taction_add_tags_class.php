<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: This file adds tagging functionality in SuiteCRM ('Search Tags' and 'ADD TAGS') 
 */

class taction_add_tags_class {
  
    function add_tags_method()
    {
		
        
        if(isset($_REQUEST['record']) && $_REQUEST['record']!=''){
            $record = $_REQUEST['record'];
            $module=$GLOBALS['app']->controller->module;
            $bean_obj = BeanFactory::getBean("$module", $record);
        }
        
		// Global Tag Search Feature
		
		$tag_search= <<<EOQ
<script type="text/javascript">
	var search_button = $("<div id='tag_search_div'><form id='tag_srch_form' action='index.php?module=tac_Tags&action=global_tag_search' name='tag_srch_form' method='POST' style='float:right;margin:-18px 0 0 0;'><input type='text' name='tag_search_qry' id='tag_search_qry' placeholder='Tag Search...' value='' style='width:80px;line-height:1px;min-height:15px;'><button type='submit' class='btn btn-default' id='search_tag' style='height:25px;padding:0px 7px'><span class='glyphicon glyphicon-search'></span></button></form></div>");
	
	$('#content').prepend(search_button);
</script>
EOQ;

		// Prevent Add dashlet Dialouge and Module Builder
		if((isset($_REQUEST['module']) && ($_REQUEST['module']!='ModuleBuilder')) && ( !isset($_REQUEST['DynamicAction']) && isset($_REQUEST['DynamicAction'])!='dashletsDialog')){
			echo $tag_search;
		}
		
        switch ($GLOBALS['app']->controller->action) 
        {
            case "DetailView": 
            global $sugar_config,$mod_strings,$app_list_strings; 
            $config_modules=explode(',',$sugar_config['en_tag_modules']);
            
            // Enabled Module Filter
             if(in_array($app_list_strings['moduleList'][$GLOBALS['app']->controller->module],$config_modules)){
					echo '<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
				<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>
				<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
					<div id="myModal" class="modal fade" >
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Create your tags:</h4>
								</div>
								<div class="modal-body">';
								include("custom/modules/tac_Tags/tags.php"); // For Add Tag Pop-up 
								echo'</div>
								
							</div>
						</div>
					</div>';
					
					// ADD TAGS button
					$button_code = <<<EOQ
<script type="text/javascript">
$(document).ready(function(){
    var button = $('<div id="add_tag"><span><input type="button" style="margin-left:2px;" class="button" id="add_tags" value="Add Tags" onclick=\'$("#myModal").modal("show");\'></span></div>"');
    var counter=$("#tag_count").val();
    
    $('.actionsContainer').append( $('<div id="tag_buttons" style="width:100px">'));    
    if(counter>0)
    {
		for (i = 0; i < counter; i++){
			var tag=$("#view_tag_"+i).val();
			var tag_id=$("#tag_id_"+i).val();
			var tag_idd=$("#tag_idd_"+i).val();
			//$('.actionsContainer').append($('<div id="tag_div_'+i+'" style="margin-left:1px;float:left;"><input type="button" id="tag_'+i+'" name="tag_'+i+'" value="'+tag+'" /><button class="close" id="delete_tag_'+i+'" name="delete_tag_'+i+'" value="'+tag_id+'" style="margin-left: -4px;" >X</button></div>'));
			
			$('.actionsContainer').append($('<div id="tag_div_'+i+'" style="margin-left:1px;float:left;"><button type="button" class="btn btn-primary" id="tag_'+i+'" name="tag_'+i+'" value="'+tag+'" onclick=window.location.href="index.php?module=tac_Tags&action=DetailView&record='+tag_idd+'">'+tag+'</button><button class="ui-btn ui-corner-all ui-shadow delete_tags" id="delete_tag_'+i+'" name="delete_tag_'+i+'" value="'+tag_id+'" style="position:relative;right:4px;top:-7px;">&times;</button></div>'));
			
			var makeColor ="#" + Math.floor((Math.random() * 999) + 1);
			$("#tag_"+i).css("background-color",makeColor);
		}
	}  
    $('.actionsContainer').append( $('</div>')); 
    
     // Add tags in a detail view
    $('.actionsContainer').append(button); 
    
    $('.delete_tags').on('click', function(){
		var pass = $(this).val();
		$(this).parent('div').fadeOut();
		$.post( "index.php?entryPoint=tac_Tags", { 
		   'delete_id':pass
		}).done(function( data ) {
			//location.reload();
		});
	}); 
	
	$("#select_tag").select2({dropdownParent: $("#select_tag_div")});
});  
    
</script>
EOQ;
					// View Tags 
					$fetch_rel="Select * from tac_tag_bean_rel WHERE bean_id='$record' and deleted=0";	
					$result=$GLOBALS['db']->query($fetch_rel);
					$view_tags='';
					$i=0;
					if($GLOBALS['db']->getRowCount($result) > 0)
					{
						while($row = $GLOBALS['db']->fetchByAssoc($result)){
							$fetch_tags="Select id,name from tac_tags where id='".$row['tag_id']."' AND deleted=0 ORDER BY name ASC";
							$result2=$GLOBALS['db']->query($fetch_tags);
							if($GLOBALS['db']->getRowCount($result2) > 0){
								while($row2 = $GLOBALS['db']->fetchByAssoc($result2)){
									$view_tags .='<input type="hidden" id="view_tag_'.$i.'" name="view_tag_'.$i.'" value="'.$row2['name'].'"/><input type="hidden" id="tag_id_'.$i.'" name="tag_id_'.$i.'" value="'.$row['id'].'"/><input type="hidden" id="tag_idd_'.$i.'" name="tag_idd_'.$i.'" value="'.$row2['id'].'"/>';
									$view_tags .=' ';
									$i++;
								}
							}
						}
					}
					$view_tags .='<input type="hidden" id="tag_count" value="'.$i.'">';
					echo $view_tags;
					echo $button_code;	
					
				}
                break;
        }
        
        
        
    }
}
