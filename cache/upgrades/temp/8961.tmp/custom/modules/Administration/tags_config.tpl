<!-- 
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Tag configurator tpl file 
 * 
 */

-->

<script src="custom/modules/Administration/js/tag.js" type="text/javascript"></script>
<link rel="stylesheet" href="custom/modules/Administration/style_tag.css"></script>


<h1>{$MOD.LBL_TAGS_CONFIGURATION_TITLE}</h1>
<form id="ConfigureSettings" name="ConfigureSettings" enctype='multipart/form-data' method="POST"
      action="index.php?module=Administration&action=tags_config&do=save">

<input type="hidden" id="en_tag_modules" name="en_tag_modules" value="">
<input type="hidden" id="dis_tag_modules" name="dis_tag_modules" value="">
    <span class='error'>{$error.main}</span>

    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="actionsContainer">
        <tr>
            <td>
                {$BUTTONS}
                 </td>
        </tr>
    </table>
     <table width="100%" border="0" cellspacing="4" cellpadding="5" class="edit view">
        <tr>
            <th align="left" scope="row" colspan="4">
                <h4>{$MOD.LBL_CONFIGURE_MODULES_TO_ENABLE_TAGGING}</h4>
            </th>
        </tr>
        <tr><td><h4>{$MOD.LBL_ENABLE_MODULES}</h4></td><td><h4>{$MOD.LBL_DISABLE_MODULES}</h4></td></tr>
        <tr>
			
			<td width="50%" border="1">
				<div id="enabled_tag_modules" class="panel-body box-container" style="overflow:scroll; width: 300px; height: 400px; padding: 0.5em; border: 1px solid #a5e8d6;">
					{php}
					global $sugar_config;
					$en_tag_modules=array();
					if(!empty($sugar_config['en_tag_modules']))
						$en_tag_modules=explode(',',$sugar_config['en_tag_modules']);
					foreach ($en_tag_modules as $key => $value) {		
						{/php}<li class="sortable-item noBullet box-item" itemid="{php}echo $value;{/php}">{php}echo $value;{/php}</li>
						{php}
					}
					{/php}
				</div>
			</td>
			<td width="50%" border="1">
				<div id="disabled_tag_modules" class="panel-body box-container" style="overflow:scroll; width: 300px; height: 400px; padding: 0.5em; border: 1px solid #a5e8d6;">
					{php}
					//$module_list[0]=$GLOBALS['moduleList'];
					global $current_user,$app_list_strings;
					foreach (query_module_access_list($current_user) as $module) {
						if (isset($app_list_strings['moduleList'][$module])) {
							$fullModuleList[$module] = $app_list_strings['moduleList'][$module];
						}
					}
					$module_list[0]=$fullModuleList;
					
					
					$result_modules = array_diff($module_list[0], $en_tag_modules);
					
					sort($result_modules);
						
					foreach ($result_modules as $key=>$value)
					{
					{/php}<li class="sortable-item noBullet box-item" itemid="{php}echo $value;{/php}">{php}echo $value;{/php}</li>
					{php}
					}
					{/php}
				
				</div>
			</td>
		</tr>
    </table>
    
    {$JAVASCRIPT}
</form>

           
