/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Tags Configurator js file 
 */
 
$(document).ready(function() {
  $(".box-item").draggable({
    cursor: 'move',
    helper: "clone"
  });

  $("#enabled_tag_modules").droppable({
    drop: function(event, ui) {
      var itemid = $(event.originalEvent.target).attr("itemid");
      $('.box-item').each(function() {
        if ($(this).attr("itemid") === itemid) {
          $(this).appendTo("#enabled_tag_modules");
        }
      });
    }
  });

  $("#disabled_tag_modules").droppable({
    drop: function(event, ui) {
      var itemid = $(event.originalEvent.target).attr("itemid");
      $('.box-item').each(function() {
        if ($(this).attr("itemid") === itemid) {
          $(this).appendTo("#disabled_tag_modules");
        }
      });
    }
  });

	
  
  $("#save").on("click",function()
    { 
		var arr = [];
		$("#enabled_tag_modules li").each(function(index, elem){
			arr.push($(this).text());
		});
		
		var arr2 = [];
		$("#disabled_tag_modules li").each(function(index, elem){
			arr2.push($(this).text());
		});
		
		$("#en_tag_modules").val(arr);
		$("#dis_tag_modules").val(arr2);
        
    });

});
