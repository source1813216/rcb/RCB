<?php

$mod_strings['LBL_TAG_CLOUD_LICENSEADDON'] = 'Tag Cloud License Add-on';
$mod_strings['LBL_TAG_CLOUD_LICENSEADDON_LICENSE_TITLE'] = 'License Configuration';
$mod_strings['LBL_TAG_CLOUD_LICENSEADDON_LICENSE'] = 'Manage and configure the license for this add-on';
