<?php

global $sugar_version;

$admin_option_defs=array();

if(preg_match( "/^6.*/", $sugar_version) ) {
    $admin_option_defs['Administration']['tag_cloud_info']= array('helpInline','LBL_TAG_CLOUD_LICENSEADDON_LICENSE_TITLE','LBL_TAG_CLOUD_LICENSEADDON_LICENSE','./index.php?module=tac_Tags&action=license');
} else {
    $admin_option_defs['Administration']['samplelicenseaddon_info']= array('helpInline','LBL_TAG_CLOUD_LICENSEADDON_LICENSE_TITLE','LBL_TAG_CLOUD_LICENSEADDON_LICENSE','javascript:parent.SUGAR.App.router.navigate("#bwc/index.php?module=tac_Tags&action=license", {trigger: true});');
}

$admin_group_header[]= array('LBL_TAG_CLOUD_LICENSEADDON','',false,$admin_option_defs, '');
