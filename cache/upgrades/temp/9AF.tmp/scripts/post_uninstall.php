<?php
if(!defined('sugarEntry') || !sugarEntry) { die('Not A Valid Entry Point'); }

$autoexecute = false; //execute the SQL
$show_output = false; //output to the screen
require_once("modules/Administration/QuickRepairAndRebuild.php");
$randc = new RepairAndClear();
$randc->repairAndClearAll(array('clearAll'),array(translate('LBL_ALL_MODULES')), $autoexecute,$show_output);