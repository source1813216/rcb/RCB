<?PHP
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/

/**
 * THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
 */
require_once('modules/UT_WebToModule/UT_WebToModule_sugar.php');
class UT_WebToModule extends UT_WebToModule_sugar {
	
	function __construct(){	
		parent::__construct();
	}
        function get_list_view_data(){
            $ut_webtomodule_fields = $this->get_list_view_array();
            $fileName = "UrdhvaTechWebToModule/".$ut_webtomodule_fields["ID"].".html";
            $downLoadFileLink = "";
            if(sugar_is_file($fileName))
                $downLoadFileLink="<a href='index.php?module=UT_WebToModule&entryPoint=customDownload&id=".$ut_webtomodule_fields["ID"].".html"."&downloadFilename=".$ut_webtomodule_fields["ID"].".html"."'>".$ut_webtomodule_fields["NAME"].".html"."</a>";
            
            $ut_webtomodule_fields["DOWNLOAD_WEB_TO_LEAD"] = $downLoadFileLink;
            return $ut_webtomodule_fields;
        }
        /***
        * get List of modules that are available for Webto Module
        * get List of modules 
        */
        function getModuleList() {
              $aIgnoreModule = array('Activities', 'UT_WebToModule','SecurityGroups');
              global $app_list_strings;
              require_once 'modules/ModuleBuilder/parsers/relationships/DeployedRelationships.php';
              $relatableModules = DeployedRelationships::findRelatableModules(false);
              $ret = array();
              foreach ($relatableModules as $module => $other) {
                if (in_array($module, $aIgnoreModule))
                  continue;
                if (!empty($app_list_strings['moduleList'][$module]))
                  $ret[$module] = $app_list_strings['moduleList'][$module];
                else
                  $ret[$module] = $module;
              }
              return $ret;
        }
        /*
         * get list of field from the specific module
         * @param   object
         * @param   module name
         * @return  array  fields
         */
        function get_module_field_list($mod_name) {
            global $current_language;
            $focus = BeanFactory::getBean($mod_name);
            
              $global_linked_modules_to_ignore = array("Teams", "Currencies");
              $fields_to_ignore = array('deleted', 'id', 'team_id', 'team_set_id', 'date_modified', 'date_entered', 'modified_user_id', 'assigned_user_id','created_by');
              $types_to_ignore = array('link', 'id', 'relate', 'image','datetimecombo');
              $linked_modules_to_ignore = array();
              $sources_to_ignore = array('non-db');
              $special_field = array('email1');     //To support email address
              
              $temp_module_strings = return_module_language($current_language, $mod_name);
              $fields = array();
              $required = array();
              foreach ($focus->field_defs as $field_def) {
                if (!in_array($field_def['type'], $types_to_ignore)) {
                  if (empty($field_def['source'])) //Undefined index
                    $field_def['source'] = '';
                  if (in_array($field_def['name'],$special_field) || !in_array($field_def['source'], $sources_to_ignore)) {
                    if (!in_array($field_def['name'], $fields_to_ignore)) {
                      if (isset($field_def['vname'])) {
                        //Strip colons
                        $lbl = translate($field_def['vname'], $mod_name);
                        $lbl = preg_replace("/:/", "", $lbl);
                        if(!empty($lbl)){
                            if(isset($field_def['required']) && $field_def['required'] == true){
                               $required[$field_def['name']] = $lbl;
                            }
                            $fields[$field_def['name']] = $lbl;
                        }
                      }
                    }
                  }
                }
              }
          asort($required);
          asort($fields);
          return array('field_list' =>$fields,'required_list'=>$required);
        }
}
/***
* fetch module arrray to display on view and as enum with function
* 
* @param mixed $field
* @param mixed $options_only
* @param mixed $selected_id
* @param mixed $view
* @param mixed $record_id
*/
function fetchModuleArray($field,$options_only,$selected_id = '', $view = 'DetailView',$record_id='') {
    global $app_strings, $beanList, $app_list_strings;
    $ret = array();
    $aIgnoreModule = array('Activities', 'UT_WebToModule','SecurityGroups');
    require_once 'modules/ModuleBuilder/parsers/relationships/DeployedRelationships.php';
      $relatableModules = DeployedRelationships::findRelatableModules(false);
      $ret = array();
    foreach ($relatableModules as $module => $other) {
        if(in_array($module, $aIgnoreModule))
            continue;
        if(!empty($app_list_strings['moduleList'][$module]))
            $ret[$module] = $app_list_strings['moduleList'][$module];
        else
            $ret[$module] = $module;
    }
    asort($ret);
    if($view == 'EditView' || $view == 'MassUpdate' || $view == 'QuickCreate')
        return get_select_options_with_id($ret,$selected_id);
    else
    {
        if(!empty($ret[$selected_id]))
            return $ret[$selected_id];
    }
    return;
}
?>