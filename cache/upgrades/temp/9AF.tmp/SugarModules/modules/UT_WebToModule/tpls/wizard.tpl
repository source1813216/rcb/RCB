{* 
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*}
{literal}
    <script language="javascript" src = "modules/UT_WebToModule/javascript/jquery.smartWizard-2.0.min.js" ></script>
    <script language="javascript" src = "modules/UT_WebToModule/javascript/module_layout.js" ></script>
    <link href="modules/UT_WebToModule/css/smart_wizard_vertical.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        $(document).ready(function(){
            var required_list;
            var required_column_list = [];
            var duplicate_column_list = [];
            
            // Smart Wizard
            $('#wizard').smartWizard({
                        transitionEffect:'slide',
                        keyNavigation: false,
                        onFinish:onFinishCallback,
                        onLeaveStep:leaveAStepCallback,
                        labelNext:"Save and Continue",
                        labelPrevious:"Back",
                        labelFinish:"Publish",
                        enableAllSteps:true,
                        enableFinishButton:false
                        });
                function leaveAStepCallback(obj){
                var step_num= obj.attr('rel');
                return validateSteps(step_num);
              }
              function onFinishCallback() {
                   if(validateAllSteps()) {
                        $("#field_form_submit").submit();
                   }
              }
              getCaptchaKey();
              getFileControlLabel();
        });

    function validateAllSteps(){
       var isStepValid = true;
       if(validateSteps(1) == false)
           isStepValid = false;
       if(validateSteps(2) == false)
           isStepValid = false;
       if(validateSteps(3) == false)
           isStepValid = false;
       if(validateSteps(4) == false)
           isStepValid = false;
       if(validateSteps(5) == false)
           isStepValid = false;
       if(validateSteps(6) == false)
           isStepValid = false;
       if(!isStepValid){
          $('#wizard').smartWizard('showMessage','Please correct the errors in the steps and continue');
       }
       return isStepValid;
    }

    function validateSteps(step){
        var record = $('#record').val();
        var isStepValid = true;
      // validate step 1
      if(step == 1){
        if(validateStep1() == false ){
          isStepValid = false;
          $('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and move on.');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});
        }else{
            $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
            $('.msgBox').hide();
        }
      }
      else if(step == 2) {
          if(validateStep2() == false ) {
              isStepValid = false;
              $('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and move on.');
              $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});
            } else {
              var from  = $('#step2_from').val();
              var answer_text = $('#answer_text_modules').val();
              $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
              $('.msgBox').hide();
            }
      }
      // validate step3
      else if(step == 3) {
          if(validateStep3() == false ) {
              $('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and move on.');
              $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});
          }
          else
          {
              $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
              $('.msgBox').hide();
          }
      }
      // validate step4
      else if(step == 4) {
        if(validateStep4() == false ){
          isStepValid = false;
          $('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and move on.');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});
        }else {
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
          $('.msgBox').hide();
        }
      }
      else if(step == 5) {
          if(validateStep5() == false ){
              isStepValid = false;
              $('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and move on.');
              $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});
          }
          else
          {
              $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
              $('.msgBox').hide();
          }
      }
      else if(step == 6) {
          if(validateStep6() == false ){
              isStepValid = false;
              $('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and move on.');
              $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});
          }
          else
          {
              $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
              $('.msgBox').hide();
          }
      }
      return isStepValid;
    }
    /* field Layout on step2*/
    function validateStep2() {
            var form_module = $("#module_selected").val();
            $("#form_module").val(form_module);
            var count=0;
            var field_list_message ='';
            var flag=0;
          var column_1_id_module = '';
          var column_2_id_module = '';
          
          var available_field_for_required = '';
          var selected_required_field = '';
          var available_field_for_duplicate = '';
          var selected_duplicate_field = '';
          var selectedFields = [];
          $("#column_1_module ul > li").each(function(){
                column_1_id_module += $(this).attr("id") + ',';
                selectedFields.push($(this).attr("id"));
                if($(this).attr("id") in required_column_list){
                    selected_required_field += "<li class='ui-state-default sortable'  id='"+$(this).attr("id")+"'>"+$(this).text()+"</li>";
                }
                else
                {
                    available_field_for_required += "<li class='ui-state-default sortable'  id='"+$(this).attr("id")+"'>"+$(this).text()+"</li>";
                }
                
                if($(this).attr("id") in duplicate_column_list){
                    selected_duplicate_field += "<li class='ui-state-default sortable'  id='"+$(this).attr("id")+"'>"+$(this).text()+"</li>";
                }
                else
                {
                    available_field_for_duplicate += "<li class='ui-state-default sortable'  id='"+$(this).attr("id")+"'>"+$(this).text()+"</li>";
                }
                count++;
          });
          $("#column_2_module ul > li").each(function(){
                column_2_id_module += $(this).attr("id") + ',';
                selectedFields.push($(this).attr("id"));
                if($(this).attr("id") in required_column_list){
                    selected_required_field += "<li class='ui-state-default sortable'  id='"+$(this).attr("id")+"'>"+$(this).text()+"</li>";
                }
                else{
                    available_field_for_required += "<li class='ui-state-default sortable'  id='"+$(this).attr("id")+"'>"+$(this).text()+"</li>";
                }
                
                if($(this).attr("id") in duplicate_column_list){
                    selected_duplicate_field += "<li class='ui-state-default sortable'  id='"+$(this).attr("id")+"'>"+$(this).text()+"</li>";
                }
                else
                {
                    available_field_for_duplicate += "<li class='ui-state-default sortable'  id='"+$(this).attr("id")+"'>"+$(this).text()+"</li>";
                }
                count++;
          });
          if(count == 0) {
              alert("Please select atleast one field in form");
              return false;
          }
          
        $("#available_field_for_required ul").html(available_field_for_required);
        $("#selected_required_field ul").html(selected_required_field);
        $("#available_field_for_duplicate ul").html(available_field_for_duplicate);
        $("#selected_duplicate_field ul").html(selected_duplicate_field);
        
        var jsonOrderObj = [];
        jsonOrderObj.push({column_1:column_1_id_module,column_2:column_2_id_module});
        var json_order_array = encodeURIComponent(JSON.stringify(jsonOrderObj));
        $("#field_layout").val(json_order_array);
        
        var module_name = $("#module_selected").val();
        ajaxObject.getDefaultValueControls(module_name,selectedFields,$("#record_edit_form").val());
        return true;
    }
    function validateStep1() {
       return true;
    }
    function validateStep3() {
        var jsonOrderObj = {};
        
        $("#default_field_table :input:not([type=hidden])").each(function () {
            //console.log($(this).attr("type")+"||||||||||||||||||||"+$(this).attr("id")+"=================="+ $(this).val());
            
            if($(this).prop('type') == "checkbox")
            {
                if($(this).is(':checked'))
                    jsonOrderObj[$(this).attr("id")] = 1;
            }
            if($(this).prop('type') == "radio")
            {
                if($(this).is(':checked'))
                    jsonOrderObj[$(this).attr("id")] = $(this).val();
            }
            if($(this).prop('type') == "text" || $(this).prop('type') == 'select-one' || $(this).prop('type') == 'select-multiple' || $(this).prop('type') == "textarea")
            {
                if($(this).val() != "")
                    jsonOrderObj[$(this).attr("id")] = $(this).val();
            }
        });

        /*$('.default_field_class').each(function() {
            console.log($(this).attr("id")+"=================="+ $(this).val());
            jsonOrderObj[$(this).attr("id")] = $(this).val();
        });*/
       var json_order_array = encodeURIComponent(JSON.stringify(jsonOrderObj));
       $("#fields_with_default_value").val(json_order_array);
       return true;
    }
    function validateStep4() {
        var requiredFieldSelected = true;
        var selectedRequiredFieldCounter = 0;
        var actualRequiredFieldName = '';
        required_column_list = [];//Must be Blank
        $("#selected_required_field ul > li").each(function(){
            required_column_list[$(this).attr("id")] = $(this).text();
            actualRequiredFieldName += $(this).attr("id") + ',';
            selectedRequiredFieldCounter++;
        });
        
        if(selectedRequiredFieldCounter == 0) {
            alert("Please select atleast one required field in form");
            return false;
        }
        var jsonOrderObj = [];
        jsonOrderObj.push({requiredFields:actualRequiredFieldName});
        var json_order_array = encodeURIComponent(JSON.stringify(jsonOrderObj));
        $("#field_layout_required").val(json_order_array);
        return requiredFieldSelected;
    }
    function validateStep5() {
        var duplicateFieldSelected = true;
        var selectedDuplicateFieldCounter = 0;
        var actualDuplicateFieldName = '';
        duplicate_column_list = [];//Must be Blank
        $("#selected_duplicate_field ul > li").each(function(){
            duplicate_column_list[$(this).attr("id")] = $(this).text();
            actualDuplicateFieldName += $(this).attr("id") + ',';
            selectedDuplicateFieldCounter++;
        });
        
        /*
        if(selectedDuplicateFieldCounter == 0) {
            alert("Please select atleast one required field in form");
            return false;
        }
        */
        var jsonOrderObj = [];
        jsonOrderObj.push({duplicateFields:actualDuplicateFieldName});
        var json_order_array = encodeURIComponent(JSON.stringify(jsonOrderObj));
        $("#field_layout_duplicate").val(json_order_array);
        return duplicateFieldSelected;
    }
    function validateStep6() {
        if($("#web_to_lead_record_name").val() =='')
            return false;
        
        if($("#assigned_user_name").val() =='')
            return false;
        
        if($("#web_set_captcha").prop("checked") && $("#captcha_key").val() =='')
            return false;
    }
    function getCaptchaKey()
    {
        if($("#web_set_captcha").prop("checked"))
            $("#captcha_key_row").css('display','');
        else
            $("#captcha_key_row").css('display','none');
    }
    
    function getFileControlLabel()
    {
        if($("#file_control").prop("checked"))
            $("#file_control_label_row").css('display','');
        else
            $("#file_control_label_row").css('display','none');
    }
    
    function addRemoveAllFields(SelectAllFields, RemoveAllFields)
    {
        var addRemove = $("#fields_add_remove_button");
        var totalNoOfFields = 0;
        if(addRemove.val() == SelectAllFields)
        {
            $("#base_field_list ul > li").each(function() {
                if (totalNoOfFields % 2 == 0 && $(this).attr("id") != ""){
                    $("#column_1_module ul ").append($(this).clone());
                    $("#available_field_for_required ul").append($(this).clone());
                    $("#available_field_for_duplicate ul").append($(this).clone());
                }
                
                if (totalNoOfFields % 2 == 1 && $(this).attr("id") != ""){
                    $("#column_2_module ul ").append($(this).clone());
                    $("#available_field_for_required ul").append($(this).clone());
                    $("#available_field_for_duplicate ul").append($(this).clone());
                    
                }
                totalNoOfFields++;
            });
            $("#base_field_list ul > li").each(function() {
                $(this).remove();
            });
            $("#fields_add_remove_button").attr('value',SUGAR.language.get('app_strings','LBL_REMOVE_ALL_LEAD_FIELDS'));
            $("#fields_add_remove_button").attr('title',SUGAR.language.get('app_strings','LBL_REMOVE_ALL_LEAD_FIELDS'));
        }
        else if(addRemove.val() == RemoveAllFields)
        {
            var column1FieldsCount = 0;
            var column2FieldsCount = 0;
            var maximumFieldsCount = 0;
            $("#column_1_module ul > li").each(function() {
                column1FieldsCount++;
            });
            
            $("#column_2_module ul > li").each(function() {
                column2FieldsCount++;
            });
            if(column1FieldsCount > column2FieldsCount)
                maximumFieldsCount = column1FieldsCount;
            else if(column2FieldsCount > column1FieldsCount)
                maximumFieldsCount = column2FieldsCount;
            else if(column1FieldsCount == column2FieldsCount)
                maximumFieldsCount = column2FieldsCount;
            
            for(var i = 0; i <= maximumFieldsCount; i++){
                if(typeof($("#column_1_module ul li:nth-child("+i+")")) == "object")
                    $("#base_field_list ul ").append($("#column_1_module ul li:nth-child("+i+")").clone());
                
                if(typeof($("#column_2_module ul li:nth-child("+i+")")) == "object")
                    $("#base_field_list ul").append($("#column_2_module ul li:nth-child("+i+")").clone());
            }
            
            $("#column_1_module ul  > li").each(function(){ $(this).remove(); });
            $("#column_2_module ul  > li").each(function(){ $(this).remove(); });
            $("#available_field_for_required ul > li").each(function(){ $(this).remove(); });
            $("#available_field_for_duplicate ul > li").each(function(){ $(this).remove(); });
            $("#fields_add_remove_button").attr('value',SUGAR.language.get('app_strings','LBL_ADD_ALL_LEAD_FIELDS'));
            $("#fields_add_remove_button").attr('title',SUGAR.language.get('app_strings','LBL_ADD_ALL_LEAD_FIELDS'));
        }
    }
    </script>
{/literal}
<input type='hidden' id='record_edit_form' name='record_edit_form' value='{$record}'>
    <table border="0" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="middle">
            <h2>{$mod.LBL_MODULE_TITLE}&nbsp;</h2>
        </td>
    </tr>
    </table>
<form name="importstep3" id="importstep3">
<div id="wizard" class="swMain">
    <ul>
        <li><a href="#step-1">
                <label class="stepNumber">1</label>
                <span class="stepDesc">
                    {$mod.LBL_SELECT_FORM_MODULE}<br />
                    <small>{$mod.LBL_SELECT_FORM_MODULE_DESC}</small>
                </span>
            </a></li>
        <li><a href="#step-2">
                <label class="stepNumber">2</label>
                <span class="stepDesc">
                    {$mod.LBL_SELECT_FORM_FIELD}<br />
                    <small>{$mod.LBL_SELECT_FORM_FIELD_DESC}</small>
                </span>
            </a></li>
         <li><a href="#step-3">
                <label class="stepNumber">3</label>
                <span class="stepDesc">
                    {$mod.LBL_SET_DEFAULT_VALUE}<br />
                    <small>{$mod.LBL_SET_DEFAULT_VALUE_DESC}</small>
                </span>
            </a></li>
        <li><a href="#step-4">
            <label class="stepNumber">4</label>
            <span class="stepDesc">
                {$mod.LBL_SELECT_REQUIRED_FIELDS}<br />
                <small>{$mod.LBL_SELECT_REQUIRED_FIELDS_DESC}</small>
            </span>
        </a></li>
        <li><a href="#step-5">
                <label class="stepNumber">5</label>
                <span class="stepDesc">
                    {$mod.LBL_FORM_DUPLICATION}<br />
                    <small>{$mod.LBL_FORM_DUPLICATION_DESC}</small>
                </span>
            </a></li>
        <li><a href="#step-6">
                <label class="stepNumber">6</label>
                <span class="stepDesc">
                    {$mod.LBL_FORM_CONFIGURATION}<br />
                    <small>{$mod.LBL_FORM_CONFIGURATION_DESC}</small>
                </span>
            </a></li>
            
    </ul>
    <div id="step-1">
        <form name="edit_portal_url" id="edit_portal_url" method='POST'>
            <input type='hidden' id='step1_from' name='step1_from' value='portal_url'>
            <h2 class="StepTitle">Step 1: {$mod.LBL_SELECT_FORM_MODULE}</h2>
            <table class="edit view" width="100%">
                <tr>
                    <td scope="col" width="12.5%">
                        {$mod.LBL_SELECT_FORM_MODULE}<span class="required">*</span>
                    </td>
                    <td scope="col" width="37.5%">
                        <select id='module_selected' name="module_selected">{$module_options}</select>
                        <!--<input type='text' name='portal_url' id='portal_url' value='{$portal_url_data}' size="60">-->
                        <span id = 'urlsaved' name = 'urlsaved' style='display:none;'>Saved! <img src="themes/default/images/yes.gif" alt="Success"></span>
                    </td>
                    <td scope="col"  colspan='2'>&nbsp;</td>
                </tr>
            </table>
        </form>
    </div>
    <div id="step-2">
        <h2 class="StepTitle">Step 2: {$mod.LBL_SELECT_FORM_FIELD}</h2>
        <span id = 'modulessaved' name = 'modulessaved' style='display:none;'>Saved! <img src="themes/default/images/yes.gif" alt="Success"></span>
            {include file="modules/UT_WebToModule/tpls/wizard_step_2.tpl"}
    </div>
    <div id="step-3">
        <h2 class="StepTitle">Step 3: {$mod.LBL_SET_DEFAULT_VALUE}</h2>
            {include file="modules/UT_WebToModule/tpls/wizard_step_3.tpl"}
    </div>
    <div id="step-4">
        <h2 class="StepTitle">Step 4: {$mod.LBL_SELECT_REQUIRED_FIELDS}</h2>
        <span id = 'layoutsaved' name = 'layoutsaved' style='display:none;'>Saved! <img src="themes/default/images/yes.gif" alt="Success"></span>
        {include file="modules/UT_WebToModule/tpls/wizard_step_4.tpl"}
    </div>
    <div id="step-5">
        <h2 class="StepTitle">Step 5: {$mod.LBL_FORM_DUPLICATION}</h2>
        <span id = 'layoutsaved' name = 'layoutsaved' style='display:none;'>Saved! <img src="themes/default/images/yes.gif" alt="Success"></span>
        {include file="modules/UT_WebToModule/tpls/wizard_step_5.tpl"}
    </div>
    <div id="step-6">
        <h2 class="StepTitle">Step 6: {$mod.LBL_FORM_CONFIGURATION}</h2>
        <span id = 'layoutsaved' name = 'layoutsaved' style='display:none;'>Saved! <img src="themes/default/images/yes.gif" alt="Success"></span>
        {include file="modules/UT_WebToModule/tpls/wizard_step_6.tpl"}
    </div>
</div>
</form>