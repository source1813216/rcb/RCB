<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Ответственный(ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный(ая)',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено(ID)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано(ID)',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Править',
  'LBL_REMOVE' => 'Удалить',
  'LBL_LIST_FORM_TITLE' => 'Web To Lead Список',
  'LBL_MODULE_NAME' => 'Web To Lead',
  'LBL_MODULE_TITLE' => 'Web To Lead',
  'LBL_HOMEPAGE_TITLE' => 'Мой Web To Lead',
  'LNK_NEW_RECORD' => 'Создать Web To Lead',
  'LNK_LIST' => 'View Web To Lead',
  'LNK_IMPORT_UT_WEBTOMODULE' => 'Import Web To Module',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Web To Module',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Просмотр истории',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_UT_WEBTOMODULE_SUBPANEL_TITLE' => 'Web To Module',
  'LBL_NEW_FORM_TITLE' => 'Новый Web To Module',
  'LBL_SELECTED_FIELDS' => 'Selected Fields',
  'LBL_CAMPAIGN' => 'Campaign',
  'LBL_WEB_HEADER' => 'web_header',
  'LBL_WEB_DESCRIPTION' => 'Form Description',
  'LBL_WEB_SUBMIT' => 'Submit Button Label',
  'LBL_POST_URL' => 'Post URL',
  'LBL_REDIRECT_URL' => 'Redirect URL',
  'LBL_CAMPAIGN_NAME' => 'Related Campaign',
  'LBL_CAMPAIGN_NAME_CAMPAIGN_ID' => 'campaign name (related Campaign ID)',
  'LBL_WEB_FOOTER' => 'Form Footer',
  'LBL_WEB_TO_LEAD' => 'Nuevo Formulario de Cliente Potencial',
  'LBL_WEB_TO_LEAD_FORM_TITLE1' => 'Create Lead Form: Select fields',
  'LBL_WEB_TO_LEAD_FORM_TITLE2' => 'Create Lead Form: Form properties',
  'LBL_LEAD_NOTIFY_CAMPAIGN' => 'Related Campaign:',
  'LBL_AVALAIBLE_FIELDS_HEADER' => 'Available Fields',
  'LBL_LEAD_FORM_FIRST_HEADER' => 'Lead Form (First Column)',
  'LBL_LEAD_FORM_SECOND_HEADER' =>'Lead Form (Second Column)',
  'LBL_SELECT_REQUIRED_LEAD_FIELDS' => 'Please select required fields:',
  'LBL_DEFINE_LEAD_HEADER' => 'Form header:',
  'LBL_DESCRIPTION_LEAD_FORM' => 'Form description:',
  'LBL_DEFINE_LEAD_SUBMIT' => 'Submit button label:',
  'LBL_DEFINE_LEAD_POST_URL' => 'Post URL:',
  'LBL_DEFINE_LEAD_REDIRECT_URL' => 'Redirect URL:',
  'LBL_LEAD_FOOTER' => 'Form footer:',
  'LBL_LEAD_DEFAULT_HEADER' => 'Webform Header',
  'LBL_DESCRIPTION_TEXT_LEAD_FORM' => 'Webform Description',
  'LBL_DEFAULT_LEAD_SUBMIT' => 'Submit',
  'LBL_DRAG_DROP_COLUMNS' => 'Drag and drop lead fields in column 1 & 2',
  'LBL_LEAD_FORM_WIZARD' => 'Lead Form Wizard',
  'LBL_NAME_RECORD_NAME' => 'Web to module record name',
  'LBL_NAME_RECORD_NAME_HELP' => 'This name field is used to identify record only.',
  'LBL_PROVIDE_WEB_TO_LEAD_FORM_FIELDS' =>'Please provide all the required fields',
  'LBL_NOT_VALID_EMAIL_ADDRESS' =>'Not a valid email address',
  'LBL_DOWNLOAD_WEB_TO_LEAD' => 'Download Template',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_SET_CAPTCHA' => 'Google reCaptcha:',
  'LBL_AUTO_RESPONSE_TEMPLATE' => 'Module auto-response template',
  'LBL_SELECT_FORM_MODULE' =>'Select module:',
  'LBL_SELECT_FORM_MODULE_DESC' =>'Select module to create form as base',
  'LBL_SELECT_FORM_FIELD' =>'Select module field(s)',
  'LBL_SELECT_FORM_FIELD_DESC' =>'Select field(s) to have in form',
  'LBL_AVAILABLE_FIELDS' =>'Available fields',
  'LBL_FIELD_COLUMN1' =>'Form - first column',
  'LBL_FIELD_COLUMN2' =>'Form - second column',
  'LBL_CAPTCHA_KEY' => 'Captcha key:',
  'WEB_AUTO_RESPONSE_TEMPLATE_HELP_TEXT' => 'The auto-response will be sent only if the selected module has Email Address(email1) selected within.',
  'LBL_SELECT_REQUIRED_FIELDS' => 'Select Required Field(s)',
  'LBL_SELECT_REQUIRED_FIELDS_DESC' => 'Mark field(s) required into WebForm',
  'LBL_MARK_FIELDS_REQUIRED' => 'Mark Field(s) Required',
  'LBL_SELECTED_REQUIRED_FIELDS' => 'Selected Required Field(s)',
  'LBL_AVAILABLE_FIELDS_TO_MARK_REQUIRED' => 'Available required field(s)',
  'LBL_FORM_DUPLICATION' => 'Duplication',
  'LBL_FORM_DUPLICATION_DESC' => 'Set field(s) to check duplication',
  'LBL_SELECTED_DUPLICATE_FIELDS' => 'Check duplicates on',
  'LBL_AVAILABLE_FIELDS_TO_MARK_DUPLICATE' => 'Available field(s)',
  'LBL_ASSIGN_SECURITY_GROUPS' => 'Security groups:',
  'LBL_ASSIGN_SECURITY_GROUPS_TEXT' => 'Relate security groups with the record created from WebToModule form.',
  'LBL_PREVENT_DUPLICATE_UPDATE' => 'Prevent duplicate update:',
  'LBL_PREVENT_DUPLICATE_UPDATE_TEXT' => 'Prevent record updation if duplicate record will be found into system. Pass flag in redirect URL for user reference.',
  'LBL_WEBFORM_UPLOAD_URL' => 'Webform Upload URL:',
  'LBL_WEBFORM_UPLOAD_URL_HELP' => 'Provide the domain where you will put this form.',
  'LBL_SET_DEFAULT_VALUE' => 'Set Default Value',
  'LBL_SET_DEFAULT_VALUE_DESC' => 'Set Default Value',
  'LBL_DEFAULT_VALUE_FIELDS' => 'Default Value Fields',
  'LBL_FILE_CONTROL' => 'Need File Control ?:',
  'LBL_FILE_CONTROL_HELP' => 'File Control will be added into your WebForm, once you checked this.Only one file control is allow in WebForm.',
  'LBL_FILE_CONTROL_TEXT_LABEL' => 'File Control Label:',
  'LBL_FIELDS_MARK_AS_HIDDEN' => 'Hidden Field(s)',
  'LBL_FIELD_LABEL' => 'Field Label',
  'LBL_FIELD_CONTROL' => 'Field Control',
  'LBL_MARK_AS_HIDDEN' => 'Mark As Hidden ?',
);