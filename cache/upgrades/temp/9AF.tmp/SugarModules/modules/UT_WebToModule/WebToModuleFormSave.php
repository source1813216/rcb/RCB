<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('include/formbase.php');
require_once('include/SugarTinyMCE.php');



global $mod_strings,$sugar_config;
global $app_strings;


//-----------begin replacing text input tags that have been marked with text area tags
//get array of text areas strings to process
$bodyHTML = html_entity_decode($_REQUEST['body_html'],ENT_QUOTES);
//Bug53791
//$bodyHTML = str_replace(chr(160), " ", $bodyHTML);

while (strpos($bodyHTML, "ta_replace") !== false){

	//define the marker edges of the sub string to process (opening and closing tag brackets)
	$marker = strpos($bodyHTML, "ta_replace");
	$start_border = strpos($bodyHTML, "input", $marker) - 1;// to account for opening '<' char;
	$end_border = strpos($bodyHTML, '>', $start_border); //get the closing tag after marker ">";

	//extract the input tag string
	$working_str = substr($bodyHTML, $marker-3, $end_border-($marker-3) );

	//replace input markup with text areas markups
	$new_str = str_replace('input','textarea',$working_str);
        //If Defaule value is available(Start)
        $intermediateDefaultValue = $defaultValue = array();
        $intermediateDefaultValue = explode('value="', $new_str);
        if(is_array($intermediateDefaultValue) && array_key_exists(1, $intermediateDefaultValue) && !empty($intermediateDefaultValue[1]))
            $defaultValue = explode('"', $intermediateDefaultValue[1]);
        //If Defaule value is available(End)
	$new_str = str_replace("type='text'", ' ', $new_str);
        if(is_array($defaultValue) && array_key_exists(0, $defaultValue) && !empty($defaultValue[0]))
            $new_str = $new_str . '>'.$defaultValue[0].'</textarea';
        else
            $new_str = $new_str . '></textarea';

	//replace the marker with generic term
	$new_str = str_replace('ta_replace', 'sugarslot', $new_str);

	//merge the processed string back into bodyhtml string
	$bodyHTML = str_replace($working_str , $new_str, $bodyHTML);
}
//<<<----------end replacing marked text inputs with text area tags

$ut_web_to_lead_record_id = $ut_web_to_lead_record_name = $ut_web_header = $ut_web_description = $ut_web_submit = '';
$ut_post_url = $ut_redirect_url = $ut_campaign_id = $ut_assigned_user_id = $ut_web_footer = $savedSelectedFields = $savedRequiredFields = $ut_web_set_captcha = $ut_captcha_key = "";
$ut_auto_response_template = $ut_assign_security_groups = $ut_web_prevent_duplicate_update = $ut_webform_upload_url = $ut_file_control = $ut_file_control_label = '';
$selected_fields = array();
$required_fields = $savedDuplicateFields = $savedDefaultFields = $savedHiddenFields = array();
if(array_key_exists('ut_web_to_lead_record', $_REQUEST) && !empty($_REQUEST['ut_web_to_lead_record']))
    $ut_web_to_lead_record_id = $_REQUEST['ut_web_to_lead_record'];
if(array_key_exists('ut_web_to_lead_record_name', $_REQUEST) && !empty($_REQUEST['ut_web_to_lead_record_name'])) $ut_web_to_lead_record_name = $_REQUEST['ut_web_to_lead_record_name'];
if(array_key_exists('ut_web_header', $_REQUEST) && !empty($_REQUEST['ut_web_header'])) $ut_web_header = $_REQUEST['ut_web_header'];
if(array_key_exists('ut_web_description', $_REQUEST) && !empty($_REQUEST['ut_web_description'])) $ut_web_description = $_REQUEST['ut_web_description'];
if(array_key_exists('ut_web_submit', $_REQUEST) && !empty($_REQUEST['ut_web_submit'])) $ut_web_submit = $_REQUEST['ut_web_submit'];
if(array_key_exists('ut_post_url', $_REQUEST) && !empty($_REQUEST['ut_post_url'])) $ut_post_url = $_REQUEST['ut_post_url'];
if(array_key_exists('ut_redirect_url', $_REQUEST) && !empty($_REQUEST['ut_redirect_url'])) $ut_redirect_url = $_REQUEST['ut_redirect_url'];
if(array_key_exists('ut_campaign_id', $_REQUEST) && !empty($_REQUEST['ut_campaign_id'])) $ut_campaign_id = $_REQUEST['ut_campaign_id'];
if(array_key_exists('ut_assigned_user_id', $_REQUEST) && !empty($_REQUEST['ut_assigned_user_id'])) $ut_assigned_user_id = $_REQUEST['ut_assigned_user_id'];
if(array_key_exists('ut_auto_response_template', $_REQUEST) && !empty($_REQUEST['ut_auto_response_template'])) $ut_auto_response_template = $_REQUEST['ut_auto_response_template'];
if(array_key_exists('ut_web_footer', $_REQUEST) && !empty($_REQUEST['ut_web_footer'])) $ut_web_footer = $_REQUEST['ut_web_footer'];
if(array_key_exists('ut_captcha_key', $_REQUEST) && !empty($_REQUEST['ut_captcha_key'])) $ut_captcha_key = $_REQUEST['ut_captcha_key'];
if(array_key_exists('ut_webform_upload_url', $_REQUEST) && !empty($_REQUEST['ut_webform_upload_url'])) $ut_webform_upload_url = $_REQUEST['ut_webform_upload_url'];
if(array_key_exists('ut_file_control_label', $_REQUEST) && !empty($_REQUEST['ut_file_control_label'])) $ut_file_control_label = $_REQUEST['ut_file_control_label'];

if(array_key_exists('web_modules', $_REQUEST) && !empty($_REQUEST['web_modules'])) $ut_web_modules = $_REQUEST['web_modules'];
if(array_key_exists('ut_selected_security_groups', $_REQUEST) && !empty($_REQUEST['ut_selected_security_groups'])) $ut_assign_security_groups = encodeMultienumValue($_REQUEST['ut_selected_security_groups']);

if(array_key_exists("ut_selected_fields_first_column", $_REQUEST) && is_array($_REQUEST['ut_selected_fields_first_column']) && count($_REQUEST['ut_selected_fields_first_column']))
    $selected_fields['colsFirst'] = $_REQUEST['ut_selected_fields_first_column'];
else
    $selected_fields['colsFirst'] = array();
if(array_key_exists("ut_selected_fields_second_column", $_REQUEST) && is_array($_REQUEST['ut_selected_fields_second_column']) && count($_REQUEST['ut_selected_fields_second_column']))
    $selected_fields['colsSecond'] = $_REQUEST['ut_selected_fields_second_column'];
else
    $selected_fields['colsSecond'] = array();

if(array_key_exists("ut_selected_required_fields", $_REQUEST) && is_array($_REQUEST['ut_selected_required_fields']) && count($_REQUEST['ut_selected_required_fields']))
    $required_fields['requiredFields'] = $_REQUEST['ut_selected_required_fields'];
else
    $required_fields['requiredFields'] = array();
    
if(array_key_exists("ut_selected_duplicate_fields", $_REQUEST) && is_array($_REQUEST['ut_selected_duplicate_fields']) && count($_REQUEST['ut_selected_duplicate_fields']))
    $duplicate_fields['duplicateFields'] = $_REQUEST['ut_selected_duplicate_fields'];
else
    $duplicate_fields['duplicateFields'] = array();

if(array_key_exists("ut_selected_hidden_fields", $_REQUEST) && is_array($_REQUEST['ut_selected_hidden_fields']) && count($_REQUEST['ut_selected_hidden_fields']))
	$hiddenFields["hiddenFields"] = $_REQUEST['ut_selected_hidden_fields'];
else
    $hiddenFields['hiddenFields'] = array();

if(array_key_exists("ut_selected_default_fields_name", $_REQUEST) && is_array($_REQUEST['ut_selected_default_fields_name']) && count($_REQUEST['ut_selected_default_fields_name'])
&& array_key_exists("ut_selected_default_fields_values", $_REQUEST) && is_array($_REQUEST['ut_selected_default_fields_values']) && count($_REQUEST['ut_selected_default_fields_values']))
{
	for($counter = 0;$counter < count($_REQUEST['ut_selected_default_fields_name']);$counter++)
		$savedDefaultFields[$_REQUEST['ut_selected_default_fields_name'][$counter]] = $_REQUEST['ut_selected_default_fields_values'][$counter];
}
else
	$savedDefaultFields = array();

if(array_key_exists('web_set_captcha', $_REQUEST) && !empty($_REQUEST['web_set_captcha'])) $ut_web_set_captcha = $_REQUEST['web_set_captcha'];
$savedCaptcha = 0;
if($ut_web_set_captcha == "Yes") 
	$savedCaptcha = 1;
else
	$ut_captcha_key = "";

if(array_key_exists('file_control', $_REQUEST) && !empty($_REQUEST['file_control'])) $ut_file_control = $_REQUEST['file_control'];
$savedFileControl = 0;
if($ut_file_control == "Yes") 
	$savedFileControl = 1;
else
	$ut_file_control_label = "";

if(array_key_exists('web_prevent_duplicate_update', $_REQUEST) && !empty($_REQUEST['web_prevent_duplicate_update'])) $ut_web_prevent_duplicate_update = $_REQUEST['web_prevent_duplicate_update'];
$savedPreventDuplicateUpdate = 0;
if($ut_web_prevent_duplicate_update == "Yes") 
	$savedPreventDuplicateUpdate = 1;


$savedSelectedFields = base64_encode(json_encode($selected_fields));
$savedRequiredFields = base64_encode(json_encode($required_fields));
$savedDuplicateFields = base64_encode(json_encode($duplicate_fields));
$savedDefaultFields = base64_encode(json_encode($savedDefaultFields));
$savedHiddenFields = base64_encode(json_encode($hiddenFields));

if($_REQUEST['isNewRecord'] == "No"){
$oUTWebToModule = BeanFactory::getBean("UT_WebToModule",$ut_web_to_lead_record_id);
}
else{
    $oUTWebToModule = BeanFactory::getBean("UT_WebToModule");
    $oUTWebToModule->new_with_id=true;
    $oUTWebToModule->id=$ut_web_to_lead_record_id;
}
$oUTWebToModule->name = $ut_web_to_lead_record_name;
$oUTWebToModule->web_header = $ut_web_header;
$oUTWebToModule->web_description = $ut_web_description;
$oUTWebToModule->web_submit = $ut_web_submit;
$oUTWebToModule->post_url = $ut_post_url;
$oUTWebToModule->redirect_url = $ut_redirect_url;
$oUTWebToModule->campaign_id = $ut_campaign_id;
$oUTWebToModule->assigned_user_id = $ut_assigned_user_id;
$oUTWebToModule->selected_fields = $savedSelectedFields;
$oUTWebToModule->required_fields = $savedRequiredFields;
$oUTWebToModule->duplicate_fields = $savedDuplicateFields;
$oUTWebToModule->web_footer = $ut_web_footer;
$oUTWebToModule->set_captcha = $savedCaptcha;
$oUTWebToModule->captcha_key = $ut_captcha_key;
$oUTWebToModule->auto_response_template = $ut_auto_response_template;
$oUTWebToModule->web_modules = $ut_web_modules;
$oUTWebToModule->assign_security_groups = $ut_assign_security_groups;
$oUTWebToModule->prevent_duplicate_update = $savedPreventDuplicateUpdate;
$oUTWebToModule->webform_upload_url = $ut_webform_upload_url;
$oUTWebToModule->default_value_fields = $savedDefaultFields;
$oUTWebToModule->fields_mark_as_hidden = $savedHiddenFields;
$oUTWebToModule->file_control = $savedFileControl;
$oUTWebToModule->file_control_label = $ut_file_control_label;
$oUTWebToModule->save();
$webToModuleFileName = $oUTWebToModule->id.".html";

$directoryName = "UrdhvaTechWebToModule";
if(!file_exists($directoryName) && !is_dir($directoryName))
{
    mkdir($directoryName, 0755, true);
}

$form_file = $directoryName."/".$webToModuleFileName;

$SugarTiny =  new SugarTinyMCE();
$html = $SugarTiny->cleanEncodedMCEHtml($bodyHTML);  

//Check to ensure we have <html> tags in the form. Without them, IE8 will attempt to display the page as XML.
if (stripos($html, "<html") === false) {
    $langHeader = get_language_header();
    $html = "<html {$langHeader}><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body><style type='text/css'></style>" . $html . "</body></html>";
}
file_put_contents($form_file, $html);
global $sugar_config;
SugarApplication::redirect($sugar_config['site_url']."/index.php?module=UT_WebToModule&action=index&return_module=UT_WebToModule&return_action=DetailView");