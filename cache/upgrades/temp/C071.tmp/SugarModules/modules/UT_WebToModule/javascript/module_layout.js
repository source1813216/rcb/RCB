/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
$(function() {
    //Enable sorting 
    $( ".droptrue" ).sortable({
        connectWith: ".connectedSortable",
        opacity: 0.6,
        cursor: 'move',
        scroll: true
    });
    $( ".list_droptrue" ).sortable({
        connectWith: ".list_connectedSortable",
        opacity: 0.6,
        cursor: 'move',
        scroll: true
    });
});

var ajaxObject = {
        handleSuccess:function(o) {
             SUGAR.ajaxUI.hideLoadingPanel();
            if(o.responseText != '') {
                res = eval('(' + o.responseText + ')');
                if(res.result =='field_list') {
                    if(res.field_list == "" && typeof(res.field_list) == "string")
                    {
                        $("#fields_add_remove_button").attr('value','Remove all fields');
                        $("#fields_add_remove_button").attr('title','Remove all fields');
                    }
                    $("#base_field_list ul").html(res.field_list);
                    $("#column_1_module ul").html(res.field_column1);
                    $("#column_2_module ul").html(res.field_column2);
                    $("#available_field_for_required ul").html(res.available_field_for_required);
                    $("#selected_required_field ul").html(res.selected_required_field);
                    $("#available_field_for_duplicate ul").html(res.available_field_for_duplicate);
                    $("#selected_duplicate_field ul").html(res.selected_duplicate_field);
                    $("#default_field_table").html(res.default_controls);//Default Fields into Edit Mode
                    required_column_list = res.required_column_list;
                    duplicate_column_list = res.duplicate_column_list;
                    required_list = res.required_list;          //Global variable to fill required field
                }
                else if(res.result == 'get_default_controls') {
                    $("#default_field_table").html(res.default_controls);//Default Fields into Create Mode AND In Edit Mode any new fields are added at that time
                }
                else if(res.result == 'suggestion') {
                    $("#"+res.element_suggestion_id).html(res.field_suggestion);
                }
                else if(res.result =='update_fields') {
                    $("#"+res.ele_id).html(res.field_option);
                    //$("#"+res.element_suggestion_id).html(res.field_suggestion);
                }
                else if(res.result == 'update_default_dropdown') {
                    $("#"+res.ele_id).css('display','block');
                    $("#"+res.ele_id).html(res.drop_option);
                }
            }
        },
        handleFailure:function(o) {
            // Failure handler
        },
        getFieldList:function(module,record) {
            SUGAR.ajaxUI.showLoadingPanel();
                $("#web_modules").val(module);  //Set module name to submit
                YAHOO.util.Connect.asyncRequest('POST', 'index.php?module=UT_WebToModule&to_pdf=1&entryPoint=getFieldList&fetchField=true&ele_module='+module+'&record='+record, ajaxCallback, "");
        },
        getDefaultValueControls:function(module_name,selectedFields,record) {
            SUGAR.ajaxUI.showLoadingPanel();
            YAHOO.util.Connect.asyncRequest('POST', 'index.php?module=UT_WebToModule&to_pdf=1&entryPoint=getDefaultValueControls&getDefaultValueControls=true&record='+record+'&ele_module='+module_name+'&selectedFields='+selectedFields, ajaxCallback, "");
        },
};
var ajaxCallback = {
    success:ajaxObject.handleSuccess,
    failure:ajaxObject.handleFailure,
    scope: ajaxObject
};
$(document).ready(function() {
    $("#module_selected").change(function(){
        var ans = confirm("Are you sure you want to change module, It will reset the layout of fields if you set already!. ");
        if(ans == true) {
            var module_name = $(this).val();
            var record = '';
            $("#default_field_table").html();
            ajaxObject.getFieldList(module_name,record);
            ajaxObject.getDefaultValueControls(module_name,"",record);
        }
    });
    //Onload
    var module_name = $("#module_selected").val();
    var record = $("#record_edit_form").val();
    ajaxObject.getFieldList(module_name,record);
});