<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
class UT_WebToModuleViewDetail extends ViewDetail
{
    public function __construct()
    {
        parent::ViewDetail();
    }
    
    public function display() {
        if(!empty($this->bean->id))
        {
            $fileName = "UrdhvaTechWebToModule/".$this->bean->id.".html";
            if(sugar_is_file($fileName))
            {
                $downLoadFileLink="<a href='index.php?module=UT_WebToModule&entryPoint=customDownload&id=".$this->bean->id.".html"."&downloadFilename=".$this->bean->id.".html"."'>".$this->bean->name.".html"."</a>";
                $this->ss->assign("DOWNLOADWEBTOMODULEFORM", $downLoadFileLink);
            }
            else
                $this->ss->assign("DOWNLOADWEBTOMODULEFORM", "");
        }
        else
            $this->ss->assign("DOWNLOADWEBTOMODULEFORM", "");
        parent::display();
    }
}