<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
$entry_point_registry['WebToModuleCapture'] = array('file' => 'modules/UT_WebToModule/WebToModuleCapture.php', 'auth' => false);
$entry_point_registry['customDownload'] = array('file' => 'modules/UT_WebToModule/customDownload.php', 'auth' => false);
$entry_point_registry['getFieldList'] = array('file' => 'modules/UT_WebToModule/ajax_handler.php', 'auth' => true);
$entry_point_registry['getDefaultValueControls'] = array('file' => 'modules/UT_WebToModule/ajax_handler.php', 'auth' => true);