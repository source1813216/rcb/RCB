<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('include/EditView/EditView2.php');

require_once('modules/UT_WebToModule/utils.php');

global $mod_strings, $app_list_strings, $app_strings, $current_user, $import_bean_map;
global $import_file_name, $theme,$db,$app_list_strings,$db;
$lead = new Lead();
$fields = array();
$firstColumnFields = array();
$secondColumnFields = array();

$xtpl=new XTemplate ('modules/UT_WebToModule/WebToModuleCreation.html');
$xtpl->assign("MOD", $mod_strings);
$xtpl->assign("APP", $app_strings);
if(isset($_REQUEST['module']))
{
    $xtpl->assign("MODULE", $_REQUEST['module']);
}
if(isset($_REQUEST['return_module']))
{
    $xtpl->assign("RETURN_MODULE", $_REQUEST['return_module']);
}
if(isset($_REQUEST['return_id']))
{
    $xtpl->assign("RETURN_ID", $_REQUEST['return_id']);
}
if(isset($_REQUEST['return_id']))
{
    $xtpl->assign("RETURN_ACTION", $_REQUEST['return_action']);
}
if(isset($_REQUEST['record']))
{
    $xtpl->assign("RECORD", $_REQUEST['record']);
}

$xtpl->assign("WEB_TO_LEAD_RECORD_NAME_VALUE","");
$xtpl->assign("WEB_HEADER_VALUE",$mod_strings["LBL_LEAD_DEFAULT_HEADER"]);
$xtpl->assign("WEB_DESCRIPTION_VALUE",$mod_strings["LBL_DESCRIPTION_TEXT_LEAD_FORM"]);
$xtpl->assign("WEB_SUBMIT_VALUE",$mod_strings["LBL_DEFAULT_LEAD_SUBMIT"]);
$xtpl->assign("POST_URL_VALUE",$sugar_config['site_url'].'/index.php?module=UT_WebToModule&entryPoint=WebToModuleCapture');
$xtpl->assign("REDIRECT_URL_VALUE","http://");
$xtpl->assign("CAMPAIGN_NAME_VALUE","");
$xtpl->assign("CAMPAIGN_ID_VALUE","");
$xtpl->assign("ASSIGNED_USER_NAME_VALUE",$current_user->user_name);
$xtpl->assign("ASSIGNED_USER_ID_VALUE",$current_user->id);
$xtpl->assign("WEB_FOOTER_VALUE","");
global $theme;
global $currentModule;

$ev = new EditView;

$selectedFields = array("colsFirst"=>array(),"colsSecond"=>array());
if(isset($_REQUEST['record']) && !empty($_REQUEST['record']))
{
    $oWebToModule = BeanFactory::getBean("UT_WebToModule",$_REQUEST['record']);
    if(!empty($oWebToModule->selected_fields))
    {
        $selectedFields = (array)json_decode(base64_decode($oWebToModule->selected_fields));
        if(is_array($selectedFields) && count($selectedFields) > 0)
        {
            
        }
    }
    if(!empty($oWebToModule->name)) $xtpl->assign("WEB_TO_LEAD_RECORD_NAME_VALUE",$oWebToModule->name);
    if(!empty($oWebToModule->web_header)) $xtpl->assign("WEB_HEADER_VALUE",$oWebToModule->web_header);
    if(!empty($oWebToModule->web_description)) $xtpl->assign("WEB_DESCRIPTION_VALUE",$oWebToModule->web_description);
    if(!empty($oWebToModule->web_submit)) $xtpl->assign("WEB_SUBMIT_VALUE",$oWebToModule->web_submit);
    if(!empty($oWebToModule->post_url)) $xtpl->assign("POST_URL_VALUE",$oWebToModule->post_url);
    if(!empty($oWebToModule->redirect_url)) $xtpl->assign("REDIRECT_URL_VALUE",$oWebToModule->redirect_url);
    
    if(!empty($oWebToModule->campaign_id)) 
    {
        $xtpl->assign("CAMPAIGN_ID_VALUE",$oWebToModule->campaign_id);
        $selectCampaign = "SELECT id,name FROM campaigns WHERE deleted = '0' AND id = '".$oWebToModule->campaign_id."'";
        $selectCampaignRS = $db->query($selectCampaign,true);
        $selectCampaignRow = $db->fetchByAssoc($selectCampaignRS);
        $xtpl->assign("CAMPAIGN_NAME_VALUE",$selectCampaignRow["name"]);
    }
    
    if(!empty($oWebToModule->assigned_user_id)) 
    {
        $xtpl->assign("ASSIGNED_USER_ID_VALUE",$oWebToModule->assigned_user_id);
        $selectUser = "SELECT id,user_name FROM users WHERE deleted = '0' AND status = 'Active' AND id = '".$oWebToModule->assigned_user_id."'";
        $selectUserRS = $db->query($selectUser,true);
        $selectUserRow = $db->fetchByAssoc($selectUserRS);
        $xtpl->assign("ASSIGNED_USER_NAME_VALUE",$selectUserRow["user_name"]);
    }
    if(!empty($oWebToModule->web_footer)) $xtpl->assign("WEB_FOOTER_VALUE",$oWebToModule->web_footer);
    
    //==============Auto Response Email Templates(Start)=================
    $autoResponseEmailTemplates = "";
    if(!empty($oWebToModule->auto_response_template))
        $autoResponseEmailTemplates = get_email_template($oWebToModule->auto_response_template);
    else
        $autoResponseEmailTemplates = get_email_template("");
        
    $xtpl->assign("AUTO_RESPONSE_EMAIL_TEMPLATES",get_select_options_with_id($systemEmailTempates, $autoResponseEmailTemplates));
    //==============Auto Response Email Templates(End)=================
    
    if($oWebToModule->set_captcha == "1")
        $xtpl->assign("WEB_SET_CAPTCHA_VALUE",'checked=""');
    else
        $xtpl->assign("WEB_SET_CAPTCHA_VALUE",'');
        
    $recordLink = "<a href='".$sugar_config["site_url"]."/index.php?module=UT_WebToModule&record=".$_REQUEST['record']."&action=DetailView'>".$oWebToModule->name."</a>";
    $xtpl->assign("TITLE1","<h2>".$recordLink." <span class='pointer'>»</span> ".$mod_strings['LBL_EDIT_BUTTON']."</h2>");
    $xtpl->assign("TITLE2","<h2>".$recordLink." <span class='pointer'>»</span> ".$mod_strings['LBL_EDIT_BUTTON']."</h2>");
}
else
{
    $xtpl->assign("TITLE1","<h2>".$mod_strings['LNK_NEW_RECORD']."</h2>");
    $xtpl->assign("TITLE2","<h2>".$mod_strings['LNK_NEW_RECORD']."</h2>");
}


$site_url = $sugar_config['site_url'];
$web_post_url = $site_url.'/index.php?module=UT_WebToModule&entryPoint=WebToModuleCapture';
$json = getJSONobj();
// Users Popup
$popup_request_data = array(
	'call_back_function' => 'set_return',
	'form_name' => 'WebToModuleCreation',
	'field_to_name_array' => array(
		'id' => 'assigned_user_id',
		'user_name' => 'assigned_user_name',
		),
	);
$xtpl->assign('encoded_users_popup_request_data', $json->encode($popup_request_data));

//Campaigns popup
$popup_request_data = array(
		'call_back_function' => 'set_return',
		'form_name' => 'WebToModuleCreation',
		'field_to_name_array' => array(
			'id' => 'campaign_id',
			'name' => 'campaign_name',
		),
);
$encoded_users_popup_request_data = $json->encode($popup_request_data);
$xtpl->assign('encoded_campaigns_popup_request_data' , $json->encode($popup_request_data));

//create the cancel button
$cancel_buttons_html = "<input class='button' onclick=\"this.form.action.value='".$_REQUEST['return_action']."'; this.form.module.value='".$_REQUEST['return_module']."';\" type='submit' name='cancel' value='".$app_strings['LBL_BACK']."'/>";
$xtpl->assign("CANCEL_BUTTON", $cancel_buttons_html );

$field_defs_js = "var field_defs = {'Contacts':[";

//bug: 47574 - make sure, that webtomodule_email1 field has same required attribute as email1 field
if(isset($lead->field_defs['webtomodule_email1']) && isset($lead->field_defs['email1']) && isset($lead->field_defs['email1']['required'])){
    $lead->field_defs['webtomodule_email1']['required'] = $lead->field_defs['email1']['required'];
}

$count= 0;


foreach($lead->field_defs as $field_def)
{
	$email_fields = false;
    if($field_def['name']== 'email1' || $field_def['name']== 'email2')
    {
    	$email_fields = true;
    }
	  if($field_def['name']!= 'account_name'){
	    if( ( $field_def['type'] == 'relate' && empty($field_def['custom_type']) )
	    	|| $field_def['type'] == 'assigned_user_name' || $field_def['type'] =='link'
	    	|| (isset($field_def['source'])  && $field_def['source']=='non-db' && !$email_fields) || $field_def['type'] == 'id')
	    {
	        continue;
	    }
	   }
	    if($field_def['name']== 'deleted' || $field_def['name']=='converted' || $field_def['name']=='date_entered'
	        || $field_def['name']== 'date_modified' || $field_def['name']=='modified_user_id'
	        || $field_def['name']=='assigned_user_id' || $field_def['name']=='created_by'
	        || $field_def['name']=='team_id')
	    {
	    	continue;
	    }


    $field_def['vname'] = preg_replace('/:$/','',translate($field_def['vname'],'Leads'));

     //$cols_name = "{'".$field_def['vname']."'}";
     $col_arr = array();
     if((isset($field_def['required']) && $field_def['required'] != null && $field_def['required'] != 0)
     	|| $field_def['name']=='last_name'
     	){
        $cols_name=$field_def['vname'].' '.$app_strings['LBL_REQUIRED_SYMBOL'];
        $col_arr[0]=$cols_name;
        $col_arr[1]=$field_def['name'];
        if(in_array($field_def['name'], $selectedFields["colsFirst"])) array_push ($firstColumnFields, $col_arr);
        if(in_array($field_def['name'], $selectedFields["colsSecond"])) array_push ($secondColumnFields, $col_arr);
        $col_arr[2]=true;
     }
     else{
	     $cols_name=$field_def['vname'];
	     $col_arr[0]=$cols_name;
	     $col_arr[1]=$field_def['name'];
             if(in_array($field_def['name'], $selectedFields["colsFirst"])) array_push ($firstColumnFields, $col_arr);
             if(in_array($field_def['name'], $selectedFields["colsSecond"])) array_push ($secondColumnFields, $col_arr);
     }
     if(isset($_REQUEST['record']) && !empty($_REQUEST['record']))
     {
        if(! in_array($cols_name, $fields) && !in_array($col_arr[1], $selectedFields["colsFirst"]) && ! in_array($col_arr[1], $selectedFields["colsSecond"]))
            array_push($fields,$col_arr);
     }
     else
     {
        if(! in_array($cols_name, $fields))
            array_push($fields,$col_arr);
     }
     $count++;
}

$xtpl->assign("WEB_POST_URL",$web_post_url);
//$xtpl->assign("LEAD_SELECT_FIELDS",'MOD.LBL_SELECT_LEAD_FIELDS');

require_once('include/QuickSearchDefaults.php');
$qsd = QuickSearchDefaults::getQuickSearchDefaults();
$sqs_objects = array('account_name' => $qsd->getQSParent(),
					'assigned_user_name' => $qsd->getQSUser(),
					'campaign_name' => $qsd->getQSCampaigns(),

					);
$quicksearch_js = '<script type="text/javascript" language="javascript">sqs_objects = ' . $json->encode($sqs_objects) . '</script>';
$xtpl->assign("JAVASCRIPT", $quicksearch_js);



/*if (empty($focus->assigned_user_id) && empty($focus->id))  
        $focus->assigned_user_id = $current_user->id;
if (empty($focus->assigned_name) && empty($focus->id))  $focus->assigned_user_name = $current_user->user_name;
$xtpl->assign("ASSIGNED_USER_OPTIONS", get_select_options_with_id(get_user_array(TRUE, "Active", $focus->assigned_user_id), $focus->assigned_user_id));
$xtpl->assign("ASSIGNED_USER_NAME", $focus->assigned_user_name);
$xtpl->assign("ASSIGNED_USER_ID", $focus->assigned_user_id );
*/
$xtpl->assign("REDIRECT_URL_DEFAULT",'http://');

//required fields on WebToModule form
$campaign= new Campaign();

$javascript = new javascript();
$javascript->setFormName('WebToModuleCreation');
$javascript->setSugarBean($lead);
$javascript->addAllFields('');
//$javascript->addFieldGeneric('redirect_url', '', 'LBL_REDIRECT_URL' ,'true');
$javascript->addFieldGeneric('campaign_name', '', 'LBL_RELATED_CAMPAIGN' ,'true');
$javascript->addFieldGeneric('assigned_user_name', '', 'LBL_ASSIGNED_TO' ,'true');
$javascript->addFieldGeneric('web_to_lead_record_name', '', $mod_strings['LBL_NAME_RECORD_NAME'] ,'true');
$javascript->addToValidateBinaryDependency('campaign_name', 'alpha', $app_strings['ERR_SQS_NO_MATCH_FIELD'] . $mod_strings['LBL_LEAD_NOTIFY_CAMPAIGN'], 'false', '', 'campaign_id');
$javascript->addToValidateBinaryDependency('assigned_user_name', 'alpha', $app_strings['ERR_SQS_NO_MATCH_FIELD'] . $app_strings['LBL_ASSIGNED_TO'], 'false', '', 'assigned_user_id');
echo $javascript->getScript();
$json = getJSONobj();
$lead_fields = $json->encode($fields);
$xtpl->assign("LEAD_FIELDS",$lead_fields);
$classname = "SUGAR_GRID";
$xtpl->assign("CLASSNAME",$classname);


$xtpl->assign("DRAG_DROP_CHOOSER_WEB_TO_LEAD",constructDDWebToModuleFields($fields,$classname,$firstColumnFields,$secondColumnFields));

$xtpl->parse("main");
$xtpl->out("main");
/*
$str = "<script>
WebToModuleForm.lead_fields = {$lead_fields};
</script>";
echo $str;
*/
/*
 *This function constructs Drag and Drop multiselect box of subscriptions for display in manage subscription form
*/
function constructDDWebToModuleFields($fields,$classname,$firstColumnFields,$secondColumnFields){
require_once("include/templates/TemplateDragDropChooser.php");
global $mod_strings;
$d2 = array();
    //now call function that creates javascript for invoking DDChooser object
    $dd_chooser = new TemplateDragDropChooser();
    $dd_chooser->args['classname']  = $classname;
    $dd_chooser->args['left_header']  = $mod_strings['LBL_AVALAIBLE_FIELDS_HEADER'];
    $dd_chooser->args['mid_header']   = $mod_strings['LBL_LEAD_FORM_FIRST_HEADER'];
    $dd_chooser->args['right_header'] = $mod_strings['LBL_LEAD_FORM_SECOND_HEADER'];
    $dd_chooser->args['left_data']    = $fields;
    $dd_chooser->args['mid_data']     = $firstColumnFields;//$d2;
    $dd_chooser->args['right_data']   = $secondColumnFields;//$d2;
    $dd_chooser->args['title']        =  ' ';
    $dd_chooser->args['left_div_name']      = 'ddgrid2';
    $dd_chooser->args['mid_div_name']       = 'ddgrid3';
    $dd_chooser->args['right_div_name']     = 'ddgrid4';
    $dd_chooser->args['gridcount'] = 'three';
    $str = $dd_chooser->displayScriptTags();
    $str .= $dd_chooser->displayDefinitionScript();
    $str .= $dd_chooser->display();
    $str .= "<script>
	           //function post rows
	           function postMoveRows(){
			    	//Call other function when this is called
	           }
	        </script>";
	$str .= "<script>
		       function displayAddRemoveDragButtons(Add_All_Fields,Remove_All_Fields){
				    var addRemove = document.getElementById('lead_add_remove_button');
				    if(" . $dd_chooser->args['classname'] . "_grid0.getDataModel().getTotalRowCount() ==0) {
				     addRemove.setAttribute('value',Remove_All_Fields);
				     addRemove.setAttribute('title',Remove_All_Fields);
				    }
				    else if(" . $dd_chooser->args['classname'] . "_grid1.getDataModel().getTotalRowCount() ==0 && " . $dd_chooser->args['classname'] . "_grid2.getDataModel().getTotalRowCount() ==0){
				     addRemove.setAttribute('value',Add_All_Fields);
				     addRemove.setAttribute('title',Add_All_Fields);
				    }
              }
            </script>";

    return $str;
}

/**
 * function to retrieve WebToModule image and title. path to help file
 * refactored to use SugarView::getModuleTitle()
 *
 * @deprecated use SugarView::getModuleTitle() instead
 *
 * @param  $module       string  not used, only for backward compatibility
 * @param  $image_name   string  image name
 * @param  $module_title string  to display as the module title
 * @param  $show_help    boolean which determines if the print and help links are shown.
 * @return string HTML
 */
function get_webtomodule_title(
    $module,
    $image_name, 
    $module_title, 
    $show_help
    )
{
    return $GLOBALS['current_view']->getModuleTitle($show_help);
}
