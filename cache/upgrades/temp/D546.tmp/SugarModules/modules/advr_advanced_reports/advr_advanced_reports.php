<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
/*
 * created by Vision Consulting, 2020
 * contact us:  info@vision-consulting.hu
 */


require_once 'modules/AOW_WorkFlow/aow_utils.php';
require_once 'modules/AOR_Reports/aor_utils.php';
require_once('modules/advr_advanced_reports/license/VisioRDOutfittersLicense.php');

class advr_advanced_reports extends Basic
{
    public $new_schema = true;
    public $module_dir = 'advr_advanced_reports';
    public $object_name = 'advr_advanced_reports';
    public $table_name = 'advr_advanced_reports';
    public $importable = false;

    public $id;
    public $name;
    public $date_entered;
    public $date_modified;
    public $modified_user_id;
    public $modified_by_name;
    public $created_by;
    public $created_by_name;
    public $description;
    public $deleted;
    public $created_by_link;
    public $modified_user_link;
    public $assigned_user_id;
    public $assigned_user_name;
    public $assigned_user_link;
    public $SecurityGroups;
    
    public $user_parameters;
    public $reportparameters;
    
    public $maxcols;
    public $graphcounter;
    
    public $validate_license;
    
    
    public function __construct()
    {
        global $current_user;
        parent::__construct();
        
        $this->validate_license = VisioRDOutfittersLicense::isValid('advr_advanced_reports');
        
        
        
    }

	
    public function bean_implements($interface)
    {
        switch($interface)
        {
            case 'ACL':
                return true;
        }

        return false;
    }
    
    public function ACLAccess($view, $is_owner = 'not_set', $in_group = 'not_set')
    {
        $result = parent::ACLAccess($view, $is_owner, $in_group);
        
        /* to be implemented: need to check if user has list access to all modules that the dashboard have
        if ($result && $this->report_module !== '') {
            $result = ACLController::checkAccess($this->report_module, 'list', true);
        }
        */
        
        return $result;
    }
    
    
    public function save($check_notify = false)
    {
        
        if (isset($_REQUEST['report_id_1']))  {
            
            for ($i=1;$i<=3;$i++) {
                
                //clean out report ids for blank report names (id is not deleted if report name is deleted by user)
                for ($k=0;$k<sizeof($_REQUEST['report_id_'.$i]);$k++) {
                    if ($_REQUEST['report_name_'.$i][$k]=="") {
                        $_REQUEST['report_id_'.$i][$k]='';
                    }
                }
                
                $report_def['ids_'.$i]=$_REQUEST['report_id_'.$i];
                $report_def['names_'.$i]=$_REQUEST['report_name_'.$i];
                $report_def['chartonly_'.$i]=$_REQUEST['report_chartonly_'.$i];
            }
            

            $report_def['line_deleted']=$_REQUEST['line_deleted'];
            $this->stored_options=base64_encode(serialize($report_def));
        }
        
        
    parent::save($check_notify);
   
        
        
    }
    
    public function getReportParameters()
    {
        if (!$this->id) {
            return array();
        }
        
        //get all report ids
        $report_def=unserialize(base64_decode($this->stored_options));
        $ids=array_merge($report_def['ids_1'],$report_def['ids_2'],$report_def['ids_3']);
        
        foreach ($ids as $id) {
            if (isset($id) and $id!='') {
                $idsquoted[]="'".$id."'";
            }
            
        }

        $idlist="(".implode(",",$idsquoted).")";
        
        $sql="SELECT id,field,aor_report_id FROM aor_conditions WHERE aor_report_id IN ".$idlist." AND deleted='0'";
        $result=$this->db->query($sql);
        
        
        $parameters = array();
        while ($row = $this->db->fetchByAssoc($result)) {

            if (!isset($processed_names[$row['field']])) {
           
                $condition = new AOR_Condition;
                $condition->retrieve($row['id']);
                
                $report= new AOR_Report;
                $report->retrieve($row['aor_report_id']);
              
            if (!$condition->parameter) {
                continue;
            }
            
            $processed_names[$row['field']]=1;
            
            $condition->module_path = implode(":", unserialize(base64_decode($condition->module_path)));
            if ($condition->value_type == 'Date') {
                $condition->value = unserialize(base64_decode($condition->value));
            }
            $condition_item = $condition->toArray();
            $display = getDisplayForField($condition->module_path, $condition->field, $report->report_module);
            $condition_item['module_path_display'] = $display['module'];
            $condition_item['field_label'] = $display['field'];
            $condition_item['report_module']= $report->report_module;
            
            if (!empty($this->user_parameters[$condition->id])) {
                $param = $this->user_parameters[$condition->id];
                $condition_item['operator'] = $param['operator'];
                $condition_item['value_type'] = $param['type'];
                $condition_item['value'] = $param['value'];
            }
            if (isset($parameters[$condition_item['condition_order']])) {
                $parameters[] = $condition_item;
            } else {
                $parameters[$condition_item['condition_order']] = $condition_item;
            }
            
            
            }
            
        }
        
    

        $this->reportparameters=$parameters;
  
    }
    
    
    //this function comes from 'modules/AOR_Reports/aor_utils.php';
    //modified so that it uses the condition itself to determine report_module
    function requestToUserParameters($reportBean = null)
    {
        global $app_list_strings;
        $params = array();
        if (!empty($_REQUEST['parameter_id'])) {
            $dateCount = 0;
            foreach ($_REQUEST['parameter_id'] as $key => $parameterId) {
                if ($_REQUEST['parameter_type'][$key] === 'Multi') {
                    $_REQUEST['parameter_value'][$key] = encodeMultienumValue(explode(
                        ',',
                        $_REQUEST['parameter_value'][$key]
                        ));
                }
                
                $condition = BeanFactory::getBean('AOR_Conditions', $_REQUEST['parameter_id'][$key]);
                $value = $_REQUEST['parameter_value'][$key];
                
                $report = new AOR_Report;
                $report->retrieve($condition->aor_report_id);
                
                
                if ($report->report_module && $condition && !array_key_exists($value,$app_list_strings['date_time_period_list'])){
                    $value = fixUpFormatting($report->report_module, $condition->field, $value);
                }
                
                $params[$parameterId] = array(
                    'id' => $parameterId,
                    'operator' => $_REQUEST['parameter_operator'][$key],
                    'type' => $_REQUEST['parameter_type'][$key],
                    'value' => $value,
                );
                
                // Fix for issue #1272 - AOR_Report module cannot update Date type parameter.
                if ($_REQUEST['parameter_type'][$key] === 'Date') {
                    $values = array();
                    $values[] = $_REQUEST['parameter_date_value'][$dateCount];
                    $values[] = $_REQUEST['parameter_date_sign'][$dateCount];
                    $values[] = $_REQUEST['parameter_date_number'][$dateCount];
                    $values[] = $_REQUEST['parameter_date_time'][$dateCount];
                    
                    $params[$parameterId] = array(
                        'id' => $parameterId,
                        'operator' => $_REQUEST['parameter_operator'][$key],
                        'type' => $_REQUEST['parameter_type'][$key],
                        'value' => $values,
                    );
                    $dateCount++;
                }
                
                // determine if parameter is a date
                if ($_REQUEST['parameter_type'][$key] === 'Value') {
                    $paramLength = strlen($_REQUEST['parameter_value'][$key]);
                    $paramValue = $_REQUEST['parameter_value'][$key];
                    if ($paramLength === 10) {
                        if (strpos($paramValue, '/') === 2 || strpos($paramValue, '/') === 4) {
                            $params[$parameterId] = array(
                                'id' => $parameterId,
                                'operator' => $_REQUEST['parameter_operator'][$key],
                                'type' => $_REQUEST['parameter_type'][$key],
                                'value' => convertToDateTime($_REQUEST['parameter_value'][$key])->format('Y-m-d H:i:s'),
                            );
                        } elseif (strpos($paramValue, '-') === 2 || strpos($paramValue, '-') === 4) {
                            $params[$parameterId] = array(
                                'id' => $parameterId,
                                'operator' => $_REQUEST['parameter_operator'][$key],
                                'type' => $_REQUEST['parameter_type'][$key],
                                'value' => convertToDateTime($_REQUEST['parameter_value'][$key])->format('Y-m-d H:i:s'),
                            );
                        } elseif (strpos($paramValue, '.') === 2 || strpos($paramValue, '.') === 4) {
                            $params[$parameterId] = array(
                                'id' => $parameterId,
                                'operator' => $_REQUEST['parameter_operator'][$key],
                                'type' => $_REQUEST['parameter_type'][$key],
                                'value' => convertToDateTime($_REQUEST['parameter_value'][$key])->format('Y-m-d H:i:s'),
                            );
                        }
                    }
                }
               
            }
        }
        
        $this->user_parameters= $params;
;
        
    }
    
    
    private function runreport($id,$chartonly,$forpdf=false) {
        $report = new AOR_Report;
        $report->retrieve($id);
        
        //building up userparameters of the slected report based on the unified condition list updated with user values
        $conditions =$report->get_linked_beans('aor_conditions', 'AOR_Conditions', 'condition_order');
        foreach ($conditions as $condition) {
            
            //finding same field name
            foreach ($this->reportparameters AS $parameter) {
                if ($parameter['field']==$condition->field) {
                    //updating the unified condition so that it matches the current report condition
                    $parameter['id']=$condition->id;
                    $parameter['aor_report_id']=$report->id;
                    $parameter['report_module']=$report->report_module;
                    $report->user_parameters[$condition->id]= $parameter;
                    

                }
            }
            
        }
        
        
        if (!$chartonly) {
            //adding header
            $htmlout='<h3><b>'.$report->name.'</b></h3>';
            $htmlout.=$report->buildMultiGroupReport(0, true);
                        } else {
                            if (!$forpdf) {
                            $htmlout.=$report->build_report_chart(null, 'rgraph');
                            } else {
                                $htmlout.= "<div class='reportGraphs' style='width:100%; text-align:center;'>";
                                $htmlout.= "<img src='".$_POST["graphsForPDF"][$this->graphcounter]."' style='width:100%;' />";
                                $htmlout.= "</div>";
                                
                                $this->graphcounter++;
                                
                                
                                
                            }
                        }
                        
       return $htmlout;                 
        
        
    }
    
    
    public function buildcontent($forpdf=false) {
        
        $html='';
        $this->graphcounter=0;
        
        $report_def=unserialize(base64_decode($this->stored_options));
        
        $rowmax=max(sizeof($report_def['ids_1']),sizeof($report_def['ids_2']),sizeof($report_def['ids_3']));
        
        for ($i=0;$i<$rowmax;$i++) {
            
            if ($report_def['line_deleted'][$i]=='0') {
                
                $html.='<div class="row">';
                
                $colno=12;
                if ($report_def['ids_2'][$i]!='') {
                    $colno=6;
                }
                
                if ($report_def['ids_3'][$i]!='') {
                   $colno=4;
                }
                

                $this->maxcols=max($this->maxcols,(12/$colno));
                
                if (!$forpdf) {
                        $header='<div class="col-md-'.$colno.'">';
                } else {
                       $header='<div class="col-xs-'.$colno.' col-sm-'.$colno.' col-md-'.$colno.' col-lg-'.$colno.'">';
                }
                
                if ($report_def['ids_1'][$i]!='') {
                    $html.=$header;
                    $html.=$this->runreport($report_def['ids_1'][$i],$report_def['chartonly_1'][$i],$forpdf);
                    $html.='</div>';
                }
                if ($report_def['ids_2'][$i]!='') {
                    $html.=$header;
                    $html.=$this->runreport($report_def['ids_2'][$i],$report_def['chartonly_2'][$i],$forpdf);
                    $html.='</div>';
                }
                if ($report_def['ids_3'][$i]!='') {
                    $html.=$header;
                    $html.=$this->runreport($report_def['ids_3'][$i],$report_def['chartonly_3'][$i],$forpdf);
                    $html.='</div>';
                }
                
                $html.='</div>';
                
                
            }
            
                                    }
        
        return $html;
        
    }
	
}