<?php
if (! defined('sugarEntry') || ! sugarEntry) die('Not A Valid Entry Point');
require_once("modules/Administration/QuickRepairAndRebuild.php");



   global $db;
   
   if (!$db->tableExists('advr_advanced_reports')) {
            //install table for advr
	        $sql_msb = "CREATE TABLE advr_advanced_reports (id char(36)  NOT NULL ,name varchar(255)  NULL ,date_entered datetime  NULL ,date_modified datetime  NULL ,modified_user_id char(36)  NULL ,created_by char(36)  NULL ,description text  NULL ,deleted bool  DEFAULT '0' NULL ,assigned_user_id char(36)  NULL ,stored_options text  NULL  , PRIMARY KEY (id)) CHARACTER SET utf8 COLLATE utf8_general_ci;";
	        $db->query($sql_msb);
	
	   		/*
            $autoexecute = true; //execute the SQL
            $show_output = true; //output to the screen

            $randc = new RepairAndClear();
            $randc->repairAndClearAll(array('clearAll'),array(translate('LBL_ALL_MODULES')), $autoexecute,$show_output);
			*/
                                       }
                                       
    //install table for user management

    if (!$db->tableExists('so_users')) {

        $fieldDefs = array(
            'id' => array (
              'name' => 'id',
              'vname' => 'LBL_ID',
              'type' => 'id',
              'required' => true,
              'reportable' => true,
            ),
            'deleted' => array (
                'name' => 'deleted',
                'vname' => 'LBL_DELETED',
                'type' => 'bool',
                'default' => '0',
                'reportable' => false,
                'comment' => 'Record deletion indicator',
            ),
            'shortname' => array (
                'name' => 'shortname',
                'vname' => 'LBL_SHORTNAME',
                'type' => 'varchar',
                'len' => 255,
            ),
            'user_id' => array (
                'name' => 'user_id',
                'rname' => 'user_name',
                'module' => 'Users',
                'id_name' => 'user_id',
                'vname' => 'LBL_USER_ID',
                'type' => 'relate',
                'isnull' => 'false',
                'dbType' => 'id',
                'reportable' => true,
                'massupdate' => false,
            ),
        );
        
        $indices = array(
            'id' => array (
                'name' => 'so_userspk',
                'type' => 'primary',
                'fields' => array (
                    0 => 'id',
                ),
            ),
            'shortname' => array (
                'name' => 'shortname',
                'type' => 'index',
                'fields' => array (
                    0 => 'shortname',
                ),
            ),
        );
        $db->createTableParams('so_users',$fieldDefs,$indices);
    }
    
//redirect to license validation page - CHANGE NAME BELOW - To your module name
//header('Location: index.php?module=SampleLicenseAddon&action=license');
    global $sugar_version;
    if(preg_match( "/^6.*/", $sugar_version)) {
        echo "
            <script>
            document.location = 'index.php?module=advr_advanced_reports&action=license';
            </script>"
        ;
    } else {
        echo "
            <script>
            var app = window.parent.SUGAR.App;
            window.parent.SUGAR.App.sync({callback: function(){
                app.router.navigate('#bwc/index.php?module=advr_advanced_reports&action=license', {trigger:true});
            }});
            </script>"
        ;
    }


