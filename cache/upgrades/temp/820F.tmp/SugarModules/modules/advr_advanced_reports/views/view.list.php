<?php

class advr_advanced_reportsViewList extends ViewList
{
    public function preDisplay()
    {
        parent::preDisplay();
        if($this->bean->validate_license !== true) {
            echo '<h2><p class="error">Reports Dashboard is not active due to the following reason: '.$this->bean->validate_license.'<br />Please <a href="index.php?module=advr_advanced_reports&action=license">configure license key</a> in the administration panel.</p></h2>';
            
        } 
    }
    
}