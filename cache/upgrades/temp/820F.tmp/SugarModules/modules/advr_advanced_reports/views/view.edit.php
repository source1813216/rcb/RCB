<?php

class advr_advanced_reportsViewEdit extends ViewEdit
{
    public function preDisplay()
    {
        if($this->bean->validate_license !== true) {
            SugarApplication::redirect('index.php?module=advr_advanced_reports');
        }
        parent::preDisplay();
    }
    
}