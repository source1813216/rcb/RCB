<?php
//require_once 'modules/AOR_Reports/aor_utils.php';
//error_reporting(E_ALL);
class advr_advanced_reportsViewDetail extends ViewDetail
{
    public function preDisplay()
    {
        if($this->bean->validate_license !== true) {
            SugarApplication::redirect('index.php?module=advr_advanced_reports');
        }
        
        
        parent::preDisplay();
        
        $canExport = $this->bean->ACLAccess('Export');
        $this->ss->assign('can_export', $canExport);
        
        $this->bean->requestToUserParameters();
        
        $this->bean->getReportParameters();
        
        echo "<script>var reportParameters = ".json_encode($this->bean->reportparameters).";</script>";
        
        if (!is_file('cache/jsLanguage/AOR_Conditions/' . $GLOBALS['current_language'] . '.js')) {
            require_once('include/language/jsLanguage.php');
            jsLanguage::createModuleStringsCache('AOR_Conditions', $GLOBALS['current_language']);
        }
        echo '<script src="cache/jsLanguage/AOR_Conditions/'. $GLOBALS['current_language'] . '.js"></script>';
        
        
        
        $html=$this->bean->buildcontent();
        
        
        $chartsPerRow=$this->bean->maxcols;
        $resizeGraphsPerRow = <<<EOD
        
       <script>
        function resizeGraphsPerRow()
        {
                var maxWidth = 900;
                var maxHeight = 500;
                var maxTextSize = 10;
                var divWidth = $("#detailpanel_report").width();
                
                var graphWidth = Math.floor(divWidth / $chartsPerRow);
                
                var graphs = document.getElementsByClassName('resizableCanvas');
                for(var i = 0; i < graphs.length; i++)
                {
                    if(graphWidth * 0.9 > maxWidth)
                    graphs[i].width  = maxWidth;
                else
                    graphs[i].width = graphWidth * 0.9;
                if(graphWidth * 0.9 > maxHeight)
                    graphs[i].height = maxHeight;
                else
                    graphs[i].height = graphWidth * 0.9;
                    
                    
                /*
                var text_size = Math.min(12, (graphWidth / 1000) * 12 );
                if(text_size < 6)text_size=6;
                if(text_size > maxTextSize) text_size = maxTextSize;
                
                if(     graphs[i] !== undefined
                    &&  graphs[i].__object__ !== undefined
                    &&  graphs[i].__object__["properties"] !== undefined
                    &&  graphs[i].__object__["properties"]["chart.text.size"] !== undefined
                    &&  graphs[i].__object__["properties"]["chart.key.text.size"] !== undefined)
                 {
                    graphs[i].__object__["properties"]["chart.text.size"] = text_size;
                    graphs[i].__object__["properties"]["chart.key.text.size"] = text_size;
                 }
                //http://www.rgraph.net/docs/issues.html
                //As per Google Chrome not initially drawing charts
                RGraph.redrawCanvas(graphs[i]);
                */
                }
                if (typeof RGraph !== 'undefined') {
                    RGraph.redraw();
                }
        }
        </script>
        
EOD;
        
        
        
        echo $resizeGraphsPerRow;
        echo "<script> $(document).ready(function(){resizeGraphsPerRow();}); </script>";
        echo "<script> $(window).resize(function(){resizeGraphsPerRow();}); </script>";
        
        
        
        $this->ss->assign('report_content', $html);
        
    }
}