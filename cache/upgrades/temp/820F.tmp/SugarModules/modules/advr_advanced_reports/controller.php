<?php 
/*
 * created by Vision Consulting, 2020
 * contact us:  info@vision-consulting.hu
 */


require_once("modules/AOW_WorkFlow/aow_utils.php");
require_once("modules/AOR_Reports/aor_utils.php");

class advr_advanced_reportsController extends SugarController
{
 
    protected function action_downloadPDF()
    {
        if (!$this->bean->ACLAccess('Export')) {
            SugarApplication::appendErrorMessage(translate('LBL_NO_ACCESS', 'ACL'));
            SugarApplication::redirect("index.php?module=advr_advanced_reports&action=DetailView&record=".$this->bean->id);
            sugar_die('');
        }
        
        error_reporting(0);
        require_once('modules/AOS_PDF_Templates/PDF_Lib/mpdf.php');
        
        $d_image = explode('?', SugarThemeRegistry::current()->getImageURL('company_logo.png'));
        
        $head = '<table style="width: 100%; font-family: Arial; text-align: center;" border="0" cellpadding="2" cellspacing="2">
                <tbody style="text-align: center;">
                <tr style="text-align: center;">
                <td style="text-align: center;">
                <b>' . strtoupper($this->bean->name) . '</b>
                </td>
                </tr>
                </tbody>
                </table><br />';
        
        $this->bean->requestToUserParameters();
        $this->bean->getReportParameters();
        
        $printable =$this->bean->buildcontent(true);
        

        
        $stylesheet = file_get_contents(SugarThemeRegistry::current()->getCSSURL('style.css', false));
        
        //to fix bootstrap cols in mpdf, https://stackoverflow.com/questions/27691475/mpdf-and-bootstrap-column-on-next-line
        $customCss = '
.row {
    margin-left: 0;
    margin-right: 0;
}
            
.row .col-xs-1, .row .col-xs-2, .row .col-xs-3, .row .col-xs-4, .row .col-xs-5, .row .col-xs-6, .row .col-xs-7, .row .col-xs-8, .row .col-xs-9, .row .col-xs-10, .row .col-xs-11, .row .col-xs-12 {
    padding-left: 0;
    padding-right: 0;
}';
        
        $stylesheet.=$customCss;
        
        ob_clean();
        
        try {
            $pdf = new mPDF('en', 'A4', '', 'DejaVuSansCondensed');
            $pdf->SetAutoFont();
            $pdf->WriteHTML($stylesheet, 1);
            $pdf->SetDefaultBodyCSS('background-color', '#FFFFFF');
            unset($pdf->cssmgr->CSS['INPUT']['FONT-SIZE']);
            $pdf->WriteHTML($head, 2);
            $pdf->WriteHTML($printable);
            $pdf->setFooter('{PAGENO}');
            $pdf->Output($this->bean->name . '.pdf', "D");
        } catch (mPDF_exception $e) {
            echo $e;
        }
        
    }
    
}


