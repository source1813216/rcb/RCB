<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Felelős felhasználó ID',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős',
  'LBL_SECURITYGROUPS' => 'Biztonsági Csoportok',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Biztonsági Csoportok',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Létrehozva',
  'LBL_DATE_MODIFIED' => 'Módosítva',
  'LBL_MODIFIED' => 'Módosító',
  'LBL_MODIFIED_ID' => 'Módosító Id',
  'LBL_MODIFIED_NAME' => 'Módosító név',
  'LBL_CREATED' => 'Készítette:',
  'LBL_CREATED_ID' => 'Készítette: Id',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_DELETED' => 'Törölve',
  'LBL_NAME' => 'Név',
  'LBL_CREATED_USER' => 'Létrehozó',
  'LBL_MODIFIED_USER' => 'Módosító',
  'LBL_LIST_NAME' => 'Név',
  'LBL_EDIT_BUTTON' => 'Módosítás',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_LIST_FORM_TITLE' => 'Komplex jelentések listája',
  'LBL_MODULE_NAME' => 'Komplex jelentések',
  'LBL_MODULE_TITLE' => 'Komplex jelentések',
  'LBL_HOMEPAGE_TITLE' => 'Saját komplex jelentéseim',
  'LNK_NEW_RECORD' => 'Új komplex jelentés',
  'LNK_LIST' => 'Komplex jelentések listája',
  'LNK_IMPORT_ADVR_ADVANCED_REPORTS' => 'Komplex jelentések importálása',
  'LBL_SEARCH_FORM_TITLE' => 'Komplex jelentés keresése',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_ADVR_ADVANCED_REPORTS_SUBPANEL_TITLE' => 'Komplex jelentések',
  'LBL_NEW_FORM_TITLE' => 'Új komplex jelentés',
  'LBL_REPORT' => 'Jelentés',
  'LBL_REPORT_ITEMS' => 'Elemek',
  'LBL_ADDLINE' => 'Új sor hozzáadása',
  'LBL_DOWNLOAD_PDF' => 'PDF letöltés',
  'LBL_REMOVE_PRODUCT_LINE' => 'Sor törlése',
  'LBL_NO_ACCESS' => 'Önnek nincs hozzáférése ehhez a funkcióhoz',
  'LBL_CHARTONLY' => 'csak grafikont mutat',
  'LBL_COLUMN' => 'oszlop',
    
);