<?php
/*
 * created by Vision Consulting, 2020
 * contact us:  info@vision-consulting.hu
 */

function display_items($focus, $field, $value, $view)
{
    global $mod_strings;
    
    /*
    $sql="SELECT name,id FROM aor_reports WHERE deleted=0";
    $result = $focus->db->query($sql);
    while ($row = $focus->db->fetchByAssoc($result)) {
        $option_reports.='<option label="'.$row['name'].'" value="'.$row['id'].'">'.$row['name'].'</option>';
    }
    */
    

    $html='<div class="col-xs-12 col-sm-12 edit-view-row-item">
<table id="report_items" style="width:100%">
<thead>
<tr>
<td style="width:33%">1. '.$mod_strings['LBL_COLUMN'].'</td>
<td style="width:33%">2. '.$mod_strings['LBL_COLUMN'].'</td>
<td style="width:33%">3. '.$mod_strings['LBL_COLUMN'].'</td>
<td> </td>

</tr>
</thead>
<tbody id="reportstructure">
</tbody>
<tfoot>
<tr>
<td><input type=\'button\' tabindex=\'116\' class=\'button add_product_line\' value=\''.$mod_strings['LBL_ADDLINE'].'\' id=\'addline\' onclick=\'insertReportLine()\' /></td>
</tr>
</tfoot>
</table>
</div>
';
    
    if ($view == 'EditView') {
      //preload definitions if thrre is already

        
        if ($focus->stored_options!='') {
            $report_def=unserialize(base64_decode($focus->stored_options));
            $rowmax=max(sizeof($report_def['ids_1']),sizeof($report_def['ids_2']),sizeof($report_def['ids_3']));
            
            
            for ($i=0;$i<$rowmax;$i++) {
                
                if ($report_def['line_deleted'][$i]=='0') {
                    
                    $data['report_name_1']=$report_def['names_1'][$i];
                    $data['report_name_2']=$report_def['names_2'][$i];
                    $data['report_name_3']=$report_def['names_3'][$i];
                    
                    $data['report_id_1']=$report_def['ids_1'][$i];
                    $data['report_id_2']=$report_def['ids_2'][$i];
                    $data['report_id_3']=$report_def['ids_3'][$i];
                    
                    if (isset($report_def['chartonly_1'][$i]) and $report_def['chartonly_1'][$i]=='1') {
                        $data['report_chartonly_1']=1;
                    } else {
                        $data['report_chartonly_1']=0;
                    }
                    
                    if (isset($report_def['chartonly_2'][$i]) and $report_def['chartonly_2'][$i]=='1') {
                        $data['report_chartonly_2']=1;
                    } else {
                        $data['report_chartonly_2']=0;
                    }
                    
                    
                    if (isset($report_def['chartonly_3'][$i]) and $report_def['chartonly_3'][$i]=='1') {
                        $data['report_chartonly_3']=1;
                    } else {
                        $data['report_chartonly_3']=0;
                    }
                    
                    $data_coded=json_encode($data);
                    
                    $html.='<script>populateline('.$data_coded.');</script>';
                    
                    
                }
                
            }
            
        }
        
        
    }
    
    return $html;
    
}