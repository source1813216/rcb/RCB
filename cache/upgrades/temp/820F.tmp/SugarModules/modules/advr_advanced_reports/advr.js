var prodln = 0;
function openReportPopup(ln){

	  lineno=ln;
	  var popupRequestData = {
	    "call_back_function" : "setReportReturn",
	    "form_name" : "EditView",
	    "field_to_name_array" : {
	      "id" : "report_id_" + ln,
	      "name" : "report_name_" + ln,
	    }
	  };

	  open_popup('AOR_Reports', 800, 850, '', true, true, popupRequestData);

	}

function setReportReturn(popupReplyData){
	  set_return(popupReplyData);
	}

function insertReportLine() {
	
	var tablebody=document.getElementById('reportstructure');
	var newrow = tablebody.insertRow(-1);
	newrow.id='structurerow'+prodln;
	
	var a = newrow.insertCell(0);
	a.innerHTML="<input class='sqsEnabled report_name_1' autocomplete='off' type='text' name='report_name_1[" + prodln + "]' id='report_name_1" + prodln + "' maxlength='100' value='' title='' tabindex='116' value='' style='width:70%'><input type='hidden' name='report_id_1[" + prodln + "]' id='report_id_1" + prodln + "'  maxlength='50' value=''><span class='id-ff multiple'><button style='width:32px' title='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_TITLE') + "' accessKey='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_KEY') + "' type='button' tabindex='116' class='button' value='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_LABEL') + "' name='btn1' onclick='openReportPopup(1" + prodln + ");'><span class=\"suitepicon suitepicon-action-select\"></span></button></span><br /><input type='checkbox' id='report_chartonly_1" + prodln + "' name='report_chartonly_1[" + prodln + "]' value='1'><label for='report_chartonly_1" + prodln + "'>"+SUGAR.language.get('advr_advanced_reports', 'LBL_CHARTONLY')+"</label>";
	
	var b = newrow.insertCell(1);
	b.innerHTML="<input class='sqsEnabled report_name_2' autocomplete='off' type='text' name='report_name_2[" + prodln + "]' id='report_name_2" + prodln + "' maxlength='100' value='' title='' tabindex='116' value='' style='width:70%'><input type='hidden' name='report_id_2[" + prodln + "]' id='report_id_2" + prodln + "'  maxlength='50' value=''><span class='id-ff multiple'><button style='width:32px' title='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_TITLE') + "' accessKey='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_KEY') + "' type='button' tabindex='116' class='button' value='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_LABEL') + "' name='btn2' onclick='openReportPopup(2" + prodln + ");'><span class=\"suitepicon suitepicon-action-select\"></span></button></span><br /><input type='checkbox' id='report_chartonly_2" + prodln + "' name='report_chartonly_2[" + prodln + "]' value='1'><label for='report_chartonly_2" + prodln + "'>"+SUGAR.language.get('advr_advanced_reports', 'LBL_CHARTONLY')+"</label>";
	
	var c = newrow.insertCell(2);
	c.innerHTML="<input class='sqsEnabled report_name_3' autocomplete='off' type='text' name='report_name_3[" + prodln + "]' id='report_name_3" + prodln + "' maxlength='100' value='' title='' tabindex='116' value='' style='width:70%'><input type='hidden' name='report_id_3[" + prodln + "]' id='report_id_3" + prodln + "'  maxlength='50' value=''><span class='id-ff multiple'><button style='width:32px' title='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_TITLE') + "' accessKey='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_KEY') + "' type='button' tabindex='116' class='button' value='" + SUGAR.language.get('app_strings', 'LBL_SELECT_BUTTON_LABEL') + "' name='btn3' onclick='openReportPopup(3" + prodln + ");'><span class=\"suitepicon suitepicon-action-select\"></span></button></span><br /><input type='checkbox' id='report_chartonly_3" + prodln + "' name='report_chartonly_3[" + prodln + "]' value='1'><label for='report_chartonly_3" + prodln + "'>"+SUGAR.language.get('advr_advanced_reports', 'LBL_CHARTONLY')+"</label>";
	
	var d = newrow.insertCell(3);
	d.innerHTML="<input type='hidden' name='line_deleted[" + prodln + "]' id='line_deleted" + prodln + "' value='0'><span class='id-ff multiple'><button style='width:32px' title='" + SUGAR.language.get('advr_advanced_reports', 'LBL_REMOVE_PRODUCT_LINE') + "' type='button' tabindex='116' class='button' value='" + SUGAR.language.get('advr_advanced_reports', 'LBL_REMOVE_PRODUCT_LINE') + "' name='remove_btn1' onclick='markLineDeleted(" + prodln + ");'><span class=\"suitepicon suitepicon-action-clear\"></span></button></span><br />&nbsp;";
	
	prodln++;
	
	return prodln - 1;
	
}

function markLineDeleted(ln)
{
  document.getElementById('structurerow' + ln).style.display = 'none';
  document.getElementById('line_deleted' + ln).value = '1';

}

function populateline(data) {
	  var ln = 0;
	  
	  ln=insertReportLine();
	  
	  for(var key in data){
		    if (key=='report_chartonly_1' || key=='report_chartonly_2' || key=='report_chartonly_3') {
		    	document.getElementById(key + ln).checked = data[key];
		    } else {
		    	document.getElementById(key + ln).value = data[key];
		    }
		    
		  
	  }
	  
	  
}