<script src="modules/AOR_Conditions/conditionLines.js"></script>
<script>
  var report_module = '';
</script>
<div id='detailpanel_parameters' class='detail view  detail508 expanded hidden'>
    <form onsubmit="return false" id="EditView" name="EditView">
        <h4>
            <a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel('parameters');">
                <img border="0" id="detailpanel_parameters_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
            <a href="javascript:void(0)" class="expandLink" onclick="expandPanel('parameters');">
                <img border="0" id="detailpanel_parameters_img_show"
                     src="{sugar_getimagepath file="advanced_search.gif"}"></a>
            {sugar_translate label='LBL_PARAMETERS' module='AOR_Reports'}
            <script>
              document.getElementById('detailpanel_parameters').className += ' expanded';
            </script>
        </h4>
        <div id="aor_conditionLines" class="panelContainer" style="min-height: 50px;">
        </div>
        <input id='updateParametersButton' class="panelContainer" type="button"
               value="{sugar_translate label='LBL_UPDATE_PARAMETERS' module='AOR_Reports'}"/>
        <script>
            {literal}
            $.each(reportParameters, function (key, val) {
              report_module=val['report_module'];
              loadConditionLine(val, 'EditView');
            });

            $(document).ready(function () {
              $('#updateParametersButton').click(function () {
                //Update the Detail view form to have the parameter info and reload the page
                var _form = $('#formDetailView');
                _form.find('input[name=action]').val('DetailView');
                //Add each parameter to the form in turn
                $('.aor_conditions_id').each(function (index, elem) {
                  $elem = $(elem);
                  var ln = $elem.attr('id').substr(17);
                  var id = $elem.val();
                  appendHiddenFields(_form, ln, id);
                  updateTimeDateFields(fieldInput, ln);
                  // Fix for issue #1272 - AOR_Report module cannot update Date type parameter.
                  updateHiddenReportFields(ln, _form);
                });
                _form.submit();
              });
            });
            {/literal}
        </script>
        <script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function () {ldelim} initPanel('parameters', 'expanded'); {rdelim}); </script>
    </form>
</div>

<div class="panel-content">
    <div>&nbsp;</div>
    <div class="panel panel-default">
        <div class="panel-heading ">
            <a class="" role="button" data-toggle="collapse" href="#detailpanel_report" aria-expanded="false">
                <div class="col-xs-10 col-sm-11 col-md-11">
                             {sugar_translate label='LBL_REPORT' module='advr_advanced_reports'}
                </div>
            </a>
        </div>
        <div class="panel-body panel-collapse collapse in" id="detailpanel_report">
            <div class="tab-content">
                {$report_content}
            </div>
        </div>
    </div>