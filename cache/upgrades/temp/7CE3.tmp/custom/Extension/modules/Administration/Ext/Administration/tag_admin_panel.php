<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Adding admin panel for Tag Setting
 * 
 */

// Create panel In Administration
$admin_options_defs=array();
$admin_options_defs['Administration']['Tagging_Manager']=array(       
        'ModuleLoader',
        'LBL_SET_MODULES_FOR_TAGS',
        'LBL_TAGS_ADMIN_PANEL_DESCRIPTION',
        'index.php?module=Administration&action=tags_config'
        );

$admin_group_header[]=array(
		'LBL_TAG_SETTING_TITLE',
		'',
		false,
		$admin_options_defs,
);
?>
