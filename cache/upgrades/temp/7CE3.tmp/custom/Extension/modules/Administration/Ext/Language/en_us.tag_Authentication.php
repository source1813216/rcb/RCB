<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Language file
 * 
 */
  $mod_strings['LBL_TAG_SETTING_TITLE'] = 'Tagging System Setting';
  $mod_strings['LBL_SET_MODULES_FOR_TAGS'] = 'Set Modules for Tags';
  $mod_strings['LBL_CONFIGURE_TAG_MODULE'] = 'Configure Tag Modules';
  $mod_strings['LBL_CANCEL_BUTTON_TITLE'] = 'Cancel';
  $mod_strings['TAGS_ENABLED_MODULES'] = 'Tags Enable Modules';
  $mod_strings['TAGS_DISABLED_MODULES'] = 'Tags Disable Modules';
  $mod_strings['LBL_TAGS_ADMIN_PANEL_DESCRIPTION'] = 'Manage modules for Tagging';
  $mod_strings['LBL_CONFIGURE_MODULES_TO_ENABLE_TAGGING'] = 'Configure Modules to Enable Tagging';
  $mod_strings['LBL_ENABLE_MODULES'] = 'Enabled Modules';
  $mod_strings['LBL_DISABLE_MODULES'] = 'Disable Modules';
  $mod_strings['LBL_TAGS_CONFIGURATION_TITLE'] = 'Tags Configuration';
