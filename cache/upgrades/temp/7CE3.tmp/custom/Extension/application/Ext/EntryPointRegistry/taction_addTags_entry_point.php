<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: This entrypoint to add tags from enabled modules
 * 
 */
  $entry_point_registry['tac_Tags'] = array(
    'file' => 'custom/modules/tac_Tags/tac_add_tags.php',
    'auth' => true
  );
