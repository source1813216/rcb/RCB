<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: This entrypoint to show popup action form from Dashlet
 *
 */
  $entry_point_registry['dashlet_model'] = array(
    'file' => 'custom/modules/tac_Tags/Dashlets/tac_TagsDashlet/show_model.php',
    'auth' => true
  );

