<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Global search action view of results
 * 
 */

// Validate License

global $current_user;
require_once('modules/tac_Tags/license/OutfittersLicense.php');
$validate_license = OutfittersLicense::isValid('tac_Tags',$current_user->id);
if($validate_license !== true) {
    echo '<h2><p class="error">Tags Cloud is no longer active</p></h2><p class="error">'.$validate_license.'</p>';

    //functionality may be altered here in response to the key failing to validate
}
else
{
	$string=addslashes($_REQUEST['tag_search_qry']);
	echo '<h2>'.$string.'</h2>';
	if($string!=''){
		$qry="select * from tac_tags where name like '%$string%' and deleted=0";
		$result=$GLOBALS["db"]->query($qry);
		echo '<div id="tag_bean" class="tag_bean-container" style="height:660px; overflow-y:auto;">';
		if($GLOBALS['db']->getRowCount($result)>0){
			while($row=$GLOBALS['db']->fetchByAssoc($result)){
			$name=$row['name'];
			$tag_id=$row['id'];
			$date_entered=$row['date_entered'];
			$date_modified=$row['date_modified'];
			$description=$row['description'];
			
			echo $display='
		<table cellpadding="0" cellspacing="0" width="100%" border="0" class="list View">
		<tbody><tr><td width="1%" nowrap="nowrap" ></td></tr></tbody>
		  <thead>
			<tr height="20">
				<th scope="col" width="10%" data-hide="phone">
						<span sugar="sugar1">
							<div style="white-space: nowrap;" width="100%" align="left">
								Module                    </div>
						</span>
				</th>
				<th scope="col" width="30%" data-toggle="true">
						<span sugar="sugar1">
							<div style="white-space: nowrap;" width="100%" align="left">
								Name                    </div>
						</span>
				</th>
				<th scope="col" width="30%" data-hide="phone">
						<span sugar="sugar1">
							<div style="white-space: nowrap;" width="100%" align="left">
								Description                    </div>
						</span>
				</th>
				<th scope="col" width="25%" data-hide="phone,phonelandscape">
					<div style="white-space: nowrap;" width="100%" align="left">
						Date Created            </div>
				</th>
				<th scope="col" width="25%" data-hide="phone,phonelandscape">
					<div style="white-space: nowrap;" width="100%" align="left">
						Date Modified            </div>
				</th>
			</tr>
			</thead>
			<tbody><tr><td>Tags</td><td><a href="index.php?module=tac_Tags&amp;action=DetailView&amp;record='.$tag_id.'">'.$name.'</a></td><td>'.$description.'</td><td>'.$date_entered.'</td><td>'.$date_modified.'</td></tr></tbody>';
			
			
			
				// Fetch related beans
				$related_qry="SELECT * FROM `tac_tag_bean_rel` where tag_id='$tag_id' and deleted=0";
				$result_related_qry =$GLOBALS['db']->query($related_qry);
				if($GLOBALS['db']->getRowCount($result_related_qry)>0){
					echo '<b>Related records</b>';
					while($row_bean=$GLOBALS["db"]->fetchByAssoc($result_related_qry)){
						$record_id = $row_bean['bean_id'];
						$module = $row_bean['bean_module'];
						$bean = BeanFactory::getBean($module, $record_id);
						echo $display_related_beans='<table cellpadding="0" cellspacing="0" width="100%" border="0" class="list View">
						<tbody><tr><td width="1%" nowrap="nowrap" ></td></tr></tbody>
		  <thead>
			<tr height="20">
				<th scope="col" width="10%" data-hide="phone">
						<span sugar="sugar1">
							<div style="white-space: nowrap;" width="100%" align="left">
								Module                    </div>
						</span>
				</th>
				<th scope="col" width="30%" data-toggle="true">
						<span sugar="sugar1">
							<div style="white-space: nowrap;" width="100%" align="left">
								Name                    </div>
						</span>
				</th>
				<th scope="col" width="30%" data-hide="phone">
						<span sugar="sugar1">
							<div style="white-space: nowrap;" width="100%" align="left">
								Description                    </div>
						</span>
				</th>
				<th scope="col" width="25%" data-hide="phone,phonelandscape">
					<div style="white-space: nowrap;" width="100%" align="left">
						Date Created            </div>
				</th>
				<th scope="col" width="25%" data-hide="phone,phonelandscape">
					<div style="white-space: nowrap;" width="100%" align="left">
						Date Modified            </div>
				</th>
			</tr>
			</thead>
			<tbody><tr><td>'.$module.'</td><td><a href="index.php?module='.$module.'&amp;action=DetailView&amp;record='.$record_id.'">'.$bean->name.'</a></td><td>'.$bean->description.'</td><td>'.$bean->date_entered.'</td><td>'.$bean->date_modified.'</td></tr></tbody></table></table>';
					}
				}
				echo"<br></br><br></br>";
			}                    
		}
		else{
			echo "<h5 class='text-center' style='color:red'>No result found about ".$string."</h5>";
		}
		echo '</div>';
	}
	else{
		echo "<h5 class='text-center' style='color:red'>No records found</h5>";
	}
}
