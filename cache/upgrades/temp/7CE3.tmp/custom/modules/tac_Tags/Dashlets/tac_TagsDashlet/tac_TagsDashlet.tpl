<!-- 
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: Dashlet tpl file
 
 */
-->
<style>
{literal}

#tag_div {
	height:auto;
    width: auto;
	text-align:center;
}
 
#tag_div .smallest {
    font-size: 12 px;
}
 
#tag_div .small {
    font-size: 18px;
}
 
#tag_div .medium {
    font-size:26px;
}
 
#tag_div .large {
    font-size:35px;
}
 
#tag_div .largest {
    font-size:50px;
}
{/literal}

</style>
<script type="text/javascript">
{literal}

$(document).ready(function(){
    $("#tag_div button").each(function(el){
		var makeColor ="#" + Math.floor((Math.random() * 999) + 1);
        $(this).css("color",makeColor);
    });
    
	$('.tag_names').on('click', function(){
		var pass = $(this).val();
		var tag_name = $(this).text();
		$.ajax({
			type: "POST",
			data: {'tag_id' : pass, 'tag_name':tag_name},
			url: 'index.php?entryPoint=dashlet_model',
			success: function(data){
				if(data) {
					$('body').append(data);
					$('#myModal_'+pass).modal('show');
				} 
				else {
					alert('Error');
				}
			}
		});
	}); 

});  

{/literal}

</script>
<body>
<div style="width:100%;vertical-align:middle;">
<form style="background-color:#e7f1ff;" id="configure_tag" name='configure_tag' action="" method="post">
<table width="100%" border="0" align="center" class="list view" cellspacing="0" cellpadding="0">
<div id="tag_div" style="margin-left:1px;float:left;">
	{assign var=count value=1}
	{foreach from=$title key=k item=v}
	<button style="display: inline;width: auto;" type="button" class="btn-link {$class.$k} tag_names" id="view_tag_{$count}" name="view_tag" value="{$k}">{$v}</button>
	<input type="hidden" id="tag_count_{$count}" name="tag_count_{$count}" value="{$row_count.$k}">
	{assign var=count value=$count+1}
	{/foreach}
</div>

</table>
</form>
</div>
</body>
