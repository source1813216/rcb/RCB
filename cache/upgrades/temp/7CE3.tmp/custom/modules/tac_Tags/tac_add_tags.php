<?php
/*
 * Created by Taction Software LLC - Copyright 2017
 * Website: www.tactionsoftware.com/
 * Mail: info@tactionsoftware.com
 * @Author:Akanksha Srivastava
 * Description: 'Add Tags' pop-up form action
 */

global $current_user;
$source_id=$_REQUEST['source_id'];
$source_type=addslashes($_REQUEST['source_type']);
//Validate License
require_once('modules/tac_Tags/license/OutfittersLicense.php');
$validate_license = OutfittersLicense::isValid('tac_Tags',$current_user->id);
if($validate_license !== true) 
{
    echo '<h2><p class="error">Tags Cloud is no longer active</p></h2><p class="error">'.$validate_license.'</p>';
    echo '<input type="button" onclick="window.location.href=\'index.php?action=DetailView&module='.$source_type.'&record='.$source_id.'\'" value="Go Back">';
}

else
{
	
	$description=addslashes($_REQUEST['description']);
	$date=date('Y-m-d H:i:s');

	// Adding Relationship with existing Tags
	if(isset($_REQUEST['select_tag']) && $_REQUEST['select_tag']!=''){
		$tags=$_REQUEST['select_tag'];
		$count=count($tags);
		for($i=0;$i<$count;$i++){
			if($tags[$i]!=''){	
				$rel_id=create_guid();
				$insert_rel="INSERT INTO tac_tag_bean_rel(id, tag_id, bean_id, bean_module, date_modified, deleted) VALUES ('$rel_id','".$tags[$i]."','$source_id','$source_type','$date',0)";
				$GLOBALS['db']->query($insert_rel);
			}
		}
	}

	// Creating and Adding New Tag relation
	if(isset($_REQUEST['create_tag']) && $_REQUEST['create_tag']!=''){
		$name=addslashes($_REQUEST['create_tag']);
		$bean = BeanFactory::newBean('tac_Tags');
		$bean->name = $name;
		$bean->name_lower= strtolower($name);
		$bean->source_id=$source_id;
		$bean->source_type=$source_type;
		$bean->description=$description;
		$bean->save();
		
		$rel_id=create_guid();
		$insert_rel="INSERT INTO tac_tag_bean_rel(id, tag_id, bean_id, bean_module, date_modified, deleted) VALUES ('$rel_id','".$bean->id."','$source_id','$source_type','$date',0)";
		$GLOBALS['db']->query($insert_rel);
	}

	//Deleting Tags
	if(isset($_POST['delete_id']) && $_POST['delete_id'] != ""){
		$del_tag_id=$_POST['delete_id'];
		$delete_tag_rel="DELETE FROM tac_tag_bean_rel WHERE id='$del_tag_id' and deleted=0";
		$GLOBALS['db']->query($delete_tag_rel);
	}
	SugarApplication::redirect('index.php?action=DetailView&module='.$source_type.'&record='.$source_id);
}

