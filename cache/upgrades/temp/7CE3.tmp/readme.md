README
CONTACT: info@tactionsoftware.com

If you feel as if the SSL generation is too difficult, please get in contact with us and we will assist you

If you don't have file upload/FTP access to the server where your SuiteCRM is installed, please contact us and we will assist you

If you have any problems with the installation, and/or thoughts on improving the functionality of the Xero Configuration module and it's associated add-ons, please don't hesitate to contact us
