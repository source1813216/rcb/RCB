<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
require_once("modules/Import/Forms.php");
if(isset($_REQUEST['fetchField']) && !empty($_REQUEST['ele_module'])) {
    global $current_language,$timedate,$mod_strings;
    if(!empty($_REQUEST['record']))
        $oWebModule = BeanFactory::getBean("UT_WebToModule",$_REQUEST['record']);
    else
        $oWebModule = BeanFactory::getBean("UT_WebToModule");
        
            $aFieldList = $oWebModule->get_module_field_list($_REQUEST['ele_module']);
            $defaultControls = $field_selected_column1_li = $field_selected_column2_li = $available_field_for_required_li = $selected_required_field_li = "";
            $available_field_for_duplicate_li = $selected_duplicate_field_li = $base_field_layout = "";
            $required_column_list = array();
            $duplicate_column_list = array();
            
            if(!empty($oWebModule->id)) {
                $selectedFields = (array)json_decode(base64_decode($oWebModule->selected_fields));
				$requiredFields = (array)json_decode(base64_decode($oWebModule->required_fields));
                $duplicateFields = (array)json_decode(base64_decode($oWebModule->duplicate_fields));
				$defalutFields = (array)json_decode(base64_decode($oWebModule->default_value_fields));
				$fieldsMarkAsHidden = (array)json_decode(base64_decode($oWebModule->fields_mark_as_hidden));
                if(!empty($selectedFields['colsFirst'])) {
                    foreach ($selectedFields['colsFirst'] as $key => $values) {
                        if(!empty($values))
						{
                            $field_selected_column1_li .= " <li class='ui-state-default sortable'  id='{$values}'>" .$aFieldList['field_list'][$values]. "</li> ";
							if(in_array($values, $requiredFields['requiredFields']))
							{
								$selected_required_field_li .= "<li class='ui-state-default sortable' id='{$values}'>".$aFieldList['field_list'][$values]."</li>";
								$required_column_list[$values] = $aFieldList['field_list'][$values];
							}
							else
								$available_field_for_required_li .= "<li class='ui-state-default sortable' id='{$values}'>".$aFieldList['field_list'][$values]."</li>";;
                                
                            
                            if(in_array($values, $duplicateFields['duplicateFields']))
                            {
                                $selected_duplicate_field_li .= "<li class='ui-state-default sortable' id='{$values}'>".$aFieldList['field_list'][$values]."</li>";
                                $duplicate_column_list[$values] = $aFieldList['field_list'][$values];
                            }
                            else
                                $available_field_for_duplicate_li .= "<li class='ui-state-default sortable' id='{$values}'>".$aFieldList['field_list'][$values]."</li>";;
                                
						}
                    }
                }
                if(!empty($selectedFields['colsSecond'])) {
                    foreach ($selectedFields['colsSecond'] as $key => $values) {
                        if(!empty($values))
						{
                            $field_selected_column2_li .= " <li class='ui-state-default sortable'  id='{$values}'>" . $aFieldList['field_list'][$values] . "</li> ";
							if(in_array($values, $requiredFields['requiredFields']))
							{
								$selected_required_field_li .= "<li class='ui-state-default sortable' id='{$values}'>".$aFieldList['field_list'][$values]."</li>";
								$required_column_list[$values] = $aFieldList['field_list'][$values];
							}
							else
								$available_field_for_required_li .= "<li class='ui-state-default sortable' id='{$values}'>".$aFieldList['field_list'][$values]."</li>";
                                
                            if(in_array($values, $duplicateFields['duplicateFields']))
                            {
                                $selected_duplicate_field_li .= "<li class='ui-state-default sortable' id='{$values}'>".$aFieldList['field_list'][$values]."</li>";
                                $duplicate_column_list[$values] = $aFieldList['field_list'][$values];
                            }
                            else
                                $available_field_for_duplicate_li .= "<li class='ui-state-default sortable' id='{$values}'>".$aFieldList['field_list'][$values]."</li>";
						}
                    }
                }
                foreach ($aFieldList['field_list'] as $field_name => $label) {
                        if(isset($selectedFields['colsFirst']) && in_array($field_name,$selectedFields['colsFirst']))
                            continue;
                        elseif(isset($selectedFields['colsSecond']) && in_array($field_name,$selectedFields['colsSecond']))
                            continue;
                        else
                            $base_field_layout .= " <li class='ui-state-default sortable'  id='{$field_name}'>{$label}</li> ";
                }
				$allSelectedFields = array_merge($selectedFields["colsFirst"],$selectedFields["colsSecond"]);
				$selectedModuleName = $_REQUEST['ele_module'];
				$oSelectedModule = BeanFactory::getBean($selectedModuleName);
				$defaultControls .= "<tr>";
					$defaultControls .= "<td width='33%' align='left'><b>".$mod_strings['LBL_FIELD_LABEL']."</b></td>";
					$defaultControls .= "<td width='33%'><b>".$mod_strings['LBL_FIELD_CONTROL']."</b></td>";
					$defaultControls .= "<td width='33%'><b>".$mod_strings['LBL_MARK_AS_HIDDEN']."</b></td>";
				$defaultControls .= "</tr>";
				foreach($allSelectedFields as $fieldName)
				{
					if((!array_key_exists('source', $oSelectedModule->field_defs[$fieldName]) || (array_key_exists('source', $oSelectedModule->field_defs[$fieldName]) && $oSelectedModule->field_defs[$fieldName]['source'] != 'non-db')) && $fieldName != "email1")
					{
						if(array_key_exists($fieldName, $defalutFields) && !empty($defalutFields[$fieldName]))
						{
							if($oSelectedModule->field_defs[$fieldName]['type'] == "multienum")
								$defaultValue = unencodeMultienum($defalutFields[$fieldName]);
							else if($oSelectedModule->field_defs[$fieldName]['type'] == "date")
								$defaultValue = $timedate->to_display_date($defalutFields[$fieldName],false);
							else
								$defaultValue = $defalutFields[$fieldName];
						}
						else
							$defaultValue = "";
						
						$defaultControls .= "<tr>";
							$defaultControls .= '<td width="33%"><span sugar="slot40">'.translate($oSelectedModule->field_defs[$fieldName]['vname'], $selectedModuleName).'</td>';
							$defaultControls .= '<td width="33%" valign="top">'.getControl($selectedModuleName,$fieldName, $oSelectedModule->getFieldDefinition($fieldName), $defaultValue).'</td>';
							if(in_array($fieldName,$fieldsMarkAsHidden['hiddenFields']))
								$defaultControls .= "<td width='33%'><input type='checkbox' checked='checked' name='ut_hidden_checkbox_".$fieldName."' id='ut_hidden_checkbox_".$fieldName."'></td>";
							else
								$defaultControls .= "<td width='33%'><input type='checkbox' name='ut_hidden_checkbox_".$fieldName."' id='ut_hidden_checkbox_".$fieldName."'></td>";
						$defaultControls .= '</tr>';
					}
				}
            }
            else {
                foreach ($aFieldList['field_list'] as $field_name => $label) {
                        $base_field_layout .= " <li class='ui-state-default sortable'  id='{$field_name}'>{$label}</li> ";
                }
            }
    echo json_encode(   array(
                            'result' => 'field_list' ,
                            'field_list' => $base_field_layout,
                            'required_list' => $aFieldList['required_list'],
                            'field_column1' => $field_selected_column1_li,
                            'field_column2' => $field_selected_column2_li,
                            'available_field_for_required' => $available_field_for_required_li,
							'selected_required_field' => $selected_required_field_li,
							'required_column_list' => $required_column_list,
                            'available_field_for_duplicate' => $available_field_for_duplicate_li,
                            'selected_duplicate_field' => $selected_duplicate_field_li,
                            'duplicate_column_list' => $duplicate_column_list,
							'default_controls' => $defaultControls,
                            )
			);
}
if(isset($_REQUEST['getDefaultValueControls']) && !empty($_REQUEST['selectedFields'])) {
	
	global $current_language,$timedate,$mod_strings;
	$defaultControls = $hiddenDefaultFieldName = "";
	$selectedModuleName = trim($_REQUEST['ele_module']);
	
	if(!empty($_REQUEST['record']))
        $oWebModule = BeanFactory::getBean("UT_WebToModule",$_REQUEST['record']);
    else
        $oWebModule = BeanFactory::getBean("UT_WebToModule");
        
	$defalutFields = (array)json_decode(base64_decode($oWebModule->default_value_fields));
	
	$oSelectedModule = BeanFactory::getBean($selectedModuleName);
    $aFieldList = explode(",",$_REQUEST['selectedFields']);
	
	$fieldsMarkAsHidden = (array)json_decode(base64_decode($oWebModule->fields_mark_as_hidden));
	$defaultControls .= "<tr>";
		$defaultControls .= "<td width='33%' align='left'><b>".$mod_strings['LBL_FIELD_LABEL']."</b></td>";
		$defaultControls .= "<td width='33%'><b>".$mod_strings['LBL_FIELD_CONTROL']."</b></td>";
		$defaultControls .= "<td width='33%'><b>".$mod_strings['LBL_MARK_AS_HIDDEN']."</b></td>";
	$defaultControls .= "</tr>";
	foreach($aFieldList as $fieldName)
	{
		if((!array_key_exists('source', $oSelectedModule->field_defs[$fieldName]) || (array_key_exists('source', $oSelectedModule->field_defs[$fieldName]) && $oSelectedModule->field_defs[$fieldName]['source'] != 'non-db')) && $fieldName != "email1")
		{
			if(array_key_exists($fieldName, $defalutFields) && !empty($defalutFields[$fieldName]))
			{
				if($oSelectedModule->field_defs[$fieldName]['type'] == "multienum")
					$defaultValue = unencodeMultienum($defalutFields[$fieldName]);
				else if($oSelectedModule->field_defs[$fieldName]['type'] == "date")
					$defaultValue = $timedate->to_display_date($defalutFields[$fieldName],false);
				else
					$defaultValue = $defalutFields[$fieldName];
			}
			else
				$defaultValue = "";
			
			$hiddenDefaultFieldName .= $fieldName.",";
			$defaultControls .= "<tr>";
				$defaultControls .= '<td width="33%"><span sugar="slot40">'.translate($oSelectedModule->field_defs[$fieldName]['vname'], $selectedModuleName).'</td>';
				$defaultControls .= '<td width="33%" valign="top">'.getControl($selectedModuleName,$fieldName, $oSelectedModule->getFieldDefinition($fieldName), $defaultValue).'</td>';
				if(in_array($fieldName,$fieldsMarkAsHidden['hiddenFields']))
					$defaultControls .= "<td width='33%'><input type='checkbox' checked='checked' name='ut_hidden_checkbox_".$fieldName."' id='ut_hidden_checkbox_".$fieldName."'></td>";
				else
					$defaultControls .= "<td width='33%'><input type='checkbox' name='ut_hidden_checkbox_".$fieldName."' id='ut_hidden_checkbox_".$fieldName."'></td>";
			$defaultControls .= '</tr>';
		}
	}
			
	echo json_encode(array(
		'result' => 'get_default_controls',
		'default_controls' => $defaultControls,
		'default_field_name' => $hiddenDefaultFieldName
	));
}