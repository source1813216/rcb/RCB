<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

if(empty($_REQUEST['id']) || !isset($_SESSION['authenticated_user_id'])) {
	die("Not a Valid Entry Point");
}
else {
    
        $app_strings = return_application_language($GLOBALS['current_language']);
        $local_location = "UrdhvaTechWebToModule/{$_REQUEST['id']}";
        
        if(!file_exists($local_location))
            die($app_strings['ERR_INVALID_FILE_REFERENCE']);
        $download_location = $local_location;
        $fileName = $_REQUEST['downloadFilename'];
        header("Pragma: public");
		header("Cache-Control: maxage=1, post-check=0, pre-check=0");
        header("Content-Type: application/force-download; charset=".$GLOBALS['locale']->getExportCharset());
        header("Content-type: application/octet-stream; charset=".$GLOBALS['locale']->getExportCharset());
        header("Content-Disposition: attachment; filename=\"".$fileName."\";");
		// disable content type sniffing in MSIE
		header("X-Content-Type-Options: nosniff");
		header("Content-Length: " . filesize($local_location));
		header("Expires: 0");
		set_time_limit(0);

		@ob_end_clean();
		ob_start();
	        readfile($download_location);
		@ob_flush();
}