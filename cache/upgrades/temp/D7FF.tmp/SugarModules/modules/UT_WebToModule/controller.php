<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class UT_WebToModuleController extends SugarController
{
    
    function action_setup_wizard() {
        if(!$this->isLicensed())
            SugarApplication::redirect('index.php?module=UT_WebToModule&action=license');
        $this->view = 'setup_wizard';
    }
	function action_listview() {
		if(!$this->isLicensed())
            SugarApplication::redirect('index.php?module=UT_WebToModule&action=license');
		$this->view = 'list';
	}
	
	function action_detailview() {
		if(!$this->isLicensed())
            SugarApplication::redirect('index.php?module=UT_WebToModule&action=license');
		$this->view = 'detail';
	}
	
	
    function action_editview() {
		if(!$this->isLicensed())
            SugarApplication::redirect('index.php?module=UT_WebToModule&action=license');
        $this->view = 'setup_wizard';
	}
	function isLicensed() {
        require_once('modules/UT_WebToModule/license/OutfittersLicense.php');
        $oOutfittersLicense = new UT_WebToModuleOutfittersLicense();
        $valid = $oOutfittersLicense->isValid("UT_WebToModule");
        if($valid===true)
            return true;
        else
            return false;
    }
}
