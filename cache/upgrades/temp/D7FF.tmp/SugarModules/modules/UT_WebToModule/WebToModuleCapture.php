<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/formbase.php');
require_once("modules/Emails/Email.php");
require_once("modules/UT_WebToModule/utils.php");
require_once('include/upload_file.php');
global $app_strings, $app_list_strings, $sugar_config, $timedate, $current_user,$mod_strings;
$sMainDupQuery = "";
$fields_to_ignore = array('deleted', 'id', 'team_id', 'team_set_id', 'date_modified', 'date_entered', 'modified_user_id', 'assigned_user_id','created_by');
$types_to_ignore = array('link', 'id', 'relate', 'image','datetimecombo');
//$types_to_allow = array('varchar', 'name','email', 'enum', 'phone', 'currency','date','int','bool');
$sources_to_ignore = array('non-db');
$special_field = array('email1');     //To support email address
$sDuplicateWhere = array();
$sEmailQuery = "";
$iInlcudesCustomField = false;
$assignedSecurityGroups = '';
$preventDuplicateUpdate = false;
$duplicateFound = false;
$webModule = $webToModuleRecordId = $webFormUploadURL = $httpReferrer = $httpReferrerHost = $webFormUploadURLHost = "";
$selectedFields = array();
$webModule = (isset($_REQUEST['web_modules']) && !empty($_REQUEST['web_modules']))?$_REQUEST['web_modules']:"";
$webToModuleRecordId = (isset($_REQUEST['ut_web_to_lead_record']) && !empty($_REQUEST['ut_web_to_lead_record']))?$_REQUEST['ut_web_to_lead_record']:"";

// Load Webtomodule record and check for duplicates field
if(!empty($webToModuleRecordId)){
	$oWebToMod = BeanFactory::getBean("UT_WebToModule",$webToModuleRecordId);
	$duplicateFields = (array)json_decode(base64_decode($oWebToMod->duplicate_fields));
	$assignedSecurityGroups = $oWebToMod->assign_security_groups;
	$preventDuplicateUpdate = $oWebToMod->prevent_duplicate_update;
	$selectedFields = (array)json_decode(base64_decode($oWebToMod->selected_fields));
	$webFormUploadURL = $oWebToMod->webform_upload_url;
}
$selectedFields["colsFirst"][] = "assigned_user_id";
if(!empty($webFormUploadURL))
{
	if(isset($_SERVER["HTTP_REFERER"]) && !empty($_SERVER["HTTP_REFERER"]))
		$httpReferrer = $_SERVER["HTTP_REFERER"];

	if(!empty($httpReferrer))
	{
		$httpReferrerURL = parse_url($httpReferrer);
		$httpReferrerHost =  $httpReferrerURL["host"];
		$httpReferrerHost = str_replace("www.", "", $httpReferrerHost);
	}

	if(!empty($webFormUploadURL))
	{
		$webFormUploadURLDB = parse_url($webFormUploadURL);
		$webFormUploadURLHost = $webFormUploadURLDB["host"];
		$webFormUploadURLHost = str_replace("www.", "", $webFormUploadURLHost);
	}
	if($httpReferrerHost == $webFormUploadURLHost || (strpos($httpReferrerHost,$webFormUploadURLHost) !== false))
	{
	}
	else
	{
		die("Please upload the form on correct domain.");
	}
}

if (!empty($webModule) && $webModule == $oWebToMod->web_modules) {
    $current_mod_strings = return_module_language($sugar_config['default_language'], $_POST['web_modules']);
    
	    //adding the client ip address
	    $_POST['client_id_address'] = query_client_ip();
        $db = DBManagerFactory::getInstance();
		    if (isset($_REQUEST['assigned_user_id']) && !empty($_REQUEST['assigned_user_id'])) {
			    $current_user = new User();
			    $current_user->retrieve($_REQUEST['assigned_user_id']);
		    }
            
            $oModule = BeanFactory::getBean($oWebToMod->web_modules);
            
            foreach ($oModule->field_defs as $field_def) {
                if (!empty($duplicateFields) && in_array($field_def['name'],$duplicateFields['duplicateFields']) && !in_array($field_def['type'], $types_to_ignore))
                {
                    if (empty($field_def['source'])) //Undefined index
                    $field_def['source'] = '';
                    if (in_array($field_def['name'],$special_field) || !in_array($field_def['source'], $sources_to_ignore))
                    {
                        if (!in_array($field_def['name'], $fields_to_ignore) && isset($_POST[$field_def['name']]) )
                        {
                            if($field_def['name'] != 'email1' && ($field_def['type'] == 'varchar' || $field_def['type'] == 'name' || $field_def['type'] == 'enum' || $field_def['type'] == 'date' || $field_def['type'] == 'phone' || $field_def['type'] == 'user_name'))
                            {
                                $iIsCstmFld = substr($field_def['name'],-2);
                                if($iIsCstmFld == "_c")
                                    $iInlcudesCustomField = true;
                                $sDuplicateWhere[]= $field_def['name']."='".$_POST[$field_def['name']]."' ";
                            }
                            else if($field_def['type'] == 'currency' || $field_def['type'] == 'int' || $field_def['type'] == 'float'){
                                $iIsCstmFld = substr($field_def['name'],-2);
                                if($iIsCstmFld == "_c")
                                    $iInlcudesCustomField = true;
                                $sDuplicateWhere[]= $field_def['name']."=".$_POST[$field_def['name']];
                            }
                            else if($field_def['type'] == 'bool'){
                                if($_POST[$field_def['name']] == 'on' || $_POST[$field_def['name']] == 'ON' || $_POST[$field_def['name']] == 1){
                                    $iIsCstmFld = substr($field_def['name'],-2);
                                    if($iIsCstmFld == "_c")
                                        $iInlcudesCustomField = true;
                                    $sDuplicateWhere[]= $field_def['name']."=1 ";
                                }
                                else{
                                    $iIsCstmFld = substr($field_def['name'],-2);
                                    if($iIsCstmFld == "_c")
                                        $iInlcudesCustomField = true;
                                    $sDuplicateWhere[]= $field_def['name']."=0 ";
                                }
                            }
                            else if($field_def['type'] == 'varchar' && $field_def['name'] == 'email1'){
                                $sEmailQuery = " INNER JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_module='".$oModule->module_dir."' AND email_addr_bean_rel.bean_id = ".$oModule->table_name.".id  AND email_addr_bean_rel.deleted = 0 
                                        INNER JOIN email_addresses ON email_addresses.id = email_addr_bean_rel.email_address_id AND email_addresses.deleted=0 AND email_addresses.email_address='".$_POST['email1']."'";
                            }
                        }
                    }
                }
            }
            $sLimitQuery = "";
            if(!empty($sDuplicateWhere) || !empty($sEmailQuery)){
                if($sugar_config['dbconfig']['db_type'] == "mysql" || $sugar_config['dbconfig']['db_type'] == "mysqli")
                {
                    $sMainDupQuery="SELECT ".$oModule->table_name.".id FROM ".$oModule->table_name." ";
                    $sLimitQuery = " LIMIT 0,1 ";
                }
                else
                    $sMainDupQuery="SELECT TOP 1 ".$oModule->table_name.".id FROM ".$oModule->table_name." ";
            }
            if($iInlcudesCustomField){
                $sMainDupQuery .=" INNER JOIN ".$oModule->table_name."_cstm ON ".$oModule->table_name."_cstm.id_c=".$oModule->table_name.".id ";
            }
            if(!empty($sEmailQuery)){
                $sMainDupQuery .= $sEmailQuery;
            }
            if(!empty($sDuplicateWhere)){
                $sWhere = implode(' AND ',$sDuplicateWhere);
                if(!empty($sWhere)){
                    $sMainDupQuery .= ' WHERE '.$sWhere;
                    $sMainDupQuery .= " AND ".$oModule->table_name.".deleted=0";
                }
            }
            if(!empty($sMainDupQuery)){
                $sMainDupQuery .= $sLimitQuery;
            }

            if(!empty($sMainDupQuery)){
                $oRes = $db->query($sMainDupQuery,true);
                $aDupRow = $db->fetchByAssoc($oRes);
                if(empty($aDupRow['id'])){
                    $oModule->id = create_guid();
                    $oModule->new_with_id = true;
                }
                else{
					 $duplicateFound = true;
					 if($oModule->module_dir == "Users" && $oModule->object_name == "User")
					 {
						 $userName = '';
						 if(array_key_exists("user_name", $_POST) && !empty($_POST['user_name']))
						 {
							 $userName = $_POST['user_name'];
							 $userName = $userName.mt_rand(1000,9999);
						 }
						 else if(array_key_exists("first_name", $_POST) && !empty($_POST['first_name']) && !array_key_exists("last_name", $_POST))
						 {
							 $firstName = str_replace(" ","",  strtolower(trim($_POST["first_name"])));
							 $userName = $firstName.mt_rand(1000,9999);
						 }
						 else if(!array_key_exists("first_name", $_POST) && array_key_exists("last_name", $_POST) && !empty($_POST['last_name']))
						 {
							 $lastName = str_replace(" ","",strtolower(trim($_POST["last_name"])));
							 $userName = $lastName.mt_rand(1000,9999);
						 }
						 else if(array_key_exists("first_name", $_POST) && !empty($_POST['first_name']) && array_key_exists("last_name", $_POST) && !empty($_POST['last_name']))
						 {
							 $firstName = str_replace(" ","",strtolower(trim($_POST["first_name"])));
							 $lastName = str_replace(" ","",strtolower(trim($_POST["last_name"])));
							 $userName = $firstName."_".$lastName.mt_rand(1000,9999);
						 }
						 else if(!array_key_exists("first_name", $_POST) && !array_key_exists("last_name", $_POST) && array_key_exists("email1", $_POST) && !empty($_POST['email1']))
						 {
							 $emailAddress = trim($_POST["email1"]);
							 if(strpos(trim($emailAddress),"@"))
								 $address = explode("@", $emailAddress);
							 else
								 $userName = $emailAddress.mt_rand(1000,9999);
						 }
						 $_POST['user_name'] = $userName;
					 }
					 else
					 {
						$oModule = BeanFactory::getBean($_POST['web_modules'],$aDupRow['id']);
					 }
                }
            }
            else{
                 $oModule->id = create_guid();
                 $oModule->new_with_id = true;
            }
			$prefix = '';
			if(!empty($_POST['prefix'])){
				$prefix = $_POST['prefix'];
			}
			
			if($oModule->module_dir == "Users" && $oModule->object_name == "User" && array_key_exists("user_hash", $_POST) && !empty($_POST["user_hash"]))
					$_POST["user_hash"] = md5($_POST["user_hash"]);
			
			if($oModule->module_dir == "Users" && $oModule->object_name == "User")
			{
				$_POST["is_admin"] = "0";
				$_POST["status"] = "Inactive";
			}
			
            $GLOBALS['check_notify'] = true;
            //bug: 47574 - make sure, that webtomodule_email1 field has same required attribute as email1 field
            if(isset($oModule->required_fields['email1'])) {
                $oModule->required_fields['webtomodule_email1'] = $oModule->required_fields['email1'];
            }
            
            //bug: 42398 - have to unset the id from the required_fields since it is not populated in the $_POST
            unset($oModule->required_fields['id']);
            unset($oModule->required_fields['team_name']);
            unset($oModule->required_fields['team_count']);

            // Bug #52563 : Web to Lead form redirects to Sugar when duplicate detected
            // prevent duplicates check
            $_POST['dup_checked'] = true;

			// checkRequired needs a major overhaul before it works for web to lead forms.
			if(($duplicateFound && !$preventDuplicateUpdate) || !$duplicateFound)
				$oModule = handleSave($prefix, false, false, false, $oModule,$selectedFields);

			if(!empty($oModule)) {
				if(($duplicateFound && !$preventDuplicateUpdate) || !$duplicateFound)
				{
					if (isset($_POST['email1']) && $_POST['email1'] != null) {
						$oModule->email1 = $_POST['email1'];
					} 
					//in case there are old forms used webtomodule_email1
					elseif (isset($_POST['webtomodule_email1']) && $_POST['webtomodule_email1'] != null) {
						$oModule->email1 = $_POST['webtomodule_email1'];
					}
					if (isset($_POST['email2']) && $_POST['email2'] != null) {
						$oModule->email2 = $_POST['email2'];
					} 
					//in case there are old forms used webtomodule_email2
					elseif (isset($_POST['webtomodule_email2']) && $_POST['webtomodule_email2'] != null) {
						$oModule->email2 = $_POST['webtomodule_email2'];
					}
					if(!empty($GLOBALS['check_notify'])) {
							$oModule->save($GLOBALS['check_notify']);
					}
					else {
							$oModule->save(FALSE);
					}

					if($assignedSecurityGroups)
					{
                                            if($oWebToMod->web_modules == "Users")
                                            {
                                                $securityGroupsId = unencodeMultienum($assignedSecurityGroups);
                                                $oModule->load_relationship('SecurityGroups');
						foreach($securityGroupsId as $groupId)
						{
                                                    $oModule->SecurityGroups->add($groupId);
						}
                                            }
                                            else
                                            {
                                                $assignedUserRole = array();
						$oSecurityGroup = BeanFactory::getBean("SecurityGroups");
						$assignedUserRole = $oSecurityGroup->getUserSecurityGroups($oWebToMod->assigned_user_id);
						$securityGroupsId = unencodeMultienum($assignedSecurityGroups);
						foreach($securityGroupsId as $groupId)
						{
							if(!array_key_exists($groupId,$assignedUserRole) && !is_admin($current_user))
								$oSecurityGroup->addGroupToRecord($oWebToMod->web_modules,$oModule->id,$groupId);
						}
                                            }
					}
					//in case there are forms out there still using email_opt_out
					if(isset($_POST['webtomodule_email_opt_out']) || isset($_POST['email_opt_out'])) {
						if(isset ($oModule->email1) && !empty($oModule->email1)){
							$sea = new SugarEmailAddress();
							$sea->AddUpdateEmailAddress($oModule->email1,0,1);
						}   
						if(isset ($oModule->email2) && !empty($oModule->email2)){
							$sea = new SugarEmailAddress();
							$sea->AddUpdateEmailAddress($oModule->email2,0,1);
						}
					}

					//webtomodule_notes
					if($_FILES['webtomodule_notes']['error'] == "0" )
					{
						$upload_file = new UploadFile('webtomodule_notes');
						$do_final_move = 0;
						if(isset($_FILES['webtomodule_notes']) && $upload_file->confirm_upload())
						{
							$fileNameWithExtesion = array();
							$notesName = $fileName = "";
							$oNote = BeanFactory::getBean("Notes");
							if(isset($_POST['name']) && !empty($_POST['name'])) $notesName .= $_POST['name'];
							if(isset($_POST['first_name']) && !empty($_POST['first_name'])) $notesName .= empty($notesName)?$_POST['first_name']:(" ".$_POST['first_name']);
							if(isset($_POST['last_name']) && !empty($_POST['last_name'])) $notesName .= empty($notesName)?$_POST['last_name']:(" ".$_POST['last_name']);


							$fileNameWithExtesion = explode(".", $_FILES['webtomodule_notes']['name']);
							if(is_array($fileNameWithExtesion) && array_key_exists("0", $fileNameWithExtesion) && !empty($fileNameWithExtesion[0]))
									$fileName = $fileNameWithExtesion[0];

							if(!empty($notesName) && !empty($fileName))
								$oNote->name = $notesName."-".$fileName;
							else if(!empty($notesName) && empty($fileName))
								$oNote->name = $notesName;
							else if(empty($notesName) && !empty($fileName))
								$oNote->name = $fileName;
							else
								$oNote->name = "Notes From WebToModule";

							$oNote->filename = $upload_file->get_stored_file_name();
							$oNote->parent_type = $oModule->module_name;
							$oNote->parent_id = $oModule->id;
                                                        if($oModule->module_name == "Contacts" && !empty($oModule->module_name) && !empty($oModule->id))
                                                        {
                                                            $oNote->contact_name = $oModule->name;
                                                            $oNote->contact_id = $oModule->id;
                                                        }
							$oNote->assigned_user_id = $_REQUEST['assigned_user_id'];
							$oNote->file_mime_type = $upload_file->mime_type;
							$oNote->save();
							$upload_file->final_move($oNote->id);
						}
					}

					if( ( array_key_exists("web_auto_response_template",$_REQUEST) && (array_key_exists("email1",$_REQUEST) || array_key_exists("email2",$_REQUEST)) )
						&& (!empty($_REQUEST['web_auto_response_template']) && (!empty($_REQUEST['email1']) || !empty($_REQUEST['email2'])))) {
						$emailTemplatesId = $_REQUEST['web_auto_response_template'];
						$mail = new SugarPHPMailer();
						$emailTemp = BeanFactory::getBean("EmailTemplates", $emailTemplatesId);
						$emailObj = new Email();
						$defaults = $emailObj->getSystemDefaultEmail();

						$mail->From = $defaults['email'];
						$mail->FromName = $defaults['name'];
						$mail->ClearAllRecipients();
						$mail->addReplyTo($emailTemp->from_address,$emailTemp->from_name);
						
						$receiverName = '';
						$receiverName = $oModule->name;
						if(empty($receiverName))
						{
							if(!empty($oModule->first_name))
								$receiverName = $oModule->first_name." ".$oModule->last_name;
							else
								$receiverName = $oModule->last_name;
						}
						if(!empty($_REQUEST['email1']))
							$mail->AddAddress($_REQUEST['email1'] ,$reveiverName);
						else
							$mail->AddAddress($_REQUEST['email2'] , $reveiverName);

						$mail->Subject = from_html($emailTemp->subject);
						$mail->Body = EmailTemplate::parse_template_bean($emailTemp->body_html,$oModule->module_name,$oModule);
						$mail->IsHTML(true);
						$mail->prepForOutbound();
						$mail->setMailerForSystem();

						if (!$mail->Send())
							$GLOBALS['log']->fatal('ERROR: Urdhva Tech Lead Response Message Send Failed');
					}
				}
            }
			if(isset($_POST['redirect_url']) && !empty($_POST['redirect_url'])) {
			    // Get the redirect url, and make sure the query string is not too long
		        $redirect_url = $_POST['redirect_url'];
		        $query_string = '';
				$first_char = '&';
				if(strpos($redirect_url, '?') === FALSE){
					$first_char = '?';
				}
				$first_iteration = true;
				$get_and_post = array_merge($_GET, $_POST);
				foreach($get_and_post as $param => $value) {

					if($param == 'redirect_url' && $param == 'submit')
						continue;
					
					if($first_iteration){
						$first_iteration = false;
						$query_string .= $first_char;
					}
					else{
						$query_string .= "&";
					}
					$query_string .= "{$param}=".urlencode($value);
				}
				if(empty($oModule)) {
					if($first_iteration){
						$query_string .= $first_char;
					}
					else{
						$query_string .= "&";
					}
					$query_string .= "error=1";
				}
				
				$redirect_url = $redirect_url;
				
				if($duplicateFound && $preventDuplicateUpdate)
				{
					if(strpos($redirect_url, '?'))
						$redirect_url .= "&status=duplicate_found";
					else
						$redirect_url .= "?status=duplicate_found";
				}
				if($duplicateFound && !$preventDuplicateUpdate)
				{
					if(strpos($redirect_url, '?'))
						$redirect_url .= "&status=updated";
					else
						$redirect_url .= "?status=updated";
				}
				if(!$duplicateFound)
				{
					if(strpos($redirect_url, '?'))
						$redirect_url .= "&status=created";
					else
						$redirect_url .= "?status=created";
				}

				// Check if the headers have been sent, or if the redirect url is greater than 2083 characters (IE max URL length)
				//   and use a javascript form submission if that is the case.
			    if(headers_sent() || strlen($redirect_url) > 2083) {
    				echo '<html ' . get_language_header() . '><head><title>SugarCRM</title></head><body>';
    				echo '<form name="redirect" action="' .$_POST['redirect_url']. '" method="GET">';
    
    				foreach($_POST as $param => $value) {
    					if($param != 'redirect_url' ||$param != 'submit') {
    						echo '<input type="hidden" name="'.$param.'" value="'.$value.'">';
    					}
    				}
    				if(empty($oModule)) {
    					echo '<input type="hidden" name="error" value="1">';
    				}
    				echo '</form><script language="javascript" type="text/javascript">document.redirect.submit();</script>';
    				echo '</body></html>';
    			}
				else{
    				header("Location: {$redirect_url}");
    				die();
			    }
			}
			else {
				echo $mod_strings['LBL_THANKS_FOR_SUBMITTING'];
			}
			sugar_cleanup();
			// die to keep code from running into redirect case below
			die();
}
else
{
	echo "Please provide valid module name.";
	die();
}

if (!empty($_POST['redirect'])) {
    if(headers_sent()){
    	echo '<html ' . get_language_header() . '><head><title>SugarCRM</title></head><body>';
    	echo '<form name="redirect" action="' .$_POST['redirect']. '" method="GET">';
    	echo '</form><script language="javascript" type="text/javascript">document.redirect.submit();</script>';
    	echo '</body></html>';
    }
    else{
    	header("Location: {$_POST['redirect']}");
    	die();
    }
}

echo $mod_strings['LBL_SERVER_IS_CURRENTLY_UNAVAILABLE'];

?>
