<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
global $mod_strings,$app_strings;
//if(ACLController::checkAccess('UT_WebToModule', 'edit', true))$module_menu[]=Array("index.php?module=UT_WebToModule&action=EditView&return_module=UT_WebToModule&return_action=DetailView", $mod_strings['LNK_NEW_RECORD'],"CreateUT_WebToModule");
$module_menu[] = array(
		"index.php?module=UT_WebToModule&action=setup_wizard&return_module=UT_WebToModule&return_action=index",
		$mod_strings['LBL_WEB_TO_MODULE'],"CreateWebToModuleForm"
	);
if(ACLController::checkAccess('UT_WebToModule', 'list', true))$module_menu[]=Array("index.php?module=UT_WebToModule&action=index&return_module=UT_WebToModule&return_action=DetailView", $mod_strings['LNK_LIST'],"UT_WebToModule");
