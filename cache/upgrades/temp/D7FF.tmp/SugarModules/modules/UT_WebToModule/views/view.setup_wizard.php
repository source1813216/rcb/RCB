<?php        
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
require_once('include/MVC/View/SugarView.php');
require_once("modules/UT_WebToModule/utils.php");
class Viewsetup_wizard extends SugarView {

    public function __construct() {
        parent::SugarView();
    }

    public function display() {
        global $app_strings, $app_list_strings, $mod_strings,$current_user,$db,$sugar_config;
        if(!empty($_REQUEST['record'])) {
            $this->ss->assign('record', $_REQUEST['record']);
            $oWebToModule = BeanFactory::getBean("UT_WebToModule",$_REQUEST['record']);
        }
        else {
            $oWebToModule = BeanFactory::getBean("UT_WebToModule");    
        }
        $utIsDuplicate = 'No';
		if(array_key_exists('isDuplicate', $_REQUEST) && $_REQUEST['isDuplicate'] == "true")
			$utIsDuplicate = 'Yes';
        // step 1
        $this->ss->assign("app", $app_strings);
        $this->ss->assign("mod", $mod_strings);
        
        if(!empty($oWebToModule->web_modules))
            $module_options = get_select_options_with_id($oWebToModule->getModuleList(),$oWebToModule->web_modules);
        else
            $module_options = get_select_options_with_id($oWebToModule->getModuleList(),"");

        $this->ss->assign("module_options", $module_options); 
        
        if(!empty($oWebToModule->id)) $this->ss->assign("WEB_TO_MODULE_RECORD_ID_VALUE",$oWebToModule->id);
        if(!empty($oWebToModule->name)) $this->ss->assign("WEB_TO_LEAD_RECORD_NAME_VALUE",$oWebToModule->name);
        if(!empty($oWebToModule->web_header)) $this->ss->assign("WEB_HEADER_VALUE",$oWebToModule->web_header);
        if(!empty($oWebToModule->web_description)) $this->ss->assign("WEB_DESCRIPTION_VALUE",$oWebToModule->web_description);
        if(!empty($oWebToModule->web_submit)) $this->ss->assign("WEB_SUBMIT_VALUE",$oWebToModule->web_submit);
        if(!empty($oWebToModule->post_url)) $this->ss->assign("POST_URL_VALUE",$oWebToModule->post_url);
		if(!empty($oWebToModule->post_url)) $this->ss->assign("UT_IS_DUPLICATE",$utIsDuplicate);
        if(!empty($oWebToModule->redirect_url))
            $this->ss->assign("REDIRECT_URL_VALUE",$oWebToModule->redirect_url);
        else
            $this->ss->assign("REDIRECT_URL_VALUE","http://");
        
        if($oWebToModule->set_captcha == "1")
            $this->ss->assign("WEB_SET_CAPTCHA_VALUE",'checked=""');
        else
            $this->ss->assign("WEB_SET_CAPTCHA_VALUE",'');
		
		if(!empty($oWebToModule->captcha_key)) $this->ss->assign("CAPTCHA_KEY_VALUE",$oWebToModule->captcha_key);
        
		if($oWebToModule->file_control == "1")
            $this->ss->assign("FILE_CONTROL_VALUE",'checked=""');
        else
            $this->ss->assign("FILE_CONTROL_VALUE",'');
		
		if(!empty($oWebToModule->file_control_label)) $this->ss->assign("FILE_CONTROL_LABEL_VALUE",$oWebToModule->file_control_label);
		
        //Post URL
        $web_post_url = $sugar_config['site_url'].'/index.php?module=UT_WebToModule&entryPoint=WebToModuleCapture';
        if(!empty($oWebToModule->post_url))
            $web_post_url = $oWebToModule->post_url;
                
        $this->ss->assign("POST_URL_VALUE",$web_post_url);
	
        $webformUploadURL = "";
        if(!empty($oWebToModule->webform_upload_url))
            $webformUploadURL = $oWebToModule->webform_upload_url;
                
        $this->ss->assign("WEBFORM_UPLOAD_URL_VALUE",$webformUploadURL);
        
        
        //Email Template
        if(!empty($oWebToModule->auto_response_template))
            $email_template_options = get_email_template($oWebToModule->auto_response_template);
        else
            $email_template_options = get_email_template("");
        $this->ss->assign("AUTO_RESPONSE_EMAIL_TEMPLATES",$email_template_options);
		
		if(!empty($oWebToModule->assign_security_groups))
			$select_securitygroup_options = getUTSecurityGroups(unencodeMultienum($oWebToModule->assign_security_groups));
		else
			$select_securitygroup_options = getUTSecurityGroups(array());
		$this->ss->assign("SECURITYGROUPOPTIONS",$select_securitygroup_options);
		
		if($oWebToModule->prevent_duplicate_update == "1")
            $this->ss->assign("WEB_PREVENT_DUPLICATE_UPDATE_VALUE",'checked=""');
        else
            $this->ss->assign("WEB_PREVENT_DUPLICATE_UPDATE_VALUE",'');
            
        //Assign user Start  // Users Popup
        $json = getJSONobj();
        $popup_request_data = array(
            'call_back_function' => 'set_return',
            'form_name' => 'field_form_submit',
            'field_to_name_array' => array(
                'id' => 'assigned_user_id',
                'user_name' => 'assigned_user_name',
                ),
        );
        $this->ss->assign('encoded_users_popup_request_data', $json->encode($popup_request_data)); 
        
        //If no user set by default current user
        if(empty($oWebToModule->assigned_user_id))
            $oWebToModule->assigned_user_id = $current_user->id;
            
        $this->ss->assign("ASSIGNED_USER_ID_VALUE",$oWebToModule->assigned_user_id);
        
        $this->ss->assign("ASSIGNED_USER_NAME_VALUE",get_assigned_user_name($oWebToModule->assigned_user_id));
        
        require_once('include/QuickSearchDefaults.php');
        $qsd = QuickSearchDefaults::getQuickSearchDefaults();
        $sqs_objects = array('assigned_user_name' => $qsd->getQSUser());
        
        //Assign User End
        
        if(!empty($oWebToModule->web_footer)) $this->ss->assign("WEB_FOOTER_VALUE",$oWebToModule->web_footer);     
        global $current_user, $sugar_config;
        $this->ss->assign('site_url', $sugar_config['site_url']);
        $this->ss->assign('current_user', json_encode(array('user_name' => $current_user->user_name, 'is_admin' => $current_user->is_admin)));
        $javascript =  '<script type="text/javascript" language="javascript">sqs_objects = ' . $json->encode($sqs_objects) . '</script>';
        $this->ss->assign('javascript', $javascript);
        $this->ss->display('modules/UT_WebToModule/tpls/wizard.tpl');
        
        
    }

}