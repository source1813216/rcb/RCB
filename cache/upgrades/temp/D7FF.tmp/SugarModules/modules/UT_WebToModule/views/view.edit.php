<?php
/*
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*/
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class UT_WebToModuleViewEdit extends ViewEdit
{
    public function __construct()
    {
        parent::ViewEdit();
    }
    public function display()
    {
        global $sugar_config;
        $recordId = $moduleName = $action = $returnModule = $returnAction = '';
        if(!empty($this->bean->id))
            $recordId = $this->bean->id;
        
        $moduleName = "UT_WebToModule";
        $action = "WebToModuleCreation";
        $returnModule = "UT_WebToModule";
        $returnAction = "index";
        
        $redirectUrl = $sugar_config['site_url']."/index.php?module=".$moduleName."&action=".$action;
        if(!empty($recordId))
            $redirectUrl .= "&record=".$recordId;
        
        $redirectUrl .= "&return_module=".$returnModule;
        $redirectUrl .= "&return_action=".$returnAction;
        SugarApplication::redirect($redirectUrl);
        parent::display();
    }
}