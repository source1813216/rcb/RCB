<?php
$module_name = 'UT_WebToModule';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
		'enctype'=>'multipart/form-data',
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'auto_response_template',
        ),
        1 => 
        array (
            0 => 'web_modules',
            1 => 'set_captcha',
        ),
        2 => 
        array (
          0 => 'date_entered',
          1 => 'date_modified',
        ),
        3 => 
        array (
          0 => 'description',
          1 => 'assigned_user_name',
        ),
        4 => array(
            0=>array(
                'label' => 'LBL_DOWNLOAD_WEB_TO_MODULE',
                'customCode' => '{$DOWNLOADWEBTOMODULEFORM}',
            ),
        ),
      ),
    ),
  ),
);
?>
