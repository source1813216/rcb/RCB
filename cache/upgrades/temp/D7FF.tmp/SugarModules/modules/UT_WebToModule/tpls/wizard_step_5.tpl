{* 
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*}
{literal}
    <style>
        .droptrue li { list-style-type: none; margin: 1; padding: 0; margin-right: 10px; background: #eee; padding: 5px;}
        .droptrue { padding: 0;margin:1px;height:200px;}
    </style>
{/literal}
<div style='padding:10px;'>
    <form name='duplicate_field_list' id='duplicate_field_list' method='POST'>
        <input type='hidden' name='step4_from' id='step4_from' value='duplicate_field_list'>
        <table class="edit view" width="100%">
            <tr>
                <td scope="col" colspan="2" width="100%">
                    <div id="div_available_field_for_duplicate" style="float:left;margin-right: 10px;"><!-- div_column_1_module_required -->
                        <fieldset id="available_field_for_duplicate" style="height:300px;width:250px;padding:.35em .625em .75em !important;border:1px solid silver !important;margin:0 2px !important;"><!-- column_1_module_required -->
                            <legend style="margin-bottom:0px;border:0 !important;width:auto !important;padding-left:0;">{$mod.LBL_AVAILABLE_FIELDS_TO_MARK_DUPLICATE}</legend>
                            <ul class="droptrue connectedSortable" style="overflow:auto;height:270px;width:238px;" >
                                
                            </ul>
                        </fieldset>
                    </div>
                    <div id="div_selected_duplicate_field" style="float:left;margin-right: 10px;"><!-- div_column_2_module_required-->
                        <fieldset id="selected_duplicate_field" style="height:300px;width:250px;padding:.35em .625em .75em !important;border:1px solid silver !important;margin:0 2px !important;"><!-- column_2_module_required -->
                            <legend style="margin-bottom:0px;border:0 !important;width:auto !important;padding-left:0;">{$mod.LBL_SELECTED_DUPLICATE_FIELDS}</legend>
                            <ul class="droptrue connectedSortable" style="overflow:auto;height:270px;width:238px;">
                                
                            </ul>
                        </fieldset>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</div>