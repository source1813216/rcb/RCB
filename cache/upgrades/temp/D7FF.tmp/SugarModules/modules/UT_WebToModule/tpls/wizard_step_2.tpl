{* 
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*}
{literal}
    <style>
        .droptrue li { list-style-type: none; margin: 1; padding: 0; margin-right: 10px; background: #eee; padding: 5px;}
        .droptrue { padding: 0;margin:1px;height:200px;}
    </style>
{/literal}
<div style='padding:10px;'>
    <form name='module_layout' id='module_layout' method='POST'>
        <input type='hidden' name='step2_from' id='step2_from' value='module_layout'>
        <input type='hidden' name='module_changed' value=''>
        <table class="edit view" width="100%">
            <tr>
                <td scope="col" colspan="2" width="100%">
                   <input id="fields_add_remove_button" type='button' title="Add All Fields"
			class="button"
			onclick="addRemoveAllFields('{$app.LBL_ADD_ALL_LEAD_FIELDS}','{$app.LBL_REMOVE_ALL_LEAD_FIELDS}');"
			name="fields_add_remove_button" value="{$app.LBL_ADD_ALL_LEAD_FIELDS}">
                </td>
            </tr>
            <tr>
                <td scope="col" colspan="2" width="100%">
                    <div id="field_list" style="float:left;margin-right: 10px;">
                        <fieldset id="base_field_list" style="height:300px;width:250px;padding:.35em .625em .75em !important;border:1px solid silver !important;margin:0 2px !important;">
                            <legend style="margin-bottom:0px;border:0 !important;width:auto !important;padding-left:0;">{$mod.LBL_AVAILABLE_FIELDS}</legend>
                            <ul  class="droptrue connectedSortable" style="overflow:auto;height:270px;width:238px;">
                                {$field_not_selected}
                            </ul>
                        </fieldset>
                    </div>
                    <div id="div_column_1_module" style="float:left;margin-right: 10px;">
                        <fieldset id="column_1_module" style="height:300px;width:250px;padding:.35em .625em .75em !important;border:1px solid silver !important;margin:0 2px !important;">
                            <legend style="margin-bottom:0px;border:0 !important;width:auto !important;padding-left:0;">{$mod.LBL_FIELD_COLUMN1}</legend>
                            <ul class="droptrue connectedSortable" style="overflow:auto;height:270px;width:238px;" >
                                {$field_selected_column1}
                            </ul>
                        </fieldset>
                    </div>
                    <div id="div_column_2_module" style="float:left;margin-right: 10px;">
                        <fieldset id="column_2_module" style="height:300px;width:250px;padding:.35em .625em .75em !important;border:1px solid silver !important;margin:0 2px !important;">
                            <legend style="margin-bottom:0px;border:0 !important;width:auto !important;padding-left:0;">{$mod.LBL_FIELD_COLUMN2}</legend>
                            <ul class="droptrue connectedSortable" style="overflow:auto;height:270px;width:238px;">
                                {$field_selected_column2}
                            </ul>
                        </fieldset>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</div>