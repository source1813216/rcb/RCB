{* 
 Created By   : Urdhva Tech Pvt. Ltd.
 Created date : 2020-10-01
 Contact at   : contact@urdhva-tech.com
          Web : www.urdhva-tech.com
        Skype : urdhvatech
       Module : WebToModule
*}
{literal}
    <style>
        .droptrue li { list-style-type: none; margin: 1; padding: 0; margin-right: 10px; background: #eee; padding: 5px;}
        .droptrue { padding: 0;margin:1px;height:200px;}
        .list_droptrue li { list-style-type: none; margin: 1; padding: 0; margin-right: 10px; background: #eee; padding: 5px;}
        .list_droptrue { padding: 0;margin:1px;height:200px;}
    </style>
{/literal}
<div style='padding:10px;'>
    <form name="field_form_submit" id="field_form_submit" method='POST'>
        <textarea id='field_layout_duplicate' name='field_layout_duplicate' style='display:none;'></textarea>
        <textarea id='field_layout_required' name='field_layout_required' style='display:none;'></textarea>
        <textarea id='field_layout' name='field_layout' style='display:none;'></textarea>
        <textarea id='fields_with_default_value' name='fields_with_default_value' style='display:none;'></textarea>
        <input type='hidden' name='ut_is_duplicate' value='{$UT_IS_DUPLICATE}'>
        <input type='hidden' name='action' value='GenerateWebToModuleForm'>
        <input type='hidden' name='module' id='module' value='UT_WebToModule'>
        <input type="hidden" id='web_modules' name='web_modules'>
        <input type="hidden" id='web_modules_record' name='web_modules_record' value='{$WEB_TO_MODULE_RECORD_ID_VALUE}'/>
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="edit view">
            <tr>
                <td>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                    <tr>
                        <td scope="row"><span sugar='slot40'>{$mod.LBL_NAME_RECORD_NAME}</span><span class="required">{$app.LBL_REQUIRED_SYMBOL}</span></td>
                        <td  width="80%" valign="top"><input id="web_to_lead_record_name" name="web_to_lead_record_name" title="Name" size="50"
                                                                value="{$WEB_TO_LEAD_RECORD_NAME_VALUE}" type="text">&nbsp;&nbsp;<span class="required">{$mod.LBL_NAME_RECORD_NAME_HELP}</span></td>
                    </tr>
                    <tr>
                        <td scope="row">{$mod.LBL_DEFINE_LEAD_HEADER}</td>
                        <td  width="80%" valign="top"><input id="web_header" name="web_header" title="Name" size="50"
                            value="{$WEB_HEADER_VALUE}" type="text"></td>
                    </tr>
                    <tr>
                        <td scope="row">{$mod.LBL_DESCRIPTION_LEAD_FORM}</td>
                        <td><textarea  name='web_description' rows='2' cols='57'>{$WEB_DESCRIPTION_VALUE}</textarea></td>
                    </tr>
                    <tr>
                        <td scope="row">{$mod.LBL_DEFINE_LEAD_SUBMIT}</td>
                        <td valign="top"><input id="web_submit" name="web_submit" title="Name" size="50" value="{$WEB_SUBMIT_VALUE}" type="text"></td>
                    </tr>
                    <tr>
                        <td scope="row">{$mod.LBL_DEFINE_LEAD_POST_URL}</td>
                        <td><input id="post_url" name="post_url" size="50" disabled='true' value="{$POST_URL_VALUE}" type="text"></td>
                    </tr>
                    <tr>
                        <td scope="row">{$mod.LBL_DEFINE_LEAD_REDIRECT_URL}</td>
                        <td><input id="redirect_url" name="redirect_url" size="50" value="{$REDIRECT_URL_VALUE}" type="text"></td>
                    </tr>
                    <tr>
                         <td scope="row"><span sugar='slot45'>{$app.LBL_ASSIGNED_TO}</span><span class="required">{$app.LBL_REQUIRED_SYMBOL}</span></td>
                             <td><span sugar='slot45b'><input class="sqsEnabled" autocomplete="off" id="assigned_user_name" name='assigned_user_name' type="text" value="{$ASSIGNED_USER_NAME_VALUE}"><input id='assigned_user_id' name='assigned_user_id' type="hidden" value="{$ASSIGNED_USER_ID_VALUE}" />
                         <input title="{$app.LBL_SELECT_BUTTON_TITLE}" type="button"  class="button" value='{$app.LBL_SELECT_BUTTON_LABEL}' name=btn1
                          onclick='open_popup("Users", 600, 400, "", true, false, {$encoded_users_popup_request_data});' /></span></td>
                    </tr>
                    <tr>
                        <td scope="row">{$mod.LBL_AUTO_RESPONSE_TEMPLATE}&nbsp;{sugar_help text=$MOD.WEB_AUTO_RESPONSE_TEMPLATE_HELP_TEXT WIDTH=250}</td>
                        <td>
                            <select name="web_auto_response_template" id="web_auto_response_template">
                                {$AUTO_RESPONSE_EMAIL_TEMPLATES}
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td scope="row">{$mod.LBL_PREVENT_DUPLICATE_UPDATE}&nbsp;{sugar_help text=$MOD.LBL_PREVENT_DUPLICATE_UPDATE_TEXT WIDTH=250}</td>
                        <td><input id="web_prevent_duplicate_update" name="web_prevent_duplicate_update" title="Prevent Duplicate Update" type="checkbox" {$WEB_PREVENT_DUPLICATE_UPDATE_VALUE}></td>
                    </tr>
                    
                    <tr id="assign_security_groups_row" name="assign_security_groups_row">
                        <td scope="row">{$mod.LBL_ASSIGN_SECURITY_GROUPS}&nbsp;{sugar_help text=$MOD.LBL_ASSIGN_SECURITY_GROUPS_TEXT WIDTH=250}</td>
                        <td>
                            <select multiple name="web_assign_security_groups[]" id="web_assign_security_groups">
                                {$SECURITYGROUPOPTIONS}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row">{$mod.LBL_WEBFORM_UPLOAD_URL}&nbsp;{sugar_help text=$MOD.LBL_WEBFORM_UPLOAD_URL_HELP WIDTH=250}</td>
                        <td><input id="webform_upload_url" name="webform_upload_url" size="50" value="{$WEBFORM_UPLOAD_URL_VALUE}" type="text"></td>
                    </tr>
                    <tr>
                        <td scope="row">{$mod.LBL_SET_CAPTCHA}</td>
                        <td><input id="web_set_captcha" onchange="getCaptchaKey();" name="web_set_captcha" title="Captcha" type="checkbox" {$WEB_SET_CAPTCHA_VALUE}></td>
                    </tr>
                    <tr id="captcha_key_row" name="captcha_key_row" style="display:none">
                        <td scope="row">{$mod.LBL_CAPTCHA_KEY}<span class="required">{$app.LBL_REQUIRED_SYMBOL}</span></td>
                        <td><input id="captcha_key" name="captcha_key" size="40" value="{$CAPTCHA_KEY_VALUE}" type="text"></td>
                    </tr>
                    <tr>
                        <td scope="row">{$mod.LBL_FILE_CONTROL}&nbsp;{sugar_help text=$MOD.LBL_FILE_CONTROL_HELP WIDTH=250}</td>
                        <td><input id="file_control" onchange="getFileControlLabel();" name="file_control" title="File Control" type="checkbox" {$FILE_CONTROL_VALUE}></td>
                     </tr>
                     <tr id="file_control_label_row" name="file_control_label_row" style="display:none">
                        <td scope="row">{$mod.LBL_FILE_CONTROL_TEXT_LABEL}</td>
                        <td><input id="file_control_label" name="file_control_label" title="File Control Label" type="text" value="{$FILE_CONTROL_LABEL_VALUE}"></td>
                     </tr>
            <!-- BEGIN: open_source -->
            <!-- END: open_source -->
                    <tr>
                        <td scope="row">{$mod.LBL_LEAD_FOOTER}</td>
                        <td><textarea  name='web_footer' rows='2' cols='57'>{$WEB_FOOTER_VALUE}</textarea></td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
    </form>
</div>
{$javascript}