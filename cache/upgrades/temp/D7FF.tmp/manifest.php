<?php
$manifest = array (
  'acceptable_sugar_versions' => array(
        "regex_matches" => array(
                    0 => "6\\.5\\.*",
                    1 => "6\\.6\\.*",
                    2 => "6\\.7\\.*",
                    )
    ),
    'acceptable_sugar_flavors' => array(
        'OS', 'PRO', 'ENT', 'CE', 'ULT', 'CORP'
    ),
  'readme' => '',
  'key' => 'UT',
  'author' => 'Urdhva Tech Pvt Ltd',
  'description' => 'Create web form for any module by Urdhva Tech',
  'icon' => '',
  'is_uninstallable' => true,
  'name' => 'WebToModule',
  'published_date' => '2020-10-01 16:00:00',
  'type' => 'module',
  'version' => '5.3',
  'remove_tables' => 'prompt',
);


$installdefs = array (
  'id' => 'UT_WebToModule',
  'beans' => 
  array (
    0 => 
    array (
      'module' => 'UT_WebToModule',
      'class' => 'UT_WebToModule',
      'path' => 'modules/UT_WebToModule/UT_WebToModule.php',
      'tab' => true,
    ),
  ),
  'image_dir' => '<basepath>/icons',
  'copy' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/modules/UT_WebToModule',
      'to' => 'modules/UT_WebToModule',
    ),
  ),
  'language' => 
  array (
    0 => 
    array (
      'from' => '<basepath>/SugarModules/language/application/en_us.WebToModule.php',
      'to_module' => 'application',
      'language' => 'en_us',
    ),
	1 => 
    array (
		'from' => '<basepath>/SugarModules/Administration/Ext/Language/en_us.WebToModule.php',
		'to_module' => 'Administration',
		'language' =>'en_us'
    ),
  ),
  'action_view_map' => array(
	array(
		'from' => '<basepath>/SugarModules/ActionViewMap/WebToModule.php',
		'to_module' => 'UT_WebToModule',
	)
  ),
  'administration' => array(
	array(
	   'from' => '<basepath>/SugarModules/Administration/Ext/Administration/WebToModule.php'
	)
  ),
  'post_uninstall' => 
    array
    (
        '<basepath>/scripts/post_uninstall.php',
    ),
);