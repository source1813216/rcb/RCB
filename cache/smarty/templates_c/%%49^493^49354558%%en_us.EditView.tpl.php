<?php /* Smarty version 2.6.31, created on 2022-11-30 11:13:47
         compiled from custom/include/SugarFields/Fields/Address/en_us.EditView.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'custom/include/SugarFields/Fields/Address/en_us.EditView.tpl', 13, false),array('modifier', 'lower', 'custom/include/SugarFields/Fields/Address/en_us.EditView.tpl', 14, false),array('modifier', 'cat', 'custom/include/SugarFields/Fields/Address/en_us.EditView.tpl', 15, false),array('modifier', 'in_array', 'custom/include/SugarFields/Fields/Address/en_us.EditView.tpl', 26, false),array('modifier', 'default', 'custom/include/SugarFields/Fields/Address/en_us.EditView.tpl', 37, false),)), $this); ?>
{**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 *}
<script type="text/javascript"
        src='{sugar_getjspath file="include/SugarFields/Fields/Address/SugarFieldAddress.js"}'></script>
<?php $this->assign('key', ((is_array($_tmp=$this->_tpl_vars['displayParams']['key'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp))); ?>
<?php $this->assign('keyLow', ((is_array($_tmp=$this->_tpl_vars['displayParams']['key'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp))); ?>
<?php $this->assign('street', ((is_array($_tmp=$this->_tpl_vars['displayParams']['key'])) ? $this->_run_mod_handler('cat', true, $_tmp, '_address_street') : smarty_modifier_cat($_tmp, '_address_street'))); ?>
<?php $this->assign('city', ((is_array($_tmp=$this->_tpl_vars['displayParams']['key'])) ? $this->_run_mod_handler('cat', true, $_tmp, '_address_city') : smarty_modifier_cat($_tmp, '_address_city'))); ?>
<?php $this->assign('state', ((is_array($_tmp=$this->_tpl_vars['displayParams']['key'])) ? $this->_run_mod_handler('cat', true, $_tmp, '_address_state') : smarty_modifier_cat($_tmp, '_address_state'))); ?>
<?php $this->assign('country', ((is_array($_tmp=$this->_tpl_vars['displayParams']['key'])) ? $this->_run_mod_handler('cat', true, $_tmp, '_address_country') : smarty_modifier_cat($_tmp, '_address_country'))); ?>
<?php $this->assign('postalcode', ((is_array($_tmp=$this->_tpl_vars['displayParams']['key'])) ? $this->_run_mod_handler('cat', true, $_tmp, '_address_postalcode') : smarty_modifier_cat($_tmp, '_address_postalcode'))); ?>
<fieldset id='<?php echo $this->_tpl_vars['key']; ?>
_address_fieldset'>
    <legend>{sugar_translate label='LBL_<?php echo $this->_tpl_vars['key']; ?>
_ADDRESS' module='<?php echo $this->_tpl_vars['module']; ?>
'}</legend>
    <table border="0" cellspacing="1" cellpadding="0" class="edit" width="100%">
        <tr>
            <td valign="top" id="<?php echo $this->_tpl_vars['street']; ?>
_label" width='25%' scope='row'>
                <label for="<?php echo $this->_tpl_vars['street']; ?>
">{sugar_translate label='LBL_<?php echo $this->_tpl_vars['key']; ?>
_STREET' module='<?php echo $this->_tpl_vars['module']; ?>
'}:</label>
                {if $fields.<?php echo $this->_tpl_vars['street']; ?>
.required || <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['street'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)))) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['displayParams']['required']) : smarty_modifier_in_array($_tmp, $this->_tpl_vars['displayParams']['required']))): ?>true<?php else: ?>false<?php endif; ?>}
                <span class="required">{$APP.LBL_REQUIRED_SYMBOL}</span>
                {/if}
            </td>
            <td width="*">
                {*Added By Biztech*}
                {if $fields.<?php echo $this->_tpl_vars['street']; ?>
.acl <= 1}
                    <label id="<?php echo $this->_tpl_vars['street']; ?>
" name="<?php echo $this->_tpl_vars['street']; ?>
">{$fields.<?php echo $this->_tpl_vars['street']; ?>
.value}</label>
                {else}
                <?php if ($this->_tpl_vars['displayParams']['maxlength']): ?>
                <textarea id="<?php echo $this->_tpl_vars['street']; ?>
" name="<?php echo $this->_tpl_vars['street']; ?>
" maxlength="<?php echo $this->_tpl_vars['displayParams']['maxlength']; ?>
"
                          rows="<?php echo ((is_array($_tmp=@$this->_tpl_vars['displayParams']['rows'])) ? $this->_run_mod_handler('default', true, $_tmp, 4) : smarty_modifier_default($_tmp, 4)); ?>
" cols="<?php echo ((is_array($_tmp=@$this->_tpl_vars['displayParams']['cols'])) ? $this->_run_mod_handler('default', true, $_tmp, 60) : smarty_modifier_default($_tmp, 60)); ?>
"
                          tabindex="<?php echo $this->_tpl_vars['tabindex']; ?>
">{$fields.<?php echo $this->_tpl_vars['street']; ?>
.value}</textarea>
                <?php else: ?>
                <textarea id="<?php echo $this->_tpl_vars['street']; ?>
" name="<?php echo $this->_tpl_vars['street']; ?>
" rows="<?php echo ((is_array($_tmp=@$this->_tpl_vars['displayParams']['rows'])) ? $this->_run_mod_handler('default', true, $_tmp, 4) : smarty_modifier_default($_tmp, 4)); ?>
"
                          cols="<?php echo ((is_array($_tmp=@$this->_tpl_vars['displayParams']['cols'])) ? $this->_run_mod_handler('default', true, $_tmp, 60) : smarty_modifier_default($_tmp, 60)); ?>
"
                          tabindex="<?php echo $this->_tpl_vars['tabindex']; ?>
">{$fields.<?php echo $this->_tpl_vars['street']; ?>
.value}</textarea>
                <?php endif; ?>
                {/if}
            </td>
        </tr>

        <tr>

            <td id="<?php echo $this->_tpl_vars['city']; ?>
_label" width='<?php echo $this->_tpl_vars['def']['templateMeta']['widths'][($this->_foreach['colIteration']['iteration']-1)]['label']; ?>
%'
                scope='row'>
                <label for="<?php echo $this->_tpl_vars['city']; ?>
">{sugar_translate label='LBL_CITY' module='<?php echo $this->_tpl_vars['module']; ?>
'}:
                    {if $fields.<?php echo $this->_tpl_vars['city']; ?>
.required || <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['city'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)))) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['displayParams']['required']) : smarty_modifier_in_array($_tmp, $this->_tpl_vars['displayParams']['required']))): ?>true<?php else: ?>false<?php endif; ?>}
                    <span class="required">{$APP.LBL_REQUIRED_SYMBOL}</span>
                    {/if}
            </td>
            <td>
                {*Added By Biztech*}
                {if $fields.<?php echo $this->_tpl_vars['city']; ?>
.acl <= 1}
                    <label name="<?php echo $this->_tpl_vars['city']; ?>
" id="<?php echo $this->_tpl_vars['city']; ?>
" >{$fields.<?php echo $this->_tpl_vars['city']; ?>
.value}</label>
                {else}
                <input type="text" name="<?php echo $this->_tpl_vars['city']; ?>
" id="<?php echo $this->_tpl_vars['city']; ?>
" size="<?php echo ((is_array($_tmp=@$this->_tpl_vars['displayParams']['size'])) ? $this->_run_mod_handler('default', true, $_tmp, 30) : smarty_modifier_default($_tmp, 30)); ?>
"
                       <?php if (! empty ( $this->_tpl_vars['vardef']['len'] )): ?>maxlength='<?php echo $this->_tpl_vars['vardef']['len']; ?>
'<?php endif; ?> value='{$fields.<?php echo $this->_tpl_vars['city']; ?>
.value}'
                       tabindex="<?php echo $this->_tpl_vars['tabindex']; ?>
">
                {/if}
            </td>
        </tr>

        <tr>
            <td id="<?php echo $this->_tpl_vars['state']; ?>
_label" width='<?php echo $this->_tpl_vars['def']['templateMeta']['widths'][($this->_foreach['colIteration']['iteration']-1)]['label']; ?>
%'
                scope='row'>
                <label for="<?php echo $this->_tpl_vars['state']; ?>
">{sugar_translate label='LBL_STATE' module='<?php echo $this->_tpl_vars['module']; ?>
'}:</label>
                {if $fields.<?php echo $this->_tpl_vars['state']; ?>
.required || <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['state'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)))) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['displayParams']['required']) : smarty_modifier_in_array($_tmp, $this->_tpl_vars['displayParams']['required']))): ?>true<?php else: ?>false<?php endif; ?>}
                <span class="required">{$APP.LBL_REQUIRED_SYMBOL}</span>
                {/if}
            </td>
            <td>
                {*Added By Biztech*}
                {if $fields.<?php echo $this->_tpl_vars['state']; ?>
.acl <= 1}
                    <label name="<?php echo $this->_tpl_vars['state']; ?>
" id="<?php echo $this->_tpl_vars['state']; ?>
">{$fields.<?php echo $this->_tpl_vars['state']; ?>
.value}</label>
                {else}
                <input type="text" name="<?php echo $this->_tpl_vars['state']; ?>
" id="<?php echo $this->_tpl_vars['state']; ?>
" size="<?php echo ((is_array($_tmp=@$this->_tpl_vars['displayParams']['size'])) ? $this->_run_mod_handler('default', true, $_tmp, 30) : smarty_modifier_default($_tmp, 30)); ?>
"
                       <?php if (! empty ( $this->_tpl_vars['vardef']['len'] )): ?>maxlength='<?php echo $this->_tpl_vars['vardef']['len']; ?>
'<?php endif; ?> value='{$fields.<?php echo $this->_tpl_vars['state']; ?>
.value}'
                       tabindex="<?php echo $this->_tpl_vars['tabindex']; ?>
">
                {/if}
            </td>
        </tr>

        <tr>

            <td id="<?php echo $this->_tpl_vars['postalcode']; ?>
_label"
                width='<?php echo $this->_tpl_vars['def']['templateMeta']['widths'][($this->_foreach['colIteration']['iteration']-1)]['label']; ?>
%' scope='row'>

                <label for="<?php echo $this->_tpl_vars['postalcode']; ?>
">{sugar_translate label='LBL_POSTAL_CODE' module='<?php echo $this->_tpl_vars['module']; ?>
'}:</label>
                {if $fields.<?php echo $this->_tpl_vars['postalcode']; ?>
.required || <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['postalcode'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)))) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['displayParams']['required']) : smarty_modifier_in_array($_tmp, $this->_tpl_vars['displayParams']['required']))): ?>true<?php else: ?>false<?php endif; ?>}
                <span class="required">{$APP.LBL_REQUIRED_SYMBOL}</span>
                {/if}
            </td>
            <td>
                {*Added By Biztech*}
                {if $fields.<?php echo $this->_tpl_vars['postalcode']; ?>
.acl <= 1}
                    <label name="<?php echo $this->_tpl_vars['postalcode']; ?>
" id="<?php echo $this->_tpl_vars['postalcode']; ?>
">{$fields.<?php echo $this->_tpl_vars['postalcode']; ?>
.value}</label>
                {else}
                <input type="text" name="<?php echo $this->_tpl_vars['postalcode']; ?>
" id="<?php echo $this->_tpl_vars['postalcode']; ?>
" size="<?php echo ((is_array($_tmp=@$this->_tpl_vars['displayParams']['size'])) ? $this->_run_mod_handler('default', true, $_tmp, 30) : smarty_modifier_default($_tmp, 30)); ?>
"
                       <?php if (! empty ( $this->_tpl_vars['vardef']['len'] )): ?>maxlength='<?php echo $this->_tpl_vars['vardef']['len']; ?>
'<?php endif; ?>
                       value='{$fields.<?php echo $this->_tpl_vars['postalcode']; ?>
.value}' tabindex="<?php echo $this->_tpl_vars['tabindex']; ?>
">
                {/if}
            </td>
        </tr>

        <tr>

            <td id="<?php echo $this->_tpl_vars['country']; ?>
_label" width='<?php echo $this->_tpl_vars['def']['templateMeta']['widths'][($this->_foreach['colIteration']['iteration']-1)]['label']; ?>
%'
                scope='row'>

                <label for="<?php echo $this->_tpl_vars['country']; ?>
">{sugar_translate label='LBL_COUNTRY' module='<?php echo $this->_tpl_vars['module']; ?>
'}:</label>
                {if $fields.<?php echo $this->_tpl_vars['country']; ?>
.required || <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['country'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)))) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['displayParams']['required']) : smarty_modifier_in_array($_tmp, $this->_tpl_vars['displayParams']['required']))): ?>true<?php else: ?>false<?php endif; ?>}
                <span class="required">{$APP.LBL_REQUIRED_SYMBOL}</span>
                {/if}
            </td>
            <td>
                {*Added By Biztech*}
                {if $fields.<?php echo $this->_tpl_vars['country']; ?>
.acl <= 1}
                    <label name="<?php echo $this->_tpl_vars['country']; ?>
" id="<?php echo $this->_tpl_vars['country']; ?>
">{$fields.<?php echo $this->_tpl_vars['country']; ?>
.value}</label>
                {else}
                <input type="text" name="<?php echo $this->_tpl_vars['country']; ?>
" id="<?php echo $this->_tpl_vars['country']; ?>
" size="<?php echo ((is_array($_tmp=@$this->_tpl_vars['displayParams']['size'])) ? $this->_run_mod_handler('default', true, $_tmp, 30) : smarty_modifier_default($_tmp, 30)); ?>
"
                       <?php if (! empty ( $this->_tpl_vars['vardef']['len'] )): ?>maxlength='<?php echo $this->_tpl_vars['vardef']['len']; ?>
'<?php endif; ?> value='{$fields.<?php echo $this->_tpl_vars['country']; ?>
.value}'
                       tabindex="<?php echo $this->_tpl_vars['tabindex']; ?>
">
                {/if}
            </td>
        </tr>
        {*Added By Biztech*}
      
        <?php $this->assign('concatePrimaryAddr', ((is_array($_tmp=$this->_tpl_vars['displayParams']['copy'])) ? $this->_run_mod_handler('cat', true, $_tmp, '_address_street') : smarty_modifier_cat($_tmp, '_address_street'))); ?>
        {if $fields.<?php echo $this->_tpl_vars['concatePrimaryAddr']; ?>
.acl > 1}
            <?php if ($this->_tpl_vars['displayParams']['copy']): ?>
            <tr>
                <td scope='row' NOWRAP>
                    {sugar_translate label='LBL_COPY_ADDRESS_FROM_LEFT' module=''}:
                </td>
                <td>

                    <input id="<?php echo $this->_tpl_vars['displayParams']['key']; ?>
_checkbox" name="<?php echo $this->_tpl_vars['displayParams']['key']; ?>
_checkbox" type="checkbox"
                           onclick="<?php echo $this->_tpl_vars['displayParams']['key']; ?>
_address.syncFields();">

                </td>
            </tr>
            <?php else: ?>
            <tr>
                <td colspan='2' NOWRAP>&nbsp;</td>
            </tr>
            <?php endif; ?>
        {/if}
    </table>
</fieldset>
<script type="text/javascript">
  SUGAR.util.doWhen("typeof(SUGAR.AddressField) != 'undefined'", function () {ldelim}
      <?php echo $this->_tpl_vars['displayParams']['key']; ?>
_address = new SUGAR.AddressField("<?php echo $this->_tpl_vars['displayParams']['key']; ?>
_checkbox", '<?php echo $this->_tpl_vars['displayParams']['copy']; ?>
', '<?php echo $this->_tpl_vars['displayParams']['key']; ?>
');
      {rdelim});
</script>