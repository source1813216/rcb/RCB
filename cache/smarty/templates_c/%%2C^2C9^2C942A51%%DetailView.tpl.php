<?php /* Smarty version 2.6.31, created on 2022-11-29 17:52:04
         compiled from custom/include/DetailView/DetailView.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'sugar_include', 'custom/include/DetailView/DetailView.tpl', 24, false),array('function', 'counter', 'custom/include/DetailView/DetailView.tpl', 31, false),array('function', 'sugar_evalcolumn', 'custom/include/DetailView/DetailView.tpl', 466, false),array('function', 'sugar_field', 'custom/include/DetailView/DetailView.tpl', 471, false),array('modifier', 'upper', 'custom/include/DetailView/DetailView.tpl', 36, false),array('modifier', 'count', 'custom/include/DetailView/DetailView.tpl', 410, false),)), $this); ?>
{*
/**
 * The file used to handle layout actions
 *
 * LICENSE: The contents of this file are subject to the license agreement ("License") which is included
 * in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
 * agreed to the terms and conditions of the License, and you may not use this file except in compliance
 * with the License.
 *
 * @author     Biztech Consultancy
 */
*}
{php}
global $current_user,$sugar_config;
$this->_tpl_vars['current_user'] = $current_user->id;
$this->_tpl_vars['is_admin_user'] = $current_user->is_admin;
$this->_tpl_vars['current_theme'] = $current_user->getPreference('user_theme');
$this->_tpl_vars['suite_valid_version'] =  true;
if(preg_match('/(7\.7\.[0-9])/', $sugar_config['suitecrm_version']) || $sugar_config['suitecrm_version'] == '7.7'){
    $this->_tpl_vars['suite_valid_version'] = false;
}
{/php}
{if $current_theme == 'SuiteP'}
<?php echo smarty_function_sugar_include(array('type' => 'smarty','file' => $this->_tpl_vars['headerTpl']), $this);?>

{sugar_include include=$includes}
<div class="detail-view">
    {if $suite_valid_version}
    <div class="mobile-pagination">{$PAGINATION}</div>
    {/if}
    {*display tabs*}
    <?php echo smarty_function_counter(array('name' => 'tabCount','start' => -1,'print' => false,'assign' => 'tabCount'), $this);?>

    <ul class="nav nav-tabs">
        <?php if ($this->_tpl_vars['useTabs']): ?>
        <?php echo smarty_function_counter(array('name' => 'isection','start' => 0,'print' => false,'assign' => 'isection'), $this);?>

        <?php $_from = $this->_tpl_vars['sectionPanels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['section'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['section']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['label'] => $this->_tpl_vars['panel']):
        $this->_foreach['section']['iteration']++;
?>
        <?php ob_start(); ?><?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
<?php $this->_smarty_vars['capture']['label_upper'] = ob_get_contents();  $this->assign('label_upper', ob_get_contents());ob_end_clean(); ?>
        {* if tab *}
        <?php if (( isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] == true )): ?>
        {*if tab display*}
        <?php echo smarty_function_counter(array('name' => 'tabCount','print' => false), $this);?>

        <?php if ($this->_tpl_vars['tabCount'] == '0'): ?>
        <li role="presentation" class="active">
            <a id="tab<?php echo $this->_tpl_vars['tabCount']; ?>
" data-toggle="tab" class="hidden-xs">
                {sugar_translate label='<?php echo $this->_tpl_vars['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}
            </a>

            {* Count Tabs *}
            <?php echo smarty_function_counter(array('name' => 'tabCountOnlyXS','start' => -1,'print' => false,'assign' => 'tabCountOnlyXS'), $this);?>

            <?php $_from = $this->_tpl_vars['sectionPanels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['sectionOnlyXS'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['sectionOnlyXS']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['labelOnly'] => $this->_tpl_vars['panelOnlyXS']):
        $this->_foreach['sectionOnlyXS']['iteration']++;
?>
            <?php ob_start(); ?><?php echo ((is_array($_tmp=$this->_tpl_vars['labelOnly'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
<?php $this->_smarty_vars['capture']['label_upper_count_only'] = ob_get_contents();  $this->assign('label_upper_count_only', ob_get_contents());ob_end_clean(); ?>
            <?php if (( isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper_count_only']]['newTab'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper_count_only']]['newTab'] == true )): ?>
            <?php echo smarty_function_counter(array('name' => 'tabCountOnlyXS','print' => false), $this);?>

            <?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>

            {*
                For the mobile view, only show the first tab has a drop down when:
                * There is more than one tab set
                * When Acton Menu's are enabled
            *}
            <a id="xstab<?php echo $this->_tpl_vars['tabCount']; ?>
" href="#" class="visible-xs first-tab<?php if ($this->_tpl_vars['tabCountOnlyXS'] > 0): ?>-xs<?php endif; ?> dropdown-toggle" data-toggle="dropdown">
                {sugar_translate label='<?php echo $this->_tpl_vars['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}
            </a>
            <?php if ($this->_tpl_vars['tabCountOnlyXS'] > 0): ?>
            <ul id="first-tab-menu-xs" class="dropdown-menu">
                <?php echo smarty_function_counter(array('name' => 'tabCountXS','start' => 1,'print' => false,'assign' => 'tabCountXS'), $this);?>

                <?php $_from = $this->_tpl_vars['sectionPanels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['sectionXS'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['sectionXS']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['label'] => $this->_tpl_vars['panelXS']):
        $this->_foreach['sectionXS']['iteration']++;
?>
                <?php ob_start(); ?><?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
<?php $this->_smarty_vars['capture']['label_upper_xs'] = ob_get_contents();  $this->assign('label_upper_xs', ob_get_contents());ob_end_clean(); ?>
                <?php if (( isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper_xs']]['newTab'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper_xs']]['newTab'] == true )): ?>
                <li role="presentation">
                    <a id="tab<?php echo $this->_tpl_vars['tabCountXS']; ?>
" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-<?php echo $this->_tpl_vars['tabCountXS']; ?>
');">
                        {sugar_translate label='<?php echo $this->_tpl_vars['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}
                    </a>
                </li>
                <?php endif; ?>
                <?php echo smarty_function_counter(array('name' => 'tabCountXS','print' => false), $this);?>

                <?php endforeach; endif; unset($_from); ?>
            </ul>
            <?php endif; ?>

        </li>
        <?php else: ?>
        <li role="presentation" class="hidden-xs">
            <a id="tab<?php echo $this->_tpl_vars['tabCount']; ?>
" data-toggle="tab">
                {sugar_translate label='<?php echo $this->_tpl_vars['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}
            </a>
        </li>
        <?php endif; ?>
        <?php else: ?>
        {* if panel skip*}
        <?php endif; ?>
        <?php endforeach; endif; unset($_from); ?>
        <?php else: ?>
        {*
            Since: SuieCRM 7.8
            When action menus are enabled and When there are only panels and there are not any tabs,
            make the first panel a tab so that the action menu looks correct. This is regardless of what the
            meta/studio defines the first panel should always be tab.
        *}
        {if $config.enable_action_menu and $config.enable_action_menu != false}
            <?php $_from = $this->_tpl_vars['sectionPanels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['section'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['section']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['label'] => $this->_tpl_vars['panel']):
        $this->_foreach['section']['iteration']++;
?>
            <?php ob_start(); ?><?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
<?php $this->_smarty_vars['capture']['label_upper'] = ob_get_contents();  $this->assign('label_upper', ob_get_contents());ob_end_clean(); ?>
            <?php echo smarty_function_counter(array('name' => 'tabCount','print' => false), $this);?>

            <?php if ($this->_tpl_vars['tabCount'] == '0'): ?>
            <li role="presentation" class="active">
                <a id="tab<?php echo $this->_tpl_vars['tabCount']; ?>
" data-toggle="tab" class="hidden-xs">
                    {sugar_translate label='<?php echo $this->_tpl_vars['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}
                </a>
                <a id="xstab<?php echo $this->_tpl_vars['tabCount']; ?>
" href="#" class="visible-xs first-tab dropdown-toggle" data-toggle="dropdown">
                    {sugar_translate label='<?php echo $this->_tpl_vars['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}
                </a>
            </li>
            <?php else: ?>
            {* if panel skip *}
            <?php endif; ?>
        <?php endforeach; endif; unset($_from); ?>
        {/if}
        <?php endif; ?>
        {if $config.enable_action_menu and $config.enable_action_menu != false}
        <li id="tab-actions" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $this->_tpl_vars['APP']['LBL_LINK_ACTIONS']; ?>
<span class="suitepicon suitepicon-action-caret"></span></a>
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "themes/SuiteP/include/DetailView/actions_menu.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </li>
        {if $suite_valid_version}
        <li class="tab-inline-pagination">
            <?php if ($this->_tpl_vars['panelCount'] == 0): ?>
                        <?php if ($this->_tpl_vars['SHOW_VCR_CONTROL']): ?>
            {$PAGINATION}
            <?php endif; ?>
            <?php echo smarty_function_counter(array('name' => 'panelCount','print' => false), $this);?>

            <?php endif; ?>
        </li>
        {/if}
        {/if}
    </ul>
    <?php echo smarty_function_counter(array('name' => 'tabCount','start' => 0,'print' => false,'assign' => 'tabCount'), $this);?>

    <div class="clearfix"></div>
    <?php if ($this->_tpl_vars['useTabs']): ?>
    {*<!-- TAB CONTENT USE TABS -->*}
    <div class="tab-content">
        <?php else: ?>
        {*
           Since: SuieCRM 7.8
           When action menus are enabled and When there are only panels and there are not any tabs,
           make the first panel a tab so that the action menu looks correct. This is regardless of what the
           meta/studio defines the first panel should always be tab.
       *}
        {if $config.enable_action_menu and $config.enable_action_menu != false}
        <?php if (tabCount == 0): ?>
        {*<!-- TAB CONTENT USE TABS -->*}
        <div class="tab-content">
            <?php else: ?>
            {*<!-- TAB CONTENT DOESN'T USE TABS -->*}
            <div class="tab-content" style="padding: 0; border: 0;">
                <?php endif; ?>
                {else}
                {*<!-- TAB CONTENT DOESN'T USE TABS -->*}
                <div class="tab-content" style="padding: 0; border: 0;">
                    {/if}
                    <?php endif; ?>
                    {* Loop through all top level panels first *}
                    <?php if ($this->_tpl_vars['useTabs']): ?>
                    <?php $_from = $this->_tpl_vars['sectionPanels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['section'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['section']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['label'] => $this->_tpl_vars['panel']):
        $this->_foreach['section']['iteration']++;
?>
                    <?php ob_start(); ?><?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
<?php $this->_smarty_vars['capture']['label_upper'] = ob_get_contents();  $this->assign('label_upper', ob_get_contents());ob_end_clean(); ?>
                    <?php if (isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] == true): ?>
                    <?php if ($this->_tpl_vars['tabCount'] == '0'): ?>
                    <div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-<?php echo $this->_tpl_vars['tabCount']; ?>
'>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'custom/themes/tab_panel/tab_panel_content_detail.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
                    <?php else: ?>
                    <div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-<?php echo $this->_tpl_vars['tabCount']; ?>
'>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'custom/themes/tab_panel/tab_panel_content_detail.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php echo smarty_function_counter(array('name' => 'tabCount','print' => false), $this);?>

                    <?php endforeach; endif; unset($_from); ?>
                    <?php else: ?>
                    {*
                       Since: SuieCRM 7.8
                       When action menus are enabled and When there are only panels and there are not any tabs,
                       make the first panel a tab so that the action menu looks correct. This is regardless of what the
                       meta/studio defines the first panel should always be tab.
                   *}
                    {if $config.enable_action_menu and $config.enable_action_menu != false}
                        <?php $_from = $this->_tpl_vars['sectionPanels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['section'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['section']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['label'] => $this->_tpl_vars['panel']):
        $this->_foreach['section']['iteration']++;
?>
                        <?php ob_start(); ?><?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
<?php $this->_smarty_vars['capture']['label_upper'] = ob_get_contents();  $this->assign('label_upper', ob_get_contents());ob_end_clean(); ?>
                        <?php if ($this->_tpl_vars['tabCount'] == '0'): ?>
                        <div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-<?php echo $this->_tpl_vars['tabCount']; ?>
'>
                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'custom/themes/tab_panel/tab_panel_content_detail.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        </div>
                        <?php else: ?>

                        <?php endif; ?>
                    <?php echo smarty_function_counter(array('name' => 'tabCount','print' => false), $this);?>

                    <?php endforeach; endif; unset($_from); ?>
                    {else}
                    {*<!-- TAB CONTENT DOESN'T USE TABS -->*}
                    <div class="tab-pane-NOBOOTSTRAPTOGGLER panel-collapse"></div>
                    {/if}
                    <?php endif; ?>
                </div>
                {*display panels*}
                <div class="panel-content">
                    <div>&nbsp;</div>
                    <?php echo smarty_function_counter(array('name' => 'panelCount','start' => -1,'print' => false,'assign' => 'panelCount'), $this);?>

                    <?php $_from = $this->_tpl_vars['sectionPanels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['section'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['section']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['label'] => $this->_tpl_vars['panel']):
        $this->_foreach['section']['iteration']++;
?>

                    <?php ob_start(); ?><?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
<?php $this->_smarty_vars['capture']['label_upper'] = ob_get_contents();  $this->assign('label_upper', ob_get_contents());ob_end_clean(); ?>
                    {* if tab *}
                    <?php if (( isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] == true && $this->_tpl_vars['useTabs'] )): ?>
                    {*if tab skip*}
                    <?php else: ?>
                    {* if panel display*}
                    {*if panel collasped*}
                    <?php if (( isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['panelDefault'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['panelDefault'] == 'collapsed' )): ?>
                    {*collapse panel*}
                    <?php $this->assign('collapse', "panel-collapse collapse"); ?>
                    <?php $this->assign('collapsed', 'collapsed'); ?>
                    <?php $this->assign('collapseIcon', "glyphicon glyphicon-plus"); ?>
                    <?php $this->assign('panelHeadingCollapse', "panel-heading-collapse"); ?>
                    <?php else: ?>
                    {*expand panel*}
                    <?php $this->assign('collapse', "panel-collapse collapse in"); ?>
                    <?php $this->assign('collapseIcon', "glyphicon glyphicon-minus"); ?>
                    <?php $this->assign('panelHeadingCollapse', ""); ?>
                    <?php endif; ?>
                    <?php if ($this->_tpl_vars['label'] != 'LBL_AOP_CASE_UPDATES'): ?>
                    <?php $this->assign('panelId', "top-panel-".($this->_tpl_vars['panelCount'])); ?>
                    <?php else: ?>
                    <?php $this->assign('panelId', 'LBL_AOP_CASE_UPDATES'); ?>
                    <?php endif; ?>

                    {*
                       Since: SuieCRM 7.8
                       When action menus are enabled and When there are only panels and there are not any tabs,
                       make the first panel a tab so that the action menu looks correct. This is regardless of what the
                       meta/studio defines the first panel should always be tab.
                    *}
                    {if $config.enable_action_menu and $config.enable_action_menu != false}
                        <?php if ($this->_tpl_vars['panelCount'] == -1): ?>
                        {* skip panel as it has been converted to a tab*}
                        <?php else: ?>
                        {* display panels as they have always been displayed *}
                        <div class="panel panel-default">
                            <div class="panel-heading <?php echo $this->_tpl_vars['panelHeadingCollapse']; ?>
">
                                <a class="<?php echo $this->_tpl_vars['collapsed']; ?>
" role="button" data-toggle="collapse" href="#<?php echo $this->_tpl_vars['panelId']; ?>
" aria-expanded="false">
                                    <div class="col-xs-10 col-sm-11 col-md-11">
                                        {sugar_translate label='<?php echo $this->_tpl_vars['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}
                                    </div>
                                </a>
                            </div>
                            <div class="panel-body <?php echo $this->_tpl_vars['collapse']; ?>
 panelContainer" id="<?php echo $this->_tpl_vars['panelId']; ?>
"  data-id="<?php echo $this->_tpl_vars['label_upper']; ?>
">
                                <div class="tab-content">
                                    <!-- TAB CONTENT -->
                                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'custom/themes/tab_panel/tab_panel_content_detail.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    {else}
                    {* display panels as they have always been displayed *}
                    <div class="panel panel-default">
                        <div class="panel-heading <?php echo $this->_tpl_vars['panelHeadingCollapse']; ?>
">
                            <a class="<?php echo $this->_tpl_vars['collapsed']; ?>
" role="button" data-toggle="collapse" href="#<?php echo $this->_tpl_vars['panelId']; ?>
" aria-expanded="false">
                                <div class="col-xs-10 col-sm-11 col-md-11">
                                    {sugar_translate label='<?php echo $this->_tpl_vars['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}
                                </div>
                            </a>

                        </div>
                        <div class="panel-body <?php echo $this->_tpl_vars['collapse']; ?>
 panelContainer" id="<?php echo $this->_tpl_vars['panelId']; ?>
" data-id="<?php echo $this->_tpl_vars['label_upper']; ?>
">
                            <div class="tab-content">
                                <!-- TAB CONTENT -->
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'custom/themes/tab_panel/tab_panel_content_detail.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            </div>
                        </div>
                    </div>
                    {/if}


                    <?php endif; ?>
                    <?php echo smarty_function_counter(array('name' => 'panelCount','print' => false), $this);?>

                    <?php endforeach; endif; unset($_from); ?>
                </div>
            </div>

            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['footerTpl'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
            <script type="text/javascript" src="modules/Favorites/favorites.js"></script>

            {literal}

                <script type="text/javascript">

                    var selectTabDetailView = function(tab) {
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').hide();
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').eq(tab).show().addClass('active').addClass('in');
                    };

                    var selectTabOnError = function(tab) {
                        selectTabDetailView(tab);
                        $('#content ul.nav.nav-tabs > li').removeClass('active');
                        $('#content ul.nav.nav-tabs > li a').css('color', '');

                        $('#content ul.nav.nav-tabs > li').eq(tab).find('a').first().css('color', 'red');
                        $('#content ul.nav.nav-tabs > li').eq(tab).addClass('active');

                    };

                    var selectTabOnErrorInputHandle = function(inputHandle) {
                        var tab = $(inputHandle).closest('.tab-pane-NOBOOTSTRAPTOGGLER').attr('id').match(/^detailpanel_(.*)$/)[1];
                        selectTabOnError(tab);
                    };


                    $(function(){
                        $('#content ul.nav.nav-tabs > li > a[data-toggle="tab"]').click(function(e){
                            if(typeof $(this).parent().find('a').first().attr('id') != 'undefined') {
                                var tab = parseInt($(this).parent().find('a').first().attr('id').match(/^tab(.)*$/)[1]);
                                selectTabDetailView(tab);
                            }
                        });
                    });

                </script>

            {/literal}
{else}
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['headerTpl'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
{sugar_include include=$includes}
<div id="<?php echo $this->_tpl_vars['module']; ?>
_detailview_tabs"
<?php if ($this->_tpl_vars['useTabs']): ?>
class="yui-navset detailview_tabs"
<?php endif; ?>
>
    <?php if ($this->_tpl_vars['useTabs']): ?>
    {* Generate the Tab headers *}
    <?php echo smarty_function_counter(array('name' => 'tabCount','start' => -1,'print' => false,'assign' => 'tabCount'), $this);?>

    <ul class="yui-nav">
    <?php $_from = $this->_tpl_vars['sectionPanels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['section'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['section']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['label'] => $this->_tpl_vars['panel']):
        $this->_foreach['section']['iteration']++;
?>
        <?php ob_start(); ?><?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
<?php $this->_smarty_vars['capture']['label_upper'] = ob_get_contents();  $this->assign('label_upper', ob_get_contents());ob_end_clean(); ?>
        {* override from tab definitions *}
        <?php if (( isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] == true )): ?>
            <?php echo smarty_function_counter(array('name' => 'tabCount','print' => false), $this);?>

            <li><a id="tab<?php echo $this->_tpl_vars['tabCount']; ?>
" href="javascript:void(0)"><em>{sugar_translate label='<?php echo $this->_tpl_vars['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}</em></a></li>
        <?php endif; ?>
    <?php endforeach; endif; unset($_from); ?>
    </ul>
    <?php endif; ?>
    <div <?php if ($this->_tpl_vars['useTabs']): ?>class="yui-content"<?php endif; ?>>
<?php echo smarty_function_counter(array('name' => 'panelCount','print' => false,'start' => 0,'assign' => 'panelCount'), $this);?>

<?php echo smarty_function_counter(array('name' => 'tabCount','start' => -1,'print' => false,'assign' => 'tabCount'), $this);?>

<?php $_from = $this->_tpl_vars['sectionPanels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['section'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['section']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['label'] => $this->_tpl_vars['panel']):
        $this->_foreach['section']['iteration']++;
?>
<?php $this->assign('panel_id', $this->_tpl_vars['panelCount']); ?>
<?php ob_start(); ?><?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
<?php $this->_smarty_vars['capture']['label_upper'] = ob_get_contents();  $this->assign('label_upper', ob_get_contents());ob_end_clean(); ?>
  <?php if (( isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] == true )): ?>
    <?php echo smarty_function_counter(array('name' => 'tabCount','print' => false), $this);?>

    <?php if ($this->_tpl_vars['tabCount'] != 0): ?></div><?php endif; ?>
    <div id='tabcontent<?php echo $this->_tpl_vars['tabCount']; ?>
'>
  <?php endif; ?>

    <?php if (( isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['panelDefault'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['panelDefault'] == 'collapsed' && isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] == false )): ?>
        <?php $this->assign('panelState', $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['panelDefault']); ?>
    <?php else: ?>
        <?php $this->assign('panelState', 'expanded'); ?>
    <?php endif; ?>
<div id='detailpanel_<?php echo $this->_foreach['section']['iteration']; ?>
' class='detail view  detail508 <?php echo $this->_tpl_vars['panelState']; ?>
'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}

<?php if (! is_array ( $this->_tpl_vars['panel'] )): ?>
    {sugar_include type='php' file='<?php echo $this->_tpl_vars['panel']; ?>
'}
<?php else: ?>

    <?php if (! empty ( $this->_tpl_vars['label'] ) && ! is_int ( $this->_tpl_vars['label'] ) && $this->_tpl_vars['label'] != 'DEFAULT' && ( ! isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] ) || ( isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] == false ) )): ?>
    <h4>
      <a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(<?php echo $this->_foreach['section']['iteration']; ?>
);">
      <img border="0" id="detailpanel_<?php echo $this->_foreach['section']['iteration']; ?>
_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
      <a href="javascript:void(0)" class="expandLink" onclick="expandPanel(<?php echo $this->_foreach['section']['iteration']; ?>
);">
      <img border="0" id="detailpanel_<?php echo $this->_foreach['section']['iteration']; ?>
_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
      {sugar_translate label='<?php echo $this->_tpl_vars['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}
    <?php if (isset ( $this->_tpl_vars['panelState'] ) && $this->_tpl_vars['panelState'] == 'collapsed'): ?>
    <script>
      document.getElementById('detailpanel_<?php echo $this->_foreach['section']['iteration']; ?>
').className += ' collapsed';
    </script>
    <?php else: ?>
    <script>
      document.getElementById('detailpanel_<?php echo $this->_foreach['section']['iteration']; ?>
').className += ' expanded';
    </script>
    <?php endif; ?>
    </h4>

    <?php endif; ?>
		<!-- PANEL CONTAINER HERE.. -->
  <table id='<?php echo $this->_tpl_vars['label']; ?>
' class="panelContainer" cellspacing='{$gridline}'>



	<?php $_from = $this->_tpl_vars['panel']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['rowIteration'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['rowIteration']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['row'] => $this->_tpl_vars['rowData']):
        $this->_foreach['rowIteration']['iteration']++;
?>
	{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
	{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
	{capture name="tr" assign="tableRow"}
	<tr>
		<?php $this->assign('columnsInRow', count($this->_tpl_vars['rowData'])); ?>
		<?php $this->assign('columnsUsed', 0); ?>
		<?php $_from = $this->_tpl_vars['rowData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['colIteration'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['colIteration']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['col'] => $this->_tpl_vars['colData']):
        $this->_foreach['colIteration']['iteration']++;
?>
	    <?php if (! empty ( $this->_tpl_vars['colData']['field']['hideIf'] )): ?>
	    	{if !(<?php echo $this->_tpl_vars['colData']['field']['hideIf']; ?>
) }
	    <?php endif; ?>
               {*Added By Biztech*}
               <?php if (! empty ( $this->_tpl_vars['colData']['field']['name'] )): ?>
                    {if $fields.<?php echo $this->_tpl_vars['colData']['field']['name']; ?>
.acl > 1 || $fields.<?php echo $this->_tpl_vars['colData']['field']['name']; ?>
.acl > 0}
                <?php endif; ?>
                {*Added By Biztech*}
			{counter name="fieldsUsed"}
			<?php if (empty ( $this->_tpl_vars['colData']['field']['hideLabel'] )): ?>
			<td width='<?php echo $this->_tpl_vars['def']['templateMeta']['widths'][($this->_foreach['colIteration']['iteration']-1)]['label']; ?>
%' scope="col">
				<?php if (! empty ( $this->_tpl_vars['colData']['field']['name'] )): ?>
				    {if !$fields.<?php echo $this->_tpl_vars['colData']['field']['name']; ?>
.hidden}
                <?php endif; ?>
				<?php if (isset ( $this->_tpl_vars['colData']['field']['customLabel'] )): ?>
			       <?php echo $this->_tpl_vars['colData']['field']['customLabel']; ?>

				<?php elseif (isset ( $this->_tpl_vars['colData']['field']['label'] ) && strpos ( $this->_tpl_vars['colData']['field']['label'] , '$' )): ?>
				   {capture name="label" assign="label"}<?php echo $this->_tpl_vars['colData']['field']['label']; ?>
{/capture}
			       {$label|strip_semicolon}:
				<?php elseif (isset ( $this->_tpl_vars['colData']['field']['label'] )): ?>
				   {capture name="label" assign="label"}{sugar_translate label='<?php echo $this->_tpl_vars['colData']['field']['label']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}{/capture}
			       {$label|strip_semicolon}:
				<?php elseif (isset ( $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']] )): ?>
				   {capture name="label" assign="label"}{sugar_translate label='<?php echo $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['vname']; ?>
' module='<?php echo $this->_tpl_vars['module']; ?>
'}{/capture}
			       {$label|strip_semicolon}:
				<?php else: ?>
				   &nbsp;
				<?php endif; ?>
                <?php if (isset ( $this->_tpl_vars['colData']['field']['popupHelp'] ) || isset ( $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']] ) && isset ( $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['popupHelp'] )): ?>
                   <?php if (isset ( $this->_tpl_vars['colData']['field']['popupHelp'] )): ?>
                     {capture name="popupText" assign="popupText"}{sugar_translate label="<?php echo $this->_tpl_vars['colData']['field']['popupHelp']; ?>
" module='<?php echo $this->_tpl_vars['module']; ?>
'}{/capture}
                   <?php elseif (isset ( $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['popupHelp'] )): ?>
                     {capture name="popupText" assign="popupText"}{sugar_translate label="<?php echo $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['popupHelp']; ?>
" module='<?php echo $this->_tpl_vars['module']; ?>
'}{/capture}
                   <?php endif; ?>
                   {sugar_help text=$popupText WIDTH=400}
                <?php endif; ?>
                {*Added By Biztech*}
                <?php if (! empty ( $this->_tpl_vars['colData']['field']['name'] )): ?>
                            {else}
                                {counter name="fieldsHidden"}
                {*Added By Biztech*}
                {/if}
                <?php endif; ?>
                <?php endif; ?>
			</td>
                        {*Added By Biztech*}
			<td class="<?php if ($this->_tpl_vars['inline_edit'] && $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['acl'] > 1 && ! empty ( $this->_tpl_vars['colData']['field']['name'] ) && ( $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['inline_edit'] == 1 || ! isset ( $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['inline_edit'] ) )): ?>inlineEdit<?php endif; ?>" type="<?php echo $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['type']; ?>
" field="<?php echo $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['name']; ?>
" width='<?php echo $this->_tpl_vars['def']['templateMeta']['widths'][($this->_foreach['colIteration']['iteration']-1)]['field']; ?>
%' <?php if ($this->_tpl_vars['colData']['colspan']): ?>colspan='<?php echo $this->_tpl_vars['colData']['colspan']; ?>
'<?php endif; ?> <?php if (isset ( $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['type'] ) && $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['type'] == 'phone'): ?>class="phone"<?php endif; ?>>
			    <?php if (! empty ( $this->_tpl_vars['colData']['field']['name'] )): ?>
			    {if !$fields.<?php echo $this->_tpl_vars['colData']['field']['name']; ?>
.hidden}
			    <?php endif; ?>
				<?php echo $this->_tpl_vars['colData']['field']['prefix']; ?>

				<?php if (( $this->_tpl_vars['colData']['field']['customCode'] && ! $this->_tpl_vars['colData']['field']['customCodeRenderField'] ) || $this->_tpl_vars['colData']['field']['assign']): ?>
					{counter name="panelFieldCount"}
					<span id="<?php echo $this->_tpl_vars['colData']['field']['name']; ?>
" class="sugar_field"><?php echo smarty_function_sugar_evalcolumn(array('var' => $this->_tpl_vars['colData']['field'],'colData' => $this->_tpl_vars['colData']), $this);?>
</span>
				<?php elseif ($this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']] && ! empty ( $this->_tpl_vars['colData']['field']['fields'] )): ?>
				    <?php $_from = $this->_tpl_vars['colData']['field']['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['subField']):
?>
				        <?php if ($this->_tpl_vars['fields'][$this->_tpl_vars['subField']]): ?>
				        	{counter name="panelFieldCount"}
				            <?php echo smarty_function_sugar_field(array('parentFieldArray' => 'fields','tabindex' => $this->_tpl_vars['tabIndex'],'vardef' => $this->_tpl_vars['fields'][$this->_tpl_vars['subField']],'displayType' => 'DetailView'), $this);?>
&nbsp;

				        <?php else: ?>
				        	{counter name="panelFieldCount"}
				            <?php echo $this->_tpl_vars['subField']; ?>

				        <?php endif; ?>
				    <?php endforeach; endif; unset($_from); ?>
				<?php elseif ($this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]): ?>
					{counter name="panelFieldCount"}
					<?php echo smarty_function_sugar_field(array('parentFieldArray' => 'fields','vardef' => $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']],'displayType' => 'DetailView','displayParams' => $this->_tpl_vars['colData']['field']['displayParams'],'typeOverride' => $this->_tpl_vars['colData']['field']['type']), $this);?>


				<?php endif; ?>
				<?php if (! empty ( $this->_tpl_vars['colData']['field']['customCode'] ) && $this->_tpl_vars['colData']['field']['customCodeRenderField']): ?>
				    {counter name="panelFieldCount"}
				    <span id="<?php echo $this->_tpl_vars['colData']['field']['name']; ?>
" class="sugar_field"><?php echo smarty_function_sugar_evalcolumn(array('var' => $this->_tpl_vars['colData']['field'],'colData' => $this->_tpl_vars['colData']), $this);?>
</span>
                <?php endif; ?>
				<?php echo $this->_tpl_vars['colData']['field']['suffix']; ?>

				<?php if (! empty ( $this->_tpl_vars['colData']['field']['name'] )): ?>
				{/if}
				<?php endif; ?>
                                {*Added By Biztech*}
				<?php if ($this->_tpl_vars['inline_edit'] && $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['acl'] > 1 && ! empty ( $this->_tpl_vars['colData']['field']['name'] ) && ( $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['inline_edit'] == 1 || ! isset ( $this->_tpl_vars['fields'][$this->_tpl_vars['colData']['field']['name']]['inline_edit'] ) )): ?><div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div><?php endif; ?>
			</td>
                                {*Added By Biztech*}
                                <?php if (! empty ( $this->_tpl_vars['colData']['field']['name'] )): ?>
                                {else}

                                    <td>&nbsp;</td><td>&nbsp;</td>
                                    {/if}
	<?php endif; ?>
            {*Added By Biztech*}
	    <?php if (! empty ( $this->_tpl_vars['colData']['field']['hideIf'] )): ?>
			{else}

			<td>&nbsp;</td><td>&nbsp;</td>
			{/if}
	    <?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
	</tr>
	{/capture}
	{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
	{$tableRow}
	{/if}
	<?php endforeach; endif; unset($_from); ?>
	</table>
    <?php if (! empty ( $this->_tpl_vars['label'] ) && ! is_int ( $this->_tpl_vars['label'] ) && $this->_tpl_vars['label'] != 'DEFAULT' && ( ! isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] ) || ( isset ( $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] ) && $this->_tpl_vars['tabDefs'][$this->_tpl_vars['label_upper']]['newTab'] == false ) )): ?>
    <script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(<?php echo $this->_foreach['section']['iteration']; ?>
, '<?php echo $this->_tpl_vars['panelState']; ?>
'); {rdelim}); </script>
    <?php endif; ?>
<?php endif; ?>
</div>
{if $panelFieldCount == 0}

<script>document.getElementById("<?php echo $this->_tpl_vars['label']; ?>
").style.display='none';</script>
{/if}
<?php endforeach; endif; unset($_from); ?>
<?php if ($this->_tpl_vars['useTabs']): ?>
  </div>
<?php endif; ?>

</div>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['footerTpl'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php if ($this->_tpl_vars['useTabs']): ?>
<script type='text/javascript' src='{sugar_getjspath file='include/javascript/popup_helper.js'}'></script>
<script type="text/javascript" src="{sugar_getjspath file='cache/include/javascript/sugar_grp_yui_widgets.js'}"></script>
<script type="text/javascript">
var <?php echo $this->_tpl_vars['module']; ?>
_detailview_tabs = new YAHOO.widget.TabView("<?php echo $this->_tpl_vars['module']; ?>
_detailview_tabs");
<?php echo $this->_tpl_vars['module']; ?>
_detailview_tabs.selectTab(0);
</script>
<?php endif; ?>
<script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
{/if}