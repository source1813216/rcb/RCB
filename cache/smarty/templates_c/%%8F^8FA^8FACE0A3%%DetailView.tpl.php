<?php /* Smarty version 2.6.31, created on 2022-12-26 08:31:14
         compiled from cache/themes/SuiteP/modules/EDP_Endorsements/DetailView.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'sugar_include', 'cache/themes/SuiteP/modules/EDP_Endorsements/DetailView.tpl', 51, false),array('function', 'sugar_translate', 'cache/themes/SuiteP/modules/EDP_Endorsements/DetailView.tpl', 62, false),array('function', 'counter', 'cache/themes/SuiteP/modules/EDP_Endorsements/DetailView.tpl', 147, false),array('function', 'sugar_ajax_url', 'cache/themes/SuiteP/modules/EDP_Endorsements/DetailView.tpl', 468, false),array('function', 'sugar_getimage', 'cache/themes/SuiteP/modules/EDP_Endorsements/DetailView.tpl', 940, false),array('function', 'sugar_getjspath', 'cache/themes/SuiteP/modules/EDP_Endorsements/DetailView.tpl', 1622, false),array('modifier', 'strip_semicolon', 'cache/themes/SuiteP/modules/EDP_Endorsements/DetailView.tpl', 140, false),)), $this); ?>

<?php 
global $current_user,$sugar_config;
$this->_tpl_vars['current_user'] = $current_user->id;
$this->_tpl_vars['is_admin_user'] = $current_user->is_admin;
$this->_tpl_vars['current_theme'] = $current_user->getPreference('user_theme');
$this->_tpl_vars['suite_valid_version'] =  true;
if(preg_match('/(7\.7\.[0-9])/', $sugar_config['suitecrm_version']) || $sugar_config['suitecrm_version'] == '7.7'){
$this->_tpl_vars['suite_valid_version'] = false;
}
 ?>
<?php if ($this->_tpl_vars['current_theme'] == 'SuiteP'): ?>

<script language="javascript">
    <?php echo '
    SUGAR.util.doWhen(function () {
        return $("#contentTable").length == 0;
    }, SUGAR.themes.actionMenu);
    '; ?>

</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="<?php echo $this->_tpl_vars['module']; ?>
">
<input type="hidden" name="record" value="<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="<?php echo $this->_tpl_vars['offset']; ?>
">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
<?php if (! $this->_tpl_vars['config']['enable_action_menu']): ?>
<div class="buttons">
<?php if ($this->_tpl_vars['bean']->aclAccess('edit')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_KEY']; ?>
" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='EDP_Endorsements'; _form.return_action.value='DetailView'; _form.return_id.value='<?php echo $this->_tpl_vars['id']; ?>
'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_LABEL']; ?>
"><?php endif; ?> 
<?php if ($this->_tpl_vars['bean']->aclAccess('delete')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_KEY']; ?>
" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='EDP_Endorsements'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('<?php echo $this->_tpl_vars['APP']['NTC_DELETE_CONFIRMATION']; ?>
')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_LABEL']; ?>
" id="delete_button"><?php endif; ?> 
<input type="button" class="button" onClick="showPopup('pdf');" value="<?php echo $this->_tpl_vars['MOD']['LBL_PRINT_AS_PDF']; ?>
"/>
<input type="button" class="button" onClick="showPopup('emailpdf');" value="<?php echo $this->_tpl_vars['MOD']['LBL_EMAIL_PDF']; ?>
"/>
<?php if ($this->_tpl_vars['bean']->aclAccess('detail')): ?><?php if (! empty ( $this->_tpl_vars['fields']['id']['value'] ) && $this->_tpl_vars['isAuditEnabled']): ?><input id="btn_view_change_log" title="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
" class="button" onclick='open_popup("Audit", "600", "400", "&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
&module_name=EDP_Endorsements", true, false,  { "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] } ); return false;' type="button" value="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
"><?php endif; ?><?php endif; ?>
</div>                    <?php endif; ?>
</form>
</div>
</td>
<td align="right" width="20%" class="buttons"><?php echo $this->_tpl_vars['ADMIN_EDIT']; ?>

</td>
</tr>
</table>
<?php echo smarty_function_sugar_include(array('include' => $this->_tpl_vars['includes']), $this);?>

<div class="detail-view">
<?php if ($this->_tpl_vars['suite_valid_version']): ?>
<div class="mobile-pagination"><?php echo $this->_tpl_vars['PAGINATION']; ?>
</div>
<?php endif; ?>

<ul class="nav nav-tabs">


<li role="presentation" class="active">
<a id="tab0" data-toggle="tab" class="hidden-xs">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL2','module' => 'EDP_Endorsements'), $this);?>

</a>


<a id="xstab0" href="#" class="visible-xs first-tab-xs dropdown-toggle" data-toggle="dropdown">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL2','module' => 'EDP_Endorsements'), $this);?>

</a>
<ul id="first-tab-menu-xs" class="dropdown-menu">
<li role="presentation">
<a id="tab1" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-1');">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL2','module' => 'EDP_Endorsements'), $this);?>

</a>
</li>
<li role="presentation">
<a id="tab2" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-2');">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL1','module' => 'EDP_Endorsements'), $this);?>

</a>
</li>
<li role="presentation">
<a id="tab3" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-3');">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL3','module' => 'EDP_Endorsements'), $this);?>

</a>
</li>
</ul>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab1" data-toggle="tab">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL1','module' => 'EDP_Endorsements'), $this);?>

</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab2" data-toggle="tab">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL3','module' => 'EDP_Endorsements'), $this);?>

</a>
</li>
<?php if ($this->_tpl_vars['config']['enable_action_menu'] && $this->_tpl_vars['config']['enable_action_menu'] != false): ?>
<li id="tab-actions" class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">ACTIONS<span class="suitepicon suitepicon-action-caret"></span></a>
<ul class="dropdown-menu">
<li><?php if ($this->_tpl_vars['bean']->aclAccess('edit')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_KEY']; ?>
" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='EDP_Endorsements'; _form.return_action.value='DetailView'; _form.return_id.value='<?php echo $this->_tpl_vars['id']; ?>
'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_LABEL']; ?>
"><?php endif; ?> </li>
<li><?php if ($this->_tpl_vars['bean']->aclAccess('delete')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_KEY']; ?>
" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='EDP_Endorsements'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('<?php echo $this->_tpl_vars['APP']['NTC_DELETE_CONFIRMATION']; ?>
')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_LABEL']; ?>
" id="delete_button"><?php endif; ?> </li>
<li><input type="button" class="button" onClick="showPopup('pdf');" value="<?php echo $this->_tpl_vars['MOD']['LBL_PRINT_AS_PDF']; ?>
"/></li>
<li><input type="button" class="button" onClick="showPopup('emailpdf');" value="<?php echo $this->_tpl_vars['MOD']['LBL_EMAIL_PDF']; ?>
"/></li>
<li><?php if ($this->_tpl_vars['bean']->aclAccess('detail')): ?><?php if (! empty ( $this->_tpl_vars['fields']['id']['value'] ) && $this->_tpl_vars['isAuditEnabled']): ?><input id="btn_view_change_log" title="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
" class="button" onclick='open_popup("Audit", "600", "400", "&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
&module_name=EDP_Endorsements", true, false,  { "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] } ); return false;' type="button" value="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
"><?php endif; ?><?php endif; ?></li>
</ul>        </li>
<?php if ($this->_tpl_vars['suite_valid_version']): ?>
<li class="tab-inline-pagination">
<?php echo $this->_tpl_vars['PAGINATION']; ?>

</li>
<?php endif; ?>
<?php endif; ?>
</ul>
<div class="clearfix"></div>

<div class="tab-content">

<div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-0'>





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['approval_status_c']['acl'] > 1 || $this->_tpl_vars['fields']['approval_status_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_APPROVAL_STATUS','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="approval_status_c"  >

<?php if (! $this->_tpl_vars['fields']['approval_status_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['approval_status_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['approval_status_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['approval_status_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['approval_status_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['approval_status_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['approval_status_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['approval_status_c']['options'][$this->_tpl_vars['fields']['approval_status_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['prepared_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['prepared_by_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PREPARED_BY','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="prepared_by_c"  >

<?php if (! $this->_tpl_vars['fields']['prepared_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="user_id6_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id6_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['prepared_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['ddm_approved_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['ddm_approved_by_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DDM_APPROVED_BY','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="ddm_approved_by_c"  >

<?php if (! $this->_tpl_vars['fields']['ddm_approved_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="user_id3_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id3_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['ddm_approved_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['ddm_response_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['ddm_response_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DDM_RESPONSE_DATE','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="ddm_response_date_c"  >

<?php if (! $this->_tpl_vars['fields']['ddm_response_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['ddm_response_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['ddm_response_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['ddm_response_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['ddm_response_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['ddm_response_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['dceo_apv_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['dceo_apv_by_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DCEO_APV_BY','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="dceo_apv_by_c"  >

<?php if (! $this->_tpl_vars['fields']['dceo_apv_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="user_id5_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id5_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['dceo_apv_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['dceo_response_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['dceo_response_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DCEO_RESPONSE_DATE','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="dceo_response_date_c"  >

<?php if (! $this->_tpl_vars['fields']['dceo_response_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['dceo_response_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['dceo_response_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['dceo_response_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['dceo_response_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['dceo_response_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['ceo_apv_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['ceo_apv_by_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CEO_APV_BY','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="ceo_apv_by_c"  >

<?php if (! $this->_tpl_vars['fields']['ceo_apv_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="user_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['ceo_apv_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['ceo_response_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['ceo_response_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CEO_RESPONSE_DATE','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="ceo_response_date_c"  >

<?php if (! $this->_tpl_vars['fields']['ceo_response_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['ceo_response_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['ceo_response_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['ceo_response_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['ceo_response_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['ceo_response_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-1'>





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['leads_edp_endorsements_1_name']['acl'] > 1 || $this->_tpl_vars['fields']['leads_edp_endorsements_1_name']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_LEADS_TITLE','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="leads_edp_endorsements_1_name"  >

<?php if (! $this->_tpl_vars['fields']['leads_edp_endorsements_1_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['leads_edp_endorsements_1leads_ida']['value'] )): ?>
<?php ob_start(); ?>index.php?module=Leads&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['leads_edp_endorsements_1leads_ida']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="leads_edp_endorsements_1leads_ida" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['leads_edp_endorsements_1leads_ida']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['leads_edp_endorsements_1_name']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['leads_edp_endorsements_1leads_ida']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['client_c']['acl'] > 1 || $this->_tpl_vars['fields']['client_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CLIENT','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="client_c"  >

<?php if (! $this->_tpl_vars['fields']['client_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['account_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=Accounts&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['account_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="account_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['account_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['client_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['account_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['contact_c']['acl'] > 1 || $this->_tpl_vars['fields']['contact_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CONTACT','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="contact_c"  >

<?php if (! $this->_tpl_vars['fields']['contact_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['contact_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=Contacts&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['contact_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="contact_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['contact_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['contact_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['contact_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['line_institute_c']['acl'] > 1 || $this->_tpl_vars['fields']['line_institute_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_LINE_INSTITUTE','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="line_institute_c"  >

<?php if (! $this->_tpl_vars['fields']['line_institute_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=LIP_Line_Institutes&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="lip_line_institutes_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['line_institute_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['event_brief_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_brief_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_BRIEF','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field " type="wysiwyg" field="event_brief_c" colspan='3' >

<?php if (! $this->_tpl_vars['fields']['event_brief_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<iframe
id="<?php echo $this->_tpl_vars['fields']['event_brief_c']['name']; ?>
"
name="<?php echo $this->_tpl_vars['fields']['event_brief_c']['name']; ?>
"
srcdoc="<?php echo $this->_tpl_vars['fields']['event_brief_c']['value']; ?>
"
style="width:100%;height:500px"
></iframe>
<?php endif; ?>

</div>


</div>

<?php endif; ?>
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-2'>





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['assigned_user_name']['acl'] > 1 || $this->_tpl_vars['fields']['assigned_user_name']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ASSIGNED_TO_NAME','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="assigned_user_name"  >

<?php if (! $this->_tpl_vars['fields']['assigned_user_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="assigned_user_id" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['assigned_user_id']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['assigned_user_name']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['created_by_name']['acl'] > 1 || $this->_tpl_vars['fields']['created_by_name']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CREATED','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="relate" field="created_by_name"  >

<?php if (! $this->_tpl_vars['fields']['created_by_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="created_by" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['created_by']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['created_by_name']['value']; ?>
</span>
<?php endif; ?>

</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['date_entered']['acl'] > 1 || $this->_tpl_vars['fields']['date_entered']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DATE_ENTERED','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="datetime" field="date_entered"  >

<?php if (! $this->_tpl_vars['fields']['date_entered']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>

<span id="date_entered" class="sugar_field"><?php echo $this->_tpl_vars['fields']['date_entered']['value']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_BY']; ?>
 <?php echo $this->_tpl_vars['fields']['created_by_name']['value']; ?>
</span>
<?php endif; ?>

</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['modified_by_name']['acl'] > 1 || $this->_tpl_vars['fields']['modified_by_name']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MODIFIED_NAME','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="relate" field="modified_by_name"  >

<?php if (! $this->_tpl_vars['fields']['modified_by_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="modified_user_id" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['modified_user_id']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['modified_by_name']['value']; ?>
</span>
<?php endif; ?>

</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['date_modified']['acl'] > 1 || $this->_tpl_vars['fields']['date_modified']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DATE_MODIFIED','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="datetime" field="date_modified"  >

<?php if (! $this->_tpl_vars['fields']['date_modified']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>

<span id="date_modified" class="sugar_field"><?php echo $this->_tpl_vars['fields']['date_modified']['value']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_BY']; ?>
 <?php echo $this->_tpl_vars['fields']['modified_by_name']['value']; ?>
</span>
<?php endif; ?>

</div>


</div>

<?php endif; ?>
</div>
                    </div>
</div>

<div class="panel-content">
<div>&nbsp;</div>






</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){SUGAR.util.buildAccessKeyLabels();});
</script>            <script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
<?php echo '
<script type="text/javascript">

                    var selectTabDetailView = function(tab) {
                        $(\'#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER\').hide();
                        $(\'#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER\').eq(tab).show().addClass(\'active\').addClass(\'in\');
                    };

                    var selectTabOnError = function(tab) {
                        selectTabDetailView(tab);
                        $(\'#content ul.nav.nav-tabs > li\').removeClass(\'active\');
                        $(\'#content ul.nav.nav-tabs > li a\').css(\'color\', \'\');

                        $(\'#content ul.nav.nav-tabs > li\').eq(tab).find(\'a\').first().css(\'color\', \'red\');
                        $(\'#content ul.nav.nav-tabs > li\').eq(tab).addClass(\'active\');

                    };

                    var selectTabOnErrorInputHandle = function(inputHandle) {
                        var tab = $(inputHandle).closest(\'.tab-pane-NOBOOTSTRAPTOGGLER\').attr(\'id\').match(/^detailpanel_(.*)$/)[1];
                        selectTabOnError(tab);
                    };


                    $(function(){
                        $(\'#content ul.nav.nav-tabs > li > a[data-toggle="tab"]\').click(function(e){
                            if(typeof $(this).parent().find(\'a\').first().attr(\'id\') != \'undefined\') {
                                var tab = parseInt($(this).parent().find(\'a\').first().attr(\'id\').match(/^tab(.)*$/)[1]);
                                selectTabDetailView(tab);
                            }
                        });
                    });

                </script>
'; ?>

<?php else: ?>

<script language="javascript">
<?php echo '
SUGAR.util.doWhen(function(){
    return $("#contentTable").length == 0;
}, SUGAR.themes.actionMenu);
'; ?>

</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="<?php echo $this->_tpl_vars['module']; ?>
">
<input type="hidden" name="record" value="<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="<?php echo $this->_tpl_vars['offset']; ?>
">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
</form>
<ul id="detail_header_action_menu" class="clickMenu fancymenu" ><li class="sugar_action_button" ><?php if ($this->_tpl_vars['bean']->aclAccess('edit')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_KEY']; ?>
" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='EDP_Endorsements'; _form.return_action.value='DetailView'; _form.return_id.value='<?php echo $this->_tpl_vars['id']; ?>
'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_LABEL']; ?>
"><?php endif; ?> <ul id class="subnav" ><li><?php if ($this->_tpl_vars['bean']->aclAccess('delete')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_KEY']; ?>
" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='EDP_Endorsements'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('<?php echo $this->_tpl_vars['APP']['NTC_DELETE_CONFIRMATION']; ?>
')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_LABEL']; ?>
" id="delete_button"><?php endif; ?> </li><li><input type="button" class="button" onClick="showPopup('pdf');" value="<?php echo $this->_tpl_vars['MOD']['LBL_PRINT_AS_PDF']; ?>
"/></li><li><input type="button" class="button" onClick="showPopup('emailpdf');" value="<?php echo $this->_tpl_vars['MOD']['LBL_EMAIL_PDF']; ?>
"/></li><li><?php if ($this->_tpl_vars['bean']->aclAccess('detail')): ?><?php if (! empty ( $this->_tpl_vars['fields']['id']['value'] ) && $this->_tpl_vars['isAuditEnabled']): ?><input id="btn_view_change_log" title="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
" class="button" onclick='open_popup("Audit", "600", "400", "&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
&module_name=EDP_Endorsements", true, false,  { "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] } ); return false;' type="button" value="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
"><?php endif; ?><?php endif; ?></li></ul></li></ul>
</div>
</td>
<td align="right" width="20%"><?php echo $this->_tpl_vars['ADMIN_EDIT']; ?>

</td>
</tr>
</table><?php echo smarty_function_sugar_include(array('include' => $this->_tpl_vars['includes']), $this);?>

<div id="EDP_Endorsements_detailview_tabs"
class="yui-navset detailview_tabs"
>

<ul class="yui-nav">

<li><a id="tab0" href="javascript:void(0)"><em><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL2','module' => 'EDP_Endorsements'), $this);?>
</em></a></li>

<li><a id="tab1" href="javascript:void(0)"><em><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL1','module' => 'EDP_Endorsements'), $this);?>
</em></a></li>

<li><a id="tab2" href="javascript:void(0)"><em><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL3','module' => 'EDP_Endorsements'), $this);?>
</em></a></li>
</ul>
<div class="yui-content">
<div id='tabcontent0'>
<div id='detailpanel_1' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL2' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['approval_status_c']['acl'] > 1 || $this->_tpl_vars['fields']['approval_status_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['approval_status_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_APPROVAL_STATUS','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="approval_status_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['approval_status_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['approval_status_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['approval_status_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['approval_status_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['approval_status_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['approval_status_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['approval_status_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['approval_status_c']['options'][$this->_tpl_vars['fields']['approval_status_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['prepared_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['prepared_by_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['prepared_by_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PREPARED_BY','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="prepared_by_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['prepared_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="user_id6_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id6_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['prepared_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>


<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['ddm_approved_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['ddm_approved_by_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['ddm_approved_by_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DDM_APPROVED_BY','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="ddm_approved_by_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['ddm_approved_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="user_id3_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id3_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['ddm_approved_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['ddm_response_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['ddm_response_date_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['ddm_response_date_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DDM_RESPONSE_DATE','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="ddm_response_date_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['ddm_response_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['ddm_response_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['ddm_response_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['ddm_response_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['ddm_response_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['ddm_response_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['dceo_apv_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['dceo_apv_by_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['dceo_apv_by_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DCEO_APV_BY','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="dceo_apv_by_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['dceo_apv_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="user_id5_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id5_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['dceo_apv_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['dceo_response_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['dceo_response_date_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['dceo_response_date_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DCEO_RESPONSE_DATE','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="dceo_response_date_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['dceo_response_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['dceo_response_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['dceo_response_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['dceo_response_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['dceo_response_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['dceo_response_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['ceo_apv_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['ceo_apv_by_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['ceo_apv_by_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CEO_APV_BY','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="ceo_apv_by_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['ceo_apv_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="user_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['ceo_apv_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['ceo_response_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['ceo_response_date_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['ceo_response_date_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CEO_RESPONSE_DATE','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="ceo_response_date_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['ceo_response_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['ceo_response_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['ceo_response_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['ceo_response_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['ceo_response_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['ceo_response_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL2").style.display='none';</script>
<?php endif; ?>
</div>    <div id='tabcontent1'>
<div id='detailpanel_2' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL1' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['leads_edp_endorsements_1_name']['acl'] > 1 || $this->_tpl_vars['fields']['leads_edp_endorsements_1_name']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['leads_edp_endorsements_1_name']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_LEADS_EDP_ENDORSEMENTS_1_FROM_LEADS_TITLE','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="leads_edp_endorsements_1_name" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['leads_edp_endorsements_1_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['leads_edp_endorsements_1leads_ida']['value'] )): ?>
<?php ob_start(); ?>index.php?module=Leads&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['leads_edp_endorsements_1leads_ida']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="leads_edp_endorsements_1leads_ida" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['leads_edp_endorsements_1leads_ida']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['leads_edp_endorsements_1_name']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['leads_edp_endorsements_1leads_ida']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['client_c']['acl'] > 1 || $this->_tpl_vars['fields']['client_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['client_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CLIENT','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="client_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['client_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['account_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=Accounts&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['account_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="account_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['account_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['client_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['account_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['contact_c']['acl'] > 1 || $this->_tpl_vars['fields']['contact_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['contact_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CONTACT','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="contact_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['contact_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['contact_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=Contacts&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['contact_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="contact_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['contact_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['contact_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['contact_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['line_institute_c']['acl'] > 1 || $this->_tpl_vars['fields']['line_institute_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['line_institute_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_LINE_INSTITUTE','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="line_institute_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['line_institute_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=LIP_Line_Institutes&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="lip_line_institutes_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['line_institute_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['event_brief_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_brief_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['event_brief_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_BRIEF','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="" type="wysiwyg" field="event_brief_c" width='37.5%' colspan='3' >
<?php if (! $this->_tpl_vars['fields']['event_brief_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<iframe
id="<?php echo $this->_tpl_vars['fields']['event_brief_c']['name']; ?>
"
name="<?php echo $this->_tpl_vars['fields']['event_brief_c']['name']; ?>
"
srcdoc="<?php echo $this->_tpl_vars['fields']['event_brief_c']['value']; ?>
"
style="width:100%;height:500px"
></iframe>
<?php endif; ?>

</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL1").style.display='none';</script>
<?php endif; ?>
</div>    <div id='tabcontent2'>
<div id='detailpanel_3' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL3' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['assigned_user_name']['acl'] > 1 || $this->_tpl_vars['fields']['assigned_user_name']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['assigned_user_name']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ASSIGNED_TO_NAME','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="assigned_user_name" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['assigned_user_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="assigned_user_id" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['assigned_user_id']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['assigned_user_name']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['created_by_name']['acl'] > 1 || $this->_tpl_vars['fields']['created_by_name']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['created_by_name']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CREATED','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="" type="relate" field="created_by_name" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['created_by_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="created_by" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['created_by']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['created_by_name']['value']; ?>
</span>
<?php endif; ?>

</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['date_entered']['acl'] > 1 || $this->_tpl_vars['fields']['date_entered']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['date_entered']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DATE_ENTERED','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="" type="datetime" field="date_entered" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['date_entered']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>

<span id="date_entered" class="sugar_field"><?php echo $this->_tpl_vars['fields']['date_entered']['value']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_BY']; ?>
 <?php echo $this->_tpl_vars['fields']['created_by_name']['value']; ?>
</span>
<?php endif; ?>

</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['modified_by_name']['acl'] > 1 || $this->_tpl_vars['fields']['modified_by_name']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['modified_by_name']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MODIFIED_NAME','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="" type="relate" field="modified_by_name" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['modified_by_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="modified_user_id" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['modified_user_id']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['modified_by_name']['value']; ?>
</span>
<?php endif; ?>

</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['date_modified']['acl'] > 1 || $this->_tpl_vars['fields']['date_modified']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['date_modified']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DATE_MODIFIED','module' => 'EDP_Endorsements'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="" type="datetime" field="date_modified" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['date_modified']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>

<span id="date_modified" class="sugar_field"><?php echo $this->_tpl_vars['fields']['date_modified']['value']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_BY']; ?>
 <?php echo $this->_tpl_vars['fields']['modified_by_name']['value']; ?>
</span>
<?php endif; ?>

</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL3").style.display='none';</script>
<?php endif; ?>
</div>
</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){SUGAR.util.buildAccessKeyLabels();});
</script><script type='text/javascript' src='<?php echo smarty_function_sugar_getjspath(array('file' => 'include/javascript/popup_helper.js'), $this);?>
'></script>
<script type="text/javascript" src="<?php echo smarty_function_sugar_getjspath(array('file' => 'cache/include/javascript/sugar_grp_yui_widgets.js'), $this);?>
"></script>
<script type="text/javascript">
var EDP_Endorsements_detailview_tabs = new YAHOO.widget.TabView("EDP_Endorsements_detailview_tabs");
EDP_Endorsements_detailview_tabs.selectTab(0);
</script>
<script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
<?php endif; ?>