<?php /* Smarty version 2.6.31, created on 2022-12-13 19:26:46
         compiled from cache/themes/SuiteP/modules/Leads/DetailView.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 18, false),array('modifier', 'strip_semicolon', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 207, false),array('modifier', 'escape', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 266, false),array('modifier', 'url2html', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 266, false),array('modifier', 'nl2br', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 266, false),array('function', 'sugar_include', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 89, false),array('function', 'sugar_translate', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 100, false),array('function', 'counter', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 214, false),array('function', 'sugar_ajax_url', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 734, false),array('function', 'sugar_number_format', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 1135, false),array('function', 'sugar_getimage', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 4971, false),array('function', 'sugar_getimagepath', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 5545, false),array('function', 'sugar_getjspath', 'cache/themes/SuiteP/modules/Leads/DetailView.tpl', 8527, false),)), $this); ?>

<?php 
global $current_user,$sugar_config;
$this->_tpl_vars['current_user'] = $current_user->id;
$this->_tpl_vars['is_admin_user'] = $current_user->is_admin;
$this->_tpl_vars['current_theme'] = $current_user->getPreference('user_theme');
$this->_tpl_vars['suite_valid_version'] =  true;
if(preg_match('/(7\.7\.[0-9])/', $sugar_config['suitecrm_version']) || $sugar_config['suitecrm_version'] == '7.7'){
$this->_tpl_vars['suite_valid_version'] = false;
}
 ?>
<?php if ($this->_tpl_vars['current_theme'] == 'SuiteP'): ?>

<?php $this->assign('preForm', "<table width='100%' border='1' cellspacing='0' cellpadding='0' class='converted_account'><tr><td><table width='100%'><tr><td>"); ?>
<?php $this->assign('displayPreform', false); ?>
<?php if (isset ( $this->_tpl_vars['bean']->contact_id ) && ! empty ( $this->_tpl_vars['bean']->contact_id )): ?>
<?php $this->assign('displayPreform', true); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_CONTACT']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_CONTACT']))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "&nbsp;<a href='index.php?module=Contacts&action=DetailView&record=") : smarty_modifier_cat($_tmp, "&nbsp;<a href='index.php?module=Contacts&action=DetailView&record="))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->contact_id) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->contact_id))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "'>") : smarty_modifier_cat($_tmp, "'>"))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->contact_name) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->contact_name))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</a>") : smarty_modifier_cat($_tmp, "</a>"))); ?>
<?php endif; ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</td><td>") : smarty_modifier_cat($_tmp, "</td><td>"))); ?>
<?php if (( $this->_tpl_vars['bean']->converted == '1' ) && isset ( $this->_tpl_vars['bean']->account_id ) && ! empty ( $this->_tpl_vars['bean']->account_id )): ?>
<?php $this->assign('displayPreform', true); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_ACCOUNT']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_ACCOUNT']))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "&nbsp;<a href='index.php?module=Accounts&action=DetailView&record=") : smarty_modifier_cat($_tmp, "&nbsp;<a href='index.php?module=Accounts&action=DetailView&record="))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->account_id) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->account_id))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "'>") : smarty_modifier_cat($_tmp, "'>"))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->account_name) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->account_name))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</a>") : smarty_modifier_cat($_tmp, "</a>"))); ?>
<?php endif; ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</td><td>") : smarty_modifier_cat($_tmp, "</td><td>"))); ?>
<?php if (isset ( $this->_tpl_vars['bean']->opportunity_id ) && ! empty ( $this->_tpl_vars['bean']->opportunity_id )): ?>
<?php $this->assign('displayPreform', true); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_OPP']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_OPP']))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "&nbsp;<a href='index.php?module=Opportunities&action=DetailView&record=") : smarty_modifier_cat($_tmp, "&nbsp;<a href='index.php?module=Opportunities&action=DetailView&record="))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->opportunity_id) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->opportunity_id))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "'>") : smarty_modifier_cat($_tmp, "'>"))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->opportunity_name) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->opportunity_name))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</a>") : smarty_modifier_cat($_tmp, "</a>"))); ?>
<?php endif; ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</td></tr></table></td></tr></table>") : smarty_modifier_cat($_tmp, "</td></tr></table></td></tr></table>"))); ?>
<?php if ($this->_tpl_vars['displayPreform']): ?>
<?php echo $this->_tpl_vars['preForm']; ?>

<br>
<?php endif; ?>

<script language="javascript">
    <?php echo '
    SUGAR.util.doWhen(function () {
        return $("#contentTable").length == 0;
    }, SUGAR.themes.actionMenu);
    '; ?>

</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="<?php echo $this->_tpl_vars['module']; ?>
">
<input type="hidden" name="record" value="<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="<?php echo $this->_tpl_vars['offset']; ?>
">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
<?php if (! $this->_tpl_vars['config']['enable_action_menu']): ?>
<div class="buttons">
<?php if ($this->_tpl_vars['bean']->aclAccess('edit')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_KEY']; ?>
" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='<?php echo $this->_tpl_vars['id']; ?>
'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_LABEL']; ?>
"><?php endif; ?> 
<?php if ($this->_tpl_vars['bean']->aclAccess('edit')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_DUPLICATE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_DUPLICATE_BUTTON_KEY']; ?>
" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.isDuplicate.value=true; _form.action.value='EditView'; _form.return_id.value='<?php echo $this->_tpl_vars['id']; ?>
';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Duplicate" value="<?php echo $this->_tpl_vars['APP']['LBL_DUPLICATE_BUTTON_LABEL']; ?>
" id="duplicate_button"><?php endif; ?> 
<?php if ($this->_tpl_vars['bean']->aclAccess('delete')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_KEY']; ?>
" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('<?php echo $this->_tpl_vars['APP']['NTC_DELETE_CONFIRMATION']; ?>
')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_LABEL']; ?>
" id="delete_button"><?php endif; ?> 
<input class="button hidden" id="send_confirm_opt_in_email" title="<?php echo $this->_tpl_vars['APP']['LBL_SEND_CONFIRM_OPT_IN_EMAIL']; ?>
" onclick="var _form = document.getElementById('formDetailView');_form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
'; _form.action.value='sendConfirmOptInEmail'; _form.module.value='Leads'; _form.module_tab.value='Leads';_form.submit();" name="send_confirm_opt_in_email" disabled="1" type="button" value="<?php echo $this->_tpl_vars['APP']['LBL_SEND_CONFIRM_OPT_IN_EMAIL']; ?>
"/>
<input type="button" class="button" onClick="showPopup();" value="<?php echo $this->_tpl_vars['APP']['LBL_PRINT_AS_PDF']; ?>
"/>
<?php if ($this->_tpl_vars['bean']->aclAccess('detail')): ?><?php if (! empty ( $this->_tpl_vars['fields']['id']['value'] ) && $this->_tpl_vars['isAuditEnabled']): ?><input id="btn_view_change_log" title="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
" class="button" onclick='open_popup("Audit", "600", "400", "&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
&module_name=Leads", true, false,  { "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] } ); return false;' type="button" value="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
"><?php endif; ?><?php endif; ?>
</div>                    <?php endif; ?>
</form>
</div>
</td>
<td align="right" width="20%" class="buttons"><?php echo $this->_tpl_vars['ADMIN_EDIT']; ?>

</td>
</tr>
</table>
<?php echo smarty_function_sugar_include(array('include' => $this->_tpl_vars['includes']), $this);?>

<div class="detail-view">
<?php if ($this->_tpl_vars['suite_valid_version']): ?>
<div class="mobile-pagination"><?php echo $this->_tpl_vars['PAGINATION']; ?>
</div>
<?php endif; ?>

<ul class="nav nav-tabs">


<li role="presentation" class="active">
<a id="tab0" data-toggle="tab" class="hidden-xs">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL5','module' => 'Leads'), $this);?>

</a>


<a id="xstab0" href="#" class="visible-xs first-tab-xs dropdown-toggle" data-toggle="dropdown">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL5','module' => 'Leads'), $this);?>

</a>
<ul id="first-tab-menu-xs" class="dropdown-menu">
<li role="presentation">
<a id="tab1" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-1');">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL5','module' => 'Leads'), $this);?>

</a>
</li>
<li role="presentation">
<a id="tab10" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-10');">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_EDITVIEW_PANEL1','module' => 'Leads'), $this);?>

</a>
</li>
<li role="presentation">
<a id="tab11" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-11');">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL4','module' => 'Leads'), $this);?>

</a>
</li>
<li role="presentation">
<a id="tab12" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-12');">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_PANEL_ASSIGNMENT','module' => 'Leads'), $this);?>

</a>
</li>
</ul>
</li>


















<li role="presentation" class="hidden-xs">
<a id="tab1" data-toggle="tab">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_EDITVIEW_PANEL1','module' => 'Leads'), $this);?>

</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab2" data-toggle="tab">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL4','module' => 'Leads'), $this);?>

</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab3" data-toggle="tab">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_PANEL_ASSIGNMENT','module' => 'Leads'), $this);?>

</a>
</li>
<?php if ($this->_tpl_vars['config']['enable_action_menu'] && $this->_tpl_vars['config']['enable_action_menu'] != false): ?>
<li id="tab-actions" class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">ACTIONS<span class="suitepicon suitepicon-action-caret"></span></a>
<ul class="dropdown-menu">
<li><?php if ($this->_tpl_vars['bean']->aclAccess('edit')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_KEY']; ?>
" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='<?php echo $this->_tpl_vars['id']; ?>
'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_LABEL']; ?>
"><?php endif; ?> </li>
<li><?php if ($this->_tpl_vars['bean']->aclAccess('edit')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_DUPLICATE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_DUPLICATE_BUTTON_KEY']; ?>
" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.isDuplicate.value=true; _form.action.value='EditView'; _form.return_id.value='<?php echo $this->_tpl_vars['id']; ?>
';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Duplicate" value="<?php echo $this->_tpl_vars['APP']['LBL_DUPLICATE_BUTTON_LABEL']; ?>
" id="duplicate_button"><?php endif; ?> </li>
<li><?php if ($this->_tpl_vars['bean']->aclAccess('delete')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_KEY']; ?>
" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('<?php echo $this->_tpl_vars['APP']['NTC_DELETE_CONFIRMATION']; ?>
')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_LABEL']; ?>
" id="delete_button"><?php endif; ?> </li>
<li><input class="button hidden" id="send_confirm_opt_in_email" title="<?php echo $this->_tpl_vars['APP']['LBL_SEND_CONFIRM_OPT_IN_EMAIL']; ?>
" onclick="var _form = document.getElementById('formDetailView');_form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
'; _form.action.value='sendConfirmOptInEmail'; _form.module.value='Leads'; _form.module_tab.value='Leads';_form.submit();" name="send_confirm_opt_in_email" disabled="1" type="button" value="<?php echo $this->_tpl_vars['APP']['LBL_SEND_CONFIRM_OPT_IN_EMAIL']; ?>
"/></li>
<li><input type="button" class="button" onClick="showPopup();" value="<?php echo $this->_tpl_vars['APP']['LBL_PRINT_AS_PDF']; ?>
"/></li>
<li><?php if ($this->_tpl_vars['bean']->aclAccess('detail')): ?><?php if (! empty ( $this->_tpl_vars['fields']['id']['value'] ) && $this->_tpl_vars['isAuditEnabled']): ?><input id="btn_view_change_log" title="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
" class="button" onclick='open_popup("Audit", "600", "400", "&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
&module_name=Leads", true, false,  { "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] } ); return false;' type="button" value="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
"><?php endif; ?><?php endif; ?></li>
</ul>        </li>
<?php if ($this->_tpl_vars['suite_valid_version']): ?>
<li class="tab-inline-pagination">
<?php echo $this->_tpl_vars['PAGINATION']; ?>

</li>
<?php endif; ?>
<?php endif; ?>
</ul>
<div class="clearfix"></div>

<div class="tab-content">

<div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-0'>





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['status']['acl'] > 1 || $this->_tpl_vars['fields']['status']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STATUS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="status"  >

<?php if (! $this->_tpl_vars['fields']['status']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['status']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['status']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['status']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['status']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['status']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['status']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['status']['options'][$this->_tpl_vars['fields']['status']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['reason_c']['acl'] > 1 || $this->_tpl_vars['fields']['reason_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_REASON','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="text" field="reason_c"  >

<?php if (! $this->_tpl_vars['fields']['reason_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span class="sugar_field" id="<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['fields']['reason_c']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('url2html', true, $_tmp) : url2html($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['fields']['reason_c']['value'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('escape', true, $_tmp, 'html_entity_decode') : smarty_modifier_escape($_tmp, 'html_entity_decode')))) ? $this->_run_mod_handler('url2html', true, $_tmp) : url2html($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['start_engagement_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_engagement_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_ENGAGEMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_engagement_c"  >

<?php if (! $this->_tpl_vars['fields']['start_engagement_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_engagement_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_engagement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_engagement_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_engagement_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_engagement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_engagement_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_engagement_c']['options'][$this->_tpl_vars['fields']['start_engagement_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['start_event_approval_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_event_approval_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_EVENT_APPROVAL','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_event_approval_c"  >

<?php if (! $this->_tpl_vars['fields']['start_event_approval_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_event_approval_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['options'][$this->_tpl_vars['fields']['start_event_approval_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['start_eoi_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_eoi_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_EOI','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_eoi_c"  >

<?php if (! $this->_tpl_vars['fields']['start_eoi_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_eoi_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_eoi_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_eoi_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_eoi_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_eoi_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_eoi_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_eoi_c']['options'][$this->_tpl_vars['fields']['start_eoi_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['start_endorsement_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_endorsement_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_ENDORSEMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_endorsement_c"  >

<?php if (! $this->_tpl_vars['fields']['start_endorsement_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_endorsement_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['options'][$this->_tpl_vars['fields']['start_endorsement_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['start_event_docs_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_event_docs_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_EVENT_DOCS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_event_docs_c"  >

<?php if (! $this->_tpl_vars['fields']['start_event_docs_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_event_docs_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['options'][$this->_tpl_vars['fields']['start_event_docs_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['start_inspection_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_inspection_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_INSPECTION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_inspection_c"  >

<?php if (! $this->_tpl_vars['fields']['start_inspection_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_inspection_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_inspection_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_inspection_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_inspection_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_inspection_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_inspection_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_inspection_c']['options'][$this->_tpl_vars['fields']['start_inspection_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['start_presentation_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_presentation_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_PRESENTATION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_presentation_c"  >

<?php if (! $this->_tpl_vars['fields']['start_presentation_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_presentation_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_presentation_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_presentation_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_presentation_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_presentation_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_presentation_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_presentation_c']['options'][$this->_tpl_vars['fields']['start_presentation_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['start_mou_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_mou_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_MOU','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_mou_c"  >

<?php if (! $this->_tpl_vars['fields']['start_mou_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_mou_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_mou_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_mou_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_mou_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_mou_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_mou_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_mou_c']['options'][$this->_tpl_vars['fields']['start_mou_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-9'>





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['account_c']['acl'] > 1 || $this->_tpl_vars['fields']['account_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ACCOUNT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="account_c"  >

<?php if (! $this->_tpl_vars['fields']['account_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['account_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=Accounts&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['account_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="account_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['account_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['account_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['account_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['segment_c']['acl'] > 1 || $this->_tpl_vars['fields']['segment_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SEGMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="segment_c"  >

<?php if (! $this->_tpl_vars['fields']['segment_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['segment_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['segment_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['segment_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['segment_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['segment_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['segment_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['segment_c']['options'][$this->_tpl_vars['fields']['segment_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['event_title_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_title_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_TITLE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="event_title_c"  >

<?php if (! $this->_tpl_vars['fields']['event_title_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['event_title_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_title_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_title_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['event_title_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['event_title_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['primary_contact_c']['acl'] > 1 || $this->_tpl_vars['fields']['primary_contact_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PRIMARY_CONTACT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="primary_contact_c"  >

<?php if (! $this->_tpl_vars['fields']['primary_contact_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['contact_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=Contacts&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['contact_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="contact_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['contact_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['primary_contact_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['contact_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['event_abbreviation_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_abbreviation_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_ABBREVIATION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="event_abbreviation_c"  >

<?php if (! $this->_tpl_vars['fields']['event_abbreviation_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['event_abbreviation_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_abbreviation_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_abbreviation_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['event_abbreviation_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['event_abbreviation_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['event_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="date" field="event_date_c"  >

<?php if (! $this->_tpl_vars['fields']['event_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (strlen ( $this->_tpl_vars['fields']['event_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_date_c']['value']); ?>
<?php endif; ?>
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['event_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['event_end_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_end_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_END_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="date" field="event_end_date_c"  >

<?php if (! $this->_tpl_vars['fields']['event_end_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (strlen ( $this->_tpl_vars['fields']['event_end_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_end_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_end_date_c']['value']); ?>
<?php endif; ?>
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['event_end_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['packs_c']['acl'] > 1 || $this->_tpl_vars['fields']['packs_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PACKS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="int" field="packs_c"  >

<?php if (! $this->_tpl_vars['fields']['packs_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['packs_c']['name']; ?>
">
<?php $this->assign('value', $this->_tpl_vars['fields']['packs_c']['value']); ?>
<?php echo $this->_tpl_vars['value']; ?>

</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['event_days_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_days_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_DAYS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="int" field="event_days_c"  >

<?php if (! $this->_tpl_vars['fields']['event_days_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['event_days_c']['name']; ?>
">
<?php $this->assign('value', $this->_tpl_vars['fields']['event_days_c']['value']); ?>
<?php echo $this->_tpl_vars['value']; ?>

</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>


<?php if ($this->_tpl_vars['fields']['currency_id']['acl'] > 1 || $this->_tpl_vars['fields']['currency_id']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CURRENCY','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency_id" field="currency_id"  >

<?php if (! $this->_tpl_vars['fields']['currency_id']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>

<span id='currency_id_span'>
<?php echo $this->_tpl_vars['fields']['currency_id']['value']; ?>

</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['expected_revenue_c']['acl'] > 1 || $this->_tpl_vars['fields']['expected_revenue_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EXPECTED_REVENUE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="expected_revenue_c"  >

<?php if (! $this->_tpl_vars['fields']['expected_revenue_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id='<?php echo $this->_tpl_vars['fields']['expected_revenue_c']['name']; ?>
'>
<?php echo smarty_function_sugar_number_format(array('var' => $this->_tpl_vars['fields']['expected_revenue_c']['value']), $this);?>

</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['gor_commitment_c']['acl'] > 1 || $this->_tpl_vars['fields']['gor_commitment_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_GOR_COMMITMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="gor_commitment_c"  >

<?php if (! $this->_tpl_vars['fields']['gor_commitment_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id='<?php echo $this->_tpl_vars['fields']['gor_commitment_c']['name']; ?>
'>
<?php echo smarty_function_sugar_number_format(array('var' => $this->_tpl_vars['fields']['gor_commitment_c']['value']), $this);?>

</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['lead_source']['acl'] > 1 || $this->_tpl_vars['fields']['lead_source']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_LEAD_SOURCE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="lead_source"  >

<?php if (! $this->_tpl_vars['fields']['lead_source']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['lead_source']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['lead_source']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['lead_source']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['lead_source']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['lead_source']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['lead_source']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['lead_source']['options'][$this->_tpl_vars['fields']['lead_source']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['source_details_c']['acl'] > 1 || $this->_tpl_vars['fields']['source_details_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SOURCE_DETAILS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="dynamicenum" field="source_details_c"  >

<?php if (! $this->_tpl_vars['fields']['source_details_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['source_details_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['source_details_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['source_details_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['source_details_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['source_details_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['source_details_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['source_details_c']['options'][$this->_tpl_vars['fields']['source_details_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['probability_c']['acl'] > 1 || $this->_tpl_vars['fields']['probability_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PROBABILITY ','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="probability_c"  >

<?php if (! $this->_tpl_vars['fields']['probability_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['probability_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['probability_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['probability_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['probability_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['probability_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['probability_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['probability_c']['options'][$this->_tpl_vars['fields']['probability_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['event_coordinator_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_coordinator_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_COORDINATOR','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="event_coordinator_c"  >

<?php if (! $this->_tpl_vars['fields']['event_coordinator_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="user_id2_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id2_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['event_coordinator_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['line_institute_c']['acl'] > 1 || $this->_tpl_vars['fields']['line_institute_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_LINE_INSTITUTE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="line_institute_c"  >

<?php if (! $this->_tpl_vars['fields']['line_institute_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=LIP_Line_Institutes&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="lip_line_institutes_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['line_institute_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['venue_c']['acl'] > 1 || $this->_tpl_vars['fields']['venue_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_VENUE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="venue_c"  >

<?php if (! $this->_tpl_vars['fields']['venue_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['vnp_venues_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=VNP_Venues&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['vnp_venues_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="vnp_venues_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['vnp_venues_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['venue_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['vnp_venues_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['stp_sales_target_leads_1_name']['acl'] > 1 || $this->_tpl_vars['fields']['stp_sales_target_leads_1_name']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_STP_SALES_TARGET_TITLE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="stp_sales_target_leads_1_name"  >

<?php if (! $this->_tpl_vars['fields']['stp_sales_target_leads_1_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['stp_sales_target_leads_1stp_sales_target_ida']['value'] )): ?>
<?php ob_start(); ?>index.php?module=STP_Sales_Target&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['stp_sales_target_leads_1stp_sales_target_ida']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="stp_sales_target_leads_1stp_sales_target_ida" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['stp_sales_target_leads_1stp_sales_target_ida']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['stp_sales_target_leads_1_name']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['stp_sales_target_leads_1stp_sales_target_ida']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-10'>





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['event_brief_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_brief_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_BRIEF','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field " type="wysiwyg" field="event_brief_c" colspan='3' >

<?php if (! $this->_tpl_vars['fields']['event_brief_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<iframe
id="<?php echo $this->_tpl_vars['fields']['event_brief_c']['name']; ?>
"
name="<?php echo $this->_tpl_vars['fields']['event_brief_c']['name']; ?>
"
srcdoc="<?php echo $this->_tpl_vars['fields']['event_brief_c']['value']; ?>
"
style="width:100%;height:500px"
></iframe>
<?php endif; ?>

</div>


</div>

<?php endif; ?>
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-11'>





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['lead_progress_c']['acl'] > 1 || $this->_tpl_vars['fields']['lead_progress_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_LEAD_PROGRESS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="lead_progress_c"  >

<?php if (! $this->_tpl_vars['fields']['lead_progress_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['lead_progress_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['lead_progress_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['lead_progress_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['lead_progress_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['lead_progress_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['lead_progress_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['lead_progress_c']['options'][$this->_tpl_vars['fields']['lead_progress_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['assigned_user_name']['acl'] > 1 || $this->_tpl_vars['fields']['assigned_user_name']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ASSIGNED_TO','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="assigned_user_name"  >

<?php if (! $this->_tpl_vars['fields']['assigned_user_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="assigned_user_id" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['assigned_user_id']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['assigned_user_name']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['day_spending_c']['acl'] > 1 || $this->_tpl_vars['fields']['day_spending_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DAY_SPENDING','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="day_spending_c"  >

<?php if (! $this->_tpl_vars['fields']['day_spending_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id='<?php echo $this->_tpl_vars['fields']['day_spending_c']['name']; ?>
'>
<?php echo smarty_function_sugar_number_format(array('var' => $this->_tpl_vars['fields']['day_spending_c']['value']), $this);?>

</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['created_by_name']['acl'] > 1 || $this->_tpl_vars['fields']['created_by_name']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="relate" field="created_by_name"  >

<?php if (! $this->_tpl_vars['fields']['created_by_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="created_by" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['created_by']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['created_by_name']['value']; ?>
</span>
<?php endif; ?>

</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['modified_by_name']['acl'] > 1 || $this->_tpl_vars['fields']['modified_by_name']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MODIFIED_NAME','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="relate" field="modified_by_name"  >

<?php if (! $this->_tpl_vars['fields']['modified_by_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="modified_user_id" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['modified_user_id']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['modified_by_name']['value']; ?>
</span>
<?php endif; ?>

</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['date_entered']['acl'] > 1 || $this->_tpl_vars['fields']['date_entered']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DATE_ENTERED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="datetime" field="date_entered"  >

<?php if (! $this->_tpl_vars['fields']['date_entered']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>

<span id="date_entered" class="sugar_field"><?php echo $this->_tpl_vars['fields']['date_entered']['value']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_BY']; ?>
 <?php echo $this->_tpl_vars['fields']['created_by_name']['value']; ?>
</span>
<?php endif; ?>

</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['date_modified']['acl'] > 1 || $this->_tpl_vars['fields']['date_modified']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DATE_MODIFIED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="datetime" field="date_modified"  >

<?php if (! $this->_tpl_vars['fields']['date_modified']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>

<span id="date_modified" class="sugar_field"><?php echo $this->_tpl_vars['fields']['date_modified']['value']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_BY']; ?>
 <?php echo $this->_tpl_vars['fields']['modified_by_name']['value']; ?>
</span>
<?php endif; ?>

</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['client_reponse_details_c']['acl'] > 1 || $this->_tpl_vars['fields']['client_reponse_details_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CLIENT_REPONSE_DETAILS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="text" field="client_reponse_details_c"  >

<?php if (! $this->_tpl_vars['fields']['client_reponse_details_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span class="sugar_field" id="<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['fields']['client_reponse_details_c']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('url2html', true, $_tmp) : url2html($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['fields']['client_reponse_details_c']['value'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('escape', true, $_tmp, 'html_entity_decode') : smarty_modifier_escape($_tmp, 'html_entity_decode')))) ? $this->_run_mod_handler('url2html', true, $_tmp) : url2html($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['gor_revenue_c']['acl'] > 1 || $this->_tpl_vars['fields']['gor_revenue_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_GOR_REVENUE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="gor_revenue_c"  >

<?php if (! $this->_tpl_vars['fields']['gor_revenue_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id='<?php echo $this->_tpl_vars['fields']['gor_revenue_c']['name']; ?>
'>
<?php echo smarty_function_sugar_number_format(array('var' => $this->_tpl_vars['fields']['gor_revenue_c']['value']), $this);?>

</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['sales_target_c']['acl'] > 1 || $this->_tpl_vars['fields']['sales_target_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SALES_TARGET','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="sales_target_c"  >

<?php if (! $this->_tpl_vars['fields']['sales_target_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id='<?php echo $this->_tpl_vars['fields']['sales_target_c']['name']; ?>
'>
<?php echo smarty_function_sugar_number_format(array('var' => $this->_tpl_vars['fields']['sales_target_c']['value']), $this);?>

</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['qualfication_c']['acl'] > 1 || $this->_tpl_vars['fields']['qualfication_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_QUALFICATION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="qualfication_c"  >

<?php if (! $this->_tpl_vars['fields']['qualfication_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['qualfication_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['qualfication_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['qualfication_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['qualfication_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['qualfication_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['qualfication_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['qualfication_c']['options'][$this->_tpl_vars['fields']['qualfication_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['step2_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step2_created_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP2_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step2_created_c"  >

<?php if (! $this->_tpl_vars['fields']['step2_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step2_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step2_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step2_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step2_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step2_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step2_created_c']['name']; ?>
" value="$fields.step2_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['step3_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step3_created_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP3_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step3_created_c"  >

<?php if (! $this->_tpl_vars['fields']['step3_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step3_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step3_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step3_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step3_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step3_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step3_created_c']['name']; ?>
" value="$fields.step3_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['step4_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step4_created_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP4_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step4_created_c"  >

<?php if (! $this->_tpl_vars['fields']['step4_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step4_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step4_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step4_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step4_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step4_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step4_created_c']['name']; ?>
" value="$fields.step4_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['step5_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step5_created_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP5_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step5_created_c"  >

<?php if (! $this->_tpl_vars['fields']['step5_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step5_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step5_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step5_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step5_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step5_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step5_created_c']['name']; ?>
" value="$fields.step5_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['step6_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step6_created_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP6_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step6_created_c"  >

<?php if (! $this->_tpl_vars['fields']['step6_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step6_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step6_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step6_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step6_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step6_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step6_created_c']['name']; ?>
" value="$fields.step6_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['step7_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step7_created_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP7_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step7_created_c"  >

<?php if (! $this->_tpl_vars['fields']['step7_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step7_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step7_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step7_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step7_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step7_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step7_created_c']['name']; ?>
" value="$fields.step7_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['step8_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step8_created_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP8_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step8_created_c"  >

<?php if (! $this->_tpl_vars['fields']['step8_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step8_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step8_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step8_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step8_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step8_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step8_created_c']['name']; ?>
" value="$fields.step8_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['step3_eoi_chk_c']['acl'] > 1 || $this->_tpl_vars['fields']['step3_eoi_chk_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP3_EOI_CHK','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step3_eoi_chk_c"  >

<?php if (! $this->_tpl_vars['fields']['step3_eoi_chk_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step3_eoi_chk_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step3_eoi_chk_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step3_eoi_chk_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step3_eoi_chk_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step3_eoi_chk_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step3_eoi_chk_c']['name']; ?>
" value="$fields.step3_eoi_chk_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['step4_endo_chk_c']['acl'] > 1 || $this->_tpl_vars['fields']['step4_endo_chk_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP4_ENDO_CHK','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step4_endo_chk_c"  >

<?php if (! $this->_tpl_vars['fields']['step4_endo_chk_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step4_endo_chk_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step4_endo_chk_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step4_endo_chk_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step4_endo_chk_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step4_endo_chk_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step4_endo_chk_c']['name']; ?>
" value="$fields.step4_endo_chk_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['project_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['project_created_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PROJECT_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="bool" field="project_created_c" colspan='3' >

<?php if (! $this->_tpl_vars['fields']['project_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['project_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['project_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['project_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['project_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['project_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['project_created_c']['name']; ?>
" value="$fields.project_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                    </div>
</div>

<div class="panel-content">
<div>&nbsp;</div>







<?php if ($this->_tpl_vars['config']['enable_action_menu'] && $this->_tpl_vars['config']['enable_action_menu'] != false): ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-0" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL7','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-0"  data-id="LBL_DETAILVIEW_PANEL7">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['initial_email_c']['acl'] > 1 || $this->_tpl_vars['fields']['initial_email_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INITIAL_EMAIL','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="initial_email_c"  >

<?php if (! $this->_tpl_vars['fields']['initial_email_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['initial_email_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_email_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_email_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_email_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_email_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_email_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_email_c']['options'][$this->_tpl_vars['fields']['initial_email_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['engaged_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['engaged_by_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENGAGED_BY','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="engaged_by_c"  >

<?php if (! $this->_tpl_vars['fields']['engaged_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="user_id1_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id1_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['engaged_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['sent_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['sent_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SENT_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="sent_date_c"  >

<?php if (! $this->_tpl_vars['fields']['sent_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['sent_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['sent_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['sent_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['sent_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['sent_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['responded_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['responded_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_RESPONDED_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="responded_date_c"  >

<?php if (! $this->_tpl_vars['fields']['responded_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['responded_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['responded_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['responded_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['responded_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['responded_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                                </div>
</div>
</div>
<?php else: ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-0" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL7','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-0" data-id="LBL_DETAILVIEW_PANEL7">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['initial_email_c']['acl'] > 1 || $this->_tpl_vars['fields']['initial_email_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INITIAL_EMAIL','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="initial_email_c"  >

<?php if (! $this->_tpl_vars['fields']['initial_email_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['initial_email_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_email_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_email_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_email_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_email_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_email_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_email_c']['options'][$this->_tpl_vars['fields']['initial_email_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['engaged_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['engaged_by_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENGAGED_BY','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="engaged_by_c"  >

<?php if (! $this->_tpl_vars['fields']['engaged_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<span id="user_id1_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id1_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['engaged_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['sent_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['sent_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SENT_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="sent_date_c"  >

<?php if (! $this->_tpl_vars['fields']['sent_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['sent_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['sent_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['sent_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['sent_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['sent_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['responded_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['responded_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_RESPONDED_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="responded_date_c"  >

<?php if (! $this->_tpl_vars['fields']['responded_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['responded_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['responded_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['responded_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['responded_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['responded_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                            </div>
</div>
</div>
<?php endif; ?>





<?php if ($this->_tpl_vars['config']['enable_action_menu'] && $this->_tpl_vars['config']['enable_action_menu'] != false): ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-1" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL8','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-1"  data-id="LBL_DETAILVIEW_PANEL8">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['initial_event_approval_c']['acl'] > 1 || $this->_tpl_vars['fields']['initial_event_approval_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INITIAL_EVENT_APPROVAL','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="initial_event_approval_c"  >

<?php if (! $this->_tpl_vars['fields']['initial_event_approval_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['initial_event_approval_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['options'][$this->_tpl_vars['fields']['initial_event_approval_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['iea_start_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['iea_start_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_IEA_START_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="iea_start_date_c"  >

<?php if (! $this->_tpl_vars['fields']['iea_start_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['iea_start_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_start_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_start_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['iea_start_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['iea_start_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['iea_end_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['iea_end_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_IEA_END_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="iea_end_date_c"  >

<?php if (! $this->_tpl_vars['fields']['iea_end_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['iea_end_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_end_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_end_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['iea_end_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['iea_end_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                                </div>
</div>
</div>
<?php else: ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-1" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL8','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-1" data-id="LBL_DETAILVIEW_PANEL8">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['initial_event_approval_c']['acl'] > 1 || $this->_tpl_vars['fields']['initial_event_approval_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INITIAL_EVENT_APPROVAL','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="initial_event_approval_c"  >

<?php if (! $this->_tpl_vars['fields']['initial_event_approval_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['initial_event_approval_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['options'][$this->_tpl_vars['fields']['initial_event_approval_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['iea_start_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['iea_start_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_IEA_START_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="iea_start_date_c"  >

<?php if (! $this->_tpl_vars['fields']['iea_start_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['iea_start_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_start_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_start_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['iea_start_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['iea_start_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['iea_end_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['iea_end_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_IEA_END_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="iea_end_date_c"  >

<?php if (! $this->_tpl_vars['fields']['iea_end_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['iea_end_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_end_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_end_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['iea_end_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['iea_end_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                            </div>
</div>
</div>
<?php endif; ?>





<?php if ($this->_tpl_vars['config']['enable_action_menu'] && $this->_tpl_vars['config']['enable_action_menu'] != false): ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-2" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL9','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-2"  data-id="LBL_DETAILVIEW_PANEL9">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['eoi_c']['acl'] > 1 || $this->_tpl_vars['fields']['eoi_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EOI','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="eoi_c"  >

<?php if (! $this->_tpl_vars['fields']['eoi_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['eoi_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['eoi_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['eoi_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['eoi_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['eoi_c']['options'][$this->_tpl_vars['fields']['eoi_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['eoi_approval_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['eoi_approval_start_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EOI_APPROVAL_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="eoi_approval_start_c"  >

<?php if (! $this->_tpl_vars['fields']['eoi_approval_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['eoi_approval_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_approval_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['eoi_approval_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['eoi_approval_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['eoi_approval_end_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EOI_APPROVAL_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="eoi_approval_end_c"  >

<?php if (! $this->_tpl_vars['fields']['eoi_approval_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['eoi_approval_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_approval_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['eoi_approval_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                                </div>
</div>
</div>
<?php else: ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-2" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL9','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-2" data-id="LBL_DETAILVIEW_PANEL9">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['eoi_c']['acl'] > 1 || $this->_tpl_vars['fields']['eoi_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EOI','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="eoi_c"  >

<?php if (! $this->_tpl_vars['fields']['eoi_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['eoi_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['eoi_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['eoi_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['eoi_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['eoi_c']['options'][$this->_tpl_vars['fields']['eoi_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['eoi_approval_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['eoi_approval_start_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EOI_APPROVAL_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="eoi_approval_start_c"  >

<?php if (! $this->_tpl_vars['fields']['eoi_approval_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['eoi_approval_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_approval_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['eoi_approval_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['eoi_approval_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['eoi_approval_end_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EOI_APPROVAL_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="eoi_approval_end_c"  >

<?php if (! $this->_tpl_vars['fields']['eoi_approval_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['eoi_approval_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_approval_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['eoi_approval_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                            </div>
</div>
</div>
<?php endif; ?>





<?php if ($this->_tpl_vars['config']['enable_action_menu'] && $this->_tpl_vars['config']['enable_action_menu'] != false): ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-3" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL10','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-3"  data-id="LBL_DETAILVIEW_PANEL10">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['endorsement_c']['acl'] > 1 || $this->_tpl_vars['fields']['endorsement_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENDORSEMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="endorsement_c"  >

<?php if (! $this->_tpl_vars['fields']['endorsement_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['endorsement_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['endorsement_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['endorsement_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['endorsement_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['endorsement_c']['options'][$this->_tpl_vars['fields']['endorsement_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['endorsement_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['endorsement_start_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENDORSEMENT_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="endorsement_start_c"  >

<?php if (! $this->_tpl_vars['fields']['endorsement_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['endorsement_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['endorsement_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['endorsement_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['endorsement_end_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENDORSEMENT_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="endorsement_end_c"  >

<?php if (! $this->_tpl_vars['fields']['endorsement_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['endorsement_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['endorsement_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                                </div>
</div>
</div>
<?php else: ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-3" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL10','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-3" data-id="LBL_DETAILVIEW_PANEL10">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['endorsement_c']['acl'] > 1 || $this->_tpl_vars['fields']['endorsement_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENDORSEMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="endorsement_c"  >

<?php if (! $this->_tpl_vars['fields']['endorsement_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['endorsement_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['endorsement_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['endorsement_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['endorsement_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['endorsement_c']['options'][$this->_tpl_vars['fields']['endorsement_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['endorsement_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['endorsement_start_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENDORSEMENT_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="endorsement_start_c"  >

<?php if (! $this->_tpl_vars['fields']['endorsement_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['endorsement_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['endorsement_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['endorsement_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['endorsement_end_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENDORSEMENT_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="endorsement_end_c"  >

<?php if (! $this->_tpl_vars['fields']['endorsement_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['endorsement_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['endorsement_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                            </div>
</div>
</div>
<?php endif; ?>





<?php if ($this->_tpl_vars['config']['enable_action_menu'] && $this->_tpl_vars['config']['enable_action_menu'] != false): ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-4" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL11','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-4"  data-id="LBL_DETAILVIEW_PANEL11">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['document_process_c']['acl'] > 1 || $this->_tpl_vars['fields']['document_process_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOCUMENT_PROCESS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="document_process_c"  >

<?php if (! $this->_tpl_vars['fields']['document_process_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['document_process_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['document_process_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['document_process_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['document_process_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['document_process_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['document_process_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['document_process_c']['options'][$this->_tpl_vars['fields']['document_process_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['doc_complete_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['doc_complete_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOC_COMPLETE_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="doc_complete_date_c"  >

<?php if (! $this->_tpl_vars['fields']['doc_complete_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['doc_complete_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['doc_complete_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['doc_complete_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['doc_complete_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['doc_complete_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                                </div>
</div>
</div>
<?php else: ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-4" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL11','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-4" data-id="LBL_DETAILVIEW_PANEL11">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['document_process_c']['acl'] > 1 || $this->_tpl_vars['fields']['document_process_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOCUMENT_PROCESS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="document_process_c"  >

<?php if (! $this->_tpl_vars['fields']['document_process_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['document_process_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['document_process_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['document_process_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['document_process_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['document_process_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['document_process_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['document_process_c']['options'][$this->_tpl_vars['fields']['document_process_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['doc_complete_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['doc_complete_date_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOC_COMPLETE_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="doc_complete_date_c"  >

<?php if (! $this->_tpl_vars['fields']['doc_complete_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['doc_complete_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['doc_complete_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['doc_complete_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['doc_complete_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['doc_complete_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                            </div>
</div>
</div>
<?php endif; ?>





<?php if ($this->_tpl_vars['config']['enable_action_menu'] && $this->_tpl_vars['config']['enable_action_menu'] != false): ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-5" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL12','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-5"  data-id="LBL_DETAILVIEW_PANEL12">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['site_inspection_c']['acl'] > 1 || $this->_tpl_vars['fields']['site_inspection_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SITE_INSPECTION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="site_inspection_c"  >

<?php if (! $this->_tpl_vars['fields']['site_inspection_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['site_inspection_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['site_inspection_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['site_inspection_c']['options'][$this->_tpl_vars['fields']['site_inspection_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['inspection_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['inspection_start_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INSPECTION_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="inspection_start_c"  >

<?php if (! $this->_tpl_vars['fields']['inspection_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['inspection_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['inspection_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['inspection_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['inspection_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['inspection_end_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INSPECTION_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="inspection_end_c"  >

<?php if (! $this->_tpl_vars['fields']['inspection_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['inspection_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['inspection_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['inspection_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                                </div>
</div>
</div>
<?php else: ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-5" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL12','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-5" data-id="LBL_DETAILVIEW_PANEL12">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['site_inspection_c']['acl'] > 1 || $this->_tpl_vars['fields']['site_inspection_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SITE_INSPECTION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="site_inspection_c"  >

<?php if (! $this->_tpl_vars['fields']['site_inspection_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['site_inspection_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['site_inspection_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['site_inspection_c']['options'][$this->_tpl_vars['fields']['site_inspection_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['inspection_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['inspection_start_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INSPECTION_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="inspection_start_c"  >

<?php if (! $this->_tpl_vars['fields']['inspection_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['inspection_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['inspection_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['inspection_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['inspection_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['inspection_end_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INSPECTION_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="inspection_end_c"  >

<?php if (! $this->_tpl_vars['fields']['inspection_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['inspection_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['inspection_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['inspection_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                            </div>
</div>
</div>
<?php endif; ?>





<?php if ($this->_tpl_vars['config']['enable_action_menu'] && $this->_tpl_vars['config']['enable_action_menu'] != false): ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-6" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL13','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-6"  data-id="LBL_DETAILVIEW_PANEL13">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['bid_presentation_c']['acl'] > 1 || $this->_tpl_vars['fields']['bid_presentation_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_BID_PRESENTATION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="bid_presentation_c"  >

<?php if (! $this->_tpl_vars['fields']['bid_presentation_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['bid_presentation_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['options'][$this->_tpl_vars['fields']['bid_presentation_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['presentation_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['presentation_start_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PRESENTATION_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="presentation_start_c"  >

<?php if (! $this->_tpl_vars['fields']['presentation_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['presentation_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['presentation_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['presentation_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['presentation_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['presentation_end_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PRESENTATION_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="presentation_end_c"  >

<?php if (! $this->_tpl_vars['fields']['presentation_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['presentation_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['presentation_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['presentation_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                                </div>
</div>
</div>
<?php else: ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-6" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL13','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-6" data-id="LBL_DETAILVIEW_PANEL13">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['bid_presentation_c']['acl'] > 1 || $this->_tpl_vars['fields']['bid_presentation_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_BID_PRESENTATION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="bid_presentation_c"  >

<?php if (! $this->_tpl_vars['fields']['bid_presentation_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['bid_presentation_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['options'][$this->_tpl_vars['fields']['bid_presentation_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['presentation_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['presentation_start_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PRESENTATION_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="presentation_start_c"  >

<?php if (! $this->_tpl_vars['fields']['presentation_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['presentation_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['presentation_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['presentation_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['presentation_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['presentation_end_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PRESENTATION_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="presentation_end_c"  >

<?php if (! $this->_tpl_vars['fields']['presentation_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['presentation_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['presentation_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['presentation_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                            </div>
</div>
</div>
<?php endif; ?>





<?php if ($this->_tpl_vars['config']['enable_action_menu'] && $this->_tpl_vars['config']['enable_action_menu'] != false): ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-7" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL14','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-7"  data-id="LBL_DETAILVIEW_PANEL14">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['mou_c']['acl'] > 1 || $this->_tpl_vars['fields']['mou_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MOU','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="mou_c"  >

<?php if (! $this->_tpl_vars['fields']['mou_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['mou_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['mou_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['mou_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['mou_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['mou_c']['options'][$this->_tpl_vars['fields']['mou_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['mou_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['mou_start_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MOU_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="mou_start_c"  >

<?php if (! $this->_tpl_vars['fields']['mou_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['mou_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['mou_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['mou_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['mou_end_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MOU_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="mou_end_c"  >

<?php if (! $this->_tpl_vars['fields']['mou_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['mou_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['mou_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                                </div>
</div>
</div>
<?php else: ?>

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-7" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL14','module' => 'Leads'), $this);?>

</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-7" data-id="LBL_DETAILVIEW_PANEL14">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['mou_c']['acl'] > 1 || $this->_tpl_vars['fields']['mou_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MOU','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="mou_c"  >

<?php if (! $this->_tpl_vars['fields']['mou_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['mou_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['mou_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['mou_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['mou_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['mou_c']['options'][$this->_tpl_vars['fields']['mou_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


<?php if ($this->_tpl_vars['fields']['mou_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['mou_start_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MOU_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="mou_start_c"  >

<?php if (! $this->_tpl_vars['fields']['mou_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['mou_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['mou_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['mou_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['mou_end_c']['acl'] > 0): ?>

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MOU_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="mou_end_c"  >

<?php if (! $this->_tpl_vars['fields']['mou_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','print' => false), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['mou_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['mou_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

<?php endif; ?>
</div>
                            </div>
</div>
</div>
<?php endif; ?>






</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){SUGAR.util.buildAccessKeyLabels();});
</script>            <script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
<?php echo '
<script type="text/javascript">

                    var selectTabDetailView = function(tab) {
                        $(\'#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER\').hide();
                        $(\'#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER\').eq(tab).show().addClass(\'active\').addClass(\'in\');
                    };

                    var selectTabOnError = function(tab) {
                        selectTabDetailView(tab);
                        $(\'#content ul.nav.nav-tabs > li\').removeClass(\'active\');
                        $(\'#content ul.nav.nav-tabs > li a\').css(\'color\', \'\');

                        $(\'#content ul.nav.nav-tabs > li\').eq(tab).find(\'a\').first().css(\'color\', \'red\');
                        $(\'#content ul.nav.nav-tabs > li\').eq(tab).addClass(\'active\');

                    };

                    var selectTabOnErrorInputHandle = function(inputHandle) {
                        var tab = $(inputHandle).closest(\'.tab-pane-NOBOOTSTRAPTOGGLER\').attr(\'id\').match(/^detailpanel_(.*)$/)[1];
                        selectTabOnError(tab);
                    };


                    $(function(){
                        $(\'#content ul.nav.nav-tabs > li > a[data-toggle="tab"]\').click(function(e){
                            if(typeof $(this).parent().find(\'a\').first().attr(\'id\') != \'undefined\') {
                                var tab = parseInt($(this).parent().find(\'a\').first().attr(\'id\').match(/^tab(.)*$/)[1]);
                                selectTabDetailView(tab);
                            }
                        });
                    });

                </script>
'; ?>

<?php else: ?>

<?php $this->assign('preForm', "<table width='100%' border='1' cellspacing='0' cellpadding='0' class='converted_account'><tr><td><table width='100%'><tr><td>"); ?>
<?php $this->assign('displayPreform', false); ?>
<?php if (isset ( $this->_tpl_vars['bean']->contact_id ) && ! empty ( $this->_tpl_vars['bean']->contact_id )): ?>
<?php $this->assign('displayPreform', true); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_CONTACT']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_CONTACT']))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "&nbsp;<a href='index.php?module=Contacts&action=DetailView&record=") : smarty_modifier_cat($_tmp, "&nbsp;<a href='index.php?module=Contacts&action=DetailView&record="))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->contact_id) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->contact_id))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "'>") : smarty_modifier_cat($_tmp, "'>"))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->contact_name) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->contact_name))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</a>") : smarty_modifier_cat($_tmp, "</a>"))); ?>
<?php endif; ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</td><td>") : smarty_modifier_cat($_tmp, "</td><td>"))); ?>
<?php if (( $this->_tpl_vars['bean']->converted == '1' ) && isset ( $this->_tpl_vars['bean']->account_id ) && ! empty ( $this->_tpl_vars['bean']->account_id )): ?>
<?php $this->assign('displayPreform', true); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_ACCOUNT']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_ACCOUNT']))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "&nbsp;<a href='index.php?module=Accounts&action=DetailView&record=") : smarty_modifier_cat($_tmp, "&nbsp;<a href='index.php?module=Accounts&action=DetailView&record="))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->account_id) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->account_id))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "'>") : smarty_modifier_cat($_tmp, "'>"))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->account_name) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->account_name))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</a>") : smarty_modifier_cat($_tmp, "</a>"))); ?>
<?php endif; ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</td><td>") : smarty_modifier_cat($_tmp, "</td><td>"))); ?>
<?php if (isset ( $this->_tpl_vars['bean']->opportunity_id ) && ! empty ( $this->_tpl_vars['bean']->opportunity_id )): ?>
<?php $this->assign('displayPreform', true); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_OPP']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['MOD']['LBL_CONVERTED_OPP']))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "&nbsp;<a href='index.php?module=Opportunities&action=DetailView&record=") : smarty_modifier_cat($_tmp, "&nbsp;<a href='index.php?module=Opportunities&action=DetailView&record="))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->opportunity_id) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->opportunity_id))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "'>") : smarty_modifier_cat($_tmp, "'>"))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['bean']->opportunity_name) : smarty_modifier_cat($_tmp, $this->_tpl_vars['bean']->opportunity_name))); ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</a>") : smarty_modifier_cat($_tmp, "</a>"))); ?>
<?php endif; ?>
<?php $this->assign('preForm', ((is_array($_tmp=$this->_tpl_vars['preForm'])) ? $this->_run_mod_handler('cat', true, $_tmp, "</td></tr></table></td></tr></table>") : smarty_modifier_cat($_tmp, "</td></tr></table></td></tr></table>"))); ?>
<?php if ($this->_tpl_vars['displayPreform']): ?>
<?php echo $this->_tpl_vars['preForm']; ?>

<br>
<?php endif; ?>

<script language="javascript">
    <?php echo '
    SUGAR.util.doWhen(function () {
        return $("#contentTable").length == 0;
    }, SUGAR.themes.actionMenu);
    '; ?>

</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="<?php echo $this->_tpl_vars['module']; ?>
">
<input type="hidden" name="record" value="<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="<?php echo $this->_tpl_vars['offset']; ?>
">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
<?php if (! $this->_tpl_vars['config']['enable_action_menu']): ?>
<div class="buttons">
<?php if ($this->_tpl_vars['bean']->aclAccess('edit')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_KEY']; ?>
" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='<?php echo $this->_tpl_vars['id']; ?>
'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="<?php echo $this->_tpl_vars['APP']['LBL_EDIT_BUTTON_LABEL']; ?>
"><?php endif; ?> 
<?php if ($this->_tpl_vars['bean']->aclAccess('edit')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_DUPLICATE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_DUPLICATE_BUTTON_KEY']; ?>
" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.isDuplicate.value=true; _form.action.value='EditView'; _form.return_id.value='<?php echo $this->_tpl_vars['id']; ?>
';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Duplicate" value="<?php echo $this->_tpl_vars['APP']['LBL_DUPLICATE_BUTTON_LABEL']; ?>
" id="duplicate_button"><?php endif; ?> 
<?php if ($this->_tpl_vars['bean']->aclAccess('delete')): ?><input title="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_TITLE']; ?>
" accessKey="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_KEY']; ?>
" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('<?php echo $this->_tpl_vars['APP']['NTC_DELETE_CONFIRMATION']; ?>
')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="<?php echo $this->_tpl_vars['APP']['LBL_DELETE_BUTTON_LABEL']; ?>
" id="delete_button"><?php endif; ?> 
<input class="button hidden" id="send_confirm_opt_in_email" title="<?php echo $this->_tpl_vars['APP']['LBL_SEND_CONFIRM_OPT_IN_EMAIL']; ?>
" onclick="var _form = document.getElementById('formDetailView');_form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
'; _form.action.value='sendConfirmOptInEmail'; _form.module.value='Leads'; _form.module_tab.value='Leads';_form.submit();" name="send_confirm_opt_in_email" disabled="1" type="button" value="<?php echo $this->_tpl_vars['APP']['LBL_SEND_CONFIRM_OPT_IN_EMAIL']; ?>
"/>
<input type="button" class="button" onClick="showPopup();" value="<?php echo $this->_tpl_vars['APP']['LBL_PRINT_AS_PDF']; ?>
"/>
<?php if ($this->_tpl_vars['bean']->aclAccess('detail')): ?><?php if (! empty ( $this->_tpl_vars['fields']['id']['value'] ) && $this->_tpl_vars['isAuditEnabled']): ?><input id="btn_view_change_log" title="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
" class="button" onclick='open_popup("Audit", "600", "400", "&record=<?php echo $this->_tpl_vars['fields']['id']['value']; ?>
&module_name=Leads", true, false,  { "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] } ); return false;' type="button" value="<?php echo $this->_tpl_vars['APP']['LNK_VIEW_CHANGE_LOG']; ?>
"><?php endif; ?><?php endif; ?>
</div>                    <?php endif; ?>
</form>
</div>
</td>
<td align="right" width="20%" class="buttons"><?php echo $this->_tpl_vars['ADMIN_EDIT']; ?>

</td>
</tr>
</table><?php echo smarty_function_sugar_include(array('include' => $this->_tpl_vars['includes']), $this);?>

<div id="Leads_detailview_tabs"
class="yui-navset detailview_tabs"
>

<ul class="yui-nav">

<li><a id="tab0" href="javascript:void(0)"><em><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL5','module' => 'Leads'), $this);?>
</em></a></li>









<li><a id="tab1" href="javascript:void(0)"><em><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EDITVIEW_PANEL1','module' => 'Leads'), $this);?>
</em></a></li>

<li><a id="tab2" href="javascript:void(0)"><em><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL4','module' => 'Leads'), $this);?>
</em></a></li>

<li><a id="tab3" href="javascript:void(0)"><em><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PANEL_ASSIGNMENT','module' => 'Leads'), $this);?>
</em></a></li>
</ul>
<div class="yui-content">
<div id='tabcontent0'>
<div id='detailpanel_1' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL5' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['status']['acl'] > 1 || $this->_tpl_vars['fields']['status']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['status']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STATUS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="status" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['status']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['status']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['status']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['status']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['status']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['status']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['status']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['status']['options'][$this->_tpl_vars['fields']['status']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['reason_c']['acl'] > 1 || $this->_tpl_vars['fields']['reason_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['reason_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_REASON','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="text" field="reason_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['reason_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span class="sugar_field" id="<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['fields']['reason_c']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('url2html', true, $_tmp) : url2html($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['fields']['reason_c']['value'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('escape', true, $_tmp, 'html_entity_decode') : smarty_modifier_escape($_tmp, 'html_entity_decode')))) ? $this->_run_mod_handler('url2html', true, $_tmp) : url2html($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>


<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['start_engagement_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_engagement_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['start_engagement_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_ENGAGEMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="start_engagement_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['start_engagement_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_engagement_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_engagement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_engagement_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_engagement_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_engagement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_engagement_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_engagement_c']['options'][$this->_tpl_vars['fields']['start_engagement_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['start_event_approval_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_event_approval_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['start_event_approval_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_EVENT_APPROVAL','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="start_event_approval_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['start_event_approval_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_event_approval_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_event_approval_c']['options'][$this->_tpl_vars['fields']['start_event_approval_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['start_eoi_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_eoi_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['start_eoi_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_EOI','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="start_eoi_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['start_eoi_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_eoi_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_eoi_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_eoi_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_eoi_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_eoi_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_eoi_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_eoi_c']['options'][$this->_tpl_vars['fields']['start_eoi_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['start_endorsement_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_endorsement_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['start_endorsement_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_ENDORSEMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="start_endorsement_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['start_endorsement_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_endorsement_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_endorsement_c']['options'][$this->_tpl_vars['fields']['start_endorsement_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['start_event_docs_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_event_docs_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['start_event_docs_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_EVENT_DOCS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="start_event_docs_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['start_event_docs_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_event_docs_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_event_docs_c']['options'][$this->_tpl_vars['fields']['start_event_docs_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['start_inspection_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_inspection_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['start_inspection_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_INSPECTION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="start_inspection_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['start_inspection_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_inspection_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_inspection_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_inspection_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_inspection_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_inspection_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_inspection_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_inspection_c']['options'][$this->_tpl_vars['fields']['start_inspection_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['start_presentation_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_presentation_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['start_presentation_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_PRESENTATION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="start_presentation_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['start_presentation_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_presentation_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_presentation_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_presentation_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_presentation_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_presentation_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_presentation_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_presentation_c']['options'][$this->_tpl_vars['fields']['start_presentation_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['start_mou_c']['acl'] > 1 || $this->_tpl_vars['fields']['start_mou_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['start_mou_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_START_MOU','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="start_mou_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['start_mou_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['start_mou_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_mou_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_mou_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_mou_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['start_mou_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['start_mou_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['start_mou_c']['options'][$this->_tpl_vars['fields']['start_mou_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL5").style.display='none';</script>
<?php endif; ?>
<div id='detailpanel_2' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(2);">
<img border="0" id="detailpanel_2_img_hide" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "basic_search.gif"), $this);?>
"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(2);">
<img border="0" id="detailpanel_2_img_show" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "advanced_search.gif"), $this);?>
"></a>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL7','module' => 'Leads'), $this);?>

<script>
      document.getElementById('detailpanel_2').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL7' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['initial_email_c']['acl'] > 1 || $this->_tpl_vars['fields']['initial_email_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['initial_email_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INITIAL_EMAIL','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="initial_email_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['initial_email_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['initial_email_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_email_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_email_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_email_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_email_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_email_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_email_c']['options'][$this->_tpl_vars['fields']['initial_email_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['engaged_by_c']['acl'] > 1 || $this->_tpl_vars['fields']['engaged_by_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['engaged_by_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENGAGED_BY','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="engaged_by_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['engaged_by_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="user_id1_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id1_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['engaged_by_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['sent_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['sent_date_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['sent_date_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SENT_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="sent_date_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['sent_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['sent_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['sent_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['sent_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['sent_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['sent_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['responded_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['responded_date_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['responded_date_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_RESPONDED_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="responded_date_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['responded_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['responded_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['responded_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['responded_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['responded_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['responded_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() { initPanel(2, 'expanded'); }); </script>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL7").style.display='none';</script>
<?php endif; ?>
<div id='detailpanel_3' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(3);">
<img border="0" id="detailpanel_3_img_hide" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "basic_search.gif"), $this);?>
"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(3);">
<img border="0" id="detailpanel_3_img_show" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "advanced_search.gif"), $this);?>
"></a>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL8','module' => 'Leads'), $this);?>

<script>
      document.getElementById('detailpanel_3').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL8' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['initial_event_approval_c']['acl'] > 1 || $this->_tpl_vars['fields']['initial_event_approval_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['initial_event_approval_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INITIAL_EVENT_APPROVAL','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="initial_event_approval_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['initial_event_approval_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['initial_event_approval_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['initial_event_approval_c']['options'][$this->_tpl_vars['fields']['initial_event_approval_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['iea_start_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['iea_start_date_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['iea_start_date_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_IEA_START_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="iea_start_date_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['iea_start_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['iea_start_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_start_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_start_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['iea_start_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['iea_start_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['iea_end_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['iea_end_date_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['iea_end_date_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_IEA_END_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="iea_end_date_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['iea_end_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['iea_end_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_end_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['iea_end_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['iea_end_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['iea_end_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() { initPanel(3, 'expanded'); }); </script>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL8").style.display='none';</script>
<?php endif; ?>
<div id='detailpanel_4' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(4);">
<img border="0" id="detailpanel_4_img_hide" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "basic_search.gif"), $this);?>
"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(4);">
<img border="0" id="detailpanel_4_img_show" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "advanced_search.gif"), $this);?>
"></a>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL9','module' => 'Leads'), $this);?>

<script>
      document.getElementById('detailpanel_4').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL9' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['eoi_c']['acl'] > 1 || $this->_tpl_vars['fields']['eoi_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['eoi_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EOI','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="eoi_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['eoi_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['eoi_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['eoi_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['eoi_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['eoi_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['eoi_c']['options'][$this->_tpl_vars['fields']['eoi_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['eoi_approval_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['eoi_approval_start_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['eoi_approval_start_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EOI_APPROVAL_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="eoi_approval_start_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['eoi_approval_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['eoi_approval_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_approval_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['eoi_approval_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['eoi_approval_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['eoi_approval_end_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['eoi_approval_end_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EOI_APPROVAL_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="eoi_approval_end_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['eoi_approval_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['eoi_approval_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['eoi_approval_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['eoi_approval_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['eoi_approval_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() { initPanel(4, 'expanded'); }); </script>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL9").style.display='none';</script>
<?php endif; ?>
<div id='detailpanel_5' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(5);">
<img border="0" id="detailpanel_5_img_hide" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "basic_search.gif"), $this);?>
"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(5);">
<img border="0" id="detailpanel_5_img_show" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "advanced_search.gif"), $this);?>
"></a>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL10','module' => 'Leads'), $this);?>

<script>
      document.getElementById('detailpanel_5').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL10' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['endorsement_c']['acl'] > 1 || $this->_tpl_vars['fields']['endorsement_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['endorsement_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENDORSEMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="endorsement_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['endorsement_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['endorsement_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['endorsement_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['endorsement_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['endorsement_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['endorsement_c']['options'][$this->_tpl_vars['fields']['endorsement_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['endorsement_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['endorsement_start_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['endorsement_start_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENDORSEMENT_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="endorsement_start_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['endorsement_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['endorsement_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['endorsement_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['endorsement_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['endorsement_end_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['endorsement_end_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ENDORSEMENT_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="endorsement_end_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['endorsement_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['endorsement_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['endorsement_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['endorsement_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['endorsement_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() { initPanel(5, 'expanded'); }); </script>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL10").style.display='none';</script>
<?php endif; ?>
<div id='detailpanel_6' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(6);">
<img border="0" id="detailpanel_6_img_hide" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "basic_search.gif"), $this);?>
"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(6);">
<img border="0" id="detailpanel_6_img_show" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "advanced_search.gif"), $this);?>
"></a>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL11','module' => 'Leads'), $this);?>

<script>
      document.getElementById('detailpanel_6').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL11' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['document_process_c']['acl'] > 1 || $this->_tpl_vars['fields']['document_process_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['document_process_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOCUMENT_PROCESS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="document_process_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['document_process_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['document_process_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['document_process_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['document_process_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['document_process_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['document_process_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['document_process_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['document_process_c']['options'][$this->_tpl_vars['fields']['document_process_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['doc_complete_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['doc_complete_date_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['doc_complete_date_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DOC_COMPLETE_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="doc_complete_date_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['doc_complete_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['doc_complete_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['doc_complete_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['doc_complete_date_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['doc_complete_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['doc_complete_date_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() { initPanel(6, 'expanded'); }); </script>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL11").style.display='none';</script>
<?php endif; ?>
<div id='detailpanel_7' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(7);">
<img border="0" id="detailpanel_7_img_hide" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "basic_search.gif"), $this);?>
"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(7);">
<img border="0" id="detailpanel_7_img_show" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "advanced_search.gif"), $this);?>
"></a>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL12','module' => 'Leads'), $this);?>

<script>
      document.getElementById('detailpanel_7').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL12' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['site_inspection_c']['acl'] > 1 || $this->_tpl_vars['fields']['site_inspection_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['site_inspection_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SITE_INSPECTION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="site_inspection_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['site_inspection_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['site_inspection_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['site_inspection_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['site_inspection_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['site_inspection_c']['options'][$this->_tpl_vars['fields']['site_inspection_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['inspection_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['inspection_start_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['inspection_start_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INSPECTION_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="inspection_start_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['inspection_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['inspection_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['inspection_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['inspection_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['inspection_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['inspection_end_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['inspection_end_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_INSPECTION_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="inspection_end_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['inspection_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['inspection_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['inspection_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['inspection_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['inspection_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() { initPanel(7, 'expanded'); }); </script>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL12").style.display='none';</script>
<?php endif; ?>
<div id='detailpanel_8' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(8);">
<img border="0" id="detailpanel_8_img_hide" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "basic_search.gif"), $this);?>
"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(8);">
<img border="0" id="detailpanel_8_img_show" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "advanced_search.gif"), $this);?>
"></a>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL13','module' => 'Leads'), $this);?>

<script>
      document.getElementById('detailpanel_8').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL13' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['bid_presentation_c']['acl'] > 1 || $this->_tpl_vars['fields']['bid_presentation_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['bid_presentation_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_BID_PRESENTATION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="bid_presentation_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['bid_presentation_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['bid_presentation_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['bid_presentation_c']['options'][$this->_tpl_vars['fields']['bid_presentation_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['presentation_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['presentation_start_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['presentation_start_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PRESENTATION_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="presentation_start_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['presentation_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['presentation_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['presentation_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['presentation_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['presentation_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['presentation_end_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['presentation_end_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PRESENTATION_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="presentation_end_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['presentation_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['presentation_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['presentation_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['presentation_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['presentation_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() { initPanel(8, 'expanded'); }); </script>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL13").style.display='none';</script>
<?php endif; ?>
<div id='detailpanel_9' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(9);">
<img border="0" id="detailpanel_9_img_hide" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "basic_search.gif"), $this);?>
"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(9);">
<img border="0" id="detailpanel_9_img_show" src="<?php echo smarty_function_sugar_getimagepath(array('file' => "advanced_search.gif"), $this);?>
"></a>
<?php echo smarty_function_sugar_translate(array('label' => 'LBL_DETAILVIEW_PANEL14','module' => 'Leads'), $this);?>

<script>
      document.getElementById('detailpanel_9').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL14' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['mou_c']['acl'] > 1 || $this->_tpl_vars['fields']['mou_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['mou_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MOU','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="mou_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['mou_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['mou_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['mou_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['mou_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['mou_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['mou_c']['options'][$this->_tpl_vars['fields']['mou_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['mou_start_c']['acl'] > 1 || $this->_tpl_vars['fields']['mou_start_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['mou_start_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MOU_START','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="mou_start_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['mou_start_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['mou_start_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_start_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_start_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_start_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['mou_start_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['mou_end_c']['acl'] > 1 || $this->_tpl_vars['fields']['mou_end_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['mou_end_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MOU_END','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="datetimecombo" field="mou_end_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['mou_end_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['mou_end_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_end_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['mou_end_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['mou_end_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['mou_end_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() { initPanel(9, 'expanded'); }); </script>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL14").style.display='none';</script>
<?php endif; ?>
</div>    <div id='tabcontent1'>
<div id='detailpanel_10' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_EDITVIEW_PANEL1' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['account_c']['acl'] > 1 || $this->_tpl_vars['fields']['account_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['account_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ACCOUNT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="account_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['account_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['account_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=Accounts&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['account_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="account_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['account_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['account_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['account_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['segment_c']['acl'] > 1 || $this->_tpl_vars['fields']['segment_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['segment_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SEGMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="segment_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['segment_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['segment_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['segment_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['segment_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['segment_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['segment_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['segment_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['segment_c']['options'][$this->_tpl_vars['fields']['segment_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['event_title_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_title_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['event_title_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_TITLE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="varchar" field="event_title_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['event_title_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['event_title_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_title_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_title_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['event_title_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['event_title_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['primary_contact_c']['acl'] > 1 || $this->_tpl_vars['fields']['primary_contact_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['primary_contact_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PRIMARY_CONTACT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="primary_contact_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['primary_contact_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['contact_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=Contacts&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['contact_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="contact_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['contact_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['primary_contact_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['contact_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['event_abbreviation_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_abbreviation_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['event_abbreviation_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_ABBREVIATION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="varchar" field="event_abbreviation_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['event_abbreviation_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strlen ( $this->_tpl_vars['fields']['event_abbreviation_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_abbreviation_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_abbreviation_c']['value']); ?>
<?php endif; ?> 
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['event_abbreviation_c']['name']; ?>
"><?php echo $this->_tpl_vars['fields']['event_abbreviation_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['event_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_date_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['event_date_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="date" field="event_date_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['event_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (strlen ( $this->_tpl_vars['fields']['event_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_date_c']['value']); ?>
<?php endif; ?>
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['event_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['event_end_date_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_end_date_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['event_end_date_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_END_DATE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="date" field="event_end_date_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['event_end_date_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (strlen ( $this->_tpl_vars['fields']['event_end_date_c']['value'] ) <= 0): ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_end_date_c']['default_value']); ?>
<?php else: ?>
<?php $this->assign('value', $this->_tpl_vars['fields']['event_end_date_c']['value']); ?>
<?php endif; ?>
<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['event_end_date_c']['name']; ?>
"><?php echo $this->_tpl_vars['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['packs_c']['acl'] > 1 || $this->_tpl_vars['fields']['packs_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['packs_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PACKS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="int" field="packs_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['packs_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['packs_c']['name']; ?>
">
<?php $this->assign('value', $this->_tpl_vars['fields']['packs_c']['value']); ?>
<?php echo $this->_tpl_vars['value']; ?>

</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['event_days_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_days_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['event_days_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_DAYS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="int" field="event_days_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['event_days_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['event_days_c']['name']; ?>
">
<?php $this->assign('value', $this->_tpl_vars['fields']['event_days_c']['value']); ?>
<?php echo $this->_tpl_vars['value']; ?>

</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>


<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>



<?php if ($this->_tpl_vars['fields']['currency_id']['acl'] > 1 || $this->_tpl_vars['fields']['currency_id']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['currency_id']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CURRENCY','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="currency_id" field="currency_id" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['currency_id']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>

<span id='currency_id_span'>
<?php echo $this->_tpl_vars['fields']['currency_id']['value']; ?>

</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['expected_revenue_c']['acl'] > 1 || $this->_tpl_vars['fields']['expected_revenue_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['expected_revenue_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EXPECTED_REVENUE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="currency" field="expected_revenue_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['expected_revenue_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id='<?php echo $this->_tpl_vars['fields']['expected_revenue_c']['name']; ?>
'>
<?php echo smarty_function_sugar_number_format(array('var' => $this->_tpl_vars['fields']['expected_revenue_c']['value']), $this);?>

</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['gor_commitment_c']['acl'] > 1 || $this->_tpl_vars['fields']['gor_commitment_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['gor_commitment_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_GOR_COMMITMENT','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="currency" field="gor_commitment_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['gor_commitment_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id='<?php echo $this->_tpl_vars['fields']['gor_commitment_c']['name']; ?>
'>
<?php echo smarty_function_sugar_number_format(array('var' => $this->_tpl_vars['fields']['gor_commitment_c']['value']), $this);?>

</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>


<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['lead_source']['acl'] > 1 || $this->_tpl_vars['fields']['lead_source']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['lead_source']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_LEAD_SOURCE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="lead_source" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['lead_source']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['lead_source']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['lead_source']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['lead_source']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['lead_source']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['lead_source']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['lead_source']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['lead_source']['options'][$this->_tpl_vars['fields']['lead_source']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['source_details_c']['acl'] > 1 || $this->_tpl_vars['fields']['source_details_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['source_details_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SOURCE_DETAILS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="dynamicenum" field="source_details_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['source_details_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['source_details_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['source_details_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['source_details_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['source_details_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['source_details_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['source_details_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['source_details_c']['options'][$this->_tpl_vars['fields']['source_details_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>


<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['probability_c']['acl'] > 1 || $this->_tpl_vars['fields']['probability_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['probability_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PROBABILITY ','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="probability_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['probability_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['probability_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['probability_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['probability_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['probability_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['probability_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['probability_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['probability_c']['options'][$this->_tpl_vars['fields']['probability_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['event_coordinator_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_coordinator_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['event_coordinator_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_COORDINATOR','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="event_coordinator_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['event_coordinator_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="user_id2_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['user_id2_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['event_coordinator_c']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>


<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['line_institute_c']['acl'] > 1 || $this->_tpl_vars['fields']['line_institute_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['line_institute_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_LINE_INSTITUTE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="line_institute_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['line_institute_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=LIP_Line_Institutes&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="lip_line_institutes_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['line_institute_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['lip_line_institutes_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['venue_c']['acl'] > 1 || $this->_tpl_vars['fields']['venue_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['venue_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_VENUE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="venue_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['venue_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['vnp_venues_id_c']['value'] )): ?>
<?php ob_start(); ?>index.php?module=VNP_Venues&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['vnp_venues_id_c']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="vnp_venues_id_c" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['vnp_venues_id_c']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['venue_c']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['vnp_venues_id_c']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>


<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['stp_sales_target_leads_1_name']['acl'] > 1 || $this->_tpl_vars['fields']['stp_sales_target_leads_1_name']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['stp_sales_target_leads_1_name']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STP_SALES_TARGET_LEADS_1_FROM_STP_SALES_TARGET_TITLE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="stp_sales_target_leads_1_name" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['stp_sales_target_leads_1_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (! empty ( $this->_tpl_vars['fields']['stp_sales_target_leads_1stp_sales_target_ida']['value'] )): ?>
<?php ob_start(); ?>index.php?module=STP_Sales_Target&action=DetailView&record=<?php echo $this->_tpl_vars['fields']['stp_sales_target_leads_1stp_sales_target_ida']['value']; ?>
<?php $this->_smarty_vars['capture']['default'] = ob_get_contents();  $this->assign('detail_url', ob_get_contents());ob_end_clean(); ?>
<a href="<?php echo smarty_function_sugar_ajax_url(array('url' => $this->_tpl_vars['detail_url']), $this);?>
"><?php endif; ?>
<span id="stp_sales_target_leads_1stp_sales_target_ida" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['stp_sales_target_leads_1stp_sales_target_ida']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['stp_sales_target_leads_1_name']['value']; ?>
</span>
<?php if (! empty ( $this->_tpl_vars['fields']['stp_sales_target_leads_1stp_sales_target_ida']['value'] )): ?></a><?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_EDITVIEW_PANEL1").style.display='none';</script>
<?php endif; ?>
</div>    <div id='tabcontent2'>
<div id='detailpanel_11' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL4' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['event_brief_c']['acl'] > 1 || $this->_tpl_vars['fields']['event_brief_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['event_brief_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_EVENT_BRIEF','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="" type="wysiwyg" field="event_brief_c" width='37.5%' colspan='3' >
<?php if (! $this->_tpl_vars['fields']['event_brief_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<iframe
id="<?php echo $this->_tpl_vars['fields']['event_brief_c']['name']; ?>
"
name="<?php echo $this->_tpl_vars['fields']['event_brief_c']['name']; ?>
"
srcdoc="<?php echo $this->_tpl_vars['fields']['event_brief_c']['value']; ?>
"
style="width:100%;height:500px"
></iframe>
<?php endif; ?>

</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_DETAILVIEW_PANEL4").style.display='none';</script>
<?php endif; ?>
</div>    <div id='tabcontent3'>
<div id='detailpanel_12' class='detail view  detail508 expanded'>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount','start' => 0,'print' => false,'assign' => 'panelFieldCount'), $this);?>

<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_PANEL_ASSIGNMENT' class="panelContainer" cellspacing='<?php echo $this->_tpl_vars['gridline']; ?>
'>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['lead_progress_c']['acl'] > 1 || $this->_tpl_vars['fields']['lead_progress_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['lead_progress_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_LEAD_PROGRESS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="lead_progress_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['lead_progress_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['lead_progress_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['lead_progress_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['lead_progress_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['lead_progress_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['lead_progress_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['lead_progress_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['lead_progress_c']['options'][$this->_tpl_vars['fields']['lead_progress_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['assigned_user_name']['acl'] > 1 || $this->_tpl_vars['fields']['assigned_user_name']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['assigned_user_name']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_ASSIGNED_TO','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="relate" field="assigned_user_name" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['assigned_user_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="assigned_user_id" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['assigned_user_id']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['assigned_user_name']['value']; ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['day_spending_c']['acl'] > 1 || $this->_tpl_vars['fields']['day_spending_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['day_spending_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DAY_SPENDING','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="currency" field="day_spending_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['day_spending_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id='<?php echo $this->_tpl_vars['fields']['day_spending_c']['name']; ?>
'>
<?php echo smarty_function_sugar_number_format(array('var' => $this->_tpl_vars['fields']['day_spending_c']['value']), $this);?>

</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['created_by_name']['acl'] > 1 || $this->_tpl_vars['fields']['created_by_name']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['created_by_name']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="" type="relate" field="created_by_name" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['created_by_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="created_by" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['created_by']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['created_by_name']['value']; ?>
</span>
<?php endif; ?>

</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['modified_by_name']['acl'] > 1 || $this->_tpl_vars['fields']['modified_by_name']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['modified_by_name']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_MODIFIED_NAME','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="" type="relate" field="modified_by_name" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['modified_by_name']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id="modified_user_id" class="sugar_field" data-id-value="<?php echo $this->_tpl_vars['fields']['modified_user_id']['value']; ?>
"><?php echo $this->_tpl_vars['fields']['modified_by_name']['value']; ?>
</span>
<?php endif; ?>

</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['date_entered']['acl'] > 1 || $this->_tpl_vars['fields']['date_entered']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['date_entered']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DATE_ENTERED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="" type="datetime" field="date_entered" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['date_entered']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>

<span id="date_entered" class="sugar_field"><?php echo $this->_tpl_vars['fields']['date_entered']['value']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_BY']; ?>
 <?php echo $this->_tpl_vars['fields']['created_by_name']['value']; ?>
</span>
<?php endif; ?>

</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['date_modified']['acl'] > 1 || $this->_tpl_vars['fields']['date_modified']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['date_modified']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_DATE_MODIFIED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="" type="datetime" field="date_modified" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['date_modified']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>

<span id="date_modified" class="sugar_field"><?php echo $this->_tpl_vars['fields']['date_modified']['value']; ?>
 <?php echo $this->_tpl_vars['APP']['LBL_BY']; ?>
 <?php echo $this->_tpl_vars['fields']['modified_by_name']['value']; ?>
</span>
<?php endif; ?>

</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['client_reponse_details_c']['acl'] > 1 || $this->_tpl_vars['fields']['client_reponse_details_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['client_reponse_details_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_CLIENT_REPONSE_DETAILS','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="text" field="client_reponse_details_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['client_reponse_details_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span class="sugar_field" id="<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['fields']['client_reponse_details_c']['name'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('url2html', true, $_tmp) : url2html($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['fields']['client_reponse_details_c']['value'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')))) ? $this->_run_mod_handler('escape', true, $_tmp, 'html_entity_decode') : smarty_modifier_escape($_tmp, 'html_entity_decode')))) ? $this->_run_mod_handler('url2html', true, $_tmp) : url2html($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['gor_revenue_c']['acl'] > 1 || $this->_tpl_vars['fields']['gor_revenue_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['gor_revenue_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_GOR_REVENUE','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="currency" field="gor_revenue_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['gor_revenue_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id='<?php echo $this->_tpl_vars['fields']['gor_revenue_c']['name']; ?>
'>
<?php echo smarty_function_sugar_number_format(array('var' => $this->_tpl_vars['fields']['gor_revenue_c']['value']), $this);?>

</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['sales_target_c']['acl'] > 1 || $this->_tpl_vars['fields']['sales_target_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['sales_target_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_SALES_TARGET','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="currency" field="sales_target_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['sales_target_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<span id='<?php echo $this->_tpl_vars['fields']['sales_target_c']['name']; ?>
'>
<?php echo smarty_function_sugar_number_format(array('var' => $this->_tpl_vars['fields']['sales_target_c']['value']), $this);?>

</span>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['qualfication_c']['acl'] > 1 || $this->_tpl_vars['fields']['qualfication_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['qualfication_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_QUALFICATION','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="enum" field="qualfication_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['qualfication_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>



<?php if (is_string ( $this->_tpl_vars['fields']['qualfication_c']['options'] )): ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['qualfication_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['qualfication_c']['options']; ?>
">
<?php echo $this->_tpl_vars['fields']['qualfication_c']['options']; ?>

<?php else: ?>
<input type="hidden" class="sugar_field" id="<?php echo $this->_tpl_vars['fields']['qualfication_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['fields']['qualfication_c']['value']; ?>
">
<?php echo $this->_tpl_vars['fields']['qualfication_c']['options'][$this->_tpl_vars['fields']['qualfication_c']['value']]; ?>

<?php endif; ?>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['step2_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step2_created_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['step2_created_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP2_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="bool" field="step2_created_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['step2_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step2_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step2_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step2_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step2_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step2_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step2_created_c']['name']; ?>
" value="$fields.step2_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['step3_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step3_created_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['step3_created_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP3_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="bool" field="step3_created_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['step3_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step3_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step3_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step3_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step3_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step3_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step3_created_c']['name']; ?>
" value="$fields.step3_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['step4_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step4_created_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['step4_created_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP4_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="bool" field="step4_created_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['step4_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step4_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step4_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step4_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step4_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step4_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step4_created_c']['name']; ?>
" value="$fields.step4_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['step5_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step5_created_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['step5_created_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP5_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="bool" field="step5_created_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['step5_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step5_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step5_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step5_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step5_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step5_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step5_created_c']['name']; ?>
" value="$fields.step5_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['step6_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step6_created_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['step6_created_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP6_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="bool" field="step6_created_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['step6_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step6_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step6_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step6_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step6_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step6_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step6_created_c']['name']; ?>
" value="$fields.step6_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['step7_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step7_created_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['step7_created_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP7_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="bool" field="step7_created_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['step7_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step7_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step7_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step7_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step7_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step7_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step7_created_c']['name']; ?>
" value="$fields.step7_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['step8_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['step8_created_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['step8_created_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP8_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="bool" field="step8_created_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['step8_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step8_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step8_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step8_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step8_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step8_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step8_created_c']['name']; ?>
" value="$fields.step8_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>



<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['step3_eoi_chk_c']['acl'] > 1 || $this->_tpl_vars['fields']['step3_eoi_chk_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['step3_eoi_chk_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP3_EOI_CHK','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="bool" field="step3_eoi_chk_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['step3_eoi_chk_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step3_eoi_chk_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step3_eoi_chk_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step3_eoi_chk_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step3_eoi_chk_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step3_eoi_chk_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step3_eoi_chk_c']['name']; ?>
" value="$fields.step3_eoi_chk_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>


<?php if ($this->_tpl_vars['fields']['step4_endo_chk_c']['acl'] > 1 || $this->_tpl_vars['fields']['step4_endo_chk_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['step4_endo_chk_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_STEP4_ENDO_CHK','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="bool" field="step4_endo_chk_c" width='37.5%'  >
<?php if (! $this->_tpl_vars['fields']['step4_endo_chk_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['step4_endo_chk_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['step4_endo_chk_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['step4_endo_chk_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['step4_endo_chk_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['step4_endo_chk_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['step4_endo_chk_c']['name']; ?>
" value="$fields.step4_endo_chk_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
<?php echo smarty_function_counter(array('name' => 'fieldsUsed','start' => 0,'print' => false,'assign' => 'fieldsUsed'), $this);?>

<?php echo smarty_function_counter(array('name' => 'fieldsHidden','start' => 0,'print' => false,'assign' => 'fieldsHidden'), $this);?>

<?php ob_start(); ?>
<tr>

<?php if ($this->_tpl_vars['fields']['project_created_c']['acl'] > 1 || $this->_tpl_vars['fields']['project_created_c']['acl'] > 0): ?>

<?php echo smarty_function_counter(array('name' => 'fieldsUsed'), $this);?>

<td width='12.5%' scope="col">
<?php if (! $this->_tpl_vars['fields']['project_created_c']['hidden']): ?>
<?php ob_start(); ?><?php echo smarty_function_sugar_translate(array('label' => 'LBL_PROJECT_CREATED','module' => 'Leads'), $this);?>
<?php $this->_smarty_vars['capture']['label'] = ob_get_contents();  $this->assign('label', ob_get_contents());ob_end_clean(); ?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['label'])) ? $this->_run_mod_handler('strip_semicolon', true, $_tmp) : smarty_modifier_strip_semicolon($_tmp)); ?>
:

<?php else: ?>
<?php echo smarty_function_counter(array('name' => 'fieldsHidden'), $this);?>


<?php endif; ?>
</td>

<td class="inlineEdit" type="bool" field="project_created_c" width='37.5%' colspan='3' >
<?php if (! $this->_tpl_vars['fields']['project_created_c']['hidden']): ?>
<?php echo smarty_function_counter(array('name' => 'panelFieldCount'), $this);?>


<?php if (strval ( $this->_tpl_vars['fields']['project_created_c']['value'] ) == '1' || strval ( $this->_tpl_vars['fields']['project_created_c']['value'] ) == 'yes' || strval ( $this->_tpl_vars['fields']['project_created_c']['value'] ) == 'on'): ?> 
<?php $this->assign('checked', 'CHECKED'); ?>
<?php $this->assign('hidden_val', '1'); ?>
<?php else: ?>
<?php $this->assign('checked', ""); ?>
<?php $this->assign('hidden_val', ""); ?>
<?php endif; ?>
<?php if ($_REQUEST['action'] == 'EditView'): ?>
<input type="hidden" name="<?php echo $this->_tpl_vars['fields']['project_created_c']['name']; ?>
" value="<?php echo $this->_tpl_vars['hidden_val']; ?>
">
<?php endif; ?>
<input type="checkbox" class="checkbox" name="<?php echo $this->_tpl_vars['fields']['project_created_c']['name']; ?>
" id="<?php echo $this->_tpl_vars['fields']['project_created_c']['name']; ?>
" value="$fields.project_created_c.value" disabled="true" <?php echo $this->_tpl_vars['checked']; ?>
>
<?php endif; ?>

<div class="inlineEditIcon"> <?php echo smarty_function_sugar_getimage(array('name' => "inline_edit_icon.svg",'attr' => 'border="0" ','alt' => ($this->_tpl_vars['alt_edit'])), $this);?>
</div>			</td>

<?php else: ?>
<td>&nbsp;</td><td>&nbsp;</td>
<?php endif; ?>

</tr>
<?php $this->_smarty_vars['capture']['tr'] = ob_get_contents();  $this->assign('tableRow', ob_get_contents());ob_end_clean(); ?>
<?php if ($this->_tpl_vars['fieldsUsed'] > 0 && $this->_tpl_vars['fieldsUsed'] != $this->_tpl_vars['fieldsHidden']): ?>
<?php echo $this->_tpl_vars['tableRow']; ?>

<?php endif; ?>
</table>
</div>
<?php if ($this->_tpl_vars['panelFieldCount'] == 0): ?>
<script>document.getElementById("LBL_PANEL_ASSIGNMENT").style.display='none';</script>
<?php endif; ?>
</div>
</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){SUGAR.util.buildAccessKeyLabels();});
</script><script type='text/javascript' src='<?php echo smarty_function_sugar_getjspath(array('file' => 'include/javascript/popup_helper.js'), $this);?>
'></script>
<script type="text/javascript" src="<?php echo smarty_function_sugar_getjspath(array('file' => 'cache/include/javascript/sugar_grp_yui_widgets.js'), $this);?>
"></script>
<script type="text/javascript">
var Leads_detailview_tabs = new YAHOO.widget.TabView("Leads_detailview_tabs");
Leads_detailview_tabs.selectTab(0);
</script>
<script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
<?php endif; ?>