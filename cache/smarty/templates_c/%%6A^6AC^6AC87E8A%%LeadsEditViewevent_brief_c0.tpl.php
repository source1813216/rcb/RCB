<?php /* Smarty version 2.6.31, created on 2022-12-14 18:24:18
         compiled from cache/modules/AOW_WorkFlow/LeadsEditViewevent_brief_c0.tpl */ ?>

<link rel="stylesheet" type="text/css" href="include/SugarFields/Fields/Wysiwyg/css/wysiwyg-editview.css" />

<?php if (empty ( $this->_tpl_vars['fields']['event_brief_c']['value'] )): ?>
    <?php $this->assign('value', $this->_tpl_vars['fields']['event_brief_c']['default_value']); ?>
<?php else: ?>
    <?php $this->assign('value', $this->_tpl_vars['fields']['event_brief_c']['value']); ?>
<?php endif; ?>

    <?php echo '
        <script type="text/javascript" language="Javascript"> tinyMCE.init({"convert_urls":false,"valid_children":"+body[style]","height":300,"width":"100%","theme":"modern","theme_advanced_toolbar_align":"left","theme_advanced_toolbar_location":"top","theme_advanced_buttons1":"","theme_advanced_buttons2":"","theme_advanced_buttons3":"","strict_loading_mode":true,"mode":"exact","language":"en","plugins":"print code preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern ","elements":"# #event_brief_c","extended_valid_elements":"style[dir|lang|media|title|type],hr[class|width|size|noshade],@[class|style]","content_css":"vendor\\/tinymce\\/tinymce\\/skins\\/lightgray\\/content.min.css","apply_source_formatting":false,"cleanup_on_startup":true,"relative_urls":false,"selector":"# #event_brief_c","toolbar1":"formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat"});</script>
    '; ?>

    <div class="wysiwyg">
        <textarea
            id="<?php echo $this->_tpl_vars['fields']['event_brief_c']['name']; ?>
"
            name="<?php echo $this->_tpl_vars['fields']['event_brief_c']['name']; ?>
"
            rows="4"
            cols="60"
            title=''
            tabindex="1"
            
        ><?php echo $this->_tpl_vars['value']; ?>
</textarea>
    </div>

<br />