
{php}
global $current_user;
$this->_tpl_vars['current_user'] = $current_user->id;
$this->_tpl_vars['is_admin_user'] = $current_user->is_admin;
$this->_tpl_vars['current_theme'] = $current_user->getPreference('user_theme');
{/php}
{if $current_theme == 'SuiteP'}

<script>
    {literal}
    $(document).ready(function(){
	    $("ul.clickMenu").each(function(index, node){
	        $(node).sugarActionMenu();
	    });

        if($('.edit-view-pagination').children().length == 0) $('.saveAndContinue').remove();
    });
    {/literal}
</script>
<div class="clear"></div>
<form action="index.php" method="POST" name="{$form_name}" id="{$form_id}" {$enctype}>
<div class="edit-view-pagination-mobile-container">
<div class="edit-view-pagination edit-view-mobile-pagination">
{$PAGINATION}
</div>
</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit">
<tr>
<td class="buttons">
<input type="hidden" name="module" value="{$module}">
{if isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true"}
<input type="hidden" name="record" value="">
<input type="hidden" name="duplicateSave" value="true">
<input type="hidden" name="duplicateId" value="{$fields.id.value}">
{else}
<input type="hidden" name="record" value="{$fields.id.value}">
{/if}
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="action">
<input type="hidden" name="return_module" value="{$smarty.request.return_module}">
<input type="hidden" name="return_action" value="{$smarty.request.return_action}">
<input type="hidden" name="return_id" value="{$smarty.request.return_id}">
<input type="hidden" name="module_tab"> 
<input type="hidden" name="contact_role">
{if (!empty($smarty.request.return_module) || !empty($smarty.request.relate_to)) && !(isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true")}
<input type="hidden" name="relate_to" value="{if $smarty.request.return_relationship}{$smarty.request.return_relationship}{elseif $smarty.request.relate_to && empty($smarty.request.from_dcmenu)}{$smarty.request.relate_to}{elseif empty($isDCForm) && empty($smarty.request.from_dcmenu)}{$smarty.request.return_module}{/if}">
<input type="hidden" name="relate_id" value="{$smarty.request.return_id}">
{/if}
<input type="hidden" name="offset" value="{$offset}">
{assign var='place' value="_HEADER"} <!-- to be used for id for buttons with custom code in def files-->
<div class="buttons">
{if $bean->aclAccess("save")}<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('EditView'); {if $isDuplicate}_form.return_id.value=''; {/if}_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" id="SAVE">{/if} 
{if !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($smarty.request.return_id))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" type="button" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($fields.id.value))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && empty($fields.id.value)) && empty($smarty.request.return_id)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && !empty($smarty.request.return_module)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action={$smarty.request.return_action}&module={$smarty.request.return_module|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif empty($smarty.request.return_action) || empty($smarty.request.return_id) && !empty($fields.id.value)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=AOS_Quotes'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {else}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {/if}
{if $showVCRControl}
<button type="button" id="save_and_continue" class="button saveAndContinue" title="{$app_strings.LBL_SAVE_AND_CONTINUE}" onClick="SUGAR.saveAndContinue(this);">
{$APP.LBL_SAVE_AND_CONTINUE}
</button>
{/if}
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AOS_Quotes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>
</td>
<td align='right' class="edit-view-pagination-desktop-container">
<div class="edit-view-pagination edit-view-pagination-desktop">
{$PAGINATION}
</div>
</td>
</tr>
</table>
{sugar_include include=$includes}
<div id="EditView_tabs">

<ul class="nav nav-tabs">


<li role="presentation" class="active">
<a id="tab0" data-toggle="tab" class="hidden-xs">
{sugar_translate label='LBL_ACCOUNT_INFORMATION' module='AOS_Quotes'}
</a>


<!-- Counting Tabs 5-->
<a id="xstab0" href="#" class="visible-xs first-tab-xs dropdown-toggle" data-toggle="dropdown">
{sugar_translate label='LBL_ACCOUNT_INFORMATION' module='AOS_Quotes'}
</a>
<ul id="first-tab-menu-xs" class="dropdown-menu">
<li role="presentation">
<a id="tab0" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-0');">
{sugar_translate label='LBL_ACCOUNT_INFORMATION' module='AOS_Quotes'}
</a>
</li>
<li role="presentation">
<a id="tab1" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-1');">
{sugar_translate label='LBL_EDITVIEW_PANEL3' module='AOS_Quotes'}
</a>
</li>
<li role="presentation">
<a id="tab2" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-2');">
{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes'}
</a>
</li>
<li role="presentation">
<a id="tab3" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-3');">
{sugar_translate label='LBL_EDITVIEW_PANEL2' module='AOS_Quotes'}
</a>
</li>
<li role="presentation">
<a id="tab4" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-4');">
{sugar_translate label='LBL_EDITVIEW_PANEL4' module='AOS_Quotes'}
</a>
</li>
<li role="presentation">
<a id="tab5" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-5');">
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='AOS_Quotes'}
</a>
</li>
</ul>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab1"  data-toggle="tab">
{sugar_translate label='LBL_EDITVIEW_PANEL3' module='AOS_Quotes'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab2"  data-toggle="tab">
{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab3"  data-toggle="tab">
{sugar_translate label='LBL_EDITVIEW_PANEL2' module='AOS_Quotes'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab4"  data-toggle="tab">
{sugar_translate label='LBL_EDITVIEW_PANEL4' module='AOS_Quotes'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab5"  data-toggle="tab">
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='AOS_Quotes'}
</a>
</li>
</ul>
<div class="clearfix"></div>
<div class="tab-content">

<div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-0'>


{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}
    
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.venue_c.acl > 1 || ($showDetailData && $fields.venue_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_VENUE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_VENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="venue_c"  >

{if $fields.venue_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.venue_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.venue_c.name}" size="" value="{$fields.venue_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.venue_c.id_name}" 
id="{$fields.venue_c.id_name}" 
value="{$fields.vnp_venues_id_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.venue_c.name}" id="btn_{$fields.venue_c.name}" tabindex="0" title="{sugar_translate label="LBL_SELECT_BUTTON_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_SELECT_BUTTON_LABEL"}"
onclick='open_popup(
"{$fields.venue_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"vnp_venues_id_c","name":"venue_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.venue_c.name}" id="btn_clr_{$fields.venue_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_RELATE_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.venue_c.name}', '{$fields.venue_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_RELATE_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.venue_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{if !empty($fields.vnp_venues_id_c.value)}
{capture assign="detail_url"}index.php?module=VNP_Venues&action=DetailView&record={$fields.vnp_venues_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="vnp_venues_id_c" class="sugar_field" data-id-value="{$fields.vnp_venues_id_c.value}">{$fields.venue_c.value}</span>
{if !empty($fields.vnp_venues_id_c.value)}</a>{/if}
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.venue_c.acl > 1 || ($showDetailData && $fields.venue_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.event_date_c.acl > 1 || ($showDetailData && $fields.event_date_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_EVENT_DATE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="date" field="event_date_c"  >

{if $fields.event_date_c.acl > 1}

{counter name="panelFieldCount" print=false}

<span class="dateTime">
{assign var=date_value value=$fields.event_date_c.value }
<input class="date_input" autocomplete="off" type="text" name="{$fields.event_date_c.name}" id="{$fields.event_date_c.name}" value="{$date_value}" title=''  tabindex='0'    size="11" maxlength="10" >
<button type="button" id="{$fields.event_date_c.name}_trigger" class="btn btn-danger" onclick="return false;"><span class="suitepicon suitepicon-module-calendar" alt="{$APP.LBL_ENTER_DATE}"></span></button>
</span>
<script type="text/javascript">
Calendar.setup ({ldelim}
inputField : "{$fields.event_date_c.name}",
form : "EditView",
ifFormat : "{$CALENDAR_FORMAT}",
daFormat : "{$CALENDAR_FORMAT}",
button : "{$fields.event_date_c.name}_trigger",
singleClick : true,
dateStr : "{$date_value}",
startWeekday: {$CALENDAR_FDOW|default:'0'},
step : 1,
weekNumbers:false
{rdelim}
);
</script>

{else}


{if strlen($fields.event_date_c.value) <= 0}
{assign var="value" value=$fields.event_date_c.default_value }
{else}
{assign var="value" value=$fields.event_date_c.value }
{/if}
<span class="sugar_field" id="{$fields.event_date_c.name}">{$value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.event_date_c.acl > 1 || ($showDetailData && $fields.event_date_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-12 edit-view-row-item">


{if $fields.event_brief_c.acl > 1 || ($showDetailData && $fields.event_brief_c.acl > 0)}


<div class="col-xs-12 col-sm-2 label" data-label="LBL_EVENT_BRIEF">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_BRIEF' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="wysiwyg" field="event_brief_c" colspan='3' >

{if $fields.event_brief_c.acl > 1}

{counter name="panelFieldCount" print=false}

<link rel="stylesheet" type="text/css" href="include/SugarFields/Fields/Wysiwyg/css/wysiwyg-editview.css" />
{if empty($fields.event_brief_c.value)}
{assign var="value" value=$fields.event_brief_c.default_value }
{else}
{assign var="value" value=$fields.event_brief_c.value }
{/if}
{literal}
<script type="text/javascript" language="Javascript"> tinyMCE.init({"convert_urls":false,"valid_children":"+body[style]","height":300,"width":"100%","theme":"modern","theme_advanced_toolbar_align":"left","theme_advanced_toolbar_location":"top","theme_advanced_buttons1":"","theme_advanced_buttons2":"","theme_advanced_buttons3":"","strict_loading_mode":true,"mode":"exact","language":"en","plugins":"print code preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern ","elements":"#EditView #event_brief_c","extended_valid_elements":"style[dir|lang|media|title|type],hr[class|width|size|noshade],@[class|style]","content_css":"vendor\/tinymce\/tinymce\/skins\/lightgray\/content.min.css","apply_source_formatting":false,"cleanup_on_startup":true,"relative_urls":false,"selector":"#EditView #event_brief_c","toolbar1":"formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat"});</script>
{/literal}
<div class="wysiwyg">
<textarea
            id="{$fields.event_brief_c.name}"
            name="{$fields.event_brief_c.name}"
            rows="4"
            cols="60"
            title=''
            tabindex="0"
            
        >{$value}</textarea>
</div>
<br />

{else}

<iframe
id="{$fields.event_brief_c.name}"
name="{$fields.event_brief_c.name}"
srcdoc="{$fields.event_brief_c.value}"
style="width:100%;height:500px"
></iframe>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.event_brief_c.acl > 1 || ($showDetailData && $fields.event_brief_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
</div>            </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-1'>


{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}
    
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.manually_calculate_c.acl > 1 || ($showDetailData && $fields.manually_calculate_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MANUALLY_CALCULATE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MANUALLY_CALCULATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="manually_calculate_c"  >

{if $fields.manually_calculate_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if strval($fields.manually_calculate_c.value) == "1" || strval($fields.manually_calculate_c.value) == "yes" || strval($fields.manually_calculate_c.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.manually_calculate_c.name}" value="0"> 
<input type="checkbox" id="{$fields.manually_calculate_c.name}" 
name="{$fields.manually_calculate_c.name}" 
value="1" title='' tabindex="0" {$checked} >

{else}

{if strval($fields.manually_calculate_c.value) == "1" || strval($fields.manually_calculate_c.value) == "yes" || strval($fields.manually_calculate_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.manually_calculate_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.manually_calculate_c.name}" id="{$fields.manually_calculate_c.name}" value="$fields.manually_calculate_c.value" disabled="true" {$checked}>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.manually_calculate_c.acl > 1 || ($showDetailData && $fields.manually_calculate_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


</div>


<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.no_of_delegates_c.acl > 1 || ($showDetailData && $fields.no_of_delegates_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_NO_OF_DELEGATES">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_NO_OF_DELEGATES' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="int" field="no_of_delegates_c"  >

{if $fields.no_of_delegates_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if strlen($fields.no_of_delegates_c.value) <= 0}
{assign var="value" value=$fields.no_of_delegates_c.default_value }
{else}
{assign var="value" value=$fields.no_of_delegates_c.value }
{/if}  
<input type='text' name='{$fields.no_of_delegates_c.name}' 
id='{$fields.no_of_delegates_c.name}' size='30' maxlength='5' value='{$value}' title='' tabindex='0'    >

{else}

<span class="sugar_field" id="{$fields.no_of_delegates_c.name}">
{assign var="value" value=$fields.no_of_delegates_c.value }
{$value}
</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.no_of_delegates_c.acl > 1 || ($showDetailData && $fields.no_of_delegates_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.event_days_c.acl > 1 || ($showDetailData && $fields.event_days_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_EVENT_DAYS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DAYS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="int" field="event_days_c"  >

{if $fields.event_days_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if strlen($fields.event_days_c.value) <= 0}
{assign var="value" value=$fields.event_days_c.default_value }
{else}
{assign var="value" value=$fields.event_days_c.value }
{/if}  
<input type='text' name='{$fields.event_days_c.name}' 
id='{$fields.event_days_c.name}' size='30' maxlength='3' value='{$value}' title='' tabindex='0'    >

{else}

<span class="sugar_field" id="{$fields.event_days_c.name}">
{assign var="value" value=$fields.event_days_c.value }
{$value}
</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.event_days_c.acl > 1 || ($showDetailData && $fields.event_days_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.additional_revenue_c.acl > 1 || ($showDetailData && $fields.additional_revenue_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_ADDITIONAL_REVENUE ">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_ADDITIONAL_REVENUE ' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="additional_revenue_c"  >

{if $fields.additional_revenue_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if strlen($fields.additional_revenue_c.value) <= 0}
{assign var="value" value=$fields.additional_revenue_c.default_value }
{else}
{assign var="value" value=$fields.additional_revenue_c.value }
{/if}  
<input type='text' name='{$fields.additional_revenue_c.name}' 
id='{$fields.additional_revenue_c.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>

{else}

<span id='{$fields.additional_revenue_c.name}'>
{sugar_number_format var=$fields.additional_revenue_c.value }
</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.additional_revenue_c.acl > 1 || ($showDetailData && $fields.additional_revenue_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.revenue_description_c.acl > 1 || ($showDetailData && $fields.revenue_description_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_REVENUE_DESCRIPTION">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_REVENUE_DESCRIPTION' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="text" field="revenue_description_c"  >

{if $fields.revenue_description_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if empty($fields.revenue_description_c.value)}
{assign var="value" value=$fields.revenue_description_c.default_value }
{else}
{assign var="value" value=$fields.revenue_description_c.value }
{/if}
<textarea  id='{$fields.revenue_description_c.name}' name='{$fields.revenue_description_c.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

<span class="sugar_field" id="{$fields.revenue_description_c.name|escape:'html'|url2html|nl2br}">{$fields.revenue_description_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.revenue_description_c.acl > 1 || ($showDetailData && $fields.revenue_description_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.expected_revenue_c.acl > 1 || ($showDetailData && $fields.expected_revenue_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_EXPECTED_REVENUE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_EXPECTED_REVENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="expected_revenue_c"  >

{if $fields.expected_revenue_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if strlen($fields.expected_revenue_c.value) <= 0}
{assign var="value" value=$fields.expected_revenue_c.default_value }
{else}
{assign var="value" value=$fields.expected_revenue_c.value }
{/if}  
<input type='text' name='{$fields.expected_revenue_c.name}' 
id='{$fields.expected_revenue_c.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>

{else}

<span id='{$fields.expected_revenue_c.name}'>
{sugar_number_format var=$fields.expected_revenue_c.value }
</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.expected_revenue_c.acl > 1 || ($showDetailData && $fields.expected_revenue_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


</div>


<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.gor_revenue_c.acl > 1 || ($showDetailData && $fields.gor_revenue_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_GOR_REVENUE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_REVENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="gor_revenue_c"  >

{if $fields.gor_revenue_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if strlen($fields.gor_revenue_c.value) <= 0}
{assign var="value" value=$fields.gor_revenue_c.default_value }
{else}
{assign var="value" value=$fields.gor_revenue_c.value }
{/if}  
<input type='text' name='{$fields.gor_revenue_c.name}' 
id='{$fields.gor_revenue_c.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>

{else}

<span id='{$fields.gor_revenue_c.name}'>
{sugar_number_format var=$fields.gor_revenue_c.value }
</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.gor_revenue_c.acl > 1 || ($showDetailData && $fields.gor_revenue_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.gor_commitment_c.acl > 1 || ($showDetailData && $fields.gor_commitment_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_GOR_COMMITMENT">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_COMMITMENT' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="currency" field="gor_commitment_c"  >

{if $fields.gor_commitment_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if strlen($fields.gor_commitment_c.value) <= 0}
{assign var="value" value=$fields.gor_commitment_c.default_value }
{else}
{assign var="value" value=$fields.gor_commitment_c.value }
{/if}  
<input type='text' name='{$fields.gor_commitment_c.name}' 
id='{$fields.gor_commitment_c.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>

{else}

<span id='{$fields.gor_commitment_c.name}'>
{sugar_number_format var=$fields.gor_commitment_c.value }
</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.gor_commitment_c.acl > 1 || ($showDetailData && $fields.gor_commitment_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>
</div>            </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-2'>


{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}
    
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.currency_id.acl > 1 || ($showDetailData && $fields.currency_id.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CURRENCY">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_CURRENCY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="id" field="currency_id"  >

{if $fields.currency_id.acl > 1}

{counter name="panelFieldCount" print=false}
<span id='currency_id_span'>
{$fields.currency_id.value}</span>

{else}
<span id='currency_id_span'>
{$fields.currency_id.value}
</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.currency_id.acl > 1 || ($showDetailData && $fields.currency_id.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


</div>


<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-12 edit-view-row-item">


{if $fields.line_items.acl > 1 || ($showDetailData && $fields.line_items.acl > 0)}


<div class="col-xs-12 col-sm-2 label" data-label="LBL_LINE_ITEMS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="function" field="line_items" colspan='3' >

{if $fields.line_items.acl > 1}

{counter name="panelFieldCount" print=false}
<span id='line_items_span'>
{$fields.line_items.value}</span>

{else}
<span id='line_items_span'>
{$fields.line_items.value}
</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.line_items.acl > 1 || ($showDetailData && $fields.line_items.acl > 0)}

</div>

{/if}

<div class="clear"></div>



<div class="col-xs-12 col-sm-12 edit-view-row-item">


</div>


<div class="clear"></div>
</div>            </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-3'>


{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}
    
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-12 edit-view-row-item">


{if $fields.recommendation_c.acl > 1 || ($showDetailData && $fields.recommendation_c.acl > 0)}


<div class="col-xs-12 col-sm-2 label" data-label="LBL_RECOMMENDATION">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_RECOMMENDATION' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="wysiwyg" field="recommendation_c" colspan='3' >

{if $fields.recommendation_c.acl > 1}

{counter name="panelFieldCount" print=false}

<link rel="stylesheet" type="text/css" href="include/SugarFields/Fields/Wysiwyg/css/wysiwyg-editview.css" />
{if empty($fields.recommendation_c.value)}
{assign var="value" value=$fields.recommendation_c.default_value }
{else}
{assign var="value" value=$fields.recommendation_c.value }
{/if}
{literal}
<script type="text/javascript" language="Javascript"> tinyMCE.init({"convert_urls":false,"valid_children":"+body[style]","height":300,"width":"100%","theme":"modern","theme_advanced_toolbar_align":"left","theme_advanced_toolbar_location":"top","theme_advanced_buttons1":"","theme_advanced_buttons2":"","theme_advanced_buttons3":"","strict_loading_mode":true,"mode":"exact","language":"en","plugins":"print code preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern ","elements":"#EditView #recommendation_c","extended_valid_elements":"style[dir|lang|media|title|type],hr[class|width|size|noshade],@[class|style]","content_css":"vendor\/tinymce\/tinymce\/skins\/lightgray\/content.min.css","apply_source_formatting":false,"cleanup_on_startup":true,"relative_urls":false,"selector":"#EditView #recommendation_c","toolbar1":"formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat"});</script>
{/literal}
<div class="wysiwyg">
<textarea
            id="{$fields.recommendation_c.name}"
            name="{$fields.recommendation_c.name}"
            rows="4"
            cols="60"
            title=''
            tabindex="0"
            
        >{$value}</textarea>
</div>
<br />

{else}

<iframe
id="{$fields.recommendation_c.name}"
name="{$fields.recommendation_c.name}"
srcdoc="{$fields.recommendation_c.value}"
style="width:100%;height:500px"
></iframe>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.recommendation_c.acl > 1 || ($showDetailData && $fields.recommendation_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
</div>            </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-4'>


{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}
    
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.send_for_approval_c.acl > 1 || ($showDetailData && $fields.send_for_approval_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_SEND_FOR_APPROVAL">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_SEND_FOR_APPROVAL' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="send_for_approval_c"  >

{if $fields.send_for_approval_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if strval($fields.send_for_approval_c.value) == "1" || strval($fields.send_for_approval_c.value) == "yes" || strval($fields.send_for_approval_c.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.send_for_approval_c.name}" value="0"> 
<input type="checkbox" id="{$fields.send_for_approval_c.name}" 
name="{$fields.send_for_approval_c.name}" 
value="1" title='' tabindex="0" {$checked} >

{else}

{if strval($fields.send_for_approval_c.value) == "1" || strval($fields.send_for_approval_c.value) == "yes" || strval($fields.send_for_approval_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.send_for_approval_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.send_for_approval_c.name}" id="{$fields.send_for_approval_c.name}" value="$fields.send_for_approval_c.value" disabled="true" {$checked}>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.send_for_approval_c.acl > 1 || ($showDetailData && $fields.send_for_approval_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.approval_status.acl > 1 || ($showDetailData && $fields.approval_status.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_APPROVAL_STATUS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_STATUS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="approval_status"  >

{if $fields.approval_status.acl > 1}

{counter name="panelFieldCount" print=false}

{if !isset($config.enable_autocomplete) || $config.enable_autocomplete==false}
<select name="{$fields.approval_status.name}" 
id="{$fields.approval_status.name}" 
title=''       
>
{if isset($fields.approval_status.value) && $fields.approval_status.value != ''}
{html_options options=$fields.approval_status.options selected=$fields.approval_status.value}
{else}
{html_options options=$fields.approval_status.options selected=$fields.approval_status.default}
{/if}
</select>
{else}
{assign var="field_options" value=$fields.approval_status.options }
{capture name="field_val"}{$fields.approval_status.value}{/capture}
{assign var="field_val" value=$smarty.capture.field_val}
{capture name="ac_key"}{$fields.approval_status.name}{/capture}
{assign var="ac_key" value=$smarty.capture.ac_key}
<select style='display:none' name="{$fields.approval_status.name}" 
id="{$fields.approval_status.name}" 
title=''          
>
{if isset($fields.approval_status.value) && $fields.approval_status.value != ''}
{html_options options=$fields.approval_status.options selected=$fields.approval_status.value}
{else}
{html_options options=$fields.approval_status.options selected=$fields.approval_status.default}
{/if}
</select>
<input
id="{$fields.approval_status.name}-input"
name="{$fields.approval_status.name}-input"
size="30"
value="{$field_val|lookup:$field_options}"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="{sugar_getimagepath file="id-ff-down.png"}" id="{$fields.approval_status.name}-image"></button><button type="button"
id="btn-clear-{$fields.approval_status.name}-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '{$fields.approval_status.name}-input', '{$fields.approval_status.name}');sync_{$fields.approval_status.name}()"><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
{literal}
<script>
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal} = [];
	{/literal}

			{literal}
		(function (){
			var selectElem = document.getElementById("{/literal}{$fields.approval_status.name}{literal}");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:''};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use('datasource', 'datasource-jsonschema',function (Y) {
				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value=='' && selectElem.options[i].innerHTML==''))
				    			ret.push({'key':selectElem.options[i].value,'text':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		{/literal}
	
	{literal}
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	{/literal}
			
	SUGAR.AutoComplete.{$ac_key}.inputNode = Y.one('#{$fields.approval_status.name}-input');
	SUGAR.AutoComplete.{$ac_key}.inputImage = Y.one('#{$fields.approval_status.name}-image');
	SUGAR.AutoComplete.{$ac_key}.inputHidden = Y.one('#{$fields.approval_status.name}');
	
			{literal}
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("{/literal}{$fields.approval_status.name}{literal}");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('change');
			}

			//global variable 
			sync_{/literal}{$fields.approval_status.name}{literal} = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("{/literal}{$fields.approval_status.name}{literal}");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value');

				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.simulate('keyup');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById('{/literal}{$fields.approval_status.name}-input{literal}'))
						SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("{/literal}{$fields.approval_status.name}{literal}", syncFromHiddenToWidget);
		{/literal}

		SUGAR.AutoComplete.{$ac_key}.minQLen = 0;
		SUGAR.AutoComplete.{$ac_key}.queryDelay = 0;
		SUGAR.AutoComplete.{$ac_key}.numOptions = {$field_options|@count};
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 300) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 200;
			{literal}
		}
		{/literal}
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 3000) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 500;
			{literal}
		}
		{/literal}
		
	SUGAR.AutoComplete.{$ac_key}.optionsVisible = false;
	
	{literal}
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		{/literal}
		minQueryLength: SUGAR.AutoComplete.{$ac_key}.minQLen,
		queryDelay: SUGAR.AutoComplete.{$ac_key}.queryDelay,
		zIndex: 99999,

				
		{literal}
		source: SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds,
		
		resultTextLocator: 'text',
		resultHighlighter: 'phraseMatch',
		resultFilters: 'phraseMatch',
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName('dccontent');
		if(hover[0] != null){
			if (ex) {
				var h = '1000px';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = '';
			}
		}
	}
		
	if({/literal}SUGAR.AutoComplete.{$ac_key}.minQLen{literal} == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function () {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.sendRequest('');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('click', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('click');
		});
		
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('dblclick', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('dblclick');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('focus');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mouseup', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mouseup');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mousedown', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mousedown');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('blur', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('blur');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = false;
			var selectElem = document.getElementById("{/literal}{$fields.approval_status.name}{literal}");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputImage.ancestor().on('click', function () {
		if (SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.blur();
		} else {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('query', function () {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.set('value', '');
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('visibleChange', function (e) {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('select', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
{/literal}
{/if}

{else}


{if is_string($fields.approval_status.options)}
<input type="hidden" class="sugar_field" id="{$fields.approval_status.name}" value="{ $fields.approval_status.options }">
{ $fields.approval_status.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.approval_status.name}" value="{ $fields.approval_status.value }">
{ $fields.approval_status.options[$fields.approval_status.value]}
{/if}
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.approval_status.acl > 1 || ($showDetailData && $fields.approval_status.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.clarification_done_c.acl > 1 || ($showDetailData && $fields.clarification_done_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CLARIFICATION_DONE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_CLARIFICATION_DONE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="bool" field="clarification_done_c"  >

{if $fields.clarification_done_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if strval($fields.clarification_done_c.value) == "1" || strval($fields.clarification_done_c.value) == "yes" || strval($fields.clarification_done_c.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.clarification_done_c.name}" value="0"> 
<input type="checkbox" id="{$fields.clarification_done_c.name}" 
name="{$fields.clarification_done_c.name}" 
value="1" title='' tabindex="0" {$checked} >

{else}

{if strval($fields.clarification_done_c.value) == "1" || strval($fields.clarification_done_c.value) == "yes" || strval($fields.clarification_done_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.clarification_done_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.clarification_done_c.name}" id="{$fields.clarification_done_c.name}" value="$fields.clarification_done_c.value" disabled="true" {$checked}>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.clarification_done_c.acl > 1 || ($showDetailData && $fields.clarification_done_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.remarks_c.acl > 1 || ($showDetailData && $fields.remarks_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_REMARKS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_REMARKS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="text" field="remarks_c"  >

{if $fields.remarks_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if empty($fields.remarks_c.value)}
{assign var="value" value=$fields.remarks_c.default_value }
{else}
{assign var="value" value=$fields.remarks_c.value }
{/if}
<textarea  id='{$fields.remarks_c.name}' name='{$fields.remarks_c.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

<span class="sugar_field" id="{$fields.remarks_c.name|escape:'html'|url2html|nl2br}">{$fields.remarks_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.remarks_c.acl > 1 || ($showDetailData && $fields.remarks_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.ddm_approval_title_c.acl > 1 || ($showDetailData && $fields.ddm_approval_title_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DDM_APPROVAL_TITLE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_APPROVAL_TITLE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="html" field="ddm_approval_title_c"  >

{if $fields.ddm_approval_title_c.acl > 1}

{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.ddm_approval_title_c.name}"><p>Approval by Director Destination Marketing</p></span>

{else}

<span class="sugar_field" id="{$fields.ddm_approval_title_c.name}">{sugar_literal content=$fields.ddm_approval_title_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.ddm_approval_title_c.acl > 1 || ($showDetailData && $fields.ddm_approval_title_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.ddm_approval_c.acl > 1 || ($showDetailData && $fields.ddm_approval_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DDM_APPROVAL">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_APPROVAL' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="ddm_approval_c"  >

{if $fields.ddm_approval_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if !isset($config.enable_autocomplete) || $config.enable_autocomplete==false}
<select name="{$fields.ddm_approval_c.name}" 
id="{$fields.ddm_approval_c.name}" 
title=''       
>
{if isset($fields.ddm_approval_c.value) && $fields.ddm_approval_c.value != ''}
{html_options options=$fields.ddm_approval_c.options selected=$fields.ddm_approval_c.value}
{else}
{html_options options=$fields.ddm_approval_c.options selected=$fields.ddm_approval_c.default}
{/if}
</select>
{else}
{assign var="field_options" value=$fields.ddm_approval_c.options }
{capture name="field_val"}{$fields.ddm_approval_c.value}{/capture}
{assign var="field_val" value=$smarty.capture.field_val}
{capture name="ac_key"}{$fields.ddm_approval_c.name}{/capture}
{assign var="ac_key" value=$smarty.capture.ac_key}
<select style='display:none' name="{$fields.ddm_approval_c.name}" 
id="{$fields.ddm_approval_c.name}" 
title=''          
>
{if isset($fields.ddm_approval_c.value) && $fields.ddm_approval_c.value != ''}
{html_options options=$fields.ddm_approval_c.options selected=$fields.ddm_approval_c.value}
{else}
{html_options options=$fields.ddm_approval_c.options selected=$fields.ddm_approval_c.default}
{/if}
</select>
<input
id="{$fields.ddm_approval_c.name}-input"
name="{$fields.ddm_approval_c.name}-input"
size="30"
value="{$field_val|lookup:$field_options}"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="{sugar_getimagepath file="id-ff-down.png"}" id="{$fields.ddm_approval_c.name}-image"></button><button type="button"
id="btn-clear-{$fields.ddm_approval_c.name}-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '{$fields.ddm_approval_c.name}-input', '{$fields.ddm_approval_c.name}');sync_{$fields.ddm_approval_c.name}()"><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
{literal}
<script>
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal} = [];
	{/literal}

			{literal}
		(function (){
			var selectElem = document.getElementById("{/literal}{$fields.ddm_approval_c.name}{literal}");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:''};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use('datasource', 'datasource-jsonschema',function (Y) {
				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value=='' && selectElem.options[i].innerHTML==''))
				    			ret.push({'key':selectElem.options[i].value,'text':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		{/literal}
	
	{literal}
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	{/literal}
			
	SUGAR.AutoComplete.{$ac_key}.inputNode = Y.one('#{$fields.ddm_approval_c.name}-input');
	SUGAR.AutoComplete.{$ac_key}.inputImage = Y.one('#{$fields.ddm_approval_c.name}-image');
	SUGAR.AutoComplete.{$ac_key}.inputHidden = Y.one('#{$fields.ddm_approval_c.name}');
	
			{literal}
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("{/literal}{$fields.ddm_approval_c.name}{literal}");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('change');
			}

			//global variable 
			sync_{/literal}{$fields.ddm_approval_c.name}{literal} = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("{/literal}{$fields.ddm_approval_c.name}{literal}");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value');

				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.simulate('keyup');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById('{/literal}{$fields.ddm_approval_c.name}-input{literal}'))
						SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("{/literal}{$fields.ddm_approval_c.name}{literal}", syncFromHiddenToWidget);
		{/literal}

		SUGAR.AutoComplete.{$ac_key}.minQLen = 0;
		SUGAR.AutoComplete.{$ac_key}.queryDelay = 0;
		SUGAR.AutoComplete.{$ac_key}.numOptions = {$field_options|@count};
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 300) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 200;
			{literal}
		}
		{/literal}
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 3000) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 500;
			{literal}
		}
		{/literal}
		
	SUGAR.AutoComplete.{$ac_key}.optionsVisible = false;
	
	{literal}
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		{/literal}
		minQueryLength: SUGAR.AutoComplete.{$ac_key}.minQLen,
		queryDelay: SUGAR.AutoComplete.{$ac_key}.queryDelay,
		zIndex: 99999,

				
		{literal}
		source: SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds,
		
		resultTextLocator: 'text',
		resultHighlighter: 'phraseMatch',
		resultFilters: 'phraseMatch',
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName('dccontent');
		if(hover[0] != null){
			if (ex) {
				var h = '1000px';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = '';
			}
		}
	}
		
	if({/literal}SUGAR.AutoComplete.{$ac_key}.minQLen{literal} == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function () {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.sendRequest('');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('click', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('click');
		});
		
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('dblclick', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('dblclick');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('focus');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mouseup', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mouseup');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mousedown', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mousedown');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('blur', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('blur');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = false;
			var selectElem = document.getElementById("{/literal}{$fields.ddm_approval_c.name}{literal}");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputImage.ancestor().on('click', function () {
		if (SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.blur();
		} else {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('query', function () {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.set('value', '');
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('visibleChange', function (e) {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('select', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
{/literal}
{/if}

{else}


{if is_string($fields.ddm_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.ddm_approval_c.name}" value="{ $fields.ddm_approval_c.options }">
{ $fields.ddm_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.ddm_approval_c.name}" value="{ $fields.ddm_approval_c.value }">
{ $fields.ddm_approval_c.options[$fields.ddm_approval_c.value]}
{/if}
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.ddm_approval_c.acl > 1 || ($showDetailData && $fields.ddm_approval_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


</div>




<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.ddm_remarks_c.acl > 1 || ($showDetailData && $fields.ddm_remarks_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DDM_REMARKS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_REMARKS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="text" field="ddm_remarks_c"  >

{if $fields.ddm_remarks_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if empty($fields.ddm_remarks_c.value)}
{assign var="value" value=$fields.ddm_remarks_c.default_value }
{else}
{assign var="value" value=$fields.ddm_remarks_c.value }
{/if}
<textarea  id='{$fields.ddm_remarks_c.name}' name='{$fields.ddm_remarks_c.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

<span class="sugar_field" id="{$fields.ddm_remarks_c.name|escape:'html'|url2html|nl2br}">{$fields.ddm_remarks_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.ddm_remarks_c.acl > 1 || ($showDetailData && $fields.ddm_remarks_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.mgmt_apv_titile_c.acl > 1 || ($showDetailData && $fields.mgmt_apv_titile_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MGMT_APV_TITILE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MGMT_APV_TITILE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="html" field="mgmt_apv_titile_c"  >

{if $fields.mgmt_apv_titile_c.acl > 1}

{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.mgmt_apv_titile_c.name}"><p>Approval by Senior Management Team</p></span>

{else}

<span class="sugar_field" id="{$fields.mgmt_apv_titile_c.name}">{sugar_literal content=$fields.mgmt_apv_titile_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.mgmt_apv_titile_c.acl > 1 || ($showDetailData && $fields.mgmt_apv_titile_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.management_approval_c.acl > 1 || ($showDetailData && $fields.management_approval_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MANAGEMENT_APPROVAL">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MANAGEMENT_APPROVAL' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="management_approval_c"  >

{if $fields.management_approval_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if !isset($config.enable_autocomplete) || $config.enable_autocomplete==false}
<select name="{$fields.management_approval_c.name}" 
id="{$fields.management_approval_c.name}" 
title=''       
>
{if isset($fields.management_approval_c.value) && $fields.management_approval_c.value != ''}
{html_options options=$fields.management_approval_c.options selected=$fields.management_approval_c.value}
{else}
{html_options options=$fields.management_approval_c.options selected=$fields.management_approval_c.default}
{/if}
</select>
{else}
{assign var="field_options" value=$fields.management_approval_c.options }
{capture name="field_val"}{$fields.management_approval_c.value}{/capture}
{assign var="field_val" value=$smarty.capture.field_val}
{capture name="ac_key"}{$fields.management_approval_c.name}{/capture}
{assign var="ac_key" value=$smarty.capture.ac_key}
<select style='display:none' name="{$fields.management_approval_c.name}" 
id="{$fields.management_approval_c.name}" 
title=''          
>
{if isset($fields.management_approval_c.value) && $fields.management_approval_c.value != ''}
{html_options options=$fields.management_approval_c.options selected=$fields.management_approval_c.value}
{else}
{html_options options=$fields.management_approval_c.options selected=$fields.management_approval_c.default}
{/if}
</select>
<input
id="{$fields.management_approval_c.name}-input"
name="{$fields.management_approval_c.name}-input"
size="30"
value="{$field_val|lookup:$field_options}"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="{sugar_getimagepath file="id-ff-down.png"}" id="{$fields.management_approval_c.name}-image"></button><button type="button"
id="btn-clear-{$fields.management_approval_c.name}-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '{$fields.management_approval_c.name}-input', '{$fields.management_approval_c.name}');sync_{$fields.management_approval_c.name}()"><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
{literal}
<script>
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal} = [];
	{/literal}

			{literal}
		(function (){
			var selectElem = document.getElementById("{/literal}{$fields.management_approval_c.name}{literal}");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:''};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use('datasource', 'datasource-jsonschema',function (Y) {
				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value=='' && selectElem.options[i].innerHTML==''))
				    			ret.push({'key':selectElem.options[i].value,'text':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		{/literal}
	
	{literal}
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	{/literal}
			
	SUGAR.AutoComplete.{$ac_key}.inputNode = Y.one('#{$fields.management_approval_c.name}-input');
	SUGAR.AutoComplete.{$ac_key}.inputImage = Y.one('#{$fields.management_approval_c.name}-image');
	SUGAR.AutoComplete.{$ac_key}.inputHidden = Y.one('#{$fields.management_approval_c.name}');
	
			{literal}
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("{/literal}{$fields.management_approval_c.name}{literal}");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('change');
			}

			//global variable 
			sync_{/literal}{$fields.management_approval_c.name}{literal} = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("{/literal}{$fields.management_approval_c.name}{literal}");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value');

				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.simulate('keyup');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById('{/literal}{$fields.management_approval_c.name}-input{literal}'))
						SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("{/literal}{$fields.management_approval_c.name}{literal}", syncFromHiddenToWidget);
		{/literal}

		SUGAR.AutoComplete.{$ac_key}.minQLen = 0;
		SUGAR.AutoComplete.{$ac_key}.queryDelay = 0;
		SUGAR.AutoComplete.{$ac_key}.numOptions = {$field_options|@count};
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 300) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 200;
			{literal}
		}
		{/literal}
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 3000) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 500;
			{literal}
		}
		{/literal}
		
	SUGAR.AutoComplete.{$ac_key}.optionsVisible = false;
	
	{literal}
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		{/literal}
		minQueryLength: SUGAR.AutoComplete.{$ac_key}.minQLen,
		queryDelay: SUGAR.AutoComplete.{$ac_key}.queryDelay,
		zIndex: 99999,

				
		{literal}
		source: SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds,
		
		resultTextLocator: 'text',
		resultHighlighter: 'phraseMatch',
		resultFilters: 'phraseMatch',
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName('dccontent');
		if(hover[0] != null){
			if (ex) {
				var h = '1000px';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = '';
			}
		}
	}
		
	if({/literal}SUGAR.AutoComplete.{$ac_key}.minQLen{literal} == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function () {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.sendRequest('');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('click', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('click');
		});
		
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('dblclick', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('dblclick');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('focus');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mouseup', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mouseup');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mousedown', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mousedown');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('blur', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('blur');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = false;
			var selectElem = document.getElementById("{/literal}{$fields.management_approval_c.name}{literal}");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputImage.ancestor().on('click', function () {
		if (SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.blur();
		} else {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('query', function () {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.set('value', '');
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('visibleChange', function (e) {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('select', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
{/literal}
{/if}

{else}


{if is_string($fields.management_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.management_approval_c.name}" value="{ $fields.management_approval_c.options }">
{ $fields.management_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.management_approval_c.name}" value="{ $fields.management_approval_c.value }">
{ $fields.management_approval_c.options[$fields.management_approval_c.value]}
{/if}
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.management_approval_c.acl > 1 || ($showDetailData && $fields.management_approval_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


</div>




<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.management_remarks_c.acl > 1 || ($showDetailData && $fields.management_remarks_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MANAGEMENT_REMARKS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MANAGEMENT_REMARKS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="text" field="management_remarks_c"  >

{if $fields.management_remarks_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if empty($fields.management_remarks_c.value)}
{assign var="value" value=$fields.management_remarks_c.default_value }
{else}
{assign var="value" value=$fields.management_remarks_c.value }
{/if}
<textarea  id='{$fields.management_remarks_c.name}' name='{$fields.management_remarks_c.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

<span class="sugar_field" id="{$fields.management_remarks_c.name|escape:'html'|url2html|nl2br}">{$fields.management_remarks_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.management_remarks_c.acl > 1 || ($showDetailData && $fields.management_remarks_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.ceo_apv_title_c.acl > 1 || ($showDetailData && $fields.ceo_apv_title_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CEO_APV_TITLE">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_APV_TITLE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="html" field="ceo_apv_title_c"  >

{if $fields.ceo_apv_title_c.acl > 1}

{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.ceo_apv_title_c.name}"><p>Approval by the CEO&#039;s Office</p></span>

{else}

<span class="sugar_field" id="{$fields.ceo_apv_title_c.name}">{sugar_literal content=$fields.ceo_apv_title_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.ceo_apv_title_c.acl > 1 || ($showDetailData && $fields.ceo_apv_title_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.ceo_office_approval_c.acl > 1 || ($showDetailData && $fields.ceo_office_approval_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CEO_OFFICE_APPROVAL">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_OFFICE_APPROVAL' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="enum" field="ceo_office_approval_c"  >

{if $fields.ceo_office_approval_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if !isset($config.enable_autocomplete) || $config.enable_autocomplete==false}
<select name="{$fields.ceo_office_approval_c.name}" 
id="{$fields.ceo_office_approval_c.name}" 
title=''       
>
{if isset($fields.ceo_office_approval_c.value) && $fields.ceo_office_approval_c.value != ''}
{html_options options=$fields.ceo_office_approval_c.options selected=$fields.ceo_office_approval_c.value}
{else}
{html_options options=$fields.ceo_office_approval_c.options selected=$fields.ceo_office_approval_c.default}
{/if}
</select>
{else}
{assign var="field_options" value=$fields.ceo_office_approval_c.options }
{capture name="field_val"}{$fields.ceo_office_approval_c.value}{/capture}
{assign var="field_val" value=$smarty.capture.field_val}
{capture name="ac_key"}{$fields.ceo_office_approval_c.name}{/capture}
{assign var="ac_key" value=$smarty.capture.ac_key}
<select style='display:none' name="{$fields.ceo_office_approval_c.name}" 
id="{$fields.ceo_office_approval_c.name}" 
title=''          
>
{if isset($fields.ceo_office_approval_c.value) && $fields.ceo_office_approval_c.value != ''}
{html_options options=$fields.ceo_office_approval_c.options selected=$fields.ceo_office_approval_c.value}
{else}
{html_options options=$fields.ceo_office_approval_c.options selected=$fields.ceo_office_approval_c.default}
{/if}
</select>
<input
id="{$fields.ceo_office_approval_c.name}-input"
name="{$fields.ceo_office_approval_c.name}-input"
size="30"
value="{$field_val|lookup:$field_options}"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="{sugar_getimagepath file="id-ff-down.png"}" id="{$fields.ceo_office_approval_c.name}-image"></button><button type="button"
id="btn-clear-{$fields.ceo_office_approval_c.name}-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '{$fields.ceo_office_approval_c.name}-input', '{$fields.ceo_office_approval_c.name}');sync_{$fields.ceo_office_approval_c.name}()"><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
{literal}
<script>
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal} = [];
	{/literal}

			{literal}
		(function (){
			var selectElem = document.getElementById("{/literal}{$fields.ceo_office_approval_c.name}{literal}");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:''};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use('datasource', 'datasource-jsonschema',function (Y) {
				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value=='' && selectElem.options[i].innerHTML==''))
				    			ret.push({'key':selectElem.options[i].value,'text':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		{/literal}
	
	{literal}
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	{/literal}
			
	SUGAR.AutoComplete.{$ac_key}.inputNode = Y.one('#{$fields.ceo_office_approval_c.name}-input');
	SUGAR.AutoComplete.{$ac_key}.inputImage = Y.one('#{$fields.ceo_office_approval_c.name}-image');
	SUGAR.AutoComplete.{$ac_key}.inputHidden = Y.one('#{$fields.ceo_office_approval_c.name}');
	
			{literal}
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("{/literal}{$fields.ceo_office_approval_c.name}{literal}");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('change');
			}

			//global variable 
			sync_{/literal}{$fields.ceo_office_approval_c.name}{literal} = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("{/literal}{$fields.ceo_office_approval_c.name}{literal}");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value');

				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.simulate('keyup');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById('{/literal}{$fields.ceo_office_approval_c.name}-input{literal}'))
						SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("{/literal}{$fields.ceo_office_approval_c.name}{literal}", syncFromHiddenToWidget);
		{/literal}

		SUGAR.AutoComplete.{$ac_key}.minQLen = 0;
		SUGAR.AutoComplete.{$ac_key}.queryDelay = 0;
		SUGAR.AutoComplete.{$ac_key}.numOptions = {$field_options|@count};
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 300) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 200;
			{literal}
		}
		{/literal}
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 3000) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 500;
			{literal}
		}
		{/literal}
		
	SUGAR.AutoComplete.{$ac_key}.optionsVisible = false;
	
	{literal}
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		{/literal}
		minQueryLength: SUGAR.AutoComplete.{$ac_key}.minQLen,
		queryDelay: SUGAR.AutoComplete.{$ac_key}.queryDelay,
		zIndex: 99999,

				
		{literal}
		source: SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds,
		
		resultTextLocator: 'text',
		resultHighlighter: 'phraseMatch',
		resultFilters: 'phraseMatch',
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName('dccontent');
		if(hover[0] != null){
			if (ex) {
				var h = '1000px';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = '';
			}
		}
	}
		
	if({/literal}SUGAR.AutoComplete.{$ac_key}.minQLen{literal} == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function () {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.sendRequest('');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('click', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('click');
		});
		
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('dblclick', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('dblclick');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('focus');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mouseup', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mouseup');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mousedown', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mousedown');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('blur', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('blur');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = false;
			var selectElem = document.getElementById("{/literal}{$fields.ceo_office_approval_c.name}{literal}");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputImage.ancestor().on('click', function () {
		if (SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.blur();
		} else {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('query', function () {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.set('value', '');
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('visibleChange', function (e) {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('select', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
{/literal}
{/if}

{else}


{if is_string($fields.ceo_office_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.ceo_office_approval_c.name}" value="{ $fields.ceo_office_approval_c.options }">
{ $fields.ceo_office_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.ceo_office_approval_c.name}" value="{ $fields.ceo_office_approval_c.value }">
{ $fields.ceo_office_approval_c.options[$fields.ceo_office_approval_c.value]}
{/if}
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.ceo_office_approval_c.acl > 1 || ($showDetailData && $fields.ceo_office_approval_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


</div>




<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.ceo_remarks_c.acl > 1 || ($showDetailData && $fields.ceo_remarks_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CEO_REMARKS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_REMARKS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="text" field="ceo_remarks_c"  >

{if $fields.ceo_remarks_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if empty($fields.ceo_remarks_c.value)}
{assign var="value" value=$fields.ceo_remarks_c.default_value }
{else}
{assign var="value" value=$fields.ceo_remarks_c.value }
{/if}
<textarea  id='{$fields.ceo_remarks_c.name}' name='{$fields.ceo_remarks_c.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

<span class="sugar_field" id="{$fields.ceo_remarks_c.name|escape:'html'|url2html|nl2br}">{$fields.ceo_remarks_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.ceo_remarks_c.acl > 1 || ($showDetailData && $fields.ceo_remarks_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>
</div>            </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-5'>


{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}
    
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.assigned_user_name.acl > 1 || ($showDetailData && $fields.assigned_user_name.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_ASSIGNED_TO_NAME">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_ASSIGNED_TO_NAME' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="assigned_user_name"  >

{if $fields.assigned_user_name.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.assigned_user_name.name}" class="sqsEnabled" tabindex="0" id="{$fields.assigned_user_name.name}" size="" value="{$fields.assigned_user_name.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.assigned_user_name.id_name}" 
id="{$fields.assigned_user_name.id_name}" 
value="{$fields.assigned_user_id.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.assigned_user_name.name}" id="btn_{$fields.assigned_user_name.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.assigned_user_name.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"assigned_user_id","user_name":"assigned_user_name"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.assigned_user_name.name}" id="btn_clr_{$fields.assigned_user_name.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.assigned_user_name.name}', '{$fields.assigned_user_name.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.assigned_user_name.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="assigned_user_id" class="sugar_field" data-id-value="{$fields.assigned_user_id.value}">{$fields.assigned_user_name.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.assigned_user_name.acl > 1 || ($showDetailData && $fields.assigned_user_name.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.approval_steps_c.acl > 1 || ($showDetailData && $fields.approval_steps_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_APPROVAL_STEPS">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_STEPS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="int" field="approval_steps_c"  >

{if $fields.approval_steps_c.acl > 1}

{counter name="panelFieldCount" print=false}

{if strlen($fields.approval_steps_c.value) <= 0}
{assign var="value" value=$fields.approval_steps_c.default_value }
{else}
{assign var="value" value=$fields.approval_steps_c.value }
{/if}  
<input type='text' name='{$fields.approval_steps_c.name}' 
id='{$fields.approval_steps_c.name}' size='30' maxlength='1' value='{sugar_number_format precision=0 var=$value}' title='' tabindex='0'    >

{else}

<span class="sugar_field" id="{$fields.approval_steps_c.name}">
{sugar_number_format precision=0 var=$fields.approval_steps_c.value}
</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.approval_steps_c.acl > 1 || ($showDetailData && $fields.approval_steps_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>
</div>            </div>
</div>

<div class="panel-content">
<div>&nbsp;</div>












</div>
</div>

<script language="javascript">
    var _form_id = '{$form_id}';
    {literal}
    SUGAR.util.doWhen(function(){
        _form_id = (_form_id == '') ? 'EditView' : _form_id;
        return document.getElementById(_form_id) != null;
    }, SUGAR.themes.actionMenu);
    {/literal}
</script>
{assign var='place' value="_FOOTER"} <!-- to be used for id for buttons with custom code in def files-->
<div class="buttons">
{if $bean->aclAccess("save")}<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('EditView'); {if $isDuplicate}_form.return_id.value=''; {/if}_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" id="SAVE">{/if} 
{if !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($smarty.request.return_id))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" type="button" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($fields.id.value))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && empty($fields.id.value)) && empty($smarty.request.return_id)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && !empty($smarty.request.return_module)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action={$smarty.request.return_action}&module={$smarty.request.return_module|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif empty($smarty.request.return_action) || empty($smarty.request.return_id) && !empty($fields.id.value)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=AOS_Quotes'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {else}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {/if}
{if $showVCRControl}
<button type="button" id="save_and_continue" class="button saveAndContinue" title="{$app_strings.LBL_SAVE_AND_CONTINUE}" onClick="SUGAR.saveAndContinue(this);">
{$APP.LBL_SAVE_AND_CONTINUE}
</button>
{/if}
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AOS_Quotes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>
</form>
{$set_focus_block}
<script>SUGAR.util.doWhen("document.getElementById('EditView') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script>
{sugar_getscript file="cache/include/javascript/sugar_grp_yui_widgets.js"}
<script type="text/javascript">
var EditView_tabs = new YAHOO.widget.TabView("EditView_tabs");
EditView_tabs.selectTab(0);
</script>
<script type="text/javascript">
YAHOO.util.Event.onContentReady("EditView",
    function () {ldelim} initEditView(document.forms.EditView) {rdelim});
//window.setTimeout(, 100);
window.onbeforeunload = function () {ldelim} return onUnloadEditView(); {rdelim};
// bug 55468 -- IE is too aggressive with onUnload event
if ($.browser.msie) {ldelim}
$(document).ready(function() {ldelim}
    $(".collapseLink,.expandLink").click(function (e) {ldelim} e.preventDefault(); {rdelim});
  {rdelim});
{rdelim}
</script>
{literal}
<script type="text/javascript">

    var selectTab = function(tab) {
        $('#EditView_tabs div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').hide();
        $('#EditView_tabs div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').eq(tab).show().addClass('active').addClass('in');
    };

    var selectTabOnError = function(tab) {
        selectTab(tab);
        $('#EditView_tabs ul.nav.nav-tabs li').removeClass('active');
        $('#EditView_tabs ul.nav.nav-tabs li a').css('color', '');

        $('#EditView_tabs ul.nav.nav-tabs li').eq(tab).find('a').first().css('color', 'red');
        $('#EditView_tabs ul.nav.nav-tabs li').eq(tab).addClass('active');

    };

    var selectTabOnErrorInputHandle = function(inputHandle) {
        var tab = $(inputHandle).closest('.tab-pane-NOBOOTSTRAPTOGGLER').attr('id').match(/^detailpanel_(.*)$/)[1];
        selectTabOnError(tab);
    };


    $(function(){
        $('#EditView_tabs ul.nav.nav-tabs li > a[data-toggle="tab"]').click(function(e){
            if(typeof $(this).parent().find('a').first().attr('id') != 'undefined') {
                var tab = parseInt($(this).parent().find('a').first().attr('id').match(/^tab(.)*$/)[1]);
                selectTab(tab);
            }
        });

        $('a[data-toggle="collapse-edit"]').click(function(e){
            if($(this).hasClass('collapsed')) {
              // Expand panel
                // Change style of .panel-header
                $(this).removeClass('collapsed');
                // Expand .panel-body
                $(this).parents('.panel').find('.panel-body').removeClass('in').addClass('in');
            } else {
              // Collapse panel
                // Change style of .panel-header
                $(this).addClass('collapsed');
                // Collapse .panel-body
                $(this).parents('.panel').find('.panel-body').removeClass('in').removeClass('in');
            }
        });
    });

    </script>
{/literal}
{else}

{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}


<script>
    {literal}
    $(document).ready(function(){
	    $("ul.clickMenu").each(function(index, node){
	        $(node).sugarActionMenu();
	    });
    });
    {/literal}
</script>
<div class="clear"></div>
<form action="index.php" method="POST" name="{$form_name}" id="{$form_id}" {$enctype}>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit">
<tr>
<td class="buttons">
<input type="hidden" name="module" value="{$module}">
{if isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true"}
<input type="hidden" name="record" value="">
<input type="hidden" name="duplicateSave" value="true">
<input type="hidden" name="duplicateId" value="{$fields.id.value}">
{else}
<input type="hidden" name="record" value="{$fields.id.value}">
{/if}
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="action">
<input type="hidden" name="return_module" value="{$smarty.request.return_module}">
<input type="hidden" name="return_action" value="{$smarty.request.return_action}">
<input type="hidden" name="return_id" value="{$smarty.request.return_id}">
<input type="hidden" name="module_tab"> 
<input type="hidden" name="contact_role">
{if (!empty($smarty.request.return_module) || !empty($smarty.request.relate_to)) && !(isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true")}
<input type="hidden" name="relate_to" value="{if $smarty.request.return_relationship}{$smarty.request.return_relationship}{elseif $smarty.request.relate_to && empty($smarty.request.from_dcmenu)}{$smarty.request.relate_to}{elseif empty($isDCForm) && empty($smarty.request.from_dcmenu)}{$smarty.request.return_module}{/if}">
<input type="hidden" name="relate_id" value="{$smarty.request.return_id}">
{/if}
<input type="hidden" name="offset" value="{$offset}">
{assign var='place' value="_HEADER"} <!-- to be used for id for buttons with custom code in def files-->
<div class="action_buttons">{if $bean->aclAccess("save")}<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('EditView'); {if $isDuplicate}_form.return_id.value=''; {/if}_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" id="SAVE_HEADER">{/if}  {if !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($smarty.request.return_id))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" type="button" id="CANCEL_HEADER"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($fields.id.value))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_HEADER"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && empty($fields.id.value)) && empty($smarty.request.return_id)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_HEADER"> {elseif !empty($smarty.request.return_action) && !empty($smarty.request.return_module)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action={$smarty.request.return_action}&module={$smarty.request.return_module|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_HEADER"> {elseif empty($smarty.request.return_action) || empty($smarty.request.return_id) && !empty($fields.id.value)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=AOS_Quotes'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_HEADER"> {else}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_HEADER"> {/if} {if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AOS_Quotes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}<div class="clear"></div></div>
</td>
<td align='right' class="edit-view-pagination">
</td>
</tr>
</table>{sugar_include include=$includes}
<span id='tabcounterJS'><script>SUGAR.TabFields=new Array();//this will be used to track tabindexes for references</script></span>
<div id="EditView_tabs"
class="yui-navset"
>

<ul class="yui-nav">
<li class="selected"><a id="tab0" href="javascript:void(0)"><em>{sugar_translate label='LBL_ACCOUNT_INFORMATION' module='AOS_Quotes'}</em></a></li>
<li class="selected"><a id="tab1" href="javascript:void(1)"><em>{sugar_translate label='LBL_EDITVIEW_PANEL3' module='AOS_Quotes'}</em></a></li>
<li class="selected"><a id="tab2" href="javascript:void(2)"><em>{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes'}</em></a></li>
<li class="selected"><a id="tab3" href="javascript:void(3)"><em>{sugar_translate label='LBL_EDITVIEW_PANEL2' module='AOS_Quotes'}</em></a></li>
<li class="selected"><a id="tab4" href="javascript:void(4)"><em>{sugar_translate label='LBL_EDITVIEW_PANEL4' module='AOS_Quotes'}</em></a></li>
<li class="selected"><a id="tab5" href="javascript:void(5)"><em>{sugar_translate label='LBL_EDITVIEW_PANEL1' module='AOS_Quotes'}</em></a></li>
</ul>
<div class="yui-content">
<div id='tabcontent0'>
<div id="detailpanel_1" class="{$def.templateMeta.panelClass|default:'edit view edit508'}">
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<table width="100%" border="0" cellspacing="1" cellpadding="0"  id='LBL_ACCOUNT_INFORMATION'  class="yui3-skin-sam edit view panelContainer">
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.venue_c.acl > 1 || ($showDetailData && $fields.venue_c.acl > 0)}

<td valign="top" id='venue_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_VENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.venue_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.venue_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.venue_c.name}" size="" value="{$fields.venue_c.value}" title='' autocomplete="off"  	 accesskey='7'  >
<input type="hidden" name="{$fields.venue_c.id_name}" 
id="{$fields.venue_c.id_name}" 
value="{$fields.vnp_venues_id_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.venue_c.name}" id="btn_{$fields.venue_c.name}" tabindex="0" title="{sugar_translate label="LBL_SELECT_BUTTON_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_SELECT_BUTTON_LABEL"}"
onclick='open_popup(
"{$fields.venue_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"vnp_venues_id_c","name":"venue_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.venue_c.name}" id="btn_clr_{$fields.venue_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_RELATE_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.venue_c.name}', '{$fields.venue_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_RELATE_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.venue_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

{if !empty($fields.vnp_venues_id_c.value)}
{capture assign="detail_url"}index.php?module=VNP_Venues&action=DetailView&record={$fields.vnp_venues_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="vnp_venues_id_c" class="sugar_field" data-id-value="{$fields.vnp_venues_id_c.value}">{$fields.venue_c.value}</span>
{if !empty($fields.vnp_venues_id_c.value)}</a>{/if}

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.event_date_c.acl > 1 || ($showDetailData && $fields.event_date_c.acl > 0)}

<td valign="top" id='event_date_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.event_date_c.acl > 1}

{counter name="panelFieldCount"}

<span class="dateTime">
{assign var=date_value value=$fields.event_date_c.value }
<input class="date_input" autocomplete="off" type="text" name="{$fields.event_date_c.name}" id="{$fields.event_date_c.name}" value="{$date_value}" title=''  tabindex='0'    size="11" maxlength="10" >
<button type="button" id="{$fields.event_date_c.name}_trigger" class="btn btn-danger" onclick="return false;"><span class="suitepicon suitepicon-module-calendar" alt="{$APP.LBL_ENTER_DATE}"></span></button>
</span>
<script type="text/javascript">
Calendar.setup ({ldelim}
inputField : "{$fields.event_date_c.name}",
form : "EditView",
ifFormat : "{$CALENDAR_FORMAT}",
daFormat : "{$CALENDAR_FORMAT}",
button : "{$fields.event_date_c.name}_trigger",
singleClick : true,
dateStr : "{$date_value}",
startWeekday: {$CALENDAR_FDOW|default:'0'},
step : 1,
weekNumbers:false
{rdelim}
);
</script>

{else}

{counter name="panelFieldCount"}


{if strlen($fields.event_date_c.value) <= 0}
{assign var="value" value=$fields.event_date_c.default_value }
{else}
{assign var="value" value=$fields.event_date_c.value }
{/if}
<span class="sugar_field" id="{$fields.event_date_c.name}">{$value}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.event_brief_c.acl > 1 || ($showDetailData && $fields.event_brief_c.acl > 0)}

<td valign="top" id='event_brief_c_label' width='12.5%' data-total-columns="1" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_BRIEF' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="1" colspan='3'>

{if $fields.event_brief_c.acl > 1}

{counter name="panelFieldCount"}

<link rel="stylesheet" type="text/css" href="include/SugarFields/Fields/Wysiwyg/css/wysiwyg-editview.css" />
{if empty($fields.event_brief_c.value)}
{assign var="value" value=$fields.event_brief_c.default_value }
{else}
{assign var="value" value=$fields.event_brief_c.value }
{/if}
{literal}
<script type="text/javascript" language="Javascript"> tinyMCE.init({"convert_urls":false,"valid_children":"+body[style]","height":300,"width":"100%","theme":"modern","theme_advanced_toolbar_align":"left","theme_advanced_toolbar_location":"top","theme_advanced_buttons1":"","theme_advanced_buttons2":"","theme_advanced_buttons3":"","strict_loading_mode":true,"mode":"exact","language":"en","plugins":"print code preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern ","elements":"#EditView #event_brief_c","extended_valid_elements":"style[dir|lang|media|title|type],hr[class|width|size|noshade],@[class|style]","content_css":"vendor\/tinymce\/tinymce\/skins\/lightgray\/content.min.css","apply_source_formatting":false,"cleanup_on_startup":true,"relative_urls":false,"selector":"#EditView #event_brief_c","toolbar1":"formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat"});</script>
{/literal}
<div class="wysiwyg">
<textarea
            id="{$fields.event_brief_c.name}"
            name="{$fields.event_brief_c.name}"
            rows="4"
            cols="60"
            title=''
            tabindex="0"
            
        >{$value}</textarea>
</div>
<br />

{else}

{counter name="panelFieldCount"}

<iframe
id="{$fields.event_brief_c.name}"
name="{$fields.event_brief_c.name}"
srcdoc="{$fields.event_brief_c.value}"
style="width:100%;height:500px"
></iframe>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_ACCOUNT_INFORMATION").style.display='none';</script>
{/if}
</div>    <div id='tabcontent1'>
<div id="detailpanel_2" class="{$def.templateMeta.panelClass|default:'edit view edit508'}">
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<table width="100%" border="0" cellspacing="1" cellpadding="0"  id='LBL_EDITVIEW_PANEL3'  class="yui3-skin-sam edit view panelContainer">
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.manually_calculate_c.acl > 1 || ($showDetailData && $fields.manually_calculate_c.acl > 0)}

<td valign="top" id='manually_calculate_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MANUALLY_CALCULATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.manually_calculate_c.acl > 1}

{counter name="panelFieldCount"}

{if strval($fields.manually_calculate_c.value) == "1" || strval($fields.manually_calculate_c.value) == "yes" || strval($fields.manually_calculate_c.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.manually_calculate_c.name}" value="0"> 
<input type="checkbox" id="{$fields.manually_calculate_c.name}" 
name="{$fields.manually_calculate_c.name}" 
value="1" title='' tabindex="0" {$checked} >

{else}

{counter name="panelFieldCount"}

{if strval($fields.manually_calculate_c.value) == "1" || strval($fields.manually_calculate_c.value) == "yes" || strval($fields.manually_calculate_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.manually_calculate_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.manually_calculate_c.name}" id="{$fields.manually_calculate_c.name}" value="$fields.manually_calculate_c.value" disabled="true" {$checked}>

{/if}
{else}
<td></td><td></td>
{/if}



<td valign="top" id='_label' width='12.5%' data-total-columns="2" scope="col">
&nbsp;
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >



</td>

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.no_of_delegates_c.acl > 1 || ($showDetailData && $fields.no_of_delegates_c.acl > 0)}

<td valign="top" id='no_of_delegates_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_NO_OF_DELEGATES' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.no_of_delegates_c.acl > 1}

{counter name="panelFieldCount"}

{if strlen($fields.no_of_delegates_c.value) <= 0}
{assign var="value" value=$fields.no_of_delegates_c.default_value }
{else}
{assign var="value" value=$fields.no_of_delegates_c.value }
{/if}  
<input type='text' name='{$fields.no_of_delegates_c.name}' 
id='{$fields.no_of_delegates_c.name}' size='30' maxlength='5' value='{$value}' title='' tabindex='0'    >

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.no_of_delegates_c.name}">
{assign var="value" value=$fields.no_of_delegates_c.value }
{$value}
</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.event_days_c.acl > 1 || ($showDetailData && $fields.event_days_c.acl > 0)}

<td valign="top" id='event_days_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DAYS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.event_days_c.acl > 1}

{counter name="panelFieldCount"}

{if strlen($fields.event_days_c.value) <= 0}
{assign var="value" value=$fields.event_days_c.default_value }
{else}
{assign var="value" value=$fields.event_days_c.value }
{/if}  
<input type='text' name='{$fields.event_days_c.name}' 
id='{$fields.event_days_c.name}' size='30' maxlength='3' value='{$value}' title='' tabindex='0'    >

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.event_days_c.name}">
{assign var="value" value=$fields.event_days_c.value }
{$value}
</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.additional_revenue_c.acl > 1 || ($showDetailData && $fields.additional_revenue_c.acl > 0)}

<td valign="top" id='additional_revenue_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_ADDITIONAL_REVENUE ' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.additional_revenue_c.acl > 1}

{counter name="panelFieldCount"}

{if strlen($fields.additional_revenue_c.value) <= 0}
{assign var="value" value=$fields.additional_revenue_c.default_value }
{else}
{assign var="value" value=$fields.additional_revenue_c.value }
{/if}  
<input type='text' name='{$fields.additional_revenue_c.name}' 
id='{$fields.additional_revenue_c.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>

{else}

{counter name="panelFieldCount"}

<span id='{$fields.additional_revenue_c.name}'>
{sugar_number_format var=$fields.additional_revenue_c.value }
</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.revenue_description_c.acl > 1 || ($showDetailData && $fields.revenue_description_c.acl > 0)}

<td valign="top" id='revenue_description_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_REVENUE_DESCRIPTION' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.revenue_description_c.acl > 1}

{counter name="panelFieldCount"}

{if empty($fields.revenue_description_c.value)}
{assign var="value" value=$fields.revenue_description_c.default_value }
{else}
{assign var="value" value=$fields.revenue_description_c.value }
{/if}
<textarea  id='{$fields.revenue_description_c.name}' name='{$fields.revenue_description_c.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.revenue_description_c.name|escape:'html'|url2html|nl2br}">{$fields.revenue_description_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.expected_revenue_c.acl > 1 || ($showDetailData && $fields.expected_revenue_c.acl > 0)}

<td valign="top" id='expected_revenue_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_EXPECTED_REVENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.expected_revenue_c.acl > 1}

{counter name="panelFieldCount"}

{if strlen($fields.expected_revenue_c.value) <= 0}
{assign var="value" value=$fields.expected_revenue_c.default_value }
{else}
{assign var="value" value=$fields.expected_revenue_c.value }
{/if}  
<input type='text' name='{$fields.expected_revenue_c.name}' 
id='{$fields.expected_revenue_c.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>

{else}

{counter name="panelFieldCount"}

<span id='{$fields.expected_revenue_c.name}'>
{sugar_number_format var=$fields.expected_revenue_c.value }
</span>

{/if}
{else}
<td></td><td></td>
{/if}



<td valign="top" id='_label' width='12.5%' data-total-columns="2" scope="col">
&nbsp;
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >



</td>

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.gor_revenue_c.acl > 1 || ($showDetailData && $fields.gor_revenue_c.acl > 0)}

<td valign="top" id='gor_revenue_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_REVENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.gor_revenue_c.acl > 1}

{counter name="panelFieldCount"}

{if strlen($fields.gor_revenue_c.value) <= 0}
{assign var="value" value=$fields.gor_revenue_c.default_value }
{else}
{assign var="value" value=$fields.gor_revenue_c.value }
{/if}  
<input type='text' name='{$fields.gor_revenue_c.name}' 
id='{$fields.gor_revenue_c.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>

{else}

{counter name="panelFieldCount"}

<span id='{$fields.gor_revenue_c.name}'>
{sugar_number_format var=$fields.gor_revenue_c.value }
</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.gor_commitment_c.acl > 1 || ($showDetailData && $fields.gor_commitment_c.acl > 0)}

<td valign="top" id='gor_commitment_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_COMMITMENT' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.gor_commitment_c.acl > 1}

{counter name="panelFieldCount"}

{if strlen($fields.gor_commitment_c.value) <= 0}
{assign var="value" value=$fields.gor_commitment_c.default_value }
{else}
{assign var="value" value=$fields.gor_commitment_c.value }
{/if}  
<input type='text' name='{$fields.gor_commitment_c.name}' 
id='{$fields.gor_commitment_c.name}' size='30' maxlength='26' value='{sugar_number_format var=$value}' title='' tabindex='0'
>

{else}

{counter name="panelFieldCount"}

<span id='{$fields.gor_commitment_c.name}'>
{sugar_number_format var=$fields.gor_commitment_c.value }
</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_EDITVIEW_PANEL3").style.display='none';</script>
{/if}
</div>    <div id='tabcontent2'>
<div id="detailpanel_3" class="{$def.templateMeta.panelClass|default:'edit view edit508'}">
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<table width="100%" border="0" cellspacing="1" cellpadding="0"  id='LBL_LINE_ITEMS'  class="yui3-skin-sam edit view panelContainer">
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.currency_id.acl > 1 || ($showDetailData && $fields.currency_id.acl > 0)}

<td valign="top" id='currency_id_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_CURRENCY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.currency_id.acl > 1}

{counter name="panelFieldCount"}
<span id='currency_id_span'>
{$fields.currency_id.value}</span>

{else}

{counter name="panelFieldCount"}
<span id='currency_id_span'>
{$fields.currency_id.value}
</span>

{/if}
{else}
<td></td><td></td>
{/if}



<td valign="top" id='_label' width='12.5%' data-total-columns="2" scope="col">
&nbsp;
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >



</td>

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.line_items.acl > 1 || ($showDetailData && $fields.line_items.acl > 0)}

<td valign="top" id='line_items_label' width='12.5%' data-total-columns="1" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="1" colspan='3'>

{if $fields.line_items.acl > 1}

{counter name="panelFieldCount"}
<span id='line_items_span'>
{$fields.line_items.value}</span>

{else}

{counter name="panelFieldCount"}
<span id='line_items_span'>
{$fields.line_items.value}
</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>


<td valign="top" id='_label' width='12.5%' data-total-columns="1" scope="col">
&nbsp;
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="1" colspan='3'>



</td>

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_LINE_ITEMS").style.display='none';</script>
{/if}
</div>    <div id='tabcontent3'>
<div id="detailpanel_4" class="{$def.templateMeta.panelClass|default:'edit view edit508'}">
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<table width="100%" border="0" cellspacing="1" cellpadding="0"  id='LBL_EDITVIEW_PANEL2'  class="yui3-skin-sam edit view panelContainer">
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.recommendation_c.acl > 1 || ($showDetailData && $fields.recommendation_c.acl > 0)}

<td valign="top" id='recommendation_c_label' width='12.5%' data-total-columns="1" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_RECOMMENDATION' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="1" colspan='3'>

{if $fields.recommendation_c.acl > 1}

{counter name="panelFieldCount"}

<link rel="stylesheet" type="text/css" href="include/SugarFields/Fields/Wysiwyg/css/wysiwyg-editview.css" />
{if empty($fields.recommendation_c.value)}
{assign var="value" value=$fields.recommendation_c.default_value }
{else}
{assign var="value" value=$fields.recommendation_c.value }
{/if}
{literal}
<script type="text/javascript" language="Javascript"> tinyMCE.init({"convert_urls":false,"valid_children":"+body[style]","height":300,"width":"100%","theme":"modern","theme_advanced_toolbar_align":"left","theme_advanced_toolbar_location":"top","theme_advanced_buttons1":"","theme_advanced_buttons2":"","theme_advanced_buttons3":"","strict_loading_mode":true,"mode":"exact","language":"en","plugins":"print code preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern ","elements":"#EditView #recommendation_c","extended_valid_elements":"style[dir|lang|media|title|type],hr[class|width|size|noshade],@[class|style]","content_css":"vendor\/tinymce\/tinymce\/skins\/lightgray\/content.min.css","apply_source_formatting":false,"cleanup_on_startup":true,"relative_urls":false,"selector":"#EditView #recommendation_c","toolbar1":"formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat"});</script>
{/literal}
<div class="wysiwyg">
<textarea
            id="{$fields.recommendation_c.name}"
            name="{$fields.recommendation_c.name}"
            rows="4"
            cols="60"
            title=''
            tabindex="0"
            
        >{$value}</textarea>
</div>
<br />

{else}

{counter name="panelFieldCount"}

<iframe
id="{$fields.recommendation_c.name}"
name="{$fields.recommendation_c.name}"
srcdoc="{$fields.recommendation_c.value}"
style="width:100%;height:500px"
></iframe>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_EDITVIEW_PANEL2").style.display='none';</script>
{/if}
</div>    <div id='tabcontent4'>
<div id="detailpanel_5" class="{$def.templateMeta.panelClass|default:'edit view edit508'}">
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<table width="100%" border="0" cellspacing="1" cellpadding="0"  id='LBL_EDITVIEW_PANEL4'  class="yui3-skin-sam edit view panelContainer">
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.send_for_approval_c.acl > 1 || ($showDetailData && $fields.send_for_approval_c.acl > 0)}

<td valign="top" id='send_for_approval_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_SEND_FOR_APPROVAL' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.send_for_approval_c.acl > 1}

{counter name="panelFieldCount"}

{if strval($fields.send_for_approval_c.value) == "1" || strval($fields.send_for_approval_c.value) == "yes" || strval($fields.send_for_approval_c.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.send_for_approval_c.name}" value="0"> 
<input type="checkbox" id="{$fields.send_for_approval_c.name}" 
name="{$fields.send_for_approval_c.name}" 
value="1" title='' tabindex="0" {$checked} >

{else}

{counter name="panelFieldCount"}

{if strval($fields.send_for_approval_c.value) == "1" || strval($fields.send_for_approval_c.value) == "yes" || strval($fields.send_for_approval_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.send_for_approval_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.send_for_approval_c.name}" id="{$fields.send_for_approval_c.name}" value="$fields.send_for_approval_c.value" disabled="true" {$checked}>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.approval_status.acl > 1 || ($showDetailData && $fields.approval_status.acl > 0)}

<td valign="top" id='approval_status_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_STATUS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.approval_status.acl > 1}

{counter name="panelFieldCount"}

{if !isset($config.enable_autocomplete) || $config.enable_autocomplete==false}
<select name="{$fields.approval_status.name}" 
id="{$fields.approval_status.name}" 
title=''       
>
{if isset($fields.approval_status.value) && $fields.approval_status.value != ''}
{html_options options=$fields.approval_status.options selected=$fields.approval_status.value}
{else}
{html_options options=$fields.approval_status.options selected=$fields.approval_status.default}
{/if}
</select>
{else}
{assign var="field_options" value=$fields.approval_status.options }
{capture name="field_val"}{$fields.approval_status.value}{/capture}
{assign var="field_val" value=$smarty.capture.field_val}
{capture name="ac_key"}{$fields.approval_status.name}{/capture}
{assign var="ac_key" value=$smarty.capture.ac_key}
<select style='display:none' name="{$fields.approval_status.name}" 
id="{$fields.approval_status.name}" 
title=''          
>
{if isset($fields.approval_status.value) && $fields.approval_status.value != ''}
{html_options options=$fields.approval_status.options selected=$fields.approval_status.value}
{else}
{html_options options=$fields.approval_status.options selected=$fields.approval_status.default}
{/if}
</select>
<input
id="{$fields.approval_status.name}-input"
name="{$fields.approval_status.name}-input"
size="30"
value="{$field_val|lookup:$field_options}"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="{sugar_getimagepath file="id-ff-down.png"}" id="{$fields.approval_status.name}-image"></button><button type="button"
id="btn-clear-{$fields.approval_status.name}-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '{$fields.approval_status.name}-input', '{$fields.approval_status.name}');sync_{$fields.approval_status.name}()"><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
{literal}
<script>
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal} = [];
	{/literal}

			{literal}
		(function (){
			var selectElem = document.getElementById("{/literal}{$fields.approval_status.name}{literal}");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:''};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use('datasource', 'datasource-jsonschema',function (Y) {
				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value=='' && selectElem.options[i].innerHTML==''))
				    			ret.push({'key':selectElem.options[i].value,'text':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		{/literal}
	
	{literal}
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	{/literal}
			
	SUGAR.AutoComplete.{$ac_key}.inputNode = Y.one('#{$fields.approval_status.name}-input');
	SUGAR.AutoComplete.{$ac_key}.inputImage = Y.one('#{$fields.approval_status.name}-image');
	SUGAR.AutoComplete.{$ac_key}.inputHidden = Y.one('#{$fields.approval_status.name}');
	
			{literal}
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("{/literal}{$fields.approval_status.name}{literal}");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('change');
			}

			//global variable 
			sync_{/literal}{$fields.approval_status.name}{literal} = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("{/literal}{$fields.approval_status.name}{literal}");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value');

				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.simulate('keyup');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById('{/literal}{$fields.approval_status.name}-input{literal}'))
						SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("{/literal}{$fields.approval_status.name}{literal}", syncFromHiddenToWidget);
		{/literal}

		SUGAR.AutoComplete.{$ac_key}.minQLen = 0;
		SUGAR.AutoComplete.{$ac_key}.queryDelay = 0;
		SUGAR.AutoComplete.{$ac_key}.numOptions = {$field_options|@count};
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 300) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 200;
			{literal}
		}
		{/literal}
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 3000) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 500;
			{literal}
		}
		{/literal}
		
	SUGAR.AutoComplete.{$ac_key}.optionsVisible = false;
	
	{literal}
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		{/literal}
		minQueryLength: SUGAR.AutoComplete.{$ac_key}.minQLen,
		queryDelay: SUGAR.AutoComplete.{$ac_key}.queryDelay,
		zIndex: 99999,

				
		{literal}
		source: SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds,
		
		resultTextLocator: 'text',
		resultHighlighter: 'phraseMatch',
		resultFilters: 'phraseMatch',
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName('dccontent');
		if(hover[0] != null){
			if (ex) {
				var h = '1000px';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = '';
			}
		}
	}
		
	if({/literal}SUGAR.AutoComplete.{$ac_key}.minQLen{literal} == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function () {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.sendRequest('');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('click', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('click');
		});
		
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('dblclick', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('dblclick');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('focus');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mouseup', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mouseup');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mousedown', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mousedown');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('blur', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('blur');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = false;
			var selectElem = document.getElementById("{/literal}{$fields.approval_status.name}{literal}");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputImage.ancestor().on('click', function () {
		if (SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.blur();
		} else {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('query', function () {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.set('value', '');
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('visibleChange', function (e) {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('select', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
{/literal}
{/if}

{else}

{counter name="panelFieldCount"}


{if is_string($fields.approval_status.options)}
<input type="hidden" class="sugar_field" id="{$fields.approval_status.name}" value="{ $fields.approval_status.options }">
{ $fields.approval_status.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.approval_status.name}" value="{ $fields.approval_status.value }">
{ $fields.approval_status.options[$fields.approval_status.value]}
{/if}

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.clarification_done_c.acl > 1 || ($showDetailData && $fields.clarification_done_c.acl > 0)}

<td valign="top" id='clarification_done_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_CLARIFICATION_DONE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.clarification_done_c.acl > 1}

{counter name="panelFieldCount"}

{if strval($fields.clarification_done_c.value) == "1" || strval($fields.clarification_done_c.value) == "yes" || strval($fields.clarification_done_c.value) == "on"} 
{assign var="checked" value='checked="checked"'}
{else}
{assign var="checked" value=""}
{/if}
<input type="hidden" name="{$fields.clarification_done_c.name}" value="0"> 
<input type="checkbox" id="{$fields.clarification_done_c.name}" 
name="{$fields.clarification_done_c.name}" 
value="1" title='' tabindex="0" {$checked} >

{else}

{counter name="panelFieldCount"}

{if strval($fields.clarification_done_c.value) == "1" || strval($fields.clarification_done_c.value) == "yes" || strval($fields.clarification_done_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.clarification_done_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.clarification_done_c.name}" id="{$fields.clarification_done_c.name}" value="$fields.clarification_done_c.value" disabled="true" {$checked}>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.remarks_c.acl > 1 || ($showDetailData && $fields.remarks_c.acl > 0)}

<td valign="top" id='remarks_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_REMARKS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.remarks_c.acl > 1}

{counter name="panelFieldCount"}

{if empty($fields.remarks_c.value)}
{assign var="value" value=$fields.remarks_c.default_value }
{else}
{assign var="value" value=$fields.remarks_c.value }
{/if}
<textarea  id='{$fields.remarks_c.name}' name='{$fields.remarks_c.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.remarks_c.name|escape:'html'|url2html|nl2br}">{$fields.remarks_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.ddm_approval_title_c.acl > 1 || ($showDetailData && $fields.ddm_approval_title_c.acl > 0)}

<td valign="top" id='ddm_approval_title_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_APPROVAL_TITLE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.ddm_approval_title_c.acl > 1}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.ddm_approval_title_c.name}"><p>Approval by Director Destination Marketing</p></span>

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.ddm_approval_title_c.name}">{sugar_literal content=$fields.ddm_approval_title_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.ddm_approval_c.acl > 1 || ($showDetailData && $fields.ddm_approval_c.acl > 0)}

<td valign="top" id='ddm_approval_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_APPROVAL' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.ddm_approval_c.acl > 1}

{counter name="panelFieldCount"}

{if !isset($config.enable_autocomplete) || $config.enable_autocomplete==false}
<select name="{$fields.ddm_approval_c.name}" 
id="{$fields.ddm_approval_c.name}" 
title=''       
>
{if isset($fields.ddm_approval_c.value) && $fields.ddm_approval_c.value != ''}
{html_options options=$fields.ddm_approval_c.options selected=$fields.ddm_approval_c.value}
{else}
{html_options options=$fields.ddm_approval_c.options selected=$fields.ddm_approval_c.default}
{/if}
</select>
{else}
{assign var="field_options" value=$fields.ddm_approval_c.options }
{capture name="field_val"}{$fields.ddm_approval_c.value}{/capture}
{assign var="field_val" value=$smarty.capture.field_val}
{capture name="ac_key"}{$fields.ddm_approval_c.name}{/capture}
{assign var="ac_key" value=$smarty.capture.ac_key}
<select style='display:none' name="{$fields.ddm_approval_c.name}" 
id="{$fields.ddm_approval_c.name}" 
title=''          
>
{if isset($fields.ddm_approval_c.value) && $fields.ddm_approval_c.value != ''}
{html_options options=$fields.ddm_approval_c.options selected=$fields.ddm_approval_c.value}
{else}
{html_options options=$fields.ddm_approval_c.options selected=$fields.ddm_approval_c.default}
{/if}
</select>
<input
id="{$fields.ddm_approval_c.name}-input"
name="{$fields.ddm_approval_c.name}-input"
size="30"
value="{$field_val|lookup:$field_options}"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="{sugar_getimagepath file="id-ff-down.png"}" id="{$fields.ddm_approval_c.name}-image"></button><button type="button"
id="btn-clear-{$fields.ddm_approval_c.name}-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '{$fields.ddm_approval_c.name}-input', '{$fields.ddm_approval_c.name}');sync_{$fields.ddm_approval_c.name}()"><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
{literal}
<script>
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal} = [];
	{/literal}

			{literal}
		(function (){
			var selectElem = document.getElementById("{/literal}{$fields.ddm_approval_c.name}{literal}");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:''};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use('datasource', 'datasource-jsonschema',function (Y) {
				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value=='' && selectElem.options[i].innerHTML==''))
				    			ret.push({'key':selectElem.options[i].value,'text':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		{/literal}
	
	{literal}
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	{/literal}
			
	SUGAR.AutoComplete.{$ac_key}.inputNode = Y.one('#{$fields.ddm_approval_c.name}-input');
	SUGAR.AutoComplete.{$ac_key}.inputImage = Y.one('#{$fields.ddm_approval_c.name}-image');
	SUGAR.AutoComplete.{$ac_key}.inputHidden = Y.one('#{$fields.ddm_approval_c.name}');
	
			{literal}
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("{/literal}{$fields.ddm_approval_c.name}{literal}");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('change');
			}

			//global variable 
			sync_{/literal}{$fields.ddm_approval_c.name}{literal} = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("{/literal}{$fields.ddm_approval_c.name}{literal}");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value');

				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.simulate('keyup');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById('{/literal}{$fields.ddm_approval_c.name}-input{literal}'))
						SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("{/literal}{$fields.ddm_approval_c.name}{literal}", syncFromHiddenToWidget);
		{/literal}

		SUGAR.AutoComplete.{$ac_key}.minQLen = 0;
		SUGAR.AutoComplete.{$ac_key}.queryDelay = 0;
		SUGAR.AutoComplete.{$ac_key}.numOptions = {$field_options|@count};
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 300) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 200;
			{literal}
		}
		{/literal}
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 3000) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 500;
			{literal}
		}
		{/literal}
		
	SUGAR.AutoComplete.{$ac_key}.optionsVisible = false;
	
	{literal}
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		{/literal}
		minQueryLength: SUGAR.AutoComplete.{$ac_key}.minQLen,
		queryDelay: SUGAR.AutoComplete.{$ac_key}.queryDelay,
		zIndex: 99999,

				
		{literal}
		source: SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds,
		
		resultTextLocator: 'text',
		resultHighlighter: 'phraseMatch',
		resultFilters: 'phraseMatch',
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName('dccontent');
		if(hover[0] != null){
			if (ex) {
				var h = '1000px';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = '';
			}
		}
	}
		
	if({/literal}SUGAR.AutoComplete.{$ac_key}.minQLen{literal} == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function () {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.sendRequest('');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('click', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('click');
		});
		
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('dblclick', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('dblclick');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('focus');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mouseup', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mouseup');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mousedown', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mousedown');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('blur', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('blur');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = false;
			var selectElem = document.getElementById("{/literal}{$fields.ddm_approval_c.name}{literal}");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputImage.ancestor().on('click', function () {
		if (SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.blur();
		} else {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('query', function () {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.set('value', '');
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('visibleChange', function (e) {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('select', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
{/literal}
{/if}

{else}

{counter name="panelFieldCount"}


{if is_string($fields.ddm_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.ddm_approval_c.name}" value="{ $fields.ddm_approval_c.options }">
{ $fields.ddm_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.ddm_approval_c.name}" value="{ $fields.ddm_approval_c.value }">
{ $fields.ddm_approval_c.options[$fields.ddm_approval_c.value]}
{/if}

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>


<td valign="top" id='_label' width='12.5%' data-total-columns="2" scope="col">
&nbsp;
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >



</td>


{if $fields.ddm_remarks_c.acl > 1 || ($showDetailData && $fields.ddm_remarks_c.acl > 0)}

<td valign="top" id='ddm_remarks_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_REMARKS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.ddm_remarks_c.acl > 1}

{counter name="panelFieldCount"}

{if empty($fields.ddm_remarks_c.value)}
{assign var="value" value=$fields.ddm_remarks_c.default_value }
{else}
{assign var="value" value=$fields.ddm_remarks_c.value }
{/if}
<textarea  id='{$fields.ddm_remarks_c.name}' name='{$fields.ddm_remarks_c.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.ddm_remarks_c.name|escape:'html'|url2html|nl2br}">{$fields.ddm_remarks_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.mgmt_apv_titile_c.acl > 1 || ($showDetailData && $fields.mgmt_apv_titile_c.acl > 0)}

<td valign="top" id='mgmt_apv_titile_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MGMT_APV_TITILE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.mgmt_apv_titile_c.acl > 1}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.mgmt_apv_titile_c.name}"><p>Approval by Senior Management Team</p></span>

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.mgmt_apv_titile_c.name}">{sugar_literal content=$fields.mgmt_apv_titile_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.management_approval_c.acl > 1 || ($showDetailData && $fields.management_approval_c.acl > 0)}

<td valign="top" id='management_approval_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MANAGEMENT_APPROVAL' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.management_approval_c.acl > 1}

{counter name="panelFieldCount"}

{if !isset($config.enable_autocomplete) || $config.enable_autocomplete==false}
<select name="{$fields.management_approval_c.name}" 
id="{$fields.management_approval_c.name}" 
title=''       
>
{if isset($fields.management_approval_c.value) && $fields.management_approval_c.value != ''}
{html_options options=$fields.management_approval_c.options selected=$fields.management_approval_c.value}
{else}
{html_options options=$fields.management_approval_c.options selected=$fields.management_approval_c.default}
{/if}
</select>
{else}
{assign var="field_options" value=$fields.management_approval_c.options }
{capture name="field_val"}{$fields.management_approval_c.value}{/capture}
{assign var="field_val" value=$smarty.capture.field_val}
{capture name="ac_key"}{$fields.management_approval_c.name}{/capture}
{assign var="ac_key" value=$smarty.capture.ac_key}
<select style='display:none' name="{$fields.management_approval_c.name}" 
id="{$fields.management_approval_c.name}" 
title=''          
>
{if isset($fields.management_approval_c.value) && $fields.management_approval_c.value != ''}
{html_options options=$fields.management_approval_c.options selected=$fields.management_approval_c.value}
{else}
{html_options options=$fields.management_approval_c.options selected=$fields.management_approval_c.default}
{/if}
</select>
<input
id="{$fields.management_approval_c.name}-input"
name="{$fields.management_approval_c.name}-input"
size="30"
value="{$field_val|lookup:$field_options}"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="{sugar_getimagepath file="id-ff-down.png"}" id="{$fields.management_approval_c.name}-image"></button><button type="button"
id="btn-clear-{$fields.management_approval_c.name}-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '{$fields.management_approval_c.name}-input', '{$fields.management_approval_c.name}');sync_{$fields.management_approval_c.name}()"><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
{literal}
<script>
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal} = [];
	{/literal}

			{literal}
		(function (){
			var selectElem = document.getElementById("{/literal}{$fields.management_approval_c.name}{literal}");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:''};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use('datasource', 'datasource-jsonschema',function (Y) {
				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value=='' && selectElem.options[i].innerHTML==''))
				    			ret.push({'key':selectElem.options[i].value,'text':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		{/literal}
	
	{literal}
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	{/literal}
			
	SUGAR.AutoComplete.{$ac_key}.inputNode = Y.one('#{$fields.management_approval_c.name}-input');
	SUGAR.AutoComplete.{$ac_key}.inputImage = Y.one('#{$fields.management_approval_c.name}-image');
	SUGAR.AutoComplete.{$ac_key}.inputHidden = Y.one('#{$fields.management_approval_c.name}');
	
			{literal}
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("{/literal}{$fields.management_approval_c.name}{literal}");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('change');
			}

			//global variable 
			sync_{/literal}{$fields.management_approval_c.name}{literal} = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("{/literal}{$fields.management_approval_c.name}{literal}");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value');

				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.simulate('keyup');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById('{/literal}{$fields.management_approval_c.name}-input{literal}'))
						SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("{/literal}{$fields.management_approval_c.name}{literal}", syncFromHiddenToWidget);
		{/literal}

		SUGAR.AutoComplete.{$ac_key}.minQLen = 0;
		SUGAR.AutoComplete.{$ac_key}.queryDelay = 0;
		SUGAR.AutoComplete.{$ac_key}.numOptions = {$field_options|@count};
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 300) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 200;
			{literal}
		}
		{/literal}
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 3000) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 500;
			{literal}
		}
		{/literal}
		
	SUGAR.AutoComplete.{$ac_key}.optionsVisible = false;
	
	{literal}
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		{/literal}
		minQueryLength: SUGAR.AutoComplete.{$ac_key}.minQLen,
		queryDelay: SUGAR.AutoComplete.{$ac_key}.queryDelay,
		zIndex: 99999,

				
		{literal}
		source: SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds,
		
		resultTextLocator: 'text',
		resultHighlighter: 'phraseMatch',
		resultFilters: 'phraseMatch',
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName('dccontent');
		if(hover[0] != null){
			if (ex) {
				var h = '1000px';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = '';
			}
		}
	}
		
	if({/literal}SUGAR.AutoComplete.{$ac_key}.minQLen{literal} == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function () {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.sendRequest('');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('click', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('click');
		});
		
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('dblclick', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('dblclick');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('focus');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mouseup', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mouseup');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mousedown', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mousedown');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('blur', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('blur');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = false;
			var selectElem = document.getElementById("{/literal}{$fields.management_approval_c.name}{literal}");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputImage.ancestor().on('click', function () {
		if (SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.blur();
		} else {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('query', function () {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.set('value', '');
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('visibleChange', function (e) {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('select', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
{/literal}
{/if}

{else}

{counter name="panelFieldCount"}


{if is_string($fields.management_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.management_approval_c.name}" value="{ $fields.management_approval_c.options }">
{ $fields.management_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.management_approval_c.name}" value="{ $fields.management_approval_c.value }">
{ $fields.management_approval_c.options[$fields.management_approval_c.value]}
{/if}

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>


<td valign="top" id='_label' width='12.5%' data-total-columns="2" scope="col">
&nbsp;
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >



</td>


{if $fields.management_remarks_c.acl > 1 || ($showDetailData && $fields.management_remarks_c.acl > 0)}

<td valign="top" id='management_remarks_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MANAGEMENT_REMARKS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.management_remarks_c.acl > 1}

{counter name="panelFieldCount"}

{if empty($fields.management_remarks_c.value)}
{assign var="value" value=$fields.management_remarks_c.default_value }
{else}
{assign var="value" value=$fields.management_remarks_c.value }
{/if}
<textarea  id='{$fields.management_remarks_c.name}' name='{$fields.management_remarks_c.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.management_remarks_c.name|escape:'html'|url2html|nl2br}">{$fields.management_remarks_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.ceo_apv_title_c.acl > 1 || ($showDetailData && $fields.ceo_apv_title_c.acl > 0)}

<td valign="top" id='ceo_apv_title_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_APV_TITLE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.ceo_apv_title_c.acl > 1}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.ceo_apv_title_c.name}"><p>Approval by the CEO&#039;s Office</p></span>

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.ceo_apv_title_c.name}">{sugar_literal content=$fields.ceo_apv_title_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.ceo_office_approval_c.acl > 1 || ($showDetailData && $fields.ceo_office_approval_c.acl > 0)}

<td valign="top" id='ceo_office_approval_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_OFFICE_APPROVAL' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.ceo_office_approval_c.acl > 1}

{counter name="panelFieldCount"}

{if !isset($config.enable_autocomplete) || $config.enable_autocomplete==false}
<select name="{$fields.ceo_office_approval_c.name}" 
id="{$fields.ceo_office_approval_c.name}" 
title=''       
>
{if isset($fields.ceo_office_approval_c.value) && $fields.ceo_office_approval_c.value != ''}
{html_options options=$fields.ceo_office_approval_c.options selected=$fields.ceo_office_approval_c.value}
{else}
{html_options options=$fields.ceo_office_approval_c.options selected=$fields.ceo_office_approval_c.default}
{/if}
</select>
{else}
{assign var="field_options" value=$fields.ceo_office_approval_c.options }
{capture name="field_val"}{$fields.ceo_office_approval_c.value}{/capture}
{assign var="field_val" value=$smarty.capture.field_val}
{capture name="ac_key"}{$fields.ceo_office_approval_c.name}{/capture}
{assign var="ac_key" value=$smarty.capture.ac_key}
<select style='display:none' name="{$fields.ceo_office_approval_c.name}" 
id="{$fields.ceo_office_approval_c.name}" 
title=''          
>
{if isset($fields.ceo_office_approval_c.value) && $fields.ceo_office_approval_c.value != ''}
{html_options options=$fields.ceo_office_approval_c.options selected=$fields.ceo_office_approval_c.value}
{else}
{html_options options=$fields.ceo_office_approval_c.options selected=$fields.ceo_office_approval_c.default}
{/if}
</select>
<input
id="{$fields.ceo_office_approval_c.name}-input"
name="{$fields.ceo_office_approval_c.name}-input"
size="30"
value="{$field_val|lookup:$field_options}"
type="text" style="vertical-align: top;">
<span class="id-ff multiple">
<button type="button"><img src="{sugar_getimagepath file="id-ff-down.png"}" id="{$fields.ceo_office_approval_c.name}-image"></button><button type="button"
id="btn-clear-{$fields.ceo_office_approval_c.name}-input"
title="Clear"
onclick="SUGAR.clearRelateField(this.form, '{$fields.ceo_office_approval_c.name}-input', '{$fields.ceo_office_approval_c.name}');sync_{$fields.ceo_office_approval_c.name}()"><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
{literal}
<script>
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal} = [];
	{/literal}

			{literal}
		(function (){
			var selectElem = document.getElementById("{/literal}{$fields.ceo_office_approval_c.name}{literal}");
			
			if (typeof select_defaults =="undefined")
				select_defaults = [];
			
			select_defaults[selectElem.id] = {key:selectElem.value,text:''};

			//get default
			for (i=0;i<selectElem.options.length;i++){
				if (selectElem.options[i].value==selectElem.value)
					select_defaults[selectElem.id].text = selectElem.options[i].innerHTML;
			}

			//SUGAR.AutoComplete.{$ac_key}.ds = 
			//get options array from vardefs
			var options = SUGAR.AutoComplete.getOptionsArray("");

			YUI().use('datasource', 'datasource-jsonschema',function (Y) {
				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds = new Y.DataSource.Function({
				    source: function (request) {
				    	var ret = [];
				    	for (i=0;i<selectElem.options.length;i++)
				    		if (!(selectElem.options[i].value=='' && selectElem.options[i].innerHTML==''))
				    			ret.push({'key':selectElem.options[i].value,'text':selectElem.options[i].innerHTML});
				    	return ret;
				    }
				});
			});
		})();
		{/literal}
	
	{literal}
		YUI().use("autocomplete", "autocomplete-filters", "autocomplete-highlighters", "node","node-event-simulate", function (Y) {
	{/literal}
			
	SUGAR.AutoComplete.{$ac_key}.inputNode = Y.one('#{$fields.ceo_office_approval_c.name}-input');
	SUGAR.AutoComplete.{$ac_key}.inputImage = Y.one('#{$fields.ceo_office_approval_c.name}-image');
	SUGAR.AutoComplete.{$ac_key}.inputHidden = Y.one('#{$fields.ceo_office_approval_c.name}');
	
			{literal}
			function SyncToHidden(selectme){
				var selectElem = document.getElementById("{/literal}{$fields.ceo_office_approval_c.name}{literal}");
				var doSimulateChange = false;
				
				if (selectElem.value!=selectme)
					doSimulateChange=true;
				
				selectElem.value=selectme;

				for (i=0;i<selectElem.options.length;i++){
					selectElem.options[i].selected=false;
					if (selectElem.options[i].value==selectme)
						selectElem.options[i].selected=true;
				}

				if (doSimulateChange)
					SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('change');
			}

			//global variable 
			sync_{/literal}{$fields.ceo_office_approval_c.name}{literal} = function(){
				SyncToHidden();
			}
			function syncFromHiddenToWidget(){

				var selectElem = document.getElementById("{/literal}{$fields.ceo_office_approval_c.name}{literal}");

				//if select no longer on page, kill timer
				if (selectElem==null || selectElem.options == null)
					return;

				var currentvalue = SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value');

				SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.simulate('keyup');

				for (i=0;i<selectElem.options.length;i++){

					if (selectElem.options[i].value==selectElem.value && document.activeElement != document.getElementById('{/literal}{$fields.ceo_office_approval_c.name}-input{literal}'))
						SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value',selectElem.options[i].innerHTML);
				}
			}

            YAHOO.util.Event.onAvailable("{/literal}{$fields.ceo_office_approval_c.name}{literal}", syncFromHiddenToWidget);
		{/literal}

		SUGAR.AutoComplete.{$ac_key}.minQLen = 0;
		SUGAR.AutoComplete.{$ac_key}.queryDelay = 0;
		SUGAR.AutoComplete.{$ac_key}.numOptions = {$field_options|@count};
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 300) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 200;
			{literal}
		}
		{/literal}
		if(SUGAR.AutoComplete.{$ac_key}.numOptions >= 3000) {literal}{
			{/literal}
			SUGAR.AutoComplete.{$ac_key}.minQLen = 1;
			SUGAR.AutoComplete.{$ac_key}.queryDelay = 500;
			{literal}
		}
		{/literal}
		
	SUGAR.AutoComplete.{$ac_key}.optionsVisible = false;
	
	{literal}
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.plug(Y.Plugin.AutoComplete, {
		activateFirstItem: true,
		{/literal}
		minQueryLength: SUGAR.AutoComplete.{$ac_key}.minQLen,
		queryDelay: SUGAR.AutoComplete.{$ac_key}.queryDelay,
		zIndex: 99999,

				
		{literal}
		source: SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.ds,
		
		resultTextLocator: 'text',
		resultHighlighter: 'phraseMatch',
		resultFilters: 'phraseMatch',
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover = function(ex){
		var hover = YAHOO.util.Dom.getElementsByClassName('dccontent');
		if(hover[0] != null){
			if (ex) {
				var h = '1000px';
				hover[0].style.height = h;
			}
			else{
				hover[0].style.height = '';
			}
		}
	}
		
	if({/literal}SUGAR.AutoComplete.{$ac_key}.minQLen{literal} == 0){
		// expand the dropdown options upon focus
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function () {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.sendRequest('');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = true;
		});
	}

			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('click', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('click');
		});
		
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('dblclick', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('dblclick');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('focus', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('focus');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mouseup', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mouseup');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('mousedown', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('mousedown');
		});

		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.on('blur', function(e) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.simulate('blur');
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible = false;
			var selectElem = document.getElementById("{/literal}{$fields.ceo_office_approval_c.name}{literal}");
			//if typed value is a valid option, do nothing
			for (i=0;i<selectElem.options.length;i++)
				if (selectElem.options[i].innerHTML==SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.get('value'))
					return;
			
			//typed value is invalid, so set the text and the hidden to blank
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.set('value', select_defaults[selectElem.id].text);
			SyncToHidden(select_defaults[selectElem.id].key);
		});
	
	// when they click on the arrow image, toggle the visibility of the options
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputImage.ancestor().on('click', function () {
		if (SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.optionsVisible) {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.blur();
		} else {
			SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.focus();
		}
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('query', function () {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputHidden.set('value', '');
	});

	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('visibleChange', function (e) {
		SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.expandHover(e.newVal); // expand
	});

	// when they select an option, set the hidden input with the KEY, to be saved
	SUGAR.AutoComplete.{/literal}{$ac_key}{literal}.inputNode.ac.on('select', function(e) {
		SyncToHidden(e.result.raw.key);
	});
 
});
</script> 
{/literal}
{/if}

{else}

{counter name="panelFieldCount"}


{if is_string($fields.ceo_office_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.ceo_office_approval_c.name}" value="{ $fields.ceo_office_approval_c.options }">
{ $fields.ceo_office_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.ceo_office_approval_c.name}" value="{ $fields.ceo_office_approval_c.value }">
{ $fields.ceo_office_approval_c.options[$fields.ceo_office_approval_c.value]}
{/if}

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>


<td valign="top" id='_label' width='12.5%' data-total-columns="2" scope="col">
&nbsp;
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >



</td>


{if $fields.ceo_remarks_c.acl > 1 || ($showDetailData && $fields.ceo_remarks_c.acl > 0)}

<td valign="top" id='ceo_remarks_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_REMARKS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.ceo_remarks_c.acl > 1}

{counter name="panelFieldCount"}

{if empty($fields.ceo_remarks_c.value)}
{assign var="value" value=$fields.ceo_remarks_c.default_value }
{else}
{assign var="value" value=$fields.ceo_remarks_c.value }
{/if}
<textarea  id='{$fields.ceo_remarks_c.name}' name='{$fields.ceo_remarks_c.name}'
rows="4"
cols="20"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.ceo_remarks_c.name|escape:'html'|url2html|nl2br}">{$fields.ceo_remarks_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_EDITVIEW_PANEL4").style.display='none';</script>
{/if}
</div>    <div id='tabcontent5'>
<div id="detailpanel_6" class="{$def.templateMeta.panelClass|default:'edit view edit508'}">
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<table width="100%" border="0" cellspacing="1" cellpadding="0"  id='LBL_EDITVIEW_PANEL1'  class="yui3-skin-sam edit view panelContainer">
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.assigned_user_name.acl > 1 || ($showDetailData && $fields.assigned_user_name.acl > 0)}

<td valign="top" id='assigned_user_name_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_ASSIGNED_TO_NAME' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.assigned_user_name.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.assigned_user_name.name}" class="sqsEnabled" tabindex="0" id="{$fields.assigned_user_name.name}" size="" value="{$fields.assigned_user_name.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.assigned_user_name.id_name}" 
id="{$fields.assigned_user_name.id_name}" 
value="{$fields.assigned_user_id.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.assigned_user_name.name}" id="btn_{$fields.assigned_user_name.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.assigned_user_name.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"assigned_user_id","user_name":"assigned_user_name"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.assigned_user_name.name}" id="btn_clr_{$fields.assigned_user_name.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.assigned_user_name.name}', '{$fields.assigned_user_name.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.assigned_user_name.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="assigned_user_id" class="sugar_field" data-id-value="{$fields.assigned_user_id.value}">{$fields.assigned_user_name.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.approval_steps_c.acl > 1 || ($showDetailData && $fields.approval_steps_c.acl > 0)}

<td valign="top" id='approval_steps_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_STEPS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.approval_steps_c.acl > 1}

{counter name="panelFieldCount"}

{if strlen($fields.approval_steps_c.value) <= 0}
{assign var="value" value=$fields.approval_steps_c.default_value }
{else}
{assign var="value" value=$fields.approval_steps_c.value }
{/if}  
<input type='text' name='{$fields.approval_steps_c.name}' 
id='{$fields.approval_steps_c.name}' size='30' maxlength='1' value='{sugar_number_format precision=0 var=$value}' title='' tabindex='0'    >

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.approval_steps_c.name}">
{sugar_number_format precision=0 var=$fields.approval_steps_c.value}
</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_EDITVIEW_PANEL1").style.display='none';</script>
{/if}
</div></div>

<script language="javascript">
    var _form_id = '{$form_id}';
    {literal}
    SUGAR.util.doWhen(function(){
        _form_id = (_form_id == '') ? 'EditView' : _form_id;
        return document.getElementById(_form_id) != null;
    }, SUGAR.themes.actionMenu);
    {/literal}
</script>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit">
<tr>
<td class="buttons">
{assign var='place' value="_FOOTER"}
<!-- to be used for id for buttons with custom code in def files-->
<div class="action_buttons">{if $bean->aclAccess("save")}<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('EditView'); {if $isDuplicate}_form.return_id.value=''; {/if}_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" id="SAVE_FOOTER">{/if}  {if !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($smarty.request.return_id))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" type="button" id="CANCEL_FOOTER"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($fields.id.value))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_FOOTER"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && empty($fields.id.value)) && empty($smarty.request.return_id)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_FOOTER"> {elseif !empty($smarty.request.return_action) && !empty($smarty.request.return_module)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action={$smarty.request.return_action}&module={$smarty.request.return_module|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_FOOTER"> {elseif empty($smarty.request.return_action) || empty($smarty.request.return_id) && !empty($fields.id.value)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=AOS_Quotes'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_FOOTER"> {else}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_FOOTER"> {/if} {if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AOS_Quotes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}<div class="clear"></div></div>
</td>
<td align='right' class="edit-view-pagination">
</td>
</tr>
</table>
</form>
{$set_focus_block}
<script>SUGAR.util.doWhen("document.getElementById('EditView') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script>{sugar_getscript file="cache/include/javascript/sugar_grp_yui_widgets.js"}
<script type="text/javascript">
var EditView_tabs = new YAHOO.widget.TabView("EditView_tabs");
EditView_tabs.selectTab(0);
</script>
<script type="text/javascript">
YAHOO.util.Event.onContentReady("EditView",
    function () {ldelim} initEditView(document.forms.EditView) {rdelim});
//window.setTimeout(, 100);
window.onbeforeunload = function () {ldelim} return onUnloadEditView(); {rdelim};
// bug 55468 -- IE is too aggressive with onUnload event
if ($.browser.msie) {ldelim}
$(document).ready(function() {ldelim}
    $(".collapseLink,.expandLink").click(function (e) {ldelim} e.preventDefault(); {rdelim});
  {rdelim});
{rdelim}
</script>
{/if}{literal}
<script type="text/javascript">
addForm('EditView');addToValidate('EditView', 'name', 'name', true,'{/literal}{sugar_translate label='LBL_NAME' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'date_entered_date', 'date', false,'Date Created' );
addToValidate('EditView', 'date_modified_date', 'date', false,'Date Modified' );
addToValidate('EditView', 'modified_user_id', 'assigned_user_name', false,'{/literal}{sugar_translate label='LBL_MODIFIED' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'modified_by_name', 'relate', false,'{/literal}{sugar_translate label='LBL_MODIFIED_NAME' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'created_by', 'assigned_user_name', false,'{/literal}{sugar_translate label='LBL_CREATED' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'created_by_name', 'relate', false,'{/literal}{sugar_translate label='LBL_CREATED' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'description', 'text', false,'{/literal}{sugar_translate label='LBL_DESCRIPTION' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'deleted', 'bool', false,'{/literal}{sugar_translate label='LBL_DELETED' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'assigned_user_id', 'relate', false,'{/literal}{sugar_translate label='LBL_ASSIGNED_TO_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'assigned_user_name', 'relate', false,'{/literal}{sugar_translate label='LBL_ASSIGNED_TO_NAME' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'approval_issue', 'text', false,'{/literal}{sugar_translate label='LBL_APPROVAL_ISSUE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'billing_account_id', 'id', false,'{/literal}{sugar_translate label='' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'billing_account', 'relate', false,'{/literal}{sugar_translate label='LBL_BILLING_ACCOUNT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'billing_contact_id', 'id', false,'{/literal}{sugar_translate label='' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'billing_contact', 'relate', false,'{/literal}{sugar_translate label='LBL_BILLING_CONTACT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'billing_address_street', 'varchar', false,'{/literal}{sugar_translate label='LBL_BILLING_ADDRESS_STREET' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'billing_address_city', 'varchar', false,'{/literal}{sugar_translate label='LBL_BILLING_ADDRESS_CITY' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'billing_address_state', 'varchar', false,'{/literal}{sugar_translate label='LBL_BILLING_ADDRESS_STATE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'billing_address_postalcode', 'varchar', false,'{/literal}{sugar_translate label='LBL_BILLING_ADDRESS_POSTALCODE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'billing_address_country', 'varchar', false,'{/literal}{sugar_translate label='LBL_BILLING_ADDRESS_COUNTRY' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'shipping_address_street', 'varchar', false,'{/literal}{sugar_translate label='LBL_SHIPPING_ADDRESS_STREET' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'shipping_address_city', 'varchar', false,'{/literal}{sugar_translate label='LBL_SHIPPING_ADDRESS_CITY' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'shipping_address_state', 'varchar', false,'{/literal}{sugar_translate label='LBL_SHIPPING_ADDRESS_STATE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'shipping_address_postalcode', 'varchar', false,'{/literal}{sugar_translate label='LBL_SHIPPING_ADDRESS_POSTALCODE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'shipping_address_country', 'varchar', false,'{/literal}{sugar_translate label='LBL_SHIPPING_ADDRESS_COUNTRY' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'expiration', 'date', true,'{/literal}{sugar_translate label='LBL_EXPIRATION' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'number', 'int', false,'{/literal}{sugar_translate label='LBL_QUOTE_NUMBER' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'opportunity_id', 'id', false,'{/literal}{sugar_translate label='' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'opportunity', 'relate', false,'{/literal}{sugar_translate label='LBL_OPPORTUNITY' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'template_ddown_c[]', 'multienum', false,'{/literal}{sugar_translate label='LBL_TEMPLATE_DDOWN_C' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'line_items', 'function', false,'{/literal}{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'total_amt', 'currency', false,'{/literal}{sugar_translate label='LBL_TOTAL_AMT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'total_amt_usdollar', 'currency', false,'{/literal}{sugar_translate label='LBL_TOTAL_AMT_USDOLLAR' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'subtotal_amount', 'currency', false,'{/literal}{sugar_translate label='LBL_SUBTOTAL_AMOUNT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'subtotal_amount_usdollar', 'currency', false,'{/literal}{sugar_translate label='LBL_SUBTOTAL_AMOUNT_USDOLLAR' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'discount_amount', 'currency', false,'{/literal}{sugar_translate label='LBL_DISCOUNT_AMOUNT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'discount_amount_usdollar', 'currency', false,'{/literal}{sugar_translate label='LBL_DISCOUNT_AMOUNT_USDOLLAR' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'tax_amount', 'currency', false,'{/literal}{sugar_translate label='LBL_TAX_AMOUNT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'tax_amount_usdollar', 'currency', false,'{/literal}{sugar_translate label='LBL_TAX_AMOUNT_USDOLLAR' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'shipping_amount', 'currency', false,'{/literal}{sugar_translate label='LBL_SHIPPING_AMOUNT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'shipping_amount_usdollar', 'currency', false,'{/literal}{sugar_translate label='LBL_SHIPPING_AMOUNT_USDOLLAR' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'shipping_tax', 'enum', false,'{/literal}{sugar_translate label='LBL_SHIPPING_TAX' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'shipping_tax_amt', 'currency', false,'{/literal}{sugar_translate label='LBL_SHIPPING_TAX_AMT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'shipping_tax_amt_usdollar', 'currency', false,'{/literal}{sugar_translate label='LBL_SHIPPING_TAX_AMT_USDOLLAR' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'total_amount', 'currency', false,'{/literal}{sugar_translate label='LBL_GRAND_TOTAL' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'total_amount_usdollar', 'currency', false,'{/literal}{sugar_translate label='LBL_GRAND_TOTAL_USDOLLAR' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'currency_id', 'id', false,'{/literal}{sugar_translate label='LBL_CURRENCY' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'stage', 'enum', true,'{/literal}{sugar_translate label='LBL_STAGE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'term', 'enum', false,'{/literal}{sugar_translate label='LBL_TERM' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'terms_c', 'text', false,'{/literal}{sugar_translate label='LBL_TERMS_C' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'approval_status', 'enum', false,'{/literal}{sugar_translate label='LBL_APPROVAL_STATUS' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'invoice_status', 'enum', false,'{/literal}{sugar_translate label='LBL_INVOICE_STATUS' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'subtotal_tax_amount', 'currency', false,'{/literal}{sugar_translate label='LBL_SUBTOTAL_TAX_AMOUNT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'subtotal_tax_amount_usdollar', 'currency', false,'{/literal}{sugar_translate label='LBL_SUBTOTAL_TAX_AMOUNT_USDOLLAR' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'leads_aos_quotes_1_name', 'relate', false,'{/literal}{sugar_translate label='LBL_LEADS_AOS_QUOTES_1_FROM_LEADS_TITLE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'additional_revenue_c', 'currency', true,'{/literal}{sugar_translate label='LBL_ADDITIONAL_REVENUE ' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'approval_end_date_c_date', 'date', false,'Approval End Date' );
addToValidate('EditView', 'approval_routine_c', 'relate', false,'{/literal}{sugar_translate label='LBL_APPROVAL_ROUTINE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'approval_stamp_c', 'varchar', false,'{/literal}{sugar_translate label='LBL_APPROVAL_STAMP' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'approval_start_date_c_date', 'date', false,'Approval Start Date' );
addToValidate('EditView', 'approval_steps_c', 'int', false,'{/literal}{sugar_translate label='LBL_APPROVAL_STEPS' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'apv_rej_notified_c', 'bool', false,'{/literal}{sugar_translate label='LBL_APV_REJ_NOTIFIED' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'budget_total_c', 'currency', false,'{/literal}{sugar_translate label='LBL_BUDGET_TOTAL' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ceo_approval_by_c', 'relate', false,'{/literal}{sugar_translate label='LBL_CEO_APPROVAL_BY' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ceo_apv_title_c', 'html', false,'{/literal}{sugar_translate label='LBL_CEO_APV_TITLE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ceo_date_c_date', 'date', false,'CEO Office Response Date' );
addToValidate('EditView', 'ceo_flg_c', 'bool', false,'{/literal}{sugar_translate label='LBL_CEO_FLG' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ceo_office_approval_c', 'enum', false,'{/literal}{sugar_translate label='LBL_CEO_OFFICE_APPROVAL' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ceo_remarks_c', 'text', false,'{/literal}{sugar_translate label='LBL_CEO_REMARKS' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ceo_stamp_c', 'relate', false,'{/literal}{sugar_translate label='LBL_CEO_STAMP' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'clarification_done_c', 'bool', false,'{/literal}{sugar_translate label='LBL_CLARIFICATION_DONE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'day_spending_c', 'currency', false,'{/literal}{sugar_translate label='LBL_DAY_SPENDING' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ddm_approval_by_c', 'relate', false,'{/literal}{sugar_translate label='LBL_DDM_APPROVAL_BY' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ddm_approval_c', 'enum', false,'{/literal}{sugar_translate label='LBL_DDM_APPROVAL' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ddm_approval_title_c', 'html', false,'{/literal}{sugar_translate label='LBL_DDM_APPROVAL_TITLE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ddm_date_c_date', 'date', false,'DDM Response  Date' );
addToValidate('EditView', 'ddm_flg_c', 'bool', false,'{/literal}{sugar_translate label='LBL_DDM_FLG' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ddm_remarks_c', 'text', false,'{/literal}{sugar_translate label='LBL_DDM_REMARKS' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'ddm_stamp_c', 'relate', false,'{/literal}{sugar_translate label='LBL_DDM_STAMP' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'event_brief_c', 'wysiwyg', false,'{/literal}{sugar_translate label='LBL_EVENT_BRIEF' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'event_date_c', 'date', true,'{/literal}{sugar_translate label='LBL_EVENT_DATE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'event_days_c', 'int', true,'{/literal}{sugar_translate label='LBL_EVENT_DAYS' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'event_month_c', 'varchar', false,'{/literal}{sugar_translate label='LBL_EVENT_MONTH' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'event_year_c', 'varchar', false,'{/literal}{sugar_translate label='LBL_EVENT_YEAR' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'expected_revenue_c', 'currency', true,'{/literal}{sugar_translate label='LBL_EXPECTED_REVENUE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'final_approval_text_c', 'varchar', false,'{/literal}{sugar_translate label='LBL_FINAL_APPROVAL_TEXT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'final_approver_c', 'relate', false,'{/literal}{sugar_translate label='LBL_FINAL_APPROVER' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'gor_commitment_c', 'currency', true,'{/literal}{sugar_translate label='LBL_GOR_COMMITMENT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'gor_commitment_limit_c', 'currency', false,'{/literal}{sugar_translate label='LBL_GOR_COMMITMENT_LIMIT' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'gor_revenue_c', 'currency', true,'{/literal}{sugar_translate label='LBL_GOR_REVENUE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'help_notifications_c', 'bool', false,'{/literal}{sugar_translate label='LBL_HELP_NOTIFICATIONS' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'management_approval_by_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MANAGEMENT_APPROVAL_BY' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'management_approval_c', 'enum', false,'{/literal}{sugar_translate label='LBL_MANAGEMENT_APPROVAL' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'management_date_c_date', 'date', false,'Management Response Date' );
addToValidate('EditView', 'management_remarks_c', 'text', false,'{/literal}{sugar_translate label='LBL_MANAGEMENT_REMARKS' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'manually_calculate_c', 'bool', false,'{/literal}{sugar_translate label='LBL_MANUALLY_CALCULATE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'member_1_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_1' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'member_2_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_2' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'member_3_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_3' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'member_4_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_4' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'member_5_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_5' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'member_6_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_6' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'memstamp1_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP1' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'memstamp2_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP2' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'memstamp3_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP3' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'memstamp4_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP4' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'memstamp5_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP5' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'memstamp6_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP6' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'mgmt_apv_titile_c', 'html', false,'{/literal}{sugar_translate label='LBL_MGMT_APV_TITILE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'mgmt_stamp_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MGMT_STAMP' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'mmgt_flg_c', 'bool', false,'{/literal}{sugar_translate label='LBL_MMGT_FLG' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'no_of_delegates_c', 'int', true,'{/literal}{sugar_translate label='LBL_NO_OF_DELEGATES' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'prepared_by_c', 'relate', false,'{/literal}{sugar_translate label='LBL_PREPARED_BY' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'recommendation_c', 'wysiwyg', false,'{/literal}{sugar_translate label='LBL_RECOMMENDATION' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'remarks_c', 'text', false,'{/literal}{sugar_translate label='LBL_REMARKS' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'revenue_description_c', 'text', true,'{/literal}{sugar_translate label='LBL_REVENUE_DESCRIPTION' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'revenue_total_c', 'currency', false,'{/literal}{sugar_translate label='LBL_REVENUE_TOTAL' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'send_for_approval_c', 'bool', false,'{/literal}{sugar_translate label='LBL_SEND_FOR_APPROVAL' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'srp_stdapproval_routine_id_c', 'id', false,'{/literal}{sugar_translate label='LBL_APPROVAL_ROUTINE_SRP_STDAPPROVAL_ROUTINE_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'title1_c', 'varchar', false,'{/literal}{sugar_translate label='LBL_TITLE1' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'title2_c', 'varchar', false,'{/literal}{sugar_translate label='LBL_TITLE2' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id10_c', 'id', false,'{/literal}{sugar_translate label='LBL_FINAL_APPROVER_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id11_c', 'id', false,'{/literal}{sugar_translate label='LBL_DDM_STAMP_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id12_c', 'id', false,'{/literal}{sugar_translate label='LBL_MGMT_STAMP_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id13_c', 'id', false,'{/literal}{sugar_translate label='LBL_CEO_STAMP_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id14_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP1_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id15_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP2_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id16_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP3_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id17_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP4_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id18_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP5_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id19_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMSTAMP6_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id1_c', 'id', false,'{/literal}{sugar_translate label='LBL_MANAGEMENT_APPROVAL_BY_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id2_c', 'id', false,'{/literal}{sugar_translate label='LBL_CEO_APPROVAL_BY_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id3_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_1_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id4_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_2_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id5_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_3_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id6_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_4_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id7_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_5_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id8_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_6_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id9_c', 'id', false,'{/literal}{sugar_translate label='LBL_PREPARED_BY_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'user_id_c', 'id', false,'{/literal}{sugar_translate label='LBL_DDM_APPROVAL_BY_USER_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'venue_c', 'relate', true,'{/literal}{sugar_translate label='LBL_VENUE' module='AOS_Quotes' for_js=true}{literal}' );
addToValidate('EditView', 'vnp_venues_id_c', 'id', false,'{/literal}{sugar_translate label='LBL_VENUE_VNP_VENUES_ID' module='AOS_Quotes' for_js=true}{literal}' );
addToValidateBinaryDependency('EditView', 'assigned_user_name', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='AOS_Quotes' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_ASSIGNED_TO' module='AOS_Quotes' for_js=true}{literal}', 'assigned_user_id' );
addToValidateBinaryDependency('EditView', 'venue_c', 'alpha', true,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='AOS_Quotes' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_VENUE' module='AOS_Quotes' for_js=true}{literal}', 'vnp_venues_id_c' );
</script><script language="javascript">if(typeof sqs_objects == 'undefined'){var sqs_objects = new Array;}sqs_objects['EditView_venue_c']={"form":"EditView","method":"query","modules":["VNP_Venues"],"group":"or","field_list":["name","id"],"populate_list":["venue_c","vnp_venues_id_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_assigned_user_name']={"form":"EditView","method":"get_user_array","field_list":["user_name","id"],"populate_list":["assigned_user_name","assigned_user_id"],"required_list":["assigned_user_id"],"conditions":[{"name":"user_name","op":"like_custom","end":"%","value":""}],"limit":"30","no_match_text":"No Match"};</script>{/literal}
