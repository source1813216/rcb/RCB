
{php}
global $current_user,$sugar_config;
$this->_tpl_vars['current_user'] = $current_user->id;
$this->_tpl_vars['is_admin_user'] = $current_user->is_admin;
$this->_tpl_vars['current_theme'] = $current_user->getPreference('user_theme');
$this->_tpl_vars['suite_valid_version'] =  true;
if(preg_match('/(7\.7\.[0-9])/', $sugar_config['suitecrm_version']) || $sugar_config['suitecrm_version'] == '7.7'){
$this->_tpl_vars['suite_valid_version'] = false;
}
{/php}
{if $current_theme == 'SuiteP'}

<script language="javascript">
    {literal}
    SUGAR.util.doWhen(function () {
        return $("#contentTable").length == 0;
    }, SUGAR.themes.actionMenu);
    {/literal}
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="{$module}">
<input type="hidden" name="record" value="{$fields.id.value}">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="{$offset}">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
{if !$config.enable_action_menu}
<div class="buttons">
{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AOS_Quotes'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} 
{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AOS_Quotes'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} 
<input type="button" class="button" onClick="showPopup('pdf');" value="{$MOD.LBL_PRINT_AS_PDF}"/>
<input type="button" class="button" onClick="showPopup('emailpdf');" value="{$MOD.LBL_EMAIL_PDF}"/>
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AOS_Quotes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>                    {/if}
</form>
</div>
</td>
<td align="right" width="20%" class="buttons">{$ADMIN_EDIT}
</td>
</tr>
</table>
{sugar_include include=$includes}
<div class="detail-view">
{if $suite_valid_version}
<div class="mobile-pagination">{$PAGINATION}</div>
{/if}

<ul class="nav nav-tabs">


<li role="presentation" class="active">
<a id="tab0" data-toggle="tab" class="hidden-xs">
{sugar_translate label='LBL_DETAILVIEW_PANEL1' module='AOS_Quotes'}
</a>


<a id="xstab0" href="#" class="visible-xs first-tab-xs dropdown-toggle" data-toggle="dropdown">
{sugar_translate label='LBL_DETAILVIEW_PANEL1' module='AOS_Quotes'}
</a>
<ul id="first-tab-menu-xs" class="dropdown-menu">
<li role="presentation">
<a id="tab1" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-1');">
{sugar_translate label='LBL_DETAILVIEW_PANEL1' module='AOS_Quotes'}
</a>
</li>
<li role="presentation">
<a id="tab2" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-2');">
{sugar_translate label='LBL_PANEL_OVERVIEW' module='AOS_Quotes'}
</a>
</li>
<li role="presentation">
<a id="tab3" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-3');">
{sugar_translate label='LBL_DETAILVIEW_PANEL2' module='AOS_Quotes'}
</a>
</li>
<li role="presentation">
<a id="tab4" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-4');">
{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes'}
</a>
</li>
<li role="presentation">
<a id="tab5" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-5');">
{sugar_translate label='LBL_DETAILVIEW_PANEL3' module='AOS_Quotes'}
</a>
</li>
<li role="presentation">
<a id="tab6" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-6');">
{sugar_translate label='LBL_PANEL_ASSIGNMENT' module='AOS_Quotes'}
</a>
</li>
</ul>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab1" data-toggle="tab">
{sugar_translate label='LBL_PANEL_OVERVIEW' module='AOS_Quotes'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab2" data-toggle="tab">
{sugar_translate label='LBL_DETAILVIEW_PANEL2' module='AOS_Quotes'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab3" data-toggle="tab">
{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab4" data-toggle="tab">
{sugar_translate label='LBL_DETAILVIEW_PANEL3' module='AOS_Quotes'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab5" data-toggle="tab">
{sugar_translate label='LBL_PANEL_ASSIGNMENT' module='AOS_Quotes'}
</a>
</li>
{if $config.enable_action_menu and $config.enable_action_menu != false}
<li id="tab-actions" class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">ACTIONS<span class="suitepicon suitepicon-action-caret"></span></a>
<ul class="dropdown-menu">
<li>{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AOS_Quotes'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} </li>
<li>{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AOS_Quotes'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} </li>
<li><input type="button" class="button" onClick="showPopup('pdf');" value="{$MOD.LBL_PRINT_AS_PDF}"/></li>
<li><input type="button" class="button" onClick="showPopup('emailpdf');" value="{$MOD.LBL_EMAIL_PDF}"/></li>
<li>{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AOS_Quotes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}</li>
</ul>        </li>
{if $suite_valid_version}
<li class="tab-inline-pagination">
{$PAGINATION}
</li>
{/if}
{/if}
</ul>
<div class="clearfix"></div>

<div class="tab-content">

<div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-0'>





<div class="row detail-view-row">


{if $fields.approval_status.acl > 1 || $fields.approval_status.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_STATUS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="approval_status"  >

{if !$fields.approval_status.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.approval_status.options)}
<input type="hidden" class="sugar_field" id="{$fields.approval_status.name}" value="{ $fields.approval_status.options }">
{ $fields.approval_status.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.approval_status.name}" value="{ $fields.approval_status.value }">
{ $fields.approval_status.options[$fields.approval_status.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.prepared_by_c.acl > 1 || $fields.prepared_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PREPARED_BY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="prepared_by_c"  >

{if !$fields.prepared_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id9_c" class="sugar_field" data-id-value="{$fields.user_id9_c.value}">{$fields.prepared_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.title1_c.acl > 1 || $fields.title1_c.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TITLE1' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="varchar" field="title1_c" colspan='3' >

{if !$fields.title1_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.title1_c.value) <= 0}
{assign var="value" value=$fields.title1_c.default_value }
{else}
{assign var="value" value=$fields.title1_c.value }
{/if} 
<span class="sugar_field" id="{$fields.title1_c.name}">{$fields.title1_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.ddm_approval_by_c.acl > 1 || $fields.ddm_approval_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_APPROVAL_BY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="ddm_approval_by_c"  >

{if !$fields.ddm_approval_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id_c" class="sugar_field" data-id-value="{$fields.user_id_c.value}">{$fields.ddm_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.ddm_date_c.acl > 1 || $fields.ddm_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_DATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="ddm_date_c"  >

{if !$fields.ddm_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.ddm_date_c.value) <= 0}
{assign var="value" value=$fields.ddm_date_c.default_value }
{else}
{assign var="value" value=$fields.ddm_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.ddm_date_c.name}">{$fields.ddm_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.management_approval_by_c.acl > 1 || $fields.management_approval_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MANAGEMENT_APPROVAL_BY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="management_approval_by_c"  >

{if !$fields.management_approval_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id1_c" class="sugar_field" data-id-value="{$fields.user_id1_c.value}">{$fields.management_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.management_date_c.acl > 1 || $fields.management_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MANAGEMENT_DATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="management_date_c"  >

{if !$fields.management_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.management_date_c.value) <= 0}
{assign var="value" value=$fields.management_date_c.default_value }
{else}
{assign var="value" value=$fields.management_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.management_date_c.name}">{$fields.management_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.ceo_approval_by_c.acl > 1 || $fields.ceo_approval_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_APPROVAL_BY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="ceo_approval_by_c"  >

{if !$fields.ceo_approval_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id2_c" class="sugar_field" data-id-value="{$fields.user_id2_c.value}">{$fields.ceo_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.ceo_date_c.acl > 1 || $fields.ceo_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_DATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="ceo_date_c"  >

{if !$fields.ceo_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.ceo_date_c.value) <= 0}
{assign var="value" value=$fields.ceo_date_c.default_value }
{else}
{assign var="value" value=$fields.ceo_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.ceo_date_c.name}">{$fields.ceo_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.title2_c.acl > 1 || $fields.title2_c.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_TITLE2' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="varchar" field="title2_c" colspan='3' >

{if !$fields.title2_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.title2_c.value) <= 0}
{assign var="value" value=$fields.title2_c.default_value }
{else}
{assign var="value" value=$fields.title2_c.value }
{/if} 
<span class="sugar_field" id="{$fields.title2_c.name}">{$fields.title2_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.member_1_c.acl > 1 || $fields.member_1_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_1' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="member_1_c"  >

{if !$fields.member_1_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id3_c" class="sugar_field" data-id-value="{$fields.user_id3_c.value}">{$fields.member_1_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.member_2_c.acl > 1 || $fields.member_2_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_2' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="member_2_c"  >

{if !$fields.member_2_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id4_c" class="sugar_field" data-id-value="{$fields.user_id4_c.value}">{$fields.member_2_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.member_3_c.acl > 1 || $fields.member_3_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_3' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="member_3_c"  >

{if !$fields.member_3_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id5_c" class="sugar_field" data-id-value="{$fields.user_id5_c.value}">{$fields.member_3_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.member_4_c.acl > 1 || $fields.member_4_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_4' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="member_4_c"  >

{if !$fields.member_4_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id6_c" class="sugar_field" data-id-value="{$fields.user_id6_c.value}">{$fields.member_4_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.member_5_c.acl > 1 || $fields.member_5_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_5' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="member_5_c"  >

{if !$fields.member_5_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id7_c" class="sugar_field" data-id-value="{$fields.user_id7_c.value}">{$fields.member_5_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.member_6_c.acl > 1 || $fields.member_6_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_6' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="member_6_c"  >

{if !$fields.member_6_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id8_c" class="sugar_field" data-id-value="{$fields.user_id8_c.value}">{$fields.member_6_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-1'>





<div class="row detail-view-row">


{if $fields.billing_account.acl > 1 || $fields.billing_account.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_BILLING_ACCOUNT' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="billing_account"  >

{if !$fields.billing_account.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.billing_account_id.value)}
{capture assign="detail_url"}index.php?module=Accounts&action=DetailView&record={$fields.billing_account_id.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="billing_account_id" class="sugar_field" data-id-value="{$fields.billing_account_id.value}">{$fields.billing_account.value}</span>
{if !empty($fields.billing_account_id.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.billing_contact.acl > 1 || $fields.billing_contact.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_BILLING_CONTACT' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="billing_contact"  >

{if !$fields.billing_contact.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.billing_contact_id.value)}
{capture assign="detail_url"}index.php?module=Contacts&action=DetailView&record={$fields.billing_contact_id.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="billing_contact_id" class="sugar_field" data-id-value="{$fields.billing_contact_id.value}">{$fields.billing_contact.value}</span>
{if !empty($fields.billing_contact_id.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.leads_aos_quotes_1_name.acl > 1 || $fields.leads_aos_quotes_1_name.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_LEADS_AOS_QUOTES_1_FROM_LEADS_TITLE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="leads_aos_quotes_1_name"  >

{if !$fields.leads_aos_quotes_1_name.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.leads_aos_quotes_1leads_ida.value)}
{capture assign="detail_url"}index.php?module=Leads&action=DetailView&record={$fields.leads_aos_quotes_1leads_ida.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="leads_aos_quotes_1leads_ida" class="sugar_field" data-id-value="{$fields.leads_aos_quotes_1leads_ida.value}">{$fields.leads_aos_quotes_1_name.value}</span>
{if !empty($fields.leads_aos_quotes_1leads_ida.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.event_date_c.acl > 1 || $fields.event_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="date" field="event_date_c"  >

{if !$fields.event_date_c.hidden}
{counter name="panelFieldCount" print=false}


{if strlen($fields.event_date_c.value) <= 0}
{assign var="value" value=$fields.event_date_c.default_value }
{else}
{assign var="value" value=$fields.event_date_c.value }
{/if}
<span class="sugar_field" id="{$fields.event_date_c.name}">{$value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.venue_c.acl > 1 || $fields.venue_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_VENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="venue_c"  >

{if !$fields.venue_c.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.vnp_venues_id_c.value)}
{capture assign="detail_url"}index.php?module=VNP_Venues&action=DetailView&record={$fields.vnp_venues_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="vnp_venues_id_c" class="sugar_field" data-id-value="{$fields.vnp_venues_id_c.value}">{$fields.venue_c.value}</span>
{if !empty($fields.vnp_venues_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.manually_calculate_c.acl > 1 || $fields.manually_calculate_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MANUALLY_CALCULATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="manually_calculate_c"  >

{if !$fields.manually_calculate_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.manually_calculate_c.value) == "1" || strval($fields.manually_calculate_c.value) == "yes" || strval($fields.manually_calculate_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.manually_calculate_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.manually_calculate_c.name}" id="{$fields.manually_calculate_c.name}" value="$fields.manually_calculate_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.no_of_delegates_c.acl > 1 || $fields.no_of_delegates_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_NO_OF_DELEGATES' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="int" field="no_of_delegates_c"  >

{if !$fields.no_of_delegates_c.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.no_of_delegates_c.name}">
{assign var="value" value=$fields.no_of_delegates_c.value }
{$value}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.event_days_c.acl > 1 || $fields.event_days_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DAYS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="int" field="event_days_c"  >

{if !$fields.event_days_c.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.event_days_c.name}">
{assign var="value" value=$fields.event_days_c.value }
{$value}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.day_spending_c.acl > 1 || $fields.day_spending_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DAY_SPENDING' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="day_spending_c"  >

{if !$fields.day_spending_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.day_spending_c.name}'>
{sugar_number_format var=$fields.day_spending_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.expected_revenue_c.acl > 1 || $fields.expected_revenue_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EXPECTED_REVENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="expected_revenue_c"  >

{if !$fields.expected_revenue_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.expected_revenue_c.name}'>
{sugar_number_format var=$fields.expected_revenue_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.gor_revenue_c.acl > 1 || $fields.gor_revenue_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_REVENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="gor_revenue_c"  >

{if !$fields.gor_revenue_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.gor_revenue_c.name}'>
{sugar_number_format var=$fields.gor_revenue_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.gor_commitment_c.acl > 1 || $fields.gor_commitment_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_COMMITMENT' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="gor_commitment_c"  >

{if !$fields.gor_commitment_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.gor_commitment_c.name}'>
{sugar_number_format var=$fields.gor_commitment_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.additional_revenue_c.acl > 1 || $fields.additional_revenue_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ADDITIONAL_REVENUE ' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="additional_revenue_c"  >

{if !$fields.additional_revenue_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.additional_revenue_c.name}'>
{sugar_number_format var=$fields.additional_revenue_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.revenue_description_c.acl > 1 || $fields.revenue_description_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_REVENUE_DESCRIPTION' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="text" field="revenue_description_c"  >

{if !$fields.revenue_description_c.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.revenue_description_c.name|escape:'html'|url2html|nl2br}">{$fields.revenue_description_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-2'>





<div class="row detail-view-row">


{if $fields.event_brief_c.acl > 1 || $fields.event_brief_c.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_BRIEF' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field " type="wysiwyg" field="event_brief_c" colspan='3' >

{if !$fields.event_brief_c.hidden}
{counter name="panelFieldCount" print=false}

<iframe
id="{$fields.event_brief_c.name}"
name="{$fields.event_brief_c.name}"
srcdoc="{$fields.event_brief_c.value}"
style="width:100%;height:500px"
></iframe>
{/if}

</div>


</div>

{/if}
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-3'>





<div class="row detail-view-row">


{if $fields.currency_id.acl > 1 || $fields.currency_id.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CURRENCY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="id" field="currency_id"  >

{if !$fields.currency_id.hidden}
{counter name="panelFieldCount" print=false}
<span id='currency_id_span'>
{$fields.currency_id.value}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.line_items.acl > 1 || $fields.line_items.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field " type="function" field="line_items" colspan='3' >

{if !$fields.line_items.hidden}
{counter name="panelFieldCount" print=false}
<span id='line_items_span'>
{$fields.line_items.value}
</span>
{/if}

</div>


</div>

{/if}
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>


{if $fields.total_amount.acl > 1 || $fields.total_amount.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_GRAND_TOTAL' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="total_amount"  >

{if !$fields.total_amount.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.total_amount.name}'>
{sugar_number_format var=$fields.total_amount.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-4'>





<div class="row detail-view-row">


{if $fields.recommendation_c.acl > 1 || $fields.recommendation_c.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RECOMMENDATION' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field " type="wysiwyg" field="recommendation_c" colspan='3' >

{if !$fields.recommendation_c.hidden}
{counter name="panelFieldCount" print=false}

<iframe
id="{$fields.recommendation_c.name}"
name="{$fields.recommendation_c.name}"
srcdoc="{$fields.recommendation_c.value}"
style="width:100%;height:500px"
></iframe>
{/if}

</div>


</div>

{/if}
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-5'>





<div class="row detail-view-row">


{if $fields.assigned_user_name.acl > 1 || $fields.assigned_user_name.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ASSIGNED_TO' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="assigned_user_name"  >

{if !$fields.assigned_user_name.hidden}
{counter name="panelFieldCount" print=false}

<span id="assigned_user_id" class="sugar_field" data-id-value="{$fields.assigned_user_id.value}">{$fields.assigned_user_name.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.approval_routine_c.acl > 1 || $fields.approval_routine_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_ROUTINE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="approval_routine_c"  >

{if !$fields.approval_routine_c.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.srp_stdapproval_routine_id_c.value)}
{capture assign="detail_url"}index.php?module=SRP_StdApproval_Routine&action=DetailView&record={$fields.srp_stdapproval_routine_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="srp_stdapproval_routine_id_c" class="sugar_field" data-id-value="{$fields.srp_stdapproval_routine_id_c.value}">{$fields.approval_routine_c.value}</span>
{if !empty($fields.srp_stdapproval_routine_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.date_entered.acl > 1 || $fields.date_entered.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_ENTERED' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="datetime" field="date_entered"  >

{if !$fields.date_entered.hidden}
{counter name="panelFieldCount" print=false}
<span id="date_entered" class="sugar_field">{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}</span>
{/if}

</div>


</div>

{/if}


{if $fields.date_modified.acl > 1 || $fields.date_modified.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_MODIFIED' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="datetime" field="date_modified"  >

{if !$fields.date_modified.hidden}
{counter name="panelFieldCount" print=false}
<span id="date_modified" class="sugar_field">{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}</span>
{/if}

</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.approval_steps_c.acl > 1 || $fields.approval_steps_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_STEPS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="int" field="approval_steps_c"  >

{if !$fields.approval_steps_c.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.approval_steps_c.name}">
{sugar_number_format precision=0 var=$fields.approval_steps_c.value}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.gor_commitment_limit_c.acl > 1 || $fields.gor_commitment_limit_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_COMMITMENT_LIMIT' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="gor_commitment_limit_c"  >

{if !$fields.gor_commitment_limit_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.gor_commitment_limit_c.name}'>
{sugar_number_format var=$fields.gor_commitment_limit_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.help_notifications_c.acl > 1 || $fields.help_notifications_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_HELP_NOTIFICATIONS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="help_notifications_c"  >

{if !$fields.help_notifications_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.help_notifications_c.value) == "1" || strval($fields.help_notifications_c.value) == "yes" || strval($fields.help_notifications_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.help_notifications_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.help_notifications_c.name}" id="{$fields.help_notifications_c.name}" value="$fields.help_notifications_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>
                    </div>
</div>

<div class="panel-content">
<div>&nbsp;</div>












</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script>            <script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
{literal}
<script type="text/javascript">

                    var selectTabDetailView = function(tab) {
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').hide();
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').eq(tab).show().addClass('active').addClass('in');
                    };

                    var selectTabOnError = function(tab) {
                        selectTabDetailView(tab);
                        $('#content ul.nav.nav-tabs > li').removeClass('active');
                        $('#content ul.nav.nav-tabs > li a').css('color', '');

                        $('#content ul.nav.nav-tabs > li').eq(tab).find('a').first().css('color', 'red');
                        $('#content ul.nav.nav-tabs > li').eq(tab).addClass('active');

                    };

                    var selectTabOnErrorInputHandle = function(inputHandle) {
                        var tab = $(inputHandle).closest('.tab-pane-NOBOOTSTRAPTOGGLER').attr('id').match(/^detailpanel_(.*)$/)[1];
                        selectTabOnError(tab);
                    };


                    $(function(){
                        $('#content ul.nav.nav-tabs > li > a[data-toggle="tab"]').click(function(e){
                            if(typeof $(this).parent().find('a').first().attr('id') != 'undefined') {
                                var tab = parseInt($(this).parent().find('a').first().attr('id').match(/^tab(.)*$/)[1]);
                                selectTabDetailView(tab);
                            }
                        });
                    });

                </script>
{/literal}
{else}

<script language="javascript">
{literal}
SUGAR.util.doWhen(function(){
    return $("#contentTable").length == 0;
}, SUGAR.themes.actionMenu);
{/literal}
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="{$module}">
<input type="hidden" name="record" value="{$fields.id.value}">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="{$offset}">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
</form>
<ul id="detail_header_action_menu" class="clickMenu fancymenu" ><li class="sugar_action_button" >{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AOS_Quotes'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} <ul id class="subnav" ><li>{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='AOS_Quotes'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} </li><li><input type="button" class="button" onClick="showPopup('pdf');" value="{$MOD.LBL_PRINT_AS_PDF}"/></li><li><input type="button" class="button" onClick="showPopup('emailpdf');" value="{$MOD.LBL_EMAIL_PDF}"/></li><li>{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=AOS_Quotes", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}</li></ul></li></ul>
</div>
</td>
<td align="right" width="20%">{$ADMIN_EDIT}
</td>
</tr>
</table>{sugar_include include=$includes}
<div id="AOS_Quotes_detailview_tabs"
class="yui-navset detailview_tabs"
>

<ul class="yui-nav">

<li><a id="tab0" href="javascript:void(0)"><em>{sugar_translate label='LBL_DETAILVIEW_PANEL1' module='AOS_Quotes'}</em></a></li>

<li><a id="tab1" href="javascript:void(0)"><em>{sugar_translate label='LBL_PANEL_OVERVIEW' module='AOS_Quotes'}</em></a></li>

<li><a id="tab2" href="javascript:void(0)"><em>{sugar_translate label='LBL_DETAILVIEW_PANEL2' module='AOS_Quotes'}</em></a></li>

<li><a id="tab3" href="javascript:void(0)"><em>{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes'}</em></a></li>

<li><a id="tab4" href="javascript:void(0)"><em>{sugar_translate label='LBL_DETAILVIEW_PANEL3' module='AOS_Quotes'}</em></a></li>

<li><a id="tab5" href="javascript:void(0)"><em>{sugar_translate label='LBL_PANEL_ASSIGNMENT' module='AOS_Quotes'}</em></a></li>
</ul>
<div class="yui-content">
<div id='tabcontent0'>
<div id='detailpanel_1' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL1' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.approval_status.acl > 1 || $fields.approval_status.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.approval_status.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_STATUS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="approval_status" width='37.5%'  >
{if !$fields.approval_status.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.approval_status.options)}
<input type="hidden" class="sugar_field" id="{$fields.approval_status.name}" value="{ $fields.approval_status.options }">
{ $fields.approval_status.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.approval_status.name}" value="{ $fields.approval_status.value }">
{ $fields.approval_status.options[$fields.approval_status.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.prepared_by_c.acl > 1 || $fields.prepared_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.prepared_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_PREPARED_BY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="prepared_by_c" width='37.5%'  >
{if !$fields.prepared_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id9_c" class="sugar_field" data-id-value="{$fields.user_id9_c.value}">{$fields.prepared_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>


{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.title1_c.acl > 1 || $fields.title1_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.title1_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_TITLE1' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="varchar" field="title1_c" width='37.5%' colspan='3' >
{if !$fields.title1_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.title1_c.value) <= 0}
{assign var="value" value=$fields.title1_c.default_value }
{else}
{assign var="value" value=$fields.title1_c.value }
{/if} 
<span class="sugar_field" id="{$fields.title1_c.name}">{$fields.title1_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.ddm_approval_by_c.acl > 1 || $fields.ddm_approval_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.ddm_approval_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_APPROVAL_BY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="ddm_approval_by_c" width='37.5%'  >
{if !$fields.ddm_approval_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id_c" class="sugar_field" data-id-value="{$fields.user_id_c.value}">{$fields.ddm_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.ddm_date_c.acl > 1 || $fields.ddm_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.ddm_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_DATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="ddm_date_c" width='37.5%'  >
{if !$fields.ddm_date_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.ddm_date_c.value) <= 0}
{assign var="value" value=$fields.ddm_date_c.default_value }
{else}
{assign var="value" value=$fields.ddm_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.ddm_date_c.name}">{$fields.ddm_date_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.management_approval_by_c.acl > 1 || $fields.management_approval_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.management_approval_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MANAGEMENT_APPROVAL_BY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="management_approval_by_c" width='37.5%'  >
{if !$fields.management_approval_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id1_c" class="sugar_field" data-id-value="{$fields.user_id1_c.value}">{$fields.management_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.management_date_c.acl > 1 || $fields.management_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.management_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MANAGEMENT_DATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="management_date_c" width='37.5%'  >
{if !$fields.management_date_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.management_date_c.value) <= 0}
{assign var="value" value=$fields.management_date_c.default_value }
{else}
{assign var="value" value=$fields.management_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.management_date_c.name}">{$fields.management_date_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.ceo_approval_by_c.acl > 1 || $fields.ceo_approval_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.ceo_approval_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_APPROVAL_BY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="ceo_approval_by_c" width='37.5%'  >
{if !$fields.ceo_approval_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id2_c" class="sugar_field" data-id-value="{$fields.user_id2_c.value}">{$fields.ceo_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.ceo_date_c.acl > 1 || $fields.ceo_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.ceo_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_DATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="ceo_date_c" width='37.5%'  >
{if !$fields.ceo_date_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.ceo_date_c.value) <= 0}
{assign var="value" value=$fields.ceo_date_c.default_value }
{else}
{assign var="value" value=$fields.ceo_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.ceo_date_c.name}">{$fields.ceo_date_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>


{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.title2_c.acl > 1 || $fields.title2_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.title2_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_TITLE2' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="varchar" field="title2_c" width='37.5%' colspan='3' >
{if !$fields.title2_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.title2_c.value) <= 0}
{assign var="value" value=$fields.title2_c.default_value }
{else}
{assign var="value" value=$fields.title2_c.value }
{/if} 
<span class="sugar_field" id="{$fields.title2_c.name}">{$fields.title2_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.member_1_c.acl > 1 || $fields.member_1_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.member_1_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_1' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="member_1_c" width='37.5%'  >
{if !$fields.member_1_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id3_c" class="sugar_field" data-id-value="{$fields.user_id3_c.value}">{$fields.member_1_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.member_2_c.acl > 1 || $fields.member_2_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.member_2_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_2' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="member_2_c" width='37.5%'  >
{if !$fields.member_2_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id4_c" class="sugar_field" data-id-value="{$fields.user_id4_c.value}">{$fields.member_2_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.member_3_c.acl > 1 || $fields.member_3_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.member_3_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_3' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="member_3_c" width='37.5%'  >
{if !$fields.member_3_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id5_c" class="sugar_field" data-id-value="{$fields.user_id5_c.value}">{$fields.member_3_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.member_4_c.acl > 1 || $fields.member_4_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.member_4_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_4' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="member_4_c" width='37.5%'  >
{if !$fields.member_4_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id6_c" class="sugar_field" data-id-value="{$fields.user_id6_c.value}">{$fields.member_4_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.member_5_c.acl > 1 || $fields.member_5_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.member_5_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_5' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="member_5_c" width='37.5%'  >
{if !$fields.member_5_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id7_c" class="sugar_field" data-id-value="{$fields.user_id7_c.value}">{$fields.member_5_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.member_6_c.acl > 1 || $fields.member_6_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.member_6_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_6' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="member_6_c" width='37.5%'  >
{if !$fields.member_6_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id8_c" class="sugar_field" data-id-value="{$fields.user_id8_c.value}">{$fields.member_6_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL1").style.display='none';</script>
{/if}
</div>    <div id='tabcontent1'>
<div id='detailpanel_2' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_PANEL_OVERVIEW' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.billing_account.acl > 1 || $fields.billing_account.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.billing_account.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_BILLING_ACCOUNT' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="billing_account" width='37.5%'  >
{if !$fields.billing_account.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.billing_account_id.value)}
{capture assign="detail_url"}index.php?module=Accounts&action=DetailView&record={$fields.billing_account_id.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="billing_account_id" class="sugar_field" data-id-value="{$fields.billing_account_id.value}">{$fields.billing_account.value}</span>
{if !empty($fields.billing_account_id.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.billing_contact.acl > 1 || $fields.billing_contact.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.billing_contact.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_BILLING_CONTACT' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="billing_contact" width='37.5%'  >
{if !$fields.billing_contact.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.billing_contact_id.value)}
{capture assign="detail_url"}index.php?module=Contacts&action=DetailView&record={$fields.billing_contact_id.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="billing_contact_id" class="sugar_field" data-id-value="{$fields.billing_contact_id.value}">{$fields.billing_contact.value}</span>
{if !empty($fields.billing_contact_id.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.leads_aos_quotes_1_name.acl > 1 || $fields.leads_aos_quotes_1_name.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.leads_aos_quotes_1_name.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_LEADS_AOS_QUOTES_1_FROM_LEADS_TITLE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="leads_aos_quotes_1_name" width='37.5%'  >
{if !$fields.leads_aos_quotes_1_name.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.leads_aos_quotes_1leads_ida.value)}
{capture assign="detail_url"}index.php?module=Leads&action=DetailView&record={$fields.leads_aos_quotes_1leads_ida.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="leads_aos_quotes_1leads_ida" class="sugar_field" data-id-value="{$fields.leads_aos_quotes_1leads_ida.value}">{$fields.leads_aos_quotes_1_name.value}</span>
{if !empty($fields.leads_aos_quotes_1leads_ida.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.event_date_c.acl > 1 || $fields.event_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="date" field="event_date_c" width='37.5%'  >
{if !$fields.event_date_c.hidden}
{counter name="panelFieldCount"}


{if strlen($fields.event_date_c.value) <= 0}
{assign var="value" value=$fields.event_date_c.default_value }
{else}
{assign var="value" value=$fields.event_date_c.value }
{/if}
<span class="sugar_field" id="{$fields.event_date_c.name}">{$value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.venue_c.acl > 1 || $fields.venue_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.venue_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_VENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="venue_c" width='37.5%'  >
{if !$fields.venue_c.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.vnp_venues_id_c.value)}
{capture assign="detail_url"}index.php?module=VNP_Venues&action=DetailView&record={$fields.vnp_venues_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="vnp_venues_id_c" class="sugar_field" data-id-value="{$fields.vnp_venues_id_c.value}">{$fields.venue_c.value}</span>
{if !empty($fields.vnp_venues_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.manually_calculate_c.acl > 1 || $fields.manually_calculate_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.manually_calculate_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MANUALLY_CALCULATE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="manually_calculate_c" width='37.5%'  >
{if !$fields.manually_calculate_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.manually_calculate_c.value) == "1" || strval($fields.manually_calculate_c.value) == "yes" || strval($fields.manually_calculate_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.manually_calculate_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.manually_calculate_c.name}" id="{$fields.manually_calculate_c.name}" value="$fields.manually_calculate_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.no_of_delegates_c.acl > 1 || $fields.no_of_delegates_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.no_of_delegates_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_NO_OF_DELEGATES' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="int" field="no_of_delegates_c" width='37.5%'  >
{if !$fields.no_of_delegates_c.hidden}
{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.no_of_delegates_c.name}">
{assign var="value" value=$fields.no_of_delegates_c.value }
{$value}
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.event_days_c.acl > 1 || $fields.event_days_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_days_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DAYS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="int" field="event_days_c" width='37.5%'  >
{if !$fields.event_days_c.hidden}
{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.event_days_c.name}">
{assign var="value" value=$fields.event_days_c.value }
{$value}
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.day_spending_c.acl > 1 || $fields.day_spending_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.day_spending_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DAY_SPENDING' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="day_spending_c" width='37.5%'  >
{if !$fields.day_spending_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.day_spending_c.name}'>
{sugar_number_format var=$fields.day_spending_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.expected_revenue_c.acl > 1 || $fields.expected_revenue_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.expected_revenue_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EXPECTED_REVENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="expected_revenue_c" width='37.5%'  >
{if !$fields.expected_revenue_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.expected_revenue_c.name}'>
{sugar_number_format var=$fields.expected_revenue_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.gor_revenue_c.acl > 1 || $fields.gor_revenue_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.gor_revenue_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_REVENUE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="gor_revenue_c" width='37.5%'  >
{if !$fields.gor_revenue_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.gor_revenue_c.name}'>
{sugar_number_format var=$fields.gor_revenue_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.gor_commitment_c.acl > 1 || $fields.gor_commitment_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.gor_commitment_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_COMMITMENT' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="gor_commitment_c" width='37.5%'  >
{if !$fields.gor_commitment_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.gor_commitment_c.name}'>
{sugar_number_format var=$fields.gor_commitment_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.additional_revenue_c.acl > 1 || $fields.additional_revenue_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.additional_revenue_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_ADDITIONAL_REVENUE ' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="additional_revenue_c" width='37.5%'  >
{if !$fields.additional_revenue_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.additional_revenue_c.name}'>
{sugar_number_format var=$fields.additional_revenue_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.revenue_description_c.acl > 1 || $fields.revenue_description_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.revenue_description_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_REVENUE_DESCRIPTION' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="text" field="revenue_description_c" width='37.5%'  >
{if !$fields.revenue_description_c.hidden}
{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.revenue_description_c.name|escape:'html'|url2html|nl2br}">{$fields.revenue_description_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_PANEL_OVERVIEW").style.display='none';</script>
{/if}
</div>    <div id='tabcontent2'>
<div id='detailpanel_3' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL2' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.event_brief_c.acl > 1 || $fields.event_brief_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_brief_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_BRIEF' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="wysiwyg" field="event_brief_c" width='37.5%' colspan='3' >
{if !$fields.event_brief_c.hidden}
{counter name="panelFieldCount"}

<iframe
id="{$fields.event_brief_c.name}"
name="{$fields.event_brief_c.name}"
srcdoc="{$fields.event_brief_c.value}"
style="width:100%;height:500px"
></iframe>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL2").style.display='none';</script>
{/if}
</div>    <div id='tabcontent3'>
<div id='detailpanel_4' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_LINE_ITEMS' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.currency_id.acl > 1 || $fields.currency_id.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.currency_id.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CURRENCY' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="id" field="currency_id" width='37.5%'  >
{if !$fields.currency_id.hidden}
{counter name="panelFieldCount"}
<span id='currency_id_span'>
{$fields.currency_id.value}
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.line_items.acl > 1 || $fields.line_items.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.line_items.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_LINE_ITEMS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="function" field="line_items" width='37.5%' colspan='3' >
{if !$fields.line_items.hidden}
{counter name="panelFieldCount"}
<span id='line_items_span'>
{$fields.line_items.value}
</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>


{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>



{if $fields.total_amount.acl > 1 || $fields.total_amount.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.total_amount.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_GRAND_TOTAL' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="total_amount" width='37.5%'  >
{if !$fields.total_amount.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.total_amount.name}'>
{sugar_number_format var=$fields.total_amount.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_LINE_ITEMS").style.display='none';</script>
{/if}
</div>    <div id='tabcontent4'>
<div id='detailpanel_5' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL3' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.recommendation_c.acl > 1 || $fields.recommendation_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.recommendation_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_RECOMMENDATION' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="wysiwyg" field="recommendation_c" width='37.5%' colspan='3' >
{if !$fields.recommendation_c.hidden}
{counter name="panelFieldCount"}

<iframe
id="{$fields.recommendation_c.name}"
name="{$fields.recommendation_c.name}"
srcdoc="{$fields.recommendation_c.value}"
style="width:100%;height:500px"
></iframe>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL3").style.display='none';</script>
{/if}
</div>    <div id='tabcontent5'>
<div id='detailpanel_6' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_PANEL_ASSIGNMENT' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.assigned_user_name.acl > 1 || $fields.assigned_user_name.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.assigned_user_name.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_ASSIGNED_TO' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="assigned_user_name" width='37.5%'  >
{if !$fields.assigned_user_name.hidden}
{counter name="panelFieldCount"}

<span id="assigned_user_id" class="sugar_field" data-id-value="{$fields.assigned_user_id.value}">{$fields.assigned_user_name.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.approval_routine_c.acl > 1 || $fields.approval_routine_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.approval_routine_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_ROUTINE' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="approval_routine_c" width='37.5%'  >
{if !$fields.approval_routine_c.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.srp_stdapproval_routine_id_c.value)}
{capture assign="detail_url"}index.php?module=SRP_StdApproval_Routine&action=DetailView&record={$fields.srp_stdapproval_routine_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="srp_stdapproval_routine_id_c" class="sugar_field" data-id-value="{$fields.srp_stdapproval_routine_id_c.value}">{$fields.approval_routine_c.value}</span>
{if !empty($fields.srp_stdapproval_routine_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.date_entered.acl > 1 || $fields.date_entered.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.date_entered.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_ENTERED' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="datetime" field="date_entered" width='37.5%'  >
{if !$fields.date_entered.hidden}
{counter name="panelFieldCount"}
<span id="date_entered" class="sugar_field">{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.date_modified.acl > 1 || $fields.date_modified.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.date_modified.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_MODIFIED' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="datetime" field="date_modified" width='37.5%'  >
{if !$fields.date_modified.hidden}
{counter name="panelFieldCount"}
<span id="date_modified" class="sugar_field">{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.approval_steps_c.acl > 1 || $fields.approval_steps_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.approval_steps_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_STEPS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="int" field="approval_steps_c" width='37.5%'  >
{if !$fields.approval_steps_c.hidden}
{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.approval_steps_c.name}">
{sugar_number_format precision=0 var=$fields.approval_steps_c.value}
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.gor_commitment_limit_c.acl > 1 || $fields.gor_commitment_limit_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.gor_commitment_limit_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_COMMITMENT_LIMIT' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="gor_commitment_limit_c" width='37.5%'  >
{if !$fields.gor_commitment_limit_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.gor_commitment_limit_c.name}'>
{sugar_number_format var=$fields.gor_commitment_limit_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.help_notifications_c.acl > 1 || $fields.help_notifications_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.help_notifications_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_HELP_NOTIFICATIONS' module='AOS_Quotes'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="help_notifications_c" width='37.5%'  >
{if !$fields.help_notifications_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.help_notifications_c.value) == "1" || strval($fields.help_notifications_c.value) == "yes" || strval($fields.help_notifications_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.help_notifications_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.help_notifications_c.name}" id="{$fields.help_notifications_c.name}" value="$fields.help_notifications_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_PANEL_ASSIGNMENT").style.display='none';</script>
{/if}
</div>
</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script><script type='text/javascript' src='{sugar_getjspath file='include/javascript/popup_helper.js'}'></script>
<script type="text/javascript" src="{sugar_getjspath file='cache/include/javascript/sugar_grp_yui_widgets.js'}"></script>
<script type="text/javascript">
var AOS_Quotes_detailview_tabs = new YAHOO.widget.TabView("AOS_Quotes_detailview_tabs");
AOS_Quotes_detailview_tabs.selectTab(0);
</script>
<script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
{/if}