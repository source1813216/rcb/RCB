
{php}
global $current_user;
$this->_tpl_vars['current_user'] = $current_user->id;
$this->_tpl_vars['is_admin_user'] = $current_user->is_admin;
$this->_tpl_vars['current_theme'] = $current_user->getPreference('user_theme');
{/php}
{if $current_theme == 'SuiteP'}

<script>
    {literal}
    $(document).ready(function(){
	    $("ul.clickMenu").each(function(index, node){
	        $(node).sugarActionMenu();
	    });

        if($('.edit-view-pagination').children().length == 0) $('.saveAndContinue').remove();
    });
    {/literal}
</script>
<div class="clear"></div>
<form action="index.php" method="POST" name="{$form_name}" id="{$form_id}" {$enctype}>
<div class="edit-view-pagination-mobile-container">
<div class="edit-view-pagination edit-view-mobile-pagination">
{$PAGINATION}
</div>
</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit">
<tr>
<td class="buttons">
<input type="hidden" name="module" value="{$module}">
{if isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true"}
<input type="hidden" name="record" value="">
<input type="hidden" name="duplicateSave" value="true">
<input type="hidden" name="duplicateId" value="{$fields.id.value}">
{else}
<input type="hidden" name="record" value="{$fields.id.value}">
{/if}
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="action">
<input type="hidden" name="return_module" value="{$smarty.request.return_module}">
<input type="hidden" name="return_action" value="{$smarty.request.return_action}">
<input type="hidden" name="return_id" value="{$smarty.request.return_id}">
<input type="hidden" name="module_tab"> 
<input type="hidden" name="contact_role">
{if (!empty($smarty.request.return_module) || !empty($smarty.request.relate_to)) && !(isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true")}
<input type="hidden" name="relate_to" value="{if $smarty.request.return_relationship}{$smarty.request.return_relationship}{elseif $smarty.request.relate_to && empty($smarty.request.from_dcmenu)}{$smarty.request.relate_to}{elseif empty($isDCForm) && empty($smarty.request.from_dcmenu)}{$smarty.request.return_module}{/if}">
<input type="hidden" name="relate_id" value="{$smarty.request.return_id}">
{/if}
<input type="hidden" name="offset" value="{$offset}">
{assign var='place' value="_HEADER"} <!-- to be used for id for buttons with custom code in def files-->
<div class="buttons">
{if $bean->aclAccess("save")}<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('EditView'); {if $isDuplicate}_form.return_id.value=''; {/if}_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" id="SAVE">{/if} 
{if !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($smarty.request.return_id))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" type="button" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($fields.id.value))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && empty($fields.id.value)) && empty($smarty.request.return_id)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && !empty($smarty.request.return_module)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action={$smarty.request.return_action}&module={$smarty.request.return_module|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif empty($smarty.request.return_action) || empty($smarty.request.return_id) && !empty($fields.id.value)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=SRP_StdApproval_Routine'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {else}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {/if}
{if $showVCRControl}
<button type="button" id="save_and_continue" class="button saveAndContinue" title="{$app_strings.LBL_SAVE_AND_CONTINUE}" onClick="SUGAR.saveAndContinue(this);">
{$APP.LBL_SAVE_AND_CONTINUE}
</button>
{/if}
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=SRP_StdApproval_Routine", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>
</td>
<td align='right' class="edit-view-pagination-desktop-container">
<div class="edit-view-pagination edit-view-pagination-desktop">
{$PAGINATION}
</div>
</td>
</tr>
</table>
{sugar_include include=$includes}
<div id="EditView_tabs">

<ul class="nav nav-tabs">
</ul>
<div class="clearfix"></div>
<div class="tab-content" style="padding: 0; border: 0;">

<div class="tab-pane panel-collapse">&nbsp;</div>
</div>

<div class="panel-content">
<div>&nbsp;</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='DEFAULT' module='SRP_StdApproval_Routine'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_-1" data-id="DEFAULT">
<div class="tab-content">


{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}
    
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.name.acl > 1 || ($showDetailData && $fields.name.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_NAME">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_NAME' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="name" field="name"  >

{if $fields.name.acl > 1}

{counter name="panelFieldCount" print=false}

{if strlen($fields.name.value) <= 0}
{assign var="value" value=$fields.name.default_value }
{else}
{assign var="value" value=$fields.name.value }
{/if}  
<input type='text' name='{$fields.name.name}' 
id='{$fields.name.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      >

{else}

{if strlen($fields.name.value) <= 0}
{assign var="value" value=$fields.name.default_value }
{else}
{assign var="value" value=$fields.name.value }
{/if} 
<span class="sugar_field" id="{$fields.name.name}">{$fields.name.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.name.acl > 1 || ($showDetailData && $fields.name.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.assigned_user_name.acl > 1 || ($showDetailData && $fields.assigned_user_name.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_ASSIGNED_TO_NAME">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_ASSIGNED_TO_NAME' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="assigned_user_name"  >

{if $fields.assigned_user_name.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.assigned_user_name.name}" class="sqsEnabled" tabindex="0" id="{$fields.assigned_user_name.name}" size="" value="{$fields.assigned_user_name.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.assigned_user_name.id_name}" 
id="{$fields.assigned_user_name.id_name}" 
value="{$fields.assigned_user_id.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.assigned_user_name.name}" id="btn_{$fields.assigned_user_name.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.assigned_user_name.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"assigned_user_id","user_name":"assigned_user_name"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.assigned_user_name.name}" id="btn_clr_{$fields.assigned_user_name.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.assigned_user_name.name}', '{$fields.assigned_user_name.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.assigned_user_name.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="assigned_user_id" class="sugar_field" data-id-value="{$fields.assigned_user_id.value}">{$fields.assigned_user_name.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.assigned_user_name.acl > 1 || ($showDetailData && $fields.assigned_user_name.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-12 edit-view-row-item">


{if $fields.description.acl > 1 || ($showDetailData && $fields.description.acl > 0)}


<div class="col-xs-12 col-sm-2 label" data-label="LBL_DESCRIPTION">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DESCRIPTION' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="text" field="description" colspan='3' >

{if $fields.description.acl > 1}

{counter name="panelFieldCount" print=false}

{if empty($fields.description.value)}
{assign var="value" value=$fields.description.default_value }
{else}
{assign var="value" value=$fields.description.value }
{/if}
<textarea  id='{$fields.description.name}' name='{$fields.description.name}'
rows="6"
cols="80"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

<span class="sugar_field" id="{$fields.description.name|escape:'html'|url2html|nl2br}">{$fields.description.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.description.acl > 1 || ($showDetailData && $fields.description.acl > 0)}

</div>

{/if}

<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='SRP_StdApproval_Routine'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_0" data-id="LBL_EDITVIEW_PANEL1">
<div class="tab-content">


{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}
    
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.ddm_approval_by_c.acl > 1 || ($showDetailData && $fields.ddm_approval_by_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DDM_APPROVAL_BY">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_APPROVAL_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="ddm_approval_by_c"  >

{if $fields.ddm_approval_by_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.ddm_approval_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.ddm_approval_by_c.name}" size="" value="{$fields.ddm_approval_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.ddm_approval_by_c.id_name}" 
id="{$fields.ddm_approval_by_c.id_name}" 
value="{$fields.user_id_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.ddm_approval_by_c.name}" id="btn_{$fields.ddm_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.ddm_approval_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id_c","name":"ddm_approval_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.ddm_approval_by_c.name}" id="btn_clr_{$fields.ddm_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.ddm_approval_by_c.name}', '{$fields.ddm_approval_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.ddm_approval_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id_c" class="sugar_field" data-id-value="{$fields.user_id_c.value}">{$fields.ddm_approval_by_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.ddm_approval_by_c.acl > 1 || ($showDetailData && $fields.ddm_approval_by_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.legal_approval_by_c.acl > 1 || ($showDetailData && $fields.legal_approval_by_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_LEGAL_APPROVAL_BY">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_LEGAL_APPROVAL_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="legal_approval_by_c"  >

{if $fields.legal_approval_by_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.legal_approval_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.legal_approval_by_c.name}" size="" value="{$fields.legal_approval_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.legal_approval_by_c.id_name}" 
id="{$fields.legal_approval_by_c.id_name}" 
value="{$fields.user_id12_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.legal_approval_by_c.name}" id="btn_{$fields.legal_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.legal_approval_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id12_c","name":"legal_approval_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.legal_approval_by_c.name}" id="btn_clr_{$fields.legal_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.legal_approval_by_c.name}', '{$fields.legal_approval_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.legal_approval_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id12_c" class="sugar_field" data-id-value="{$fields.user_id12_c.value}">{$fields.legal_approval_by_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.legal_approval_by_c.acl > 1 || ($showDetailData && $fields.legal_approval_by_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.mgmt_apv_by_c.acl > 1 || ($showDetailData && $fields.mgmt_apv_by_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MGMT_APV_BY">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MGMT_APV_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="mgmt_apv_by_c"  >

{if $fields.mgmt_apv_by_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.mgmt_apv_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.mgmt_apv_by_c.name}" size="" value="{$fields.mgmt_apv_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.mgmt_apv_by_c.id_name}" 
id="{$fields.mgmt_apv_by_c.id_name}" 
value="{$fields.user_id1_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.mgmt_apv_by_c.name}" id="btn_{$fields.mgmt_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.mgmt_apv_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id1_c","name":"mgmt_apv_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.mgmt_apv_by_c.name}" id="btn_clr_{$fields.mgmt_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.mgmt_apv_by_c.name}', '{$fields.mgmt_apv_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.mgmt_apv_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id1_c" class="sugar_field" data-id-value="{$fields.user_id1_c.value}">{$fields.mgmt_apv_by_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.mgmt_apv_by_c.acl > 1 || ($showDetailData && $fields.mgmt_apv_by_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.ssd_approver_c.acl > 1 || ($showDetailData && $fields.ssd_approver_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_SSD_APPROVER">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_SSD_APPROVER' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="ssd_approver_c"  >

{if $fields.ssd_approver_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.ssd_approver_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.ssd_approver_c.name}" size="" value="{$fields.ssd_approver_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.ssd_approver_c.id_name}" 
id="{$fields.ssd_approver_c.id_name}" 
value="{$fields.user_id10_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.ssd_approver_c.name}" id="btn_{$fields.ssd_approver_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.ssd_approver_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id10_c","name":"ssd_approver_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.ssd_approver_c.name}" id="btn_clr_{$fields.ssd_approver_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.ssd_approver_c.name}', '{$fields.ssd_approver_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.ssd_approver_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id10_c" class="sugar_field" data-id-value="{$fields.user_id10_c.value}">{$fields.ssd_approver_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.ssd_approver_c.acl > 1 || ($showDetailData && $fields.ssd_approver_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.ceo_office_apv_by_c.acl > 1 || ($showDetailData && $fields.ceo_office_apv_by_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_CEO_OFFICE_APV_BY">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_OFFICE_APV_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="ceo_office_apv_by_c"  >

{if $fields.ceo_office_apv_by_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.ceo_office_apv_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.ceo_office_apv_by_c.name}" size="" value="{$fields.ceo_office_apv_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.ceo_office_apv_by_c.id_name}" 
id="{$fields.ceo_office_apv_by_c.id_name}" 
value="{$fields.user_id2_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.ceo_office_apv_by_c.name}" id="btn_{$fields.ceo_office_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.ceo_office_apv_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id2_c","name":"ceo_office_apv_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.ceo_office_apv_by_c.name}" id="btn_clr_{$fields.ceo_office_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.ceo_office_apv_by_c.name}', '{$fields.ceo_office_apv_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.ceo_office_apv_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id2_c" class="sugar_field" data-id-value="{$fields.user_id2_c.value}">{$fields.ceo_office_apv_by_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.ceo_office_apv_by_c.acl > 1 || ($showDetailData && $fields.ceo_office_apv_by_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.dec_approval_by_c.acl > 1 || ($showDetailData && $fields.dec_approval_by_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DEC_APPROVAL_BY">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DEC_APPROVAL_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="dec_approval_by_c"  >

{if $fields.dec_approval_by_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.dec_approval_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.dec_approval_by_c.name}" size="" value="{$fields.dec_approval_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.dec_approval_by_c.id_name}" 
id="{$fields.dec_approval_by_c.id_name}" 
value="{$fields.user_id11_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.dec_approval_by_c.name}" id="btn_{$fields.dec_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.dec_approval_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id11_c","name":"dec_approval_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.dec_approval_by_c.name}" id="btn_clr_{$fields.dec_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.dec_approval_by_c.name}', '{$fields.dec_approval_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.dec_approval_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id11_c" class="sugar_field" data-id-value="{$fields.user_id11_c.value}">{$fields.dec_approval_by_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.dec_approval_by_c.acl > 1 || ($showDetailData && $fields.dec_approval_by_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.dceo_apv_by_c.acl > 1 || ($showDetailData && $fields.dceo_apv_by_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_DCEO_APV_BY">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_DCEO_APV_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="dceo_apv_by_c"  >

{if $fields.dceo_apv_by_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.dceo_apv_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.dceo_apv_by_c.name}" size="" value="{$fields.dceo_apv_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.dceo_apv_by_c.id_name}" 
id="{$fields.dceo_apv_by_c.id_name}" 
value="{$fields.user_id9_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.dceo_apv_by_c.name}" id="btn_{$fields.dceo_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.dceo_apv_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id9_c","name":"dceo_apv_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.dceo_apv_by_c.name}" id="btn_clr_{$fields.dceo_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.dceo_apv_by_c.name}', '{$fields.dceo_apv_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.dceo_apv_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id9_c" class="sugar_field" data-id-value="{$fields.user_id9_c.value}">{$fields.dceo_apv_by_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.dceo_apv_by_c.acl > 1 || ($showDetailData && $fields.dceo_apv_by_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


</div>


<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>




<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse-edit" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_EDITVIEW_PANEL2' module='SRP_StdApproval_Routine'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="detailpanel_1" data-id="LBL_EDITVIEW_PANEL2">
<div class="tab-content">


{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}
    
<!-- tab_panel_content.tpl -->
<div class="row edit-view-row">



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.member_1_c.acl > 1 || ($showDetailData && $fields.member_1_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MEMBER_1">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_1' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

<span class="required">*</span>
{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="member_1_c"  >

{if $fields.member_1_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.member_1_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_1_c.name}" size="" value="{$fields.member_1_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_1_c.id_name}" 
id="{$fields.member_1_c.id_name}" 
value="{$fields.user_id3_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_1_c.name}" id="btn_{$fields.member_1_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_1_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id3_c","name":"member_1_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_1_c.name}" id="btn_clr_{$fields.member_1_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_1_c.name}', '{$fields.member_1_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_1_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id3_c" class="sugar_field" data-id-value="{$fields.user_id3_c.value}">{$fields.member_1_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.member_1_c.acl > 1 || ($showDetailData && $fields.member_1_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.member_2_c.acl > 1 || ($showDetailData && $fields.member_2_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MEMBER_2">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_2' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="member_2_c"  >

{if $fields.member_2_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.member_2_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_2_c.name}" size="" value="{$fields.member_2_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_2_c.id_name}" 
id="{$fields.member_2_c.id_name}" 
value="{$fields.user_id4_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_2_c.name}" id="btn_{$fields.member_2_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_2_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id4_c","name":"member_2_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_2_c.name}" id="btn_clr_{$fields.member_2_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_2_c.name}', '{$fields.member_2_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_2_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id4_c" class="sugar_field" data-id-value="{$fields.user_id4_c.value}">{$fields.member_2_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.member_2_c.acl > 1 || ($showDetailData && $fields.member_2_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.member_3_c.acl > 1 || ($showDetailData && $fields.member_3_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MEMBER_3">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_3' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="member_3_c"  >

{if $fields.member_3_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.member_3_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_3_c.name}" size="" value="{$fields.member_3_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_3_c.id_name}" 
id="{$fields.member_3_c.id_name}" 
value="{$fields.user_id5_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_3_c.name}" id="btn_{$fields.member_3_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_3_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id5_c","name":"member_3_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_3_c.name}" id="btn_clr_{$fields.member_3_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_3_c.name}', '{$fields.member_3_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_3_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id5_c" class="sugar_field" data-id-value="{$fields.user_id5_c.value}">{$fields.member_3_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.member_3_c.acl > 1 || ($showDetailData && $fields.member_3_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.member_4_c.acl > 1 || ($showDetailData && $fields.member_4_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MEMBER_4">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_4' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="member_4_c"  >

{if $fields.member_4_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.member_4_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_4_c.name}" size="" value="{$fields.member_4_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_4_c.id_name}" 
id="{$fields.member_4_c.id_name}" 
value="{$fields.user_id6_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_4_c.name}" id="btn_{$fields.member_4_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_4_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id6_c","name":"member_4_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_4_c.name}" id="btn_clr_{$fields.member_4_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_4_c.name}', '{$fields.member_4_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_4_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id6_c" class="sugar_field" data-id-value="{$fields.user_id6_c.value}">{$fields.member_4_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.member_4_c.acl > 1 || ($showDetailData && $fields.member_4_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.member_5_c.acl > 1 || ($showDetailData && $fields.member_5_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MEMBER_5">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_5' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="member_5_c"  >

{if $fields.member_5_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.member_5_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_5_c.name}" size="" value="{$fields.member_5_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_5_c.id_name}" 
id="{$fields.member_5_c.id_name}" 
value="{$fields.user_id7_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_5_c.name}" id="btn_{$fields.member_5_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_5_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id7_c","name":"member_5_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_5_c.name}" id="btn_clr_{$fields.member_5_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_5_c.name}', '{$fields.member_5_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_5_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id7_c" class="sugar_field" data-id-value="{$fields.user_id7_c.value}">{$fields.member_5_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.member_5_c.acl > 1 || ($showDetailData && $fields.member_5_c.acl > 0)}

</div>

{/if}



<div class="col-xs-12 col-sm-6 edit-view-row-item">


{if $fields.member_6_c.acl > 1 || ($showDetailData && $fields.member_6_c.acl > 0)}


<div class="col-xs-12 col-sm-4 label" data-label="LBL_MEMBER_6">

{minify}
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_6' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:

{/minify}
</div>

<div class="col-xs-12 col-sm-8 edit-view-field " type="relate" field="member_6_c"  >

{if $fields.member_6_c.acl > 1}

{counter name="panelFieldCount" print=false}

<input type="text" name="{$fields.member_6_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_6_c.name}" size="" value="{$fields.member_6_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_6_c.id_name}" 
id="{$fields.member_6_c.id_name}" 
value="{$fields.user_id8_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_6_c.name}" id="btn_{$fields.member_6_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_6_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id8_c","name":"member_6_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_6_c.name}" id="btn_clr_{$fields.member_6_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_6_c.name}', '{$fields.member_6_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_6_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

<span id="user_id8_c" class="sugar_field" data-id-value="{$fields.user_id8_c.value}">{$fields.member_6_c.value}</span>
{/if}
{else}
{/if}

</div>

<!-- [/hide] -->

{if $fields.member_6_c.acl > 1 || ($showDetailData && $fields.member_6_c.acl > 0)}

</div>

{/if}

<div class="clear"></div>
<div class="clear"></div>
</div>                    </div>
</div>
</div>
</div>
</div>

<script language="javascript">
    var _form_id = '{$form_id}';
    {literal}
    SUGAR.util.doWhen(function(){
        _form_id = (_form_id == '') ? 'EditView' : _form_id;
        return document.getElementById(_form_id) != null;
    }, SUGAR.themes.actionMenu);
    {/literal}
</script>
{assign var='place' value="_FOOTER"} <!-- to be used for id for buttons with custom code in def files-->
<div class="buttons">
{if $bean->aclAccess("save")}<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('EditView'); {if $isDuplicate}_form.return_id.value=''; {/if}_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" id="SAVE">{/if} 
{if !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($smarty.request.return_id))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" type="button" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($fields.id.value))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && empty($fields.id.value)) && empty($smarty.request.return_id)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif !empty($smarty.request.return_action) && !empty($smarty.request.return_module)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action={$smarty.request.return_action}&module={$smarty.request.return_module|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {elseif empty($smarty.request.return_action) || empty($smarty.request.return_id) && !empty($fields.id.value)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=SRP_StdApproval_Routine'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {else}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL"> {/if}
{if $showVCRControl}
<button type="button" id="save_and_continue" class="button saveAndContinue" title="{$app_strings.LBL_SAVE_AND_CONTINUE}" onClick="SUGAR.saveAndContinue(this);">
{$APP.LBL_SAVE_AND_CONTINUE}
</button>
{/if}
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=SRP_StdApproval_Routine", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>
</form>
{$set_focus_block}
<script>SUGAR.util.doWhen("document.getElementById('EditView') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script>
<script type="text/javascript">
YAHOO.util.Event.onContentReady("EditView",
    function () {ldelim} initEditView(document.forms.EditView) {rdelim});
//window.setTimeout(, 100);
window.onbeforeunload = function () {ldelim} return onUnloadEditView(); {rdelim};
// bug 55468 -- IE is too aggressive with onUnload event
if ($.browser.msie) {ldelim}
$(document).ready(function() {ldelim}
    $(".collapseLink,.expandLink").click(function (e) {ldelim} e.preventDefault(); {rdelim});
  {rdelim});
{rdelim}
</script>
{literal}
<script type="text/javascript">

    var selectTab = function(tab) {
        $('#EditView_tabs div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').hide();
        $('#EditView_tabs div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').eq(tab).show().addClass('active').addClass('in');
    };

    var selectTabOnError = function(tab) {
        selectTab(tab);
        $('#EditView_tabs ul.nav.nav-tabs li').removeClass('active');
        $('#EditView_tabs ul.nav.nav-tabs li a').css('color', '');

        $('#EditView_tabs ul.nav.nav-tabs li').eq(tab).find('a').first().css('color', 'red');
        $('#EditView_tabs ul.nav.nav-tabs li').eq(tab).addClass('active');

    };

    var selectTabOnErrorInputHandle = function(inputHandle) {
        var tab = $(inputHandle).closest('.tab-pane-NOBOOTSTRAPTOGGLER').attr('id').match(/^detailpanel_(.*)$/)[1];
        selectTabOnError(tab);
    };


    $(function(){
        $('#EditView_tabs ul.nav.nav-tabs li > a[data-toggle="tab"]').click(function(e){
            if(typeof $(this).parent().find('a').first().attr('id') != 'undefined') {
                var tab = parseInt($(this).parent().find('a').first().attr('id').match(/^tab(.)*$/)[1]);
                selectTab(tab);
            }
        });

        $('a[data-toggle="collapse-edit"]').click(function(e){
            if($(this).hasClass('collapsed')) {
              // Expand panel
                // Change style of .panel-header
                $(this).removeClass('collapsed');
                // Expand .panel-body
                $(this).parents('.panel').find('.panel-body').removeClass('in').addClass('in');
            } else {
              // Collapse panel
                // Change style of .panel-header
                $(this).addClass('collapsed');
                // Collapse .panel-body
                $(this).parents('.panel').find('.panel-body').removeClass('in').removeClass('in');
            }
        });
    });

    </script>
{/literal}
{else}

{literal}
<script type="text/javascript" src="custom/include/js/emailAddressReadOnly.js"></script>
{/literal}


<script>
    {literal}
    $(document).ready(function(){
	    $("ul.clickMenu").each(function(index, node){
	        $(node).sugarActionMenu();
	    });
    });
    {/literal}
</script>
<div class="clear"></div>
<form action="index.php" method="POST" name="{$form_name}" id="{$form_id}" {$enctype}>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit">
<tr>
<td class="buttons">
<input type="hidden" name="module" value="{$module}">
{if isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true"}
<input type="hidden" name="record" value="">
<input type="hidden" name="duplicateSave" value="true">
<input type="hidden" name="duplicateId" value="{$fields.id.value}">
{else}
<input type="hidden" name="record" value="{$fields.id.value}">
{/if}
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="action">
<input type="hidden" name="return_module" value="{$smarty.request.return_module}">
<input type="hidden" name="return_action" value="{$smarty.request.return_action}">
<input type="hidden" name="return_id" value="{$smarty.request.return_id}">
<input type="hidden" name="module_tab"> 
<input type="hidden" name="contact_role">
{if (!empty($smarty.request.return_module) || !empty($smarty.request.relate_to)) && !(isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true")}
<input type="hidden" name="relate_to" value="{if $smarty.request.return_relationship}{$smarty.request.return_relationship}{elseif $smarty.request.relate_to && empty($smarty.request.from_dcmenu)}{$smarty.request.relate_to}{elseif empty($isDCForm) && empty($smarty.request.from_dcmenu)}{$smarty.request.return_module}{/if}">
<input type="hidden" name="relate_id" value="{$smarty.request.return_id}">
{/if}
<input type="hidden" name="offset" value="{$offset}">
{assign var='place' value="_HEADER"} <!-- to be used for id for buttons with custom code in def files-->
<div class="action_buttons">{if $bean->aclAccess("save")}<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('EditView'); {if $isDuplicate}_form.return_id.value=''; {/if}_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" id="SAVE_HEADER">{/if}  {if !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($smarty.request.return_id))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" type="button" id="CANCEL_HEADER"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($fields.id.value))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_HEADER"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && empty($fields.id.value)) && empty($smarty.request.return_id)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_HEADER"> {elseif !empty($smarty.request.return_action) && !empty($smarty.request.return_module)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action={$smarty.request.return_action}&module={$smarty.request.return_module|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_HEADER"> {elseif empty($smarty.request.return_action) || empty($smarty.request.return_id) && !empty($fields.id.value)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=SRP_StdApproval_Routine'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_HEADER"> {else}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_HEADER"> {/if} {if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=SRP_StdApproval_Routine", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}<div class="clear"></div></div>
</td>
<td align='right' class="edit-view-pagination">
</td>
</tr>
</table>{sugar_include include=$includes}
<span id='tabcounterJS'><script>SUGAR.TabFields=new Array();//this will be used to track tabindexes for references</script></span>
<div id="EditView_tabs"
>
<div >
<div id="detailpanel_1" >
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<table width="100%" border="0" cellspacing="1" cellpadding="0"  id='Default_{$module}_Subpanel'  class="yui3-skin-sam edit view panelContainer">
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.name.acl > 1 || ($showDetailData && $fields.name.acl > 0)}

<td valign="top" id='name_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_NAME' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.name.acl > 1}

{counter name="panelFieldCount"}

{if strlen($fields.name.value) <= 0}
{assign var="value" value=$fields.name.default_value }
{else}
{assign var="value" value=$fields.name.value }
{/if}  
<input type='text' name='{$fields.name.name}' 
id='{$fields.name.name}' size='30' 
maxlength='255' 
value='{$value}' title=''      accesskey='7'  >

{else}

{counter name="panelFieldCount"}

{if strlen($fields.name.value) <= 0}
{assign var="value" value=$fields.name.default_value }
{else}
{assign var="value" value=$fields.name.value }
{/if} 
<span class="sugar_field" id="{$fields.name.name}">{$fields.name.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.assigned_user_name.acl > 1 || ($showDetailData && $fields.assigned_user_name.acl > 0)}

<td valign="top" id='assigned_user_name_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_ASSIGNED_TO_NAME' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.assigned_user_name.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.assigned_user_name.name}" class="sqsEnabled" tabindex="0" id="{$fields.assigned_user_name.name}" size="" value="{$fields.assigned_user_name.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.assigned_user_name.id_name}" 
id="{$fields.assigned_user_name.id_name}" 
value="{$fields.assigned_user_id.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.assigned_user_name.name}" id="btn_{$fields.assigned_user_name.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.assigned_user_name.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"assigned_user_id","user_name":"assigned_user_name"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.assigned_user_name.name}" id="btn_clr_{$fields.assigned_user_name.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.assigned_user_name.name}', '{$fields.assigned_user_name.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.assigned_user_name.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="assigned_user_id" class="sugar_field" data-id-value="{$fields.assigned_user_id.value}">{$fields.assigned_user_name.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.description.acl > 1 || ($showDetailData && $fields.description.acl > 0)}

<td valign="top" id='description_label' width='12.5%' data-total-columns="1" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_DESCRIPTION' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="1" colspan='3'>

{if $fields.description.acl > 1}

{counter name="panelFieldCount"}

{if empty($fields.description.value)}
{assign var="value" value=$fields.description.default_value }
{else}
{assign var="value" value=$fields.description.value }
{/if}
<textarea  id='{$fields.description.name}' name='{$fields.description.name}'
rows="6"
cols="80"
title='' tabindex="0" 
 >{$value}</textarea>
{literal}{/literal}

{else}

{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.description.name|escape:'html'|url2html|nl2br}">{$fields.description.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("DEFAULT").style.display='none';</script>
{/if}
<div id="detailpanel_2" class="{$def.templateMeta.panelClass|default:'edit view edit508'}">
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<h4>&nbsp;&nbsp;
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(2);">
<img border="0" id="detailpanel_2_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(2);">
<img border="0" id="detailpanel_2_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='SRP_StdApproval_Routine'}
<script>
      document.getElementById('detailpanel_2').className += ' expanded';
    </script>
</h4>
<table width="100%" border="0" cellspacing="1" cellpadding="0"  id='LBL_EDITVIEW_PANEL1'  class="yui3-skin-sam edit view panelContainer">
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.ddm_approval_by_c.acl > 1 || ($showDetailData && $fields.ddm_approval_by_c.acl > 0)}

<td valign="top" id='ddm_approval_by_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_APPROVAL_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.ddm_approval_by_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.ddm_approval_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.ddm_approval_by_c.name}" size="" value="{$fields.ddm_approval_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.ddm_approval_by_c.id_name}" 
id="{$fields.ddm_approval_by_c.id_name}" 
value="{$fields.user_id_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.ddm_approval_by_c.name}" id="btn_{$fields.ddm_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.ddm_approval_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id_c","name":"ddm_approval_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.ddm_approval_by_c.name}" id="btn_clr_{$fields.ddm_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.ddm_approval_by_c.name}', '{$fields.ddm_approval_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.ddm_approval_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id_c" class="sugar_field" data-id-value="{$fields.user_id_c.value}">{$fields.ddm_approval_by_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.legal_approval_by_c.acl > 1 || ($showDetailData && $fields.legal_approval_by_c.acl > 0)}

<td valign="top" id='legal_approval_by_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_LEGAL_APPROVAL_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.legal_approval_by_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.legal_approval_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.legal_approval_by_c.name}" size="" value="{$fields.legal_approval_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.legal_approval_by_c.id_name}" 
id="{$fields.legal_approval_by_c.id_name}" 
value="{$fields.user_id12_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.legal_approval_by_c.name}" id="btn_{$fields.legal_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.legal_approval_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id12_c","name":"legal_approval_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.legal_approval_by_c.name}" id="btn_clr_{$fields.legal_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.legal_approval_by_c.name}', '{$fields.legal_approval_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.legal_approval_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id12_c" class="sugar_field" data-id-value="{$fields.user_id12_c.value}">{$fields.legal_approval_by_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.mgmt_apv_by_c.acl > 1 || ($showDetailData && $fields.mgmt_apv_by_c.acl > 0)}

<td valign="top" id='mgmt_apv_by_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MGMT_APV_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.mgmt_apv_by_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.mgmt_apv_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.mgmt_apv_by_c.name}" size="" value="{$fields.mgmt_apv_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.mgmt_apv_by_c.id_name}" 
id="{$fields.mgmt_apv_by_c.id_name}" 
value="{$fields.user_id1_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.mgmt_apv_by_c.name}" id="btn_{$fields.mgmt_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.mgmt_apv_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id1_c","name":"mgmt_apv_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.mgmt_apv_by_c.name}" id="btn_clr_{$fields.mgmt_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.mgmt_apv_by_c.name}', '{$fields.mgmt_apv_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.mgmt_apv_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id1_c" class="sugar_field" data-id-value="{$fields.user_id1_c.value}">{$fields.mgmt_apv_by_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.ssd_approver_c.acl > 1 || ($showDetailData && $fields.ssd_approver_c.acl > 0)}

<td valign="top" id='ssd_approver_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_SSD_APPROVER' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.ssd_approver_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.ssd_approver_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.ssd_approver_c.name}" size="" value="{$fields.ssd_approver_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.ssd_approver_c.id_name}" 
id="{$fields.ssd_approver_c.id_name}" 
value="{$fields.user_id10_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.ssd_approver_c.name}" id="btn_{$fields.ssd_approver_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.ssd_approver_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id10_c","name":"ssd_approver_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.ssd_approver_c.name}" id="btn_clr_{$fields.ssd_approver_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.ssd_approver_c.name}', '{$fields.ssd_approver_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.ssd_approver_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id10_c" class="sugar_field" data-id-value="{$fields.user_id10_c.value}">{$fields.ssd_approver_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.ceo_office_apv_by_c.acl > 1 || ($showDetailData && $fields.ceo_office_apv_by_c.acl > 0)}

<td valign="top" id='ceo_office_apv_by_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_OFFICE_APV_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.ceo_office_apv_by_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.ceo_office_apv_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.ceo_office_apv_by_c.name}" size="" value="{$fields.ceo_office_apv_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.ceo_office_apv_by_c.id_name}" 
id="{$fields.ceo_office_apv_by_c.id_name}" 
value="{$fields.user_id2_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.ceo_office_apv_by_c.name}" id="btn_{$fields.ceo_office_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.ceo_office_apv_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id2_c","name":"ceo_office_apv_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.ceo_office_apv_by_c.name}" id="btn_clr_{$fields.ceo_office_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.ceo_office_apv_by_c.name}', '{$fields.ceo_office_apv_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.ceo_office_apv_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id2_c" class="sugar_field" data-id-value="{$fields.user_id2_c.value}">{$fields.ceo_office_apv_by_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.dec_approval_by_c.acl > 1 || ($showDetailData && $fields.dec_approval_by_c.acl > 0)}

<td valign="top" id='dec_approval_by_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_DEC_APPROVAL_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.dec_approval_by_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.dec_approval_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.dec_approval_by_c.name}" size="" value="{$fields.dec_approval_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.dec_approval_by_c.id_name}" 
id="{$fields.dec_approval_by_c.id_name}" 
value="{$fields.user_id11_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.dec_approval_by_c.name}" id="btn_{$fields.dec_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.dec_approval_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id11_c","name":"dec_approval_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.dec_approval_by_c.name}" id="btn_clr_{$fields.dec_approval_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.dec_approval_by_c.name}', '{$fields.dec_approval_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.dec_approval_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id11_c" class="sugar_field" data-id-value="{$fields.user_id11_c.value}">{$fields.dec_approval_by_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.dceo_apv_by_c.acl > 1 || ($showDetailData && $fields.dceo_apv_by_c.acl > 0)}

<td valign="top" id='dceo_apv_by_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_DCEO_APV_BY' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.dceo_apv_by_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.dceo_apv_by_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.dceo_apv_by_c.name}" size="" value="{$fields.dceo_apv_by_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.dceo_apv_by_c.id_name}" 
id="{$fields.dceo_apv_by_c.id_name}" 
value="{$fields.user_id9_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.dceo_apv_by_c.name}" id="btn_{$fields.dceo_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.dceo_apv_by_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id9_c","name":"dceo_apv_by_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.dceo_apv_by_c.name}" id="btn_clr_{$fields.dceo_apv_by_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.dceo_apv_by_c.name}', '{$fields.dceo_apv_by_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.dceo_apv_by_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id9_c" class="sugar_field" data-id-value="{$fields.user_id9_c.value}">{$fields.dceo_apv_by_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}



<td valign="top" id='_label' width='12.5%' data-total-columns="2" scope="col">
&nbsp;
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >



</td>

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(2, 'expanded'); {rdelim}); </script>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_EDITVIEW_PANEL1").style.display='none';</script>
{/if}
<div id="detailpanel_3" class="{$def.templateMeta.panelClass|default:'edit view edit508'}">
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<h4>&nbsp;&nbsp;
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(3);">
<img border="0" id="detailpanel_3_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(3);">
<img border="0" id="detailpanel_3_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
{sugar_translate label='LBL_EDITVIEW_PANEL2' module='SRP_StdApproval_Routine'}
<script>
      document.getElementById('detailpanel_3').className += ' expanded';
    </script>
</h4>
<table width="100%" border="0" cellspacing="1" cellpadding="0"  id='LBL_EDITVIEW_PANEL2'  class="yui3-skin-sam edit view panelContainer">
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.member_1_c.acl > 1 || ($showDetailData && $fields.member_1_c.acl > 0)}

<td valign="top" id='member_1_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_1' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
<span class="required">*</span>
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.member_1_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.member_1_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_1_c.name}" size="" value="{$fields.member_1_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_1_c.id_name}" 
id="{$fields.member_1_c.id_name}" 
value="{$fields.user_id3_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_1_c.name}" id="btn_{$fields.member_1_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_1_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id3_c","name":"member_1_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_1_c.name}" id="btn_clr_{$fields.member_1_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_1_c.name}', '{$fields.member_1_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_1_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id3_c" class="sugar_field" data-id-value="{$fields.user_id3_c.value}">{$fields.member_1_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.member_2_c.acl > 1 || ($showDetailData && $fields.member_2_c.acl > 0)}

<td valign="top" id='member_2_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_2' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.member_2_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.member_2_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_2_c.name}" size="" value="{$fields.member_2_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_2_c.id_name}" 
id="{$fields.member_2_c.id_name}" 
value="{$fields.user_id4_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_2_c.name}" id="btn_{$fields.member_2_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_2_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id4_c","name":"member_2_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_2_c.name}" id="btn_clr_{$fields.member_2_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_2_c.name}', '{$fields.member_2_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_2_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id4_c" class="sugar_field" data-id-value="{$fields.user_id4_c.value}">{$fields.member_2_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.member_3_c.acl > 1 || ($showDetailData && $fields.member_3_c.acl > 0)}

<td valign="top" id='member_3_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_3' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.member_3_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.member_3_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_3_c.name}" size="" value="{$fields.member_3_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_3_c.id_name}" 
id="{$fields.member_3_c.id_name}" 
value="{$fields.user_id5_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_3_c.name}" id="btn_{$fields.member_3_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_3_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id5_c","name":"member_3_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_3_c.name}" id="btn_clr_{$fields.member_3_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_3_c.name}', '{$fields.member_3_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_3_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id5_c" class="sugar_field" data-id-value="{$fields.user_id5_c.value}">{$fields.member_3_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.member_4_c.acl > 1 || ($showDetailData && $fields.member_4_c.acl > 0)}

<td valign="top" id='member_4_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_4' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.member_4_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.member_4_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_4_c.name}" size="" value="{$fields.member_4_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_4_c.id_name}" 
id="{$fields.member_4_c.id_name}" 
value="{$fields.user_id6_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_4_c.name}" id="btn_{$fields.member_4_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_4_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id6_c","name":"member_4_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_4_c.name}" id="btn_clr_{$fields.member_4_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_4_c.name}', '{$fields.member_4_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_4_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id6_c" class="sugar_field" data-id-value="{$fields.user_id6_c.value}">{$fields.member_4_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.member_5_c.acl > 1 || ($showDetailData && $fields.member_5_c.acl > 0)}

<td valign="top" id='member_5_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_5' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.member_5_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.member_5_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_5_c.name}" size="" value="{$fields.member_5_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_5_c.id_name}" 
id="{$fields.member_5_c.id_name}" 
value="{$fields.user_id7_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_5_c.name}" id="btn_{$fields.member_5_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_5_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id7_c","name":"member_5_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_5_c.name}" id="btn_clr_{$fields.member_5_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_5_c.name}', '{$fields.member_5_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_5_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id7_c" class="sugar_field" data-id-value="{$fields.user_id7_c.value}">{$fields.member_5_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}


{if $fields.member_6_c.acl > 1 || ($showDetailData && $fields.member_6_c.acl > 0)}

<td valign="top" id='member_6_c_label' width='12.5%' data-total-columns="2" scope="col">
{capture name="label" assign="label"}{sugar_translate label='LBL_MEMBER_6' module='SRP_StdApproval_Routine'}{/capture}
{$label|strip_semicolon}:
</td>
{counter name="fieldsUsed"}

<td valign="top" width='37.5%' data-total-columns="2" >

{if $fields.member_6_c.acl > 1}

{counter name="panelFieldCount"}

<input type="text" name="{$fields.member_6_c.name}" class="sqsEnabled" tabindex="0" id="{$fields.member_6_c.name}" size="" value="{$fields.member_6_c.value}" title='' autocomplete="off"  	 >
<input type="hidden" name="{$fields.member_6_c.id_name}" 
id="{$fields.member_6_c.id_name}" 
value="{$fields.user_id8_c.value}">
<span class="id-ff multiple">
<button type="button" name="btn_{$fields.member_6_c.name}" id="btn_{$fields.member_6_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_TITLE"}" class="button firstChild" value="{sugar_translate label="LBL_ACCESSKEY_SELECT_USERS_LABEL"}"
onclick='open_popup(
"{$fields.member_6_c.module}", 
600, 
400, 
"", 
true, 
false, 
{literal}{"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"user_id8_c","name":"member_6_c"}}{/literal}, 
"single", 
true
);' ><span class="suitepicon suitepicon-action-select"></span></button><button type="button" name="btn_clr_{$fields.member_6_c.name}" id="btn_clr_{$fields.member_6_c.name}" tabindex="0" title="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_TITLE"}"  class="button lastChild"
onclick="SUGAR.clearRelateField(this.form, '{$fields.member_6_c.name}', '{$fields.member_6_c.id_name}');"  value="{sugar_translate label="LBL_ACCESSKEY_CLEAR_USERS_LABEL"}" ><span class="suitepicon suitepicon-action-clear"></span></button>
</span>
<script type="text/javascript">
SUGAR.util.doWhen(
		"typeof(sqs_objects) != 'undefined' && typeof(sqs_objects['{$form_name}_{$fields.member_6_c.name}']) != 'undefined'",
		enableQS
);
</script>

{else}

{counter name="panelFieldCount"}

<span id="user_id8_c" class="sugar_field" data-id-value="{$fields.user_id8_c.value}">{$fields.member_6_c.value}</span>

{/if}
{else}
<td></td><td></td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 }
{$tableRow}
{/if}
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(3, 'expanded'); {rdelim}); </script>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_EDITVIEW_PANEL2").style.display='none';</script>
{/if}
</div></div>

<script language="javascript">
    var _form_id = '{$form_id}';
    {literal}
    SUGAR.util.doWhen(function(){
        _form_id = (_form_id == '') ? 'EditView' : _form_id;
        return document.getElementById(_form_id) != null;
    }, SUGAR.themes.actionMenu);
    {/literal}
</script>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="dcQuickEdit">
<tr>
<td class="buttons">
{assign var='place' value="_FOOTER"}
<!-- to be used for id for buttons with custom code in def files-->
<div class="action_buttons">{if $bean->aclAccess("save")}<input title="{$APP.LBL_SAVE_BUTTON_TITLE}" accessKey="{$APP.LBL_SAVE_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('EditView'); {if $isDuplicate}_form.return_id.value=''; {/if}_form.action.value='Save'; if(check_form('EditView'))SUGAR.ajaxUI.submitForm(_form);return false;" type="submit" name="button" value="{$APP.LBL_SAVE_BUTTON_LABEL}" id="SAVE_FOOTER">{/if}  {if !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($smarty.request.return_id))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" type="button" id="CANCEL_FOOTER"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && !empty($fields.id.value))}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=DetailView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_FOOTER"> {elseif !empty($smarty.request.return_action) && ($smarty.request.return_action == "DetailView" && empty($fields.id.value)) && empty($smarty.request.return_id)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=ListView&module={$smarty.request.return_module|escape:"url"}&record={$fields.id.value}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_FOOTER"> {elseif !empty($smarty.request.return_action) && !empty($smarty.request.return_module)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action={$smarty.request.return_action}&module={$smarty.request.return_module|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_FOOTER"> {elseif empty($smarty.request.return_action) || empty($smarty.request.return_id) && !empty($fields.id.value)}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module=SRP_StdApproval_Routine'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_FOOTER"> {else}<input title="{$APP.LBL_CANCEL_BUTTON_TITLE}" accessKey="{$APP.LBL_CANCEL_BUTTON_KEY}" class="button" onclick="SUGAR.ajaxUI.loadContent('index.php?action=index&module={$smarty.request.return_module|escape:"url"}&record={$smarty.request.return_id|escape:"url"}'); return false;" type="button" name="button" value="{$APP.LBL_CANCEL_BUTTON_LABEL}" id="CANCEL_FOOTER"> {/if} {if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=SRP_StdApproval_Routine", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}<div class="clear"></div></div>
</td>
<td align='right' class="edit-view-pagination">
</td>
</tr>
</table>
</form>
{$set_focus_block}
<script>SUGAR.util.doWhen("document.getElementById('EditView') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script><script type="text/javascript">
YAHOO.util.Event.onContentReady("EditView",
    function () {ldelim} initEditView(document.forms.EditView) {rdelim});
//window.setTimeout(, 100);
window.onbeforeunload = function () {ldelim} return onUnloadEditView(); {rdelim};
// bug 55468 -- IE is too aggressive with onUnload event
if ($.browser.msie) {ldelim}
$(document).ready(function() {ldelim}
    $(".collapseLink,.expandLink").click(function (e) {ldelim} e.preventDefault(); {rdelim});
  {rdelim});
{rdelim}
</script>
{/if}{literal}
<script type="text/javascript">
addForm('EditView');addToValidate('EditView', 'name', 'name', true,'{/literal}{sugar_translate label='LBL_NAME' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'date_entered_date', 'date', false,'Date Created' );
addToValidate('EditView', 'date_modified_date', 'date', false,'Date Modified' );
addToValidate('EditView', 'modified_user_id', 'assigned_user_name', false,'{/literal}{sugar_translate label='LBL_MODIFIED' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'modified_by_name', 'relate', false,'{/literal}{sugar_translate label='LBL_MODIFIED_NAME' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'created_by', 'assigned_user_name', false,'{/literal}{sugar_translate label='LBL_CREATED' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'created_by_name', 'relate', false,'{/literal}{sugar_translate label='LBL_CREATED' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'description', 'text', false,'{/literal}{sugar_translate label='LBL_DESCRIPTION' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'deleted', 'bool', false,'{/literal}{sugar_translate label='LBL_DELETED' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'assigned_user_id', 'relate', false,'{/literal}{sugar_translate label='LBL_ASSIGNED_TO_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'assigned_user_name', 'relate', false,'{/literal}{sugar_translate label='LBL_ASSIGNED_TO_NAME' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'ceo_office_apv_by_c', 'relate', false,'{/literal}{sugar_translate label='LBL_CEO_OFFICE_APV_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'dceo_apv_by_c', 'relate', true,'{/literal}{sugar_translate label='LBL_DCEO_APV_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'ddm_approval_by_c', 'relate', true,'{/literal}{sugar_translate label='LBL_DDM_APPROVAL_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'dec_approval_by_c', 'relate', true,'{/literal}{sugar_translate label='LBL_DEC_APPROVAL_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'legal_approval_by_c', 'relate', true,'{/literal}{sugar_translate label='LBL_LEGAL_APPROVAL_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'member_1_c', 'relate', true,'{/literal}{sugar_translate label='LBL_MEMBER_1' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'member_2_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_2' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'member_3_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_3' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'member_4_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_4' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'member_5_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_5' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'member_6_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MEMBER_6' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'mgmt_apv_by_c', 'relate', false,'{/literal}{sugar_translate label='LBL_MGMT_APV_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'ssd_approver_c', 'relate', true,'{/literal}{sugar_translate label='LBL_SSD_APPROVER' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id10_c', 'id', false,'{/literal}{sugar_translate label='LBL_SSD_APPROVER_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id11_c', 'id', false,'{/literal}{sugar_translate label='LBL_DEC_APPROVAL_BY_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id12_c', 'id', false,'{/literal}{sugar_translate label='LBL_LEGAL_APPROVAL_BY_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id1_c', 'id', false,'{/literal}{sugar_translate label='LBL_MGMT_APV_BY_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id2_c', 'id', false,'{/literal}{sugar_translate label='LBL_CEO_OFFICE_APV_BY_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id3_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_1_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id4_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_2_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id5_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_3_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id6_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_4_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id7_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_5_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id8_c', 'id', false,'{/literal}{sugar_translate label='LBL_MEMBER_6_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id9_c', 'id', false,'{/literal}{sugar_translate label='LBL_DCEO_APV_BY_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidate('EditView', 'user_id_c', 'id', false,'{/literal}{sugar_translate label='LBL_DDM_APPROVAL_BY_USER_ID' module='SRP_StdApproval_Routine' for_js=true}{literal}' );
addToValidateBinaryDependency('EditView', 'assigned_user_name', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_ASSIGNED_TO' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'assigned_user_id' );
addToValidateBinaryDependency('EditView', 'ceo_office_apv_by_c', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_CEO_OFFICE_APV_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id2_c' );
addToValidateBinaryDependency('EditView', 'dceo_apv_by_c', 'alpha', true,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_DCEO_APV_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id9_c' );
addToValidateBinaryDependency('EditView', 'ddm_approval_by_c', 'alpha', true,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_DDM_APPROVAL_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id_c' );
addToValidateBinaryDependency('EditView', 'dec_approval_by_c', 'alpha', true,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_DEC_APPROVAL_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id11_c' );
addToValidateBinaryDependency('EditView', 'legal_approval_by_c', 'alpha', true,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_LEGAL_APPROVAL_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id12_c' );
addToValidateBinaryDependency('EditView', 'member_1_c', 'alpha', true,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_MEMBER_1' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id3_c' );
addToValidateBinaryDependency('EditView', 'member_2_c', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_MEMBER_2' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id4_c' );
addToValidateBinaryDependency('EditView', 'member_3_c', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_MEMBER_3' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id5_c' );
addToValidateBinaryDependency('EditView', 'member_4_c', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_MEMBER_4' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id6_c' );
addToValidateBinaryDependency('EditView', 'member_5_c', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_MEMBER_5' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id7_c' );
addToValidateBinaryDependency('EditView', 'member_6_c', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_MEMBER_6' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id8_c' );
addToValidateBinaryDependency('EditView', 'mgmt_apv_by_c', 'alpha', false,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_MGMT_APV_BY' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id1_c' );
addToValidateBinaryDependency('EditView', 'ssd_approver_c', 'alpha', true,'{/literal}{sugar_translate label='ERR_SQS_NO_MATCH_FIELD' module='SRP_StdApproval_Routine' for_js=true}{literal}: {/literal}{sugar_translate label='LBL_SSD_APPROVER' module='SRP_StdApproval_Routine' for_js=true}{literal}', 'user_id10_c' );
</script><script language="javascript">if(typeof sqs_objects == 'undefined'){var sqs_objects = new Array;}sqs_objects['EditView_assigned_user_name']={"form":"EditView","method":"get_user_array","field_list":["user_name","id"],"populate_list":["assigned_user_name","assigned_user_id"],"required_list":["assigned_user_id"],"conditions":[{"name":"user_name","op":"like_custom","end":"%","value":""}],"limit":"30","no_match_text":"No Match"};sqs_objects['EditView_ddm_approval_by_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["ddm_approval_by_c","user_id_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_legal_approval_by_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["legal_approval_by_c","user_id12_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_mgmt_apv_by_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["mgmt_apv_by_c","user_id1_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_ssd_approver_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["ssd_approver_c","user_id10_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_ceo_office_apv_by_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["ceo_office_apv_by_c","user_id2_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_dec_approval_by_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["dec_approval_by_c","user_id11_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_dceo_apv_by_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["dceo_apv_by_c","user_id9_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_member_1_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["member_1_c","user_id3_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_member_2_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["member_2_c","user_id4_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_member_3_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["member_3_c","user_id5_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_member_4_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["member_4_c","user_id6_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_member_5_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["member_5_c","user_id7_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};sqs_objects['EditView_member_6_c']={"form":"EditView","method":"query","modules":["Users"],"group":"or","field_list":["name","id"],"populate_list":["member_6_c","user_id8_c"],"required_list":["parent_id"],"conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],"order":"name","limit":"30","no_match_text":"No Match"};</script>{/literal}
