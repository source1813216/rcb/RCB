
{php}
global $current_user,$sugar_config;
$this->_tpl_vars['current_user'] = $current_user->id;
$this->_tpl_vars['is_admin_user'] = $current_user->is_admin;
$this->_tpl_vars['current_theme'] = $current_user->getPreference('user_theme');
$this->_tpl_vars['suite_valid_version'] =  true;
if(preg_match('/(7\.7\.[0-9])/', $sugar_config['suitecrm_version']) || $sugar_config['suitecrm_version'] == '7.7'){
$this->_tpl_vars['suite_valid_version'] = false;
}
{/php}
{if $current_theme == 'SuiteP'}

{assign var=preForm value="<table width='100%' border='1' cellspacing='0' cellpadding='0' class='converted_account'><tr><td><table width='100%'><tr><td>"}
{assign var=displayPreform value=false}
{if isset($bean->contact_id) && !empty($bean->contact_id)}
{assign var=displayPreform value=true}
{assign var=preForm value=$preForm|cat:$MOD.LBL_CONVERTED_CONTACT}
{assign var=preForm value=$preForm|cat:"&nbsp;<a href='index.php?module=Contacts&action=DetailView&record="}
{assign var=preForm value=$preForm|cat:$bean->contact_id}
{assign var=preForm value=$preForm|cat:"'>"}
{assign var=preForm value=$preForm|cat:$bean->contact_name}
{assign var=preForm value=$preForm|cat:"</a>"}
{/if}
{assign var=preForm value=$preForm|cat:"</td><td>"}
{if ($bean->converted=='1') && isset($bean->account_id) && !empty($bean->account_id)}
{assign var=displayPreform value=true}
{assign var=preForm value=$preForm|cat:$MOD.LBL_CONVERTED_ACCOUNT}
{assign var=preForm value=$preForm|cat:"&nbsp;<a href='index.php?module=Accounts&action=DetailView&record="}
{assign var=preForm value=$preForm|cat:$bean->account_id}
{assign var=preForm value=$preForm|cat:"'>"}
{assign var=preForm value=$preForm|cat:$bean->account_name}
{assign var=preForm value=$preForm|cat:"</a>"}
{/if}
{assign var=preForm value=$preForm|cat:"</td><td>"}
{if isset($bean->opportunity_id) && !empty($bean->opportunity_id)}
{assign var=displayPreform value=true}
{assign var=preForm value=$preForm|cat:$MOD.LBL_CONVERTED_OPP}
{assign var=preForm value=$preForm|cat:"&nbsp;<a href='index.php?module=Opportunities&action=DetailView&record="}
{assign var=preForm value=$preForm|cat:$bean->opportunity_id}
{assign var=preForm value=$preForm|cat:"'>"}
{assign var=preForm value=$preForm|cat:$bean->opportunity_name}
{assign var=preForm value=$preForm|cat:"</a>"}
{/if}
{assign var=preForm value=$preForm|cat:"</td></tr></table></td></tr></table>"}
{if $displayPreform}
{$preForm}
<br>
{/if}

<script language="javascript">
    {literal}
    SUGAR.util.doWhen(function () {
        return $("#contentTable").length == 0;
    }, SUGAR.themes.actionMenu);
    {/literal}
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="{$module}">
<input type="hidden" name="record" value="{$fields.id.value}">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="{$offset}">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
{if !$config.enable_action_menu}
<div class="buttons">
{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} 
{if $bean->aclAccess("edit")}<input title="{$APP.LBL_DUPLICATE_BUTTON_TITLE}" accessKey="{$APP.LBL_DUPLICATE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.isDuplicate.value=true; _form.action.value='EditView'; _form.return_id.value='{$id}';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Duplicate" value="{$APP.LBL_DUPLICATE_BUTTON_LABEL}" id="duplicate_button">{/if} 
{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} 
<input class="button hidden" id="send_confirm_opt_in_email" title="{$APP.LBL_SEND_CONFIRM_OPT_IN_EMAIL}" onclick="var _form = document.getElementById('formDetailView');_form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='{$fields.id.value}'; _form.action.value='sendConfirmOptInEmail'; _form.module.value='Leads'; _form.module_tab.value='Leads';_form.submit();" name="send_confirm_opt_in_email" disabled="1" type="button" value="{$APP.LBL_SEND_CONFIRM_OPT_IN_EMAIL}"/>
<input type="button" class="button" onClick="showPopup();" value="{$APP.LBL_PRINT_AS_PDF}"/>
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=Leads", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>                    {/if}
</form>
</div>
</td>
<td align="right" width="20%" class="buttons">{$ADMIN_EDIT}
</td>
</tr>
</table>
{sugar_include include=$includes}
<div class="detail-view">
{if $suite_valid_version}
<div class="mobile-pagination">{$PAGINATION}</div>
{/if}

<ul class="nav nav-tabs">


<li role="presentation" class="active">
<a id="tab0" data-toggle="tab" class="hidden-xs">
{sugar_translate label='LBL_DETAILVIEW_PANEL5' module='Leads'}
</a>


<a id="xstab0" href="#" class="visible-xs first-tab-xs dropdown-toggle" data-toggle="dropdown">
{sugar_translate label='LBL_DETAILVIEW_PANEL5' module='Leads'}
</a>
<ul id="first-tab-menu-xs" class="dropdown-menu">
<li role="presentation">
<a id="tab1" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-1');">
{sugar_translate label='LBL_DETAILVIEW_PANEL5' module='Leads'}
</a>
</li>
<li role="presentation">
<a id="tab10" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-10');">
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='Leads'}
</a>
</li>
<li role="presentation">
<a id="tab11" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-11');">
{sugar_translate label='LBL_DETAILVIEW_PANEL4' module='Leads'}
</a>
</li>
<li role="presentation">
<a id="tab12" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-12');">
{sugar_translate label='LBL_PANEL_ASSIGNMENT' module='Leads'}
</a>
</li>
</ul>
</li>


















<li role="presentation" class="hidden-xs">
<a id="tab1" data-toggle="tab">
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='Leads'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab2" data-toggle="tab">
{sugar_translate label='LBL_DETAILVIEW_PANEL4' module='Leads'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab3" data-toggle="tab">
{sugar_translate label='LBL_PANEL_ASSIGNMENT' module='Leads'}
</a>
</li>
{if $config.enable_action_menu and $config.enable_action_menu != false}
<li id="tab-actions" class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">ACTIONS<span class="suitepicon suitepicon-action-caret"></span></a>
<ul class="dropdown-menu">
<li>{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} </li>
<li>{if $bean->aclAccess("edit")}<input title="{$APP.LBL_DUPLICATE_BUTTON_TITLE}" accessKey="{$APP.LBL_DUPLICATE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.isDuplicate.value=true; _form.action.value='EditView'; _form.return_id.value='{$id}';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Duplicate" value="{$APP.LBL_DUPLICATE_BUTTON_LABEL}" id="duplicate_button">{/if} </li>
<li>{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} </li>
<li><input class="button hidden" id="send_confirm_opt_in_email" title="{$APP.LBL_SEND_CONFIRM_OPT_IN_EMAIL}" onclick="var _form = document.getElementById('formDetailView');_form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='{$fields.id.value}'; _form.action.value='sendConfirmOptInEmail'; _form.module.value='Leads'; _form.module_tab.value='Leads';_form.submit();" name="send_confirm_opt_in_email" disabled="1" type="button" value="{$APP.LBL_SEND_CONFIRM_OPT_IN_EMAIL}"/></li>
<li><input type="button" class="button" onClick="showPopup();" value="{$APP.LBL_PRINT_AS_PDF}"/></li>
<li>{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=Leads", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}</li>
</ul>        </li>
{if $suite_valid_version}
<li class="tab-inline-pagination">
{$PAGINATION}
</li>
{/if}
{/if}
</ul>
<div class="clearfix"></div>

<div class="tab-content">

<div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-0'>





<div class="row detail-view-row">


{if $fields.status.acl > 1 || $fields.status.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STATUS' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="status"  >

{if !$fields.status.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.status.options)}
<input type="hidden" class="sugar_field" id="{$fields.status.name}" value="{ $fields.status.options }">
{ $fields.status.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.status.name}" value="{ $fields.status.value }">
{ $fields.status.options[$fields.status.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.reason_c.acl > 1 || $fields.reason_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_REASON' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="text" field="reason_c"  >

{if !$fields.reason_c.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.reason_c.name|escape:'html'|url2html|nl2br}">{$fields.reason_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.start_engagement_c.acl > 1 || $fields.start_engagement_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_START_ENGAGEMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_engagement_c"  >

{if !$fields.start_engagement_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.start_engagement_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_engagement_c.name}" value="{ $fields.start_engagement_c.options }">
{ $fields.start_engagement_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_engagement_c.name}" value="{ $fields.start_engagement_c.value }">
{ $fields.start_engagement_c.options[$fields.start_engagement_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.start_event_approval_c.acl > 1 || $fields.start_event_approval_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_START_EVENT_APPROVAL' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_event_approval_c"  >

{if !$fields.start_event_approval_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.start_event_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_event_approval_c.name}" value="{ $fields.start_event_approval_c.options }">
{ $fields.start_event_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_event_approval_c.name}" value="{ $fields.start_event_approval_c.value }">
{ $fields.start_event_approval_c.options[$fields.start_event_approval_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.start_eoi_c.acl > 1 || $fields.start_eoi_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_START_EOI' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_eoi_c"  >

{if !$fields.start_eoi_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.start_eoi_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_eoi_c.name}" value="{ $fields.start_eoi_c.options }">
{ $fields.start_eoi_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_eoi_c.name}" value="{ $fields.start_eoi_c.value }">
{ $fields.start_eoi_c.options[$fields.start_eoi_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.start_endorsement_c.acl > 1 || $fields.start_endorsement_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_START_ENDORSEMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_endorsement_c"  >

{if !$fields.start_endorsement_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.start_endorsement_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_endorsement_c.name}" value="{ $fields.start_endorsement_c.options }">
{ $fields.start_endorsement_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_endorsement_c.name}" value="{ $fields.start_endorsement_c.value }">
{ $fields.start_endorsement_c.options[$fields.start_endorsement_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.start_event_docs_c.acl > 1 || $fields.start_event_docs_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_START_EVENT_DOCS' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_event_docs_c"  >

{if !$fields.start_event_docs_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.start_event_docs_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_event_docs_c.name}" value="{ $fields.start_event_docs_c.options }">
{ $fields.start_event_docs_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_event_docs_c.name}" value="{ $fields.start_event_docs_c.value }">
{ $fields.start_event_docs_c.options[$fields.start_event_docs_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.start_inspection_c.acl > 1 || $fields.start_inspection_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_START_INSPECTION' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_inspection_c"  >

{if !$fields.start_inspection_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.start_inspection_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_inspection_c.name}" value="{ $fields.start_inspection_c.options }">
{ $fields.start_inspection_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_inspection_c.name}" value="{ $fields.start_inspection_c.value }">
{ $fields.start_inspection_c.options[$fields.start_inspection_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.start_presentation_c.acl > 1 || $fields.start_presentation_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_START_PRESENTATION' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_presentation_c"  >

{if !$fields.start_presentation_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.start_presentation_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_presentation_c.name}" value="{ $fields.start_presentation_c.options }">
{ $fields.start_presentation_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_presentation_c.name}" value="{ $fields.start_presentation_c.value }">
{ $fields.start_presentation_c.options[$fields.start_presentation_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.start_mou_c.acl > 1 || $fields.start_mou_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_START_MOU' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="start_mou_c"  >

{if !$fields.start_mou_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.start_mou_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_mou_c.name}" value="{ $fields.start_mou_c.options }">
{ $fields.start_mou_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_mou_c.name}" value="{ $fields.start_mou_c.value }">
{ $fields.start_mou_c.options[$fields.start_mou_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-9'>





<div class="row detail-view-row">


{if $fields.account_c.acl > 1 || $fields.account_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ACCOUNT' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="account_c"  >

{if !$fields.account_c.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.account_id_c.value)}
{capture assign="detail_url"}index.php?module=Accounts&action=DetailView&record={$fields.account_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="account_id_c" class="sugar_field" data-id-value="{$fields.account_id_c.value}">{$fields.account_c.value}</span>
{if !empty($fields.account_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.segment_c.acl > 1 || $fields.segment_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SEGMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="segment_c"  >

{if !$fields.segment_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.segment_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.segment_c.name}" value="{ $fields.segment_c.options }">
{ $fields.segment_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.segment_c.name}" value="{ $fields.segment_c.value }">
{ $fields.segment_c.options[$fields.segment_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.event_title_c.acl > 1 || $fields.event_title_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_TITLE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="event_title_c"  >

{if !$fields.event_title_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.event_title_c.value) <= 0}
{assign var="value" value=$fields.event_title_c.default_value }
{else}
{assign var="value" value=$fields.event_title_c.value }
{/if} 
<span class="sugar_field" id="{$fields.event_title_c.name}">{$fields.event_title_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.primary_contact_c.acl > 1 || $fields.primary_contact_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRIMARY_CONTACT' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="primary_contact_c"  >

{if !$fields.primary_contact_c.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.contact_id_c.value)}
{capture assign="detail_url"}index.php?module=Contacts&action=DetailView&record={$fields.contact_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="contact_id_c" class="sugar_field" data-id-value="{$fields.contact_id_c.value}">{$fields.primary_contact_c.value}</span>
{if !empty($fields.contact_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.event_abbreviation_c.acl > 1 || $fields.event_abbreviation_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_ABBREVIATION' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="event_abbreviation_c"  >

{if !$fields.event_abbreviation_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.event_abbreviation_c.value) <= 0}
{assign var="value" value=$fields.event_abbreviation_c.default_value }
{else}
{assign var="value" value=$fields.event_abbreviation_c.value }
{/if} 
<span class="sugar_field" id="{$fields.event_abbreviation_c.name}">{$fields.event_abbreviation_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.event_date_c.acl > 1 || $fields.event_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="date" field="event_date_c"  >

{if !$fields.event_date_c.hidden}
{counter name="panelFieldCount" print=false}


{if strlen($fields.event_date_c.value) <= 0}
{assign var="value" value=$fields.event_date_c.default_value }
{else}
{assign var="value" value=$fields.event_date_c.value }
{/if}
<span class="sugar_field" id="{$fields.event_date_c.name}">{$value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.event_end_date_c.acl > 1 || $fields.event_end_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_END_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="date" field="event_end_date_c"  >

{if !$fields.event_end_date_c.hidden}
{counter name="panelFieldCount" print=false}


{if strlen($fields.event_end_date_c.value) <= 0}
{assign var="value" value=$fields.event_end_date_c.default_value }
{else}
{assign var="value" value=$fields.event_end_date_c.value }
{/if}
<span class="sugar_field" id="{$fields.event_end_date_c.name}">{$value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.packs_c.acl > 1 || $fields.packs_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PACKS' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="int" field="packs_c"  >

{if !$fields.packs_c.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.packs_c.name}">
{assign var="value" value=$fields.packs_c.value }
{$value}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.event_days_c.acl > 1 || $fields.event_days_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DAYS' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="int" field="event_days_c"  >

{if !$fields.event_days_c.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.event_days_c.name}">
{assign var="value" value=$fields.event_days_c.value }
{$value}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>


{if $fields.currency_id.acl > 1 || $fields.currency_id.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CURRENCY' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency_id" field="currency_id"  >

{if !$fields.currency_id.hidden}
{counter name="panelFieldCount" print=false}
<span id='currency_id_span'>
{$fields.currency_id.value}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.expected_revenue_c.acl > 1 || $fields.expected_revenue_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EXPECTED_REVENUE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="expected_revenue_c"  >

{if !$fields.expected_revenue_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.expected_revenue_c.name}'>
{sugar_number_format var=$fields.expected_revenue_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.gor_commitment_c.acl > 1 || $fields.gor_commitment_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_COMMITMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="gor_commitment_c"  >

{if !$fields.gor_commitment_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.gor_commitment_c.name}'>
{sugar_number_format var=$fields.gor_commitment_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.lead_source.acl > 1 || $fields.lead_source.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_LEAD_SOURCE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="lead_source"  >

{if !$fields.lead_source.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.lead_source.options)}
<input type="hidden" class="sugar_field" id="{$fields.lead_source.name}" value="{ $fields.lead_source.options }">
{ $fields.lead_source.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.lead_source.name}" value="{ $fields.lead_source.value }">
{ $fields.lead_source.options[$fields.lead_source.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.source_details_c.acl > 1 || $fields.source_details_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SOURCE_DETAILS' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="dynamicenum" field="source_details_c"  >

{if !$fields.source_details_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.source_details_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.source_details_c.name}" value="{ $fields.source_details_c.options }">
{ $fields.source_details_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.source_details_c.name}" value="{ $fields.source_details_c.value }">
{ $fields.source_details_c.options[$fields.source_details_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.probability_c.acl > 1 || $fields.probability_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PROBABILITY ' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="probability_c"  >

{if !$fields.probability_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.probability_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.probability_c.name}" value="{ $fields.probability_c.options }">
{ $fields.probability_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.probability_c.name}" value="{ $fields.probability_c.value }">
{ $fields.probability_c.options[$fields.probability_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.event_coordinator_c.acl > 1 || $fields.event_coordinator_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_COORDINATOR' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="event_coordinator_c"  >

{if !$fields.event_coordinator_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id2_c" class="sugar_field" data-id-value="{$fields.user_id2_c.value}">{$fields.event_coordinator_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.line_institute_c.acl > 1 || $fields.line_institute_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_LINE_INSTITUTE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="line_institute_c"  >

{if !$fields.line_institute_c.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.lip_line_institutes_id_c.value)}
{capture assign="detail_url"}index.php?module=LIP_Line_Institutes&action=DetailView&record={$fields.lip_line_institutes_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="lip_line_institutes_id_c" class="sugar_field" data-id-value="{$fields.lip_line_institutes_id_c.value}">{$fields.line_institute_c.value}</span>
{if !empty($fields.lip_line_institutes_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.venue_c.acl > 1 || $fields.venue_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_VENUE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="venue_c"  >

{if !$fields.venue_c.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.vnp_venues_id_c.value)}
{capture assign="detail_url"}index.php?module=VNP_Venues&action=DetailView&record={$fields.vnp_venues_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="vnp_venues_id_c" class="sugar_field" data-id-value="{$fields.vnp_venues_id_c.value}">{$fields.venue_c.value}</span>
{if !empty($fields.vnp_venues_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.stp_sales_target_leads_1_name.acl > 1 || $fields.stp_sales_target_leads_1_name.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STP_SALES_TARGET_LEADS_1_FROM_STP_SALES_TARGET_TITLE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="stp_sales_target_leads_1_name"  >

{if !$fields.stp_sales_target_leads_1_name.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.stp_sales_target_leads_1stp_sales_target_ida.value)}
{capture assign="detail_url"}index.php?module=STP_Sales_Target&action=DetailView&record={$fields.stp_sales_target_leads_1stp_sales_target_ida.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="stp_sales_target_leads_1stp_sales_target_ida" class="sugar_field" data-id-value="{$fields.stp_sales_target_leads_1stp_sales_target_ida.value}">{$fields.stp_sales_target_leads_1_name.value}</span>
{if !empty($fields.stp_sales_target_leads_1stp_sales_target_ida.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-10'>





<div class="row detail-view-row">


{if $fields.event_brief_c.acl > 1 || $fields.event_brief_c.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_BRIEF' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field " type="wysiwyg" field="event_brief_c" colspan='3' >

{if !$fields.event_brief_c.hidden}
{counter name="panelFieldCount" print=false}

<iframe
id="{$fields.event_brief_c.name}"
name="{$fields.event_brief_c.name}"
srcdoc="{$fields.event_brief_c.value}"
style="width:100%;height:500px"
></iframe>
{/if}

</div>


</div>

{/if}
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-11'>





<div class="row detail-view-row">


{if $fields.lead_progress_c.acl > 1 || $fields.lead_progress_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_LEAD_PROGRESS' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="lead_progress_c"  >

{if !$fields.lead_progress_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.lead_progress_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.lead_progress_c.name}" value="{ $fields.lead_progress_c.options }">
{ $fields.lead_progress_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.lead_progress_c.name}" value="{ $fields.lead_progress_c.value }">
{ $fields.lead_progress_c.options[$fields.lead_progress_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.assigned_user_name.acl > 1 || $fields.assigned_user_name.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ASSIGNED_TO' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="assigned_user_name"  >

{if !$fields.assigned_user_name.hidden}
{counter name="panelFieldCount" print=false}

<span id="assigned_user_id" class="sugar_field" data-id-value="{$fields.assigned_user_id.value}">{$fields.assigned_user_name.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.day_spending_c.acl > 1 || $fields.day_spending_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DAY_SPENDING' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="day_spending_c"  >

{if !$fields.day_spending_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.day_spending_c.name}'>
{sugar_number_format var=$fields.day_spending_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.created_by_name.acl > 1 || $fields.created_by_name.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="relate" field="created_by_name"  >

{if !$fields.created_by_name.hidden}
{counter name="panelFieldCount" print=false}

<span id="created_by" class="sugar_field" data-id-value="{$fields.created_by.value}">{$fields.created_by_name.value}</span>
{/if}

</div>


</div>

{/if}


{if $fields.modified_by_name.acl > 1 || $fields.modified_by_name.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MODIFIED_NAME' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="relate" field="modified_by_name"  >

{if !$fields.modified_by_name.hidden}
{counter name="panelFieldCount" print=false}

<span id="modified_user_id" class="sugar_field" data-id-value="{$fields.modified_user_id.value}">{$fields.modified_by_name.value}</span>
{/if}

</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.date_entered.acl > 1 || $fields.date_entered.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_ENTERED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="datetime" field="date_entered"  >

{if !$fields.date_entered.hidden}
{counter name="panelFieldCount" print=false}
<span id="date_entered" class="sugar_field">{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}</span>
{/if}

</div>


</div>

{/if}


{if $fields.date_modified.acl > 1 || $fields.date_modified.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_MODIFIED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="datetime" field="date_modified"  >

{if !$fields.date_modified.hidden}
{counter name="panelFieldCount" print=false}
<span id="date_modified" class="sugar_field">{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}</span>
{/if}

</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.client_reponse_details_c.acl > 1 || $fields.client_reponse_details_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CLIENT_REPONSE_DETAILS' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="text" field="client_reponse_details_c"  >

{if !$fields.client_reponse_details_c.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.client_reponse_details_c.name|escape:'html'|url2html|nl2br}">{$fields.client_reponse_details_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.gor_revenue_c.acl > 1 || $fields.gor_revenue_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_REVENUE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="gor_revenue_c"  >

{if !$fields.gor_revenue_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.gor_revenue_c.name}'>
{sugar_number_format var=$fields.gor_revenue_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.sales_target_c.acl > 1 || $fields.sales_target_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SALES_TARGET' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="currency" field="sales_target_c"  >

{if !$fields.sales_target_c.hidden}
{counter name="panelFieldCount" print=false}

<span id='{$fields.sales_target_c.name}'>
{sugar_number_format var=$fields.sales_target_c.value }
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.qualfication_c.acl > 1 || $fields.qualfication_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_QUALFICATION' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="qualfication_c"  >

{if !$fields.qualfication_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.qualfication_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.qualfication_c.name}" value="{ $fields.qualfication_c.options }">
{ $fields.qualfication_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.qualfication_c.name}" value="{ $fields.qualfication_c.value }">
{ $fields.qualfication_c.options[$fields.qualfication_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.step2_created_c.acl > 1 || $fields.step2_created_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STEP2_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step2_created_c"  >

{if !$fields.step2_created_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.step2_created_c.value) == "1" || strval($fields.step2_created_c.value) == "yes" || strval($fields.step2_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step2_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step2_created_c.name}" id="{$fields.step2_created_c.name}" value="$fields.step2_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.step3_created_c.acl > 1 || $fields.step3_created_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STEP3_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step3_created_c"  >

{if !$fields.step3_created_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.step3_created_c.value) == "1" || strval($fields.step3_created_c.value) == "yes" || strval($fields.step3_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step3_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step3_created_c.name}" id="{$fields.step3_created_c.name}" value="$fields.step3_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.step4_created_c.acl > 1 || $fields.step4_created_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STEP4_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step4_created_c"  >

{if !$fields.step4_created_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.step4_created_c.value) == "1" || strval($fields.step4_created_c.value) == "yes" || strval($fields.step4_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step4_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step4_created_c.name}" id="{$fields.step4_created_c.name}" value="$fields.step4_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.step5_created_c.acl > 1 || $fields.step5_created_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STEP5_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step5_created_c"  >

{if !$fields.step5_created_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.step5_created_c.value) == "1" || strval($fields.step5_created_c.value) == "yes" || strval($fields.step5_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step5_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step5_created_c.name}" id="{$fields.step5_created_c.name}" value="$fields.step5_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.step6_created_c.acl > 1 || $fields.step6_created_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STEP6_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step6_created_c"  >

{if !$fields.step6_created_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.step6_created_c.value) == "1" || strval($fields.step6_created_c.value) == "yes" || strval($fields.step6_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step6_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step6_created_c.name}" id="{$fields.step6_created_c.name}" value="$fields.step6_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.step7_created_c.acl > 1 || $fields.step7_created_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STEP7_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step7_created_c"  >

{if !$fields.step7_created_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.step7_created_c.value) == "1" || strval($fields.step7_created_c.value) == "yes" || strval($fields.step7_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step7_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step7_created_c.name}" id="{$fields.step7_created_c.name}" value="$fields.step7_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.step8_created_c.acl > 1 || $fields.step8_created_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STEP8_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step8_created_c"  >

{if !$fields.step8_created_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.step8_created_c.value) == "1" || strval($fields.step8_created_c.value) == "yes" || strval($fields.step8_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step8_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step8_created_c.name}" id="{$fields.step8_created_c.name}" value="$fields.step8_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.step3_eoi_chk_c.acl > 1 || $fields.step3_eoi_chk_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STEP3_EOI_CHK' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step3_eoi_chk_c"  >

{if !$fields.step3_eoi_chk_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.step3_eoi_chk_c.value) == "1" || strval($fields.step3_eoi_chk_c.value) == "yes" || strval($fields.step3_eoi_chk_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step3_eoi_chk_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step3_eoi_chk_c.name}" id="{$fields.step3_eoi_chk_c.name}" value="$fields.step3_eoi_chk_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.step4_endo_chk_c.acl > 1 || $fields.step4_endo_chk_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_STEP4_ENDO_CHK' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="bool" field="step4_endo_chk_c"  >

{if !$fields.step4_endo_chk_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.step4_endo_chk_c.value) == "1" || strval($fields.step4_endo_chk_c.value) == "yes" || strval($fields.step4_endo_chk_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step4_endo_chk_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step4_endo_chk_c.name}" id="{$fields.step4_endo_chk_c.name}" value="$fields.step4_endo_chk_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.project_created_c.acl > 1 || $fields.project_created_c.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PROJECT_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="bool" field="project_created_c" colspan='3' >

{if !$fields.project_created_c.hidden}
{counter name="panelFieldCount" print=false}

{if strval($fields.project_created_c.value) == "1" || strval($fields.project_created_c.value) == "yes" || strval($fields.project_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.project_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.project_created_c.name}" id="{$fields.project_created_c.name}" value="$fields.project_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                    </div>
</div>

<div class="panel-content">
<div>&nbsp;</div>







{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-0" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL7' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-0"  data-id="LBL_DETAILVIEW_PANEL7">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.initial_email_c.acl > 1 || $fields.initial_email_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INITIAL_EMAIL' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="initial_email_c"  >

{if !$fields.initial_email_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.initial_email_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.initial_email_c.name}" value="{ $fields.initial_email_c.options }">
{ $fields.initial_email_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.initial_email_c.name}" value="{ $fields.initial_email_c.value }">
{ $fields.initial_email_c.options[$fields.initial_email_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.engaged_by_c.acl > 1 || $fields.engaged_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ENGAGED_BY' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="engaged_by_c"  >

{if !$fields.engaged_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id1_c" class="sugar_field" data-id-value="{$fields.user_id1_c.value}">{$fields.engaged_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.sent_date_c.acl > 1 || $fields.sent_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SENT_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="sent_date_c"  >

{if !$fields.sent_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.sent_date_c.value) <= 0}
{assign var="value" value=$fields.sent_date_c.default_value }
{else}
{assign var="value" value=$fields.sent_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.sent_date_c.name}">{$fields.sent_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.responded_date_c.acl > 1 || $fields.responded_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RESPONDED_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="responded_date_c"  >

{if !$fields.responded_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.responded_date_c.value) <= 0}
{assign var="value" value=$fields.responded_date_c.default_value }
{else}
{assign var="value" value=$fields.responded_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.responded_date_c.name}">{$fields.responded_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-0" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL7' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-0" data-id="LBL_DETAILVIEW_PANEL7">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.initial_email_c.acl > 1 || $fields.initial_email_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INITIAL_EMAIL' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="initial_email_c"  >

{if !$fields.initial_email_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.initial_email_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.initial_email_c.name}" value="{ $fields.initial_email_c.options }">
{ $fields.initial_email_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.initial_email_c.name}" value="{ $fields.initial_email_c.value }">
{ $fields.initial_email_c.options[$fields.initial_email_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.engaged_by_c.acl > 1 || $fields.engaged_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ENGAGED_BY' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="engaged_by_c"  >

{if !$fields.engaged_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id1_c" class="sugar_field" data-id-value="{$fields.user_id1_c.value}">{$fields.engaged_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.sent_date_c.acl > 1 || $fields.sent_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SENT_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="sent_date_c"  >

{if !$fields.sent_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.sent_date_c.value) <= 0}
{assign var="value" value=$fields.sent_date_c.default_value }
{else}
{assign var="value" value=$fields.sent_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.sent_date_c.name}">{$fields.sent_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.responded_date_c.acl > 1 || $fields.responded_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_RESPONDED_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="responded_date_c"  >

{if !$fields.responded_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.responded_date_c.value) <= 0}
{assign var="value" value=$fields.responded_date_c.default_value }
{else}
{assign var="value" value=$fields.responded_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.responded_date_c.name}">{$fields.responded_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-1" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL8' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-1"  data-id="LBL_DETAILVIEW_PANEL8">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.initial_event_approval_c.acl > 1 || $fields.initial_event_approval_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INITIAL_EVENT_APPROVAL' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="initial_event_approval_c"  >

{if !$fields.initial_event_approval_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.initial_event_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.initial_event_approval_c.name}" value="{ $fields.initial_event_approval_c.options }">
{ $fields.initial_event_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.initial_event_approval_c.name}" value="{ $fields.initial_event_approval_c.value }">
{ $fields.initial_event_approval_c.options[$fields.initial_event_approval_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.iea_start_date_c.acl > 1 || $fields.iea_start_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_IEA_START_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="iea_start_date_c"  >

{if !$fields.iea_start_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.iea_start_date_c.value) <= 0}
{assign var="value" value=$fields.iea_start_date_c.default_value }
{else}
{assign var="value" value=$fields.iea_start_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.iea_start_date_c.name}">{$fields.iea_start_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.iea_end_date_c.acl > 1 || $fields.iea_end_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_IEA_END_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="iea_end_date_c"  >

{if !$fields.iea_end_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.iea_end_date_c.value) <= 0}
{assign var="value" value=$fields.iea_end_date_c.default_value }
{else}
{assign var="value" value=$fields.iea_end_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.iea_end_date_c.name}">{$fields.iea_end_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-1" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL8' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-1" data-id="LBL_DETAILVIEW_PANEL8">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.initial_event_approval_c.acl > 1 || $fields.initial_event_approval_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INITIAL_EVENT_APPROVAL' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="initial_event_approval_c"  >

{if !$fields.initial_event_approval_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.initial_event_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.initial_event_approval_c.name}" value="{ $fields.initial_event_approval_c.options }">
{ $fields.initial_event_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.initial_event_approval_c.name}" value="{ $fields.initial_event_approval_c.value }">
{ $fields.initial_event_approval_c.options[$fields.initial_event_approval_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.iea_start_date_c.acl > 1 || $fields.iea_start_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_IEA_START_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="iea_start_date_c"  >

{if !$fields.iea_start_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.iea_start_date_c.value) <= 0}
{assign var="value" value=$fields.iea_start_date_c.default_value }
{else}
{assign var="value" value=$fields.iea_start_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.iea_start_date_c.name}">{$fields.iea_start_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.iea_end_date_c.acl > 1 || $fields.iea_end_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_IEA_END_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="iea_end_date_c"  >

{if !$fields.iea_end_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.iea_end_date_c.value) <= 0}
{assign var="value" value=$fields.iea_end_date_c.default_value }
{else}
{assign var="value" value=$fields.iea_end_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.iea_end_date_c.name}">{$fields.iea_end_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-2" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL9' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-2"  data-id="LBL_DETAILVIEW_PANEL9">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.eoi_c.acl > 1 || $fields.eoi_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EOI' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="eoi_c"  >

{if !$fields.eoi_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.eoi_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.eoi_c.name}" value="{ $fields.eoi_c.options }">
{ $fields.eoi_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.eoi_c.name}" value="{ $fields.eoi_c.value }">
{ $fields.eoi_c.options[$fields.eoi_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.eoi_approval_start_c.acl > 1 || $fields.eoi_approval_start_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EOI_APPROVAL_START' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="eoi_approval_start_c"  >

{if !$fields.eoi_approval_start_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.eoi_approval_start_c.value) <= 0}
{assign var="value" value=$fields.eoi_approval_start_c.default_value }
{else}
{assign var="value" value=$fields.eoi_approval_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.eoi_approval_start_c.name}">{$fields.eoi_approval_start_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.eoi_approval_end_c.acl > 1 || $fields.eoi_approval_end_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EOI_APPROVAL_END' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="eoi_approval_end_c"  >

{if !$fields.eoi_approval_end_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.eoi_approval_end_c.value) <= 0}
{assign var="value" value=$fields.eoi_approval_end_c.default_value }
{else}
{assign var="value" value=$fields.eoi_approval_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.eoi_approval_end_c.name}">{$fields.eoi_approval_end_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-2" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL9' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-2" data-id="LBL_DETAILVIEW_PANEL9">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.eoi_c.acl > 1 || $fields.eoi_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EOI' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="eoi_c"  >

{if !$fields.eoi_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.eoi_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.eoi_c.name}" value="{ $fields.eoi_c.options }">
{ $fields.eoi_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.eoi_c.name}" value="{ $fields.eoi_c.value }">
{ $fields.eoi_c.options[$fields.eoi_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.eoi_approval_start_c.acl > 1 || $fields.eoi_approval_start_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EOI_APPROVAL_START' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="eoi_approval_start_c"  >

{if !$fields.eoi_approval_start_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.eoi_approval_start_c.value) <= 0}
{assign var="value" value=$fields.eoi_approval_start_c.default_value }
{else}
{assign var="value" value=$fields.eoi_approval_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.eoi_approval_start_c.name}">{$fields.eoi_approval_start_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.eoi_approval_end_c.acl > 1 || $fields.eoi_approval_end_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EOI_APPROVAL_END' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="eoi_approval_end_c"  >

{if !$fields.eoi_approval_end_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.eoi_approval_end_c.value) <= 0}
{assign var="value" value=$fields.eoi_approval_end_c.default_value }
{else}
{assign var="value" value=$fields.eoi_approval_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.eoi_approval_end_c.name}">{$fields.eoi_approval_end_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-3" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL10' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-3"  data-id="LBL_DETAILVIEW_PANEL10">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.endorsement_c.acl > 1 || $fields.endorsement_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ENDORSEMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="endorsement_c"  >

{if !$fields.endorsement_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.endorsement_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.endorsement_c.name}" value="{ $fields.endorsement_c.options }">
{ $fields.endorsement_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.endorsement_c.name}" value="{ $fields.endorsement_c.value }">
{ $fields.endorsement_c.options[$fields.endorsement_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.endorsement_start_c.acl > 1 || $fields.endorsement_start_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ENDORSEMENT_START' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="endorsement_start_c"  >

{if !$fields.endorsement_start_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.endorsement_start_c.value) <= 0}
{assign var="value" value=$fields.endorsement_start_c.default_value }
{else}
{assign var="value" value=$fields.endorsement_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.endorsement_start_c.name}">{$fields.endorsement_start_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.endorsement_end_c.acl > 1 || $fields.endorsement_end_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ENDORSEMENT_END' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="endorsement_end_c"  >

{if !$fields.endorsement_end_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.endorsement_end_c.value) <= 0}
{assign var="value" value=$fields.endorsement_end_c.default_value }
{else}
{assign var="value" value=$fields.endorsement_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.endorsement_end_c.name}">{$fields.endorsement_end_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-3" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL10' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-3" data-id="LBL_DETAILVIEW_PANEL10">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.endorsement_c.acl > 1 || $fields.endorsement_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ENDORSEMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="endorsement_c"  >

{if !$fields.endorsement_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.endorsement_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.endorsement_c.name}" value="{ $fields.endorsement_c.options }">
{ $fields.endorsement_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.endorsement_c.name}" value="{ $fields.endorsement_c.value }">
{ $fields.endorsement_c.options[$fields.endorsement_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.endorsement_start_c.acl > 1 || $fields.endorsement_start_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ENDORSEMENT_START' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="endorsement_start_c"  >

{if !$fields.endorsement_start_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.endorsement_start_c.value) <= 0}
{assign var="value" value=$fields.endorsement_start_c.default_value }
{else}
{assign var="value" value=$fields.endorsement_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.endorsement_start_c.name}">{$fields.endorsement_start_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.endorsement_end_c.acl > 1 || $fields.endorsement_end_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ENDORSEMENT_END' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="endorsement_end_c"  >

{if !$fields.endorsement_end_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.endorsement_end_c.value) <= 0}
{assign var="value" value=$fields.endorsement_end_c.default_value }
{else}
{assign var="value" value=$fields.endorsement_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.endorsement_end_c.name}">{$fields.endorsement_end_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-4" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL11' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-4"  data-id="LBL_DETAILVIEW_PANEL11">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.document_process_c.acl > 1 || $fields.document_process_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DOCUMENT_PROCESS' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="document_process_c"  >

{if !$fields.document_process_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.document_process_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.document_process_c.name}" value="{ $fields.document_process_c.options }">
{ $fields.document_process_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.document_process_c.name}" value="{ $fields.document_process_c.value }">
{ $fields.document_process_c.options[$fields.document_process_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.doc_complete_date_c.acl > 1 || $fields.doc_complete_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DOC_COMPLETE_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="doc_complete_date_c"  >

{if !$fields.doc_complete_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.doc_complete_date_c.value) <= 0}
{assign var="value" value=$fields.doc_complete_date_c.default_value }
{else}
{assign var="value" value=$fields.doc_complete_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.doc_complete_date_c.name}">{$fields.doc_complete_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-4" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL11' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-4" data-id="LBL_DETAILVIEW_PANEL11">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.document_process_c.acl > 1 || $fields.document_process_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DOCUMENT_PROCESS' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="document_process_c"  >

{if !$fields.document_process_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.document_process_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.document_process_c.name}" value="{ $fields.document_process_c.options }">
{ $fields.document_process_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.document_process_c.name}" value="{ $fields.document_process_c.value }">
{ $fields.document_process_c.options[$fields.document_process_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.doc_complete_date_c.acl > 1 || $fields.doc_complete_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DOC_COMPLETE_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="doc_complete_date_c"  >

{if !$fields.doc_complete_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.doc_complete_date_c.value) <= 0}
{assign var="value" value=$fields.doc_complete_date_c.default_value }
{else}
{assign var="value" value=$fields.doc_complete_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.doc_complete_date_c.name}">{$fields.doc_complete_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-5" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL12' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-5"  data-id="LBL_DETAILVIEW_PANEL12">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.site_inspection_c.acl > 1 || $fields.site_inspection_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SITE_INSPECTION' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="site_inspection_c"  >

{if !$fields.site_inspection_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.site_inspection_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.site_inspection_c.name}" value="{ $fields.site_inspection_c.options }">
{ $fields.site_inspection_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.site_inspection_c.name}" value="{ $fields.site_inspection_c.value }">
{ $fields.site_inspection_c.options[$fields.site_inspection_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.inspection_start_c.acl > 1 || $fields.inspection_start_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INSPECTION_START' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="inspection_start_c"  >

{if !$fields.inspection_start_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.inspection_start_c.value) <= 0}
{assign var="value" value=$fields.inspection_start_c.default_value }
{else}
{assign var="value" value=$fields.inspection_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.inspection_start_c.name}">{$fields.inspection_start_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.inspection_end_c.acl > 1 || $fields.inspection_end_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INSPECTION_END' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="inspection_end_c"  >

{if !$fields.inspection_end_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.inspection_end_c.value) <= 0}
{assign var="value" value=$fields.inspection_end_c.default_value }
{else}
{assign var="value" value=$fields.inspection_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.inspection_end_c.name}">{$fields.inspection_end_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-5" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL12' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-5" data-id="LBL_DETAILVIEW_PANEL12">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.site_inspection_c.acl > 1 || $fields.site_inspection_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SITE_INSPECTION' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="site_inspection_c"  >

{if !$fields.site_inspection_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.site_inspection_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.site_inspection_c.name}" value="{ $fields.site_inspection_c.options }">
{ $fields.site_inspection_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.site_inspection_c.name}" value="{ $fields.site_inspection_c.value }">
{ $fields.site_inspection_c.options[$fields.site_inspection_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.inspection_start_c.acl > 1 || $fields.inspection_start_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INSPECTION_START' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="inspection_start_c"  >

{if !$fields.inspection_start_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.inspection_start_c.value) <= 0}
{assign var="value" value=$fields.inspection_start_c.default_value }
{else}
{assign var="value" value=$fields.inspection_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.inspection_start_c.name}">{$fields.inspection_start_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.inspection_end_c.acl > 1 || $fields.inspection_end_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_INSPECTION_END' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="inspection_end_c"  >

{if !$fields.inspection_end_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.inspection_end_c.value) <= 0}
{assign var="value" value=$fields.inspection_end_c.default_value }
{else}
{assign var="value" value=$fields.inspection_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.inspection_end_c.name}">{$fields.inspection_end_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-6" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL13' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-6"  data-id="LBL_DETAILVIEW_PANEL13">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.bid_presentation_c.acl > 1 || $fields.bid_presentation_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_BID_PRESENTATION' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="bid_presentation_c"  >

{if !$fields.bid_presentation_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.bid_presentation_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.bid_presentation_c.name}" value="{ $fields.bid_presentation_c.options }">
{ $fields.bid_presentation_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.bid_presentation_c.name}" value="{ $fields.bid_presentation_c.value }">
{ $fields.bid_presentation_c.options[$fields.bid_presentation_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.presentation_start_c.acl > 1 || $fields.presentation_start_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRESENTATION_START' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="presentation_start_c"  >

{if !$fields.presentation_start_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.presentation_start_c.value) <= 0}
{assign var="value" value=$fields.presentation_start_c.default_value }
{else}
{assign var="value" value=$fields.presentation_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.presentation_start_c.name}">{$fields.presentation_start_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.presentation_end_c.acl > 1 || $fields.presentation_end_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRESENTATION_END' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="presentation_end_c"  >

{if !$fields.presentation_end_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.presentation_end_c.value) <= 0}
{assign var="value" value=$fields.presentation_end_c.default_value }
{else}
{assign var="value" value=$fields.presentation_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.presentation_end_c.name}">{$fields.presentation_end_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-6" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL13' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-6" data-id="LBL_DETAILVIEW_PANEL13">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.bid_presentation_c.acl > 1 || $fields.bid_presentation_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_BID_PRESENTATION' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="bid_presentation_c"  >

{if !$fields.bid_presentation_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.bid_presentation_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.bid_presentation_c.name}" value="{ $fields.bid_presentation_c.options }">
{ $fields.bid_presentation_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.bid_presentation_c.name}" value="{ $fields.bid_presentation_c.value }">
{ $fields.bid_presentation_c.options[$fields.bid_presentation_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.presentation_start_c.acl > 1 || $fields.presentation_start_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRESENTATION_START' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="presentation_start_c"  >

{if !$fields.presentation_start_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.presentation_start_c.value) <= 0}
{assign var="value" value=$fields.presentation_start_c.default_value }
{else}
{assign var="value" value=$fields.presentation_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.presentation_start_c.name}">{$fields.presentation_start_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.presentation_end_c.acl > 1 || $fields.presentation_end_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PRESENTATION_END' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="presentation_end_c"  >

{if !$fields.presentation_end_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.presentation_end_c.value) <= 0}
{assign var="value" value=$fields.presentation_end_c.default_value }
{else}
{assign var="value" value=$fields.presentation_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.presentation_end_c.name}">{$fields.presentation_end_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                            </div>
</div>
</div>
{/if}





{if $config.enable_action_menu and $config.enable_action_menu != false}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-7" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL14' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-7"  data-id="LBL_DETAILVIEW_PANEL14">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.mou_c.acl > 1 || $fields.mou_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MOU' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="mou_c"  >

{if !$fields.mou_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.mou_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.mou_c.name}" value="{ $fields.mou_c.options }">
{ $fields.mou_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.mou_c.name}" value="{ $fields.mou_c.value }">
{ $fields.mou_c.options[$fields.mou_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.mou_start_c.acl > 1 || $fields.mou_start_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MOU_START' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="mou_start_c"  >

{if !$fields.mou_start_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.mou_start_c.value) <= 0}
{assign var="value" value=$fields.mou_start_c.default_value }
{else}
{assign var="value" value=$fields.mou_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.mou_start_c.name}">{$fields.mou_start_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.mou_end_c.acl > 1 || $fields.mou_end_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MOU_END' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="mou_end_c"  >

{if !$fields.mou_end_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.mou_end_c.value) <= 0}
{assign var="value" value=$fields.mou_end_c.default_value }
{else}
{assign var="value" value=$fields.mou_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.mou_end_c.name}">{$fields.mou_end_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                                </div>
</div>
</div>
{else}

<div class="panel panel-default">
<div class="panel-heading ">
<a class="" role="button" data-toggle="collapse" href="#top-panel-7" aria-expanded="false">
<div class="col-xs-10 col-sm-11 col-md-11">
{sugar_translate label='LBL_DETAILVIEW_PANEL14' module='Leads'}
</div>
</a>
</div>
<div class="panel-body panel-collapse collapse in panelContainer" id="top-panel-7" data-id="LBL_DETAILVIEW_PANEL14">
<div class="tab-content">
<!-- TAB CONTENT -->





<div class="row detail-view-row">


{if $fields.mou_c.acl > 1 || $fields.mou_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MOU' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="mou_c"  >

{if !$fields.mou_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.mou_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.mou_c.name}" value="{ $fields.mou_c.options }">
{ $fields.mou_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.mou_c.name}" value="{ $fields.mou_c.value }">
{ $fields.mou_c.options[$fields.mou_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>
</div>


<div class="row detail-view-row">


{if $fields.mou_start_c.acl > 1 || $fields.mou_start_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MOU_START' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="mou_start_c"  >

{if !$fields.mou_start_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.mou_start_c.value) <= 0}
{assign var="value" value=$fields.mou_start_c.default_value }
{else}
{assign var="value" value=$fields.mou_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.mou_start_c.name}">{$fields.mou_start_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.mou_end_c.acl > 1 || $fields.mou_end_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MOU_END' module='Leads'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="mou_end_c"  >

{if !$fields.mou_end_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.mou_end_c.value) <= 0}
{assign var="value" value=$fields.mou_end_c.default_value }
{else}
{assign var="value" value=$fields.mou_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.mou_end_c.name}">{$fields.mou_end_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                            </div>
</div>
</div>
{/if}






</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script>            <script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
{literal}
<script type="text/javascript">

                    var selectTabDetailView = function(tab) {
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').hide();
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').eq(tab).show().addClass('active').addClass('in');
                    };

                    var selectTabOnError = function(tab) {
                        selectTabDetailView(tab);
                        $('#content ul.nav.nav-tabs > li').removeClass('active');
                        $('#content ul.nav.nav-tabs > li a').css('color', '');

                        $('#content ul.nav.nav-tabs > li').eq(tab).find('a').first().css('color', 'red');
                        $('#content ul.nav.nav-tabs > li').eq(tab).addClass('active');

                    };

                    var selectTabOnErrorInputHandle = function(inputHandle) {
                        var tab = $(inputHandle).closest('.tab-pane-NOBOOTSTRAPTOGGLER').attr('id').match(/^detailpanel_(.*)$/)[1];
                        selectTabOnError(tab);
                    };


                    $(function(){
                        $('#content ul.nav.nav-tabs > li > a[data-toggle="tab"]').click(function(e){
                            if(typeof $(this).parent().find('a').first().attr('id') != 'undefined') {
                                var tab = parseInt($(this).parent().find('a').first().attr('id').match(/^tab(.)*$/)[1]);
                                selectTabDetailView(tab);
                            }
                        });
                    });

                </script>
{/literal}
{else}

{assign var=preForm value="<table width='100%' border='1' cellspacing='0' cellpadding='0' class='converted_account'><tr><td><table width='100%'><tr><td>"}
{assign var=displayPreform value=false}
{if isset($bean->contact_id) && !empty($bean->contact_id)}
{assign var=displayPreform value=true}
{assign var=preForm value=$preForm|cat:$MOD.LBL_CONVERTED_CONTACT}
{assign var=preForm value=$preForm|cat:"&nbsp;<a href='index.php?module=Contacts&action=DetailView&record="}
{assign var=preForm value=$preForm|cat:$bean->contact_id}
{assign var=preForm value=$preForm|cat:"'>"}
{assign var=preForm value=$preForm|cat:$bean->contact_name}
{assign var=preForm value=$preForm|cat:"</a>"}
{/if}
{assign var=preForm value=$preForm|cat:"</td><td>"}
{if ($bean->converted=='1') && isset($bean->account_id) && !empty($bean->account_id)}
{assign var=displayPreform value=true}
{assign var=preForm value=$preForm|cat:$MOD.LBL_CONVERTED_ACCOUNT}
{assign var=preForm value=$preForm|cat:"&nbsp;<a href='index.php?module=Accounts&action=DetailView&record="}
{assign var=preForm value=$preForm|cat:$bean->account_id}
{assign var=preForm value=$preForm|cat:"'>"}
{assign var=preForm value=$preForm|cat:$bean->account_name}
{assign var=preForm value=$preForm|cat:"</a>"}
{/if}
{assign var=preForm value=$preForm|cat:"</td><td>"}
{if isset($bean->opportunity_id) && !empty($bean->opportunity_id)}
{assign var=displayPreform value=true}
{assign var=preForm value=$preForm|cat:$MOD.LBL_CONVERTED_OPP}
{assign var=preForm value=$preForm|cat:"&nbsp;<a href='index.php?module=Opportunities&action=DetailView&record="}
{assign var=preForm value=$preForm|cat:$bean->opportunity_id}
{assign var=preForm value=$preForm|cat:"'>"}
{assign var=preForm value=$preForm|cat:$bean->opportunity_name}
{assign var=preForm value=$preForm|cat:"</a>"}
{/if}
{assign var=preForm value=$preForm|cat:"</td></tr></table></td></tr></table>"}
{if $displayPreform}
{$preForm}
<br>
{/if}

<script language="javascript">
    {literal}
    SUGAR.util.doWhen(function () {
        return $("#contentTable").length == 0;
    }, SUGAR.themes.actionMenu);
    {/literal}
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="{$module}">
<input type="hidden" name="record" value="{$fields.id.value}">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="{$offset}">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
{if !$config.enable_action_menu}
<div class="buttons">
{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} 
{if $bean->aclAccess("edit")}<input title="{$APP.LBL_DUPLICATE_BUTTON_TITLE}" accessKey="{$APP.LBL_DUPLICATE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.isDuplicate.value=true; _form.action.value='EditView'; _form.return_id.value='{$id}';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Duplicate" value="{$APP.LBL_DUPLICATE_BUTTON_LABEL}" id="duplicate_button">{/if} 
{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='Leads'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} 
<input class="button hidden" id="send_confirm_opt_in_email" title="{$APP.LBL_SEND_CONFIRM_OPT_IN_EMAIL}" onclick="var _form = document.getElementById('formDetailView');_form.return_module.value='Leads'; _form.return_action.value='DetailView'; _form.return_id.value='{$fields.id.value}'; _form.action.value='sendConfirmOptInEmail'; _form.module.value='Leads'; _form.module_tab.value='Leads';_form.submit();" name="send_confirm_opt_in_email" disabled="1" type="button" value="{$APP.LBL_SEND_CONFIRM_OPT_IN_EMAIL}"/>
<input type="button" class="button" onClick="showPopup();" value="{$APP.LBL_PRINT_AS_PDF}"/>
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=Leads", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>                    {/if}
</form>
</div>
</td>
<td align="right" width="20%" class="buttons">{$ADMIN_EDIT}
</td>
</tr>
</table>{sugar_include include=$includes}
<div id="Leads_detailview_tabs"
class="yui-navset detailview_tabs"
>

<ul class="yui-nav">

<li><a id="tab0" href="javascript:void(0)"><em>{sugar_translate label='LBL_DETAILVIEW_PANEL5' module='Leads'}</em></a></li>









<li><a id="tab1" href="javascript:void(0)"><em>{sugar_translate label='LBL_EDITVIEW_PANEL1' module='Leads'}</em></a></li>

<li><a id="tab2" href="javascript:void(0)"><em>{sugar_translate label='LBL_DETAILVIEW_PANEL4' module='Leads'}</em></a></li>

<li><a id="tab3" href="javascript:void(0)"><em>{sugar_translate label='LBL_PANEL_ASSIGNMENT' module='Leads'}</em></a></li>
</ul>
<div class="yui-content">
<div id='tabcontent0'>
<div id='detailpanel_1' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL5' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.status.acl > 1 || $fields.status.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.status.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STATUS' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="status" width='37.5%'  >
{if !$fields.status.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.status.options)}
<input type="hidden" class="sugar_field" id="{$fields.status.name}" value="{ $fields.status.options }">
{ $fields.status.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.status.name}" value="{ $fields.status.value }">
{ $fields.status.options[$fields.status.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.reason_c.acl > 1 || $fields.reason_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.reason_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_REASON' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="text" field="reason_c" width='37.5%'  >
{if !$fields.reason_c.hidden}
{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.reason_c.name|escape:'html'|url2html|nl2br}">{$fields.reason_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>


{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.start_engagement_c.acl > 1 || $fields.start_engagement_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.start_engagement_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_START_ENGAGEMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="start_engagement_c" width='37.5%'  >
{if !$fields.start_engagement_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.start_engagement_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_engagement_c.name}" value="{ $fields.start_engagement_c.options }">
{ $fields.start_engagement_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_engagement_c.name}" value="{ $fields.start_engagement_c.value }">
{ $fields.start_engagement_c.options[$fields.start_engagement_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.start_event_approval_c.acl > 1 || $fields.start_event_approval_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.start_event_approval_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_START_EVENT_APPROVAL' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="start_event_approval_c" width='37.5%'  >
{if !$fields.start_event_approval_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.start_event_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_event_approval_c.name}" value="{ $fields.start_event_approval_c.options }">
{ $fields.start_event_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_event_approval_c.name}" value="{ $fields.start_event_approval_c.value }">
{ $fields.start_event_approval_c.options[$fields.start_event_approval_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.start_eoi_c.acl > 1 || $fields.start_eoi_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.start_eoi_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_START_EOI' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="start_eoi_c" width='37.5%'  >
{if !$fields.start_eoi_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.start_eoi_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_eoi_c.name}" value="{ $fields.start_eoi_c.options }">
{ $fields.start_eoi_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_eoi_c.name}" value="{ $fields.start_eoi_c.value }">
{ $fields.start_eoi_c.options[$fields.start_eoi_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.start_endorsement_c.acl > 1 || $fields.start_endorsement_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.start_endorsement_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_START_ENDORSEMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="start_endorsement_c" width='37.5%'  >
{if !$fields.start_endorsement_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.start_endorsement_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_endorsement_c.name}" value="{ $fields.start_endorsement_c.options }">
{ $fields.start_endorsement_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_endorsement_c.name}" value="{ $fields.start_endorsement_c.value }">
{ $fields.start_endorsement_c.options[$fields.start_endorsement_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.start_event_docs_c.acl > 1 || $fields.start_event_docs_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.start_event_docs_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_START_EVENT_DOCS' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="start_event_docs_c" width='37.5%'  >
{if !$fields.start_event_docs_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.start_event_docs_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_event_docs_c.name}" value="{ $fields.start_event_docs_c.options }">
{ $fields.start_event_docs_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_event_docs_c.name}" value="{ $fields.start_event_docs_c.value }">
{ $fields.start_event_docs_c.options[$fields.start_event_docs_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.start_inspection_c.acl > 1 || $fields.start_inspection_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.start_inspection_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_START_INSPECTION' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="start_inspection_c" width='37.5%'  >
{if !$fields.start_inspection_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.start_inspection_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_inspection_c.name}" value="{ $fields.start_inspection_c.options }">
{ $fields.start_inspection_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_inspection_c.name}" value="{ $fields.start_inspection_c.value }">
{ $fields.start_inspection_c.options[$fields.start_inspection_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.start_presentation_c.acl > 1 || $fields.start_presentation_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.start_presentation_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_START_PRESENTATION' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="start_presentation_c" width='37.5%'  >
{if !$fields.start_presentation_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.start_presentation_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_presentation_c.name}" value="{ $fields.start_presentation_c.options }">
{ $fields.start_presentation_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_presentation_c.name}" value="{ $fields.start_presentation_c.value }">
{ $fields.start_presentation_c.options[$fields.start_presentation_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.start_mou_c.acl > 1 || $fields.start_mou_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.start_mou_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_START_MOU' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="start_mou_c" width='37.5%'  >
{if !$fields.start_mou_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.start_mou_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.start_mou_c.name}" value="{ $fields.start_mou_c.options }">
{ $fields.start_mou_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.start_mou_c.name}" value="{ $fields.start_mou_c.value }">
{ $fields.start_mou_c.options[$fields.start_mou_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL5").style.display='none';</script>
{/if}
<div id='detailpanel_2' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(2);">
<img border="0" id="detailpanel_2_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(2);">
<img border="0" id="detailpanel_2_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
{sugar_translate label='LBL_DETAILVIEW_PANEL7' module='Leads'}
<script>
      document.getElementById('detailpanel_2').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL7' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.initial_email_c.acl > 1 || $fields.initial_email_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.initial_email_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_INITIAL_EMAIL' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="initial_email_c" width='37.5%'  >
{if !$fields.initial_email_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.initial_email_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.initial_email_c.name}" value="{ $fields.initial_email_c.options }">
{ $fields.initial_email_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.initial_email_c.name}" value="{ $fields.initial_email_c.value }">
{ $fields.initial_email_c.options[$fields.initial_email_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.engaged_by_c.acl > 1 || $fields.engaged_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.engaged_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_ENGAGED_BY' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="engaged_by_c" width='37.5%'  >
{if !$fields.engaged_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id1_c" class="sugar_field" data-id-value="{$fields.user_id1_c.value}">{$fields.engaged_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.sent_date_c.acl > 1 || $fields.sent_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.sent_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_SENT_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="sent_date_c" width='37.5%'  >
{if !$fields.sent_date_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.sent_date_c.value) <= 0}
{assign var="value" value=$fields.sent_date_c.default_value }
{else}
{assign var="value" value=$fields.sent_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.sent_date_c.name}">{$fields.sent_date_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.responded_date_c.acl > 1 || $fields.responded_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.responded_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_RESPONDED_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="responded_date_c" width='37.5%'  >
{if !$fields.responded_date_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.responded_date_c.value) <= 0}
{assign var="value" value=$fields.responded_date_c.default_value }
{else}
{assign var="value" value=$fields.responded_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.responded_date_c.name}">{$fields.responded_date_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(2, 'expanded'); {rdelim}); </script>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL7").style.display='none';</script>
{/if}
<div id='detailpanel_3' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(3);">
<img border="0" id="detailpanel_3_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(3);">
<img border="0" id="detailpanel_3_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
{sugar_translate label='LBL_DETAILVIEW_PANEL8' module='Leads'}
<script>
      document.getElementById('detailpanel_3').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL8' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.initial_event_approval_c.acl > 1 || $fields.initial_event_approval_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.initial_event_approval_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_INITIAL_EVENT_APPROVAL' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="initial_event_approval_c" width='37.5%'  >
{if !$fields.initial_event_approval_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.initial_event_approval_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.initial_event_approval_c.name}" value="{ $fields.initial_event_approval_c.options }">
{ $fields.initial_event_approval_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.initial_event_approval_c.name}" value="{ $fields.initial_event_approval_c.value }">
{ $fields.initial_event_approval_c.options[$fields.initial_event_approval_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.iea_start_date_c.acl > 1 || $fields.iea_start_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.iea_start_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_IEA_START_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="iea_start_date_c" width='37.5%'  >
{if !$fields.iea_start_date_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.iea_start_date_c.value) <= 0}
{assign var="value" value=$fields.iea_start_date_c.default_value }
{else}
{assign var="value" value=$fields.iea_start_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.iea_start_date_c.name}">{$fields.iea_start_date_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.iea_end_date_c.acl > 1 || $fields.iea_end_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.iea_end_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_IEA_END_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="iea_end_date_c" width='37.5%'  >
{if !$fields.iea_end_date_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.iea_end_date_c.value) <= 0}
{assign var="value" value=$fields.iea_end_date_c.default_value }
{else}
{assign var="value" value=$fields.iea_end_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.iea_end_date_c.name}">{$fields.iea_end_date_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(3, 'expanded'); {rdelim}); </script>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL8").style.display='none';</script>
{/if}
<div id='detailpanel_4' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(4);">
<img border="0" id="detailpanel_4_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(4);">
<img border="0" id="detailpanel_4_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
{sugar_translate label='LBL_DETAILVIEW_PANEL9' module='Leads'}
<script>
      document.getElementById('detailpanel_4').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL9' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.eoi_c.acl > 1 || $fields.eoi_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.eoi_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EOI' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="eoi_c" width='37.5%'  >
{if !$fields.eoi_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.eoi_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.eoi_c.name}" value="{ $fields.eoi_c.options }">
{ $fields.eoi_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.eoi_c.name}" value="{ $fields.eoi_c.value }">
{ $fields.eoi_c.options[$fields.eoi_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.eoi_approval_start_c.acl > 1 || $fields.eoi_approval_start_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.eoi_approval_start_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EOI_APPROVAL_START' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="eoi_approval_start_c" width='37.5%'  >
{if !$fields.eoi_approval_start_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.eoi_approval_start_c.value) <= 0}
{assign var="value" value=$fields.eoi_approval_start_c.default_value }
{else}
{assign var="value" value=$fields.eoi_approval_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.eoi_approval_start_c.name}">{$fields.eoi_approval_start_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.eoi_approval_end_c.acl > 1 || $fields.eoi_approval_end_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.eoi_approval_end_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EOI_APPROVAL_END' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="eoi_approval_end_c" width='37.5%'  >
{if !$fields.eoi_approval_end_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.eoi_approval_end_c.value) <= 0}
{assign var="value" value=$fields.eoi_approval_end_c.default_value }
{else}
{assign var="value" value=$fields.eoi_approval_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.eoi_approval_end_c.name}">{$fields.eoi_approval_end_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(4, 'expanded'); {rdelim}); </script>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL9").style.display='none';</script>
{/if}
<div id='detailpanel_5' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(5);">
<img border="0" id="detailpanel_5_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(5);">
<img border="0" id="detailpanel_5_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
{sugar_translate label='LBL_DETAILVIEW_PANEL10' module='Leads'}
<script>
      document.getElementById('detailpanel_5').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL10' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.endorsement_c.acl > 1 || $fields.endorsement_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.endorsement_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_ENDORSEMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="endorsement_c" width='37.5%'  >
{if !$fields.endorsement_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.endorsement_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.endorsement_c.name}" value="{ $fields.endorsement_c.options }">
{ $fields.endorsement_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.endorsement_c.name}" value="{ $fields.endorsement_c.value }">
{ $fields.endorsement_c.options[$fields.endorsement_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.endorsement_start_c.acl > 1 || $fields.endorsement_start_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.endorsement_start_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_ENDORSEMENT_START' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="endorsement_start_c" width='37.5%'  >
{if !$fields.endorsement_start_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.endorsement_start_c.value) <= 0}
{assign var="value" value=$fields.endorsement_start_c.default_value }
{else}
{assign var="value" value=$fields.endorsement_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.endorsement_start_c.name}">{$fields.endorsement_start_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.endorsement_end_c.acl > 1 || $fields.endorsement_end_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.endorsement_end_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_ENDORSEMENT_END' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="endorsement_end_c" width='37.5%'  >
{if !$fields.endorsement_end_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.endorsement_end_c.value) <= 0}
{assign var="value" value=$fields.endorsement_end_c.default_value }
{else}
{assign var="value" value=$fields.endorsement_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.endorsement_end_c.name}">{$fields.endorsement_end_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(5, 'expanded'); {rdelim}); </script>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL10").style.display='none';</script>
{/if}
<div id='detailpanel_6' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(6);">
<img border="0" id="detailpanel_6_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(6);">
<img border="0" id="detailpanel_6_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
{sugar_translate label='LBL_DETAILVIEW_PANEL11' module='Leads'}
<script>
      document.getElementById('detailpanel_6').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL11' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.document_process_c.acl > 1 || $fields.document_process_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.document_process_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DOCUMENT_PROCESS' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="document_process_c" width='37.5%'  >
{if !$fields.document_process_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.document_process_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.document_process_c.name}" value="{ $fields.document_process_c.options }">
{ $fields.document_process_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.document_process_c.name}" value="{ $fields.document_process_c.value }">
{ $fields.document_process_c.options[$fields.document_process_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.doc_complete_date_c.acl > 1 || $fields.doc_complete_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.doc_complete_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DOC_COMPLETE_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="doc_complete_date_c" width='37.5%'  >
{if !$fields.doc_complete_date_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.doc_complete_date_c.value) <= 0}
{assign var="value" value=$fields.doc_complete_date_c.default_value }
{else}
{assign var="value" value=$fields.doc_complete_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.doc_complete_date_c.name}">{$fields.doc_complete_date_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(6, 'expanded'); {rdelim}); </script>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL11").style.display='none';</script>
{/if}
<div id='detailpanel_7' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(7);">
<img border="0" id="detailpanel_7_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(7);">
<img border="0" id="detailpanel_7_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
{sugar_translate label='LBL_DETAILVIEW_PANEL12' module='Leads'}
<script>
      document.getElementById('detailpanel_7').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL12' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.site_inspection_c.acl > 1 || $fields.site_inspection_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.site_inspection_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_SITE_INSPECTION' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="site_inspection_c" width='37.5%'  >
{if !$fields.site_inspection_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.site_inspection_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.site_inspection_c.name}" value="{ $fields.site_inspection_c.options }">
{ $fields.site_inspection_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.site_inspection_c.name}" value="{ $fields.site_inspection_c.value }">
{ $fields.site_inspection_c.options[$fields.site_inspection_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.inspection_start_c.acl > 1 || $fields.inspection_start_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.inspection_start_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_INSPECTION_START' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="inspection_start_c" width='37.5%'  >
{if !$fields.inspection_start_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.inspection_start_c.value) <= 0}
{assign var="value" value=$fields.inspection_start_c.default_value }
{else}
{assign var="value" value=$fields.inspection_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.inspection_start_c.name}">{$fields.inspection_start_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.inspection_end_c.acl > 1 || $fields.inspection_end_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.inspection_end_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_INSPECTION_END' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="inspection_end_c" width='37.5%'  >
{if !$fields.inspection_end_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.inspection_end_c.value) <= 0}
{assign var="value" value=$fields.inspection_end_c.default_value }
{else}
{assign var="value" value=$fields.inspection_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.inspection_end_c.name}">{$fields.inspection_end_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(7, 'expanded'); {rdelim}); </script>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL12").style.display='none';</script>
{/if}
<div id='detailpanel_8' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(8);">
<img border="0" id="detailpanel_8_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(8);">
<img border="0" id="detailpanel_8_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
{sugar_translate label='LBL_DETAILVIEW_PANEL13' module='Leads'}
<script>
      document.getElementById('detailpanel_8').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL13' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.bid_presentation_c.acl > 1 || $fields.bid_presentation_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.bid_presentation_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_BID_PRESENTATION' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="bid_presentation_c" width='37.5%'  >
{if !$fields.bid_presentation_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.bid_presentation_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.bid_presentation_c.name}" value="{ $fields.bid_presentation_c.options }">
{ $fields.bid_presentation_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.bid_presentation_c.name}" value="{ $fields.bid_presentation_c.value }">
{ $fields.bid_presentation_c.options[$fields.bid_presentation_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.presentation_start_c.acl > 1 || $fields.presentation_start_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.presentation_start_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_PRESENTATION_START' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="presentation_start_c" width='37.5%'  >
{if !$fields.presentation_start_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.presentation_start_c.value) <= 0}
{assign var="value" value=$fields.presentation_start_c.default_value }
{else}
{assign var="value" value=$fields.presentation_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.presentation_start_c.name}">{$fields.presentation_start_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.presentation_end_c.acl > 1 || $fields.presentation_end_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.presentation_end_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_PRESENTATION_END' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="presentation_end_c" width='37.5%'  >
{if !$fields.presentation_end_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.presentation_end_c.value) <= 0}
{assign var="value" value=$fields.presentation_end_c.default_value }
{else}
{assign var="value" value=$fields.presentation_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.presentation_end_c.name}">{$fields.presentation_end_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(8, 'expanded'); {rdelim}); </script>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL13").style.display='none';</script>
{/if}
<div id='detailpanel_9' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<h4>
<a href="javascript:void(0)" class="collapseLink" onclick="collapsePanel(9);">
<img border="0" id="detailpanel_9_img_hide" src="{sugar_getimagepath file="basic_search.gif"}"></a>
<a href="javascript:void(0)" class="expandLink" onclick="expandPanel(9);">
<img border="0" id="detailpanel_9_img_show" src="{sugar_getimagepath file="advanced_search.gif"}"></a>
{sugar_translate label='LBL_DETAILVIEW_PANEL14' module='Leads'}
<script>
      document.getElementById('detailpanel_9').className += ' expanded';
    </script>
</h4>
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL14' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.mou_c.acl > 1 || $fields.mou_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.mou_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MOU' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="mou_c" width='37.5%'  >
{if !$fields.mou_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.mou_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.mou_c.name}" value="{ $fields.mou_c.options }">
{ $fields.mou_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.mou_c.name}" value="{ $fields.mou_c.value }">
{ $fields.mou_c.options[$fields.mou_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.mou_start_c.acl > 1 || $fields.mou_start_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.mou_start_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MOU_START' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="mou_start_c" width='37.5%'  >
{if !$fields.mou_start_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.mou_start_c.value) <= 0}
{assign var="value" value=$fields.mou_start_c.default_value }
{else}
{assign var="value" value=$fields.mou_start_c.value }
{/if} 
<span class="sugar_field" id="{$fields.mou_start_c.name}">{$fields.mou_start_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.mou_end_c.acl > 1 || $fields.mou_end_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.mou_end_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MOU_END' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="mou_end_c" width='37.5%'  >
{if !$fields.mou_end_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.mou_end_c.value) <= 0}
{assign var="value" value=$fields.mou_end_c.default_value }
{else}
{assign var="value" value=$fields.mou_end_c.value }
{/if} 
<span class="sugar_field" id="{$fields.mou_end_c.name}">{$fields.mou_end_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
<script type="text/javascript">SUGAR.util.doWhen("typeof initPanel == 'function'", function() {ldelim} initPanel(9, 'expanded'); {rdelim}); </script>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL14").style.display='none';</script>
{/if}
</div>    <div id='tabcontent1'>
<div id='detailpanel_10' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_EDITVIEW_PANEL1' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.account_c.acl > 1 || $fields.account_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.account_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_ACCOUNT' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="account_c" width='37.5%'  >
{if !$fields.account_c.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.account_id_c.value)}
{capture assign="detail_url"}index.php?module=Accounts&action=DetailView&record={$fields.account_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="account_id_c" class="sugar_field" data-id-value="{$fields.account_id_c.value}">{$fields.account_c.value}</span>
{if !empty($fields.account_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.segment_c.acl > 1 || $fields.segment_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.segment_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_SEGMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="segment_c" width='37.5%'  >
{if !$fields.segment_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.segment_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.segment_c.name}" value="{ $fields.segment_c.options }">
{ $fields.segment_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.segment_c.name}" value="{ $fields.segment_c.value }">
{ $fields.segment_c.options[$fields.segment_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.event_title_c.acl > 1 || $fields.event_title_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_title_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_TITLE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="varchar" field="event_title_c" width='37.5%'  >
{if !$fields.event_title_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.event_title_c.value) <= 0}
{assign var="value" value=$fields.event_title_c.default_value }
{else}
{assign var="value" value=$fields.event_title_c.value }
{/if} 
<span class="sugar_field" id="{$fields.event_title_c.name}">{$fields.event_title_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.primary_contact_c.acl > 1 || $fields.primary_contact_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.primary_contact_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_PRIMARY_CONTACT' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="primary_contact_c" width='37.5%'  >
{if !$fields.primary_contact_c.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.contact_id_c.value)}
{capture assign="detail_url"}index.php?module=Contacts&action=DetailView&record={$fields.contact_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="contact_id_c" class="sugar_field" data-id-value="{$fields.contact_id_c.value}">{$fields.primary_contact_c.value}</span>
{if !empty($fields.contact_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.event_abbreviation_c.acl > 1 || $fields.event_abbreviation_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_abbreviation_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_ABBREVIATION' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="varchar" field="event_abbreviation_c" width='37.5%'  >
{if !$fields.event_abbreviation_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.event_abbreviation_c.value) <= 0}
{assign var="value" value=$fields.event_abbreviation_c.default_value }
{else}
{assign var="value" value=$fields.event_abbreviation_c.value }
{/if} 
<span class="sugar_field" id="{$fields.event_abbreviation_c.name}">{$fields.event_abbreviation_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.event_date_c.acl > 1 || $fields.event_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="date" field="event_date_c" width='37.5%'  >
{if !$fields.event_date_c.hidden}
{counter name="panelFieldCount"}


{if strlen($fields.event_date_c.value) <= 0}
{assign var="value" value=$fields.event_date_c.default_value }
{else}
{assign var="value" value=$fields.event_date_c.value }
{/if}
<span class="sugar_field" id="{$fields.event_date_c.name}">{$value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.event_end_date_c.acl > 1 || $fields.event_end_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_end_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_END_DATE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="date" field="event_end_date_c" width='37.5%'  >
{if !$fields.event_end_date_c.hidden}
{counter name="panelFieldCount"}


{if strlen($fields.event_end_date_c.value) <= 0}
{assign var="value" value=$fields.event_end_date_c.default_value }
{else}
{assign var="value" value=$fields.event_end_date_c.value }
{/if}
<span class="sugar_field" id="{$fields.event_end_date_c.name}">{$value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.packs_c.acl > 1 || $fields.packs_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.packs_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_PACKS' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="int" field="packs_c" width='37.5%'  >
{if !$fields.packs_c.hidden}
{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.packs_c.name}">
{assign var="value" value=$fields.packs_c.value }
{$value}
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.event_days_c.acl > 1 || $fields.event_days_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_days_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_DAYS' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="int" field="event_days_c" width='37.5%'  >
{if !$fields.event_days_c.hidden}
{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.event_days_c.name}">
{assign var="value" value=$fields.event_days_c.value }
{$value}
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>


{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>



{if $fields.currency_id.acl > 1 || $fields.currency_id.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.currency_id.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CURRENCY' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency_id" field="currency_id" width='37.5%'  >
{if !$fields.currency_id.hidden}
{counter name="panelFieldCount"}
<span id='currency_id_span'>
{$fields.currency_id.value}
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.expected_revenue_c.acl > 1 || $fields.expected_revenue_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.expected_revenue_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EXPECTED_REVENUE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="expected_revenue_c" width='37.5%'  >
{if !$fields.expected_revenue_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.expected_revenue_c.name}'>
{sugar_number_format var=$fields.expected_revenue_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.gor_commitment_c.acl > 1 || $fields.gor_commitment_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.gor_commitment_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_COMMITMENT' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="gor_commitment_c" width='37.5%'  >
{if !$fields.gor_commitment_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.gor_commitment_c.name}'>
{sugar_number_format var=$fields.gor_commitment_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>


{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.lead_source.acl > 1 || $fields.lead_source.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.lead_source.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_LEAD_SOURCE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="lead_source" width='37.5%'  >
{if !$fields.lead_source.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.lead_source.options)}
<input type="hidden" class="sugar_field" id="{$fields.lead_source.name}" value="{ $fields.lead_source.options }">
{ $fields.lead_source.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.lead_source.name}" value="{ $fields.lead_source.value }">
{ $fields.lead_source.options[$fields.lead_source.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.source_details_c.acl > 1 || $fields.source_details_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.source_details_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_SOURCE_DETAILS' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="dynamicenum" field="source_details_c" width='37.5%'  >
{if !$fields.source_details_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.source_details_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.source_details_c.name}" value="{ $fields.source_details_c.options }">
{ $fields.source_details_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.source_details_c.name}" value="{ $fields.source_details_c.value }">
{ $fields.source_details_c.options[$fields.source_details_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>


{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.probability_c.acl > 1 || $fields.probability_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.probability_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_PROBABILITY ' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="probability_c" width='37.5%'  >
{if !$fields.probability_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.probability_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.probability_c.name}" value="{ $fields.probability_c.options }">
{ $fields.probability_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.probability_c.name}" value="{ $fields.probability_c.value }">
{ $fields.probability_c.options[$fields.probability_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.event_coordinator_c.acl > 1 || $fields.event_coordinator_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_coordinator_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_COORDINATOR' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="event_coordinator_c" width='37.5%'  >
{if !$fields.event_coordinator_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id2_c" class="sugar_field" data-id-value="{$fields.user_id2_c.value}">{$fields.event_coordinator_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>


{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.line_institute_c.acl > 1 || $fields.line_institute_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.line_institute_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_LINE_INSTITUTE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="line_institute_c" width='37.5%'  >
{if !$fields.line_institute_c.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.lip_line_institutes_id_c.value)}
{capture assign="detail_url"}index.php?module=LIP_Line_Institutes&action=DetailView&record={$fields.lip_line_institutes_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="lip_line_institutes_id_c" class="sugar_field" data-id-value="{$fields.lip_line_institutes_id_c.value}">{$fields.line_institute_c.value}</span>
{if !empty($fields.lip_line_institutes_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.venue_c.acl > 1 || $fields.venue_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.venue_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_VENUE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="venue_c" width='37.5%'  >
{if !$fields.venue_c.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.vnp_venues_id_c.value)}
{capture assign="detail_url"}index.php?module=VNP_Venues&action=DetailView&record={$fields.vnp_venues_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="vnp_venues_id_c" class="sugar_field" data-id-value="{$fields.vnp_venues_id_c.value}">{$fields.venue_c.value}</span>
{if !empty($fields.vnp_venues_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>


{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>




{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.stp_sales_target_leads_1_name.acl > 1 || $fields.stp_sales_target_leads_1_name.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.stp_sales_target_leads_1_name.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STP_SALES_TARGET_LEADS_1_FROM_STP_SALES_TARGET_TITLE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="stp_sales_target_leads_1_name" width='37.5%'  >
{if !$fields.stp_sales_target_leads_1_name.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.stp_sales_target_leads_1stp_sales_target_ida.value)}
{capture assign="detail_url"}index.php?module=STP_Sales_Target&action=DetailView&record={$fields.stp_sales_target_leads_1stp_sales_target_ida.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="stp_sales_target_leads_1stp_sales_target_ida" class="sugar_field" data-id-value="{$fields.stp_sales_target_leads_1stp_sales_target_ida.value}">{$fields.stp_sales_target_leads_1_name.value}</span>
{if !empty($fields.stp_sales_target_leads_1stp_sales_target_ida.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_EDITVIEW_PANEL1").style.display='none';</script>
{/if}
</div>    <div id='tabcontent2'>
<div id='detailpanel_11' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL4' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.event_brief_c.acl > 1 || $fields.event_brief_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_brief_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_BRIEF' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="wysiwyg" field="event_brief_c" width='37.5%' colspan='3' >
{if !$fields.event_brief_c.hidden}
{counter name="panelFieldCount"}

<iframe
id="{$fields.event_brief_c.name}"
name="{$fields.event_brief_c.name}"
srcdoc="{$fields.event_brief_c.value}"
style="width:100%;height:500px"
></iframe>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL4").style.display='none';</script>
{/if}
</div>    <div id='tabcontent3'>
<div id='detailpanel_12' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_PANEL_ASSIGNMENT' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.lead_progress_c.acl > 1 || $fields.lead_progress_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.lead_progress_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_LEAD_PROGRESS' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="lead_progress_c" width='37.5%'  >
{if !$fields.lead_progress_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.lead_progress_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.lead_progress_c.name}" value="{ $fields.lead_progress_c.options }">
{ $fields.lead_progress_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.lead_progress_c.name}" value="{ $fields.lead_progress_c.value }">
{ $fields.lead_progress_c.options[$fields.lead_progress_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.assigned_user_name.acl > 1 || $fields.assigned_user_name.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.assigned_user_name.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_ASSIGNED_TO' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="assigned_user_name" width='37.5%'  >
{if !$fields.assigned_user_name.hidden}
{counter name="panelFieldCount"}

<span id="assigned_user_id" class="sugar_field" data-id-value="{$fields.assigned_user_id.value}">{$fields.assigned_user_name.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.day_spending_c.acl > 1 || $fields.day_spending_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.day_spending_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DAY_SPENDING' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="day_spending_c" width='37.5%'  >
{if !$fields.day_spending_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.day_spending_c.name}'>
{sugar_number_format var=$fields.day_spending_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.created_by_name.acl > 1 || $fields.created_by_name.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.created_by_name.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="relate" field="created_by_name" width='37.5%'  >
{if !$fields.created_by_name.hidden}
{counter name="panelFieldCount"}

<span id="created_by" class="sugar_field" data-id-value="{$fields.created_by.value}">{$fields.created_by_name.value}</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.modified_by_name.acl > 1 || $fields.modified_by_name.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.modified_by_name.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MODIFIED_NAME' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="relate" field="modified_by_name" width='37.5%'  >
{if !$fields.modified_by_name.hidden}
{counter name="panelFieldCount"}

<span id="modified_user_id" class="sugar_field" data-id-value="{$fields.modified_user_id.value}">{$fields.modified_by_name.value}</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.date_entered.acl > 1 || $fields.date_entered.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.date_entered.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_ENTERED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="datetime" field="date_entered" width='37.5%'  >
{if !$fields.date_entered.hidden}
{counter name="panelFieldCount"}
<span id="date_entered" class="sugar_field">{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.date_modified.acl > 1 || $fields.date_modified.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.date_modified.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_MODIFIED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="datetime" field="date_modified" width='37.5%'  >
{if !$fields.date_modified.hidden}
{counter name="panelFieldCount"}
<span id="date_modified" class="sugar_field">{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.client_reponse_details_c.acl > 1 || $fields.client_reponse_details_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.client_reponse_details_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CLIENT_REPONSE_DETAILS' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="text" field="client_reponse_details_c" width='37.5%'  >
{if !$fields.client_reponse_details_c.hidden}
{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.client_reponse_details_c.name|escape:'html'|url2html|nl2br}">{$fields.client_reponse_details_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.gor_revenue_c.acl > 1 || $fields.gor_revenue_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.gor_revenue_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_GOR_REVENUE' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="gor_revenue_c" width='37.5%'  >
{if !$fields.gor_revenue_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.gor_revenue_c.name}'>
{sugar_number_format var=$fields.gor_revenue_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.sales_target_c.acl > 1 || $fields.sales_target_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.sales_target_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_SALES_TARGET' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="currency" field="sales_target_c" width='37.5%'  >
{if !$fields.sales_target_c.hidden}
{counter name="panelFieldCount"}

<span id='{$fields.sales_target_c.name}'>
{sugar_number_format var=$fields.sales_target_c.value }
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.qualfication_c.acl > 1 || $fields.qualfication_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.qualfication_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_QUALFICATION' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="qualfication_c" width='37.5%'  >
{if !$fields.qualfication_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.qualfication_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.qualfication_c.name}" value="{ $fields.qualfication_c.options }">
{ $fields.qualfication_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.qualfication_c.name}" value="{ $fields.qualfication_c.value }">
{ $fields.qualfication_c.options[$fields.qualfication_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.step2_created_c.acl > 1 || $fields.step2_created_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.step2_created_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STEP2_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="step2_created_c" width='37.5%'  >
{if !$fields.step2_created_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.step2_created_c.value) == "1" || strval($fields.step2_created_c.value) == "yes" || strval($fields.step2_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step2_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step2_created_c.name}" id="{$fields.step2_created_c.name}" value="$fields.step2_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.step3_created_c.acl > 1 || $fields.step3_created_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.step3_created_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STEP3_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="step3_created_c" width='37.5%'  >
{if !$fields.step3_created_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.step3_created_c.value) == "1" || strval($fields.step3_created_c.value) == "yes" || strval($fields.step3_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step3_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step3_created_c.name}" id="{$fields.step3_created_c.name}" value="$fields.step3_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.step4_created_c.acl > 1 || $fields.step4_created_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.step4_created_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STEP4_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="step4_created_c" width='37.5%'  >
{if !$fields.step4_created_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.step4_created_c.value) == "1" || strval($fields.step4_created_c.value) == "yes" || strval($fields.step4_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step4_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step4_created_c.name}" id="{$fields.step4_created_c.name}" value="$fields.step4_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.step5_created_c.acl > 1 || $fields.step5_created_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.step5_created_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STEP5_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="step5_created_c" width='37.5%'  >
{if !$fields.step5_created_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.step5_created_c.value) == "1" || strval($fields.step5_created_c.value) == "yes" || strval($fields.step5_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step5_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step5_created_c.name}" id="{$fields.step5_created_c.name}" value="$fields.step5_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.step6_created_c.acl > 1 || $fields.step6_created_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.step6_created_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STEP6_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="step6_created_c" width='37.5%'  >
{if !$fields.step6_created_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.step6_created_c.value) == "1" || strval($fields.step6_created_c.value) == "yes" || strval($fields.step6_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step6_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step6_created_c.name}" id="{$fields.step6_created_c.name}" value="$fields.step6_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.step7_created_c.acl > 1 || $fields.step7_created_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.step7_created_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STEP7_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="step7_created_c" width='37.5%'  >
{if !$fields.step7_created_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.step7_created_c.value) == "1" || strval($fields.step7_created_c.value) == "yes" || strval($fields.step7_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step7_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step7_created_c.name}" id="{$fields.step7_created_c.name}" value="$fields.step7_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.step8_created_c.acl > 1 || $fields.step8_created_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.step8_created_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STEP8_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="step8_created_c" width='37.5%'  >
{if !$fields.step8_created_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.step8_created_c.value) == "1" || strval($fields.step8_created_c.value) == "yes" || strval($fields.step8_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step8_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step8_created_c.name}" id="{$fields.step8_created_c.name}" value="$fields.step8_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}



{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>


</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.step3_eoi_chk_c.acl > 1 || $fields.step3_eoi_chk_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.step3_eoi_chk_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STEP3_EOI_CHK' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="step3_eoi_chk_c" width='37.5%'  >
{if !$fields.step3_eoi_chk_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.step3_eoi_chk_c.value) == "1" || strval($fields.step3_eoi_chk_c.value) == "yes" || strval($fields.step3_eoi_chk_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step3_eoi_chk_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step3_eoi_chk_c.name}" id="{$fields.step3_eoi_chk_c.name}" value="$fields.step3_eoi_chk_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.step4_endo_chk_c.acl > 1 || $fields.step4_endo_chk_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.step4_endo_chk_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_STEP4_ENDO_CHK' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="step4_endo_chk_c" width='37.5%'  >
{if !$fields.step4_endo_chk_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.step4_endo_chk_c.value) == "1" || strval($fields.step4_endo_chk_c.value) == "yes" || strval($fields.step4_endo_chk_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.step4_endo_chk_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.step4_endo_chk_c.name}" id="{$fields.step4_endo_chk_c.name}" value="$fields.step4_endo_chk_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.project_created_c.acl > 1 || $fields.project_created_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.project_created_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_PROJECT_CREATED' module='Leads'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="bool" field="project_created_c" width='37.5%' colspan='3' >
{if !$fields.project_created_c.hidden}
{counter name="panelFieldCount"}

{if strval($fields.project_created_c.value) == "1" || strval($fields.project_created_c.value) == "yes" || strval($fields.project_created_c.value) == "on"} 
{assign var="checked" value="CHECKED"}
{assign var="hidden_val" value="1"}
{else}
{assign var="checked" value=""}
{assign var="hidden_val" value=""}
{/if}
{if $smarty.request.action =='EditView'}
<input type="hidden" name="{$fields.project_created_c.name}" value="{$hidden_val}">
{/if}
<input type="checkbox" class="checkbox" name="{$fields.project_created_c.name}" id="{$fields.project_created_c.name}" value="$fields.project_created_c.value" disabled="true" {$checked}>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_PANEL_ASSIGNMENT").style.display='none';</script>
{/if}
</div>
</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script><script type='text/javascript' src='{sugar_getjspath file='include/javascript/popup_helper.js'}'></script>
<script type="text/javascript" src="{sugar_getjspath file='cache/include/javascript/sugar_grp_yui_widgets.js'}"></script>
<script type="text/javascript">
var Leads_detailview_tabs = new YAHOO.widget.TabView("Leads_detailview_tabs");
Leads_detailview_tabs.selectTab(0);
</script>
<script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
{/if}