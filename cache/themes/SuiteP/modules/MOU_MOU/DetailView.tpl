
{php}
global $current_user,$sugar_config;
$this->_tpl_vars['current_user'] = $current_user->id;
$this->_tpl_vars['is_admin_user'] = $current_user->is_admin;
$this->_tpl_vars['current_theme'] = $current_user->getPreference('user_theme');
$this->_tpl_vars['suite_valid_version'] =  true;
if(preg_match('/(7\.7\.[0-9])/', $sugar_config['suitecrm_version']) || $sugar_config['suitecrm_version'] == '7.7'){
$this->_tpl_vars['suite_valid_version'] = false;
}
{/php}
{if $current_theme == 'SuiteP'}

<script language="javascript">
    {literal}
    SUGAR.util.doWhen(function () {
        return $("#contentTable").length == 0;
    }, SUGAR.themes.actionMenu);
    {/literal}
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="{$module}">
<input type="hidden" name="record" value="{$fields.id.value}">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="{$offset}">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
{if !$config.enable_action_menu}
<div class="buttons">
{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='MOU_MOU'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} 
{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='MOU_MOU'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} 
<input type="button" class="button" onClick="showPopup('pdf');" value="{$MOD.LBL_PRINT_AS_PDF}"/>
<input type="button" class="button" onClick="showPopup('emailpdf');" value="{$MOD.LBL_EMAIL_PDF}"/>
{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=MOU_MOU", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}
</div>                    {/if}
</form>
</div>
</td>
<td align="right" width="20%" class="buttons">{$ADMIN_EDIT}
</td>
</tr>
</table>
{sugar_include include=$includes}
<div class="detail-view">
{if $suite_valid_version}
<div class="mobile-pagination">{$PAGINATION}</div>
{/if}

<ul class="nav nav-tabs">


<li role="presentation" class="active">
<a id="tab0" data-toggle="tab" class="hidden-xs">
{sugar_translate label='LBL_DETAILVIEW_PANEL5' module='MOU_MOU'}
</a>


<a id="xstab0" href="#" class="visible-xs first-tab-xs dropdown-toggle" data-toggle="dropdown">
{sugar_translate label='LBL_DETAILVIEW_PANEL5' module='MOU_MOU'}
</a>
<ul id="first-tab-menu-xs" class="dropdown-menu">
<li role="presentation">
<a id="tab1" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-1');">
{sugar_translate label='LBL_DETAILVIEW_PANEL5' module='MOU_MOU'}
</a>
</li>
<li role="presentation">
<a id="tab2" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-2');">
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='MOU_MOU'}
</a>
</li>
<li role="presentation">
<a id="tab3" data-toggle="tab" onclick="changeFirstTab(this, 'tab-content-3');">
{sugar_translate label='LBL_EDITVIEW_PANEL4' module='MOU_MOU'}
</a>
</li>
</ul>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab1" data-toggle="tab">
{sugar_translate label='LBL_EDITVIEW_PANEL1' module='MOU_MOU'}
</a>
</li>


<li role="presentation" class="hidden-xs">
<a id="tab2" data-toggle="tab">
{sugar_translate label='LBL_EDITVIEW_PANEL4' module='MOU_MOU'}
</a>
</li>
{if $config.enable_action_menu and $config.enable_action_menu != false}
<li id="tab-actions" class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="#">ACTIONS<span class="suitepicon suitepicon-action-caret"></span></a>
<ul class="dropdown-menu">
<li>{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='MOU_MOU'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} </li>
<li>{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='MOU_MOU'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} </li>
<li><input type="button" class="button" onClick="showPopup('pdf');" value="{$MOD.LBL_PRINT_AS_PDF}"/></li>
<li><input type="button" class="button" onClick="showPopup('emailpdf');" value="{$MOD.LBL_EMAIL_PDF}"/></li>
<li>{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=MOU_MOU", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}</li>
</ul>        </li>
{if $suite_valid_version}
<li class="tab-inline-pagination">
{$PAGINATION}
</li>
{/if}
{/if}
</ul>
<div class="clearfix"></div>

<div class="tab-content">

<div class="tab-pane-NOBOOTSTRAPTOGGLER active fade in" id='tab-content-0'>





<div class="row detail-view-row">


{if $fields.approval_status_c.acl > 1 || $fields.approval_status_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_STATUS' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="enum" field="approval_status_c"  >

{if !$fields.approval_status_c.hidden}
{counter name="panelFieldCount" print=false}


{if is_string($fields.approval_status_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.approval_status_c.name}" value="{ $fields.approval_status_c.options }">
{ $fields.approval_status_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.approval_status_c.name}" value="{ $fields.approval_status_c.value }">
{ $fields.approval_status_c.options[$fields.approval_status_c.value]}
{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.prepared_by_c.acl > 1 || $fields.prepared_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_PREPARED_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="prepared_by_c"  >

{if !$fields.prepared_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id_c" class="sugar_field" data-id-value="{$fields.user_id_c.value}">{$fields.prepared_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">



<div class="col-xs-12 col-sm-6 detail-view-row-item">
</div>

<div class="clear"></div>


{if $fields.revision_c.acl > 1 || $fields.revision_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_REVISION' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="int" field="revision_c"  >

{if !$fields.revision_c.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.revision_c.name}">
{assign var="value" value=$fields.revision_c.value }
{$value}
</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.routine_label_c.acl > 1 || $fields.routine_label_c.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ROUTINE_LABEL' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="varchar" field="routine_label_c" colspan='3' >

{if !$fields.routine_label_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.routine_label_c.value) <= 0}
{assign var="value" value=$fields.routine_label_c.default_value }
{else}
{assign var="value" value=$fields.routine_label_c.value }
{/if} 
<span class="sugar_field" id="{$fields.routine_label_c.name}">{$fields.routine_label_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.legal_approval_by_c.acl > 1 || $fields.legal_approval_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_LEGAL_APPROVAL_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="legal_approval_by_c"  >

{if !$fields.legal_approval_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id7_c" class="sugar_field" data-id-value="{$fields.user_id7_c.value}">{$fields.legal_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.legal_response_date_c.acl > 1 || $fields.legal_response_date_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_LEGAL_RESPONSE_DATE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="legal_response_date_c"  >

{if !$fields.legal_response_date_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.legal_response_date_c.value) <= 0}
{assign var="value" value=$fields.legal_response_date_c.default_value }
{else}
{assign var="value" value=$fields.legal_response_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.legal_response_date_c.name}">{$fields.legal_response_date_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.ddm_approval_by_c.acl > 1 || $fields.ddm_approval_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_APPROVAL_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="ddm_approval_by_c"  >

{if !$fields.ddm_approval_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id5_c" class="sugar_field" data-id-value="{$fields.user_id5_c.value}">{$fields.ddm_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.ddm_response_c.acl > 1 || $fields.ddm_response_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_RESPONSE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="ddm_response_c"  >

{if !$fields.ddm_response_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.ddm_response_c.value) <= 0}
{assign var="value" value=$fields.ddm_response_c.default_value }
{else}
{assign var="value" value=$fields.ddm_response_c.value }
{/if} 
<span class="sugar_field" id="{$fields.ddm_response_c.name}">{$fields.ddm_response_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.ssd_approval_by_c.acl > 1 || $fields.ssd_approval_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SSD_APPROVAL_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="ssd_approval_by_c"  >

{if !$fields.ssd_approval_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id8_c" class="sugar_field" data-id-value="{$fields.user_id8_c.value}">{$fields.ssd_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.ssd_response_c.acl > 1 || $fields.ssd_response_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_SSD_RESPONSE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="ssd_response_c"  >

{if !$fields.ssd_response_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.ssd_response_c.value) <= 0}
{assign var="value" value=$fields.ssd_response_c.default_value }
{else}
{assign var="value" value=$fields.ssd_response_c.value }
{/if} 
<span class="sugar_field" id="{$fields.ssd_response_c.name}">{$fields.ssd_response_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.dec_apv_by_c.acl > 1 || $fields.dec_apv_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DEC_APV_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="dec_apv_by_c"  >

{if !$fields.dec_apv_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id11_c" class="sugar_field" data-id-value="{$fields.user_id11_c.value}">{$fields.dec_apv_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.dec_response_c.acl > 1 || $fields.dec_response_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DEC_RESPONSE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="dec_response_c"  >

{if !$fields.dec_response_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.dec_response_c.value) <= 0}
{assign var="value" value=$fields.dec_response_c.default_value }
{else}
{assign var="value" value=$fields.dec_response_c.value }
{/if} 
<span class="sugar_field" id="{$fields.dec_response_c.name}">{$fields.dec_response_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.dceo_apv_by_c.acl > 1 || $fields.dceo_apv_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DCEO_APV_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="dceo_apv_by_c"  >

{if !$fields.dceo_apv_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id4_c" class="sugar_field" data-id-value="{$fields.user_id4_c.value}">{$fields.dceo_apv_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.dceo_response_c.acl > 1 || $fields.dceo_response_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DCEO_RESPONSE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="dceo_response_c"  >

{if !$fields.dceo_response_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.dceo_response_c.value) <= 0}
{assign var="value" value=$fields.dceo_response_c.default_value }
{else}
{assign var="value" value=$fields.dceo_response_c.value }
{/if} 
<span class="sugar_field" id="{$fields.dceo_response_c.name}">{$fields.dceo_response_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.ceo_apv_by_c.acl > 1 || $fields.ceo_apv_by_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_APV_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="ceo_apv_by_c"  >

{if !$fields.ceo_apv_by_c.hidden}
{counter name="panelFieldCount" print=false}

<span id="user_id1_c" class="sugar_field" data-id-value="{$fields.user_id1_c.value}">{$fields.ceo_apv_by_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.ceo_response_c.acl > 1 || $fields.ceo_response_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_RESPONSE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="datetimecombo" field="ceo_response_c"  >

{if !$fields.ceo_response_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.ceo_response_c.value) <= 0}
{assign var="value" value=$fields.ceo_response_c.default_value }
{else}
{assign var="value" value=$fields.ceo_response_c.value }
{/if} 
<span class="sugar_field" id="{$fields.ceo_response_c.name}">{$fields.ceo_response_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-1'>





<div class="row detail-view-row">


{if $fields.mou_title_c.acl > 1 || $fields.mou_title_c.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MOU_TITLE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="varchar" field="mou_title_c" colspan='3' >

{if !$fields.mou_title_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.mou_title_c.value) <= 0}
{assign var="value" value=$fields.mou_title_c.default_value }
{else}
{assign var="value" value=$fields.mou_title_c.value }
{/if} 
<span class="sugar_field" id="{$fields.mou_title_c.name}">{$fields.mou_title_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.client.acl > 1 || $fields.client.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CLIENT' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="client"  >

{if !$fields.client.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.account_id_c.value)}
{capture assign="detail_url"}index.php?module=Accounts&action=DetailView&record={$fields.account_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="account_id_c" class="sugar_field" data-id-value="{$fields.account_id_c.value}">{$fields.client.value}</span>
{if !empty($fields.account_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.contact.acl > 1 || $fields.contact.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CONTACT' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="contact"  >

{if !$fields.contact.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.contact_id_c.value)}
{capture assign="detail_url"}index.php?module=Contacts&action=DetailView&record={$fields.contact_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="contact_id_c" class="sugar_field" data-id-value="{$fields.contact_id_c.value}">{$fields.contact.value}</span>
{if !empty($fields.contact_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.leads_mou_mou_1_name.acl > 1 || $fields.leads_mou_mou_1_name.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_LEADS_MOU_MOU_1_FROM_LEADS_TITLE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="relate" field="leads_mou_mou_1_name" colspan='3' >

{if !$fields.leads_mou_mou_1_name.hidden}
{counter name="panelFieldCount" print=false}

{if !empty($fields.leads_mou_mou_1leads_ida.value)}
{capture assign="detail_url"}index.php?module=Leads&action=DetailView&record={$fields.leads_mou_mou_1leads_ida.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="leads_mou_mou_1leads_ida" class="sugar_field" data-id-value="{$fields.leads_mou_mou_1leads_ida.value}">{$fields.leads_mou_mou_1_name.value}</span>
{if !empty($fields.leads_mou_mou_1leads_ida.value)}</a>{/if}
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.lawyer_notes_c.acl > 1 || $fields.lawyer_notes_c.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_LAWYER_NOTES' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field inlineEdit" type="text" field="lawyer_notes_c" colspan='3' >

{if !$fields.lawyer_notes_c.hidden}
{counter name="panelFieldCount" print=false}

<span class="sugar_field" id="{$fields.lawyer_notes_c.name|escape:'html'|url2html|nl2br}">{$fields.lawyer_notes_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.event_brief.acl > 1 || $fields.event_brief.acl > 0}

<div class="col-xs-12 col-sm-12 detail-view-row-item">


<div class="col-xs-12 col-sm-2 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_BRIEF ' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-10 detail-view-field " type="wysiwyg" field="event_brief" colspan='3' >

{if !$fields.event_brief.hidden}
{counter name="panelFieldCount" print=false}

<iframe
id="{$fields.event_brief.name}"
name="{$fields.event_brief.name}"
srcdoc="{$fields.event_brief.value}"
style="width:100%;height:500px"
></iframe>
{/if}

</div>


</div>

{/if}
</div>
                    </div>
<div class="tab-pane-NOBOOTSTRAPTOGGLER fade" id='tab-content-2'>





<div class="row detail-view-row">


{if $fields.assigned_user_name.acl > 1 || $fields.assigned_user_name.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_ASSIGNED_TO_NAME' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="relate" field="assigned_user_name"  >

{if !$fields.assigned_user_name.hidden}
{counter name="panelFieldCount" print=false}

<span id="assigned_user_id" class="sugar_field" data-id-value="{$fields.assigned_user_id.value}">{$fields.assigned_user_name.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}


{if $fields.lead_id_string_c.acl > 1 || $fields.lead_id_string_c.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_LEAD_ID_STRING' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field inlineEdit" type="varchar" field="lead_id_string_c"  >

{if !$fields.lead_id_string_c.hidden}
{counter name="panelFieldCount" print=false}

{if strlen($fields.lead_id_string_c.value) <= 0}
{assign var="value" value=$fields.lead_id_string_c.default_value }
{else}
{assign var="value" value=$fields.lead_id_string_c.value }
{/if} 
<span class="sugar_field" id="{$fields.lead_id_string_c.name}">{$fields.lead_id_string_c.value}</span>
{/if}

<div class="inlineEditIcon col-xs-hidden">
<span class="suitepicon suitepicon-action-edit"></span>
</div>
</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.date_entered.acl > 1 || $fields.date_entered.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_ENTERED' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="datetime" field="date_entered"  >

{if !$fields.date_entered.hidden}
{counter name="panelFieldCount" print=false}
<span id="date_entered" class="sugar_field">{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}</span>
{/if}

</div>


</div>

{/if}


{if $fields.date_modified.acl > 1 || $fields.date_modified.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_MODIFIED' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="datetime" field="date_modified"  >

{if !$fields.date_modified.hidden}
{counter name="panelFieldCount" print=false}
<span id="date_modified" class="sugar_field">{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}</span>
{/if}

</div>


</div>

{/if}
</div>


<div class="row detail-view-row">


{if $fields.created_by_name.acl > 1 || $fields.created_by_name.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-1-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_CREATED' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="relate" field="created_by_name"  >

{if !$fields.created_by_name.hidden}
{counter name="panelFieldCount" print=false}

<span id="created_by" class="sugar_field" data-id-value="{$fields.created_by.value}">{$fields.created_by_name.value}</span>
{/if}

</div>


</div>

{/if}


{if $fields.modified_by_name.acl > 1 || $fields.modified_by_name.acl > 0}

<div class="col-xs-12 col-sm-6 detail-view-row-item">


<div class="col-xs-12 col-sm-4 label col-2-label">


{capture name="label" assign="label"}{sugar_translate label='LBL_MODIFIED_NAME' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:
</div>


<div class="col-xs-12 col-sm-8 detail-view-field " type="relate" field="modified_by_name"  >

{if !$fields.modified_by_name.hidden}
{counter name="panelFieldCount" print=false}

<span id="modified_user_id" class="sugar_field" data-id-value="{$fields.modified_user_id.value}">{$fields.modified_by_name.value}</span>
{/if}

</div>


</div>

{/if}
</div>
                    </div>
</div>

<div class="panel-content">
<div>&nbsp;</div>






</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script>            <script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
{literal}
<script type="text/javascript">

                    var selectTabDetailView = function(tab) {
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').hide();
                        $('#content div.tab-content div.tab-pane-NOBOOTSTRAPTOGGLER').eq(tab).show().addClass('active').addClass('in');
                    };

                    var selectTabOnError = function(tab) {
                        selectTabDetailView(tab);
                        $('#content ul.nav.nav-tabs > li').removeClass('active');
                        $('#content ul.nav.nav-tabs > li a').css('color', '');

                        $('#content ul.nav.nav-tabs > li').eq(tab).find('a').first().css('color', 'red');
                        $('#content ul.nav.nav-tabs > li').eq(tab).addClass('active');

                    };

                    var selectTabOnErrorInputHandle = function(inputHandle) {
                        var tab = $(inputHandle).closest('.tab-pane-NOBOOTSTRAPTOGGLER').attr('id').match(/^detailpanel_(.*)$/)[1];
                        selectTabOnError(tab);
                    };


                    $(function(){
                        $('#content ul.nav.nav-tabs > li > a[data-toggle="tab"]').click(function(e){
                            if(typeof $(this).parent().find('a').first().attr('id') != 'undefined') {
                                var tab = parseInt($(this).parent().find('a').first().attr('id').match(/^tab(.)*$/)[1]);
                                selectTabDetailView(tab);
                            }
                        });
                    });

                </script>
{/literal}
{else}

<script language="javascript">
{literal}
SUGAR.util.doWhen(function(){
    return $("#contentTable").length == 0;
}, SUGAR.themes.actionMenu);
{/literal}
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%" id="">
<tr>
<td class="buttons" align="left" NOWRAP width="80%">
<div class="actionsContainer">
<form action="index.php" method="post" name="DetailView" id="formDetailView">
<input type="hidden" name="module" value="{$module}">
<input type="hidden" name="record" value="{$fields.id.value}">
<input type="hidden" name="return_action">
<input type="hidden" name="return_module">
<input type="hidden" name="return_id">
<input type="hidden" name="module_tab">
<input type="hidden" name="isDuplicate" value="false">
<input type="hidden" name="offset" value="{$offset}">
<input type="hidden" name="action" value="EditView">
<input type="hidden" name="sugar_body_only">
</form>
<ul id="detail_header_action_menu" class="clickMenu fancymenu" ><li class="sugar_action_button" >{if $bean->aclAccess("edit")}<input title="{$APP.LBL_EDIT_BUTTON_TITLE}" accessKey="{$APP.LBL_EDIT_BUTTON_KEY}" class="button primary" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='MOU_MOU'; _form.return_action.value='DetailView'; _form.return_id.value='{$id}'; _form.action.value='EditView';SUGAR.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="{$APP.LBL_EDIT_BUTTON_LABEL}">{/if} <ul id class="subnav" ><li>{if $bean->aclAccess("delete")}<input title="{$APP.LBL_DELETE_BUTTON_TITLE}" accessKey="{$APP.LBL_DELETE_BUTTON_KEY}" class="button" onclick="var _form = document.getElementById('formDetailView'); _form.return_module.value='MOU_MOU'; _form.return_action.value='ListView'; _form.action.value='Delete'; if(confirm('{$APP.NTC_DELETE_CONFIRMATION}')) SUGAR.ajaxUI.submitForm(_form); return false;" type="submit" name="Delete" value="{$APP.LBL_DELETE_BUTTON_LABEL}" id="delete_button">{/if} </li><li><input type="button" class="button" onClick="showPopup('pdf');" value="{$MOD.LBL_PRINT_AS_PDF}"/></li><li><input type="button" class="button" onClick="showPopup('emailpdf');" value="{$MOD.LBL_EMAIL_PDF}"/></li><li>{if $bean->aclAccess("detail")}{if !empty($fields.id.value) && $isAuditEnabled}<input id="btn_view_change_log" title="{$APP.LNK_VIEW_CHANGE_LOG}" class="button" onclick='open_popup("Audit", "600", "400", "&record={$fields.id.value}&module_name=MOU_MOU", true, false,  {ldelim} "call_back_function":"set_return","form_name":"EditView","field_to_name_array":[] {rdelim} ); return false;' type="button" value="{$APP.LNK_VIEW_CHANGE_LOG}">{/if}{/if}</li></ul></li></ul>
</div>
</td>
<td align="right" width="20%">{$ADMIN_EDIT}
</td>
</tr>
</table>{sugar_include include=$includes}
<div id="MOU_MOU_detailview_tabs"
class="yui-navset detailview_tabs"
>

<ul class="yui-nav">

<li><a id="tab0" href="javascript:void(0)"><em>{sugar_translate label='LBL_DETAILVIEW_PANEL5' module='MOU_MOU'}</em></a></li>

<li><a id="tab1" href="javascript:void(0)"><em>{sugar_translate label='LBL_EDITVIEW_PANEL1' module='MOU_MOU'}</em></a></li>

<li><a id="tab2" href="javascript:void(0)"><em>{sugar_translate label='LBL_EDITVIEW_PANEL4' module='MOU_MOU'}</em></a></li>
</ul>
<div class="yui-content">
<div id='tabcontent0'>
<div id='detailpanel_1' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_DETAILVIEW_PANEL5' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.approval_status_c.acl > 1 || $fields.approval_status_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.approval_status_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_APPROVAL_STATUS' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="enum" field="approval_status_c" width='37.5%'  >
{if !$fields.approval_status_c.hidden}
{counter name="panelFieldCount"}


{if is_string($fields.approval_status_c.options)}
<input type="hidden" class="sugar_field" id="{$fields.approval_status_c.name}" value="{ $fields.approval_status_c.options }">
{ $fields.approval_status_c.options }
{else}
<input type="hidden" class="sugar_field" id="{$fields.approval_status_c.name}" value="{ $fields.approval_status_c.value }">
{ $fields.approval_status_c.options[$fields.approval_status_c.value]}
{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.prepared_by_c.acl > 1 || $fields.prepared_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.prepared_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_PREPARED_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="prepared_by_c" width='37.5%'  >
{if !$fields.prepared_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id_c" class="sugar_field" data-id-value="{$fields.user_id_c.value}">{$fields.prepared_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>


{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
&nbsp;

</td>

<td class="" type="" field="" width='37.5%'  >

</td>



{if $fields.revision_c.acl > 1 || $fields.revision_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.revision_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_REVISION' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="int" field="revision_c" width='37.5%'  >
{if !$fields.revision_c.hidden}
{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.revision_c.name}">
{assign var="value" value=$fields.revision_c.value }
{$value}
</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.routine_label_c.acl > 1 || $fields.routine_label_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.routine_label_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_ROUTINE_LABEL' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="varchar" field="routine_label_c" width='37.5%' colspan='3' >
{if !$fields.routine_label_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.routine_label_c.value) <= 0}
{assign var="value" value=$fields.routine_label_c.default_value }
{else}
{assign var="value" value=$fields.routine_label_c.value }
{/if} 
<span class="sugar_field" id="{$fields.routine_label_c.name}">{$fields.routine_label_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.legal_approval_by_c.acl > 1 || $fields.legal_approval_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.legal_approval_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_LEGAL_APPROVAL_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="legal_approval_by_c" width='37.5%'  >
{if !$fields.legal_approval_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id7_c" class="sugar_field" data-id-value="{$fields.user_id7_c.value}">{$fields.legal_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.legal_response_date_c.acl > 1 || $fields.legal_response_date_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.legal_response_date_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_LEGAL_RESPONSE_DATE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="legal_response_date_c" width='37.5%'  >
{if !$fields.legal_response_date_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.legal_response_date_c.value) <= 0}
{assign var="value" value=$fields.legal_response_date_c.default_value }
{else}
{assign var="value" value=$fields.legal_response_date_c.value }
{/if} 
<span class="sugar_field" id="{$fields.legal_response_date_c.name}">{$fields.legal_response_date_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.ddm_approval_by_c.acl > 1 || $fields.ddm_approval_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.ddm_approval_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_APPROVAL_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="ddm_approval_by_c" width='37.5%'  >
{if !$fields.ddm_approval_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id5_c" class="sugar_field" data-id-value="{$fields.user_id5_c.value}">{$fields.ddm_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.ddm_response_c.acl > 1 || $fields.ddm_response_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.ddm_response_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DDM_RESPONSE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="ddm_response_c" width='37.5%'  >
{if !$fields.ddm_response_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.ddm_response_c.value) <= 0}
{assign var="value" value=$fields.ddm_response_c.default_value }
{else}
{assign var="value" value=$fields.ddm_response_c.value }
{/if} 
<span class="sugar_field" id="{$fields.ddm_response_c.name}">{$fields.ddm_response_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.ssd_approval_by_c.acl > 1 || $fields.ssd_approval_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.ssd_approval_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_SSD_APPROVAL_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="ssd_approval_by_c" width='37.5%'  >
{if !$fields.ssd_approval_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id8_c" class="sugar_field" data-id-value="{$fields.user_id8_c.value}">{$fields.ssd_approval_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.ssd_response_c.acl > 1 || $fields.ssd_response_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.ssd_response_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_SSD_RESPONSE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="ssd_response_c" width='37.5%'  >
{if !$fields.ssd_response_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.ssd_response_c.value) <= 0}
{assign var="value" value=$fields.ssd_response_c.default_value }
{else}
{assign var="value" value=$fields.ssd_response_c.value }
{/if} 
<span class="sugar_field" id="{$fields.ssd_response_c.name}">{$fields.ssd_response_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.dec_apv_by_c.acl > 1 || $fields.dec_apv_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.dec_apv_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DEC_APV_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="dec_apv_by_c" width='37.5%'  >
{if !$fields.dec_apv_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id11_c" class="sugar_field" data-id-value="{$fields.user_id11_c.value}">{$fields.dec_apv_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.dec_response_c.acl > 1 || $fields.dec_response_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.dec_response_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DEC_RESPONSE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="dec_response_c" width='37.5%'  >
{if !$fields.dec_response_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.dec_response_c.value) <= 0}
{assign var="value" value=$fields.dec_response_c.default_value }
{else}
{assign var="value" value=$fields.dec_response_c.value }
{/if} 
<span class="sugar_field" id="{$fields.dec_response_c.name}">{$fields.dec_response_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.dceo_apv_by_c.acl > 1 || $fields.dceo_apv_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.dceo_apv_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DCEO_APV_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="dceo_apv_by_c" width='37.5%'  >
{if !$fields.dceo_apv_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id4_c" class="sugar_field" data-id-value="{$fields.user_id4_c.value}">{$fields.dceo_apv_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.dceo_response_c.acl > 1 || $fields.dceo_response_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.dceo_response_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DCEO_RESPONSE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="dceo_response_c" width='37.5%'  >
{if !$fields.dceo_response_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.dceo_response_c.value) <= 0}
{assign var="value" value=$fields.dceo_response_c.default_value }
{else}
{assign var="value" value=$fields.dceo_response_c.value }
{/if} 
<span class="sugar_field" id="{$fields.dceo_response_c.name}">{$fields.dceo_response_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.ceo_apv_by_c.acl > 1 || $fields.ceo_apv_by_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.ceo_apv_by_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_APV_BY' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="ceo_apv_by_c" width='37.5%'  >
{if !$fields.ceo_apv_by_c.hidden}
{counter name="panelFieldCount"}

<span id="user_id1_c" class="sugar_field" data-id-value="{$fields.user_id1_c.value}">{$fields.ceo_apv_by_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.ceo_response_c.acl > 1 || $fields.ceo_response_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.ceo_response_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CEO_RESPONSE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="datetimecombo" field="ceo_response_c" width='37.5%'  >
{if !$fields.ceo_response_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.ceo_response_c.value) <= 0}
{assign var="value" value=$fields.ceo_response_c.default_value }
{else}
{assign var="value" value=$fields.ceo_response_c.value }
{/if} 
<span class="sugar_field" id="{$fields.ceo_response_c.name}">{$fields.ceo_response_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_DETAILVIEW_PANEL5").style.display='none';</script>
{/if}
</div>    <div id='tabcontent1'>
<div id='detailpanel_2' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_EDITVIEW_PANEL1' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.mou_title_c.acl > 1 || $fields.mou_title_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.mou_title_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MOU_TITLE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="varchar" field="mou_title_c" width='37.5%' colspan='3' >
{if !$fields.mou_title_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.mou_title_c.value) <= 0}
{assign var="value" value=$fields.mou_title_c.default_value }
{else}
{assign var="value" value=$fields.mou_title_c.value }
{/if} 
<span class="sugar_field" id="{$fields.mou_title_c.name}">{$fields.mou_title_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.client.acl > 1 || $fields.client.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.client.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CLIENT' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="client" width='37.5%'  >
{if !$fields.client.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.account_id_c.value)}
{capture assign="detail_url"}index.php?module=Accounts&action=DetailView&record={$fields.account_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="account_id_c" class="sugar_field" data-id-value="{$fields.account_id_c.value}">{$fields.client.value}</span>
{if !empty($fields.account_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.contact.acl > 1 || $fields.contact.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.contact.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CONTACT' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="contact" width='37.5%'  >
{if !$fields.contact.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.contact_id_c.value)}
{capture assign="detail_url"}index.php?module=Contacts&action=DetailView&record={$fields.contact_id_c.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="contact_id_c" class="sugar_field" data-id-value="{$fields.contact_id_c.value}">{$fields.contact.value}</span>
{if !empty($fields.contact_id_c.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.leads_mou_mou_1_name.acl > 1 || $fields.leads_mou_mou_1_name.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.leads_mou_mou_1_name.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_LEADS_MOU_MOU_1_FROM_LEADS_TITLE' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="leads_mou_mou_1_name" width='37.5%' colspan='3' >
{if !$fields.leads_mou_mou_1_name.hidden}
{counter name="panelFieldCount"}

{if !empty($fields.leads_mou_mou_1leads_ida.value)}
{capture assign="detail_url"}index.php?module=Leads&action=DetailView&record={$fields.leads_mou_mou_1leads_ida.value}{/capture}
<a href="{sugar_ajax_url url=$detail_url}">{/if}
<span id="leads_mou_mou_1leads_ida" class="sugar_field" data-id-value="{$fields.leads_mou_mou_1leads_ida.value}">{$fields.leads_mou_mou_1_name.value}</span>
{if !empty($fields.leads_mou_mou_1leads_ida.value)}</a>{/if}
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.lawyer_notes_c.acl > 1 || $fields.lawyer_notes_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.lawyer_notes_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_LAWYER_NOTES' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="text" field="lawyer_notes_c" width='37.5%' colspan='3' >
{if !$fields.lawyer_notes_c.hidden}
{counter name="panelFieldCount"}

<span class="sugar_field" id="{$fields.lawyer_notes_c.name|escape:'html'|url2html|nl2br}">{$fields.lawyer_notes_c.value|escape:'html'|escape:'html_entity_decode'|url2html|nl2br}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.event_brief.acl > 1 || $fields.event_brief.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.event_brief.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_EVENT_BRIEF ' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="wysiwyg" field="event_brief" width='37.5%' colspan='3' >
{if !$fields.event_brief.hidden}
{counter name="panelFieldCount"}

<iframe
id="{$fields.event_brief.name}"
name="{$fields.event_brief.name}"
srcdoc="{$fields.event_brief.value}"
style="width:100%;height:500px"
></iframe>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_EDITVIEW_PANEL1").style.display='none';</script>
{/if}
</div>    <div id='tabcontent2'>
<div id='detailpanel_3' class='detail view  detail508 expanded'>
{counter name="panelFieldCount" start=0 print=false assign="panelFieldCount"}
<!-- PANEL CONTAINER HERE.. -->
<table id='LBL_EDITVIEW_PANEL4' class="panelContainer" cellspacing='{$gridline}'>
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.assigned_user_name.acl > 1 || $fields.assigned_user_name.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.assigned_user_name.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_ASSIGNED_TO_NAME' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="relate" field="assigned_user_name" width='37.5%'  >
{if !$fields.assigned_user_name.hidden}
{counter name="panelFieldCount"}

<span id="assigned_user_id" class="sugar_field" data-id-value="{$fields.assigned_user_id.value}">{$fields.assigned_user_name.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.lead_id_string_c.acl > 1 || $fields.lead_id_string_c.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.lead_id_string_c.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_LEAD_ID_STRING' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="inlineEdit" type="varchar" field="lead_id_string_c" width='37.5%'  >
{if !$fields.lead_id_string_c.hidden}
{counter name="panelFieldCount"}

{if strlen($fields.lead_id_string_c.value) <= 0}
{assign var="value" value=$fields.lead_id_string_c.default_value }
{else}
{assign var="value" value=$fields.lead_id_string_c.value }
{/if} 
<span class="sugar_field" id="{$fields.lead_id_string_c.name}">{$fields.lead_id_string_c.value}</span>
{/if}

<div class="inlineEditIcon"> {sugar_getimage name="inline_edit_icon.svg" attr='border="0" ' alt="$alt_edit"}</div>			</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.date_entered.acl > 1 || $fields.date_entered.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.date_entered.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_ENTERED' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="datetime" field="date_entered" width='37.5%'  >
{if !$fields.date_entered.hidden}
{counter name="panelFieldCount"}
<span id="date_entered" class="sugar_field">{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.date_modified.acl > 1 || $fields.date_modified.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.date_modified.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_DATE_MODIFIED' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="datetime" field="date_modified" width='37.5%'  >
{if !$fields.date_modified.hidden}
{counter name="panelFieldCount"}
<span id="date_modified" class="sugar_field">{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
{counter name="fieldsUsed" start=0 print=false assign="fieldsUsed"}
{counter name="fieldsHidden" start=0 print=false assign="fieldsHidden"}
{capture name="tr" assign="tableRow"}
<tr>

{if $fields.created_by_name.acl > 1 || $fields.created_by_name.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.created_by_name.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_CREATED' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="relate" field="created_by_name" width='37.5%'  >
{if !$fields.created_by_name.hidden}
{counter name="panelFieldCount"}

<span id="created_by" class="sugar_field" data-id-value="{$fields.created_by.value}">{$fields.created_by_name.value}</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}


{if $fields.modified_by_name.acl > 1 || $fields.modified_by_name.acl > 0}

{counter name="fieldsUsed"}
<td width='12.5%' scope="col">
{if !$fields.modified_by_name.hidden}
{capture name="label" assign="label"}{sugar_translate label='LBL_MODIFIED_NAME' module='MOU_MOU'}{/capture}
{$label|strip_semicolon}:

{else}
{counter name="fieldsHidden"}

{/if}
</td>

<td class="" type="relate" field="modified_by_name" width='37.5%'  >
{if !$fields.modified_by_name.hidden}
{counter name="panelFieldCount"}

<span id="modified_user_id" class="sugar_field" data-id-value="{$fields.modified_user_id.value}">{$fields.modified_by_name.value}</span>
{/if}

</td>

{else}
<td>&nbsp;</td><td>&nbsp;</td>
{/if}

</tr>
{/capture}
{if $fieldsUsed > 0 && $fieldsUsed != $fieldsHidden}
{$tableRow}
{/if}
</table>
</div>
{if $panelFieldCount == 0}
<script>document.getElementById("LBL_EDITVIEW_PANEL4").style.display='none';</script>
{/if}
</div>
</div>
</div>

</form>
<script>SUGAR.util.doWhen("document.getElementById('form') != null",
        function(){ldelim}SUGAR.util.buildAccessKeyLabels();{rdelim});
</script><script type='text/javascript' src='{sugar_getjspath file='include/javascript/popup_helper.js'}'></script>
<script type="text/javascript" src="{sugar_getjspath file='cache/include/javascript/sugar_grp_yui_widgets.js'}"></script>
<script type="text/javascript">
var MOU_MOU_detailview_tabs = new YAHOO.widget.TabView("MOU_MOU_detailview_tabs");
MOU_MOU_detailview_tabs.selectTab(0);
</script>
<script type="text/javascript" src="include/InlineEditing/inlineEditing.js"></script>
<script type="text/javascript" src="modules/Favorites/favorites.js"></script>
{/if}